# SoftVu Keystone

**note:** http://mercurial.selenic.com/wiki/Win32LongFileNamesExtension is needed to be enabled in the hg .ini file on the machine. Some of the node_modules have really really long filenames with the nested modules.

## Why rewrite?

This project was created as a full replacement to the legacy system that was/is in place at SoftVu. The legacy platform has become very cumbersome, it's riddled with technical debt and bugs. What it does though is support the needs of our clients, it just does it poorly. It takes an incredibly long time for a client manager to setup a client and their strategy as it stands. This is mostly related to how the legacy platform defines things, but it also related to a bunch of decisions that were made which cause the performance of the legacy platform to be extremely poor. The technology on the legacy platform is also aging, which is not a primary driver of a rewrite, but definitely helps make the decision easier.

### quickstart

Install all required dependencies on localhost:
  * CouchDB
  * RabbitMQ
  * NodeJS

Run gulp from the root directory of the repository. You should be able to visit the site using localhost:3001/ and localhost:3001/superadmin.

Make sure that `export NODE_ENV=development` on your local machine, or set to the proper environment otherwise.

## What is this repository?

A collection of expressjs websites and nodejs services to manage drip marketing campaigns. This involves the configuration of the strategies, and managing the leads through their lifetime of touchpoints. The 'web' and 'webadmin' folders are expressjs websites. The 'actionqueue', 'eventhose', and 'pipeline' are nodejs services. CouchDB is used as the central storage mechanism for all the clients strategies and configuration data. RabbitMQ is the primary queueing system. Gulp can compile and setup the project to be run in development or on production.

### web

All the error reporting and management that is needed for a single account (strategy). The full intention is to give the client access to this for full control over their marketing strategies.

Each account is the root of the website. A user must be assigned a specific account, or if admin is enabled, they will be given the choice to switch between them.

### webadmin

Anything that would be needing operational attention.

### actionqueue

Watch a couchdb database for jobs that need to be processed and monitor for errors and operational reporting purposes. The major responsibility of this service is to trigger drip messages that are scheduled in the future.

### eventhose

Control the distribution (broker) of events to the systems that are listening. Watch queues for different events which can trigger consumers, either at a system or client level. e.g. log all the events to a global event table, setup the drip messaging when a lead is segmented, etc.

### pipeline

Send an email (drip, batch, notification, sample). This includes fetching of all necessary parts, checking messages for proper structure, template compilation, link generation, and error reporting. Once a message is complete, it will hand off the delivery responsibilites to PowerMTA.

## production environment

The services are installed using winser, which can be seen using the package.json. The scripts pre and post install commands are what is run. Calling `npm install` and `npm uninstall` will do their respective actions. You can edit different settings through regedit HKLM\System\CurrentControlSet\Services\<name>.

# TODO

* add readmes to the projects themselves
* daily, weekly, monthly? maintenance processes need to be written
  * compact/cleanup couchdbs
  * cache maintenance
    * see data/modules.coffee-CacheModule
    * it creates couchdocs based off of a generated view 'query'
* report batching
  * the messages summary view is inefficient
  * events should be batched for reporting, and processed through on a timed interval
* move webadmin subproject into web/admin
* investigate 'lead.agent.changed'
  * right now, this will 'restart' an email. i think that maybe it should cancel the email, then send another one right away?
  * there are some thoughts though that maybe doing this, re-queuing and email might not be straight forward, and we would need to ensure that the right message was the right one getting requeued

# Things that need expanding on, start of longer documentation piece

* why nodejs?
  * it's javascript
    * using coffeescript makes it very readable and safer*
  * very quick iteration cycle
  * cross platform for development and deployment
  * npm is awesome
* why events?
  * distributed, async
  * horizontal scaling
  * decoupling at a system level
* why couchdb (json)
  * schema-less
  * apache foundation
  * written in erlang
* handlebars
* installing on a clean server
  * node is install c:/nodejs
  * NODE_PATH is set to nodejs/node_modules
  * npm set to install to node_modules
  * global install of gulp and coffee-script
  * local install of coffee-script
  * for windows
    * make sure apppool has permissions to execute node.exe
    * make sure apppool has permissions for web/iis folder
* gulp-aglio
  * package isn't building on server (issues in Windows) - generate API doc locally (current issues with node 4.1.1, "nvm use v0.10.26")
