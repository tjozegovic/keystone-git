_ = require 'lodash'
async = require 'async'
csv = require 'csv'

# TODO i'm not really sure if i like this processing in the actionqueue
# should the actionqueue be mostly a router?
module.exports = (ctx, data, done) ->
  queue = async.queue (record, cb) ->
    email = _.assign record,
      client: data.client
      type: 'batch'
      batch: data.batch
      jobid: ctx.id.replace /-/g, ''
    ctx.queues.push 'email', email, cb
  , 4

  # two things need to happen to completely finish, all sends batched, and csv fully parsed
  finish = _.after 2, ->
    ctx.events.emit 'batch.finished', batch: data.batch
    done()

  queue.drain = ->
    finish()

  ctx.events.emit 'batch.started', batch: data.batch

  batches = ctx.use 'client/batches'
  batches.get data.batch, with_csv: yes, (err, body) ->
    parse body.file.toString()
      .on 'record', (record) ->
        queue.push record
      .on 'error', (err) ->
        ctx.events.emit 'batch.error', batch: data.batch, error: err
        done err
      .on 'end', ->
        finish()

module.exports.parse = parse = (from) ->
  # assume that he headers in the csv row can be a dot separated 'path'
  # using those headers, make a complex object for each row
  # e.g. - csv
  #   foo.bar, bar.foo
  #   asdf, qwer
  # result.foo.bar == 'asdf'
  # result.bar.foo == 'qwer'
  csv().from from, columns: yes, relax: yes
    .transform (row) ->
      _nullguard = (obj, key) -> obj[key] ?= {}
      _.transform row, (row, val, key) ->
        keys = key.split '.'
        [final, keys] = [keys.pop(), keys]

        obj = keys.reduce _nullguard, row
        obj[final] = val
