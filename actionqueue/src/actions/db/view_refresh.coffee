_ = require 'lodash'
async = require 'async'

views =
  events: [
    'events/by_date'
  ]

  logs: [
    'logs/by_type_date'
  ]

  '-data': [
    'objects/by_type'
  ]

  '-events': [
    'events/by_fulldate'
    'notifications/by_agent'
    'reports/summary'
  ]

  '-messages': [
    'batches/summary'
    'drips/summary'
    'emails/by_date'
    'lookup/by_lead'
  ]

module.exports = (ctx, data, done) ->
  ctx.server.db.list (err, body) ->
    return done err if err

    refresh_db = (filter, views) ->
      return (cb) ->
        async.eachSeries _.filter(body, (name) -> ~name.indexOf filter)
        , (name, cb) ->
          db = ctx.server.use name
          async.eachSeries views
          , (view, cb) ->
            [design, view] = view.split '/'
            db.view design, view, limit: 0, stale: 'update_after', cb
          , cb
        , cb

    async.series (refresh_db key, val for key, val of views), done
