_ = require 'lodash'
async = require 'async'
moment = require 'moment-timezone'
uuid = require 'uuid'

module.exports = (ctx, data, done) ->
  # should we pass in a date from the job?

  # this should be run late enough in the morning that all senders can have activity
  # calculated from midnight to midnight of their configured tz

  clients = ctx.use 'core/clients'

  query = include_docs: yes, startkey: ['active'], endkey: ['active', {}]
  softvu.view 'objects', 'clients', query, (err, clients) ->
    # TODO? on err??? hope not, should still handle it..
    async.each clients.rows, ({doc: client}, cb) ->
      process_client ctx, data, client, cb
    , done

module.exports.process_client = process_client = (ctx, data, client, done) ->
  unless client.dailyvu?.on
    ctx.events.emit 'dailyvu.skipped', client.id, reason: 'client disabled'
    return done null

  get_senders ctx, client, (err, senders) ->
    async.each senders, (sender, cb) ->
      # i really am not sure i like this... if 'on' not of sender.dailyvu (which might not exist), assume true
      if sender.dailyvu?.on? and not sender.dailyvu?.on
        ctx.events.emit 'dailyvu.skipped', client.id, reason: 'sender disabled', sender: sender.id
        return cb null

      now = moment.tz sender.timezone or client.timezone
      ctx.date = if data?.date?
        moment.tz data.date, sender.timezone or client.timezone
      else
        now.subtract 1, 'day'

      get_activity ctx, client, sender, (err, activity) ->
        unless activity.length
          ctx.events.emit 'dailyvu.skipped', client.id, reason: 'no activity for sender', sender: sender.id
          return cb null

        create_job ctx, client, sender, activity, (err) ->
          # error is emitted through create_job, probably should still handle?
          cb null
    , done

# TODO wrap up senders and activity into a client accesor object
module.exports.get_senders = get_senders = (ctx, client, cb) ->
  # TODO the get_client function below isn't valid anymore. Need to pass a client id though
  # {data} = ctx.db.get_client client.id
  senders = ctx.use 'client/senders'
  senders.active (err, results) ->
    # TODO if err, raise error or fail everything? restart?

    return cb err if err
    # cb null, _.pluck results, 'doc'
    cb null, results

module.exports.get_activity = get_activity = (ctx, client, sender, cb) ->
  jobs = ctx.use 'core/jobs'
  # TODO the get_client function below isn't valid anymore
  {messages} = ctx.db.get_client client.id
  # messages = ctx.use 'client/messages'

  query =
    startkey: [sender.id, ctx.date.startOf('day').toISOString()]
    endkey: [sender.id, ctx.date.endOf('day').toISOString()]
  messages.view 'dailyvu', 'activity', query, (err, {rows: activity}) ->
    # TODO handle err
    cb null, _.pluck activity, 'id'

module.exports.create_job = create_job = (ctx, client, sender, activity, cb) ->
  jobs = ctx.use 'core/jobs'

  id = uuid.v4()
  now = moment.tz sender.timezone or client.timezone
  at = now.startOf('day').add moment.duration sender.dailyvu?.time or client.dailyvu.time or '08:00'

  jobs.set "job:#{id}",
    id: id
    at: at.toISOString()
    status: 'pending'
    action: 'email'
    data:
      client: client.id
      sender: sender.id
      type: 'dailyvu'
      emails: activity
  , (err) ->
    if err
      ctx.events.emit 'dailyvu.errored', client.id, err: err, sender: sender.id
    else
      ctx.events.emit 'dailyvu.scheduled', client.id, jobid: id, sender: sender.id, at: at.toJSON()

    cb err
