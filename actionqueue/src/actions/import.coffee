config = require 'config'
config.actionqueue or= {}

_ = require 'lodash'
async = require 'async'
csv = require 'csv'
domain = require 'domain'
S = require 'shared/util/string'
split = require 'split'

Timer = require 'shared/util/timer'

# directory that contains action modules
ACTIONS = './import/'
# query for timeouts
TIMEOUT = config.actionqueue['job timeout'] or 10000
LIMIT = 50

metrics = tick: 0, processed: 0

module.exports = (ctx, data, done) ->
  {import: import_id, import_type: type, import_table: table} = data
  errors = []
  stats = errors: 0, imported: 0
  @import = {}
  columns = []

  uploads = ctx.use 'client/uploads'
  imports = ctx.use 'client/imports', multiquery: yes
  ctx.optouts = ctx.use 'core/optouts', multiquery: yes
  ctx.leads = ctx.use 'client/leads', multiquery: yes
  [attributes, statuses] = ctx.use 'client/attributes', 'client/statuses'

  # imports.db.query 'set autocommit = on'

  run = (_import, cb) ->
    ctx.logger.info 'job starting', _.pick _import, 'line', 'action'

    on_error = (err) ->
      stats.errors += 1
      ctx.logger.warn 'caught error in row import', import: _import.line, action: 'import/' + type, message: err.message, err: err.err
      errors.push "#{_import.line},#{err.message},\"#{err.err}\""

      imports.save_lines table: table, row: _import.row, doc: err, updated: new Date(), status: 'errored', (err, result) ->
        if err?
          ctx.logger.warn 'caught error in import errored status update', import: _import.line, action: _import.action, stack: err.stack or err
        cb()

    imports.save_lines table: table, row: _import.row, updated: new Date(), status: 'running', (err, result) ->
      if err?
        return on_error message: 'caught error in import running status update', err: err

      try
        action = require ACTIONS + type
        d = domain.create()
        d.on 'error', (err) ->
          delete err.domain
          delete err.domainThrown
          return on_error message: 'import domain error', err: JSON.stringify err

        d.run -> process.nextTick ->
          columns = @import.columns.split ','
          csv().from.string(_import.line, columns: columns).to.array ([record]) ->
            data.record = record
            data.line = _import.line
            if type is 'optout'
              data.started = @import.started

            action ctx, data, (err, result) ->
              if err?
                return on_error message: 'row import failed', err: err

              ctx.logger.info 'row import finished', _.pick _import, 'line', 'action'
              stats.imported += 1
              imports.save_lines table: table, row: _import.row, doc: result, status: 'finished', updated: new Date(), cb
      catch e
        return on_error message: 'import try/catch failed', err: e.message

  query = (tick = ++metrics.tick) ->
    tick_timeout = new Timer ->
      ctx.logger.warn 'tick never finished, reseting query timer'
      setImmediate query
    , TIMEOUT * 2

    finish = ->
      tick_timeout.clear()
      imports.db.close()
      ctx.optouts.db.close()
      ctx.leads.db.close()
      if errors.length
        columns.push 'error'
        _errors = [].concat columns.join(','), errors
        # attachment is just the file column in uploads
        uploads.save type: type + '.import.error', status: 'errored', importId: import_id, new Buffer(_errors.join('\n')), (err, result) ->
          return ctx.logger.error 'could not insert file import errors', err: err if err

          ctx.events.emit type + '.import.finished', import: @import.id, stats: stats, errorId: result.id
          done()
      else
        ctx.events.emit type + '.import.finished', import: @import.id, stats: stats
        done()

    reset = ->
      tick_timeout.clear()
      setImmediate query

    imports.import_lines table: table, limit: LIMIT, status: 'pending', (err, results) ->
      if err?
        ctx.logger.error table + ' query error', err: err
        return reset()

      unless results?.length
        ctx.logger.silly 'no import rows found'
        return finish()

      ctx.logger.info 'query found pending import rows', count: results.length

      async.each results, (row, cb) ->

        job_timeout = new Timer ->
          ctx.logger.warn 'query timedout, requeing query immediately'
          cb()
        , TIMEOUT, delay: yes

        run row, (err) ->
          job_timeout.clear()
          cb err

        job_timeout.start()
      , (err) ->
        reset()

  leads_setup = (cb) ->
    async.parallel
      attributes: (cb) ->
        attributes.active_reg (err, attribs) ->
          return cb err if err
          cb null, doc: attribs, names: _.map attribs, (attr) -> S(attr.name).indexable()
      statuses: (cb) ->
        statuses.allalias (err, body) ->
          return cb err if err
          cb null, _.zipObject _.pluck(body.rows, 'name'), body.rows
    , cb

  uploads.get import_id, (err, result) ->
    if err?
      ctx.logger.error 'error getting import for query', import: import_id, err: err
      return done()

    @import = result
    if type is 'lead'
      leads_setup (err, results) ->
        if err?
          ctx.logger.error 'error getting attributes/statuses for query', import: import_id, err: err
          return done()

        ctx.attributes = results.attributes
        ctx.statuses = results.statuses
        query() # kick off the first run of the query
    else
      query() # kick off the first run of the query

module.exports.detached = yes
