_ = require 'lodash'
split = require 'split'

attributemapper = require 'shared/attributemapper'
segmenter = require 'shared/segmenter'

module.exports = (ctx, data, done) ->
  {record, line} = data
  {@attributes, @statuses} = ctx

  unless status = @statuses[record.status]
    return done message: "#{record.status} is not a valid status name"
  else
    _nullguard = (obj, key) -> obj[key] ?= {}
    lead = _.transform record, (record, val, key) ->
      keys = key.split '.'
      [final, keys] = [keys.pop(), keys]

      obj = keys.reduce _nullguard, record
      obj[final] = val
    , {}

    lead.attributes = {}
    for name in @attributes.names
      lead.attributes[name] = record[name]

    if lead.segmented?.trim().length is 0
      delete lead.segmented

    try
      path = attributemapper.mapPath lead.attributes, @attributes.doc
      segment = segmenter.segment path, status.doc.segments
    catch e
      return done e.message

    _upsert = (type, lead, cb) ->
      ctx.leads.save lead, (err, body) ->
        return done err: err if err?

        ctx.events.emit type, lead: body.id, cb

    {external} = lead
    ctx.leads.getByExternalId external.source, external.id, (err, body) ->
      return done err if err?
      if oldlead = body
        oldstatus = oldlead.$status or oldlead.status
        _upsert 'lead.updated', _.assign(oldlead, lead, $status: status.id, updated: new Date), ->
          unless oldstatus is status.id
            ctx.events.emit 'lead.status.changed', lead: oldlead.id, status: status.id
          done null, lead
      else
        _upsert 'lead.created', _.assign(lead,
          type: 'lead'
          $status: status.id
          created: new Date
        ), ->
          done null, lead
