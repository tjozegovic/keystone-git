isValidEmailAddress = (email) ->
  return no unless email?
  atidx = email.indexOf '@'
  email.length >= 3 && ~atidx && atidx < email.length - 1

module.exports = (ctx, data, done) ->
  {record, line} = data

  unless isValidEmailAddress record.optout
    return done 'invalid email address'

  ctx.optouts.optout record.optout, data.client, (err, body) ->
    return done err if err?

    ctx.events.emit 'optout',
      from: 'website - bulk upload'
      address: record.optout
      user: data.user
      start: data.started
    done null, body
