_ = require 'lodash'
async = require 'async'
split = require 'split'

module.exports = (ctx, data, done) ->
  {import: import_id, import_type: @type} = data

  uploads = ctx.use 'client/uploads'
  imports = ctx.use 'client/imports', multiquery: yes

  uploads.get import_id, (err, impt) ->
    # TODO ugh... we should retry if there's an error here
    return ctx.logger.error 'could not start importing leads', err: err if err
    _import = impt
    _import.started = new Date()
    stats = uploaded: 0, errors: 0
    errors = []

    # attachment is just the file column in uploads
    file = _import.file.toString()
    lines = file.split /\r\n?|\n/
    _import.columns = lines.shift()

    imports.create_table type: @type, at: data.at, (err, result) ->
      return done err if err?

      table = result.name

      async.forEachOfLimit lines, 100, (line, row, cb) ->
        unless line.length
          return cb()
        imports.parse row: row, table: table, started: new Date(), line: line, (err, result) ->
          if err?
            stats.errored += 1
            errors.push err
          else
            stats.uploaded += 1
          cb()
      , ->
        return ctx.logger.error 'could not insert file import data', err: errors if errors.length

        delete _import.$job
        ctx.logger.info @type + ' import stats: ' + import_id + ' errors ' + stats.errored + ' uploaded ' + stats.uploaded

        imports.db.close()
        uploads.save _import, (err, result) ->
          if err?
            ctx.logger.error 'could not insert file import data', err: err
            done()

          ctx.events.emit 'import.parsed', import: import_id, import_type: @type, import_table: table
          done()
