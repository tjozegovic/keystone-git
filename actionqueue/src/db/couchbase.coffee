{EventEmitter} = require 'events'
Couchbase      = require 'couchbase'

class View
  constructor: (@view) ->
  query: (cb) -> @view.query cb

class Database extends EventEmitter
  constructor: ->
    @couch = new Couchbase.Connection
      host: '10.0.0.10'
      bucket: 'default'

    @couch.on 'connect', =>
      @emit('connect')

  view: (doc, view, opts) ->
    new View(@couch.view doc, view, opts)

  get: (id, cb) ->
    @couch.get id, cb

  set: (id, doc, cb) ->
    @couch.set id, doc, cb

module.exports = Database
