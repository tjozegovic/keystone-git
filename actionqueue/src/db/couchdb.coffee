{EventEmitter} = require 'events'
nano = require 'nano'

pendingview = """
(doc) ->
  if doc?.action and doc?.status is 'pending'
    emit doc.at, null
"""

class View
  constructor: (@db, @design, @view, @opts = null) ->
    @_confirmed = false

  _check: (cb) ->
    return cb?() if @_confirmed
    @db.view 'jobs', 'pending_by_time', (err, body) =>
      if err
        switch err.message
          when 'missing', 'deleted'
            @db.insert
              language: "coffeescript"
              views:
                pending_by_time:
                  map: pendingview
            , '_design/jobs', =>
              @_confirmed = true
              cb?()
          when 'missing_named_view'
            @db.get '_design/jobs', (err, body) =>
              body.views['pending_by_time'] =
                map: pendingview
              @db.insert body, '_design/jobs', =>
                @_confirmed = true
                cb?()
      else
        cb?()

  query: (cb) ->
    @_check =>
      @db.view @design, @view, @opts, (err, body) -> cb err, body.rows

module.exports.JobsDatabase = class JobsDatabase extends EventEmitter
  constructor: (@url) ->
    @server = nano @url
    @server.db.create 'jobs', =>
      @db = @server.use 'jobs'
      process.nextTick => @emit 'connect'

  view: (design, view, opts) ->
    new View @db, design, view, opts

  get: (id, cb) ->
    @db.get id, cb

  set: (id, doc, cb) ->
    @db.insert doc, id, cb
