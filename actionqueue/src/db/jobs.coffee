{EventEmitter} = require 'events'
{CronTime} = require 'cron'
_ = require 'lodash'
moment = require 'moment'
nano = require 'nano'
uuid = require 'uuid'

pendingview = """
(doc) ->
  if doc?.action and doc?.status is 'pending'
    emit doc.at, null
"""

class View
  constructor: (@db, @design, @view, @opts = null) ->
    @_confirmed = no

  _check: (cb) ->
    return process.nextTick cb if @_confirmed

    @db.view 'jobs', 'pending_by_time', limit: 0, (err, body) =>
      return unless err or cb?
      return process.nextTick cb unless err

      switch err.message
        when 'missing', 'deleted'
          @db.insert
            language: "coffeescript"
            views:
              pending_by_time:
                map: pendingview
          , '_design/jobs', =>
            @_confirmed = yes
            process.nextTick cb
        when 'missing_named_view'
          @db.get '_design/jobs', (err, body) =>
            body.views['pending_by_time'] =
              map: pendingview
            @db.insert body, '_design/jobs', =>
              @_confirmed = yes
              process.nextTick cb

  query: (cb) ->
    @_check =>
      @db.view @design, @view, @opts, (err, body) -> cb err, body.rows

module.exports.JobsDatabase = class JobsDatabase extends EventEmitter
  constructor: (@url, @ctx) ->
    @server = nano @url
    @server.db.create 'jobs', =>
      @db = @server.use 'jobs'
      process.nextTick => @emit 'ready'

  view: (design, view, opts) ->
    new View @db, design, view, opts

  get: (id, cb) ->
    @db.get id, (err, body) =>
      # TODO better error handling!
      return cb err if err
      cb null, new Job @, body

  set: (id, doc, cb) ->
    doc.created ?= new Date
    @db.insert doc, id, (err) ->
      # TODO better error handling!
      console.error 'error in job insert', err if err
      cb arguments...

module.exports.Job = class Job
  constructor: (@jobs, @doc) ->
    ['action', 'data', 'status'].forEach (prop) =>
      Object.defineProperty @, prop,
        __proto__: null
        value: @doc[prop]

  save = (status, err, cb) ->
    @jobs.ctx.logger.debug 'saving job', id: @doc.id, status: status
    [err, cb] = [null, err] unless cb

    @doc.at ?= new Date
    @doc.status = status
    @doc.updated = new Date

    if err?
      @doc.lasterror = err if err
    else if @doc.lasterror?
      delete @doc.lasterror

    @jobs.set @doc.id, @doc, (err, body) =>
      @jobs.ctx.logger.error 'saving job error', id: @doc.id, err: err if err
      process.nextTick cb if cb?

      return if err
      @doc._rev = body.rev

      return unless status in ['errored', 'processed']
      return unless (repeat = @doc.repeat)?

      ct = new CronTime repeat
      next = ct.sendAt().milliseconds 0 # just for cleanliness

      @doc.at = next.toISOString()
      @doc.status = 'pending'

      @jobs.set @doc.id, @doc, (err) ->
        @jobs.ctx.logger.error 'setting next cron error', id: @doc.id, err: err if err
        console.error 'could not set next cron job!', err if err

  running: (cb) -> save.call @, 'running', cb
  processed: (cb) -> save.call @, 'processed', cb
  errored: (err, cb) -> save.call @, 'errored', err, cb
