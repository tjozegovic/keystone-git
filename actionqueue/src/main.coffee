if ~process.cwd().indexOf 'actionqueue'
  process.env.NODE_CONFIG_DIR ?= '../config'

config = require 'config'
config.actionqueue or= {}

_ = require 'lodash'
async = require 'async'
domain = require 'domain'
moment = require 'moment'
uuid = require 'uuid'

# directory that contains action modules
ACTIONS = './actions/'
# max jobs returned per poll
LIMIT = config.actionqueue['query limit'] or 100
# if no jobs are returned, how long do we wait
PAUSE = config.actionqueue['round pause'] or 3000
# query for timeouts
TIMEOUT = config.actionqueue['job timeout'] or 10000

metrics = tick: 0, processed: 0

# jobs dictionary
track = {}

{Queues} = require './queues'
events = require 'shared/eventlib'
listen = require 'shared/queues/listen'
Timer = require 'shared/util/timer'

logger = do (fs = require 'fs', dir = __dirname + '/../log') ->
  fs.mkdirSync dir unless fs.existsSync dir
  winston = require 'winston'

  return ->
    new winston.Logger
      transports: [
        new winston.transports.File
          filename: dir + '/service.log'
          maxsize: 10 * 1024 * 1024
          maxFiles: 10
          timestamp: yes
          level: 'debug'
          tailable: yes
          json: no
        new winston.transports.Console
          colorize: yes
          timestamp: yes
          level: 'debug'
          json: no
      ]

rootlogger = logger()

{Database, RepositoryFactory} = require 'shared/database'
factory = new RepositoryFactory config.db.url
jobs = factory.use 'core/jobs'
Database::LOGGER = jobs.logger = logger()

listener = listen config.queues.url
listener.consume 'actionqueue', (data, cb) ->
  return cb null unless data.action is 'cancel'

  # check for job in dictionary
  return cb null unless (job = track[data.job])?

  delete track[data.job]
  return cb null unless (handle = job.handle.stop)?

  handle (err) ->
    rootlogger.warn 'received error from stop handler', err: err if err?
    cb null
# when to close listener?

create_context = (job, queues) ->
  ctx =
    id: job.id
    queues: queues
    logger: logger()
    jobs: jobs
    events: events
    use: factory.use.bind factory

  if client = job.data?.client
    factory.client = client
    ctx.events = emit: (type, data, cb) ->
      events.emit type, client, data, cb

  return ctx

run = (id, job, done) ->
  queues = new Queues config.queues.url
  rootlogger.info 'job starting', _.pick job, 'id', 'action'

  on_error = (err) ->
    rootlogger.warn 'caught error in job', job: job.id, action: job.action, stack: err.stack

    queues.close()
    job.errored err, done

  job.running ->
    try
      action = require ACTIONS + job.action

      d = domain.create()
      d.on 'error', (err) ->
        delete err.domain
        delete err.domainThrown
        on_error err

      d.run -> process.nextTick ->
        ctx = create_context job, queues

        track[id] =
          status: job.status
          domain: d
          ctx: ctx
          data: job.data
          handle: action ctx, job.data, (err) ->
            rootlogger.info 'job finished', _.pick job, 'id', 'action'
            delete track[job.id]

            queues.close()
            return job.processed done unless err?

            # TODO emit error too
            job.errored err, done
    catch e
      on_error e

rootlogger.info 'actionqueue started, configured with', limit: LIMIT, pause: PAUSE, timeout: TIMEOUT
query = (tick = ++metrics.tick) ->
  tick_timeout = new Timer ->
    rootlogger.warn 'tick never finished, reseting query timer'
    query_timer.reset()
  , TIMEOUT * 2

  reset = ->
    tick_timeout.clear()
    query_timer.reset()

  jobs.pending limit: LIMIT, (err, results) ->
    if err?
      rootlogger.error 'query error', err: err
      return reset()

    unless results?.length
      rootlogger.silly 'no jobs found, sleeping'
      return reset()

    rootlogger.info 'query found pending jobs', count: results.length
    async.each results, ({id}, cb) ->
      # TODO do we really need to get the job again from the db? this is wiring up
      # a dto object that makes dealing with jobs easier, is this something that the
      # pending query should handle?
      jobs.get id, (err, job) ->
        job_timeout = new Timer ->
          rootlogger.warn 'query timedout, requeing query immediately'
          job.timedout cb
        , TIMEOUT, delay: yes

        rootlogger.warn 'error querying job', id: id, err: err if err?
        return cb() if err?

        if job.status isnt 'pending'
          rootlogger.debug 'job is not pending', id: id
          return cb()

        run id, job, (err) ->
          return if run.detached

          job_timeout.clear()
          cb err unless job_timeout.called

        return cb() if run.detached
        job_timeout.start()
    , reset

query_timer = new Timer query, PAUSE, delay: yes
query_timer.fn() # kick off the first run of the timer

events.heartbeat 'actionqueue.heartbeat', -> metrics: metrics
require('shared/utils/process').lockfile __dirname
