rjs = require 'rabbit.js'

module.exports.Queues = class Queues
  constructor: (@url) ->
    @ctx = rjs.createContext @url

  close: ->
    @ctx?.close()

  push: (queue, data, cb) ->
    push = @ctx.socket 'PUSH'
    push.connect queue, ->
      push.end JSON.stringify data
      process.nextTick cb if cb?
