config = require 'config'

_ = require 'lodash'
events = require 'shared/eventlib'

# middlewares
bodyParser = require 'body-parser'
compression = require 'compression'
cookieParser = require 'cookie-parser'
favicon = require 'serve-favicon'
methodOverride = require 'method-override'
session = require 'express-session'

express = require 'express'
module.exports = app = express()

{RepositoryFactory} = require 'shared/database'

app.use bodyParser.json()
app.use bodyParser.urlencoded extended: yes
app.use compression()

app.set 'port', process.env.PORT or 3003
app.set 'db url', config.db.url
app.set 'queue url', config.queues.url
app.set 'api url', config.api.url
app.set 'offline', config.api.offline

if app.get('env') is 'development'
  morgan = require 'morgan'
  app.use morgan 'dev'

if app.get('env') is 'production'
  app.enable 'trust proxy'
  app.disable 'x-powered-by'

app.use (req, res, next) ->
  req.InvalidRequest = class InvalidRequest extends Error
    constructor: (message) ->
      super message
      @message = message
      @status_code = 400

  next()

# middleware: if the request body is xml, use xml2js to parse the xml
# into a js object, and put in res.body
{parseString} = require 'xml2js'
app.use (req, res, next) ->
  return next() unless req.method is 'POST'

  # idk if this is a bug or something, but text/xml doesn't satisfy req.is('xml')
  return next() unless req.is('xml') or req.is('text/xml')

  data = ''
  req.setEncoding 'utf8'
  req.on 'data', (chunk) ->
    data += chunk

  req.on 'end', ->
    parseString data, explicitArray: no, explicitRoot: no, (err, res) ->
      req.rawxml = data
      req.body = res
    next()

app.use (req, res, next) ->
  req.db = res.locals.server = new RepositoryFactory app.get 'db url'
  return next() unless (~req.url.indexOf '/v') or (~req.url.indexOf '/integration')
  return next() if req.method is 'GET' and /\/v\d+\/?$/.test req.url

  req.key = if rekey = /Key (\w{32})/.exec req.headers.authorization
    rekey[1]
  else
    # TODO allowing the key to be passed in as a query param
    # should eventually be removed.. it is only used right now by
    # softvu (internally), for the ksstatebank (clickrsvp) and mortech integrations
    req.query.key
  return res.status(401).end() unless req.key

  keys = req.db.use 'core/apikeys'
  keys.get req.key, (err, body) ->
    # TODO better logging? or should a notice be raised
    # once a day maybe we should emit an event?
    return res.status(401).end() if err or not body

    req.db.client = req.client = body.client
    next()

app.use (req, res, next) ->
  [_end, start] = [res.end, process.hrtime()]

  log =
    source: 'api'
    at: new Date()
    req: _.pick req, ['method', 'headers', 'query', 'originalUrl', 'ip', 'body']
    res:
      headers: res._headers
      statusCode: res.statusCode

  res.end = ->
    duration = process.hrtime start

    # put the original res.end back, and forward call
    (res.end = _end).apply res, arguments

    logs = req.db.use 'core/logs'
    logs.insert _.assign log,
      $duration: "#{(duration[0] + duration[1] / 1e9).toFixed(4)} s"

  next()

app.use '/v1', require './v1'
app.use '/integration/mortech', require 'shared/integration/mortech/notifications'

app.get '/v:version', (req, res) ->
  res.sendFile __dirname + '/v' + req.params.version + '/api.html'

app.get '/v:version/*', (req, res) ->
  if req.accepts 'html'
    res.status(404).send """
<h1>SoftVu API</h1>
<h2>Client: <small>#{req.client}</small></h2>
<h2>Version: <small>v#{req.params.version}</small></h2>
"""
  else
    res.status(404).json
      client: req.client
      version: req.params.version

app.use (err, req, res, next) ->
  events.emit 'api.errored', req.client,
    at: new Date
    error: err?.message or err
    stack: err.stack
    req: _.pick req, ['headers', 'query', 'originalUrl', 'ip', 'body']
  status = err.status_code or 500
  res.status(status).json status: status, message: err?.message or err

app.on 'mount', (parent) ->
  _.assign app.settings, parent.settings

if require.main is module or process.env.IISNODE_VERSION
  app.get '/', (req, res) ->
    res.redirect '/v1'

  app.use (req, res) ->
    res.status(404).send 'Invalid API Version or method call'

  app.listen app.get('port'), ->
    console.log "API server listening on port #{app.get('port')}"
