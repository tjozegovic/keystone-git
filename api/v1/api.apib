FORMAT: 1A
HOST: https://api.softvu.com/v1

# SoftVu API

## API Integration
Our API is an HTTPS REST API which is hosted at the following base endpoint below. POST and PUT methods will accept JSON (application/json).

```no-highlight
https://api.softvu.com/v1/:object
```

You will need to use the HTTP Authorization header to submit your API key with your request.

```no-highlight
Authorization = Key :key
e.g. Authorization = Key 59b9e8a83d0f4266abc485e0237072b2
```

::: warning
If you do not have the authorization header properly set, you will receive a **401 Unauthorized** response.
:::

# Webhook Events
We will post JSON back to a URL you provide. This JSON will have information regarding the event that occured, and may be used to keep your systems up-to-date, or trigger other actions.

::: note
**email.queued**

An email has been sent to our outbound mail queues.

**email.errored**

An email failed to process and will not be sent.

**email.delivered**

An email was delivered to the recipients inbound servers.  This does **not** indicate the email is in the inbox, but does indicate that we have done everything we can.

**email.bounced**

An email was unable to be delivered and will never be delivered to that email address.

**email.deferred**

An email was temporarily rejected by the recipient server and will be retried at a later date.

**email.opened**

An email was opened.  The client or recipient server downloaded our embedded tracking pixel.

**email.clicked**

A URL within the email was clicked.

<!-- TODO **email.reported**

An email was reported by the recipient server as spam or the user clicked on the spam button. -->

<!-- TODO don't know where this one is supposed to be grouped with - **form.response**  -->
:::


::: note
**optin**

An email address was opted-in through a manual process or through an opt-in form.

**optout**

An email address has either opted-out through our optout link, a hard bounce, or we caught a spam report message through a [FBL](http://en.wikipedia.org/wiki/Feedback_loop_(email)).
:::

::: note
**lead.segmented**

A lead was successfully segmented and will begin receiving the emails defined in that segment.

**lead.timedout**

A lead has reached the end of a segment and no more triggered messages will be sent.
:::

::: note
**batch.started**

A batch send was started and all records associated with that batch are being processed.

**batch.error**

A batch send errored.

**batch.finished**

A batch send was completely processed and all records were sent to the email engine.  They will receive email events defined above.
:::

# Data Structures

## LeadData
+ status: New (required)
+ attributes: (required)
  + leadsource: Web (required)
  + product: Purchase
+ agent: jane.agent@email.com (required) - Agent's email address
+ recipient: (required)
  + firstname: John
  + lastname: Doe
  + email: john.doe@email.com (required) - Lead's email address
  + address:
      + line1: 12345 Easy St.
      + line2: address2
      + city: Nowhere
      + state: AK
      + zip: 12345
  + phones:
      + primary: 1234567890
      + cell: 0987654321
      + group: 1234509876
+ external: (required)
    + source: source (required)
    + id: 12345 (required)

## GroupData
+ name: Loan Officers (required)
+ email: loanofficers@email.com
+ address:
  + address1: 12345 Easy St.
  + address2: address2
  + city: Nowhere
  + state: AK
  + zip: 12345
+ active: true (boolean)
+ phones:
  + main: 1234567890
  + cell: 0987654321
  + fax: 1234509876

## SenderData
+ email: new.sender@email.com (required)
+ aliases: (array)
  + NewSender
+ firstname: New (required)
+ lastname: Sender (required)
+ active: true (boolean)
+ group: 6e36a10c1bff2a7b12345a29ff19d289
+ default sender?: no - Set to 'yes' to make this sender the default sender
+ phones:
  + main: 1234567890
  + cell: 0987654321
  + fax: 1234509876

## DirectSendWithIntegration
+ message: intro (required) - This is the slugname of the message configured on SoftVu's platform
+ agent: jane.agent@email.com (required)
+ lead: (required)
  + source: velocify (required) - This is the name of the external system.  Must be used in conjunction with ID below.
  + id: 29309023 (required) - This is the lead ID in the external system.  Must be used in conjunction with source above.

## DirectSendEmailOnly
+ message: intro (required) - This is the slugname of the message configured on SoftVu's platform
+ agent: jane.agent@email.com (required)
+ lead: (required)
  + email: john.doe@gmail.com (required)

# Group Leads

## GET [/leads{?external}]

### Get Lead [GET]
Use this endpoint to query a lead.

+ Parameters

    + external: %7B%22source%22%3A%22source%22%2C%22id%22%3A12 (JSON, required) - Pass in a JSON object as the external query parameter which has a source and id field defined. These values should be the exact same as used when creating the lead. The external JSON object will be parsed and must be valid JSON.

+ Response 200 (application/json)

    + Body

            {
              "id": "8461dbd128a303f5d26a968a761990bc",
              "status": "Open - Not Contacted",
              "attributes": {
                "product": "Purchase",
                "leadsource": "Web"
              },
              "agent": "softvusf@email.com",
              "recipient": {
                "email": "apitest@softvu.com",
                "firstname": "api",
                "lastname": "test",
                "address": {},
                "phones": {}
              },
              "external": {
                "source": "source",
                "id": "00Q61000002hAmUEAU",
              },
              "client": "25ca04b0565a11e5a8b78770bbc1b432",
              "created": "2015-09-17T21:49:40.311Z",
              "segmented": "2015-09-17T21:49:40.339Z",
              "segment": "f639053512fa433fa7cf0fbe91af6764",
              "updated": "2015-09-19T21:54:04.775Z",
              "step": "1a592f87733c47589a7c08f021e082f6"
            }

+ Response 404

    + Body

            "Not found"

## POST [/leads]

### Create/Update New Lead [POST]
POST JSON to this endpoint to insert a new lead into a drip campaign, setup by status. We will examine the status and attributes of the lead to determine which segment in the campaign this lead will receive messages from. If the status of the lead changes, any scheduled messages will be canceled, and the lead will be re-segmented.

It is important that when you are adding leads, you supply the external source and id of this lead. Typically, the source is a string identifying the system where the lead resides, and the id is a value which is unique to that system.

**Note:** POSTing will behave as an *upsert* if you supply a different source, id combination.

+ Request (application/json)

    + Attributes(LeadData)

+ Response 200 (application/json)

    + Body

            {
                "id": "c156953beae6cf6bb91b38ae560236bf"
            }

# Group Optouts

## Lookup Optout [/optouts/{email}]

### Lookup Optout [GET]
Use this object to query your optout database for an email address.

+ Parameters

    + email: test1@email.com - Lookup an optout by email address

+ Response 200 (application/json)

    + Body

            {
              "email": "test1@email.com",
              "history": [
                {
                  "type": "optout",
                  "at": "2015-09-23T15:01:34.733Z",
                  "from": "api"
                }
              ]
            }

+ Response 404

## GET Optouts Since [/optouts{?since}]

### Get Optouts Since [GET]
Use this object to query your optout database for all opt-outs since that date.

+ Parameters

    + since: `2015-09-24T21:22:39.939Z` (required) - Pass in a UTC date formatted as **ISO 8601 (YYYY-MM-DDTHH:MM:SSZ)** to retrieve all optouts that have occured since that date.

+ Response 200 (application/json)

    + Body

            [
              {
                "email": "test3@email.com",
                "history": [
                  {
                    "type": "optout",
                    "at": "2015-09-23T15:01:44.737Z",
                    "from": "api"
                  }
                ]
              },
              {
                "email": "test2@email.com",
                "history": [
                  {
                    "type": "optout",
                    "at": "2015-09-23T15:01:41.543Z",
                    "from": "api"
                  }
                ]
              },
              {
                "email": "test1@email.com",
                "history": [
                  {
                    "type": "optout",
                    "at": "2015-09-23T15:01:34.733Z",
                    "from": "api"
                  }
                ]
              }
            ]

## POST [/optouts]

### Add Optout [POST]
Use this object to add an optout to your optout database.  Any email that SoftVu attempts to send to an address in this database will be canceled.

+ Request (application/json)

    + Body

            {
                "email": "im.an.optout@email.com"
            }

+ Response 201

    + Headers

            Location: /v1/optouts/im.an.optout@email.com

# Group Optins

## POST [/optins]

### Add Optin [POST]
Use this object to optin an email address.

+ Request (application/json)

    + Body

            {
                "email": "opt.me.back.in@email.com"
            }

+ Response 201

    + Headers

            Location: /v1/optins/opt.me.back.in@email.com

# Group Groups
Manipulate the groups associated with your account. Senders may be assigned to a specific group to inherit these values.

## Groups Operations [/groups]

### Get Groups [GET]
Retrieve groups associated with your account.

+ Response 200 (application/json)

    + Body

            [
              {
                "id": "6e36a10c1bff2a7b73647a29ff178a7c",
                "name": "SoftVu",
                "email": "support@softvu.com",
                "address": {
                  "address1": "7381 W. 135th St",
                  "address2": "Suite 402",
                  "city": "Overland Park",
                  "state": "KS",
                  "zip": "66213"
                },
                "phones": {},
                "updated": "2014-07-25T21:25:12.477Z"
              },
              {
                "id": "6e36a10c1bff2a7b73647a29ff19d289",
                "name": "Loan Officers",
                "phones": {},
                "active": true,
                "updated": "2014-08-07T20:51:17.494Z"
              }
            ]

### Create New Group [POST]
POST JSON to this endpoint to create a new group.

+ Request (application/json)

    + Attributes(GroupData)

+ Response 200 (application/json)

    + Body

            {
                "id": "c156953beae6cf6bb91b38ae56008d92"
            }

## Get/Edit Group [/groups/{id}]

### Get Group [GET]
Use this endpoint to query a group.

+ Parameters

  + id: 6e36a10c1bff2a7b73647a29ff19d289 - The group ID

+ Response 200 (application/json)

    + Body

            {
                "id": "6e36a10c1bff2a7b73647a29ff19d289",
                "name": "Loan Officers",
                "phones": {},
                "active": true,
                "updated": "2014-08-07T20:51:17.494Z"
            }

+ Response 404

### Update Group [PUT]
Use this endpoint to update a group.

+ Parameters

  + id: 6e36a10c1bff2a7b73647a29ff19d289 - The group ID

+ Request (application/json)

    + Attributes(GroupData)

+ Response 200 (application/json)

    + Body

            {
                "ok": true
            }

### Delete Group [DELETE]
Use this endpoint to delete a group.

+ Parameters

  + id: 6e36a10c1bff2a7b73647a29ff19d289 - The group ID

+ Response 200 (application/json)

    + Body

            {
                "ok": true
            }

# Group Senders
Manipulate the senders associated with your account. Senders must have a unique email address. When posting leads or setting up batch sends, the sender email address is used to pull template values which can be used in replacement variables.

## Senders Operations [/senders]

### Get Senders [GET]
Retrieve senders associated with your account.

+ Response 200 (application/json)

    + Body

            [
              {
                "id": "6bfca41f0e83873e48ed6b37247c3d0c",
                "phones": {
                  "main": "111-222-3333",
                  "cell": "444-555-6666",
                  "fax": "777-888-9999"
                },
                "group": "6e36a10c1bff2a7b79332a29ff19d289",
                "email": "senderone@email.com",
                "firstname": "Sender",
                "lastname": "One",
                "custom": {
                  "NMLS": "54321",
                  "brokersite id": "123456789",
                  "title": "Mortgage Loan Originator"
                },
                "active": true,
                "inserted": "2014-08-22T17:48:01.034Z",
                "updated": "2015-06-15T18:57:40.585Z",
                "lastnotification": "2014-10-27T21:21:24.527Z",
                "aliases": [
                  "Sender One",
                  "One, Sender"
                ],
                "default sender?": "no",
                "override": false
              },
              {
                "id": "6bfca41f0e87167e48ed6b3724aa0472",
                "phones": {
                  "main": "816-888-8888",
                  "cell": "",
                  "fax": ""
                },
                "group": "6e36a10c1bff2a7b73647a29ff19d289",
                "firstname": "Sender",
                "lastname": "Two",
                "custom": {
                  "NMLS": "12345"
                },
                "active": false,
                "updated": "2015-06-15T18:57:40.586Z",
                "email": "sendertwo@email.com",
                "aliases": [
                  "Sender Two",
                  "Two, Sender"
                ],
                "default sender?": "no",
                "override": false,
                "inserted": "2015-06-15T18:57:00.000Z"
              }
            ]

+ Response 404

### Create New Sender [POST]
POST JSON to this endpoint to create a new sender.

+ Request (application/json)

    + Attributes(SenderData)

+ Response 200 (application/json)

    + Body

            {
                "id": "c156953beae6cf6bb91b38ae5602871b"
            }

## Get Active Senders [/senders/active]

### Get Active Senders [GET]
User this query to get only the active senders in the account (active: true).

+ Response 200 (application/json)

    + Body

            [
              {
                "id": "6bfca41f0e83873e48ed6b37247c3d0c",
                "phones": {
                  "main": "111-222-3333",
                  "cell": "444-555-6666",
                  "fax": "777-888-9999"
                },
                "group": "6e36a10c1bff2a7b79332a29ff19d289",
                "email": "senderone@email.com",
                "firstname": "Sender",
                "lastname": "One",
                "custom": {
                  "NMLS": "54321",
                  "brokersite id": "123456789",
                  "title": "Mortgage Loan Originator"
                },
                "active": true,
                "inserted": "2014-08-22T17:48:01.034Z",
                "updated": "2015-06-15T18:57:40.585Z",
                "lastnotification": "2014-10-27T21:21:24.527Z",
                "aliases": [
                  "Sender One",
                  "One, Sender"
                ],
                "default sender?": "no",
                "override": false
              }
            ]

## Get/Edit Sender [/senders/{id}]

### Get Sender [GET]
Use this endpoint to query a sender.

+ Parameters

  + id: c156953beae6cf6bb91b38ae5602871b - The sender ID

+ Response 200 (application/json)

    + Body

            {
                "id": "c156953beae6cf6bb91b38ae5602871b",
                "inserted": "2015-09-23T15:48:42.034Z",
                "email": "new.sender@email.com",
                "aliases": [
                  "NewSender"
                ],
                "firstname": "New",
                "lastname": "Sender",
                "active": true,
                "group": "6e36a10c1bff2a7b73647a29ff19d289",
                "default sender?": "no",
                "phones": {
                  "main": "1234567890",
                  "cell": "0987654321",
                  "fax": "1234509876"
                },
                "updated": "2015-09-23T15:51:45.036Z"
            }

### Update Sender [PUT]
Use this endpoint to update a sender.

+ Parameters

  + id: c156953beae6cf6bb91b38ae5602871b - The sender ID

+ Request (application/json)

    + Attributes(SenderData)

+ Response 200 (application/json)

    + Body

            {
                "ok": true
            }

### Delete Sender [DELETE]
Use this endpoint to delete a sender.

+ Parameters

  + id: c156953beae6cf6bb91b38ae5602871b - The sender ID

+ Response 200 (application/json)

    + Body

            {
                "ok": true
            }

# Group Direct Sends
There are multiple ways to initiate a direct send.

## POST [/send]

### Direct Send with Integration [POST]
Use this endpoint to direct send an e-mail when integrated with an external system.

+ Parameters

  + key: e8062b295e224c4f8273daed1ab66a59 (optional) - Your API key, required if the authorization header is not set.

+ Request (application/json)

  + Attributes(DirectSendWithIntegration)

+ Response 202

### Direct Send E-mail Only [POST]
Use this endpoint to direct send an e-mail.

+ Parameters

  + key: e8062b295e224c4f8273daed1ab66a59 (optional) - Your API key, required if the authorization header is not set.

+ Request (application/json)

  + Attributes(DirectSendEmailOnly)

+ Response 202
