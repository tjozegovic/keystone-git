_ = require 'lodash'
express = require 'express'

events = require 'shared/eventlib'

module.exports = router = express.Router()

router.use (req, res, next) ->
  req.publish = (type, id) ->
    events.emit "group.#{type}", req.client,
      id: id or req.doc?.id
      request: _.pick req, ['headers', 'query', 'originalUrl', 'ip']

  req.parse = (doc) ->
    _.assign # create a new doc with id as the first property, then the rest
      id: doc.id
    , _.omit doc, (val, key) -> key[0] in ['$', '_'] or key in ['type', 'signature', 'version', 'published']

  next()

router.param 'id', (req, res, next, id) ->
  return next() if /[a-zA-Z0-9]{32}/.test id
  next 'route'

router.get '/', (req, res, next) ->
  groups = req.db.use 'client/groups'
  groups.all (err, body) ->
    return next err if err
    res.json _(body).map(req.parse).value()

router.get '/:id', (req, res, next) ->
  groups = req.db.use 'client/groups'
  groups.get req.params.id, (err, body) ->
    return next err if err
    return res.status(404).end() unless body?
    res.json req.parse body

router.post '/', (req, res, next) ->
  _.assign req.body,
    type: 'group'
    active: yes
    inserted: new Date
    updated: new Date

  groups = req.db.use 'client/groups'
  groups.save req.body, (err, body) ->
    return next err if err
    res.json id: body.id
    req.publish 'created', body.id

router.put '/:id', (req, res, next) ->
  groups = req.db.use 'client/groups'
  groups.get req.params.id, (err, body) ->
    return next err if err

    doc = _(body)
      .pick 'id', 'inserted', 'signature', 'version'
      .assign _.omit req.body, 'id'
      .assign
        type: 'group'
        updated: new Date
      .value()
    groups.update req.params.id, doc, (err) ->
      return next err if err
      res.json ok: yes
      req.publish 'updated'

router.delete '/:id', (req, res, next) ->
  groups = req.db.use 'client/groups'
  groups.delete req.params.id, (err) ->
    return next err if err
    res.json ok: yes
    req.publish 'deleted'
