config = require 'config'

_ = require 'lodash'
async = require 'async'
express = require 'express'
rjs = require 'rabbit.js'

module.exports = router = express.Router()

_bulk = (req, res, arr, cb) ->
  ctx = rjs.createContext config.queues.url
  ctx.once 'ready', ->
    pub = ctx.socket 'PUSH'
    pub.connect 'email', ->
      cache = messages: {}, agents: {}

      async.mapLimit arr, 20
      , (message, cb) ->
        unless message.message? and message.agent?
          return cb null, ok: no, invalid: yes, err: 'Request must include a valid message slug and agent email.'

        {override} = message
        async.parallel
          message: (cb) ->
            {message: slug} = message
            return cb cache.messages[slug].error if cache.messages[slug]?.error?
            return cb null, cache.messages[slug] if cache.messages[slug]?

            messages = req.db.use 'client/messages'
            messages.bySlug slug, (err, body) ->
              # TODO the error callback needs to be rewritten for messages and agent
              return cb err if err
              return cb (cache.messages[slug] = error: "Could not find message (#{slug}).").error unless body?
              cb null, cache.messages[slug] = body.id
          agent: (cb) ->
            {agent} = message
            return cb cache.agents[agent].error if cache.agents[agent]?.error?
            return cb null, cache.agents[agent] if cache.agents[agent]?

            senders = req.db.use 'client/senders'
            senders.by_name_or_alias agent, (err, body) ->
              return cb err if err
              return cb (cache.agents[agent] = error: "Could not find agent (#{agent}).").error unless body?
              cb null, cache.agents[agent] = agent
          lead: (cb) ->
            # TODO should we rename lead_id or external id?
            return cb null unless message.lead?.lead_id? or message.lead?.email? or (message.lead?.source? and message.lead?.id?)

            if message.lead.lead_id
              cb null, message.lead.lead_id.replace /-/g, ''
            else if message.lead.email
              cb null, message.lead
            else
              {source, id} = message.lead
              leads = req.db.use 'client/leads'
              leads.getByExternalId source, id, (err, body) ->
                return cb err if err
                return cb "Could not find lead (source: #{source}, id: #{id})." unless body?
                cb null, body.id
        , (err, {message, agent, lead}) ->
          return cb null, ok: no, err: err.message or err if err

          mail = _.defaults {},
            type: 'direct'
            client: req.client
            message: message
            agent: agent
            lead: lead
          , override

          pub.write JSON.stringify mail
          cb null, ok: yes
      , (err, results) ->
        pub.close()
        ctx.close()
        cb results

router.post '/send', (req, res, next) ->
  _bulk req, res, [req.body], ([result]) ->
    if result.invalid?
      return next new req.InvalidRequest result.err
    else unless result.ok
      return next new Error result.err
    else
      res.status(202).end()

router.post '/send/_bulk', (req, res) ->
  _bulk req, res, req.body, (results) ->
    res.status(200).json results

router.use '/groups', require './groups'
router.use '/leads', require './leads'
router.use '/optins', require './optins'
router.use '/optouts', require './optouts'
router.use '/senders', require './senders'
