_ = require 'lodash'
express = require 'express'
moment = require 'moment'

events = require 'shared/eventlib'
attributemapper = require 'shared/attributemapper'
segmenter = require 'shared/segmenter'

module.exports = router = express.Router()

router.use (req, res, next) ->
  req.parse = (doc) ->
    _.assign # create a new doc with id as the first property, then the rest
      id: doc.id
    , _.omit doc, (val, key) -> key[0] in ['_', '$'] or key is 'type'

  next()

router.param 'id', (req, res, next, id) ->
  return next() if /[a-zA-Z0-9]{32}/.test id
  next 'route'

router.get '/', (req, res, next) ->
  source = id = undefined
  if req.query.external
    try
      {source, id} = JSON.parse req.query.external
    catch
      return res.status(400).end 'Invalid JSON for query external.'
  else
    {source, id} = req.query

    # TODO oooo... this is nasty..
    # when a lead is created, it uses the 'external' JSON property to key it. the id could be a string or integer
    # since the query parameters are all strings, we have to potentially do a little conversion here
    # this is actually really bad because if the id is a string that looks like an integer when it comes over...
    # TODO should the id be forced into a string to avoid this?
    id = if isNaN(float = parseFloat id) then id else float

  return next() unless source? and id?
  leads = req.db.use 'client/leads'
  leads.getByExternalId source, id, (err, body) ->
    return next err if err
    return res.status(404).end 'Not found' unless body?

    res.json req.parse body

router.get '/:id', (req, res, next) ->
  leads = req.db.use 'client/leads'
  leads.get req.params.id, (err, body) ->
    return next err if err
    res.json req.parse body

router.post '/', (req, res, next) ->
  if req.app.get 'offline'
    lead = api_key: req.key, body: req.body
    offline_leads = res.locals.server.use 'offline_leads'
    return offline_leads.insert lead, (err, body) ->
      res.json deferred: body.id

  return next new Error "A lead must have a 'status'." unless req.body.status

  statuses = req.db.use 'client/statuses'
  statuses.byalias req.body.status, (err, body) ->
    if err
      return next err
    else unless status = body
      return next new Error "Status '#{req.body.status}' is not a valid status name."

    # ignore any ids that are passed in through the POST
    # we only care about external.source and external.id
    delete req.body.id if req.body.id?

    _upsert = (type, lead, cb) ->
      leads = req.db.use 'client/leads'
      leads.save lead, (err, body) ->
        # TODO this really should not be checking a pg error
        # rather should be checking for a abstract contstraint violation
        # unique constraint violation
        if err?.code is '23505'
          return setTimeout _save, 1

        lead.id = body.id

        attrs = req.db.use 'client/attributes'
        attrs.active_reg (err, attributes) ->
          return next err if err

          try
            path = attributemapper.mapPath lead.attributes, attributes
            segment = segmenter.segment path, status.segments

            events.emit type, lead.client, lead: lead.id
            res.json id: lead.id
            cb?()
          catch e
            next e

    {external} = req.body
    leads = req.db.use 'client/leads'

    do _save = ->
      leads.by_external external.source, external.id, (err, body) ->
        return next err if err
        if lead = body
          oldstatus = lead.$status or lead.status
          _upsert 'lead.updated', _.assign(lead, req.body, $status: status.id, updated: new Date), ->
            unless oldstatus is status.id
              events.emit 'lead.status.changed', req.client, lead: lead.id, status: status.id
        else
          _upsert 'lead.created', _.assign req.body,
            type: 'lead'
            client: req.client
            $status: status.id
            created: new Date
