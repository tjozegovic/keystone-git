_ = require 'lodash'
express = require 'express'
moment = require 'moment'

events = require 'shared/eventlib'

module.exports = router = express.Router()

_fix = (email) -> email.trim().toLowerCase()

router.use (req, res, next) ->
  req.optouts = req.db.use 'core/optouts'
  next()

router.post '/', (req, res) ->
  optout = req.body
  optout.email = _fix optout.email

  save = ->
    data = from: 'api'
    req.optouts.optin optout.email, req.client, data, (err, body) ->
      events.emit 'optin', req.client,
        from: 'api'
        email: optout.email
        client: req.client
      , -> res.status(201).header('Location', "#{req.originalUrl}/#{optout.email}").end()

  save()
