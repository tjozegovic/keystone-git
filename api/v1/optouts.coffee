_ = require 'lodash'
express = require 'express'
moment = require 'moment'

events = require 'shared/eventlib'

module.exports = router = express.Router()

_fix = (email) -> email.trim().toLowerCase()

_optoutAsClientJson = (client) ->
  (optout) ->
    email: optout.id
    history: _(optout.history)
      .filter (hist) ->
        hist.type is 'optout' and hist.client is client
      .map (hist) ->
        _.omit hist, 'client'
      .value()

router.use (req, res, next) ->
  req.optouts = req.db.use 'core/optouts'
  next()

router.get '/', (req, res, next) ->
  return next new Error 'Invalid date format.' unless (since = moment req.query.since).isValid() if req.query.since?

  req.optouts.since req.client, since, (err, body) ->
    return next err if err
    res.json _.map body, _optoutAsClientJson req.client

router.get '/:email(*@*)', (req, res, next) ->
  email = _fix req.params['email']
  req.optouts.get email, (err, doc = {}) ->
    return res.status(404).end() unless req.client in doc.clients
    res.json _optoutAsClientJson(req.client) doc

router.post '/', (req, res) ->
  optout = req.body
  optout.email = _fix optout.email

  save = ->
    data = from: 'api'
    req.optouts.optout optout.email, req.client, data, (err, body) ->
      events.emit 'optout', req.client,
        from: 'api'
        email: optout.email
        client: req.client
      , -> res.status(201).header('Location', "#{req.originalUrl}/#{optout.email}").end()

  save()
