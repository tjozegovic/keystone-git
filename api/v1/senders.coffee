_ = require 'lodash'
express = require 'express'

events = require 'shared/eventlib'

module.exports = router = express.Router()

router.use (req, res, next) ->
  req.publish = (type, id) ->
    events.emit "sender.#{type}", req.client,
      id: id or req.doc?.id
      request: _.pick req, ['headers', 'query', 'originalUrl', 'ip']

  req.parse = (doc) ->
    _.assign # create a new doc with id as the first property, then the rest
      id: doc.id
    , _.omit doc, (val, key) -> key[0] is '_' or key is 'type'

  next()

router.param 'id', (req, res, next, id) ->
  return next() if /[a-zA-Z0-9]{32}/.test id
  next 'route'

router.get '/', (req, res, next) ->
  if req.query.active?
    active req, res, next
  else
    senders = req.db.use 'client/senders'
    senders.all (err, body) ->
      return next err if err
      res.json _(body).map(req.parse).value()

router.get '/active', active = (req, res, next) ->
  senders = req.db.use 'client/senders'
  senders.active (err, body) ->
    return next err if err
    res.json _(body).map(req.parse).value()

router.get '/:id', (req, res, next) ->
  senders = req.db.use 'client/senders'
  senders.get req.params.id, (err, body) ->
    return next err if err
    return res.status(404).end() unless body?
    res.json req.parse body

router.post '/', (req, res, next) ->
  unless req.body.email?
    return next new Error "a sender requires an 'email' property"

  senders = req.db.use 'client/senders'

  senders.byEmail req.body.email, (err, body) ->
    _upsert = (type, sender) ->
      sender.type = 'sender'
      senders.upsert sender, (err, body) ->
        return next err if err
        res.json id: body.id
        req.publish type, body.id

    if sender = body[0]
      _upsert 'updated',
        _(sender)
          .pick 'id', 'version', 'inserted'
          .assign _.omit req.body, 'id'
          .assign updated: new Date
          .value()
    else
      _upsert 'inserted',
        _(req.body)
          .omit 'id'
          .assign inserted: new Date
          .value()

router.put '/:id', (req, res, next) ->
  senders = req.db.use 'client/senders'
  senders.get req.params.id, (err, body) ->
    return next err if err

    doc = _(body)
      .pick 'id', 'version', 'inserted'
      .assign _.omit req.body, 'id'
      .assign
        type: 'sender'
        updated: new Date
      .value()

    senders.upsert doc, (err) ->
      return next err if err

      res.json ok: yes
      req.publish 'updated'

router.delete '/:id', (req, res, next) ->
  senders = req.db.use 'client/senders'
  senders.delete req.params.id, (err) ->
    return next err if err
    res.json ok: yes
    req.publish 'deleted'
