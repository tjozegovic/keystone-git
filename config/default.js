module.exports = config = {};

config.env = process.env.NODE_ENV || 'dev';
config.tmpdir = '/tmp';

config.sql = {};
config.sql.show = false;
config.sql.trimstack = true;

config.api = {};
config.api.url = 'http://localhost:3003';
config.api.offline = false;

config.db = {};
config.db.url = 'postgres://keystone:keystone@localhost/keystone';

config.queues = {};
config.queues.url = 'amqp://localhost';
config.queues.api = 'http://guest:guest@localhost:15672/api';

config.sessions = {};
config.sessions.host = 'localhost';

config.links = {};
config.links.baseurl = 'http://localhost:3001';

config.admin = {};
config.admin['allow cloning'] = false;
config.admin['allow delete'] = false;
config.admin.cloning = {};

config.pipeline = {};
config.pipeline['retry wait in seconds'] = [0.5, 1.1, 8];

config.pipeline.smtp = {};
config.pipeline.smtp['host'] = '127.0.0.1';
config.pipeline.smtp['port'] = 10025;
config.pipeline.smtp['ignoreTLS'] = true;

config.webhooks = {};
config.webhooks['retry wait in minutes'] = [0.5, 3, 7, 15];

config.aws = {};
config.aws.accessKeyId = '';
config.aws.secretAccessKey = '';
config.aws['upload html'] = false;
