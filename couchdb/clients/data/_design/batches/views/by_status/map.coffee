(doc) ->
  if doc.type is 'batch'
    emit [doc.status, doc.updated or doc.created]
