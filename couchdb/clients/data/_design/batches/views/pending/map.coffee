(doc) ->
  if doc.type is 'batch' and doc.status not in ['finished', 'failed', 'archived', 'deleted']
    emit [doc.updated, doc.status]
