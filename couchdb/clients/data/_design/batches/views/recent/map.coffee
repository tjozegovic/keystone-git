(doc) ->
  if doc.type is 'batch' and doc.status in ['finished', 'failed']
    emit [doc.updated, doc.status]
