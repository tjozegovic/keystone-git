(doc) ->
  if doc.type is 'lead'
    emit [doc.recipient.email, doc.segmented or doc.created]
