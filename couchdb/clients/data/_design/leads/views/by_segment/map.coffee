(doc) ->
  # TODO fix $status or status when most? leads are converted
  status = doc.$status or doc.status
  return unless doc.type is 'lead' and status? and doc.segment?
  emit [status, doc.segment, doc.segmented or doc.created]
