(doc) ->
  if doc.type is 'lead'
    emit doc.updated or doc.created
