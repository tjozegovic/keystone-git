(doc) ->
  {external} = doc
  if doc.type is 'lead' and 'source' of external and 'id' of external
    emit [external.source, external.id]
