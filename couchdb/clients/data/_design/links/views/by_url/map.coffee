(doc) ->
  if doc?.type is 'link'
    emit doc.url, null
