(doc) ->
  if doc?.type is 'message'
    emit doc._id, doc.name
