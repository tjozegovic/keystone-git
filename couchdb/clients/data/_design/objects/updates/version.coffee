(doc, req) ->
  base64 = require 'lib/base64'
  return [null, 'no document found'] unless doc

  try
    {_rev} = JSON.parse req.body
  catch e
    _rev = null

  if _rev? and _rev isnt doc._rev
    return [null, 'update conflict']

  version = JSON.stringify doc, (k, v) ->
    unless k in ['_attachments', '_revisions'] then v else undefined

  doc._attachments ?= {}
  doc._attachments["rev-#{doc._rev}-#{(new Date).toJSON()}"] =
    content_type: 'application/json'
    data: base64.encode JSON.stringify version

  [doc, JSON.stringify doc._attachments]
