(doc) ->
  if doc.type? and doc.updated?
    emit [doc.type, doc.updated]
