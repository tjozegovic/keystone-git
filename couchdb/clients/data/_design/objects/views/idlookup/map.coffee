(doc) ->
  id = switch doc.type
    when 'group' then doc.name
    when 'sender' then doc.email
    when 'message' then doc.slug or doc.name or doc._id

  return unless id?
  emit "#{doc.type}:#{id}"
