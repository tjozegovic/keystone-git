(doc) ->
  if doc?.type is 'sender' and doc?.active
    emit doc.email
