(doc) ->
  if doc?.type is 'sender' and doc?.active
    emit doc.email
    emit doc.firstname + " " + doc.lastname
    emit doc.lastname + ", " + doc.firstname
    (doc.aliases or []).forEach (alias) ->
      if alias != (doc.firstname + " " + doc.lastname) and alias != (doc.lastname + ", " + doc.firstname)
        emit alias
