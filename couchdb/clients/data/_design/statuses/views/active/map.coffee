(doc) ->
  if doc?.type is 'status' and doc?.active
    emit doc.name
