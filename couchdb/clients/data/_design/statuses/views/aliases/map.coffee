(doc) ->
  if doc?.type is 'status' and doc?.active
    emit doc.name
    (doc.aliases or []).forEach (alias) ->
      return if alias is doc.name
      emit alias
