(doc) ->
  if doc?.type is 'email.delivered'
    dt = new Date doc.at
    emit [doc.status, dt.getFullYear(), dt.getMonth() + 1, dt.getDate()], null
    emit [doc.segment, dt.getFullYear(), dt.getMonth() + 1, dt.getDate()], null
