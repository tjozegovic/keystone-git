(doc) ->
  return if doc.type is 'cache'
  emit [doc.type, doc.at]
  emit [doc.type, 'dated', doc.at]

  if doc.status? and doc.segment? and doc.step?
    emit [doc.type, 'drips', doc.at]
    emit [doc.type, 'drips/status', doc.status, doc.at]
    emit [doc.type, 'drips/segment', doc.status, doc.segment, doc.at]
    emit [doc.type, 'drips/step', doc.status, doc.segment, doc.step, doc.at]

    if doc.first
      emit [doc.type + '/unique', 'drips', doc.at]
      emit [doc.type + '/unique', 'drips/status', doc.status, doc.at]
      emit [doc.type + '/unique', 'drips/segment', doc.status, doc.segment, doc.at]
      emit [doc.type + '/unique', 'drips/step', doc.status, doc.segment, doc.step, doc.at]
  else if batch = doc.batch?._id or doc.batch
    emit [doc.type, 'batches', doc.at]
    emit [doc.type, 'batches/batch', batch, doc.at]

    if doc.first
      emit [doc.type + '/unique', 'batches', doc.at]
      emit [doc.type + '/unique', 'batches/batch', batch, doc.at]
  else if message = doc.message
    emit [doc.type, 'direct', doc.at]
    emit [doc.type, 'direct/message', message, doc.at]

    if doc.first
      emit [doc.type + '/unique', 'direct', doc.at]
      emit [doc.type + '/unique', 'direct/message', message, doc.at]
