(doc) ->
  if doc.type in ['api.errored', 'email.errored']
    emit [doc.at, doc.type]
