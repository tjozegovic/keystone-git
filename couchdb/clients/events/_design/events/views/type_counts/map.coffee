(doc) ->
  dt = new Date doc.at
  emit [doc.type, dt.getFullYear(), dt.getMonth() + 1, dt.getDate()], null
