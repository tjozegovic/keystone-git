(doc) ->
  return unless doc.type is 'form.completed'
  emit doc.at
