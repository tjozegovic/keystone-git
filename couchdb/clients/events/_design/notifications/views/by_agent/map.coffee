(doc) ->
  return unless doc?.type in ['notification.sent', 'vunotification.queued']
  emit [doc.agent or doc.to, doc.email], doc.at
