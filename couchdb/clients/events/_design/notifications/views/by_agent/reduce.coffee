(keys, values, rereduce) ->
  if rereduce
    total = values.map (summary) ->
      summary.total
    .reduce (a, b) ->
      a + b

    last = values.map (summary) ->
      summary.last
    .sort().reverse()[0]

    return total: total, last: last

  total: values.length
  last: values.sort().reverse()[0]
