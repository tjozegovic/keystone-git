(doc) ->
  at = new Date doc.at
  return unless ~doc.type.indexOf('email.') and doc.batch

  value = type: doc.type, first: !!doc.first
  date = [at.getFullYear(), at.getMonth() + 1, at.getDate()]
  emit ['batches'].concat(date).concat([doc.batch]), value
  emit ['perbatch', doc.batch].concat(date), value
