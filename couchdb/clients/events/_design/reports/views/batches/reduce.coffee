(keys, values, rereduce) ->
  if rereduce
    summary = values.shift()

    values.forEach (val) ->
      summary.sends = summary.sends + val.sends
      summary.delivers = summary.delivers + val.delivers
      summary.opens = summary.opens + val.opens
      summary.firstopens = summary.firstopens + val.firstopens
      summary.clicks = summary.clicks + val.clicks
      summary.firstclicks = summary.firstclicks + val.firstclicks

    return summary

  summary =
    sends: 0
    delivers: 0
    opens: 0
    firstopens: 0
    clicks: 0
    firstclicks: 0

  keys.forEach (key, idx) ->
    {type, first} = values[idx]
    switch type
      when 'email.queued' then summary.sends++
      when 'email.delivered' then summary.delivers++
      when 'email.opened' then summary.opens++
      when 'email.clicked' then summary.clicks++

    return unless first

    switch type
      when 'email.opened' then summary.firstopens++
      when 'email.clicked' then summary.firstclicks++

  summary
