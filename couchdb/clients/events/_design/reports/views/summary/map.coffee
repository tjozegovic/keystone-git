(doc) ->
  types = ['email.clicked', 'email.delivered', 'email.errored', 'email.opened', 'email.queued', 'optout']
  return unless not doc.sample and doc.type in types and doc.status and doc.segment and doc.step

  value = type: doc.type, first: !!doc.first

  at = new Date doc.at
  date = [at.getFullYear(), at.getMonth() + 1, at.getDate()]

  emit ['statuses'].concat(date).concat([doc.status, doc.segment, doc.step]), value
  emit ['segments', doc.status].concat(date).concat([doc.segment, doc.step]), value
  emit ['steps', doc.status, doc.segment].concat(date).concat([doc.step]), value
