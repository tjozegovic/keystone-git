->
  lookup = summary = null
  days = []
  dt = null

  while row = getRow()
    dt ?= row.key.join ''

    if dt isnt row.key.join ''
      summary.senders.sort()
      days.push key: dt, value: summary
      lookup = summary = null
      dt = row.key.join ''

    lookup ?= {}
    summary ?= senders: [], delivers: 0, clicks: 0

    {sender, delivers, clicks} = row.value
    unless lookup.hasOwnProperty sender
      lookup[sender] = yes
      summary.senders.push sender

    summary.delivers += delivers
    summary.clicks += clicks

  return JSON.stringify days
