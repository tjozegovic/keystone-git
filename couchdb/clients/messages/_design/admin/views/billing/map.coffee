(doc) ->
  return unless not doc.sample and (doc.sent or doc.delivered)

  summary =
    delivers: if doc.sent or doc.delivered then 1 else 0
    clicks: if doc.clicked then 1 else 0
    sender: doc.agent.email or doc.sender.email or doc.mail.from

  at = new Date doc.sent or doc.delivered
  emit [at.getFullYear(), at.getMonth() + 1, at.getDate()], summary
