(doc) ->
  return unless not doc.sample and doc.type in ['batch', 'drip']
  emit [doc.queued, doc.type]
