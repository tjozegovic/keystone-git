(doc) ->
  return unless not doc.sample
  return unless doc.type is 'batch' or doc.batch

  summary =
    sends: 1
    delivers: if doc.sent or doc.delivered then 1 else 0
    optouts: if doc.optout then 1 else 0
    bounces: if doc.bounced then 1 else 0
    opens: (doc.opened or []).length
    unqopens: if doc.opened then 1 else 0
    openrate: if doc.delivered and doc.opened then 1 else 0
    clicks: (doc.clicked or []).length
    unqclicks: if doc.clicked then 1 else 0
    clickrate: if doc.delivered and doc.clicked then 1 else 0

  at = new Date doc.queued
  date = [at.getFullYear(), at.getMonth() + 1, at.getDate()]
  emit ['all'].concat(date).concat(doc.batch._id), summary
  emit ['batch', doc.batch._id].concat(date), summary
