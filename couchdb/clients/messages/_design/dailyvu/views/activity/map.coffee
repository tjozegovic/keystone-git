(doc) ->
  return unless not doc.sample

  # TODO originally written to look at all activity instead of just clicks
  # events = ['clicked', 'opened', 'optout', 'bounced'].reduce (list, prop) ->
  #   list.concat doc[prop] or []
  # , []
  # return unless events.length
  #
  # events.sort (a, b) ->
  #   if a > b then -1 else if b > a then 1 else 0

  clicks = doc.clicked or []
  return unless clicks.length

  last = clicks[clicks.length - 1]
  emit [doc.agent._id, (last.at or last).toJSON()]
