(doc) ->
  return unless doc.type is 'drip' or (doc.status?._id and doc.segment?.id and doc.step?.id)

  date = doc.queued
  emit date
  emit ['status', doc.status._id, date]
  emit ['segment', doc.status._id, doc.segment.id, date]
  emit ['step', doc.status._id, doc.segment.id, doc.step.id, date]
