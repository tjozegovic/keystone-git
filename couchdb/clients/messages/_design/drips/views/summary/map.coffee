(doc) ->
  return unless not doc.sample
  return unless doc.type is 'drip' or doc.status?._id and doc.segment?.id and doc.step?.id

  summary =
    sends: 1
    delivers: if doc.sent or doc.delivered then 1 else 0
    optouts: if doc.optout then 1 else 0
    bounces: if doc.bounced then 1 else 0
    opens: (doc.opened or []).length
    unqopens: if doc.opened then 1 else 0
    openrate: if doc.delivered and doc.opened then 1 else 0
    clicks: (doc.clicked or []).length
    unqclicks: if doc.clicked then 1 else 0
    clickrate: if doc.delivered and doc.clicked then 1 else 0

  at = new Date doc.queued
  date = [at.getFullYear(), at.getMonth() + 1, at.getDate()]
  emit ['statuses'].concat(date).concat([doc.status._id, doc.segment.id, doc.step.id]), summary
  emit ['segments', doc.status._id].concat(date).concat([doc.segment.id, doc.step.id]), summary
  emit ['steps', doc.status._id, doc.segment.id].concat(date).concat([doc.step.id]), summary

  (doc.lead?.$attributes or []).forEach (attr) ->
    [group, value] = attr.split '/'
    emit ['groups'].concat(date).concat([group, value]), summary
    emit ['attributes', group].concat(date).concat([value]), summary
