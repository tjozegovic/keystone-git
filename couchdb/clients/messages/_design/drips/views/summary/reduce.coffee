(keys, values) ->
  summary = values.shift()

  values.forEach (val) ->
    summary.sends = summary.sends + val.sends
    summary.delivers = summary.delivers + val.delivers
    summary.optouts = summary.optouts + val.optouts
    summary.opens = summary.opens + val.opens
    summary.bounces = summary.bounces + val.bounces
    summary.unqopens = summary.unqopens + val.unqopens
    summary.openrate = if summary.delivers then summary.unqopens / summary.delivers else 0
    summary.clicks = summary.clicks + val.clicks
    summary.unqclicks = summary.unqclicks + val.unqclicks
    summary.clickrate = if summary.delivers then summary.unqclicks / summary.delivers else 0

  return summary
