(doc) ->
  isdrip =
    doc.type is 'drip' or (doc.status?._id and doc.segment?.id and doc.step?.id)
  isbatch =
    doc.type is 'batch' or doc.batch
  isdirect =
    doc.type is 'direct'
  return unless isdrip or isbatch or isdirect

  emit timestamp = doc.queued
  emit ['!sample', timestamp] unless doc.sample

  if isdrip
    emit ['drips', timestamp]
    emit ['drips/status', doc.status._id, timestamp]
    emit ['drips/segment', doc.status._id, doc.segment.id, timestamp]
    emit ['drips/step', doc.status._id, doc.segment.id, doc.step.id, timestamp]

    unless doc.sample
      emit ['!sample', 'drips', timestamp]
      emit ['!sample', 'drips/status', doc.status._id, timestamp]
      emit ['!sample', 'drips/segment', doc.status._id, doc.segment.id, timestamp]
      emit ['!sample', 'drips/step', doc.status._id, doc.segment.id, doc.step.id, timestamp]
  else if isbatch
    emit ['batches', timestamp]
    emit ['batches/batch', doc.batch._id or doc.batch, timestamp]

    unless doc.sample
      emit ['!sample', 'batches', timestamp]
      emit ['!sample', 'batches/batch', doc.batch._id or doc.batch, timestamp]
  else if isdirect
    emit ['direct', timestamp]
    emit ['direct/message', doc.message, timestamp]

    unless doc.sample
      emit ['!sample', 'direct', timestamp]
      emit ['!sample', 'direct/message', doc.message, timestamp]
