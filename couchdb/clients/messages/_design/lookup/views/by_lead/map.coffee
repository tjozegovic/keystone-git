(doc) ->
  return unless doc.recipient?
  emit [doc.lead._id, doc.delivered or doc.queued]