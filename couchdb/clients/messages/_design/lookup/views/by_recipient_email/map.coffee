(doc) ->
  return unless doc.recipient?
  emit [doc.recipient.email, doc.delivered or doc.queued]
