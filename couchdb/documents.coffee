fs = require 'fs'
path = require 'path'

module.exports =
  parse: parse = (dir, obj = {}, top = yes) ->
    dir = path.join dir, '_design' if top
    return obj unless fs.existsSync dir

    files = fs.readdirSync(dir).map (leaf) ->
      fullpath = path.join dir, leaf
      name: leaf, path: fullpath, stat: fs.statSync fullpath

    files.forEach ({name, path, stat}) ->
      if stat.isDirectory()
        doc = obj[name] = {}
        if top
          doc._id = "_design/#{name}"
          doc.language = 'coffeescript'
        parse path, doc, false
      else if stat.isFile() and name[0] isnt '.' and (~name.indexOf('.js') or ~name.indexOf('.coffee'))
        obj[name[...name.lastIndexOf('.')]] = fs.readFileSync path, 'ascii'
      else
        console.log "ignoring path #{path}"

    obj
