async = require 'async'
crypto = require 'crypto'
fs = require 'fs'
moment = require 'moment'
nano = require 'nano'
path = require 'path'
Q = require 'Q'
util = require 'util'

documents = require './documents'

hash = (data) ->
  md5 = crypto.createHash 'md5'
  md5.update JSON.stringify data
  md5.digest 'hex'

class CouchPromises
  constructor: (@url) ->
    @db = nano @url

  copy: (from, to) ->
    {config} = @db
    Q.promise (resolve, reject) =>
      nano(config.url).request
        method: 'COPY'
        db: config.db
        doc: from
        headers: Destination: to
      , (err) ->
        return reject err if err
        resolve()

  promise: (method, args...) ->
    Q.promise (resolve, reject) =>
      @db[method] args..., (err, body, headers) ->
        return reject err if err
        resolve body: body, headers: headers

  get: (args...) ->
    @promise 'get', args...

  view: (args...) ->
    @promise 'view', args...

  insert: (doc) ->
    @promise 'head', doc._id
    .then ({headers}) =>
      doc._rev = headers?.etag[1...-1]
      @promise 'insert', doc
    .catch (err) =>
      @promise 'insert', doc

  destroy: (id) ->
    @promise 'head', id
    .then ({headers}) =>
      @promise 'destroy', id, headers.etag[1...-1]

module.exports =
  update: (database_url, object_folder, done) ->
    dir = path.join __dirname, object_folder
    return unless fs.existsSync dir

    db = new CouchPromises database_url
    update = (doc, done) ->
      db.get doc._id
      .then ({body: current}) ->
        _rev = current._rev
        delete current._rev
        return done() unless hash(doc) isnt hash(current)
        util.log "updating design document #{doc._id} to #{database_url}"
        db.insert doc
      , (err) ->
        return done err unless err?.message in ['missing', 'deleted']

        util.log "adding design document #{doc._id} to #{database_url}"
        db.insert doc
      .then ->
        done null
      , done

    async.each (doc for name, doc of documents.parse dir), update, done

  upgrade: upgrade = (database_url, object_folder, done) ->
    dir = path.join __dirname, object_folder
    return unless fs.existsSync dir

    db = new CouchPromises database_url
    update = (doc, done) ->
      db.get doc._id
      .then ({body: current}) ->
        _rev = current._rev
        delete current._rev
        return done() unless hash(doc) isnt hash(current)

        util.log "upgrading design document #{doc._id} to #{database_url}"
        db.copy doc._id, "#{doc._id}-#{moment.utc().toJSON()}"
        .then ->
          doc._id += '-new'
          db.insert doc
        .then ->
          design = doc._id['_design/'.length ..]
          name = Object.keys(doc.views)[0]
          util.log "triggering view _design/#{design}/#{name} rebuild"
          db.view design, name, limit: 0
        .then ->
          dest = "#{current._id}?rev=#{_rev}"
          util.log "restoring view #{dest}"
          db.copy doc._id, dest
        .then ->
          util.log "destroying view #{doc._id}"
          db.destroy doc._id
      , (err) ->
        return done err unless err?.message in ['missing', 'deleted']

        util.log "adding design document #{doc._id} to #{database_url}"
        db.insert doc
      .then ->
        done null
      , done

    async.each (doc for name, doc of documents.parse dir), update, done

if require.main is module
  [url, folder] = process.argv[2..]
  util.log "updating #{url} with objects from #{folder}"
  upgrade url, folder, console.log
