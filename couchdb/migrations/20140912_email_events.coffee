_ = require 'lodash'
async = require 'async'

module.exports =
  dbs: ['events', 'messages']
  run: (migration, done) ->
    {events, messages} = migration.dbs
    events.list include_docs: yes, (err, body) ->
      return console.error err if err
      async.filter body.rows
      , (row, cb) ->
        cb 'email' of row.doc
      , (results) ->
        async.map results
        , (row, cb) ->
          return cb null unless row.doc.email
          cb null, key: row.doc.email, value: row.doc
        , (err, results) ->
          grouped = {}
          results.forEach (item) ->
            grouped[item.key] ?= []
            grouped[item.key].push item.value

          async.each Object.keys(grouped)
          , (email, cb) ->
            messages.get email, (err, message) ->
              if err
                return cb if err.message is 'missing' then null else err

              async.eachSeries grouped[email]
              , (event, cb) ->
                return cb null if event.batch or (event.status and event.segment and event.step)

                delete event.message if 'message' of event
                if message.status and message.segment and message.step
                  _.assign event,
                    status: message.status._id
                    segment: message.segment.id
                    step: message.step.id
                else if message.batch
                  _.assign event,
                    batch: message.batch

                if event.type is 'email.opened'
                  event.first = yes unless message.opened
                  message.opened = (message.opened or []).concat [event.at]
                if event.type is 'email.clicked'
                  event.first = yes unless message.clicked
                  message.clicked = (message.clicked or []).concat [event.at]

                events.insert event, cb
              , (err) ->
                console.error err if err
                messages.insert message, cb
          , (err) ->
            return done err if err
            done null, "email events: #{Object.keys(grouped).length}"
