(doc) ->
  if doc?.status is 'pending'
    emit [doc.data.client, doc.at], null
