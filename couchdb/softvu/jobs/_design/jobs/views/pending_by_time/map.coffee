(doc) ->
  if doc?.status is 'pending'
    emit doc.at, null
