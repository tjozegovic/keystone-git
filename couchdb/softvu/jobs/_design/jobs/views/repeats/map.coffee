(doc) ->
  return unless doc.repeat?
  emit doc.at, repeat: doc.repeat, updated: doc.updated
