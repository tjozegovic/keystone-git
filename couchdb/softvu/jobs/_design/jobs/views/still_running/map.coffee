(doc) ->
  if doc?.status is 'running'
    emit doc.updated
