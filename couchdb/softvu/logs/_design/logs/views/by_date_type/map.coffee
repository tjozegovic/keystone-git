(doc) ->
  if doc.source?
    emit [doc.at, "source:#{doc.source}"]
  else if doc.target?
    emit [doc.at, "target:#{doc.target}"]
