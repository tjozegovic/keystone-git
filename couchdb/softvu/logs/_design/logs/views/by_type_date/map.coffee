(doc) ->
  if doc.source?
    emit ["source:#{doc.source}", doc.at]
  else if doc.target?
    emit ["target:#{doc.target}", doc.at]
