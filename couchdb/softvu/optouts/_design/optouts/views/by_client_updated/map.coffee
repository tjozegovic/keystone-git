(doc) ->
  if doc?.type is 'optout'
    for client in doc.clients or []
      for history in doc.history
        continue unless history.client is client and history.type is 'optout'
        emit [client, history.at]
        break
