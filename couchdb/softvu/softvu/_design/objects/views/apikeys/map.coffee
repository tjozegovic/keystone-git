(doc) ->
  if doc?.type and doc.type is 'apikey'
    emit [doc.client, doc._id], null
