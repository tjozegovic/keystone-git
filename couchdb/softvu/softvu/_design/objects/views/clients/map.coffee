(doc) ->
  if doc?.type and doc.type is 'client'
    val = short: doc.short, name: doc.name
    emit ['all', doc._id], val
    emit [(if doc.archive then 'inactive' else 'active'), doc._id], val
