(doc) ->
  if doc?.type is 'client' and doc.cnames?
    for name in doc.cnames
      emit name, doc._id
