(doc) ->
  if doc?.type is 'user' and doc.token
    emit doc.token
