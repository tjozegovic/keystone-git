_ = require 'lodash'
uuid = require 'uuid'

module.exports =
  '#.batch.#':
    opts: durable: true
    call: (ctx, event, done) ->
      return done null unless _.any ['finished', 'failed'], (type) -> ~event.type.indexOf type

      batches = ctx.use 'client/batches'
      batches.get event.batch, (err, batch) ->
        return done err if err

        delete batch.$job
        batch.status = event.type.substring event.type.lastIndexOf('.') + 1
        batch.updated = new Date
        batches.save batch, done

  '#.batch.deployed':
    opts: durable: true
    call: (ctx, event, done) ->
      [batches, jobs] = ctx.use 'client/batches', 'core/jobs'
      batches.get event.batch, (err, batch) ->
        return done err if err
        send = if batch.schedule.now then new Date else batch.schedule.when

        jobs.save
          at: send
          status: 'pending'
          action: 'batch'
          data: client: event.client, batch: event.batch
        , (err, body) ->
          batch.$job = body.id
          batch.status = 'deployed'
          batch.deployed = batch.updated = new Date
          batches.save batch, done

  '#.batch.canceled':
    opts: durable: true
    call: (ctx, event, done) ->
      [batches, jobs] = ctx.use 'client/batches', 'core/jobs'
      batches.get event.batch, (err, batch) ->
        jobs.get batch.$job, (err, _job) ->
          job = _job.doc
          update = ->
            delete batch.deployed
            batch.status = 'pending'
            batch.updated = new Date
            batches.save batch, done

          return update() unless job

          job.status = 'canceled'
          job.updated = new Date
          jobs.save job, (err) ->
            return done err if err
            update()
