_ = require 'lodash'
async = require 'async'

module.exports =
  '#.form.completed':
    opts: durable: true
    call: (ctx, event, done) ->
      return done() unless event.page and event.email

      [pages, emails, leads] = ctx.use 'client/pages', 'client/emails', 'client/leads'
      async.auto
        page: (cb) -> pages.get event.page, cb
        email: (cb) -> emails.get event.email, cb
        lead: ['email', (cb, results) ->
          return cb null, null unless lead = results.email.lead?.id
          leads.get lead, cb
        ]
      , (err, {lead, email, page}) ->
        return done err if err
        return done() unless page.response?

        page.response.recipients ?= []
        if page.response.includeSender
          page.response.recipients.push if lead?.agent? and lead.agent.trim().length isnt 0 then lead.agent
          else email.sender.email

        email = _.assign event,
          type: 'form.response'
          mail: to: _.uniq page.response.recipients
          response: event.body.form
        ctx.publish 'email', email, done
