_ = require 'lodash'

module.exports =
  '#.leads.import.uploaded':
    opts: durable: yes
    call: (ctx, event, done) ->
      jobs = ctx.use 'core/jobs'
      uploads = ctx.use 'client/uploads'

      uploads.get event.import, (err, _import) ->
        return done err if err

        jobs.save
          status: 'pending'
          action: 'parse'
          data: _.pick event, 'client', 'import', 'import_type'
        , (err, body) ->
          return done err if err

          _import.$job = body.id
          _import.status = 'scheduled'
          _import.updated = new Date()
          uploads.save _import, done

  '#.optout.import.uploaded':
    opts: durable: yes
    call: (ctx, event, done) ->
      jobs = ctx.use 'core/jobs'
      uploads = ctx.use 'client/uploads'

      uploads.get event.import, (err, _import) ->
        return done err if err

        jobs.save
          status: 'pending'
          action: 'parse'
          data: _.pick event, 'client', 'import', 'import_type', 'user'
        , (err, body) ->
          return done err if err

          _import.$job = body.id
          _import.status = 'scheduled'
          _import.updated = new Date()
          uploads.save _import, done

  '#.import.parsed':
    opts: durable: yes
    call: (ctx, event, done) ->
      jobs = ctx.use 'core/jobs'
      uploads = ctx.use 'client/uploads'

      uploads.get event.import, (err, _import) ->
        return done err if err

        jobs.save
          status: 'pending'
          action: 'import'
          data: _.pick event, 'client', 'import', 'import_type', 'import_table'
        , (err, body) ->
          _import.$import_job = body.id
          _import.status = 'parsing'
          _import.updated = new Date()
          uploads.save _import, done

  '#.import.finished':
    opts: durable: yes
    call: (ctx, event, done) ->
      uploads = ctx.use 'client/uploads'

      uploads.get event.import, (err, _import) ->
        return done err if err

        _import.status = 'finished'
        _import.imported = event.stats.imported
        _import.errors = event.stats.errors
        _import.updated = _import.finished = new Date()
        _import.errorId = event.errorId if event.errorId?
        uploads.save _import, done
