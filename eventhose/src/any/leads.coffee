_ = require 'lodash'
async = require 'async'

S = require 'shared/util/string'
attributemapper = require 'shared/attributemapper'
segmenter = require 'shared/segmenter'

module.exports = do ->
  _segment = (ctx, event, client, lead, status, attributes, done) ->
    unless status?
      return done new Error "Status '#{lead.status}' is invalid."
    else unless status.segments?
      msg = "Invalid status '#{status.name}', status has no segments. Lead could not be segmented."
      return done new Error msg

    try
      path = attributemapper.mapPath lead.attributes, attributes
      segment = segmenter.segment path, status.segments
    catch e
      return done e

    # only update the segmented date if it doesn't exist on the lead already
    defaults = segmented: event.at
    props = segment: segment.id, updated: new Date, $attributes: path

    _leadupdate ctx, client, lead.id, props, defaults, (err) ->
      return done err if err?

      ctx.events.emit 'lead.segmented', client,
        lead: lead.id
        status: lead.$status
        segment: segment.id
        attributes: path
        initial: 'segmented' not of lead
      , done

  _leadupdate = (ctx, client, lead, props, defaults, done) ->
    [done, defaults] = [defaults, {}] unless done

    leads = ctx.use 'client/leads'
    leads.get lead, (err, body) ->
      return done err if err
      return done new Error "Invalid lead #{lead}" unless body

      _.defaults body, defaults
      _.assign body, props, updated: new Date()
      leads.save body, done

  '#.lead.created':
    opts: durable: true
    call: (ctx, event, done) ->
      {client, lead} = event
      return done new Error 'Missing client and/or lead' unless client and lead

      [attributes, leads, statuses] = ctx.use 'client/attributes', 'client/leads', 'client/statuses'
      async.auto
        lead: (cb) -> leads.get lead, cb
        attributes: (cb) -> attributes.all cb

        # TODO fix $status or status when most? leads are converted
        status: ['lead', (cb, {lead}) -> statuses.get lead.$status or lead.status, cb]
      , (err, results) ->
        return done err if err

        {lead, status, attributes} = results
        _segment ctx, event, client, lead, status, attributes, done

  '#.lead.status.changed':
    opts: durable: true
    call: (ctx, event, done) ->
      {client, lead, status} = event
      return done new Error 'Missing client and/or status' unless client and status

      [attributes, leads, statuses] = ctx.use 'client/attributes', 'client/leads', 'client/statuses'
      async.parallel
        lead: (cb) -> leads.get lead, cb
        status: (cb) -> statuses.get status, cb
        attributes: (cb) -> attributes.all cb
      , (err, results) ->
        return done err if err

        {lead, status, attributes} = results

        unless lead.recipient.override?.segmented? and S(lead.recipient.override.segmented).toBoolean()
          # since the status has changed, the day the lead was segmented no longer matters
          delete lead.segmented

        unless status?
          # TODO better handling here...
          ctx.logger.warn "could not find status #{event.status}, skipping resegmenting"

          delete lead.$status
          lead.updated = new Date()
          return leads.save lead, done

        if lead.$job?
          jobs = ctx.use 'core/jobs'
          jobs.get lead.$job, (err, job) ->
            # TODO error handling...
            job?.canceled (err) ->
              if err?
                ctx.logger.error 'could not cancel job', err: err
              else
                ctx.events.emit 'drip.canceled', client, job.data

          # remove the current $job, then async cancel it
          delete lead.$job

        lead.$status = status.id
        lead.updated = new Date()
        leads.save lead, (err) ->
          return done err if err
          _segment ctx, event, client, lead, status, attributes, done

  '#.lead.agent.changed':
    opts: durable: true
    call: (ctx, event, done) ->
      {client, lead, agent} = event
      unless client and lead and typeof agent isnt 'undefined'
        return done new Error 'Missing client, lead, and/or agent.'

      _leadupdate ctx, client, lead, agent: agent, done

  '#.drip.scheduled':
    opts: durable: true
    call: (ctx, event, done) ->
      {client, lead, step, jobid} = event
      return done new Error 'Missing client, lead, step, and/or job.' unless client and lead and step and jobid
      _leadupdate ctx, client, lead, step: step, '$job': jobid, done

  '#.optout.reconciliation':
    opts: durable: true
    call: (ctx, event, done) ->
      {optout, client, from} = event
      optouts = ctx.use 'core/optouts'
      data = from: from
      optouts.optout optout, client, data, (err) ->
        return done err if err
