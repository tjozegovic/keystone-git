_ = require 'lodash'

module.exports =
  '#':
    opts: durable: true
    call: (ctx, event, done) ->
      return done() if ~event.type.indexOf '.heartbeat'

      events = ctx.use if event.client then 'client/events' else 'core/events'
      save = (cb) -> events.save event, cb

      types = [
        'email.clicked'
        'email.delivered'
        'email.bounced'
        'email.spamreport'
        'email.opened'
        'optout'
      ]

      return save done unless event.client and event.email and event.type in types

      emails = ctx.use 'client/emails'
      emails.get event.email, onlymeta: yes, (err, message) ->
        return save done if err

        _.assign event,
          status: message.status?._id or message.status?.id
          segment: message.segment?.id
          step: message.step?.id
          batch: message.batch?._id or message.batch?.id
          message: message.message
          queued: message.queued
          sample: message.sample

        switch event.type
          when 'email.opened' then event.first = yes unless message.opened
          when 'email.clicked' then event.first = yes unless message.clicked
          when 'optout' then event.first = yes unless message.optout

        save (err, body) ->
          return done err if err

          event_info = at: event.at, event: body.id
          event_info.link = event.link if event.type is 'email.clicked'

          switch event.type
            # TODO should this be delivered, dropping sent...
            # {{sent}} is used in the vunotifications
            when 'email.delivered' then message.sent = message.delivered = event.at # event_info

            when 'email.bounced' then message.bounced = event.at # event_info
            when 'email.spamreport' then message.spamreport = event.at # event_info
            when 'email.opened' then (message.opened ?= []).push event_info
            when 'email.clicked' then (message.clicked ?= []).push event_info
            when 'optout' then (message.optout ?= []).push event_info

          emails.save message, done
