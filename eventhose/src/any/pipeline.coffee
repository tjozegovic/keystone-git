_ = require 'lodash'
async = require 'async'
knox = require 'knox'

upload_file = (content_type, suffix) ->
  (client, client_id, email_id, val, cb) ->
    return cb null, null unless val?

    req = client.put "/emails/#{client_id}/#{email_id}.#{suffix}",
      'Content-Length': Buffer.byteLength(val)
      , 'Content-Type': content_type
      , 'x-amz-acl': 'public-read'

    req.on 'response', (res) ->
      # TODO not a 200?
      cb null, req.url

    req.end val

upload_html = upload_file 'text/html', 'html'
upload_text = upload_file 'text/plain', 'txt'

module.exports =
  '#.pipeline.completed':
    opts: durable: true
    call: (ctx, event, done) ->
      return done() unless ctx.config.aws['upload html']

      credentials =
        key: ctx.config.aws.accessKeyId
        secret: ctx.config.aws.secretAccessKey
        bucket: 'sv-keystone'

      client = knox.createClient credentials
      emails = ctx.use 'client/emails'
      emails.get event.id, (err, email) ->
        return err if err?

        async.parallel [
          _.partial upload_html, client, emails.client, email.id, email.mail.html
          _.partial upload_text, client, emails.client, email.id, email.mail.text
        ], (err, results) ->
          # TODO what can go wrong here with the upload?
          return done err if err?

          email.mail.html = results[0]
          email.mail.text = results[1]
          emails.save email, done
