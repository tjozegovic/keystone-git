_ = require 'lodash'
async = require 'async'
moment = require 'moment-timezone'
uuid = require 'uuid'

send_segment_email = (ctx, email, from, done) ->
  id = uuid.v4()
  jobs = ctx.use 'core/jobs'
  {client, lead, segment, segmented, status, step} = email

  {delay, message} = step
  unless delay?
    # TODO make notes somewhere? admin hints?
    return done new Error "'#{step.name or step.id}' does not have a valid delay."
  else unless delay.units in ['day', 'hour']
    return done new Error "'#{delay.units}' is an invalid value. Units must be either 'day' or 'hour'."

  segmented.add parseInt(delay.value || 0, 10), delay.units + 's'

  data =
    client: client
    type: 'drip'
    lead: lead.id
    status: status
    segment: segment.id
    step: step.id
    message: message
    delay: delay
    email: lead.recipient.email
    from: from

  jobs.save
    at: segmented
    status: 'pending'
    action: 'email'
    data: data
  , (err, body) ->
    ctx.events.emit 'drip.scheduled', client, _.assign jobid: body.id, at: segmented, action: 'email', data
    done err

active_sorted_steps = (steps) ->
  # filter out any steps marked as active:no and sort by calculated delay
  _(steps)
    .filter (step) -> not step.active? or step.active
    .sortBy (step) ->
      # type is either days or hours, changes days to hours for sorting
      {value, units} = step.delay
      value * if /days?/.test(units) then 24 else 1
    .value()

find_step_by_segmented = (steps, segmented, now) ->
  # find the first step where the send moment is in the future
  _.find steps, ({delay}) ->
    stepsend = segmented.clone()
    stepsend.add parseInt(delay.value || 0, 10), delay.units + 's' if delay
    return 0 <= stepsend.diff now # future

module.exports =
  '#.lead.segmented':
    opts: durable: true
    call: (ctx, event, done) ->
      {client, lead, status, segment} = event
      return done new Error "Missing client and/or status." unless client? and status?

      [leads, statuses] = ctx.use 'client/leads', 'client/statuses'
      async.parallel
        client_info: ctx.get_client_info
        _lead: (cb) -> leads.get lead, cb
        _status: (cb) -> statuses.get status, cb
      , (err, results) ->
        return done err if err?
        {client_info, _status, _lead} = results

        return done new Error "Invalid status #{status}" unless _status?.segments

        _segment = _.find _status.segments, id: segment
        return done new Error "Invalid segment id #{segment}." unless _segment

        unless _segment.steps?.length
          ctx.logger.info 'Segment has no steps configured.', client: client, segment: segment
          return done null

        now = moment event.at
        segmented = if _lead.segmented? and not event.initial
          moment.tz _lead.segmented, client_info.timezone
        else
          now
        steps = active_sorted_steps(_segment.steps)
        step = find_step_by_segmented steps, segmented, now

        # TODO should this emit a lead.timedout?
        unless step?
          ctx.logger.info 'could not find a next step for a segmented lead', lead: lead
          return done()

        email = _.assign event,
          lead: _lead
          segment: _segment
          step: step
          segmented: segmented
        send_segment_email ctx, email, 'lead segmented', done

  '#.email.delivered':
    opts: durable: true
    call: (ctx, event, done) ->
      {client, email} = event
      return done new Error "Missing client and/or email." unless client? and email?

      [emails, leads, statuses] = ctx.use 'client/emails', 'client/leads', 'client/statuses'
      emails.get email, (err, email) ->
        return done err if err?
        return done null if email.sample or email.type isnt 'drip'

        unless email.lead?
          ctx.logger.debug 'Could not send another message, recipient was not a lead.'
          return done null

        async.auto
          client_info: ctx.get_client_info
          lead: (cb) -> leads.get email.lead.id or email.lead, cb
          status: ['lead', (cb, {lead}) -> statuses.get lead.$status or lead.status, cb]
        , (err, results) ->
          return done err if err
          {client_info, lead, status} = results

          unless lead.segmented?
            ctx.logger.warn 'Could not find next message, lead does not have a segmented date/time.'
            return done null

          segment = _.find status.segments, id: email.segment.id or email.segment
          steps = active_sorted_steps(segment.steps)

          unless segment?
            ctx.logger.debug 'Could not find segment in status.'
            return done null

          now = moment event.at
          segmented = moment.tz lead.segmented, client_info.timezone
          step = if email.triggered? then steps[_.findIndex(steps, id: email.step.id) + 1]
          else find_step_by_segmented steps, segmented, now

          unless step?
            ctx.events.emit 'lead.timedout', client,
              lead: lead.id
              status: lead.status
              segment: segment.id
            return done null

          email = _.assign event,
            lead: lead
            status: status.id
            segment: segment
            step: step
            segmented: segmented

          send_segment_email ctx, email, 'email delivered, next drip', done
