_ = require 'lodash'
async = require 'async'
moment = require 'moment'
uuid = require 'uuid'

module.exports =
  '#.status.updated':
    opts: durable: true
    call: (ctx, event, done) ->
      {client, status: id} = event
      [jobs, leads, statuses] = ctx.use 'core/jobs', 'client/leads', 'client/statuses'

      statuses.get id, (err, status) ->
        return done err if err
        return done new Error "Status #{id} does not exist." unless status

        async.each status.segments, (segment, cb) ->
          canceljob = (job, cbx) ->
            _.assign job,
              status: 'canceled'
              canceled: new Date
            jobs.save job, (err) ->
              return cb err if err
              ctx.events.emit 'drip.canceled', client, job.data
              cbx?()

          createjob = (send, data, cbx) ->
            jobs.save
              at: send.toDate()
              status: 'pending'
              action: 'email'
              data: data
            , (err, results) ->
              return cb err if err
              evt = _.assign jobid: results.id, at: send, action: 'email', data
              ctx.events.emit 'drip.scheduled', client, evt
              cbx?()

          return cb null unless segment.steps.length

          oldest = do ->
            # calculate the oldest possible message for this segment
            # used to queries all leads that are younger than this date
            {delay} = _.last segment.steps
            moment().subtract delay.value, delay.units

          leads.by_segment id, segment.id, oldest, (err, leads) ->
            return cb err if err

            jobs.fetch _.compact(_.pluck leads, '$job'), (err, body) ->
              return cb err if err

              job_lookup = _.indexBy body, 'id'
              async.eachSeries segment.steps, (step, cb) ->
                duration = moment.duration step.delay.value, step.delay.units
                async.filter leads, (lead, filter) ->
                  stepsend = moment(lead.segmented or lead.created).add duration
                  return filter yes if stepsend.isBefore moment()

                  if lead.$job
                    job = job_lookup[lead.$job]
                    scheduled = moment job.at
                    return filter no if 10 > Math.abs scheduled.diff stepsend, 'minutes', yes

                    canceljob job, -> createjob stepsend, job.data, -> filter no
                  else
                    createjob stepsend,
                      client: client
                      type: 'drip'
                      lead: lead.id
                      agent: lead.agent
                      status: status.id
                      segment: segment.id
                      step: step.id
                      message: step.message
                      delay: step.delay
                      email: lead.recipient.email
                      from: 'status update'
                    , -> filter no
                , (results) ->
                  leads = results
                  cb null
              , cb
        , done
