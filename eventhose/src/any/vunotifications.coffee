_ = require 'lodash'
hb = require 'handlebars'
moment = require 'moment-timezone'

module.exports =
  '#.email.clicked':
    opts: durable: true
    call: (ctx, event, done) ->
      # bad clicks are sometimes generated with wildly invalid values, not having a proper client is one
      return done() unless event.client

      repos = ['emails', 'events', 'leads', 'messages', 'statuses', 'senders']
      [emails, events, leads, messages, statuses, senders] = repos.map (x) -> ctx.use 'client/' + x
      {client, email, link, redirect, at} = event

      return done 'bad email id' unless email?
      ctx.use('core/clients').get client, (err, client) ->
        emails.get email, (err, email) ->
          return done err if err
          return done 'email not found' unless email?

          check = (to, cb) ->
            # TODO create hasnotification view for optimizing?
            events.notifications to, email.id, (err, notifications) ->
              return done err if err

              delay = client.notifications?.delay or 12
              return cb() unless delay > 0 and notifications?.length

              {at: last} = notifications[0]
              return cb() unless moment(last).add(delay, 'h').isAfter at

              ctx.events.emit 'notification.skipped', client.id,
                agent: to
                email: email.id
                delay: delay
                click: at
                last: last
                reason: "delay settings: delay was #{delay} hrs\n" +
                  "click was at #{at}\n" +
                  "last notification was at #{last}"
              return done()

          send = (to, tpldata) ->
            email = _.assign event,
              type: 'vunotification'
              mail: to: to
              link: redirect or link
            ctx.publish 'email', email, done

          checkAndSend = (sendTo, email) ->
            check sendTo, ->
              send sendTo, email

          return send email.sender.email, email if email.notification
          return done() if email.sample

          if email.type is 'direct'
            messages.get email.message.id, (err, message) ->
              return done() unless message?.notify
              return check email.sender.email, ->
                send email.sender.email, email
          else if email.type is 'batch'
            return done() unless email.batch.vunotification
            return check email.sender.email, ->
              send email.sender.email, email
          else
            unless email.status
              return check email.sender.email, ->
                send email.sender.email, email

            statuses.get email.status.id or email.status._id, (err, status) ->
              segment = _.find status.segments, id: email.segment.id
              step = _.find segment.steps, id: email.step.id

              unless step?.notify
                ctx.logger.log 'debug', 'skipped vunotification, message has notifications disabled'
                ctx.events.emit 'notification.skipped', client.id,
                  agent: email.sender.email
                  email: email.id
                  reason: 'message notifications disabled'
                return done()

              if email.lead?.id
                leads.get email.lead.id, (err, lead) ->
                  return done err if err?

                  if lead?.agent? and lead.agent.trim().length isnt 0
                    senders.by_name_or_alias lead.agent, (err, body) ->
                      return done err if err?

                      unless sender = body
                        ctx.logger.info "skipping vunotifiction, could not find sender #{lead.agent}"
                        return done()

                      checkAndSend(sender.email, email)
                  else
                    checkAndSend(email.sender.email, email)
              else
                checkAndSend(email.sender.email, email)
