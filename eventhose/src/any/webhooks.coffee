_ = require 'lodash'

module.exports =
  '#':
    opts: durable: true
    call: (ctx, event, done) ->
      return done() unless event.client? and event.type in [
        'email.queued'
        'email.errored'
        'email.delivered'
        'email.bounced'
        'email.deferred'
        'email.opened'
        'email.clicked'
        'email.reported'
        'email.optedin'
        'form.response'
        'lead.segmented'
        'lead.timedout'
        'batch.started'
        'batch.failed'
        'batch.finished'
        'optout'
        'optin'
      ]

      ctx.publish 'webhooks', event, done
