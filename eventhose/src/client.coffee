stdin = process.stdin
stdin.setEncoding 'utf8'

_     = require 'underscore'
util  = require 'util'
amqp  = require 'amqp'
conn  = amqp.createConnection { url: 'amqp://172.16.17.1' }

conn.on 'ready', ->
  opts =
    type: 'topic'
    durable: true
    autoDelete: false
    confirm: true

  conn.exchange 'events.hose', opts, (exchange) ->
    stdin.on 'data', (data) ->
      do process.exit if data and data.ctrl and data.name is 'c'

      data = data.toString().trim()
      console.log 'publishing: ' + data

      msg = {}
      msg[data] = 'nick.peeples@softvu.com'
      exchange.publish "#{data}.event", msg,
        mandatory: true

process.on 'exit', conn.end
