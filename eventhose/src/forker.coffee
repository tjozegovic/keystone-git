EventEmitter = require('events').EventEmitter

fs = require 'fs'
fork = require('child_process').fork

class Forker extends EventEmitter
  constructor: (@file) -> do @_fork

  _fork: (cb) =>
    @child = fork [@file]
    @child.once 'uncaughtException', @_crashed
    @child.once 'exit', @_crashed
    @emit 'start'
    do cb if cb?

  _crashed: (err) =>
    console.log err if err
    @emit 'crash'
    do @_fork

  send: (msg) => @child.send msg

  start: (cb) => @_fork cb

  stop: (cb) =>
    @timer = setTimeout (=> do @child.kill), 5000

    @child.removeListener 'exit', @_crashed
    @send action: 'shutdown'
    @child.once 'exit', =>
      clearTimeout @timer
      do cb

module.exports.Forker = Forker
