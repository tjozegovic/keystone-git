express = require 'express'
spawn = require('child_process').spawn

module.exports = app = express()
module.exports.hgroute = hgroute = (args) ->
  (req, res) ->
    hg = spawn 'hg', args.split ' '
    hg.stdout.on 'data', (data) -> res.write data
    hg.on 'close', -> do res.end

    res.writeHead 200, 'Content-Type': 'text/plain'
    res.write 'Running hg command...\n'

app.get '/tip', hgroute 'tip --template {date|isodate}\n{author}\n{desc}'
app.get '/version', hgroute 'id -i'
