util = require 'util'
spawn = require('child_process').spawn
fs = require 'fs'

forker = require './forker'
express = require 'express'
app = express()
hgapp = require './hg'

# test comment
restarting = false
child = new forker.Forker require.resolve __dirname + '/app'

app.get '/routes/:client', (req, res) ->
  path = "./clients/#{req.params.client}.js"
  if not fs.existsSync path
    res.send 404, '404 Not Found'
  else
    res.type 'text'
    res.send fs.readFileSync path

app.get '/update', (req, res) ->
  ok = ->
    util.log 'process updated'
    res.end '\nUpdated'
    restarting = false

  update = ->
    util.log 'checking for new changes...'
    hg = spawn 'hg', ['pull', '-u', '../node-test-repo']
    hg.stderr.on 'data', (data) -> util.log data
    hg.stdout.on 'data', (data) -> res.write data
    hg.on 'close', -> child.start ok

  if restarting
    res.send '200 Restarting...'
    return

  restarting = true
  res.writeHead 200, { 'Content-Type': 'text/plain' }
  res.write "#{new Date()}\n\nRestart...\n"

  child.stop update

# hgapp.route app
app.use require './hg'
app.listen 3000

do (fs = require 'fs', path = require 'path', name = '.lock') ->
  name = path.join __dirname, '..', name
  fs.openSync name, 'w'
  setInterval ->
    stat = fs.statSync name
    fs.utimesSync name, stat.atime, new Date()
  , 5000
  .unref()
