if ~process.cwd().indexOf 'eventhose'
  process.env.NODE_CONFIG_DIR ?= '../config'

config = require 'config'

domain = require 'domain'
{EventEmitter} = require 'events'
fs = require 'fs'
path = require 'path'
url = require 'url'
util = require 'util'

_ = require 'lodash'
async = require 'async'
request = require 'request'

{Database, RepositoryFactory} = require 'shared/database'
events = require 'shared/eventlib'
listen = require 'shared/queues/listen'

logger = do (fs = require 'fs', dir = __dirname + '/../log') ->
  fs.mkdirSync dir unless fs.existsSync dir
  winston = require 'winston'

  ->
    new winston.Logger
      transports: [
        new winston.transports.File _.assign
          filename: dir + '/service.log'
          json: no
          stripColors: yes
          tailable: yes
          timestamp: yes

        , if config.env is 'production'
            maxsize: 20 * 1024 * 1024
            maxFiles: 12
            level: 'info'
          else
            maxsize: 10 * 1024 * 1024
            maxFiles: 10
            level: 'debug'
        new winston.transports.Console
          colorize: yes
          timestamp: yes
          json: no
      ]

rootlogger = logger()
Database::LOGGER = rootlogger

module.exports = class Eventhose extends EventEmitter
  constructor: (@opts) ->
    @opts.modules ?= __dirname

    @metrics = last: null
    @bindings = []

    @once 'newListener', => process.nextTick => @connect()
    @on 'ready', => process.nextTick => @cleanup()

    setInterval ->
      events.emit 'eventhose.heartbeat', null, at: new Date, metrics: @metrics
    , 5000
    .unref()

  getConsumers: (dir, cb) ->
    {join} = path
    map = (path, last = '') ->
      try
        if fs.statSync(path).isDirectory()
          _.map fs.readdirSync(path), (name) -> map join(path, name), "#{last}#{name}"
        else
          name: last
          module: require path
      catch e
        rootlogger.warn 'mapping failed', path: path, name: last
        null

    dir = path.join @opts.modules, dir
    return [] unless fs.existsSync(dir)
    _.chain(map dir).flatten().compact().value()

  # TODO this is a little too much
  _wrapConsumer: (consume, info) =>
    {metrics} = this
    dbfactory = new RepositoryFactory config.db.url

    (json, done) ->
      {channel} = this
      ctx =
        config: _.cloneDeep config
        logger: logger()
        events: events
        publish: (exchange, key, msg, cb) ->
          if arguments.length is 3
            [cb, msg, key, exchange] = [msg, key, exchange, '']

          buf = new Buffer JSON.stringify msg
          channel.publish exchange, key, buf
          cb?()
        use: dbfactory.use.bind dbfactory

      if json.client?
        dbfactory.client = json.client

        # TODO needs caching
        # should follow the 'changes' stream from couchdb and update any cached clients
        ctx.get_client_info = (cb) ->
          clients = ctx.use 'core/clients'
          clients.get json.client, cb

      onerror = (from) ->
        (err) ->
          delete err.domain
          delete err.domainThrown
          rootlogger.log 'error', 'caught exception in event consumer', from: from, err: err, info: info, msg: json
          rootlogger.log 'error', err.stack if err.stack?

          # TODO log in events if client is set
          done true

      # TODO remove domain? can consumers be depended to handle their own errors?
      d = domain.create()
      d.on 'error', onerror 'domain'

      d.run -> process.nextTick ->
        metrics.last = new Date

        unless ~json.type?.indexOf '.heartbeat'
          rootlogger.debug 'consuming message', info: info, json: json

        consume ctx, json, (err) ->
          return done() unless err
          if typeof err is 'function'
            # TODO create and register saga!
            channel
            done()
          else
            onerror('consumer') err if err

  _bindAndSubscribe: (exchange, {name, module}, cb) ->
    async.each Object.keys(module), (route, cb) =>
      fn = module[route]
      unless typeof fn is 'function'
        {durable} = fn.opts or {}
        durable ?= yes
        fn = fn.call

      queue = "eventhose.queue:#{name}:#{route}"

      # TODO... ugh, this was a major oversight. maybe we should drop the .event suffix?
      # I don't really remember why I made that decision in the first place a long time ago
      route = route + '.event'

      @bindings.push info = exchange: exchange.name, queue: queue, route: route
      exchange.route route, queue, attempts: 3, durable: durable, @_wrapConsumer(fn, info), (listener) =>
        @emit 'subscribed', info

      cb null
    , cb

  onerror: (err) ->
    process.nextTick => @emit 'error', err
    rootlogger.log 'error', 'caught exception in connection', err: err

    setTimeout (=> @connect()), 1000

  connect: ->
    @emit 'connecting'
    connection = listen config.queues.url, limit: 1
    exchange = connection.exchange 'events.hose'
    binder = _.bind @_bindAndSubscribe, @, exchange

    consumers = [].concat @getConsumers('any'), @getConsumers('clients')
    async.each consumers, binder, (err) =>
      @emit 'error', err if err
      @emit 'ready' unless err

  cleanup: ->
    makeurl = (path) -> config.queues.api + path

    request makeurl('/queues'), (err, res, body) =>
      queues = JSON.parse body
      queues = _.filter _.pluck(queues, 'name'), (q) -> ~q.indexOf 'eventhose.queue:'
      exchanges = _.pluck @bindings, 'queue'

      _.difference(queues, exchanges).forEach (queue) ->
        request.del makeurl("/queues/%2f/#{encodeURIComponent queue}"), (err, res, body) ->
          @emit 'error', err if err

  close: (cb) -> =>
    @emit 'closing'
    if @connection then @connection.close().then cb else cb?()

eventhose = new Eventhose modules: __dirname
eventhose.on 'ready', -> rootlogger.info 'eventhose started'
eventhose.on 'connecting', -> rootlogger.info 'eventhose connecting'
eventhose.on 'subscribed', (info) -> rootlogger.info 'subscription setup', info

process.on 'SIGTERM', eventhose.close process.exit
process.on 'exit', -> rootlogger.info 'eventhose shutdown'

require('shared/utils/process').lockfile __dirname
