fs = require 'fs'

_ = require 'lodash'
async = require 'async'
gutil = require 'gulp-util'
pg = require 'pg'

module.exports = (url, done) ->
  base = url[0...url.lastIndexOf '/']
  pgclient = new pg.Client base + '/postgres'

  pgclient.connect (err) ->
    bail = (err) ->
      gutil.log err.message or err if err?
      pgclient.end()
      return done()

    return done err if err?

    drop_databases = (cb) ->
      pgclient.query "SELECT datname FROM pg_catalog.pg_database WHERE datname ~ 'keystone_test_.*'", (err, result) ->
        return bail err if err?

        async.each result.rows, (row, cb) ->
          pgclient.query "DROP DATABASE IF EXISTS #{row.datname}", cb
        , cb

    drop_roles = (cb) ->
      pgclient.query "SELECT rolname FROM pg_catalog.pg_roles WHERE rolname ~ 'keystone_test_.*'", (err, result) ->
        return bail err if err?

        async.each result.rows, (row, cb) ->
          pgclient.query "DROP ROLE IF EXISTS #{row.rolname}", cb
        , cb

    async.series [drop_databases, drop_roles], bail

  return
