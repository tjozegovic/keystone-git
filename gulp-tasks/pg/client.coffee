fs = require 'fs'
{join} = require 'path'

_ = require 'lodash'
pg = require 'pg'

module.exports = (url, done) ->
  id = 'bdede430b79211e48c85034b0c695ed1'

  pgclient = new pg.Client url
  pgclient.connect (err) ->
    bail = (err) ->
      pgclient.end()
      return done err

    return done err if err?

    pgclient.query 'SELECT id FROM clients WHERE id = $1', [id], (err, results) ->
      return bail err if err?
      return bail null if results.rows.length

      pgclient.query 'INSERT INTO clients (id, doc) VALUES ($1, $2)', [id, JSON.stringify(name: 'KSSB')], (err) ->
        return bail err if err?

        pgclient.query "SET search_path = client_#{id}", (err) ->
          return bail err if err?

          pgclient.query fs.readFileSync(join __dirname, 'client.sql').toString(), (err) ->
            bail err

  return
