fs = require 'fs'

_ = require 'lodash'
async = require 'async'
pg = require 'pg'

schema = require 'shared/database/schema'

module.exports = (url, name, done) ->
  base = url[0...url.lastIndexOf '/']
  pgclient = new pg.Client base + '/postgres'
  pgclient.connect (err) ->
    bail = (err) ->
      pgclient.end()
      return done err

    return done err if err?

    pgclient.query "SELECT datname FROM pg_catalog.pg_database WHERE datname = '#{name}'", (err, result) ->
      return bail err if err?

      setschema = ->
        pgclient.end()

        client = new pg.Client url
        client.connect (err) ->
          async.series [
            (cb) -> client.query "CREATE SCHEMA IF NOT EXISTS AUTHORIZATION keystone", cb
            (cb) -> client.query "SET search_path TO keystone", cb
            (cb) -> client.query schema.softvu, cb
          ], (err) ->
            client.end()
            done err

      return setschema() if result.rows.length

      pgclient.query "CREATE DATABASE #{name}", (err) ->
        return bail err if err

        # there seems to be an issue, if you do anything to a database too quickly, pg doesn't like it
        setTimeout setschema, 250

  return
