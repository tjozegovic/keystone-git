credential = require 'credential'
pg = require 'pg'

module.exports = (url, done) ->
  client = new pg.Client url
  client.connect (err) ->
    user = 'root@softvu.com'
    password = 'softvu'

    client.query "SELECT id FROM users WHERE username = 'root@softvu.com'", (err, results) ->
      if err?
        client.end()
        return done err

      if results.rows.length
        client.end()
        return done()

      credential.hash password, (err, hash) ->
        client.query "INSERT INTO users (username, doc) VALUES ($1, $2::jsonb)", [
          user
          JSON.stringify
            username: user
            password: hash
            isAdmin: yes
        ], (err) ->
          client.end()
          done err

  return
