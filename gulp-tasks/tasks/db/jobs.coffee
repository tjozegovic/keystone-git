_ = require 'lodash'
nano = require 'nano'

module.exports = (url, done) ->
  jobs = nano url + '/jobs'

  refresh_job =
    action: "db/view_refresh"
    repeat: "*/5 * * * *"
    status: "pending"

  jobs.view 'jobs', 'repeats', include_docs: yes, (err, body) ->
    return done err if err

    existing_job = _.find _.pluck(body.rows, 'doc'), action: refresh_job.action
    return done null if existing_job?.repeat is refresh_job.repeat

    existing_job = _.assign {}, existing_job, refresh_job
    jobs.insert existing_job, (err) ->
      console.error err if err
      done err

  # gulp doesn't like having stuff returned from nano..
  return
