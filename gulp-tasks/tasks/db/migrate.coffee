fs = require 'fs'
path = require 'path'

_ = require 'lodash'
async = require 'async'
gutil = require 'gulp-util'
nano = require 'nano'

MIGRATION_DIR = '../../../couchdb/migrations'

module.exports = (url) ->
  (done) ->
    async.parallel
      clients: (cb) ->
        server = nano url
        server.db.list (err, body) ->
          possible = _.filter body, (name) -> ~name.indexOf 'client-'
          cb err, _.uniq _.map possible, (name) -> name[name.indexOf('-') + 1...name.lastIndexOf('-')]
      files: (cb) ->
        console.log 'loading files', path.join(__dirname, MIGRATION_DIR)
        fs.readdir path.join(__dirname, MIGRATION_DIR), (err, files) ->
          cb err, _.map files, (name) -> path.basename name, '.coffee'
    , (err, results) ->
      return done err if err
      async.each results.files, (file, filedone) ->
        migration = require path.join MIGRATION_DIR, file
        async.each results.clients, (client, clientdone) ->
          dbs = _.reduce migration.dbs, (memo, name) ->
            memo[name] = nano "#{url}/client-#{client}-#{name}"
            memo
          , {}

          start = new Date
          async.every migration.dbs, (name, cb) ->
            db = nano "#{url}/client-#{client}-#{name}"
            db.get '_design/migrations', (err, body) ->
              return cb true if err?.message is 'missing' or err?.message is 'deleted'
              cb file not of body
          , (result) ->
            # what happens if some dbs have been migrated and others haven't? does it matter?
            if result
              console.time 'migration'
              gutil.log "running migration #{file} for client #{client}"
              migration.run dbs: dbs, (err, msg) ->
                console.timeEnd 'migration'
                async.each migration.dbs, (name, cb) ->
                  db = nano "#{url}/client-#{client}-#{name}"
                  db.get '_design/migrations', (err, body = {}) ->
                    body[file] =
                      start: start
                      end: new Date
                    db.insert body, '_design/migrations', cb
                , clientdone
            else
              gutil.log "skipping migration #{file} for client #{client}"
              clientdone null
        , filedone
      , done
