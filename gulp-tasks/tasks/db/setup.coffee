_ = require 'lodash'
async = require 'async'
nano = require 'nano'

module.exports = (url, done) ->
  server = nano url
  system = ['softvu', 'sessions', 'optouts', 'jobs', 'events', 'logs']

  server.db.list (err, body) ->
    return done err if err

    creating = _.difference system, body
    return done null unless creating.length

    async.each creating, (db, cb) ->
      console.log "Creating database #{url}/#{db}"
      server.db.create db, cb
    , done

  # gulp doesn't like having stuff returned from nano..
  return
