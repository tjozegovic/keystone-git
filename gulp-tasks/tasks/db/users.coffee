nano = require('nano')
credential = require('credential')

module.exports = (url, done) ->
  softvu = nano url + '/softvu'

  user = 'root@softvu.com'
  password = 'softvu'

  softvu.get user, (err, body) ->
    return done null unless err?.message in ['deleted', 'missing']

    credential.hash 'softvu', (err, hash) ->
      softvu.insert
        type: 'user'
        password:  hash
        isAdmin: yes
      , user, (err) ->
        console.error 'error creating default user', err if err
        done err

  # gulp doesn't like having stuff returned from nano..
  return
