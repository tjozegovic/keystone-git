async = require 'async'
nano = require 'nano'

couchdb = require '../../../couchdb'

doview = (what, url, done) ->
  server = nano url
  server.db.list (err, body) ->
    async.each body, check, done

  check = (database, cb) ->
    return cb null unless _isKeystoneDatabase database

    root = if ~database.indexOf 'client-' then 'clients' else 'softvu'
    type = database[database.lastIndexOf('-') + 1...]

    couchdb[what] "#{url}/#{database}", "#{root}/#{type}", (err) ->
      console.log err if err
      cb err

  _isKeystoneDatabase = (name) ->
    ~name.indexOf('client-') or name in ['events', 'jobs', 'optouts', 'sessions', 'softvu', 'logs']

  # gulp doesn't like having stuff returned from nano..
  return

module.exports = (url) ->
  upgrade: (cb) -> doview 'upgrade', url, cb
  update: (cb) -> doview 'update', url, cb
