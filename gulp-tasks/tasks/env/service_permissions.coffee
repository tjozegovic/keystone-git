{exec} = require 'child_process'
async = require 'async'

module.exports = (cb) ->
  async.eachSeries ['actionqueue', 'eventhose', 'pipeline'], (service, cb) ->
    exec "subinacl.exe /SERVICE keystone.#{service} /GRANT=\"IIS AppPool\\keystone\"=TOPI",
      cwd: __dirname
    , (err, stdout, stderr) ->
      setTimeout (-> cb err), 1000
  , (err) ->
    cb err
