{fork} = require 'child_process'
fs = require 'fs'
path = require 'path'

_ = require 'lodash'

gulp = require 'gulp'
$gulp = do require 'gulp-load-plugins'
combiner = require 'stream-combiner2'
es = require 'event-stream'
mainBowerFiles = require 'main-bower-files'

config = require 'config'

String::capitalize = ->
  str = if @.toString() is 'stop' then 'stopp' else @
  str.charAt(0).toUpperCase() + str.slice(1)

caffeine = ->
  cstream = combiner.obj [
    $gulp.coffeelint
      'indentation': level: 'ignore'
      'max_line_length': level: 'warn', value: 150
    $gulp.coffeelint.reporter()
    $gulp.coffee()
  ]

  cstream.on 'error', (err) ->
    # continue: the reporter will print out the errors
    @emit 'end'

  cstream

gulp.task 'config', ->
  console.log 'current configuration:'
  console.dir config

gulp.task 'src:build', ['version'], ->
  es.concat [
    gulp.src 'shared/**/*.coffee'
      .pipe $gulp.cached 'libs'
      .pipe caffeine()
      .pipe gulp.dest 'shared'

    # there are special .sql files that define different base schemas for the pg databases
    gulp.src 'shared/**/*.sql'

    gulp.src 'shared/dns_tool.js'
  ]
    .pipe gulp.dest 'node_modules/shared'

gulp.task 'src:watch', ['src:build'], ->
  gulp.watch ['shared/**/*.coffee', 'shared/**/*.sql', 'shared/dns_tool.js'], interval: 1007, debounceDelay: 4300, ['src:build']

runner = (script, task, cwd) ->
  child = undefined
  return ->
    child?.kill()
    child = fork script, null, cwd: cwd
    child.on 'error', (err) ->
      gulp.start task if task

['actionqueue', 'eventhose', 'pipeline', 'webhooks'].forEach (name) ->
  gulp.task "#{name}:build", ['src:build'], ->
    gulp.src "#{name}/src/**/*.coffee"
      .pipe $gulp.coffeelint
        'indentation': level: 'ignore'
        'max_line_length': level: 'warn', value: 120
      .pipe $gulp.coffeelint.reporter()

  gulp.task "#{name}:run", ["#{name}:build"], runner "#{name}/bootstrap.js", "#{name}:run", "#{name}"
  gulp.task "#{name}:watch", ["#{name}:run", 'src:watch'], ->
    gulp.watch ['shared/**/*.js', "#{name}/src/**/*.coffee"], interval: 1007, debounceDelay: 4200, ["#{name}:run"]

gulp.task 'api:run', runner 'api/index.coffee', 'api:run'
gulp.task 'api:watch', ['api:run', 'src:watch'], ->
  gulp.watch ['shared/**/*.js', 'api/index.coffee', 'api/{lib,v1}/**/*.coffee'], interval: 1007, ['api:run']

gulp.task 'sockets:run', runner 'websockets/app.coffee', 'sockets:run'
gulp.task 'sockets:watch', ['sockets:run', 'src:watch'], ->
  gulp.watch ['shared/**/*.js', 'websockets/app.coffee'], interval: 1007, ['sockets:run']

gulp.task 'web:scripts:coffee', ->
  gulp.src 'web/public/**/*.coffee'
    .pipe $gulp.cached 'web/public'
    .pipe caffeine()
    .pipe gulp.dest 'web/public'

gulp.task 'web:scripts:templates', ->
  gulp.src 'web/public/templates/**/*.html'
    .pipe $gulp.order()
    .pipe $gulp.angularTemplatecache module: 'app'
    .pipe gulp.dest 'web/public'

gulp.task 'web:scripts:concat', ['web:scripts:coffee', 'web:scripts:templates'], ->
  gulp.src ['web/public/app/base.js', 'web/public/app/javascripts/**/**/*.js', 'web/public/templates.js']
    .pipe $gulp.sourcemaps.init()
    .pipe $gulp.concat 'app.js'
    .pipe $gulp.uglify()
    .pipe $gulp.sourcemaps.write '.', sourceRoot: '/app'
    .pipe gulp.dest 'web/public/app'

gulp.task 'web:bower:bootstrap', ->
  gulp.src ['web/bower_components/bootstrap/fonts/*']
    .pipe gulp.dest 'web/public/fonts/'

gulp.task 'web:bower:font-awesome', ->
  gulp.src ['web/bower_components/font-awesome/fonts/*']
    .pipe gulp.dest 'web/public/fonts/'

gulp.task 'web:scripts:bower:async', ->
  gulp.src ['web/bower_components/async/lib/async.js']
    .pipe $gulp.sourcemaps.init()
    .pipe $gulp.uglify()
    .pipe $gulp.rename suffix: '.min'
    .pipe $gulp.sourcemaps.write '.', addComment: no, sourceRoot: '/'
    .pipe gulp.dest 'web/bower_components/async/lib'

gulp.task 'web:scripts:bower:flot', ->
  gulp.src ['web/bower_components/flot/jquery.flot*js']
    .pipe $gulp.sourcemaps.init()
    .pipe $gulp.uglify()
    .pipe $gulp.rename suffix: '.min'
    .pipe $gulp.sourcemaps.write '.', addComment: no, sourceRoot: '/'
    .pipe gulp.dest 'web/bower_components/flot/dist'

gulp.task 'web:scripts:bower:daterangepicker', ->
  gulp.src 'web/bower_components/bootstrap-daterangepicker/daterangepicker.js'
    .pipe $gulp.sourcemaps.init()
    .pipe $gulp.uglify()
    .pipe $gulp.rename suffix: '.min'
    .pipe $gulp.sourcemaps.write '.', addComment: no, sourceRoot: '/'
    .pipe gulp.dest 'web/bower_components/bootstrap-daterangepicker'

gulp.task 'web:bower', ['web:bower:bootstrap', 'web:bower:font-awesome', 'web:scripts:bower:async', 'web:scripts:bower:daterangepicker', 'web:scripts:bower:flot'], ->
  gulp.src mainBowerFiles paths: 'web/'
    .pipe $gulp.sourcemaps.init loadMaps: yes
    .pipe jsfilter = $gulp.filter '*.js'
    .pipe $gulp.concat 'main.js'
    .pipe jsfilter.restore()
    .pipe cssfilter = $gulp.filter '*.css'
    .pipe $gulp.concat 'main.css'
    .pipe cssfilter.restore()
    .pipe $gulp.sourcemaps.write '.', sourceRoot: '/bower_components'
    .pipe gulp.dest 'web/bower_components'

gulp.task 'web:scripts:ckeditor', ->
  gulp.src [
    'ckeditor/ckeditor.js'
    'ckeditor/lang/en.js'
    'ckeditor/styles.js'
    'bootstrapck4-skin/skins/bootstrapck/skin.js'
    'ckeditor-strinsert/plugin.js'
    'ng-ckeditor/ng-ckeditor.js'
  ], cwd: 'web/bower_components/'
    .pipe $gulp.concat 'ckeditor.js'
    .pipe gulp.dest 'web/bower_components'

gulp.task 'web:scripts:aceeditor', ->
  gulp.src [
    'ace-builds/src-noconflict/ace.js'
    'ace-builds/src-noconflict/ext-language_tools.js'
    'ace-builds/src-noconflict/mode-html.js'
    'ace-builds/src-noconflict/theme-monokai.js'
    'angular-ui-ace/ui-ace.js'
  ], cwd: 'web/bower_components/'
    .pipe $gulp.concat 'ace.js'
    .pipe $gulp.sourcemaps.init()
    .pipe $gulp.uglify()
    .pipe $gulp.rename suffix: '.min'
    .pipe $gulp.sourcemaps.write '.', sourceRoot: '/bower_components'
    .pipe gulp.dest 'web/bower_components'

gulp.task 'webadmin:scripts', ->
  gulp.src 'webadmin/public/javascripts/**/*.coffee'
    .pipe caffeine()
    .pipe gulp.dest 'webadmin/public/javascripts'

gulp.task 'web:scripts', ->
  gulp.start 'web:bower', 'web:scripts:aceeditor', 'web:scripts:ckeditor', 'web:scripts:concat', 'webadmin:scripts'

gulp.task 'web:sass', ->
  gulp.src 'web/public/stylesheets/app.scss'
    .pipe $gulp.sass errLogToConsole: yes
    .pipe gulp.dest 'web/public/stylesheets'

gulp.task 'web:build', ['src:build'], ->
  gulp.start 'web:scripts', 'web:sass', 'webadmin:scripts'

gulp.task 'web:run', ['web:build'], runner 'web/app.coffee', 'web:run'

gulp.task 'web:watch', ['web:run', 'src:watch'], ->
  gulp.watch 'web/public/stylesheets/**/*.scss', interval: 1007, ['web:sass']

  gulp.watch ['web/public/**/*.coffee', 'web/public/templates/**/*.html'], interval: 1007, ['web:scripts:concat']
  gulp.watch 'webadmin/public/javascripts/**/*.coffee', interval: 1007, ['webadmin:scripts']
  gulp.watch ['shared/**/*.js', 'web/app.coffee', 'web/{data,middleware,routes}/**/*.coffee', 'webadmin/*.coffee'], interval: 1007, ['web:run']

  gulp.watch ['web/public/app/app.js', 'web/public/stylesheets/**/*.css'], interval: 1007, (evt) ->
    gulp.src evt.path, read: no
      .pipe $gulp.livereload()

gulp.task 'test', ['test:cleanup', 'src:build'], ->
  CI = config.env is 'staging'

  gulp.src 'testbdd/**/*.coffee', read: no
    # switched to using spawn mocha because when gulp-mocha was run multiple times through
    # gulp test:watch, it was not properly cleaning up all the stuff. spawn mocha creates
    # a new process for every test run
    .pipe $gulp.spawnMocha
      r: 'testbdd/setup.js'
      R: if CI then 'tap' else 'spec'
    .once 'error', (err) ->
      console.error err unless ~err.message.indexOf 'code 1'
      return process.exit 1
      @emit 'end'

# TODO: these don't load new tests off disk
# TODO: also, rerunning the tests have a weird caching issue with setup
gulp.task 'test:watch', ['test', 'src:watch'], ->
  gulp.watch 'testbdd/**/*.coffee', interval: 1007, debounceDelay: 1500, ['test']

gulp.task 'pg:cleanup', (cb) ->
  require('./gulp-tasks/pg/cleanup') config.db.url, cb

gulp.task 'test:cleanup', ['pg:cleanup']

gulp.task 'env:configs', ->
  for dir in ['api/iis', 'web/iis', 'websockets/iis'] when fs.existsSync dir + "/web.#{config.env}.config"
    fs.createReadStream("#{dir}/web.#{config.env}.config").pipe fs.createWriteStream "#{dir}/web.config"

gulp.task 'env:serviceperms', require './gulp-tasks/tasks/env/service_permissions'
gulp.task 'env:setup', ['env:configs', 'env:serviceperms']

gulp.task 'version', (cb) ->
  exec = require('child_process').exec
  exec 'hg parent --template "{node|short} {date|shortdate} {branch}"', (err, stdout, stderr) ->
    fs.writeFile '.version', stdout, cb

gulp.task 'pg:create', ['src:build'], (cb) ->
  require('./gulp-tasks/pg/create') config.db.url, 'keystone', cb

gulp.task 'pg:users', ['pg:create'], (cb) ->
  require('./gulp-tasks/pg/users') config.db.url, cb

gulp.task 'pg:client', ['pg:create'], (cb) ->
  require('./gulp-tasks/pg/client') config.db.url, cb

gulp.task 'pg:setup', ->
  gulp.start 'pg:cleanup', 'pg:create', 'pg:users', 'pg:client'

gulp.task 'build', ->
  gulp.start 'version', 'pg:setup', 'src:build'

gulp.task 'api:docs', ->
  gulp.src './api/v1/api.apib'
    .pipe $gulp.aglio themeVariables: 'flatly', themeTemplate: 'triple'
    .pipe gulp.dest './api/v1'

watches = ['src', 'actionqueue', 'eventhose', 'pipeline', 'webhooks', 'api', 'sockets', 'web'].map (x) -> "#{x}:watch"
gulp.task 'default', ['test', 'pg:setup'].concat watches
gulp.task 'notests', ['pg:setup'].concat watches


# TODO I'd like to make this work, but moving on
# gulpProcess.kill() only kills the spawned process, which for some reason
# does not kill the children of that spawned process (something to do with gulp.watch?)
# gulp.task 'auto-reload', ->
#   {spawn} = require 'child_process'

#   gulpProcess = null
#   do spawnChildren = _.debounce ->
#     gulpProcess.kill() if gulpProcess
#     gulpProcess = spawn 'gulp.cmd', [], stdio: 'inherit'
#   , 5000, leading: yes, trailing: no
#   gulp.watch 'gulpfile.coffee', spawnChildren
