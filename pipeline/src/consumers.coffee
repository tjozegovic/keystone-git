_ = require 'lodash'

Processor = require './processor'
{PermanentError} = require './errors'

module.exports.PipelineConsumerFactory = class PipelineConsumerFactory
  constructor: (@opts) ->

  consume: (data, done) =>
    ctx = @opts.context_builder data
    consumer = new PipelineConsumer @opts, ctx, data
    consumer.process done

module.exports.PipelineConsumer = class PipelineConsumer
  ###
  # every rabbitmq message from the 'emails' queue will hit
  # the process method of this class. there is a new consumer
  # for every message, so this object is scoped to the lifetime
  # of the message. we look for or create a message from the database,
  # make a backup copy (to save to the database in case of failure),
  # then run the data through the pipeline.
  # happy path:
  #   process (done) ->
  #     fetch (err) ->
  #       create_processor.process route, done
  #   ondone ->
  #     emit done
  ###
  constructor: (@opts, @ctx, @data) ->
    @opts ?= { }
    @opts.filter_directory ?= './filters/'
    @emails = @ctx.use 'client/emails'

  ondone: (..., done) =>
    @data = _.omit @local_data, 'mail'
    @data.queued = new Date
    @data.mail = @local_data.mail

    # sender, status, lead, template, message replace with id versions

    @emails.save @data, (err) =>
      # at this point the email has already been sent. we've been putting
      # stuff in the DB for a while. if there is an error here... we might
      # have to just write it off?
      if err?
        @ctx.events.emit 'pipeline.errored', err: err if err?
        return done no

      @ctx.events.emit 'pipeline.completed', _.pick @data, 'client', 'id'
      @ctx.events.emit 'email.queued',
        if @ctx.client?.email_mode is 'Internal' then response: messageId: @data.mail?.response.messageId
        else response: @data.mail?.response
      done no

  onrequeue: (..., done) =>
    @ctx.events.emit 'email.requeued'
    done yes

  oncancel: (..., done) =>
    @data.canceled = new Date()
    @emails.save @data, (err) =>
      if _err?
        @ctx.events.emit 'pipeline.errored', err: _err if _err?
        return done _err

      @ctx.events.emit 'email.canceled'
      done no

  onerror: (err, done) =>
    error = error: err.error, stack: err.error.stack

    @data.errored ?= []
    @data.errored.push _.assign at: new Date(), error
    @emails.save @data, (_err) =>
      if _err?
        @ctx.events.emit 'pipeline.errored', err: _err if _err?
        return done _err

      @ctx.events.emit 'email.errored', error
      done err.error not instanceof PermanentError

  create_processor: (done) ->
    processor = new Processor()
    ['done', 'cancel', 'error', 'requeue'].forEach (event) =>
      processor.on event, _.partialRight @["on#{event}"], done
    return processor

  fetch: (cb) ->
    if @data.id?
      @emails.get @data.id, (err, body) =>
        return cb err if err?
        _.defaults @data, body
        cb null
    else
      @emails.save @data, (err, body) =>
        return cb err if err?
        _.assign @data, body
        cb null

  process: (done) ->
    # there are events scheduled already that are 'typeless' from previous iterations
    @data.type ?= 'drip'
    unless route = @ctx.router @data.type
      @ctx.events.emit 'pipeline.rejected', "email type: #{@data.type} is not a valid email type"
      return done()

    @fetch (err) =>
      if err?
        @ctx.events.emit 'pipeline.errored', err: err
        return done err

      # cloneDeep for some weird reason doesn't work like it should here
      # there was a case where a property was set to an extended error, and cloneDeep ignored it
      @local_data = _.clone @data
      @ctx.events.emit 'pipeline.started'

      processor = @create_processor done
      processor.process route.map (filter) =>
        # all modules in this route should have a signature of
        #   (ctx, data, next) ->
        # `next 'cancel'` will abort the email, raising email.canceled
        # `next 'requeue'` will requeue the email, raising email.requeued
        # `next null` to continue
        # `next <anything>` to error, raising email.errored
        mod = @opts.filter_directory + filter
        _.partial require(mod), @ctx, @local_data
