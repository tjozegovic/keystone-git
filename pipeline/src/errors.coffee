module.exports.PermanentError = class PermanentError extends Error
  name: 'PermanentError'
  constructor: (@message, @nested) ->
    super @message
    Error.captureStackTrace @, @constructor
