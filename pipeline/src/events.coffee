_ = require 'lodash'

events = require 'shared/eventlib'

module.exports = class Events
  @lib = events

  constructor: (@client, @email) ->

  emit: (type, data, cb) ->
    [data, cb] = [null, data] if typeof data is 'function'

    data ?= {}
    data.email = @email.id

    if @email.status and type isnt 'lead.status.changed'
      _.assign data, _.pick @email, 'status', 'segment', 'step', 'lead', 'batch'

    # TODO process.nextTick should probably be in the eventlib itself..
    # at this release time, i didn't want to make that big of a jump
    process.nextTick =>
      @constructor.lib.emit type, @client, data, cb
