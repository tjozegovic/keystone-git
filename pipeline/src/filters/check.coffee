async = require 'async'
{PermanentError} = require '../errors'

module.exports = (ctx, data, next) ->
  async.series [
    (cb) ->
      unless ~data.mail.to.indexOf '@'
        return cb new PermanentError "Message did not send because \"#{data.mail.to}\" is not an email address."

      cb null
    (cb) ->
      optouts = ctx.use 'core/optouts'
      optouts.get data.mail.to, (err, optout) ->
        if optout?.global
          return cb new PermanentError "Message did not send because \"#{data.mail.to}\" is globally opted out."

        if data.client in (optout?.clients || [])
          return cb new PermanentError "Message did not send because \"#{data.mail.to}\" is opted out."

        cb null
    (cb) ->
      return cb null unless integration_source = data.lead?.external?.source
      return cb null unless integration_source = ctx.client.integrations?._current

      unless integration_info = ctx.client.integrations?[integration_source]
        ctx.logger.debug "skipping integration check, no #{integration_source} integration information for client"
        return cb null

      unless integration = ctx.integrations.get "integrations/check/#{integration_source}"
        ctx.logger.debug "skipping integration check, no #{integration_source} integration function"
        return cb null

      integration.validate ctx, data, integration_info, data.lead.external.id, (err, rest...) ->
        return cb new PermanentError "Integration call failed.", err if err? and err instanceof Error
        cb err, rest...
  ], next
