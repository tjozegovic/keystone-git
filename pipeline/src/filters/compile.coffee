fs = require 'fs'

_ = require 'lodash'
S = require 'shared/util/string'
async = require 'async'
cheerio = require 'cheerio'
hb = require 'handlebars'
moment = require 'moment'
util = require 'shared/util'
{PermanentError} = require '../errors'

module.exports = (ctx, data, next) ->
  return next new Error 'No template found' unless data.template

  data.unsub = ctx.urls.unsub()
  data.viewinbrowser = ctx.urls.view()

  async.parallel [
    (cb) ->
      if data.mail.subject.value?
        tpl = hb.compile data.mail.subject.value
      else
        tpl = hb.compile data.mail.subject
      data.mail.subject = tpl data,
        helpers: require './helpers'
      cb null
    (cb) ->
      templates = ctx.use 'client/templates'
      template = data.template
      if template.value?
        template = data.template.value
      templates.get template, (err, template) ->
        return cb err if err?

        compile_html = (cb) ->
          return cb null if data.onlyPlainText

          if aws = ctx.client?.credentials?.aws
            # TODO cdn, cloudfront link
            _.defaults data, assets: baseurl: "https://#{aws.bucket}.s3.amazonaws.com", public: "https://#{aws.bucket}.s3.amazonaws.com"
            if aws.private?
              data.assets.private = "https://#{aws.private.bucket}.s3.amazonaws.com/#{aws.private.folder}"

          # deep clone so that these properties and forward are not "saved" with the message
          tpldata = _.defaults _.cloneDeep(data),
            link: {}
            now: moment()
            utcnow: moment.utc()
          $ = cheerio.load template.html.toString()
          _.forIn data.html?.blocks or {}, (html, name) ->
            el = $ "[data-block=\"#{name}\"]"
            if html?.value?
              el.html html.value
            else
              el.html html
            el.removeAttr 'data-block'

          _.forIn data.html?.policies or {}, (id, name) ->
            el = $ "[data-policy=\"#{name}\"]"
            el.html ctx.policies[id.value or id]?.value or ''
            el.removeAttr 'data-policy'

          _.forIn data.html?.links or {}, (link, name) ->
            el = $ "[data-link=\"#{name}\"]"

            el.attr 'href', if link.type is 'page'
              ctx.urls.page link.value
            else
              link.value

            if link.text?
              el.each ->
                ael = $ @
                textel = ael.children '[data-text]'
                textel = ael unless textel.length
                textel.text link.text

            if link.value?
              # it is possible to add just a link url to a block
              # this allows links that might be templates themselves
              # to still be referenced in the blocks
              tpldata.link[name] =
                type: link.type
                url: if link.type is 'page'
                    ctx.urls.page link.value
                  else
                    hb.compile(link.value) tpldata

          if src = ctx.urls.bug? data.id
            data.bug = src
            (if $('body').length then $('body') else $.root()).append "<img src='#{src}' />"

          if data.lead?
            for name, value of data.lead.attributes or {}
              data.lead[name] ?= {}
              data.lead[name][S(value).indexable()] = yes

            for name, value of data.lead.proper_attributes or {}
              [name, value] = [name, value].map (str) -> S(str).indexable()
              data.lead[name] ?= {}
              data.lead[name][value] = yes

          try
            tpl = hb.compile $.html decodeEntities: no
            data.mail.html = tpl tpldata,
              helpers: require './helpers'
              partials: 'sender.signature': data.sender?.signature or ''
            cb null
          catch e
            cb new PermanentError 'Could not compile html.', e

        compile_text = (cb) ->
          tpl = hb.compile template.text.toString()

          tpldata = _.defaults _.cloneDeep(data),
            link: {}
            now: moment()
            utcnow: moment.utc()

          _.forIn data.text?.links or {}, (link, name) ->
            return unless link.value?
            tplLink = hb.compile link.value
            tpldata.link[name] =
              type: link.type
              url: if link.type is 'page'
                  link.value
                else
                  tplLink tpldata

          try
            data.mail.text = tpl tpldata,
              helpers: require './helpers'
              partials: do ->
                partials = {}
                _.forIn data.text?.blocks or {}, (block, name) ->
                  if block.value?
                    partials["block.#{name}"] = block.value
                  else
                    partials["block.#{name}"] = block

                _.forIn data.text?.policies or {}, (policy, name) ->
                  partials["policy.#{name}"] = ctx.policies[policy.value or policy]?.value or ''

                _.forIn data.text?.links or {}, (link, name) ->
                  partials["link.#{name}"] = if link.type is 'page'
                    ctx.urls.page link.value
                  else
                    link.value or ''

                partials
            cb null
          catch e
            cb new PermanentError 'Could not compile plain text.', e

        async.parallel [compile_html, compile_text], cb
  ], next
