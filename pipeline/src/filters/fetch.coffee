_ = require 'lodash'
async = require 'async'
{PermanentError} = require '../errors'

module.exports = (ctx, data, next) ->
  data.mail ?= {}
  return next() unless data.client

  is_uuid = (id) -> /\w{32}/.test id.replace /-/g, ''

  wrap = (message, inner) ->
    err = new Error message
    err.inner = inner if inner
    err

  tasks = {}

  tasks['lead'] = (cb) ->
    return cb null unless data.lead?
    return cb null if data.batch?

    if data.lead.email?
      data.recipient = email: data.lead.email
      return cb null

    if is_uuid data.lead
      [attributes, leads] = ctx.use 'client/attributes', 'client/leads'
      leads.get data.lead, (err, _lead) ->
        return cb wrap "Lead lookup failed #{data.lead}", err if err
        return cb null unless _lead? # data.lead may not be an id...

        data.lead = _.omit _lead, 'recipient'
        data.recipient = _lead.recipient
        return cb null unless data.lead.$attributes?

        attributes.all (err, attributes) ->
          return cb wrap 'Attribute lookup failed', err if err?
          return cb null unless attributes?

          data.lead.proper_attributes = {}
          for pair in data.lead.$attributes
            [nameid, valueid] = _.map pair.split '/'
            continue unless (attr = _.find attributes, id: nameid)?

            [name, value] = [attr.name, attr.values[valueid]?.name or 'n/a']
            data.lead.proper_attributes[name] or= value

          cb null

    else
      return cb new PermanentError "Could not find lead #{data.lead}"

  tasks['sender'] = ['lead', (cb, results) ->
    # trim sender to remove white space. if this leaves string empty, set as default if exists.
    sender = (data.lead?.agent or data.sender or data.agent)?.trim()

    unless sender? and sender.length
      return cb new PermanentError 'No sender given and no default sender' unless ctx.client.default_sender?
      sender = ctx.client.default_sender

    senders = ctx.use 'client/senders'
    senders.active_by_name_or_alias sender, (err, _sender) ->
      return cb wrap "Sender lookup failed #{sender}", err if err
      return cb new PermanentError "Could not find sender #{sender}" unless _sender?

      done = ->
        # TODO: all these values could be cached? trick is choosing which based off if svemails enabled
        data.sender = _sender
        unless data.sender.friendlyfrom?
          data.sender.friendlyfrom = "#{_sender.firstname} #{_sender.lastname}"

        if ctx.client.use_svemails and ctx.client.svemails?
          data.sender.domain = "#{ctx.client.svemails.prefix}.svemails.com".toLowerCase()
          data.mail.from = "#{data.sender.friendlyfrom} <#{data.sender.role || 'info'}@#{data.sender.domain}>"
        else
          data.sender.domain = data.sender.email.split('@')[1].toLowerCase()
        cb null

      return done() unless _sender.group?
      unless is_uuid _sender.group
        return cb new PermanentError "Could not complete lookup for #{_sender.group}. Expected a group id."

      groups = ctx.use 'client/groups'
      groups.get _sender.group, (err, group) ->
        return cb wrap "Group lookup failed for #{_sender.group} during email creation", err if err?
        return cb new PermanentError "Could not find group #{_sender.group}" unless group?

        _.defaults _sender, group
        _sender.signature = group.signature if group.signature? and not _sender.override
        done()
  ]

  tasks['drip'] = (cb) ->
    return cb null unless data.status? and data.segment? and data.step?

    statuses = ctx.use 'client/statuses'
    statuses.get data.status, (err, status) ->
      return cb wrap "Could not find status #{data.status}", err if err?
      return cb null unless status

      data.status = _.pick status, 'id', 'version', 'created', 'updated', 'name'

      segment = _.find status.segments, id: data.segment
      data.segment = _.pick segment, 'id', 'name'

      activeSteps = _.filter segment.steps, (step) -> not step.active? or step.active
      step = _.find activeSteps, id: data.step
      unless step?
        return cb new PermanentError "Could not find active step #{data.step}."

      data.step = _.pick step, 'id', 'delay', 'name', 'message'

      cb null

  tasks['message'] = ['drip', (cb) ->
    return cb null unless (id = data.step?.message or data.message)?

    messages = ctx.use 'client/messages'
    messages.get id, (err, message) ->
      return cb wrap "Could not find message #{id}", err if err?

      data.message = message
      cb null
  ]

  tasks['batch'] = (cb) ->
    return cb null unless data.batch?

    batches = ctx.use 'client/batches'
    batches.get2 data.batch, (err, batch) ->
      return cb wrap "Could not find batch #{data.batch}", err if err?

      data.batch = _.pick batch, 'id', 'name', 'schedule', 'vunotification'
      _.assign data, _.pick batch, 'template', 'html', 'text'
      data.mail.subject = batch.subject
      cb null

  tasks['headers'] = ['lead', 'sender', (cb) ->
    data.mail.to ?= data.recipient.email if data.recipient
    data.mail.from ?= "#{data.sender.friendlyfrom} <#{data.sender.email}>"

    cb null
  ]

  tasks['versions'] = ['drip', 'lead', 'message', (cb) ->
    return cb null if data.batch?

    {lead, message} = data
    if lead?.$attributes? and message.versions?.length
      activeVersions = _.filter message.versions, hidden: no
      found = _.find activeVersions, (segment) ->
        _.some segment.products, (pairs) ->
          _.every pairs, (pair) ->
            pair in lead.$attributes

      if found?
        [base, message] = [message, found]

        ['subject', 'template'].forEach (root) ->
          if (ref = message[root]?.ref)? and ref is 'default'
            message[root] = base[root]
          else if (val = message[root]?.value)?
            message[root] = val

        ['html', 'text'].forEach (type) ->
          ['links', 'blocks', 'policies'].forEach (area) ->
            return unless message[type]?[area]?
            Object.keys(message[type][area]).forEach (name) ->
              val = message[type][area][name]
              if (ref = val.ref)? and ref is 'default'
                message[type][area][name] = base[type][area][name]

    data.message = _.pick message, 'id', 'name'
    _.assign data, _.pick message, 'template', 'html', 'text'
    data.mail.subject = message.subject
    cb null
  ]

  tasks['policies'] = ['batch', 'versions', (cb) ->
    ids = [].concat _.values(data?.html?.policies or {}), _.values(data?.text?.policies or {})
    ids = (id.value or id for id in ids)

    policies = ctx.use 'client/policies'
    policies.fetch ids, (err, policies) ->
      return cb wrap 'Could not lookup policies', err if err?

      ctx.policies = _.indexBy policies, 'id'
      cb null
  ]

  async.auto tasks, next
