_ = require 'lodash'

module.exports = helpers = {}

helpers = _.assign helpers, require('./moment')()
helpers.capitalize = (value) -> value.charAt(0).toUpperCase() + value.substring(1).toLowerCase()
