module.exports = ->
  _ = require("lodash")
  moment = require("moment")

  moment: (context, block) ->
    if context and context.hash
      block = _.cloneDeep(context)
      context = undefined
    date = moment(context)

    # Reset the language back to default before doing anything else
    date.locale 'en' unless 'locale' of block.hash
    for fn, arg of block.hash
      if date[fn]
        date = date[fn] arg
      else
        console.log "moment.js does not support #{fn}"
    date

  duration: (context, block) ->
    if context and context.hash
      block = _.cloneDeep(context)
      context = 0
    duration = moment.duration(context)

    # Reset the language back to default before doing anything else
    duration.locale 'en' unless 'locale' of block.hash
    for i of block.hash
      if duration[i]
        duration = duration[i](block.hash[i])
      else
        console.log "moment.js duration does not support #{i}"
    duration
