async = require 'async'
cheerio = require 'cheerio'
uuid = require 'uuid'

module.exports = (ctx, data, done) ->
  return done() unless data.mail.html?.length

  $ = cheerio.load data.mail.html

  anchors = $ 'a'
  return done() unless anchors.length

  data.links = anchors.filter ->
    link = $ @
    typeof link.data('donottrack') is 'undefined' and !~link.attr('href').indexOf '/unsub'
  .map ->
    link = $ @

    link_id = uuid.v4().replace /-/g, ''
    name = link.data 'link'
    title = link.attr 'title'
    url = ctx.urls.link link_id
    original_url = link.attr 'href'

    link.attr 'href', url
    link.removeAttr 'data-link'

    id: link_id
    name: name
    title: title
    url: url
    original_url: original_url
  .get()

  data.mail.html = $.html decodeEntities: no
  done null
