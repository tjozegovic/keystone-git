_ = require 'lodash'
async = require 'async'
hb = require 'handlebars'
moment = require 'moment-timezone'

module.exports = (ctx, data, next) ->
  async.auto
    template: (cb) ->
      templates = ctx.use 'core/templates'
      templates.get_by_client data.client, (err, template) ->
        if template? and data.type is 'vunotification'
          cb err, template
        else templates.get data.type, (err, template) ->
          cb err, template
    subject: ['template', (cb, results) ->
      tpl = hb.compile results.template.subject
      cb null, tpl data, helpers: require '../helpers'
    ]
    html: ['template', (cb, results) ->
      tpl = hb.compile results.template.hbs

      tpldata = _.defaults _.cloneDeep(data),
        now: moment()
        utcnow: moment.utc()
        today: moment().format 'dddd, MMMM Do'
        sentlocal: moment.utc(data.sent).tz('America/Chicago').format 'llll'

      # assign the specific email from a vunotification to the root template data object
      _.defaults tpldata, ctx.email

      cb null, tpl tpldata, helpers: require '../helpers'
    ]
  , (err, results) ->
    _.assign data.mail, _.pick results, ['subject', 'html']
    next err
