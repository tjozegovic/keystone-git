_ = require 'lodash'
async = require 'async'
moment = require 'moment-timezone'

module.exports = (ctx, data, next) ->
  data.mail ?= {}
  data.mail.from = switch data.type
    when 'dailyvu' then 'SoftVu DailyVu <dailyvu@app.softvu.com>'
    when 'clickvu' then 'SoftVu ClickVu <clickvu@app.softvu.com>'
    else 'SoftVu Notifications <notifications@app.softvu.com>'

  [clients, senders, _emails] = ctx.use 'core/clients', 'client/senders', 'client/emails'

  async.auto
    client: (cb) ->
      clients.get data.client, (err, client) ->
        cb err, client
    email: (cb) ->
      return cb() unless data.email
      _emails.get data.email, (err, email) ->
        cb err, email
    agent: (cb) ->
      return cb() unless data.agent
      senders.get data.sender, (err, sender) ->
        data.mail.to = agent.email
        cb err, agent
    emails: ['client', 'agent', (cb, {client, agent}) ->
      return cb() unless _.isArray data.emails
      _emails.fetch data.emails, (err, body) ->
        return cb err if err
        emails = _.pluck body.rows, 'doc'
        emails.forEach (email) ->
          email.campaign = if email.batch?
            email.batch.name
          else
            "#{email.status.name} / #{email.segment.name} / #{email.step.name}"

          email.queued = moment.tz(email.queued, agent.timezone or client.timezone).format 'M/D/YYYY h:mm a z'

          click = email.clicked.pop()
          email.clicked = moment.tz(click.at, agent.timezone or client.timezone).format 'M/D/YYYY h:mm a z'
        cb null, emails
    ]
  , (err, results) ->
    _.assign ctx, results
    _.assign data, _.pick results, 'agent', 'emails'
    next err
