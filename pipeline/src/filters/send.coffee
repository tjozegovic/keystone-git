_ = require 'lodash'

module.exports = (ctx, data, done) ->
  # TODO should be configurable per message, not just based on type
  if data.type in ['drip', 'batch', 'direct']
    if (ctx.client.debug or ctx.client.email_mode is 'Debug') and ctx.config.env in ['production', 'staging']
      unless ctx.client.manager?.length
        return done 'Manager email is not set'

      data.mail.to = ctx.client.manager
    # TODO needs a little more thought. to, cc, and bcc need to be arrays here
    data.mail.envelope = _.pick data.mail, 'from', 'to', 'cc', 'bcc'

    if data.sender?.domain?
      toenc = data.mail.to.replace /@/g, '='
      data.mail.envelope.from = "#{data.client}.#{data.id}.#{toenc}@#{data.sender.domain}"

  data.mail.headers ?= {}
  if data.type in ['drip', 'batch', 'direct']
    data.mail.headers['Reply-To'] = "<#{data.sender.replyto || data.sender.email}>"
  if data.unsub?
    data.mail.headers['List-Unsubscribe'] = "<#{data.unsub}>"
  if ctx.client.email_mode is 'Debug'
    data.mail.headers['X-Recipient'] = data.recipient.email
    if data.type is 'drip' and not data.sample
      data.mail.headers['X-Message'] = data.message.name
      data.mail.headers['X-Status'] = data.status.name
      data.mail.headers['X-Segment'] = data.segment.name
      data.mail.headers['X-Step'] = data.step.name

  ctx.mailer.send data.mail, (err, response) ->
    return done err if err

    data.mail.response = response
    done null, response
