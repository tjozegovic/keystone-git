_ = require 'lodash'
async = require 'async'

module.exports = (ctx, data, next) ->
  return next() unless client_id = data.client

  email_id = data.id

  clients = ctx.use 'core/clients'
  clients.get client_id, (err, body) ->
    return next err unless ctx.client = client_doc = body

    if ctx.client.email_mode is 'Off'
      ctx.events.emit 'pipeline.dropped', message: 'Email mode is set to Off'
      return next 'cancel'

    # bring in information from account settings
    data.account = _.pick ctx.client, 'name', 'privacy_url'

    base = ctx.config?.links?.baseurl or 'https://app.softvu.com'
    ctx.urls =
      link: (id) -> "#{base}/click/#{client_id}/#{email_id}/#{id}"
      page: (id) -> "#{base}/page/#{client_id}/#{id}?e=#{email_id}"
      bug: -> "#{base}/pb/#{client_id}/#{email_id}"
      unsub: -> "#{base}/unsub/#{client_id}/#{email_id}"
      view: -> "#{base}/view/#{client_id}/#{email_id}"

    if client_doc.use_svemails and client_doc.svemails?.prefix?
      makebase = (prefix = '') ->
        prefix = prefix + '.' if prefix.length
        "http://#{prefix}#{client_doc.svemails.prefix}.svemails.com"

      data.assets =
        baseurl: "http://img.#{client_doc.svemails.prefix}.svemails.com"

      _.assign ctx.urls,
        link: (id) -> "#{makebase('go')}/click/#{email_id}/#{id}"
        bug: -> "#{makebase('go')}/pb/#{email_id}"
        unsub: -> "#{makebase('go')}/unsub/#{email_id}"
    else if client_doc.cnames?.length
      base = if _.isObject client_doc.cnames[0]
        cname = client_doc.cnames[0]
        proto = -> if cname.use_https then 'https://' else 'http://'
        "#{proto()}#{cname.host}"
      else
        'https://' + client_doc.cnames[0]

      if client_doc.assets?
        data.assets = baseurl: client_doc.assets

      _.assign ctx.urls,
        link: (id) -> "#{base}/click/#{email_id}/#{id}"
        page: (id) -> "#{base}/page/#{id}?e=#{email_id}"
        bug: -> "#{base}/pb/#{email_id}"
        unsub: -> "#{base}/unsub/#{email_id}"
        view: -> "#{base}/view/#{email_id}"

    next null
