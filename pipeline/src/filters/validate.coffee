{PermanentError} = require '../errors'

module.exports = (ctx, data, next) ->
  unless data?.mail
    return next new PermanentError 'Must have someone to send things too.'

  unless data.mail.to
    return next new PermanentError 'Mail item must have a recipient.'

  next()
