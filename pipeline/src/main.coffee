if ~process.cwd().indexOf 'pipeline'
  process.env.NODE_CONFIG_DIR ?= '../config'

config = require 'config'

_ = require 'lodash'

{Database, RepositoryFactory} = require 'shared/database'
listen = require 'shared/queues/listen'
uuid = require 'uuid'

logger = do (fs = require 'fs', dir = __dirname + '/../log') ->
  fs.mkdirSync dir unless fs.existsSync dir

  winston = require 'winston'
  winston.add winston.transports.File,
    filename: dir + '/service.log'
    maxsize: 10 * 1024 * 1024
    maxFiles: 10
    timestamp: yes
    level: 'debug'
    tailable: yes
    json: no

  winston
Database::LOGGER = logger

Events = require './events'
Processor = require './processor'
Transport = require './transport'
{PipelineConsumerFactory} = require './consumers'

integrations = new class Integrations
  constructor: ->
    @cache = {}

  get: (name) ->
    return new @cache[name] if @cache[name]?

    try
      klass = require "./#{name}"
      return new (@cache[name] = klass)
    catch
      return @cache[name] = null

module.exports.context_builder = context_builder = (data) ->
  # ctx is fed into Transport, need to keep it scoped out here
  ctx = { }

  factory = new RepositoryFactory config.db.url

  _.assign ctx,
    config: config
    use: factory.use.bind factory
    events: new Events data.client, data
    mailer: new Transport ctx, data
    logger: logger
    integrations: integrations
    router: (route_type) ->
      switch route_type
        when 'vunotification', 'form.response', 'dailyvu', 'clickvu'
          ['notifications/setup', 'notifications/compile', 'validate', 'send']
        when 'drip', 'batch', 'direct'
          ['setup', 'fetch', 'check', 'compile', 'validate', 'linkify', 'send']

  if data.client
    factory.client = data.client

  return ctx

module.exports.start = start = ->
  factory = new PipelineConsumerFactory
    filter_directory: './filters/'
    context_builder: context_builder

  listener = listen config.queues.url,
    limit: if config.env is 'production' then 12 else 4
    retries:
      enabled: yes
      waits: config.pipeline['retry wait in seconds'].map (x) -> x * 1000

  listener.consume 'email', factory.consume, ->
    logger.info "listening to queue \"email\" env: #{process.env.NODE_ENV or 'dev'}"
    Events.lib.heartbeat 'pipeline.heartbeat', ->
      metrics: Processor.metrics

  listener.on 'error', (err) ->
    logger.error 'listener errored', error: err
    Events.lib.emit 'pipeline.error', null, at: new Date, error: err

  graceful = ->
    listener.close -> process.exit()

  process.on 'SIGTERM', graceful
  process.on 'SIGINT', graceful

  require('shared/utils/process').lockfile __dirname

  do ->
    MailDev = require 'maildev'
    maildev = new MailDev ip: '127.0.0.1', smtp: 10025, web: 10080, verbose: yes
    maildev.listen()
    maildev.on 'new', (email) ->
      console.log "Received new email with subject: #{email.subject}"
      setTimeout ->
        Events.lib.emit 'email.delivered', email.headers['x-client'], email: email.headers['x-email']
      , 2000

start() if require.main is module
