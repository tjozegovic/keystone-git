domain = require 'domain'
{EventEmitter} = require 'events'

_ = require 'lodash'
async = require 'async'

module.exports.seconds = _inseconds = (duration) -> "#{(duration[0] + duration[1] / 1e9).toFixed(4)} s"

module.exports = class Processor extends EventEmitter
  @metrics =
    start: new Date
    last: null
    errors: 0
    cancels: 0
    requeues: 0
    processed: 0
    executions: []

  onerror: (err, duration) =>
    if err instanceof Error
      ['domain', 'domainThrown', 'domainBound', 'domainEmitter'].forEach (key) ->
        delete err[key]

    @emit 'error', error: err, $duration: duration or _inseconds process.hrtime @start

  process: (slip) ->
    @start = process.hrtime()

    @domain = domain.create()
    @domain.on 'error', @onerror

    @emit 'start'
    async.eachSeries slip
    , (route, cb) =>
      @domain.run ->
        try
          route cb
        catch e
          cb e
    , (err) =>
      @domain.removeListener 'error', @onerror

      {metrics} = @constructor
      duration = process.hrtime @start
      seconds = _inseconds duration

      if err is 'cancel'
        metrics.cancels += 1
        @emit 'cancel', $duration: seconds
      else if err is 'requeue'
        #TODO Should we add some type of logging to help better understand why certain
        #emails are getting requed or erroring.  Then we can devised methods to combat those.
        metrics.requeues += 1
        @emit 'requeue', $duration: seconds
      else if err?
        metrics.errors += 1
        @onerror err, seconds
      else
        @emit 'done', $duration: seconds

      metrics.processed += 1
      metrics.last = new Date()

      metrics.executions.splice 0, metrics.executions.length - 9
      metrics.executions.push
        at: new Date()
        duration: ms = duration[0] + duration[1] / 1e9
        trend: ms - _.last(metrics.executions)?.duration or 0
