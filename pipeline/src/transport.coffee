_ = require 'lodash'
nodemailer = require 'nodemailer'
{signer} = require 'nodemailer-dkim'

config = require 'config'

get_transport = (ctx, domain) ->
  transport =
    remote: (ctx.client.debug or ctx.client.email_mode is 'Debug') or
            use_powermta = config.env is 'production' or process.env.KEYSTONE_USEPMTA?
    transport: if (use_powermta or ctx.client.debug) and config.env != 'staging'
        nodemailer.createTransport config.pipeline.smtp
      # see test setup.js
      else if ctx.client.email_mode is 'Internal' or process.env.KEYSTONE_USESTUBMAILER
        stubTransport = require 'nodemailer-stub-transport'
        nodemailer.createTransport stubTransport()
      else
        nodemailer.createTransport()

  {dkim} = ctx.client
  return transport unless domain and dkim?.enabled
  return transport unless key = _.find dkim.keys, domain: domain, selector: dkim.domain_selector?[domain]

  transport.transport.use 'stream', signer
    domainName: key.domain
    keySelector: key.selector
    privateKey: key.privateKey
  return transport

module.exports = class Transport
  constructor: (@ctx, @email) ->

  send: (message, cb) ->
    message.headers ?= {}
    _.assign message.headers,
      'X-System': 'keystone'
      'X-Client': @email.client
      'X-Email': @email.id

      # these are for the legacy part that sends through power mta
      'X-ClientID': @email.client
      'X-MailID': @email.id

    domain = message.envelope.from.split('@')[1].toLowerCase() if message.envelope?.from?
    {remote, transport} = get_transport @ctx, domain

    message.xMailer = no
    transport.sendMail message, (error, info) =>
      return cb error if error

      process.nextTick ->
        cb null, _.pick info, 'response', 'messageId'

      return if remote and config.env != 'staging'

      setTimeout =>
        if info.errors?.length
          @ctx.events.emit 'email.bounced', message: info.errors[0]
        else if info.pending?.length
          @ctx.events.emit 'email.deferred', message: info.pending[0].response
        else
          @ctx.events.emit 'email.delivered',
            if @ctx.client.email_mode is 'Internal' then messageId: info.messageId
            else _.pick info, 'response', 'messageId'
      , 2000
