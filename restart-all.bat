@echo off

CALL gulp version env:setup src:build actionqueue:build eventhose:build pipeline:build webhooks:build web:build webadmin:scripts

IF "%NODE_ENV%"=="staging" CALL gulp test || GOTO :error

appcmd add vdir /app.name:keystone/ /path:/bower_components /physicalPath:"%~dp0web\bower_components"
appcmd add vdir /app.name:keystone/ /path:/app /physicalPath:"%~dp0web\public\app"
appcmd add vdir /app.name:keystone/ /path:/images /physicalPath:"%~dp0web\public\images"
appcmd add vdir /app.name:keystone/ /path:/javascripts /physicalPath:"%~dp0web\public\javascripts"
appcmd add vdir /app.name:keystone/ /path:/stylesheets /physicalPath:"%~dp0web\public\stylesheets"

appcmd recycle apppool /apppool.name:keystone
appcmd recycle apppool /apppool.name:keystone-api

CALL :restart "actionqueue"
CALL :restart "eventhose"
CALL :restart "pipeline"
CALL :restart "webhooks"

icacls web\iis /grant "IIS AppPool\keystone":F
icacls webadmin\iis /grant "IIS AppPool\keystone":F
icacls websockets\iis /grant "IIS AppPool\keystone":F
icacls api\iis /grant "IIS AppPool\keystone-api":F

GOTO :EOF

:error
EXIT /b %errorlevel%

:restart
sc query "keystone.%1" | FIND /i "RUNNING" > NUL
IF NOT ERRORLEVEL 1 (
    ECHO "%1 is running, restarting..."
    net stop keystone.%1
    net start keystone.%1
) ELSE (
    ECHO "%1 not running, skipping restart"
)
EXIT /b 0
