_ = require 'lodash'
S = require 'shared/util/string'
util = require './util'

module.exports =
  mapPath: (attributes, groups) ->
    attribute_map = _.transform groups, (attribute_map, attribute, key) ->
      attribute_name = S(attribute.name).indexable()

      group = attribute_map[attribute_name] ?= key: attribute.id or key, aliases: {}, default: attribute.default, name: attribute.name

      for own key, value of attribute.values
        group.aliases[S(value.name).indexable()] = key

      for alias in attribute.aliases || []
        group.aliases[S(alias.name).indexable()] = alias.value

    for own group, alias of attributes
      unless (group_details = attribute_map[group])?
        err = new Error "Lead attribute name '#{group}' does not exist."
        err.attribute_map = attribute_map
        throw err

      if (alias_name = S(alias).indexable()).length
        alias_index = attribute_map[group].aliases?[alias_name]
      else
        alias_index = attribute_map[group].default

      if alias_index is undefined
        throw new Error "Lead Attribute value (#{group_details.name}, #{alias}) could not be mapped."

      "#{group_details.key}/#{alias_index}"

  mapFilters: (filters, groups) ->
    map = (for {name, values} in filters

      attribute_index = name if groups[name]

      (for value in values
        value_index = value if groups?[name]?.values[value]
        if attribute_index is undefined or value_index is undefined
          throw new Error 'Lead Attribute values could not be mapped.'

        "#{attribute_index}/#{value_index}"
      ).sort()
    ).sort()

    util.xproduct map...
