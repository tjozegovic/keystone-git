(function() {
  var S, _, util,
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  S = require('shared/util/string');

  util = require('./util');

  module.exports = {
    mapPath: function(attributes, groups) {
      var alias, alias_index, alias_name, attribute_map, err, group, group_details, ref, results;
      attribute_map = _.transform(groups, function(attribute_map, attribute, key) {
        var alias, attribute_name, group, i, len, ref, ref1, results, value;
        attribute_name = S(attribute.name).indexable();
        group = attribute_map[attribute_name] != null ? attribute_map[attribute_name] : attribute_map[attribute_name] = {
          key: attribute.id || key,
          aliases: {},
          "default": attribute["default"],
          name: attribute.name
        };
        ref = attribute.values;
        for (key in ref) {
          if (!hasProp.call(ref, key)) continue;
          value = ref[key];
          group.aliases[S(value.name).indexable()] = key;
        }
        ref1 = attribute.aliases || [];
        results = [];
        for (i = 0, len = ref1.length; i < len; i++) {
          alias = ref1[i];
          results.push(group.aliases[S(alias.name).indexable()] = alias.value);
        }
        return results;
      });
      results = [];
      for (group in attributes) {
        if (!hasProp.call(attributes, group)) continue;
        alias = attributes[group];
        if ((group_details = attribute_map[group]) == null) {
          err = new Error("Lead attribute name '" + group + "' does not exist.");
          err.attribute_map = attribute_map;
          throw err;
        }
        if ((alias_name = S(alias).indexable()).length) {
          alias_index = (ref = attribute_map[group].aliases) != null ? ref[alias_name] : void 0;
        } else {
          alias_index = attribute_map[group]["default"];
        }
        if (alias_index === void 0) {
          throw new Error("Lead Attribute value (" + group_details.name + ", " + alias + ") could not be mapped.");
        }
        results.push(group_details.key + "/" + alias_index);
      }
      return results;
    },
    mapFilters: function(filters, groups) {
      var attribute_index, map, name, value, value_index, values;
      map = ((function() {
        var i, len, ref, results;
        results = [];
        for (i = 0, len = filters.length; i < len; i++) {
          ref = filters[i], name = ref.name, values = ref.values;
          if (groups[name]) {
            attribute_index = name;
          }
          results.push(((function() {
            var j, len1, ref1, results1;
            results1 = [];
            for (j = 0, len1 = values.length; j < len1; j++) {
              value = values[j];
              if (groups != null ? (ref1 = groups[name]) != null ? ref1.values[value] : void 0 : void 0) {
                value_index = value;
              }
              if (attribute_index === void 0 || value_index === void 0) {
                throw new Error('Lead Attribute values could not be mapped.');
              }
              results1.push(attribute_index + "/" + value_index);
            }
            return results1;
          })()).sort());
        }
        return results;
      })()).sort();
      return util.xproduct.apply(util, map);
    }
  };

}).call(this);
