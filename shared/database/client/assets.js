(function() {
  var Assets,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = Assets = (function(superClass) {
    extend(Assets, superClass);

    function Assets() {
      return Assets.__super__.constructor.apply(this, arguments);
    }

    return Assets;

  })(require('./module'));

}).call(this);
