_ = require 'lodash'
util = require 'util'

module.exports = class Attributes extends require './collection/data'
  @::type = 'attribute'

  all: (cb) ->
    super (err, attributes) ->
      return cb err if err?
      cb null, _.map attributes, (attribute) ->
        _.assign attribute, id: attribute.id.replace /-/g, ''

  active: (cb) ->
    super (err, attributes) ->
      return cb err if err?

      cb null, _.map attributes, (attribute) ->
        id: attribute.id.replace /-/g, ''
        version: attribute.version
        name: attribute.name
        default: attribute.default
        values: _.map attribute.values, (value, id) ->
          id: id
          name: value.name
          aliases: _.pluck _.where(attribute.aliases, value: id), 'name'

  # temporary until all calls to above function are refactored
  active_reg: (cb) ->
    sql = @squel.select()
      .from "#{@table}_current"
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'published'
      .field 'doc'
      .where "NOT (doc ? 'active') OR (doc->>'active')::bool"
      .where 'type = ?', @type

    @list sql, cb

  save: (doc, cb) ->
    doc.aliases ?= []
    doc.values = _.transform doc.values, (values, value) ->
      values[value.id] = name: value.name
      _.each value.aliases, (alias) ->
        doc.aliases.push name: alias, value: value.id
    , {}

    super doc, cb
