(function() {
  var Attributes, _, util,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  util = require('util');

  module.exports = Attributes = (function(superClass) {
    extend(Attributes, superClass);

    function Attributes() {
      return Attributes.__super__.constructor.apply(this, arguments);
    }

    Attributes.prototype.type = 'attribute';

    Attributes.prototype.all = function(cb) {
      return Attributes.__super__.all.call(this, function(err, attributes) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, _.map(attributes, function(attribute) {
          return _.assign(attribute, {
            id: attribute.id.replace(/-/g, '')
          });
        }));
      });
    };

    Attributes.prototype.active = function(cb) {
      return Attributes.__super__.active.call(this, function(err, attributes) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, _.map(attributes, function(attribute) {
          return {
            id: attribute.id.replace(/-/g, ''),
            version: attribute.version,
            name: attribute.name,
            "default": attribute["default"],
            values: _.map(attribute.values, function(value, id) {
              return {
                id: id,
                name: value.name,
                aliases: _.pluck(_.where(attribute.aliases, {
                  value: id
                }), 'name')
              };
            })
          };
        }));
      });
    };

    Attributes.prototype.active_reg = function(cb) {
      var sql;
      sql = this.squel.select().from(this.table + "_current").field("REPLACE(id::text, '-', '')", 'id').field('version').field('published').field('doc').where("NOT (doc ? 'active') OR (doc->>'active')::bool").where('type = ?', this.type);
      return this.list(sql, cb);
    };

    Attributes.prototype.save = function(doc, cb) {
      if (doc.aliases == null) {
        doc.aliases = [];
      }
      doc.values = _.transform(doc.values, function(values, value) {
        values[value.id] = {
          name: value.name
        };
        return _.each(value.aliases, function(alias) {
          return doc.aliases.push({
            name: alias,
            value: value.id
          });
        });
      }, {});
      return Attributes.__super__.save.call(this, doc, cb);
    };

    return Attributes;

  })(require('./collection/data'));

}).call(this);
