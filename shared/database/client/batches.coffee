_ = require 'lodash'
uuid = require 'uuid'

# TODO this should be converted to using the functions on crudmodule
module.exports = class Batches extends require '../scd'
  @::type = 'batch'
  @::table = 'batches'
  @::attachment_name = 'csv'

  get: (id, opts, cb) ->
    [cb, opts] = [opts, undefined] unless cb? and _.isFunction cb

    sql = @squel.select()
      .from "#{@table}_current"
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'doc'
      .field 'version'
      .field 'published'
      .where 'id = ?', id
      .limit 1

    if opts?.with_csv
      sql.field 'csv as file'

    @single sql, cb

  get2: (id, cb) ->
    sql = @squel.select()
      .from "#{@table}_current"
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'doc'
      .field 'version'
      .field 'published'
      .where 'id = ?', id
      .limit 1

    @single sql, cb

  save: (doc, cb) ->
    status = doc.status or 'pending'
    csv = doc.file or null
    doc = _.omit doc, ['file', 'status']

    if typeof csv isnt 'string' && csv isnt null
      csv = csv.toString()

    @before_save?.call @, doc
    json = JSON.stringify doc

    unless doc.id?
      sql = @squel.insert()
        .into @table
        .set 'doc', "$$#{json}$$", dontQuote: yes
        .set 'status', status
        .returning 'id, version'

      if @type?
        sql.set 'type', @type

      if csv?
        sql.set 'csv', csv

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id.replace(/-/g, ''), version: first.version
    else
      sql = @squel.update()
        .table @table
        .set 'doc', "$$#{json}$$", dontQuote: yes
        .set 'status', status
        .where 'id = ?', doc.id
        .where 'version = ?', doc.version
        .returning 'id, version'

      if csv?
        sql.set 'csv', csv

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]

        cb null, id: first.id, version: first.version

  clone: (id, cb) ->
    @get id, with_csv: yes, (err, batch) =>
      batch = _.omit batch, 'id', 'version', 'status', 'created', 'updated'
      batch.name += ' (clone)'

      @save batch, (err, body) ->
        cb err, _.assign batch, id: body.id, version: body.version

  _select_current: ->
    @squel.select()
      .from 'batches_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'doc'
      .field 'status'
      .where 'type = ?', @type

  recent: (cb) ->
    sql = @_select_current()
      .where 'status IN ?', ['failed', 'finished']

    @list sql, cb

  pending: (cb) ->
    sql = @_select_current()
      .where 'status = ?', 'pending'

    @list sql, cb

  scheduled: (cb) ->
    sql = @_select_current()
      .where 'status = ?', 'deployed'

    @list sql, cb

  finished: (cb) ->
    sql = @_select_current()
      .where 'status IN ?', ['finished', 'archived']

    @list sql, cb

  all: (cb) ->
    sql = @_select_current()
      .field 'csv'

    @list sql, cb

  delete: (id, cb) ->
    @get id, (err, obj) =>
      return cb err if err

      obj.status = 'deleted'
      @save obj, cb

  report: (id, cb) ->
    sql = @squel.select()
      .from 'batch_stats'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'queued'
      .field 'delivered'
      .field 'bounced'
      .where 'batch = ?', id

    @db.query sql, (err, result) ->
      return cb err if err

      cb null, result.rows

  summary: (query = {}, cb) ->
    @ctx.use('emails').batches query, (err, summary) =>
      ids = _.uniq _.pluck summary, (r) -> r.key[4]
      @get ids, (rows) ->
        cb null,
          batches: _.indexBy rows, 'id'
          summary: summary
