(function() {
  var Batches, _, uuid,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  uuid = require('uuid');

  module.exports = Batches = (function(superClass) {
    extend(Batches, superClass);

    function Batches() {
      return Batches.__super__.constructor.apply(this, arguments);
    }

    Batches.prototype.type = 'batch';

    Batches.prototype.table = 'batches';

    Batches.prototype.attachment_name = 'csv';

    Batches.prototype.get = function(id, opts, cb) {
      var ref, sql;
      if (!((cb != null) && _.isFunction(cb))) {
        ref = [opts, void 0], cb = ref[0], opts = ref[1];
      }
      sql = this.squel.select().from(this.table + "_current").field("REPLACE(id::text, '-', '')", 'id').field('doc').field('version').field('published').where('id = ?', id).limit(1);
      if (opts != null ? opts.with_csv : void 0) {
        sql.field('csv as file');
      }
      return this.single(sql, cb);
    };

    Batches.prototype.get2 = function(id, cb) {
      var sql;
      sql = this.squel.select().from(this.table + "_current").field("REPLACE(id::text, '-', '')", 'id').field('doc').field('version').field('published').where('id = ?', id).limit(1);
      return this.single(sql, cb);
    };

    Batches.prototype.save = function(doc, cb) {
      var csv, json, ref, sql, status;
      status = doc.status || 'pending';
      csv = doc.file || null;
      doc = _.omit(doc, ['file', 'status']);
      if (typeof csv !== 'string' && csv !== null) {
        csv = csv.toString();
      }
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      json = JSON.stringify(doc);
      if (doc.id == null) {
        sql = this.squel.insert().into(this.table).set('doc', "$$" + json + "$$", {
          dontQuote: true
        }).set('status', status).returning('id, version');
        if (this.type != null) {
          sql.set('type', this.type);
        }
        if (csv != null) {
          sql.set('csv', csv);
        }
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id.replace(/-/g, ''),
            version: first.version
          });
        });
      } else {
        sql = this.squel.update().table(this.table).set('doc', "$$" + json + "$$", {
          dontQuote: true
        }).set('status', status).where('id = ?', doc.id).where('version = ?', doc.version).returning('id, version');
        if (csv != null) {
          sql.set('csv', csv);
        }
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      }
    };

    Batches.prototype.clone = function(id, cb) {
      return this.get(id, {
        with_csv: true
      }, (function(_this) {
        return function(err, batch) {
          batch = _.omit(batch, 'id', 'version', 'status', 'created', 'updated');
          batch.name += ' (clone)';
          return _this.save(batch, function(err, body) {
            return cb(err, _.assign(batch, {
              id: body.id,
              version: body.version
            }));
          });
        };
      })(this));
    };

    Batches.prototype._select_current = function() {
      return this.squel.select().from('batches_current').field("REPLACE(id::text, '-', '')", 'id').field('version').field('doc').field('status').where('type = ?', this.type);
    };

    Batches.prototype.recent = function(cb) {
      var sql;
      sql = this._select_current().where('status IN ?', ['failed', 'finished']);
      return this.list(sql, cb);
    };

    Batches.prototype.pending = function(cb) {
      var sql;
      sql = this._select_current().where('status = ?', 'pending');
      return this.list(sql, cb);
    };

    Batches.prototype.scheduled = function(cb) {
      var sql;
      sql = this._select_current().where('status = ?', 'deployed');
      return this.list(sql, cb);
    };

    Batches.prototype.finished = function(cb) {
      var sql;
      sql = this._select_current().where('status IN ?', ['finished', 'archived']);
      return this.list(sql, cb);
    };

    Batches.prototype.all = function(cb) {
      var sql;
      sql = this._select_current().field('csv');
      return this.list(sql, cb);
    };

    Batches.prototype["delete"] = function(id, cb) {
      return this.get(id, (function(_this) {
        return function(err, obj) {
          if (err) {
            return cb(err);
          }
          obj.status = 'deleted';
          return _this.save(obj, cb);
        };
      })(this));
    };

    Batches.prototype.report = function(id, cb) {
      var sql;
      sql = this.squel.select().from('batch_stats').field("REPLACE(id::text, '-', '')", 'id').field('queued').field('delivered').field('bounced').where('batch = ?', id);
      return this.db.query(sql, function(err, result) {
        if (err) {
          return cb(err);
        }
        return cb(null, result.rows);
      });
    };

    Batches.prototype.summary = function(query, cb) {
      if (query == null) {
        query = {};
      }
      return this.ctx.use('emails').batches(query, (function(_this) {
        return function(err, summary) {
          var ids;
          ids = _.uniq(_.pluck(summary, function(r) {
            return r.key[4];
          }));
          return _this.get(ids, function(rows) {
            return cb(null, {
              batches: _.indexBy(rows, 'id'),
              summary: summary
            });
          });
        };
      })(this));
    };

    return Batches;

  })(require('../scd'));

}).call(this);
