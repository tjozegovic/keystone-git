_ = require 'lodash'

module.exports = class ClientDataCollection extends require '../../scd'
  @::table = 'data'

  constructor: (@db, @ctx) ->
    super

    Object.defineProperty @, 'attachment',
      get: ->
        get: (id, name, cb) =>
          sql = @squel.select()
            .from 'data_current_parts'
            .field 'value'
            .where 'id = ? AND name = ?', id, name
            .limit 1

          @db.query sql, (err, results) ->
            return cb err if err?
            cb null, results.rows[0]?.value or null

        getversion: (id, version, name, cb) =>
          sql = @squel.select()
            .from 'data_parts'
            .field 'value'
            .where 'id = ? AND version = ? AND name = ?', id, version, name
            .limit 1

          @db.query sql, (err, results) ->
            return cb err if err?
            cb null, results.rows[0]?.value or null

        save: (id, name, value, cb) =>
          @db.query """
INSERT INTO data_parts (id, version, name, value)
  SELECT id, version, $2, $3
  FROM data
  WHERE id = $1 AND upper(valid) = 'infinity';"""
          , [id, name, value], (err) ->
            return cb err if err
            cb null, id: id
