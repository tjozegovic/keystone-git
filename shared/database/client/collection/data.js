(function() {
  var ClientDataCollection, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = ClientDataCollection = (function(superClass) {
    extend(ClientDataCollection, superClass);

    ClientDataCollection.prototype.table = 'data';

    function ClientDataCollection(db, ctx) {
      this.db = db;
      this.ctx = ctx;
      ClientDataCollection.__super__.constructor.apply(this, arguments);
      Object.defineProperty(this, 'attachment', {
        get: function() {
          return {
            get: (function(_this) {
              return function(id, name, cb) {
                var sql;
                sql = _this.squel.select().from('data_current_parts').field('value').where('id = ? AND name = ?', id, name).limit(1);
                return _this.db.query(sql, function(err, results) {
                  var ref;
                  if (err != null) {
                    return cb(err);
                  }
                  return cb(null, ((ref = results.rows[0]) != null ? ref.value : void 0) || null);
                });
              };
            })(this),
            getversion: (function(_this) {
              return function(id, version, name, cb) {
                var sql;
                sql = _this.squel.select().from('data_parts').field('value').where('id = ? AND version = ? AND name = ?', id, version, name).limit(1);
                return _this.db.query(sql, function(err, results) {
                  var ref;
                  if (err != null) {
                    return cb(err);
                  }
                  return cb(null, ((ref = results.rows[0]) != null ? ref.value : void 0) || null);
                });
              };
            })(this),
            save: (function(_this) {
              return function(id, name, value, cb) {
                return _this.db.query("INSERT INTO data_parts (id, version, name, value)\n  SELECT id, version, $2, $3\n  FROM data\n  WHERE id = $1 AND upper(valid) = 'infinity';", [id, name, value], function(err) {
                  if (err) {
                    return cb(err);
                  }
                  return cb(null, {
                    id: id
                  });
                });
              };
            })(this)
          };
        }
      });
    }

    return ClientDataCollection;

  })(require('../../scd'));

}).call(this);
