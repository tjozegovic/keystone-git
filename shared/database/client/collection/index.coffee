_ = require 'lodash'

module.exports = class ClientCollection extends require '../../pgmodule'
  constructor: (@db, @ctx) ->
    super

    Object.defineProperty @, 'attachment',
      get: ->
        get: (id, name, cb) =>
          sql = @squel.select()
            .from 'data_parts'
            .field 'value'
            .where 'data_id = ? and version = ? and name = ?', id, -1, name
            .limit 1

          @db.query sql, (err, results) ->
            return cb err if err?
            cb null, results.rows[0]?.value or null

        save: (id, version, name, value, cb) =>
          @db.multiple (multi) ->
            multi.query 'BEGIN'
            multi.query """
UPDATE data_parts SET
  value = $4
WHERE data_id = $1 AND version = $2 AND name = $3""", [id, version, name, value]
            multi.query """
INSERT INTO data_parts (id, version, name, value)
  SELECT $1, -1, $2, $3
  WHERE NOT EXISTS (SELECT 1 FROM data_parts WHERE id = $1 AND version = -1 AND name = $2);""", [id, name, value]
            multi.query 'COMMIT', (err) ->
              multi.close err
              return cb err if err
              cb null, id: id

  getversion: (id, valid, cb) ->
    sql = @squel.select()
      .from 'data'
      .field 'doc'
      .where 'data_id = ?', id
      .where 'type = ?', @type
      .where "valid @> '#{valid}'::timestamptz"
      .limit 1

    @single sql, cb

  get: (id, cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field 'doc'
      .where 'data_id = ?', id
      .where 'type = ?', @type
      .limit 1

    @single sql, cb

  fetch: (ids, cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field 'id'
      .field 'doc'
      .where "id IN ('#{ids.join '\', \''}')"

    @list sql, cb

  active: (cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field 'id'
      .field 'doc'
      .where 'type = ?', @type
      .where "doc @> $${\"active\": true}$$"

    @list sql, cb

  all: (cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field 'id'
      .field 'doc'
      .where 'type = ?', @type

    @list sql, cb

  save: (doc, cb) ->
    @before_save?.call @, doc
    json = JSON.stringify doc

    unless doc.id?
      sql = @squel.insert()
        .into 'data'
        .set 'data_id', doc.id
        .set 'type', @type
        .set 'doc', "$$#{json}$$", dontQuote: yes
        .returning 'data_id, LOWER(valid) AS version'

      @db.query sql, (err, result) ->
        return cb err if err
        cb null, id: result.data_id, version: result.version
    else
      sql = @squel.update()
        .table 'data'
        .set 'doc', "$$#{json}$$", dontQuote: yes
        .where 'data_id = ?', doc.id
        .where 'LOWER(valid) = ?', doc.version
        .returning 'data_id, LOWER(valid) AS version'

      @db.query sql, (err, result) ->
        return cb err if err
        cb null, id: result.data_id, version: result.version

  update: (id, doc, cb) ->
    doc.id = id
    @save doc, cb

  delete: (id, cb) ->
    @get id, (err, obj) =>
      return cb err if err

      obj.active = no
      @save obj, cb
