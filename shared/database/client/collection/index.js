(function() {
  var ClientCollection, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = ClientCollection = (function(superClass) {
    extend(ClientCollection, superClass);

    function ClientCollection(db, ctx) {
      this.db = db;
      this.ctx = ctx;
      ClientCollection.__super__.constructor.apply(this, arguments);
      Object.defineProperty(this, 'attachment', {
        get: function() {
          return {
            get: (function(_this) {
              return function(id, name, cb) {
                var sql;
                sql = _this.squel.select().from('data_parts').field('value').where('data_id = ? and version = ? and name = ?', id, -1, name).limit(1);
                return _this.db.query(sql, function(err, results) {
                  var ref;
                  if (err != null) {
                    return cb(err);
                  }
                  return cb(null, ((ref = results.rows[0]) != null ? ref.value : void 0) || null);
                });
              };
            })(this),
            save: (function(_this) {
              return function(id, version, name, value, cb) {
                return _this.db.multiple(function(multi) {
                  multi.query('BEGIN');
                  multi.query("UPDATE data_parts SET\n  value = $4\nWHERE data_id = $1 AND version = $2 AND name = $3", [id, version, name, value]);
                  multi.query("INSERT INTO data_parts (id, version, name, value)\n  SELECT $1, -1, $2, $3\n  WHERE NOT EXISTS (SELECT 1 FROM data_parts WHERE id = $1 AND version = -1 AND name = $2);", [id, name, value]);
                  return multi.query('COMMIT', function(err) {
                    multi.close(err);
                    if (err) {
                      return cb(err);
                    }
                    return cb(null, {
                      id: id
                    });
                  });
                });
              };
            })(this)
          };
        }
      });
    }

    ClientCollection.prototype.getversion = function(id, valid, cb) {
      var sql;
      sql = this.squel.select().from('data').field('doc').where('data_id = ?', id).where('type = ?', this.type).where("valid @> '" + valid + "'::timestamptz").limit(1);
      return this.single(sql, cb);
    };

    ClientCollection.prototype.get = function(id, cb) {
      var sql;
      sql = this.squel.select().from('data_current').field('doc').where('data_id = ?', id).where('type = ?', this.type).limit(1);
      return this.single(sql, cb);
    };

    ClientCollection.prototype.fetch = function(ids, cb) {
      var sql;
      sql = this.squel.select().from('data_current').field('id').field('doc').where("id IN ('" + (ids.join('\', \'')) + "')");
      return this.list(sql, cb);
    };

    ClientCollection.prototype.active = function(cb) {
      var sql;
      sql = this.squel.select().from('data_current').field('id').field('doc').where('type = ?', this.type).where("doc @> $${\"active\": true}$$");
      return this.list(sql, cb);
    };

    ClientCollection.prototype.all = function(cb) {
      var sql;
      sql = this.squel.select().from('data_current').field('id').field('doc').where('type = ?', this.type);
      return this.list(sql, cb);
    };

    ClientCollection.prototype.save = function(doc, cb) {
      var json, ref, sql;
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      json = JSON.stringify(doc);
      if (doc.id == null) {
        sql = this.squel.insert().into('data').set('data_id', doc.id).set('type', this.type).set('doc', "$$" + json + "$$", {
          dontQuote: true
        }).returning('data_id, LOWER(valid) AS version');
        return this.db.query(sql, function(err, result) {
          if (err) {
            return cb(err);
          }
          return cb(null, {
            id: result.data_id,
            version: result.version
          });
        });
      } else {
        sql = this.squel.update().table('data').set('doc', "$$" + json + "$$", {
          dontQuote: true
        }).where('data_id = ?', doc.id).where('LOWER(valid) = ?', doc.version).returning('data_id, LOWER(valid) AS version');
        return this.db.query(sql, function(err, result) {
          if (err) {
            return cb(err);
          }
          return cb(null, {
            id: result.data_id,
            version: result.version
          });
        });
      }
    };

    ClientCollection.prototype.update = function(id, doc, cb) {
      doc.id = id;
      return this.save(doc, cb);
    };

    ClientCollection.prototype["delete"] = function(id, cb) {
      return this.get(id, (function(_this) {
        return function(err, obj) {
          if (err) {
            return cb(err);
          }
          obj.active = false;
          return _this.save(obj, cb);
        };
      })(this));
    };

    return ClientCollection;

  })(require('../../pgmodule'));

}).call(this);
