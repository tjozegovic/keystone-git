_ = require 'lodash'
async = require 'async'
moment = require 'moment'
{filter} = require 'shared/util'

ID_RE = /^[A-Za-z0-9]{32}$/

module.exports = class Emails extends require '../pgmodule'
  constructor: (@db, {@client}) ->
    super

  fetch: (ids, cb) ->
    return cb null, [] unless ids.length
    guidIds = []
    newIds = []

    _.forEach ids, (id) ->
      if ID_RE.test id
        guidIds.push id
      else
        newIds.push id

    if guidIds.length
      sql = """
        SELECT *
        FROM emails
        LEFT JOIN emails_legacyids el ON el.newid = id
        WHERE id IN ('#{newIds.join '\', \''}')
          OR oldid IN ('#{guidIds.join '\', \''}')
      """
    else
      sql = @squel.select()
        .from 'emails'
        .field 'id'
        .field 'type'
        .field 'doc'
        .where "id IN ('#{ids.join '\', \''}')"

    @list sql, cb

  get: (id, opts, cb) ->
    [cb, opts] = [opts, {}] unless _.isFunction cb

    if ID_RE.test id
      sql = @squel.select()
        .from 'emails_legacyids'
        .field 'newid'
        .where 'oldid = ?', id

      @single sql, (err, first) =>
        return cb err if err?
        return cb null, null unless first

        @get first?.newid, cb
    else
      sql = @squel.select()
        .from 'emails'
        .field 'id'
        .field 'type'
        .field 'doc'
        .where 'id = ?', id
        .limit 1

      unless opts.onlymeta
        sql
          .field 'html'
          .field 'text'

      @single sql, (err, first) ->
        return cb err if err?
        return cb null, null unless first?

        _.assign first.doc.mail, _.pick first, 'html', 'text'
        return cb null, _.defaults _.omit(first, 'doc', 'html', 'text'), first.doc

  save: (doc, cb) ->
    _orig = _.cloneDeep doc

    doc.created ?= new Date()
    doc.updated = new Date()

    {type} = doc
    doc = _.omit doc, 'type'
    {html, text} = doc.mail or {}
    doc.mail = _.omit doc.mail, 'html', 'text'

    unless doc.id?
      sql = @squel.insert()
        .into 'emails'
        .set 'type', type or 'drip'
        .set 'doc', @jsonb doc
        .returning 'id'

      sql.set 'html', html if html?
      sql.set 'text', text if text?

      @db.query sql, (err, result) =>
        if err?.code is '23505'
          # TODO try again... limit retries?
          # can retries be handled in the trigger? i don't think they can be
          # a big problem has to do with using default ids, maybe generate the id?
          return setTimeout (=> @save _orig, cb), 1

        return cb err if err?
        cb null, id: result.rows[0].id
    else
      sql = @squel.update()
        .table 'emails'
        .set 'doc', @jsonb doc
        .where 'id = ?', doc.id

      sql.set 'html', html if html?
      sql.set 'text', text if text?

      @db.query sql, (err, result) ->
        return cb err if err
        cb null, id: doc.id

  _transformer: (whitelist) ->
    (row) ->
      obj = _.defaults _.omit(row, 'doc', 'html', 'text'), row.doc
      return obj unless whitelist?
      filter obj, whitelist

  list: (sql, cb) ->
    @db.query sql, (err, results) =>
      return cb err if err?
      cb null, _.map results.rows, @_transformer()

  by_lead: (id, cb) ->
    # TODO this needs to be optimized in a lookup table
    sql = @squel.select()
      .from 'emails'
      .field 'id'
      .field 'type'
      .field 'doc'

      # TODO these filters are a bit ugly, left over though from a previous couchdb index
      .where 'type IN ?', ['drip', 'direct', 'batch']
      .where "doc @> '#{JSON.stringify lead: id: id}'::jsonb"
      .order 'id'

    @list sql, cb

  by_batch_errors: (id, cb) ->
    # TODO this needs to be optimized in a lookup table
    sql = @squel.select()
      .from 'emails'
      .field 'id'
      .field 'type'
      .field 'doc'

      # TODO these filters are a bit ugly, left over though from a previous couchdb index
      .where 'type IN ?', ['batch']
      .where "doc @> '#{JSON.stringify batch: id}'::jsonb"
      .where "doc ? 'errored'"
      .order 'id'

    @list sql, cb

  by_recipient_email: (email, cb) ->
    # TODO this needs to be optimized in a lookup table
    sql = @squel.select()
      .from 'emails'
      .field 'id'
      .field 'type'
      .field 'doc'

      # TODO these filters are a bit ugly, left over though from a previous couchdb index
      .where 'type IN ?', ['drip', 'direct', 'batch']
      .where "doc @> '#{JSON.stringify recipient: email: email}'::jsonb"
      .order 'id'

    @list sql, cb

  by_sender_email: (email, cb) ->
    # TODO this needs to be optimized in a lookup table
    sql = @squel.select()
      .from 'emails'
      .field 'id'
      .field 'type'
      .field 'doc'

      # TODO these filters are a bit ugly, left over though from a previous couchdb index
      .where 'type IN ?', ['drip', 'direct', 'batch']
      .where "doc @> '#{JSON.stringify sender: email: email}'::jsonb"
      .order 'id'

    @list sql, cb

  query: (query, cb) ->
    sql = @squel.select()
      .from 'emails'
      .where 'id BETWEEN get_epoch_from_ts(?) and get_epoch_from_ts(?)', _.values(_.pick query, 'from', 'to')...

      .where "doc ? 'queued'"

    if query.type
      sql.where 'type = ?', query.type
    else
      sql.where 'type IN ?', ['drip', 'direct', 'batch']

    if query.sampleless
      sql.where "NOT (doc ? 'sample')"

    if query.status? then sql.where "COALESCE(doc->'status'->>'id', doc->'status'->>'_id') = ?", query.status
    if query.segment? then sql.where "(doc->'segment'->>'id') = ?", query.segment
    if query.step? then sql.where "(doc->'step'->>'id') = ?", query.step
    if query.batch? then sql.where "COALESCE(doc->'batch'->>'id', doc->'batch'->>'_id') = ?", query.batch

    if typeof query.messages is 'string'
      sql.where "(doc->'message'->>'name') = ?", query.messages
    else if query.messages?.length > 1
      sql.where "(doc->'message'->>'name') IN ?", query.messages

    count = sql.clone()

    sql
      .field 'id'
      .field 'type'
      .field 'doc'
      .order 'id', no

    if query.skip? then sql.offset query.skip
    if query.size? then sql.limit query.size
    if query.after? then sql.where 'id < ?', query.after

    count.field 'count(*)'

    @single count, (err, results) =>
      return cb err if err?
      return cb null, null, results.count if query.count_only

      @db.stream sql, (err, stream) =>
        return cb err if err?

        cb null, stream.pipe(@stream.transform @_transformer query.whitelist), results.count

  _pick_events = (named, transform) ->
    (query = {}, cb) ->
      # TODO need to confirm working
      query.sampleless = yes

      @query query, (err, stream) =>
        return cb err if err

        skip = @stream.filter (row) ->
          _.size(row[named]) > 0

        edit = @stream.transform (row) ->
          _.omit row, 'assets', 'unsub', 'bug', 'html', 'text'

          evts = row[named]
          evts = [_.first row[named]] if query.unique
          _.map evts, (event, idx) ->
            obj =
              at: event.at or event
              first: idx is 0
              email: row
            _.defaults obj, if _.isObject event then event else {}

        stream = stream.pipe(skip).pipe(edit)
        stream = stream.pipe(@stream.transform transform) if transform?

        cb null, stream

  clicks: _pick_events 'clicked', (event) ->
    # there are two lookups here based on when link events were generated and how
    event.link = _.find event.email.links, id: event.link.id
    event.link ?= _.find event.email.links, id: event.link

    # click events that are published have a redirect property, mimic that here
    event.redirect = event.link.original_url

    event

  opens: _pick_events 'opened'
  optouts: _pick_events 'optout'
