(function() {
  var Emails, ID_RE, _, async, filter, moment,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty,
    slice = [].slice;

  _ = require('lodash');

  async = require('async');

  moment = require('moment');

  filter = require('shared/util').filter;

  ID_RE = /^[A-Za-z0-9]{32}$/;

  module.exports = Emails = (function(superClass) {
    var _pick_events;

    extend(Emails, superClass);

    function Emails(db, arg) {
      this.db = db;
      this.client = arg.client;
      Emails.__super__.constructor.apply(this, arguments);
    }

    Emails.prototype.fetch = function(ids, cb) {
      var guidIds, newIds, sql;
      if (!ids.length) {
        return cb(null, []);
      }
      guidIds = [];
      newIds = [];
      _.forEach(ids, function(id) {
        if (ID_RE.test(id)) {
          return guidIds.push(id);
        } else {
          return newIds.push(id);
        }
      });
      if (guidIds.length) {
        sql = "SELECT *\nFROM emails\nLEFT JOIN emails_legacyids el ON el.newid = id\nWHERE id IN ('" + (newIds.join('\', \'')) + "')\n  OR oldid IN ('" + (guidIds.join('\', \'')) + "')";
      } else {
        sql = this.squel.select().from('emails').field('id').field('type').field('doc').where("id IN ('" + (ids.join('\', \'')) + "')");
      }
      return this.list(sql, cb);
    };

    Emails.prototype.get = function(id, opts, cb) {
      var ref, sql;
      if (!_.isFunction(cb)) {
        ref = [opts, {}], cb = ref[0], opts = ref[1];
      }
      if (ID_RE.test(id)) {
        sql = this.squel.select().from('emails_legacyids').field('newid').where('oldid = ?', id);
        return this.single(sql, (function(_this) {
          return function(err, first) {
            if (err != null) {
              return cb(err);
            }
            if (!first) {
              return cb(null, null);
            }
            return _this.get(first != null ? first.newid : void 0, cb);
          };
        })(this));
      } else {
        sql = this.squel.select().from('emails').field('id').field('type').field('doc').where('id = ?', id).limit(1);
        if (!opts.onlymeta) {
          sql.field('html').field('text');
        }
        return this.single(sql, function(err, first) {
          if (err != null) {
            return cb(err);
          }
          if (first == null) {
            return cb(null, null);
          }
          _.assign(first.doc.mail, _.pick(first, 'html', 'text'));
          return cb(null, _.defaults(_.omit(first, 'doc', 'html', 'text'), first.doc));
        });
      }
    };

    Emails.prototype.save = function(doc, cb) {
      var _orig, html, ref, sql, text, type;
      _orig = _.cloneDeep(doc);
      if (doc.created == null) {
        doc.created = new Date();
      }
      doc.updated = new Date();
      type = doc.type;
      doc = _.omit(doc, 'type');
      ref = doc.mail || {}, html = ref.html, text = ref.text;
      doc.mail = _.omit(doc.mail, 'html', 'text');
      if (doc.id == null) {
        sql = this.squel.insert().into('emails').set('type', type || 'drip').set('doc', this.jsonb(doc)).returning('id');
        if (html != null) {
          sql.set('html', html);
        }
        if (text != null) {
          sql.set('text', text);
        }
        return this.db.query(sql, (function(_this) {
          return function(err, result) {
            if ((err != null ? err.code : void 0) === '23505') {
              return setTimeout((function() {
                return _this.save(_orig, cb);
              }), 1);
            }
            if (err != null) {
              return cb(err);
            }
            return cb(null, {
              id: result.rows[0].id
            });
          };
        })(this));
      } else {
        sql = this.squel.update().table('emails').set('doc', this.jsonb(doc)).where('id = ?', doc.id);
        if (html != null) {
          sql.set('html', html);
        }
        if (text != null) {
          sql.set('text', text);
        }
        return this.db.query(sql, function(err, result) {
          if (err) {
            return cb(err);
          }
          return cb(null, {
            id: doc.id
          });
        });
      }
    };

    Emails.prototype._transformer = function(whitelist) {
      return function(row) {
        var obj;
        obj = _.defaults(_.omit(row, 'doc', 'html', 'text'), row.doc);
        if (whitelist == null) {
          return obj;
        }
        return filter(obj, whitelist);
      };
    };

    Emails.prototype.list = function(sql, cb) {
      return this.db.query(sql, (function(_this) {
        return function(err, results) {
          if (err != null) {
            return cb(err);
          }
          return cb(null, _.map(results.rows, _this._transformer()));
        };
      })(this));
    };

    Emails.prototype.by_lead = function(id, cb) {
      var sql;
      sql = this.squel.select().from('emails').field('id').field('type').field('doc').where('type IN ?', ['drip', 'direct', 'batch']).where("doc @> '" + (JSON.stringify({
        lead: {
          id: id
        }
      })) + "'::jsonb").order('id');
      return this.list(sql, cb);
    };

    Emails.prototype.by_batch_errors = function(id, cb) {
      var sql;
      sql = this.squel.select().from('emails').field('id').field('type').field('doc').where('type IN ?', ['batch']).where("doc @> '" + (JSON.stringify({
        batch: id
      })) + "'::jsonb").where("doc ? 'errored'").order('id');
      return this.list(sql, cb);
    };

    Emails.prototype.by_recipient_email = function(email, cb) {
      var sql;
      sql = this.squel.select().from('emails').field('id').field('type').field('doc').where('type IN ?', ['drip', 'direct', 'batch']).where("doc @> '" + (JSON.stringify({
        recipient: {
          email: email
        }
      })) + "'::jsonb").order('id');
      return this.list(sql, cb);
    };

    Emails.prototype.by_sender_email = function(email, cb) {
      var sql;
      sql = this.squel.select().from('emails').field('id').field('type').field('doc').where('type IN ?', ['drip', 'direct', 'batch']).where("doc @> '" + (JSON.stringify({
        sender: {
          email: email
        }
      })) + "'::jsonb").order('id');
      return this.list(sql, cb);
    };

    Emails.prototype.query = function(query, cb) {
      var count, ref, ref1, sql;
      sql = (ref = this.squel.select().from('emails')).where.apply(ref, ['id BETWEEN get_epoch_from_ts(?) and get_epoch_from_ts(?)'].concat(slice.call(_.values(_.pick(query, 'from', 'to'))))).where("doc ? 'queued'");
      if (query.type) {
        sql.where('type = ?', query.type);
      } else {
        sql.where('type IN ?', ['drip', 'direct', 'batch']);
      }
      if (query.sampleless) {
        sql.where("NOT (doc ? 'sample')");
      }
      if (query.status != null) {
        sql.where("COALESCE(doc->'status'->>'id', doc->'status'->>'_id') = ?", query.status);
      }
      if (query.segment != null) {
        sql.where("(doc->'segment'->>'id') = ?", query.segment);
      }
      if (query.step != null) {
        sql.where("(doc->'step'->>'id') = ?", query.step);
      }
      if (query.batch != null) {
        sql.where("COALESCE(doc->'batch'->>'id', doc->'batch'->>'_id') = ?", query.batch);
      }
      if (typeof query.messages === 'string') {
        sql.where("(doc->'message'->>'name') = ?", query.messages);
      } else if (((ref1 = query.messages) != null ? ref1.length : void 0) > 1) {
        sql.where("(doc->'message'->>'name') IN ?", query.messages);
      }
      count = sql.clone();
      sql.field('id').field('type').field('doc').order('id', false);
      if (query.skip != null) {
        sql.offset(query.skip);
      }
      if (query.size != null) {
        sql.limit(query.size);
      }
      if (query.after != null) {
        sql.where('id < ?', query.after);
      }
      count.field('count(*)');
      return this.single(count, (function(_this) {
        return function(err, results) {
          if (err != null) {
            return cb(err);
          }
          if (query.count_only) {
            return cb(null, null, results.count);
          }
          return _this.db.stream(sql, function(err, stream) {
            if (err != null) {
              return cb(err);
            }
            return cb(null, stream.pipe(_this.stream.transform(_this._transformer(query.whitelist))), results.count);
          });
        };
      })(this));
    };

    _pick_events = function(named, transform) {
      return function(query, cb) {
        if (query == null) {
          query = {};
        }
        query.sampleless = true;
        return this.query(query, (function(_this) {
          return function(err, stream) {
            var edit, skip;
            if (err) {
              return cb(err);
            }
            skip = _this.stream.filter(function(row) {
              return _.size(row[named]) > 0;
            });
            edit = _this.stream.transform(function(row) {
              var evts;
              _.omit(row, 'assets', 'unsub', 'bug', 'html', 'text');
              evts = row[named];
              if (query.unique) {
                evts = [_.first(row[named])];
              }
              return _.map(evts, function(event, idx) {
                var obj;
                obj = {
                  at: event.at || event,
                  first: idx === 0,
                  email: row
                };
                return _.defaults(obj, _.isObject(event) ? event : {});
              });
            });
            stream = stream.pipe(skip).pipe(edit);
            if (transform != null) {
              stream = stream.pipe(_this.stream.transform(transform));
            }
            return cb(null, stream);
          };
        })(this));
      };
    };

    Emails.prototype.clicks = _pick_events('clicked', function(event) {
      event.link = _.find(event.email.links, {
        id: event.link.id
      });
      if (event.link == null) {
        event.link = _.find(event.email.links, {
          id: event.link
        });
      }
      event.redirect = event.link.original_url;
      return event;
    });

    Emails.prototype.opens = _pick_events('opened');

    Emails.prototype.optouts = _pick_events('optout');

    return Emails;

  })(require('../pgmodule'));

}).call(this);
