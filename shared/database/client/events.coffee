_ = require 'lodash'
async = require 'async'
moment = require 'moment'

EPOCH = moment.utc('2010-01-01').valueOf()

module.exports = class Events extends require '../pgmodule'
  get: (id, cb) ->
    sql = @squel.select()
      .from 'events'
      .field 'doc'
      .where 'id = ?', id

    @db.query sql, (err, results) ->
      return cb err if err?
      cb null, results.rows[0].doc

  save: (event, cb) ->
    sql = @squel.insert()
      .into 'events'
      # .set 'client_id', event.client
      .set 'type', event.type
      .set 'doc', "$$#{JSON.stringify event}$$", dontQuote: yes
      .returning 'id'

    @db.query sql, (err, results) ->
      return cb err if err?
      cb null, id: results.rows[0].id

  leadinfo: (id, cb) ->
    sql = @squel.select()
      .from 'events'
      .field 'doc'
      .field 'id'
      .field 'type'
      .field 'at'
      .where "doc->>'lead' = ?", id
      .where 'type IN ?', ['lead.created', 'lead.segmented', 'lead.status.changed', 'lead.timedout', 'lead.updated']

    @doc_list sql, cb

  oftype: (type, query, cb) ->
    query.descending ?= yes

    sql = @squel.select()
      .from 'events', 'ev'
      .join 'emails', 'em',
        '(case when (ev.doc->>\'email\') ~ E\'^\\\\d+$\' then (ev.doc->>\'email\')::bigint else 0 end = em.id)'
      .field 'ev.doc', 'event'
      .field 'em.id', 'id'
      .field 'em.type', 'type'
      .field 'em.doc', 'email'
      .where 'ev.type = ?', type

      # TODO these filters are a bit ugly, left over though from a previous couchdb index
      .where 'em.type IN ?', ['drip', 'direct', 'batch']
      .where 'NOT em.doc ? \'canceled\''

    if query.from? and query.to?
      sql.where 'ev.at BETWEEN ? and ?', query.from, query.to

    # TODO batches
    sql.where "(em.doc->'status'->>'_id') = ?", query.status if query.status?
    sql.where "(em.doc->'segment'->>'id') = ?", query.segment if query.segment?
    sql.where "(em.doc->'step'->>'id') = ?", query.step if query.step?
    sql.where "COALESCE((em.doc->'batch'->>'_id'), (em.doc->>'batch')) = ?", query.batch if query.batch?

    sql.offset query.skip if query.skip?
    sql.limit query.size if query.size?
    sql.limit query.limit if query.limit?

    sql.order 'ev.at', not query.descending

    if query.fields?
      query.fields = (path: x, fn: _.property(x) for x in JSON.parse query.fields)

    @db.stream sql, (err, stream) =>
      return cb err if err?
      cb null, stream.pipe @stream.transform (row) ->
        _.assign row.event, email: _.assign row.email, _.pick row, 'id', 'type'

      #   row.event.email = row.email

      #   return row.event unless query.fields?
      #   field.fn row.event for field in query.fields

  notifications: (to, email, cb) ->
    sql = @squel.select()
      .from 'events'
      .field 'doc'
      .where 'type IN ?', ['notification.sent', 'vunotification.queued']
      .where "doc->>'to' = ? OR doc->>'agent' = ?", to, to
      .where "doc->>'email' = ?", email
      .where 'at > ?', moment.utc().subtract(48, 'hours').toISOString()
      .order 'at', no
      .limit 1

    @doc_list sql, cb

  summary: (opts = {}, cb) ->
    cb new Error 'deprecated: see emails/summary'

  errors: (query, cb) ->
    sql = @squel.select()
      .from 'events'
      .field 'doc'
      .where "type IN ('api.errored', 'email.errored')"
      .where 'at BETWEEN ? and ?', query.from, query.to
      .order 'at', not query.descending

    sql.limit query.size if query.size?
    sql.offset query.skip if query.skip?

    @doc_list sql, cb

  # TODO this is very short term, there should probably be a tracked
  # pending messages table which can be inserted and deleted on the proper messages
  'email.pending': (opts, cb) ->
    @oftype 'email.queued', opts, cb

  'email.delivered': (opts, cb) ->
    @oftype 'email.delivered', opts, cb

  'email.clicked': (opts, cb) ->
    @oftype 'email.clicked', opts, cb

  'email.opened': (opts, cb) ->
    @oftype 'email.opened', opts, cb

  'email.bounced': (opts, cb) ->
    @oftype 'email.bounced', opts, cb

  sends: (status, cb) ->
    [tomorrow, thirtyDaysAgo] = [new Date, new Date]
    tomorrow.setDate tomorrow.getDate() + 1
    thirtyDaysAgo.setDate thirtyDaysAgo.getDate() - 30
    [start, end] = [[status].concat(_asDateKey thirtyDaysAgo), [status].concat(_asDateKey tomorrow)]

    opts =_makeReduceViewOptions(start, end, 4)
    @db.view 'emails', 'by_date', opts, _viewResultsAsObject _getDateFromKey, (err, obj) ->
      while thirtyDaysAgo < tomorrow
        obj[_parseDateKey _asDateKey thirtyDaysAgo] ?= 0
        thirtyDaysAgo.setDate thirtyDaysAgo.getDate() + 1
      cb null, obj

  _asDateKey: _asDateKey = (date) -> [date.getFullYear(), date.getMonth() + 1, date.getDate()]
  _getDateFromKey: _getDateFromKey = (key) -> _parseDateKey key.slice(1)
  _parseDateKey: _parseDateKey = (dateKey) -> dateKey.join '-'

  dashboard: (cb) ->
    start_of_month = moment.utc().startOf 'month'

    counts =
      'email.delivered': 0
      'email.errored': 0
      'api.errored': 0
      'optout': 0

    sql = @squel.select()
      .from 'events'
      .field 'type'
      .field 'COUNT(id)::int', 'count' # do some type coercion? node-pg turns bigints into strings
      .where 'at > ?', start_of_month.format()
      .where "type IN ('#{_.keys(counts).join '\', \''}')"
      .group 'type'

    @db.query sql, (err, results) ->
      return cb err if err?

      results.rows.forEach (row) ->
        counts[row.type] = row.count

      cb null, counts

  performance: (opts, cb) ->
    [today, monthago] = [moment().subtract(1, 'day'), moment().subtract(1, 'months')]
    types = ['email.clicked', 'email.delivered', 'email.errored', 'email.opened', 'email.queued', 'optout']

    sql = @squel.select()
      .from 'events'
      .field "doc->>'status'", 'status'
      .field "doc->>'segment'", 'segment'
      .field "COALESCE(doc->'step'->>'_id', doc->>'step')", 'step'
      .field "sum(case when type = 'email.queued' then 1 else 0 end)::int", 'sends'
      .field "sum(case when type = 'email.delivered' then 1 else 0 end)::int", 'delivers'
      .field "sum(case when type = 'email.opened' then 1 else 0 end)::int", 'opens'
      .field "sum(case when type = 'email.clicked' then 1 else 0 end)::int", 'clicks'
      .field "sum(case when doc ? 'first' AND type = 'optout' then 1 else 0 end)::int", 'optouts'
      .field "sum(case when doc ? 'first' AND type = 'email.opened' then 1 else 0 end)::int", 'firstopens'
      .field "sum(case when doc ? 'first' AND type = 'email.clicked' then 1 else 0 end)::int", 'firstclicks'

      .where 'at BETWEEN ? and ?', monthago.toISOString(), today.toISOString()
      .where 'type IN ?', types
      .where "NOT doc ? 'canceled' AND NOT doc ? 'sample'"
      .where "doc ? 'status' AND doc ? 'segment' AND doc ? 'step'"

      .group "doc->>'status'"
      .group "doc->>'segment'"
      .group "COALESCE(doc->'step'->>'_id', doc->>'step')"

    return @db.query sql, (err, results) ->
      return cb? err if err?

      rows = results.rows.map (row) ->
        {status, segment, step} = row
        stats = delivers: row.delivers, opens: row.firstopens, clicks: row.firstclicks, rate: row.clicks / row.delivers
        key: "#{status}/#{segment}/#{step}", status: status, segment: segment, step: step, stats: stats

      async.filter rows, ({stats}, cb) ->
        cb stats.delivers > 10
      , (results) ->
        async.sortBy results, ({stats}, cb) ->
          cb null, -1 * stats.rate
        , (err, sorted) ->
          limit = parseInt opts.limit or 3, 10
          fn = _[if opts.type is 'worst' then 'takeRight' else 'take']
          cb err, fn sorted, limit

  _getTypeFromKey: _getTypeFromKey = (key) -> key[0]

  _makeReduceViewOptions: _makeReduceViewOptions = (start, end, level) ->
    reduce: yes
    startkey: start
    endkey: end
    group_level: level

  _viewResultsAsObject: _viewResultsAsObject = (args...) -> (err, body = {}) ->
    [cb, keyPluck, valuePluck] = [args.pop(), args.shift(), args.shift()]

    eachKey = (row) -> keyPluck?(row.key) || row.key
    eachValue = (row) -> valuePluck?(row.value) || row.value

    body.rows ?= []
    cb err, _.object _.pluck(body.rows, eachKey), _.pluck(body.rows, eachValue)
