(function() {
  var EPOCH, Events, _, async, moment,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty,
    slice = [].slice;

  _ = require('lodash');

  async = require('async');

  moment = require('moment');

  EPOCH = moment.utc('2010-01-01').valueOf();

  module.exports = Events = (function(superClass) {
    var _asDateKey, _getDateFromKey, _getTypeFromKey, _makeReduceViewOptions, _parseDateKey, _viewResultsAsObject;

    extend(Events, superClass);

    function Events() {
      return Events.__super__.constructor.apply(this, arguments);
    }

    Events.prototype.get = function(id, cb) {
      var sql;
      sql = this.squel.select().from('events').field('doc').where('id = ?', id);
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, results.rows[0].doc);
      });
    };

    Events.prototype.save = function(event, cb) {
      var sql;
      sql = this.squel.insert().into('events').set('type', event.type).set('doc', "$$" + (JSON.stringify(event)) + "$$", {
        dontQuote: true
      }).returning('id');
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, {
          id: results.rows[0].id
        });
      });
    };

    Events.prototype.leadinfo = function(id, cb) {
      var sql;
      sql = this.squel.select().from('events').field('doc').field('id').field('type').field('at').where("doc->>'lead' = ?", id).where('type IN ?', ['lead.created', 'lead.segmented', 'lead.status.changed', 'lead.timedout', 'lead.updated']);
      return this.doc_list(sql, cb);
    };

    Events.prototype.oftype = function(type, query, cb) {
      var sql, x;
      if (query.descending == null) {
        query.descending = true;
      }
      sql = this.squel.select().from('events', 'ev').join('emails', 'em', '(case when (ev.doc->>\'email\') ~ E\'^\\\\d+$\' then (ev.doc->>\'email\')::bigint else 0 end = em.id)').field('ev.doc', 'event').field('em.id', 'id').field('em.type', 'type').field('em.doc', 'email').where('ev.type = ?', type).where('em.type IN ?', ['drip', 'direct', 'batch']).where('NOT em.doc ? \'canceled\'');
      if ((query.from != null) && (query.to != null)) {
        sql.where('ev.at BETWEEN ? and ?', query.from, query.to);
      }
      if (query.status != null) {
        sql.where("(em.doc->'status'->>'_id') = ?", query.status);
      }
      if (query.segment != null) {
        sql.where("(em.doc->'segment'->>'id') = ?", query.segment);
      }
      if (query.step != null) {
        sql.where("(em.doc->'step'->>'id') = ?", query.step);
      }
      if (query.batch != null) {
        sql.where("COALESCE((em.doc->'batch'->>'_id'), (em.doc->>'batch')) = ?", query.batch);
      }
      if (query.skip != null) {
        sql.offset(query.skip);
      }
      if (query.size != null) {
        sql.limit(query.size);
      }
      if (query.limit != null) {
        sql.limit(query.limit);
      }
      sql.order('ev.at', !query.descending);
      if (query.fields != null) {
        query.fields = (function() {
          var i, len, ref, results1;
          ref = JSON.parse(query.fields);
          results1 = [];
          for (i = 0, len = ref.length; i < len; i++) {
            x = ref[i];
            results1.push({
              path: x,
              fn: _.property(x)
            });
          }
          return results1;
        })();
      }
      return this.db.stream(sql, (function(_this) {
        return function(err, stream) {
          if (err != null) {
            return cb(err);
          }
          return cb(null, stream.pipe(_this.stream.transform(function(row) {
            return _.assign(row.event, {
              email: _.assign(row.email, _.pick(row, 'id', 'type'))
            });
          })));
        };
      })(this));
    };

    Events.prototype.notifications = function(to, email, cb) {
      var sql;
      sql = this.squel.select().from('events').field('doc').where('type IN ?', ['notification.sent', 'vunotification.queued']).where("doc->>'to' = ? OR doc->>'agent' = ?", to, to).where("doc->>'email' = ?", email).where('at > ?', moment.utc().subtract(48, 'hours').toISOString()).order('at', false).limit(1);
      return this.doc_list(sql, cb);
    };

    Events.prototype.summary = function(opts, cb) {
      if (opts == null) {
        opts = {};
      }
      return cb(new Error('deprecated: see emails/summary'));
    };

    Events.prototype.errors = function(query, cb) {
      var sql;
      sql = this.squel.select().from('events').field('doc').where("type IN ('api.errored', 'email.errored')").where('at BETWEEN ? and ?', query.from, query.to).order('at', !query.descending);
      if (query.size != null) {
        sql.limit(query.size);
      }
      if (query.skip != null) {
        sql.offset(query.skip);
      }
      return this.doc_list(sql, cb);
    };

    Events.prototype['email.pending'] = function(opts, cb) {
      return this.oftype('email.queued', opts, cb);
    };

    Events.prototype['email.delivered'] = function(opts, cb) {
      return this.oftype('email.delivered', opts, cb);
    };

    Events.prototype['email.clicked'] = function(opts, cb) {
      return this.oftype('email.clicked', opts, cb);
    };

    Events.prototype['email.opened'] = function(opts, cb) {
      return this.oftype('email.opened', opts, cb);
    };

    Events.prototype['email.bounced'] = function(opts, cb) {
      return this.oftype('email.bounced', opts, cb);
    };

    Events.prototype.sends = function(status, cb) {
      var end, opts, ref, ref1, start, thirtyDaysAgo, tomorrow;
      ref = [new Date, new Date], tomorrow = ref[0], thirtyDaysAgo = ref[1];
      tomorrow.setDate(tomorrow.getDate() + 1);
      thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);
      ref1 = [[status].concat(_asDateKey(thirtyDaysAgo)), [status].concat(_asDateKey(tomorrow))], start = ref1[0], end = ref1[1];
      opts = _makeReduceViewOptions(start, end, 4);
      return this.db.view('emails', 'by_date', opts, _viewResultsAsObject(_getDateFromKey, function(err, obj) {
        var name;
        while (thirtyDaysAgo < tomorrow) {
          if (obj[name = _parseDateKey(_asDateKey(thirtyDaysAgo))] == null) {
            obj[name] = 0;
          }
          thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() + 1);
        }
        return cb(null, obj);
      }));
    };

    Events.prototype._asDateKey = _asDateKey = function(date) {
      return [date.getFullYear(), date.getMonth() + 1, date.getDate()];
    };

    Events.prototype._getDateFromKey = _getDateFromKey = function(key) {
      return _parseDateKey(key.slice(1));
    };

    Events.prototype._parseDateKey = _parseDateKey = function(dateKey) {
      return dateKey.join('-');
    };

    Events.prototype.dashboard = function(cb) {
      var counts, sql, start_of_month;
      start_of_month = moment.utc().startOf('month');
      counts = {
        'email.delivered': 0,
        'email.errored': 0,
        'api.errored': 0,
        'optout': 0
      };
      sql = this.squel.select().from('events').field('type').field('COUNT(id)::int', 'count').where('at > ?', start_of_month.format()).where("type IN ('" + (_.keys(counts).join('\', \'')) + "')").group('type');
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return cb(err);
        }
        results.rows.forEach(function(row) {
          return counts[row.type] = row.count;
        });
        return cb(null, counts);
      });
    };

    Events.prototype.performance = function(opts, cb) {
      var monthago, ref, sql, today, types;
      ref = [moment().subtract(1, 'day'), moment().subtract(1, 'months')], today = ref[0], monthago = ref[1];
      types = ['email.clicked', 'email.delivered', 'email.errored', 'email.opened', 'email.queued', 'optout'];
      sql = this.squel.select().from('events').field("doc->>'status'", 'status').field("doc->>'segment'", 'segment').field("COALESCE(doc->'step'->>'_id', doc->>'step')", 'step').field("sum(case when type = 'email.queued' then 1 else 0 end)::int", 'sends').field("sum(case when type = 'email.delivered' then 1 else 0 end)::int", 'delivers').field("sum(case when type = 'email.opened' then 1 else 0 end)::int", 'opens').field("sum(case when type = 'email.clicked' then 1 else 0 end)::int", 'clicks').field("sum(case when doc ? 'first' AND type = 'optout' then 1 else 0 end)::int", 'optouts').field("sum(case when doc ? 'first' AND type = 'email.opened' then 1 else 0 end)::int", 'firstopens').field("sum(case when doc ? 'first' AND type = 'email.clicked' then 1 else 0 end)::int", 'firstclicks').where('at BETWEEN ? and ?', monthago.toISOString(), today.toISOString()).where('type IN ?', types).where("NOT doc ? 'canceled' AND NOT doc ? 'sample'").where("doc ? 'status' AND doc ? 'segment' AND doc ? 'step'").group("doc->>'status'").group("doc->>'segment'").group("COALESCE(doc->'step'->>'_id', doc->>'step')");
      return this.db.query(sql, function(err, results) {
        var rows;
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        rows = results.rows.map(function(row) {
          var segment, stats, status, step;
          status = row.status, segment = row.segment, step = row.step;
          stats = {
            delivers: row.delivers,
            opens: row.firstopens,
            clicks: row.firstclicks,
            rate: row.clicks / row.delivers
          };
          return {
            key: status + "/" + segment + "/" + step,
            status: status,
            segment: segment,
            step: step,
            stats: stats
          };
        });
        return async.filter(rows, function(arg, cb) {
          var stats;
          stats = arg.stats;
          return cb(stats.delivers > 10);
        }, function(results) {
          return async.sortBy(results, function(arg, cb) {
            var stats;
            stats = arg.stats;
            return cb(null, -1 * stats.rate);
          }, function(err, sorted) {
            var fn, limit;
            limit = parseInt(opts.limit || 3, 10);
            fn = _[opts.type === 'worst' ? 'takeRight' : 'take'];
            return cb(err, fn(sorted, limit));
          });
        });
      });
    };

    Events.prototype._getTypeFromKey = _getTypeFromKey = function(key) {
      return key[0];
    };

    Events.prototype._makeReduceViewOptions = _makeReduceViewOptions = function(start, end, level) {
      return {
        reduce: true,
        startkey: start,
        endkey: end,
        group_level: level
      };
    };

    Events.prototype._viewResultsAsObject = _viewResultsAsObject = function() {
      var args;
      args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      return function(err, body) {
        var cb, eachKey, eachValue, keyPluck, ref, valuePluck;
        if (body == null) {
          body = {};
        }
        ref = [args.pop(), args.shift(), args.shift()], cb = ref[0], keyPluck = ref[1], valuePluck = ref[2];
        eachKey = function(row) {
          return (typeof keyPluck === "function" ? keyPluck(row.key) : void 0) || row.key;
        };
        eachValue = function(row) {
          return (typeof valuePluck === "function" ? valuePluck(row.value) : void 0) || row.value;
        };
        if (body.rows == null) {
          body.rows = [];
        }
        return cb(err, _.object(_.pluck(body.rows, eachKey), _.pluck(body.rows, eachValue)));
      };
    };

    return Events;

  })(require('../pgmodule'));

}).call(this);
