module.exports = class Groups extends require './collection/data'
  @::type = 'group'

  delete: (id, cb) ->
    @get id, (err, group) =>
      return cb err if err

      group.active = no
      @update id, group, cb

  byGroup: (group, cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'doc'
      .where 'type = ?', @type
      .where "doc->>'name' = ?", group

    @list sql, cb

  idlookup: (item, cb) ->
    groups = []
    #for item in list
    @byGroup item, (err, group) ->
      return cb err if err
      cb null, group[0]
