(function() {
  var Groups,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = Groups = (function(superClass) {
    extend(Groups, superClass);

    function Groups() {
      return Groups.__super__.constructor.apply(this, arguments);
    }

    Groups.prototype.type = 'group';

    Groups.prototype["delete"] = function(id, cb) {
      return this.get(id, (function(_this) {
        return function(err, group) {
          if (err) {
            return cb(err);
          }
          group.active = false;
          return _this.update(id, group, cb);
        };
      })(this));
    };

    Groups.prototype.byGroup = function(group, cb) {
      var sql;
      sql = this.squel.select().from('data_current').field("REPLACE(id::text, '-', '')", 'id').field('version').field('doc').where('type = ?', this.type).where("doc->>'name' = ?", group);
      return this.list(sql, cb);
    };

    Groups.prototype.idlookup = function(item, cb) {
      var groups;
      groups = [];
      return this.byGroup(item, function(err, group) {
        if (err) {
          return cb(err);
        }
        return cb(null, group[0]);
      });
    };

    return Groups;

  })(require('./collection/data'));

}).call(this);
