_ = require 'lodash'
moment = require 'moment'

module.exports = class Imports extends require './collection/data'
  @::type = 'lead.import'

  save: (_import, cb) ->
    if _import.csv?
      csv = _import.csv
      delete _import.csv

    super _import, (err, body) =>
      return cb err if err?
      return cb null, id: body.id unless csv?

      @attachment.save body.id, 'csv', csv, (err) ->
        return cb err if err?
        cb null, id: body.id

  all: (opts, cb) ->
    sql = @squel.select()
      .from 'data_current d'
      .field 'id'
      .field 'version'
      .field 'doc'
      .where 'type = ?', @type
      .order 'published', no
      .limit opts.limit or 10

    @list sql, cb

  errors: (id, cb) ->
    @attachment.get id, 'errors.csv', cb

  create_table: (opts, cb) ->
    date = moment(opts.at).format('YYYYMMDDHHmmss')
    @db.query """
CREATE TABLE uploads_#{opts.type}_#{date} (
  row integer,
  status text,
  line text,
  started timestamptz,
  updated timestamptz,
  doc jsonb
);"""
    , (err) ->
      return cb err if err
      cb null, name: "uploads_#{opts.type}_#{date}"

  parse: (_import, cb) ->
    sql = @squel.insert()
      .into _import.table
      .setFields {row: _import.row, status: 'pending', started: _import.started, line: _import.line}

    @db.query sql, (err, result) ->
      return cb err if err?
      cb null, result.rows[0]

  import_lines: (opts, cb) ->
    sql = @squel.select()
      .from opts.table
      .field 'line'
      .field 'row'
      .field 'doc'
      .field 'status'
      .limit opts.limit || 20

    sql.where 'status = ?', opts.status if opts.status?
    sql.order 'row' if opts.order?

    @db.query sql, (err, result) ->
      return cb err if err?
      cb null, result.rows

  save_lines: (opts, cb) ->
    sql = @squel.update()
      .table opts.table
      .set 'status', opts.status
      .where 'row = ?', opts.row # where row = row_id

    sql.set 'doc', @jsonb opts.doc if opts.doc?
    sql.set 'updated', opts.updated if opts.updated?

    @db.query sql, (err, result) ->
      return cb err if err?
      cb null, result.rows[0]
