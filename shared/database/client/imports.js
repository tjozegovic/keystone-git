(function() {
  var Imports, _, moment,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  moment = require('moment');

  module.exports = Imports = (function(superClass) {
    extend(Imports, superClass);

    function Imports() {
      return Imports.__super__.constructor.apply(this, arguments);
    }

    Imports.prototype.type = 'lead.import';

    Imports.prototype.save = function(_import, cb) {
      var csv;
      if (_import.csv != null) {
        csv = _import.csv;
        delete _import.csv;
      }
      return Imports.__super__.save.call(this, _import, (function(_this) {
        return function(err, body) {
          if (err != null) {
            return cb(err);
          }
          if (csv == null) {
            return cb(null, {
              id: body.id
            });
          }
          return _this.attachment.save(body.id, 'csv', csv, function(err) {
            if (err != null) {
              return cb(err);
            }
            return cb(null, {
              id: body.id
            });
          });
        };
      })(this));
    };

    Imports.prototype.all = function(opts, cb) {
      var sql;
      sql = this.squel.select().from('data_current d').field('id').field('version').field('doc').where('type = ?', this.type).order('published', false).limit(opts.limit || 10);
      return this.list(sql, cb);
    };

    Imports.prototype.errors = function(id, cb) {
      return this.attachment.get(id, 'errors.csv', cb);
    };

    Imports.prototype.create_table = function(opts, cb) {
      var date;
      date = moment(opts.at).format('YYYYMMDDHHmmss');
      return this.db.query("CREATE TABLE uploads_" + opts.type + "_" + date + " (\n  row integer,\n  status text,\n  line text,\n  started timestamptz,\n  updated timestamptz,\n  doc jsonb\n);", function(err) {
        if (err) {
          return cb(err);
        }
        return cb(null, {
          name: "uploads_" + opts.type + "_" + date
        });
      });
    };

    Imports.prototype.parse = function(_import, cb) {
      var sql;
      sql = this.squel.insert().into(_import.table).setFields({
        row: _import.row,
        status: 'pending',
        started: _import.started,
        line: _import.line
      });
      return this.db.query(sql, function(err, result) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, result.rows[0]);
      });
    };

    Imports.prototype.import_lines = function(opts, cb) {
      var sql;
      sql = this.squel.select().from(opts.table).field('line').field('row').field('doc').field('status').limit(opts.limit || 20);
      if (opts.status != null) {
        sql.where('status = ?', opts.status);
      }
      if (opts.order != null) {
        sql.order('row');
      }
      return this.db.query(sql, function(err, result) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, result.rows);
      });
    };

    Imports.prototype.save_lines = function(opts, cb) {
      var sql;
      sql = this.squel.update().table(opts.table).set('status', opts.status).where('row = ?', opts.row);
      if (opts.doc != null) {
        sql.set('doc', this.jsonb(opts.doc));
      }
      if (opts.updated != null) {
        sql.set('updated', opts.updated);
      }
      return this.db.query(sql, function(err, result) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, result.rows[0]);
      });
    };

    return Imports;

  })(require('./collection/data'));

}).call(this);
