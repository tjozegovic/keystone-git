(function() {
  var Layouts,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = Layouts = (function(superClass) {
    extend(Layouts, superClass);

    function Layouts() {
      return Layouts.__super__.constructor.apply(this, arguments);
    }

    Layouts.prototype.type = 'layout';

    return Layouts;

  })(require('./collection/data'));

}).call(this);
