_ = require 'lodash'

module.exports = class Leads extends require '../scd'
  @::table = 'leads'

  query: (query, cb) ->
    sql = @squel.select()
      .from 'leads_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'published'
      .field 'doc'
      .where 'published BETWEEN ? and ?', query.from, query.to
      .order 'published', no

    sql.offset query.skip if query.skip?
    sql.limit query.size if query.size?

    @list sql, cb

  by_recipient_email: (email, cb) ->
    sql = @squel.select()
      .from 'leads_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'published'
      .field 'doc'
      .where "doc @> '#{JSON.stringify recipient: email: email}'::jsonb"

    @list sql, cb

  by_segment: (status, segment, oldest, cb) ->
    sql = @squel.select()
      .from 'leads_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'doc'
      .where "(doc->>'$status' = ?)", status
      .where "(doc->>'segment' = ?)", segment
      .where "(COALESCE(doc->>'segmented', doc->>'created') >= ?)", oldest.toISOString()
      .order "COALESCE(doc->>'segmented', doc->>'created')", no

    @list sql, cb

  by_external: (source, id, cb) ->
    sql = @squel.select()
      .from 'leads_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'published'
      .field 'doc'
      .where "doc ? 'external'"
      .where "(doc->'external'->>'source') = ?", source
      .where "(doc->'external'->>'id') = ?::text", id

    @single sql, cb

  # TODO obsolete
  getByExternalId: @::by_external
