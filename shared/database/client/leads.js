(function() {
  var Leads, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = Leads = (function(superClass) {
    extend(Leads, superClass);

    function Leads() {
      return Leads.__super__.constructor.apply(this, arguments);
    }

    Leads.prototype.table = 'leads';

    Leads.prototype.query = function(query, cb) {
      var sql;
      sql = this.squel.select().from('leads_current').field("REPLACE(id::text, '-', '')", 'id').field('version').field('published').field('doc').where('published BETWEEN ? and ?', query.from, query.to).order('published', false);
      if (query.skip != null) {
        sql.offset(query.skip);
      }
      if (query.size != null) {
        sql.limit(query.size);
      }
      return this.list(sql, cb);
    };

    Leads.prototype.by_recipient_email = function(email, cb) {
      var sql;
      sql = this.squel.select().from('leads_current').field("REPLACE(id::text, '-', '')", 'id').field('version').field('published').field('doc').where("doc @> '" + (JSON.stringify({
        recipient: {
          email: email
        }
      })) + "'::jsonb");
      return this.list(sql, cb);
    };

    Leads.prototype.by_segment = function(status, segment, oldest, cb) {
      var sql;
      sql = this.squel.select().from('leads_current').field("REPLACE(id::text, '-', '')", 'id').field('doc').where("(doc->>'$status' = ?)", status).where("(doc->>'segment' = ?)", segment).where("(COALESCE(doc->>'segmented', doc->>'created') >= ?)", oldest.toISOString()).order("COALESCE(doc->>'segmented', doc->>'created')", false);
      return this.list(sql, cb);
    };

    Leads.prototype.by_external = function(source, id, cb) {
      var sql;
      sql = this.squel.select().from('leads_current').field("REPLACE(id::text, '-', '')", 'id').field('version').field('published').field('doc').where("doc ? 'external'").where("(doc->'external'->>'source') = ?", source).where("(doc->'external'->>'id') = ?::text", id);
      return this.single(sql, cb);
    };

    Leads.prototype.getByExternalId = Leads.prototype.by_external;

    return Leads;

  })(require('../scd'));

}).call(this);
