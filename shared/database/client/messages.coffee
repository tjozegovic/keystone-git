util = require 'shared/util'

module.exports = class Messages extends require './collection/data'
  @::type = 'message'

  before_save: (doc) ->
    super doc

    for version in doc.versions or [] when version.filters?
      joins = for key, values of version.filters when values.length
        ("#{key}/#{value}" for value in values)
      version.products = util.xproduct joins...

  bySlug: (slug, cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'doc'
      .where 'type = ?', 'message'
      .where "doc->>'slug' = ?", slug

    @single sql, cb
