(function() {
  var Messages, util,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  util = require('shared/util');

  module.exports = Messages = (function(superClass) {
    extend(Messages, superClass);

    function Messages() {
      return Messages.__super__.constructor.apply(this, arguments);
    }

    Messages.prototype.type = 'message';

    Messages.prototype.before_save = function(doc) {
      var i, joins, key, len, ref, results, value, values, version;
      Messages.__super__.before_save.call(this, doc);
      ref = doc.versions || [];
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        version = ref[i];
        if (!(version.filters != null)) {
          continue;
        }
        joins = (function() {
          var ref1, results1;
          ref1 = version.filters;
          results1 = [];
          for (key in ref1) {
            values = ref1[key];
            if (values.length) {
              results1.push((function() {
                var j, len1, results2;
                results2 = [];
                for (j = 0, len1 = values.length; j < len1; j++) {
                  value = values[j];
                  results2.push(key + "/" + value);
                }
                return results2;
              })());
            }
          }
          return results1;
        })();
        results.push(version.products = util.xproduct.apply(util, joins));
      }
      return results;
    };

    Messages.prototype.bySlug = function(slug, cb) {
      var sql;
      sql = this.squel.select().from('data_current').field("REPLACE(id::text, '-', '')", 'id').field('doc').where('type = ?', 'message').where("doc->>'slug' = ?", slug);
      return this.single(sql, cb);
    };

    return Messages;

  })(require('./collection/data'));

}).call(this);
