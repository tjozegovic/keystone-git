module.exports = class Policies extends require './collection/data'
  @::type = 'policy'

  before_save: (doc) ->
    super doc
    if doc.$html
      delete doc.$html
