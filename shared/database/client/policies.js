(function() {
  var Policies,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = Policies = (function(superClass) {
    extend(Policies, superClass);

    function Policies() {
      return Policies.__super__.constructor.apply(this, arguments);
    }

    Policies.prototype.type = 'policy';

    Policies.prototype.before_save = function(doc) {
      Policies.__super__.before_save.call(this, doc);
      if (doc.$html) {
        return delete doc.$html;
      }
    };

    return Policies;

  })(require('./collection/data'));

}).call(this);
