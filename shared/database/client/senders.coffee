module.exports = class Senders extends require './collection/data'
  @::type = 'sender'

  upsert: (doc, cb) ->
    if doc.id
      @update doc.id, doc, cb
    else
      @save doc, cb

  by_name_or_alias: (alias, cb) ->
    sql = '''
      SELECT doc
      FROM data_current
      WHERE type = 'sender'
        AND lower($1) = ANY(ARRAY(
            SELECT lower(doc->>'email')
          UNION
            SELECT lower(trim(elem::text, '"'))
            FROM jsonb_array_elements(doc->'aliases') elem
          UNION
            SELECT lower((doc->>'firstname') || ' ' || (doc->>'lastname'))
          UNION
            SELECT lower((doc->>'lastname') || ', ' || (doc->>'firstname'))
        ))
      LIMIT 1
    '''

    @single sql, [alias], cb

  # TODO OBSOLETE remove byalias references
  byalias: @::by_name_or_alias

  active_by_name_or_alias: (alias, cb) ->
    sql = '''
      SELECT doc
      FROM data_current
      WHERE type = 'sender'
        AND (NOT (doc ? 'active') OR (doc->>'active')::bool)
        AND lower($1) = ANY(ARRAY(
            SELECT lower(doc->>'email')
          UNION
            SELECT lower(trim(elem::text, '"'))
            FROM jsonb_array_elements(doc->'aliases') elem
          UNION
            SELECT lower((doc->>'firstname') || ' ' || (doc->>'lastname'))
          UNION
            SELECT lower((doc->>'lastname') || ', ' || (doc->>'firstname'))
        ))
      LIMIT 1
    '''

    @single sql, [alias], cb

  byEmail: (email, cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'doc'
      .where 'type = ?', @type
      .where "doc->>'email' = ?", email

    @list sql, cb

  getEmail: (email, cb) ->
    #senders = []
    #for email in emails
    @byEmail email, (err, sender) ->
      return cb err if err
      cb null, sender[0]
