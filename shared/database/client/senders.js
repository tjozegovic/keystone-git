(function() {
  var Senders,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = Senders = (function(superClass) {
    extend(Senders, superClass);

    function Senders() {
      return Senders.__super__.constructor.apply(this, arguments);
    }

    Senders.prototype.type = 'sender';

    Senders.prototype.upsert = function(doc, cb) {
      if (doc.id) {
        return this.update(doc.id, doc, cb);
      } else {
        return this.save(doc, cb);
      }
    };

    Senders.prototype.by_name_or_alias = function(alias, cb) {
      var sql;
      sql = 'SELECT doc\nFROM data_current\nWHERE type = \'sender\'\n  AND lower($1) = ANY(ARRAY(\n      SELECT lower(doc->>\'email\')\n    UNION\n      SELECT lower(trim(elem::text, \'"\'))\n      FROM jsonb_array_elements(doc->\'aliases\') elem\n    UNION\n      SELECT lower((doc->>\'firstname\') || \' \' || (doc->>\'lastname\'))\n    UNION\n      SELECT lower((doc->>\'lastname\') || \', \' || (doc->>\'firstname\'))\n  ))\nLIMIT 1';
      return this.single(sql, [alias], cb);
    };

    Senders.prototype.byalias = Senders.prototype.by_name_or_alias;

    Senders.prototype.active_by_name_or_alias = function(alias, cb) {
      var sql;
      sql = 'SELECT doc\nFROM data_current\nWHERE type = \'sender\'\n  AND (NOT (doc ? \'active\') OR (doc->>\'active\')::bool)\n  AND lower($1) = ANY(ARRAY(\n      SELECT lower(doc->>\'email\')\n    UNION\n      SELECT lower(trim(elem::text, \'"\'))\n      FROM jsonb_array_elements(doc->\'aliases\') elem\n    UNION\n      SELECT lower((doc->>\'firstname\') || \' \' || (doc->>\'lastname\'))\n    UNION\n      SELECT lower((doc->>\'lastname\') || \', \' || (doc->>\'firstname\'))\n  ))\nLIMIT 1';
      return this.single(sql, [alias], cb);
    };

    Senders.prototype.byEmail = function(email, cb) {
      var sql;
      sql = this.squel.select().from('data_current').field("REPLACE(id::text, '-', '')", 'id').field('version').field('doc').where('type = ?', this.type).where("doc->>'email' = ?", email);
      return this.list(sql, cb);
    };

    Senders.prototype.getEmail = function(email, cb) {
      return this.byEmail(email, function(err, sender) {
        if (err) {
          return cb(err);
        }
        return cb(null, sender[0]);
      });
    };

    return Senders;

  })(require('./collection/data'));

}).call(this);
