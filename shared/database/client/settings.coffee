_ = require 'lodash'

module.exports = class Settings extends require '../pgmodule'
  constructor: (@db, @factory) ->
    Object.defineProperty @, 'client',
      value: @factory.client

  get: (id, section, cb) ->
    sql = @squel.select()
      .from 'settings_current'
      .field 'doc'
      .field 'version'
      .where 'user_id = ?', id
      .where 'client_id = ?', @client
      .limit 1

    @single sql, (err, result) ->
      [cb, section] = [section, undefined] unless _.isFunction cb

      return cb err if err?
      return cb null, {} unless result?

      doc = _.omit result.doc, (_, prop) -> prop[0] is '_'
      doc.version = result.version
      cb null, if section? then doc[section] else doc

  update: (id, section, settings, cb) ->
    [cb, settings, section] = [settings, section, undefined] unless cb? and _.isFunction cb
    return cb 'settings must have an id' unless id?

    @get id, (err, body) =>
      return cb err if err?

      if section?
        return cb err if err?

        body.id ?= id
        body[section] = settings
        @save id, body, cb
      else
        settings.version = body.version
        @save id, settings, cb

  save: (id, doc, cb) ->
    json = JSON.stringify doc
    @db.multiple (multi) =>
      errnext = (err) ->
        multi.close err
        cb err

      multi.query 'BEGIN'
      multi.query """
UPDATE settings SET
  doc = $4::jsonb
WHERE user_id = $1 AND client_id = $2 AND version = $3""", [id, @client, doc.version or 0, json]
      , (err) =>
        return errnext err if err?

        multi.query """
INSERT INTO settings (user_id, client_id, doc)
  SELECT $1, $2, $3::jsonb
  WHERE NOT EXISTS (SELECT 1 FROM settings WHERE user_id = $1 AND client_id = $2)""", [id, @client, json]
        , (err) =>
          return errnext err if err?

          multi.query """
SELECT version FROM settings_current WHERE user_id = $1 AND client_id = $2""", [id, @client]
          , (err, results) ->
            if err?
              multi.close err
              return cb err

            multi.query 'COMMIT', (err) ->
              multi.close err
              return cb err if err?
              cb null, version: results.rows[0].version
