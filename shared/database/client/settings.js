(function() {
  var Settings, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = Settings = (function(superClass) {
    extend(Settings, superClass);

    function Settings(db, factory) {
      this.db = db;
      this.factory = factory;
      Object.defineProperty(this, 'client', {
        value: this.factory.client
      });
    }

    Settings.prototype.get = function(id, section, cb) {
      var sql;
      sql = this.squel.select().from('settings_current').field('doc').field('version').where('user_id = ?', id).where('client_id = ?', this.client).limit(1);
      return this.single(sql, function(err, result) {
        var doc, ref;
        if (!_.isFunction(cb)) {
          ref = [section, void 0], cb = ref[0], section = ref[1];
        }
        if (err != null) {
          return cb(err);
        }
        if (result == null) {
          return cb(null, {});
        }
        doc = _.omit(result.doc, function(_, prop) {
          return prop[0] === '_';
        });
        doc.version = result.version;
        return cb(null, section != null ? doc[section] : doc);
      });
    };

    Settings.prototype.update = function(id, section, settings, cb) {
      var ref;
      if (!((cb != null) && _.isFunction(cb))) {
        ref = [settings, section, void 0], cb = ref[0], settings = ref[1], section = ref[2];
      }
      if (id == null) {
        return cb('settings must have an id');
      }
      return this.get(id, (function(_this) {
        return function(err, body) {
          if (err != null) {
            return cb(err);
          }
          if (section != null) {
            if (err != null) {
              return cb(err);
            }
            if (body.id == null) {
              body.id = id;
            }
            body[section] = settings;
            return _this.save(id, body, cb);
          } else {
            settings.version = body.version;
            return _this.save(id, settings, cb);
          }
        };
      })(this));
    };

    Settings.prototype.save = function(id, doc, cb) {
      var json;
      json = JSON.stringify(doc);
      return this.db.multiple((function(_this) {
        return function(multi) {
          var errnext;
          errnext = function(err) {
            multi.close(err);
            return cb(err);
          };
          multi.query('BEGIN');
          return multi.query("UPDATE settings SET\n  doc = $4::jsonb\nWHERE user_id = $1 AND client_id = $2 AND version = $3", [id, _this.client, doc.version || 0, json], function(err) {
            if (err != null) {
              return errnext(err);
            }
            return multi.query("INSERT INTO settings (user_id, client_id, doc)\n  SELECT $1, $2, $3::jsonb\n  WHERE NOT EXISTS (SELECT 1 FROM settings WHERE user_id = $1 AND client_id = $2)", [id, _this.client, json], function(err) {
              if (err != null) {
                return errnext(err);
              }
              return multi.query("SELECT version FROM settings_current WHERE user_id = $1 AND client_id = $2", [id, _this.client], function(err, results) {
                if (err != null) {
                  multi.close(err);
                  return cb(err);
                }
                return multi.query('COMMIT', function(err) {
                  multi.close(err);
                  if (err != null) {
                    return cb(err);
                  }
                  return cb(null, {
                    version: results.rows[0].version
                  });
                });
              });
            });
          });
        };
      })(this));
    };

    return Settings;

  })(require('../pgmodule'));

}).call(this);
