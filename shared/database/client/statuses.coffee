_ = require 'lodash'
async = require 'async'
util = require 'shared/util'

module.exports = class Statuses extends require './collection/data'
  @::type = 'status'

  save: (doc, cb) ->
    doc.segments = unless _.isArray doc?.segments then [] else doc.segments

    _.forEach doc.segments, (segment) ->
      joins = for key, values of segment.filters when values.length
        ("#{key}/#{value}" for value in values)
      segment.products = util.xproduct joins...

      _.forEach segment.steps, (step) ->
        step.message = step.message?.id

    super doc, (err, body) ->
      return cb new Error 'Could not save status, conflict detected.' if err
      cb err, body

  active: (cb) ->
    messages = @ctx.use 'client/messages'
    super (err, statuses) ->
      return cb err if err

      async.map statuses
      , (doc, done) ->
        async.each doc.segments
        , (segment, done) ->
          messageids = _(segment.steps).pluck('message').uniq()
          return done null unless messageids.length

          messages.fetch messageids, (err, messages) ->
            return done err if err

            for message in messages
              _.find(segment.steps, message: message.id)?.message = message
            done err
        , (err) ->
          done err, doc
      , (err, results) ->
        cb err, _.indexBy results, 'id'

  allalias: (cb) ->
    @db.query "select REPLACE(id::text, '-', '') id, doc, doc->>'name' as name
      from data_current
      where type = 'status'
    union
      select REPLACE(id::text, '-', ''), doc, trim(e::text, '\"') as name
      from data_current, jsonb_array_elements(doc->'aliases') e
      where type = 'status'"
    , cb

  by_alias: (alias, cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'doc'
      .where 'type = ?', 'status'
      .where "doc->>'name' = $1 or doc->'aliases' ? $1"
      .where "NOT (doc ? 'active') OR (doc->>'active')::bool"

    @single sql, [alias], cb

  byalias: @::by_alias

  get_by_name: (lead, cb) ->
    sql = @squel.select()
      .from 'data_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'doc'
      .where 'type = ?', @type
      .where "doc->>'name' = ?", lead

    @list sql, cb

  getByName: @::get_by_name
