(function() {
  var Statuses, _, async, util,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  async = require('async');

  util = require('shared/util');

  module.exports = Statuses = (function(superClass) {
    extend(Statuses, superClass);

    function Statuses() {
      return Statuses.__super__.constructor.apply(this, arguments);
    }

    Statuses.prototype.type = 'status';

    Statuses.prototype.save = function(doc, cb) {
      doc.segments = !_.isArray(doc != null ? doc.segments : void 0) ? [] : doc.segments;
      _.forEach(doc.segments, function(segment) {
        var joins, key, value, values;
        joins = (function() {
          var ref, results1;
          ref = segment.filters;
          results1 = [];
          for (key in ref) {
            values = ref[key];
            if (values.length) {
              results1.push((function() {
                var i, len, results2;
                results2 = [];
                for (i = 0, len = values.length; i < len; i++) {
                  value = values[i];
                  results2.push(key + "/" + value);
                }
                return results2;
              })());
            }
          }
          return results1;
        })();
        segment.products = util.xproduct.apply(util, joins);
        return _.forEach(segment.steps, function(step) {
          var ref;
          return step.message = (ref = step.message) != null ? ref.id : void 0;
        });
      });
      return Statuses.__super__.save.call(this, doc, function(err, body) {
        if (err) {
          return cb(new Error('Could not save status, conflict detected.'));
        }
        return cb(err, body);
      });
    };

    Statuses.prototype.active = function(cb) {
      var messages;
      messages = this.ctx.use('client/messages');
      return Statuses.__super__.active.call(this, function(err, statuses) {
        if (err) {
          return cb(err);
        }
        return async.map(statuses, function(doc, done) {
          return async.each(doc.segments, function(segment, done) {
            var messageids;
            messageids = _(segment.steps).pluck('message').uniq();
            if (!messageids.length) {
              return done(null);
            }
            return messages.fetch(messageids, function(err, messages) {
              var i, len, message, ref;
              if (err) {
                return done(err);
              }
              for (i = 0, len = messages.length; i < len; i++) {
                message = messages[i];
                if ((ref = _.find(segment.steps, {
                  message: message.id
                })) != null) {
                  ref.message = message;
                }
              }
              return done(err);
            });
          }, function(err) {
            return done(err, doc);
          });
        }, function(err, results) {
          return cb(err, _.indexBy(results, 'id'));
        });
      });
    };

    Statuses.prototype.allalias = function(cb) {
      return this.db.query("select REPLACE(id::text, '-', '') id, doc, doc->>'name' as name from data_current where type = 'status' union select REPLACE(id::text, '-', ''), doc, trim(e::text, '\"') as name from data_current, jsonb_array_elements(doc->'aliases') e where type = 'status'", cb);
    };

    Statuses.prototype.by_alias = function(alias, cb) {
      var sql;
      sql = this.squel.select().from('data_current').field("REPLACE(id::text, '-', '')", 'id').field('doc').where('type = ?', 'status').where("doc->>'name' = $1 or doc->'aliases' ? $1").where("NOT (doc ? 'active') OR (doc->>'active')::bool");
      return this.single(sql, [alias], cb);
    };

    Statuses.prototype.byalias = Statuses.prototype.by_alias;

    Statuses.prototype.get_by_name = function(lead, cb) {
      var sql;
      sql = this.squel.select().from('data_current').field("REPLACE(id::text, '-', '')", 'id').field('version').field('doc').where('type = ?', this.type).where("doc->>'name' = ?", lead);
      return this.list(sql, cb);
    };

    Statuses.prototype.getByName = Statuses.prototype.get_by_name;

    return Statuses;

  })(require('./collection/data'));

}).call(this);
