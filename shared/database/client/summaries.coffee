_ = require 'lodash'
async = require 'async'
moment = require 'moment'

EPOCH = moment.utc('2010-01-01').valueOf()

module.exports = class Summaries extends require './collection/data'
  setrange = (query) ->
    _parse = (date, otherwise) ->
      dt = if date then moment.utc new Date date else otherwise()
      [dt.year(), dt.month() + 1, dt.date()]

    query.startkey = [].concat query.keyprefix, _parse(query.from, -> moment.utc().startOf 'month')
    query.endkey = [].concat query.keyprefix, _parse(query.to, -> moment.utc().endOf 'month'), {}

  _setup_query: (query) ->
    @squel.select()
      .field 'count(*)::int', 'sends'
      .field 'count(delivered)::int', 'delivers'
      .field 'count(bounced)::int', 'bounces'
      .field 'sum(COALESCE(cardinality(optout), 0))::int', 'optouts'
      .field 'sum(COALESCE(cardinality(opened), 0))::int', 'opens'
      .field 'count(opened)::int', 'unqopens'
      .field 'count(opened)::decimal / NULLIF(count(delivered), 0)', 'openrate'
      .field 'sum(COALESCE(cardinality(clicked), 0))::int', 'clicks'
      .field 'count(clicked)::int', 'unqclicks'
      .field 'count(clicked)::decimal / NULLIF(count(delivered), 0)', 'clickrate'

  _setup_fromwithattributes: (query) ->
    @squel.select()
      .field 'e.id'
      .field "split_part(x, '/', 1)", 'group'
      .field "split_part(x, '/', 2)", 'attribute'
      .from 'emails', 'e'
      .from "jsonb_array_elements_text(doc#>'{lead, $attributes}')", 'x'
      .where 'id BETWEEN get_epoch_from_ts(?) AND get_epoch_from_ts(?)', _.values(_.pick query, 'from', 'to')...

  drips: (query = {}, cb) ->
    sql = @_setup_query()

    if query.root is 'groups'
      sql.field '"group"'
      sql.from 'drip_stats', 's'
      sql.from @_setup_fromwithattributes(query), 'attrs'
      sql.where 's.id = attrs.id'
      sql.where 's.id BETWEEN get_epoch_from_ts(?) AND get_epoch_from_ts(?)', _.values(_.pick query, 'from', 'to')...
      sql.group '"group"'

      if query.group?
        sql.where '"group" = ?', query.group
        if query.daily?
          sql.field "date_trunc('day', queued)" , 'day'
          sql.group "date_trunc('day', queued)"
          sql.order "date_trunc('day', queued)"
        else
          sql.field 'attribute'
          sql.group 'attribute'
      if query.attribute?
        sql.where 'attribute = ?', query.attribute

      if query.filter? and (filter = JSON.parse query.filter).length
        expr = @squel.expr()
        for f in filter
          expr.or_begin()
          expr.and 'status = ?', f.status if f.status?
          expr.and 'segment = ?', f.segment if f.segment?
          expr.and 'step = ?', f.step if f.step?
          expr.end()

        sql.where expr
    else
      sql.from 'drip_stats', 's'
      sql.where 's.id BETWEEN get_epoch_from_ts(?) and get_epoch_from_ts(?)', _.values(_.pick query, 'from', 'to')...
      sql.field "REPLACE(status::text, '-', '')", 'status'
      sql.group 'status'

      if query.status?
        sql.where 'status = ?', query.status
        if query.daily?
          sql.field "date_trunc('day', queued)" , 'day'
          sql.group "date_trunc('day', queued)"
          sql.order "date_trunc('day', queued)"
        else
          sql.field "REPLACE(segment::text, '-', '')", 'segment'
          sql.group 'segment'
      if query.segment?
        sql.field "REPLACE(step::text, '-', '')", 'step'
        sql.where 'segment = ?', query.segment
        sql.group 'step'
      if query.step?
        sql.where 'step = ?', query.step

      if query.filter? and (filter = JSON.parse query.filter).length
        sql.from @_setup_fromwithattributes(query), 'attrs'
        sql.where 's.id = attrs.id'

        expr = @squel.expr()
        for f in filter
          expr.or_begin()
          expr.and '"group" = ?', f.group if f.group?
          expr.and 'attribute = ?', f.attribute if f.attribute?
          expr.end()

        sql.where expr

    @db.query sql, (err, results) ->
      return cb? err if err?
      cb? null, results.rows

  direct: (query = {}, cb) ->
    dates = ['from', 'to'].map (k) ->
      moment.utc(query[k]).valueOf() - EPOCH

    sql = @_setup_query query
      .from 'direct_stats'
      .field 'id'
      .where 'id BETWEEN ? and ?', dates...
      .group 'id'

    @db.query sql, (err, results) ->
      return cb? err if err?
      cb? null, results.rows

  batches: (query = {}, cb) ->
    dates = ['from', 'to'].map (k) ->
      moment.utc(query[k]).valueOf() - EPOCH

    sql = @_setup_query query
      .from 'batch_stats'
      .field "REPLACE(batch::text, '-', '')", 'batch'
      .where 'id BETWEEN ? and ?', dates...
      .where 'NOT batch IS NULL'
      .group 'batch'

    if query.batch?
      sql.where 'batch = ?', query.batch

    @db.query sql, (err, results) ->
      return cb? err if err?
      cb? null, results.rows

  batches_by_id: (id, cb) ->
    sql = @_setup_query id
      .from 'batch_stats'
      .where 'batch = ?', id
      .group 'batch'

    @db.query sql, (err, results) ->
      return cb? err if err?
      cb? null, results.rows

  billing: (query, cb) ->
    dates = ['from', 'to'].map (k) ->
      moment.utc(query[k]).valueOf() - EPOCH

    sql = @squel.select()
      .field 'count(s.delivered)::int', 'sends'
      .field 'coalesce(sum(cardinality(s.clicked)), 0)::int', 'clicks'
      .field "count(distinct(coalesce(
          e.doc->'agent'->>'email',
          e.doc->'sender'->>'email',
          e.doc->'mail'->>'from'
        )))::int", 'users'
      .from 'drip_stats', 's'
      .from 'emails', 'e'
      .where 's.id = e.id'
      .where 's.id BETWEEN ? and ?', dates...
      .where 'e.id BETWEEN ? and ?', dates...
      .where "NOT (e.doc ? 'sample')"

    @db.query sql, (err, results) ->
      return cb? err if err?
      cb? null, results.rows[0]
