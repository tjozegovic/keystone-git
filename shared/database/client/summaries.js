(function() {
  var EPOCH, Summaries, _, async, moment,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty,
    slice = [].slice;

  _ = require('lodash');

  async = require('async');

  moment = require('moment');

  EPOCH = moment.utc('2010-01-01').valueOf();

  module.exports = Summaries = (function(superClass) {
    var setrange;

    extend(Summaries, superClass);

    function Summaries() {
      return Summaries.__super__.constructor.apply(this, arguments);
    }

    setrange = function(query) {
      var _parse;
      _parse = function(date, otherwise) {
        var dt;
        dt = date ? moment.utc(new Date(date)) : otherwise();
        return [dt.year(), dt.month() + 1, dt.date()];
      };
      query.startkey = [].concat(query.keyprefix, _parse(query.from, function() {
        return moment.utc().startOf('month');
      }));
      return query.endkey = [].concat(query.keyprefix, _parse(query.to, function() {
        return moment.utc().endOf('month');
      }), {});
    };

    Summaries.prototype._setup_query = function(query) {
      return this.squel.select().field('count(*)::int', 'sends').field('count(delivered)::int', 'delivers').field('count(bounced)::int', 'bounces').field('sum(COALESCE(cardinality(optout), 0))::int', 'optouts').field('sum(COALESCE(cardinality(opened), 0))::int', 'opens').field('count(opened)::int', 'unqopens').field('count(opened)::decimal / NULLIF(count(delivered), 0)', 'openrate').field('sum(COALESCE(cardinality(clicked), 0))::int', 'clicks').field('count(clicked)::int', 'unqclicks').field('count(clicked)::decimal / NULLIF(count(delivered), 0)', 'clickrate');
    };

    Summaries.prototype._setup_fromwithattributes = function(query) {
      var ref;
      return (ref = this.squel.select().field('e.id').field("split_part(x, '/', 1)", 'group').field("split_part(x, '/', 2)", 'attribute').from('emails', 'e').from("jsonb_array_elements_text(doc#>'{lead, $attributes}')", 'x')).where.apply(ref, ['id BETWEEN get_epoch_from_ts(?) AND get_epoch_from_ts(?)'].concat(slice.call(_.values(_.pick(query, 'from', 'to')))));
    };

    Summaries.prototype.drips = function(query, cb) {
      var expr, f, filter, i, j, len, len1, sql;
      if (query == null) {
        query = {};
      }
      sql = this._setup_query();
      if (query.root === 'groups') {
        sql.field('"group"');
        sql.from('drip_stats', 's');
        sql.from(this._setup_fromwithattributes(query), 'attrs');
        sql.where('s.id = attrs.id');
        sql.where.apply(sql, ['s.id BETWEEN get_epoch_from_ts(?) AND get_epoch_from_ts(?)'].concat(slice.call(_.values(_.pick(query, 'from', 'to')))));
        sql.group('"group"');
        if (query.group != null) {
          sql.where('"group" = ?', query.group);
          if (query.daily != null) {
            sql.field("date_trunc('day', queued)", 'day');
            sql.group("date_trunc('day', queued)");
            sql.order("date_trunc('day', queued)");
          } else {
            sql.field('attribute');
            sql.group('attribute');
          }
        }
        if (query.attribute != null) {
          sql.where('attribute = ?', query.attribute);
        }
        if ((query.filter != null) && (filter = JSON.parse(query.filter)).length) {
          expr = this.squel.expr();
          for (i = 0, len = filter.length; i < len; i++) {
            f = filter[i];
            expr.or_begin();
            if (f.status != null) {
              expr.and('status = ?', f.status);
            }
            if (f.segment != null) {
              expr.and('segment = ?', f.segment);
            }
            if (f.step != null) {
              expr.and('step = ?', f.step);
            }
            expr.end();
          }
          sql.where(expr);
        }
      } else {
        sql.from('drip_stats', 's');
        sql.where.apply(sql, ['s.id BETWEEN get_epoch_from_ts(?) and get_epoch_from_ts(?)'].concat(slice.call(_.values(_.pick(query, 'from', 'to')))));
        sql.field("REPLACE(status::text, '-', '')", 'status');
        sql.group('status');
        if (query.status != null) {
          sql.where('status = ?', query.status);
          if (query.daily != null) {
            sql.field("date_trunc('day', queued)", 'day');
            sql.group("date_trunc('day', queued)");
            sql.order("date_trunc('day', queued)");
          } else {
            sql.field("REPLACE(segment::text, '-', '')", 'segment');
            sql.group('segment');
          }
        }
        if (query.segment != null) {
          sql.field("REPLACE(step::text, '-', '')", 'step');
          sql.where('segment = ?', query.segment);
          sql.group('step');
        }
        if (query.step != null) {
          sql.where('step = ?', query.step);
        }
        if ((query.filter != null) && (filter = JSON.parse(query.filter)).length) {
          sql.from(this._setup_fromwithattributes(query), 'attrs');
          sql.where('s.id = attrs.id');
          expr = this.squel.expr();
          for (j = 0, len1 = filter.length; j < len1; j++) {
            f = filter[j];
            expr.or_begin();
            if (f.group != null) {
              expr.and('"group" = ?', f.group);
            }
            if (f.attribute != null) {
              expr.and('attribute = ?', f.attribute);
            }
            expr.end();
          }
          sql.where(expr);
        }
      }
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        return typeof cb === "function" ? cb(null, results.rows) : void 0;
      });
    };

    Summaries.prototype.direct = function(query, cb) {
      var dates, ref, sql;
      if (query == null) {
        query = {};
      }
      dates = ['from', 'to'].map(function(k) {
        return moment.utc(query[k]).valueOf() - EPOCH;
      });
      sql = (ref = this._setup_query(query).from('direct_stats').field('id')).where.apply(ref, ['id BETWEEN ? and ?'].concat(slice.call(dates))).group('id');
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        return typeof cb === "function" ? cb(null, results.rows) : void 0;
      });
    };

    Summaries.prototype.batches = function(query, cb) {
      var dates, ref, sql;
      if (query == null) {
        query = {};
      }
      dates = ['from', 'to'].map(function(k) {
        return moment.utc(query[k]).valueOf() - EPOCH;
      });
      sql = (ref = this._setup_query(query).from('batch_stats').field("REPLACE(batch::text, '-', '')", 'batch')).where.apply(ref, ['id BETWEEN ? and ?'].concat(slice.call(dates))).where('NOT batch IS NULL').group('batch');
      if (query.batch != null) {
        sql.where('batch = ?', query.batch);
      }
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        return typeof cb === "function" ? cb(null, results.rows) : void 0;
      });
    };

    Summaries.prototype.batches_by_id = function(id, cb) {
      var sql;
      sql = this._setup_query(id).from('batch_stats').where('batch = ?', id).group('batch');
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        return typeof cb === "function" ? cb(null, results.rows) : void 0;
      });
    };

    Summaries.prototype.billing = function(query, cb) {
      var dates, ref, ref1, sql;
      dates = ['from', 'to'].map(function(k) {
        return moment.utc(query[k]).valueOf() - EPOCH;
      });
      sql = (ref = (ref1 = this.squel.select().field('count(s.delivered)::int', 'sends').field('coalesce(sum(cardinality(s.clicked)), 0)::int', 'clicks').field("count(distinct(coalesce( e.doc->'agent'->>'email', e.doc->'sender'->>'email', e.doc->'mail'->>'from' )))::int", 'users').from('drip_stats', 's').from('emails', 'e').where('s.id = e.id')).where.apply(ref1, ['s.id BETWEEN ? and ?'].concat(slice.call(dates)))).where.apply(ref, ['e.id BETWEEN ? and ?'].concat(slice.call(dates))).where("NOT (e.doc ? 'sample')");
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        return typeof cb === "function" ? cb(null, results.rows[0]) : void 0;
      });
    };

    return Summaries;

  })(require('./collection/data'));

}).call(this);
