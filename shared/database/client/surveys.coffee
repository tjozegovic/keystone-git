_ = require 'lodash'

module.exports = class Surveys extends require './collection/data'
  @::type = 'form.completed'

  all: (cb) ->
    @query null, cb

  query: (query = {}, cb) ->
    sql = @squel.select()
      .from '_events'
      .where 'type = ?', 'form.completed'
      .where 'at BETWEEN ? and ?', query.from, query.to
      .order 'at', no

    sql.offset query.skip if query.skip?
    sql.limit query.size if query.size?
    @db.query sql, (err, result) =>
      return cb err if err
      return cb null unless result.rows.length

      responses = _.pluck result.rows, 'doc'
      emails = @ctx.use 'client/emails'

      # get email value from responses, make sure they are uniq, remove any that are undefined
      responseEmails = _(responses)
        .pluck 'email'
        .uniq()
        .remove (item) ->
          item isnt undefined

      emails.fetch responseEmails.value(), (err, emails) ->
        return cb err if err

        lookup = _.indexBy emails, 'id'
        _.forEach responses, (response) ->
          response.email = lookup[response.email]
        cb null, responses
