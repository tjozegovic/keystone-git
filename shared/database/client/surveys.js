(function() {
  var Surveys, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = Surveys = (function(superClass) {
    extend(Surveys, superClass);

    function Surveys() {
      return Surveys.__super__.constructor.apply(this, arguments);
    }

    Surveys.prototype.type = 'form.completed';

    Surveys.prototype.all = function(cb) {
      return this.query(null, cb);
    };

    Surveys.prototype.query = function(query, cb) {
      var sql;
      if (query == null) {
        query = {};
      }
      sql = this.squel.select().from('_events').where('type = ?', 'form.completed').where('at BETWEEN ? and ?', query.from, query.to).order('at', false);
      if (query.skip != null) {
        sql.offset(query.skip);
      }
      if (query.size != null) {
        sql.limit(query.size);
      }
      return this.db.query(sql, (function(_this) {
        return function(err, result) {
          var emails, responseEmails, responses;
          if (err) {
            return cb(err);
          }
          if (!result.rows.length) {
            return cb(null);
          }
          responses = _.pluck(result.rows, 'doc');
          emails = _this.ctx.use('client/emails');
          responseEmails = _(responses).pluck('email').uniq().remove(function(item) {
            return item !== void 0;
          });
          return emails.fetch(responseEmails.value(), function(err, emails) {
            var lookup;
            if (err) {
              return cb(err);
            }
            lookup = _.indexBy(emails, 'id');
            _.forEach(responses, function(response) {
              return response.email = lookup[response.email];
            });
            return cb(null, responses);
          });
        };
      })(this));
    };

    return Surveys;

  })(require('./collection/data'));

}).call(this);
