_ = require 'lodash'
async = require 'async'
cheerio = require 'cheerio'
handlebars = require 'handlebars'
uuid = require 'uuid'
S = require 'string'

Array::clone = -> @slice 0

module.exports = class Templates extends require './collection/data'
  @::type = 'template'

  attachments: [
    { prop: 'html', type: 'text/html' }
    { prop: 'text', type: 'text/plain' }
    { prop: 'thumbnail', type: 'img/png' }
  ]

  get: (id, cb) ->
    super id, (err, template) ->
      return cb err if err? and err.message isnt 'missing'
      return cb null, template

  save: (template, cb) ->
    {text, html} = template

    try
      handlebars.precompile html
    catch e
      error_message = e.message.substr 0, e.message.indexOf 'Expecting'
      return cb err: error_message

    # TODO: fairly ugly below, should be cleaned up
    # ... i just made it uglier :/
    template.meta = {}
    mhtml = template.meta.html = blocks: [], links: [], policies: [], variables: []
    mtext = template.meta.text = blocks: [], links: [], policies: [], variables: []

    tmphtml = blocks: {}, links: {}, policies: {}, variables: {}
    tmptext = blocks: {}, links: {}, policies: {}, variables: {}

    $ = cheerio.load html
    links = $('[data-link]')
      .map ->
        el = $ @
        name: el.data 'link'
        needs: (el.data('needs') or '').split ' '
      .get()

    _.reduce links
      , (memo, val) ->
        curr = memo[val.name] ?= needs: []
        curr.needs = _.uniq curr.needs.concat val.needs
        memo
      , tmphtml.links

    re = /data-(block|policy)=['"]?([\w ]+)['"]?/g
    while match = re.exec html
      [whole, type, name] = match
      tmphtml[switch type
        when 'block' then 'blocks'
        when 'policy' then 'policies'
      ][name] = yes

    ###
    we're looking for multiple things coming out of the handle bars template
    {{link.name.otherstuff}}
    {{>block.name}}
    {{{policy.name}}}
    {{another.completely.different.variable}}

    blocks, links, and policies are treated as special. everything else is just standard
    variables that might be made available during sample sending
    ###
    re = /\{\{\{?>?\s*(?:(?:(block|link|policy)\.([\w]+).*)?|([\w\.]+?))\s*\}\}\}?/g
    while match = re.exec html
      [whole, type, name, fullname] = match
      tmphtml[switch type
        when 'block' then 'blocks'
        when 'link' then 'links'
        when 'policy' then 'policies'
        else 'variables'
      ][if type then name else fullname] ?= yes # it's possible a link has already been found

    while match = re.exec text
      [whole, type, name, fullname] = match
      tmptext[switch type
        when 'block' then 'blocks'
        when 'link' then 'links'
        when 'policy' then 'policies'
        else 'variables'
      ][if type then name else fullname] = yes

    ['blocks', 'links', 'policies', 'variables'].forEach (piece) ->
      if piece is 'links'
        mhtml[piece] = (name: key, needs: val.needs for key, val of tmphtml[piece])
      else
        mhtml[piece] = (name: key for key of tmphtml[piece])

      mtext[piece] = (name: key for key of tmptext[piece])

    super template, (err, body) ->
      return cb err if err?
      cb null, id: body.id, version: body.version, meta: body.meta

  update: (id, template, cb) ->
    template.id = id
    @save template, cb
