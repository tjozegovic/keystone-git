(function() {
  var S, Templates, _, async, cheerio, handlebars, uuid,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  async = require('async');

  cheerio = require('cheerio');

  handlebars = require('handlebars');

  uuid = require('uuid');

  S = require('string');

  Array.prototype.clone = function() {
    return this.slice(0);
  };

  module.exports = Templates = (function(superClass) {
    extend(Templates, superClass);

    function Templates() {
      return Templates.__super__.constructor.apply(this, arguments);
    }

    Templates.prototype.type = 'template';

    Templates.prototype.attachments = [
      {
        prop: 'html',
        type: 'text/html'
      }, {
        prop: 'text',
        type: 'text/plain'
      }, {
        prop: 'thumbnail',
        type: 'img/png'
      }
    ];

    Templates.prototype.get = function(id, cb) {
      return Templates.__super__.get.call(this, id, function(err, template) {
        if ((err != null) && err.message !== 'missing') {
          return cb(err);
        }
        return cb(null, template);
      });
    };

    Templates.prototype.save = function(template, cb) {
      var $, base, e, error_message, fullname, html, links, match, mhtml, mtext, name, name1, re, text, tmphtml, tmptext, type, whole;
      text = template.text, html = template.html;
      try {
        handlebars.precompile(html);
      } catch (_error) {
        e = _error;
        error_message = e.message.substr(0, e.message.indexOf('Expecting'));
        return cb({
          err: error_message
        });
      }
      template.meta = {};
      mhtml = template.meta.html = {
        blocks: [],
        links: [],
        policies: [],
        variables: []
      };
      mtext = template.meta.text = {
        blocks: [],
        links: [],
        policies: [],
        variables: []
      };
      tmphtml = {
        blocks: {},
        links: {},
        policies: {},
        variables: {}
      };
      tmptext = {
        blocks: {},
        links: {},
        policies: {},
        variables: {}
      };
      $ = cheerio.load(html);
      links = $('[data-link]').map(function() {
        var el;
        el = $(this);
        return {
          name: el.data('link'),
          needs: (el.data('needs') || '').split(' ')
        };
      }).get();
      _.reduce(links, function(memo, val) {
        var curr, name1;
        curr = memo[name1 = val.name] != null ? memo[name1] : memo[name1] = {
          needs: []
        };
        curr.needs = _.uniq(curr.needs.concat(val.needs));
        return memo;
      }, tmphtml.links);
      re = /data-(block|policy)=['"]?([\w ]+)['"]?/g;
      while (match = re.exec(html)) {
        whole = match[0], type = match[1], name = match[2];
        tmphtml[(function() {
          switch (type) {
            case 'block':
              return 'blocks';
            case 'policy':
              return 'policies';
          }
        })()][name] = true;
      }

      /*
      we're looking for multiple things coming out of the handle bars template
      {{link.name.otherstuff}}
      {{>block.name}}
      {{{policy.name}}}
      {{another.completely.different.variable}}
      
      blocks, links, and policies are treated as special. everything else is just standard
      variables that might be made available during sample sending
       */
      re = /\{\{\{?>?\s*(?:(?:(block|link|policy)\.([\w]+).*)?|([\w\.]+?))\s*\}\}\}?/g;
      while (match = re.exec(html)) {
        whole = match[0], type = match[1], name = match[2], fullname = match[3];
        if ((base = tmphtml[(function() {
          switch (type) {
            case 'block':
              return 'blocks';
            case 'link':
              return 'links';
            case 'policy':
              return 'policies';
            default:
              return 'variables';
          }
        })()])[name1 = type ? name : fullname] == null) {
          base[name1] = true;
        }
      }
      while (match = re.exec(text)) {
        whole = match[0], type = match[1], name = match[2], fullname = match[3];
        tmptext[(function() {
          switch (type) {
            case 'block':
              return 'blocks';
            case 'link':
              return 'links';
            case 'policy':
              return 'policies';
            default:
              return 'variables';
          }
        })()][type ? name : fullname] = true;
      }
      ['blocks', 'links', 'policies', 'variables'].forEach(function(piece) {
        var key, val;
        if (piece === 'links') {
          mhtml[piece] = (function() {
            var ref, results;
            ref = tmphtml[piece];
            results = [];
            for (key in ref) {
              val = ref[key];
              results.push({
                name: key,
                needs: val.needs
              });
            }
            return results;
          })();
        } else {
          mhtml[piece] = (function() {
            var results;
            results = [];
            for (key in tmphtml[piece]) {
              results.push({
                name: key
              });
            }
            return results;
          })();
        }
        return mtext[piece] = (function() {
          var results;
          results = [];
          for (key in tmptext[piece]) {
            results.push({
              name: key
            });
          }
          return results;
        })();
      });
      return Templates.__super__.save.call(this, template, function(err, body) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, {
          id: body.id,
          version: body.version,
          meta: body.meta
        });
      });
    };

    Templates.prototype.update = function(id, template, cb) {
      template.id = id;
      return this.save(template, cb);
    };

    return Templates;

  })(require('./collection/data'));

}).call(this);
