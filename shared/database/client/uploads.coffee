_ = require 'lodash'
async = require 'async'
moment = require 'moment'

module.exports = class Uploads extends require '../scd'
  @::table = 'uploads'

  constructor: (@db) ->
    super

  get: (id, cb) ->
    sql = @squel.select()
      .from 'uploads_current'
      .field 'id'
      .field 'version'
      .field 'doc'
      .field 'type'
      .field 'status'
      .field 'file'
      .where 'id = ?', id
      .limit 1

    @db.query sql, (err, result) ->
      return cb err if err?

      first = result.rows[0]
      return cb null, null unless first?
      return cb null, _.assign first.doc, _.omit first, 'doc'

  all: (opts, cb) ->
    [cb, opts] = [opts, undefined] unless cb? and _.isFunction cb

    sql = @squel.select()
      .from "#{@table}_current"
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'doc'
      .field 'status'
      .order 'published', false

    sql.where 'type = ?', opts.type if opts.type
    sql.limit opts.limit if opts.limit

    @list sql, cb

  save: (doc, file, cb) ->
    [cb, file] = [file, undefined] unless cb? and _.isFunction cb

    upsert = =>
      sql = unless doc.id?
        @squel.insert().into 'uploads'
      else
        @squel.update()
          .table 'uploads'
          .where 'id = ?', doc.id
          .where 'version = ?', doc.version

      {type, status} = doc
      sql.set 'doc', @jsonb _.omit doc, 'id', 'version', 'file', 'type', 'status'

      sql.set 'type', type if type?
      sql.set 'status', status if status?
      sql.returning 'id, version'

      args = []
      if file?
        args.push "\\x#{file.toString 'hex'}"
        sql.set 'file', '$1', dontQuote: yes

      @db.query sql, args, (err, result) ->
        return cb err if err?
        cb null, result.rows[0]

    return upsert() if not file? or file instanceof Buffer

    bufs = []
    file.on 'data', (d) -> bufs.push d
    file.on 'end', ->
      file = Buffer.concat bufs
      upsert()
