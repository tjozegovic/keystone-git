(function() {
  var Uploads, _, async, moment,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  async = require('async');

  moment = require('moment');

  module.exports = Uploads = (function(superClass) {
    extend(Uploads, superClass);

    Uploads.prototype.table = 'uploads';

    function Uploads(db) {
      this.db = db;
      Uploads.__super__.constructor.apply(this, arguments);
    }

    Uploads.prototype.get = function(id, cb) {
      var sql;
      sql = this.squel.select().from('uploads_current').field('id').field('version').field('doc').field('type').field('status').field('file').where('id = ?', id).limit(1);
      return this.db.query(sql, function(err, result) {
        var first;
        if (err != null) {
          return cb(err);
        }
        first = result.rows[0];
        if (first == null) {
          return cb(null, null);
        }
        return cb(null, _.assign(first.doc, _.omit(first, 'doc')));
      });
    };

    Uploads.prototype.all = function(opts, cb) {
      var ref, sql;
      if (!((cb != null) && _.isFunction(cb))) {
        ref = [opts, void 0], cb = ref[0], opts = ref[1];
      }
      sql = this.squel.select().from(this.table + "_current").field("REPLACE(id::text, '-', '')", 'id').field('version').field('doc').field('status').order('published', false);
      if (opts.type) {
        sql.where('type = ?', opts.type);
      }
      if (opts.limit) {
        sql.limit(opts.limit);
      }
      return this.list(sql, cb);
    };

    Uploads.prototype.save = function(doc, file, cb) {
      var bufs, ref, upsert;
      if (!((cb != null) && _.isFunction(cb))) {
        ref = [file, void 0], cb = ref[0], file = ref[1];
      }
      upsert = (function(_this) {
        return function() {
          var args, sql, status, type;
          sql = doc.id == null ? _this.squel.insert().into('uploads') : _this.squel.update().table('uploads').where('id = ?', doc.id).where('version = ?', doc.version);
          type = doc.type, status = doc.status;
          sql.set('doc', _this.jsonb(_.omit(doc, 'id', 'version', 'file', 'type', 'status')));
          if (type != null) {
            sql.set('type', type);
          }
          if (status != null) {
            sql.set('status', status);
          }
          sql.returning('id, version');
          args = [];
          if (file != null) {
            args.push("\\x" + (file.toString('hex')));
            sql.set('file', '$1', {
              dontQuote: true
            });
          }
          return _this.db.query(sql, args, function(err, result) {
            if (err != null) {
              return cb(err);
            }
            return cb(null, result.rows[0]);
          });
        };
      })(this);
      if ((file == null) || file instanceof Buffer) {
        return upsert();
      }
      bufs = [];
      file.on('data', function(d) {
        return bufs.push(d);
      });
      return file.on('end', function() {
        file = Buffer.concat(bufs);
        return upsert();
      });
    };

    return Uploads;

  })(require('../scd'));

}).call(this);
