_ = require 'lodash'
uuid = require 'uuid'

module.exports = class ApiKeys extends require '../pgmodule'
  get: (id, cb) ->
    sql = @squel.select()
      .from 'apikeys'
      .field "REPLACE(client_id::text, '-', '')", 'client'
      .where 'id = ?', id

    @single sql, cb

  byclient: (client, cb) ->
    sql = @squel.select()
      .from 'apikeys'
      .field "REPLACE(id::text, '-', '')", 'key'
      .field 'issued'
      .where 'client_id = ?', client
      .order 'issued'

    @db.query sql, (err, results) ->
      return cb err if err?
      cb null, results.rows

  create: (client, cb) ->
    sql = @squel.insert()
      .into 'apikeys'
      .set 'client_id', client
      .returning 'id, issued'

    @single sql, cb

  all: (cb) ->
    sql = @squel.select()
      .from 'apikeys'
      .join 'clients_current ON clients_current.id=apikeys.client_id'
      .field "clients_current.doc->>'name'", 'client'
      .field "REPLACE(apikeys.id::text, '-', '')", 'apikey'
      .field 'issued'
      .field "REPLACE(clients_current.id::text, '-', '')", 'id'
      .order 'client, issued'

    @list sql, cb
