(function() {
  var ApiKeys, _, uuid,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  uuid = require('uuid');

  module.exports = ApiKeys = (function(superClass) {
    extend(ApiKeys, superClass);

    function ApiKeys() {
      return ApiKeys.__super__.constructor.apply(this, arguments);
    }

    ApiKeys.prototype.get = function(id, cb) {
      var sql;
      sql = this.squel.select().from('apikeys').field("REPLACE(client_id::text, '-', '')", 'client').where('id = ?', id);
      return this.single(sql, cb);
    };

    ApiKeys.prototype.byclient = function(client, cb) {
      var sql;
      sql = this.squel.select().from('apikeys').field("REPLACE(id::text, '-', '')", 'key').field('issued').where('client_id = ?', client).order('issued');
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, results.rows);
      });
    };

    ApiKeys.prototype.create = function(client, cb) {
      var sql;
      sql = this.squel.insert().into('apikeys').set('client_id', client).returning('id, issued');
      return this.single(sql, cb);
    };

    ApiKeys.prototype.all = function(cb) {
      var sql;
      sql = this.squel.select().from('apikeys').join('clients_current ON clients_current.id=apikeys.client_id').field("clients_current.doc->>'name'", 'client').field("REPLACE(apikeys.id::text, '-', '')", 'apikey').field('issued').field("REPLACE(clients_current.id::text, '-', '')", 'id').order('client, issued');
      return this.list(sql, cb);
    };

    return ApiKeys;

  })(require('../pgmodule'));

}).call(this);
