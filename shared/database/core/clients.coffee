module.exports = class Clients extends require '../scd'
  @::table = 'clients'

  by_shortname: (shortname, cb) ->
    sql = @_select_current()
      .where "doc ? 'short'"
      .where "lower(doc->>'short') = lower($1)"

    @single sql, [shortname], cb

  by_svemails_prefix: (prefix, cb) ->
    sql = @_select_current()
      .where "(doc->>'use_svemails')::bool"
      .where "lower(doc->'svemails'->>'prefix') = lower($1)"

    @single sql, [prefix], cb
