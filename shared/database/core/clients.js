(function() {
  var Clients,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = Clients = (function(superClass) {
    extend(Clients, superClass);

    function Clients() {
      return Clients.__super__.constructor.apply(this, arguments);
    }

    Clients.prototype.table = 'clients';

    Clients.prototype.by_shortname = function(shortname, cb) {
      var sql;
      sql = this._select_current().where("doc ? 'short'").where("lower(doc->>'short') = lower($1)");
      return this.single(sql, [shortname], cb);
    };

    Clients.prototype.by_svemails_prefix = function(prefix, cb) {
      var sql;
      sql = this._select_current().where("(doc->>'use_svemails')::bool").where("lower(doc->'svemails'->>'prefix') = lower($1)");
      return this.single(sql, [prefix], cb);
    };

    return Clients;

  })(require('../scd'));

}).call(this);
