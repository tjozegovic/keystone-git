module.exports = class CNames extends require './module'
  @::collection = 'cnames'

  get: (cname, cb) ->
    sql = @squel.select()
      .from 'clients_current'
      .from "jsonb_array_elements(doc->'cnames')", 'cname'
      .field "REPLACE(id::text, '-', '')", 'id'
      .where "(doc->'cnames') ? $1 OR (cname->>'host') = $1"
      .limit 1

    @db.query sql, [cname], (err, results) ->
      return cb err if err
      cb null, results.rows[0]?.id
