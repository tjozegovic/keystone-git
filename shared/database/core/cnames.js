(function() {
  var CNames,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = CNames = (function(superClass) {
    extend(CNames, superClass);

    function CNames() {
      return CNames.__super__.constructor.apply(this, arguments);
    }

    CNames.prototype.collection = 'cnames';

    CNames.prototype.get = function(cname, cb) {
      var sql;
      sql = this.squel.select().from('clients_current').from("jsonb_array_elements(doc->'cnames')", 'cname').field("REPLACE(id::text, '-', '')", 'id').where("(doc->'cnames') ? $1 OR (cname->>'host') = $1").limit(1);
      return this.db.query(sql, [cname], function(err, results) {
        var ref;
        if (err) {
          return cb(err);
        }
        return cb(null, (ref = results.rows[0]) != null ? ref.id : void 0);
      });
    };

    return CNames;

  })(require('./module'));

}).call(this);
