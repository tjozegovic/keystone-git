module.exports = class Events extends require '../pgmodule'
  save: (event, cb) ->
    sql = @squel.insert()
      .into 'events'
      .set 'type', event.type
      .set 'at', 'NOW()', dontQuote: yes
      .set 'doc', "$$#{JSON.stringify event}$$", dontQuote: yes
      .returning 'id'

    @db.query sql, (err, results) ->
      return cb err if err
      cb null, id: results.rows[0].id
