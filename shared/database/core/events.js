(function() {
  var Events,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = Events = (function(superClass) {
    extend(Events, superClass);

    function Events() {
      return Events.__super__.constructor.apply(this, arguments);
    }

    Events.prototype.save = function(event, cb) {
      var sql;
      sql = this.squel.insert().into('events').set('type', event.type).set('at', 'NOW()', {
        dontQuote: true
      }).set('doc', "$$" + (JSON.stringify(event)) + "$$", {
        dontQuote: true
      }).returning('id');
      return this.db.query(sql, function(err, results) {
        if (err) {
          return cb(err);
        }
        return cb(null, {
          id: results.rows[0].id
        });
      });
    };

    return Events;

  })(require('../pgmodule'));

}).call(this);
