_ = require 'lodash'
moment = require 'moment'
squel = require 'squel'
uuid = require 'uuid'

{CronTime} = require 'cron'

class MockLogger
  constructor: ->
    ['debug', 'info', 'warn'].forEach (lvl) =>
      @[lvl] = ->
        console.log lvl, arguments...

module.exports = class Jobs extends require './module'
  @::collection = 'jobs'

  constructor: (@db) ->
    super @db
    @ctx = {}

    Object.defineProperty @, 'logger',
      __proto__: null
      get: => @ctx.logger ? (@ctx.logger = new MockLogger())
      set: (val) => @ctx.logger = val

  get: (id, cb) ->
    sql = squel.select()
      .from 'jobs'
      .field 'id'
      .field 'at'
      .field 'status'
      .field 'action'
      .field 'repeat'
      .field 'data'
      .where 'id = ?', id
      .order 'at'

    # TODO is core/module:@single necessary to override the pgmodule:@single?
    @db.query sql, (err, results) =>
      return cb? err if err?
      return cb? null, null unless results.rows?[0]?
      cb? null, new Job @, results.rows[0]

  fetch: (ids, cb) ->
    return cb null, [] unless ids.length

    sql = @squel.select()
      .from 'jobs'
      .field 'id'
      .field 'at'
      .field 'status'
      .field 'action'
      .field 'repeat'
      .field 'data'
      .where "id IN ('#{ids.join '\', \''}')"

    @db.query sql, (err, result) ->
      return cb err if err
      cb null, result.rows

  get_by_client: (id, cb) ->
    sql = @squel.select()
      .from 'jobs'
      .field 'id'
      .field 'at'
      .field 'status'
      .field 'action'
      .field 'repeat'
      .field 'data'
      .where "data ->> 'client' = ?", id
      .where 'status = ?', 'pending'
      .order 'at'
      .limit 50

    @db.query sql, (err, result) ->
      return cb err if err
      cb null, result.rows

  pending: (opts, cb) ->
    [opts, cb] = [{}, opts] unless cb or not _.isFunction opts

    sql = squel.select()
      .from 'jobs'
      .field 'id'
      .field 'at'
      .field 'status'
      .field 'action'
      .field 'repeat'
      .field 'data'
      .where 'status = ?', 'pending'
      # TODO squel.registerValueHandler doesn't pick this one up
      .where 'at <= ?', (opts.at ? moment()).toISOString()
      .order 'at'
      .limit opts.limit or 50

    @db.query sql, (err, result) ->
      return cb err if err
      cb null, result.rows

  save: (doc, cb) ->
    @before_save?.call @, doc
    {at, action, status} = doc

    unless action?
      return process.nextTick -> cb new Error 'Action must be defined when saving a job.'

    sql = unless doc.id?
      @squel.insert()
        .into 'jobs'
    else
      @squel.update()
        .table 'jobs'
        .set 'updated', moment()
        .where 'id = ?', doc.id

    sql
      .set 'at', at or moment()
      .set 'status', status or 'pending'
      .set 'action', action
      .set 'data', @jsonb doc.data
      .returning 'id'

    @db.query sql, (err, results) ->
      return cb err if err
      cb null, results.rows[0]

module.exports.Job = class Job
  constructor: (@jobs, @doc) ->
    ['id', 'action', 'data', 'status', 'repeat'].forEach (prop) =>
      Object.defineProperty @, prop,
        __proto__: null
        value: @doc[prop]

  save = (status, err, cb) ->
    [err, cb] = [null, err] unless cb

    @doc.at ?= new Date
    @doc.status = status
    @doc.updated = new Date

    if err?
      @doc.lasterror = err if err
    else if @doc.lasterror?
      delete @doc.lasterror

    @jobs.save @doc, (err, body) =>
      cb? err
      return @jobs.logger.error 'saving job error', id: @doc.id, err: err if err?

      return unless status in ['errored', 'processed']
      return unless (repeat = @doc.repeat)?

      ct = new CronTime repeat
      next = ct.sendAt().milliseconds 0 # just for cleanliness

      @doc.at = next.toISOString()
      @doc.status = 'pending'

      @jobs.save @doc, (err) =>
        @jobs.logger.error 'setting next cron error', id: @doc.id, err: err if err
        console.error 'could not set next cron job!', err if err

  running: (cb) -> save.call @, 'running', cb
  processed: (cb) -> save.call @, 'processed', cb
  timedout: (cb) -> save.call @, 'timedout', cb
  canceled: (cb) -> save.call @, 'canceled', cb
  errored: (err, cb) -> save.call @, 'errored', err, cb
