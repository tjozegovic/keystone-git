(function() {
  var CronTime, Job, Jobs, MockLogger, _, moment, squel, uuid,
    slice = [].slice,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  moment = require('moment');

  squel = require('squel');

  uuid = require('uuid');

  CronTime = require('cron').CronTime;

  MockLogger = (function() {
    function MockLogger() {
      ['debug', 'info', 'warn'].forEach((function(_this) {
        return function(lvl) {
          return _this[lvl] = function() {
            return console.log.apply(console, [lvl].concat(slice.call(arguments)));
          };
        };
      })(this));
    }

    return MockLogger;

  })();

  module.exports = Jobs = (function(superClass) {
    extend(Jobs, superClass);

    Jobs.prototype.collection = 'jobs';

    function Jobs(db) {
      this.db = db;
      Jobs.__super__.constructor.call(this, this.db);
      this.ctx = {};
      Object.defineProperty(this, 'logger', {
        __proto__: null,
        get: (function(_this) {
          return function() {
            var ref;
            return (ref = _this.ctx.logger) != null ? ref : (_this.ctx.logger = new MockLogger());
          };
        })(this),
        set: (function(_this) {
          return function(val) {
            return _this.ctx.logger = val;
          };
        })(this)
      });
    }

    Jobs.prototype.get = function(id, cb) {
      var sql;
      sql = squel.select().from('jobs').field('id').field('at').field('status').field('action').field('repeat').field('data').where('id = ?', id).order('at');
      return this.db.query(sql, (function(_this) {
        return function(err, results) {
          var ref;
          if (err != null) {
            return typeof cb === "function" ? cb(err) : void 0;
          }
          if (((ref = results.rows) != null ? ref[0] : void 0) == null) {
            return typeof cb === "function" ? cb(null, null) : void 0;
          }
          return typeof cb === "function" ? cb(null, new Job(_this, results.rows[0])) : void 0;
        };
      })(this));
    };

    Jobs.prototype.fetch = function(ids, cb) {
      var sql;
      if (!ids.length) {
        return cb(null, []);
      }
      sql = this.squel.select().from('jobs').field('id').field('at').field('status').field('action').field('repeat').field('data').where("id IN ('" + (ids.join('\', \'')) + "')");
      return this.db.query(sql, function(err, result) {
        if (err) {
          return cb(err);
        }
        return cb(null, result.rows);
      });
    };

    Jobs.prototype.get_by_client = function(id, cb) {
      var sql;
      sql = this.squel.select().from('jobs').field('id').field('at').field('status').field('action').field('repeat').field('data').where("data ->> 'client' = ?", id).where('status = ?', 'pending').order('at').limit(50);
      return this.db.query(sql, function(err, result) {
        if (err) {
          return cb(err);
        }
        return cb(null, result.rows);
      });
    };

    Jobs.prototype.pending = function(opts, cb) {
      var ref, ref1, sql;
      if (!(cb || !_.isFunction(opts))) {
        ref = [{}, opts], opts = ref[0], cb = ref[1];
      }
      sql = squel.select().from('jobs').field('id').field('at').field('status').field('action').field('repeat').field('data').where('status = ?', 'pending').where('at <= ?', ((ref1 = opts.at) != null ? ref1 : moment()).toISOString()).order('at').limit(opts.limit || 50);
      return this.db.query(sql, function(err, result) {
        if (err) {
          return cb(err);
        }
        return cb(null, result.rows);
      });
    };

    Jobs.prototype.save = function(doc, cb) {
      var action, at, ref, sql, status;
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      at = doc.at, action = doc.action, status = doc.status;
      if (action == null) {
        return process.nextTick(function() {
          return cb(new Error('Action must be defined when saving a job.'));
        });
      }
      sql = doc.id == null ? this.squel.insert().into('jobs') : this.squel.update().table('jobs').set('updated', moment()).where('id = ?', doc.id);
      sql.set('at', at || moment()).set('status', status || 'pending').set('action', action).set('data', this.jsonb(doc.data)).returning('id');
      return this.db.query(sql, function(err, results) {
        if (err) {
          return cb(err);
        }
        return cb(null, results.rows[0]);
      });
    };

    return Jobs;

  })(require('./module'));

  module.exports.Job = Job = (function() {
    var save;

    function Job(jobs, doc1) {
      this.jobs = jobs;
      this.doc = doc1;
      ['id', 'action', 'data', 'status', 'repeat'].forEach((function(_this) {
        return function(prop) {
          return Object.defineProperty(_this, prop, {
            __proto__: null,
            value: _this.doc[prop]
          });
        };
      })(this));
    }

    save = function(status, err, cb) {
      var base, ref;
      if (!cb) {
        ref = [null, err], err = ref[0], cb = ref[1];
      }
      if ((base = this.doc).at == null) {
        base.at = new Date;
      }
      this.doc.status = status;
      this.doc.updated = new Date;
      if (err != null) {
        if (err) {
          this.doc.lasterror = err;
        }
      } else if (this.doc.lasterror != null) {
        delete this.doc.lasterror;
      }
      return this.jobs.save(this.doc, (function(_this) {
        return function(err, body) {
          var ct, next, repeat;
          if (typeof cb === "function") {
            cb(err);
          }
          if (err != null) {
            return _this.jobs.logger.error('saving job error', {
              id: _this.doc.id,
              err: err
            });
          }
          if (status !== 'errored' && status !== 'processed') {
            return;
          }
          if ((repeat = _this.doc.repeat) == null) {
            return;
          }
          ct = new CronTime(repeat);
          next = ct.sendAt().milliseconds(0);
          _this.doc.at = next.toISOString();
          _this.doc.status = 'pending';
          return _this.jobs.save(_this.doc, function(err) {
            if (err) {
              _this.jobs.logger.error('setting next cron error', {
                id: _this.doc.id,
                err: err
              });
            }
            if (err) {
              return console.error('could not set next cron job!', err);
            }
          });
        };
      })(this));
    };

    Job.prototype.running = function(cb) {
      return save.call(this, 'running', cb);
    };

    Job.prototype.processed = function(cb) {
      return save.call(this, 'processed', cb);
    };

    Job.prototype.timedout = function(cb) {
      return save.call(this, 'timedout', cb);
    };

    Job.prototype.canceled = function(cb) {
      return save.call(this, 'canceled', cb);
    };

    Job.prototype.errored = function(err, cb) {
      return save.call(this, 'errored', err, cb);
    };

    return Job;

  })();

}).call(this);
