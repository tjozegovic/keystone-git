_ = require 'lodash'
uuid = require 'uuid'

module.exports = class ApiKeys extends require '../pgmodule'
  get: (id, cb) ->
    sql = @squel.select()
      .field "REPLACE(id::text, '-', '')", 'id'
      .field "REPLACE(client::text, '-', '')", 'client'
      .field 'source'
      .field 'at'
      .field 'doc'
      .from 'logs'
      .where 'id = ?', id

    @single sql, cb

  insert: (doc, cb) ->
    sql = @squel.insert()
      .into 'logs'

    # pull client and source from doc up into column inserts, delete from JSON
    ['client', 'source', 'at'].forEach (col) ->
      return unless doc[col]?
      sql.set col, doc[col]
      delete doc[col]

    sql.set 'doc', "$$#{JSON.stringify doc}$$", dontQuote: yes
    sql.returning 'id'

    @single sql, cb
