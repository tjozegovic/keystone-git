(function() {
  var ApiKeys, _, uuid,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  uuid = require('uuid');

  module.exports = ApiKeys = (function(superClass) {
    extend(ApiKeys, superClass);

    function ApiKeys() {
      return ApiKeys.__super__.constructor.apply(this, arguments);
    }

    ApiKeys.prototype.get = function(id, cb) {
      var sql;
      sql = this.squel.select().field("REPLACE(id::text, '-', '')", 'id').field("REPLACE(client::text, '-', '')", 'client').field('source').field('at').field('doc').from('logs').where('id = ?', id);
      return this.single(sql, cb);
    };

    ApiKeys.prototype.insert = function(doc, cb) {
      var sql;
      sql = this.squel.insert().into('logs');
      ['client', 'source', 'at'].forEach(function(col) {
        if (doc[col] == null) {
          return;
        }
        sql.set(col, doc[col]);
        return delete doc[col];
      });
      sql.set('doc', "$$" + (JSON.stringify(doc)) + "$$", {
        dontQuote: true
      });
      sql.returning('id');
      return this.single(sql, cb);
    };

    return ApiKeys;

  })(require('../pgmodule'));

}).call(this);
