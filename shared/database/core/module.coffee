_ = require 'lodash'

module.exports = class CoreModule extends require '../pgmodule'
  constructor: (@db, @ctx) ->

  before_save: (doc) ->
    doc.type ?= @type if @type?
    doc.active ?= yes
    doc.created ?= new Date
    doc.updated = new Date

  transform: (doc) ->
    _.defaults {}, id: doc.id, version: doc.version, doc

  single: (sql, cb) ->
    @db.query sql, (err, result) =>
      return cb err if err

      first = result.rows[0]
      return cb null, null unless first?

      doc = _.assign first.doc, _.omit first, 'doc'
      cb null, @transform?.call(@, doc) or doc

  list: (sql, cb) ->
    @db.query sql, (err, result) =>
      return cb err if err
      cb null, _.map result.rows, (row) =>
        doc = _.assign row.doc, _.omit row, 'doc'
        @transform?.call(@, row.doc) or doc

  get: (id, cb) ->
    sql = @squel.select()
      .from @collection
      .field 'id'
      .field 'doc'
      .where 'id = ?', id

    sql.limit 1
    @single sql, cb

  fetch: (ids, cb) ->
    sql = @squel.select()
      .from @collection
      .field 'id'
      .field 'doc'
      .where "id IN ('#{ids.join '\', \''}')"

    @list sql, cb

  all: (cb) ->
    @allfiltered null, cb

  allfiltered: (filter, cb) ->
    sql = @squel.select()
      .from @collection
      .field 'id'
      .field 'doc'

    if filter?
      sql.where filter

    @list sql, cb

  active: (cb) ->
    sql = @squel.select()
      .from @collection
      .field 'id'
      .field 'doc'

    sql.where "not (doc ? 'archive')"

    @list sql, cb

  save: (doc, cb) ->
    doc.id ?= @newid()
    @before_save?.call @, doc

    json = JSON.stringify doc

    # TODO promises!
    @db.multiple (multi) =>
      multi.query 'BEGIN'
      multi.query """
UPDATE #{@collection} SET
  updated = NOW(),
  doc = $2::jsonb
WHERE id = $1""", [doc.id, json], (err) =>
        if err?
          multi.close err
          return cb err

        multi.query """
INSERT INTO #{@collection} (id, updated, doc)
SELECT $1, NOW(), $2::jsonb
WHERE NOT EXISTS (SELECT 1 FROM #{@collection} WHERE id = $1);""", [doc.id, json], (err) ->
          if err?
            multi.close err
            return cb err

          multi.query 'COMMIT', (err) ->
            multi.close err
            return cb err if err?
            cb null, id: doc.id
