(function() {
  var CoreModule, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = CoreModule = (function(superClass) {
    extend(CoreModule, superClass);

    function CoreModule(db, ctx) {
      this.db = db;
      this.ctx = ctx;
    }

    CoreModule.prototype.before_save = function(doc) {
      if (this.type != null) {
        if (doc.type == null) {
          doc.type = this.type;
        }
      }
      if (doc.active == null) {
        doc.active = true;
      }
      if (doc.created == null) {
        doc.created = new Date;
      }
      return doc.updated = new Date;
    };

    CoreModule.prototype.transform = function(doc) {
      return _.defaults({}, {
        id: doc.id,
        version: doc.version
      }, doc);
    };

    CoreModule.prototype.single = function(sql, cb) {
      return this.db.query(sql, (function(_this) {
        return function(err, result) {
          var doc, first, ref;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          if (first == null) {
            return cb(null, null);
          }
          doc = _.assign(first.doc, _.omit(first, 'doc'));
          return cb(null, ((ref = _this.transform) != null ? ref.call(_this, doc) : void 0) || doc);
        };
      })(this));
    };

    CoreModule.prototype.list = function(sql, cb) {
      return this.db.query(sql, (function(_this) {
        return function(err, result) {
          if (err) {
            return cb(err);
          }
          return cb(null, _.map(result.rows, function(row) {
            var doc, ref;
            doc = _.assign(row.doc, _.omit(row, 'doc'));
            return ((ref = _this.transform) != null ? ref.call(_this, row.doc) : void 0) || doc;
          }));
        };
      })(this));
    };

    CoreModule.prototype.get = function(id, cb) {
      var sql;
      sql = this.squel.select().from(this.collection).field('id').field('doc').where('id = ?', id);
      sql.limit(1);
      return this.single(sql, cb);
    };

    CoreModule.prototype.fetch = function(ids, cb) {
      var sql;
      sql = this.squel.select().from(this.collection).field('id').field('doc').where("id IN ('" + (ids.join('\', \'')) + "')");
      return this.list(sql, cb);
    };

    CoreModule.prototype.all = function(cb) {
      return this.allfiltered(null, cb);
    };

    CoreModule.prototype.allfiltered = function(filter, cb) {
      var sql;
      sql = this.squel.select().from(this.collection).field('id').field('doc');
      if (filter != null) {
        sql.where(filter);
      }
      return this.list(sql, cb);
    };

    CoreModule.prototype.active = function(cb) {
      var sql;
      sql = this.squel.select().from(this.collection).field('id').field('doc');
      sql.where("not (doc ? 'archive')");
      return this.list(sql, cb);
    };

    CoreModule.prototype.save = function(doc, cb) {
      var json, ref;
      if (doc.id == null) {
        doc.id = this.newid();
      }
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      json = JSON.stringify(doc);
      return this.db.multiple((function(_this) {
        return function(multi) {
          multi.query('BEGIN');
          return multi.query("UPDATE " + _this.collection + " SET\n  updated = NOW(),\n  doc = $2::jsonb\nWHERE id = $1", [doc.id, json], function(err) {
            if (err != null) {
              multi.close(err);
              return cb(err);
            }
            return multi.query("INSERT INTO " + _this.collection + " (id, updated, doc)\nSELECT $1, NOW(), $2::jsonb\nWHERE NOT EXISTS (SELECT 1 FROM " + _this.collection + " WHERE id = $1);", [doc.id, json], function(err) {
              if (err != null) {
                multi.close(err);
                return cb(err);
              }
              return multi.query('COMMIT', function(err) {
                multi.close(err);
                if (err != null) {
                  return cb(err);
                }
                return cb(null, {
                  id: doc.id
                });
              });
            });
          });
        };
      })(this));
    };

    return CoreModule;

  })(require('../pgmodule'));

}).call(this);
