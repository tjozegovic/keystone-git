_ = require 'lodash'

module.exports = class Optouts extends require './module'
  @::collection = 'optouts'
  @::type = 'optout'

  fix: (email) -> email?.trim().toLowerCase()

  transform: (doc) ->
    doc = super doc
    _.defaults doc, global: no, clients: [], history: []

  get: (email, cb) ->
    email = @fix email
    super email, cb

  optin: (email, client, data, cb) ->
    [cb, data] = [data, undefined] unless cb? and _.isFunction cb
    email = @fix email

    @get email, (err, doc = {}) =>
      return cb err if err?

      doc.id ?= email
      doc.history ?= []

      unless client?
        doc.global = no

        doc.history.unshift _.defaults
          type: 'global optin'
          at: new Date()
        , _.assign from: 'website', data
      else
        _.pull doc.clients or [], client

        doc.history.unshift _.defaults
          type: 'optin'
          at: new Date()
          client: client
        , _.assign from: 'website', data

      @save doc, cb

  optout: (email, client, data, cb) ->
    [cb, data] = [data, undefined] unless cb? and _.isFunction cb
    email = @fix email

    @get email, (err, doc = {}) =>
      return cb err if err?

      doc.id ?= email
      doc.history ?= []

      unless client?
        doc.global = yes

        doc.history.unshift _.defaults
          type: 'global optout'
          at: new Date()
        , _.assign from: 'website', data
      else
        unless client in doc.clients ?= []
          doc.clients.push client
          doc.clients.sort()

        doc.history.unshift _.defaults
          type: 'optout'
          at: new Date()
          client: client
        , _.assign from: 'website', data

      @save doc, cb

  since: (client, since, cb) ->
    @db.query """
      SELECT o.id, json_agg(json_build_object('type', x.type, 'at', x.at, 'from', x.from)) AS history
      FROM optouts o, jsonb_to_recordset(o.doc->'history') AS x(client uuid, at timestamp, type text, "from" text)
      WHERE
        o.updated > $2
        AND (o.doc->'clients') ? $1
        AND x.client = $1::uuid AND x.at > $2 AND x.type = 'optout'
      GROUP BY o.id
    """, [client, since], (err, results) ->
      return cb err if err?
      cb null, results.rows
