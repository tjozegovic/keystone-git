(function() {
  var Optouts, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  _ = require('lodash');

  module.exports = Optouts = (function(superClass) {
    extend(Optouts, superClass);

    function Optouts() {
      return Optouts.__super__.constructor.apply(this, arguments);
    }

    Optouts.prototype.collection = 'optouts';

    Optouts.prototype.type = 'optout';

    Optouts.prototype.fix = function(email) {
      return email != null ? email.trim().toLowerCase() : void 0;
    };

    Optouts.prototype.transform = function(doc) {
      doc = Optouts.__super__.transform.call(this, doc);
      return _.defaults(doc, {
        global: false,
        clients: [],
        history: []
      });
    };

    Optouts.prototype.get = function(email, cb) {
      email = this.fix(email);
      return Optouts.__super__.get.call(this, email, cb);
    };

    Optouts.prototype.optin = function(email, client, data, cb) {
      var ref;
      if (!((cb != null) && _.isFunction(cb))) {
        ref = [data, void 0], cb = ref[0], data = ref[1];
      }
      email = this.fix(email);
      return this.get(email, (function(_this) {
        return function(err, doc) {
          if (doc == null) {
            doc = {};
          }
          if (err != null) {
            return cb(err);
          }
          if (doc.id == null) {
            doc.id = email;
          }
          if (doc.history == null) {
            doc.history = [];
          }
          if (client == null) {
            doc.global = false;
            doc.history.unshift(_.defaults({
              type: 'global optin',
              at: new Date()
            }, _.assign({
              from: 'website'
            }, data)));
          } else {
            _.pull(doc.clients || [], client);
            doc.history.unshift(_.defaults({
              type: 'optin',
              at: new Date(),
              client: client
            }, _.assign({
              from: 'website'
            }, data)));
          }
          return _this.save(doc, cb);
        };
      })(this));
    };

    Optouts.prototype.optout = function(email, client, data, cb) {
      var ref;
      if (!((cb != null) && _.isFunction(cb))) {
        ref = [data, void 0], cb = ref[0], data = ref[1];
      }
      email = this.fix(email);
      return this.get(email, (function(_this) {
        return function(err, doc) {
          if (doc == null) {
            doc = {};
          }
          if (err != null) {
            return cb(err);
          }
          if (doc.id == null) {
            doc.id = email;
          }
          if (doc.history == null) {
            doc.history = [];
          }
          if (client == null) {
            doc.global = true;
            doc.history.unshift(_.defaults({
              type: 'global optout',
              at: new Date()
            }, _.assign({
              from: 'website'
            }, data)));
          } else {
            if (indexOf.call(doc.clients != null ? doc.clients : doc.clients = [], client) < 0) {
              doc.clients.push(client);
              doc.clients.sort();
            }
            doc.history.unshift(_.defaults({
              type: 'optout',
              at: new Date(),
              client: client
            }, _.assign({
              from: 'website'
            }, data)));
          }
          return _this.save(doc, cb);
        };
      })(this));
    };

    Optouts.prototype.since = function(client, since, cb) {
      return this.db.query("SELECT o.id, json_agg(json_build_object('type', x.type, 'at', x.at, 'from', x.from)) AS history\nFROM optouts o, jsonb_to_recordset(o.doc->'history') AS x(client uuid, at timestamp, type text, \"from\" text)\nWHERE\n  o.updated > $2\n  AND (o.doc->'clients') ? $1\n  AND x.client = $1::uuid AND x.at > $2 AND x.type = 'optout'\nGROUP BY o.id", [client, since], function(err, results) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, results.rows);
      });
    };

    return Optouts;

  })(require('./module'));

}).call(this);
