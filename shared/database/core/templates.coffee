_ = require 'lodash'

module.exports = class Templates extends require '../scd'
  @::table = 'templates'

  before_save: (doc) ->
    doc.type ?= @type if @type?
    doc.active ?= yes
    doc.created ?= new Date
    doc.updated = new Date

  get: (name, cb) ->
    sql = @squel.select()
      .from 'templates'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'doc'
      .field 'version'
      .where "upper(templates.valid) = 'infinity'::timestamp with time zone"
      .where 'name = ?', name

    @single sql, cb

  get_by_client: (client, cb) ->
    sql = @squel.select()
      .from 'templates'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'doc'
      .field 'version'
      .where "upper(templates.valid) = 'infinity'::timestamp with time zone"
      .where 'client = ?', client

    @single sql, cb

  fetch: (ids, cb) ->
    return cb null, [] unless ids.length

    sql = @squel.select()
      .from 'templates'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'doc'
      .field 'name'
      .where "upper(templates.valid) = 'infinity'::timestamp with time zone"
      .where "name IN ('#{ids.join '\', \''}')"
      .where "client is null"

    @list sql, cb

  save: (doc, name, cb) ->
    @before_save?.call @, doc
    json = JSON.stringify _.omit doc, 'id', 'version', 'valid', 'name'

    unless doc.id?
      sql = @squel.insert()
        .into 'templates'
        .set 'doc', "$$#{json}$$", dontQuote: yes
        .set 'name', name
        .returning "REPLACE(id::text, '-', '') AS id, version"

      if @type?
        sql.set 'type', @type

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id, version: first.version
    else
      sql = @squel.update()
        .table 'templates'
        .set 'doc', "$$#{json}$$", dontQuote: yes
        .where 'id = ?', doc.id
        .where 'version = ?', doc.version
        .returning "REPLACE(id::text, '-', '') AS id, version"

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id, version: first.version

  save_client_template: (doc, client, cb) ->
    @before_save?.call @, doc
    json = JSON.stringify _.omit doc, 'id', 'version', 'valid', 'name', 'client'

    unless doc.id?
      sql = @squel.insert()
        .into 'templates'
        .set 'doc', "$$#{json}$$", dontQuote: yes
        .set 'client', client
        .set 'name', 'vunotification'
        .returning "REPLACE(id::text, '-', '') AS id, version"

      if @type?
        sql.set 'type', @type

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id, version: first.version
    else
      sql = @squel.update()
        .table 'templates'
        .set 'doc', "$$#{json}$$", dontQuote: yes
        .where 'id = ?', doc.id
        .where 'version = ?', doc.version
        .returning "REPLACE(id::text, '-', '') AS id, version"

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id, version: first.version
