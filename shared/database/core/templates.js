(function() {
  var Templates, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = Templates = (function(superClass) {
    extend(Templates, superClass);

    function Templates() {
      return Templates.__super__.constructor.apply(this, arguments);
    }

    Templates.prototype.table = 'templates';

    Templates.prototype.before_save = function(doc) {
      if (this.type != null) {
        if (doc.type == null) {
          doc.type = this.type;
        }
      }
      if (doc.active == null) {
        doc.active = true;
      }
      if (doc.created == null) {
        doc.created = new Date;
      }
      return doc.updated = new Date;
    };

    Templates.prototype.get = function(name, cb) {
      var sql;
      sql = this.squel.select().from('templates').field("REPLACE(id::text, '-', '')", 'id').field('doc').field('version').where("upper(templates.valid) = 'infinity'::timestamp with time zone").where('name = ?', name);
      return this.single(sql, cb);
    };

    Templates.prototype.get_by_client = function(client, cb) {
      var sql;
      sql = this.squel.select().from('templates').field("REPLACE(id::text, '-', '')", 'id').field('doc').field('version').where("upper(templates.valid) = 'infinity'::timestamp with time zone").where('client = ?', client);
      return this.single(sql, cb);
    };

    Templates.prototype.fetch = function(ids, cb) {
      var sql;
      if (!ids.length) {
        return cb(null, []);
      }
      sql = this.squel.select().from('templates').field("REPLACE(id::text, '-', '')", 'id').field('version').field('doc').field('name').where("upper(templates.valid) = 'infinity'::timestamp with time zone").where("name IN ('" + (ids.join('\', \'')) + "')").where("client is null");
      return this.list(sql, cb);
    };

    Templates.prototype.save = function(doc, name, cb) {
      var json, ref, sql;
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      json = JSON.stringify(_.omit(doc, 'id', 'version', 'valid', 'name'));
      if (doc.id == null) {
        sql = this.squel.insert().into('templates').set('doc', "$$" + json + "$$", {
          dontQuote: true
        }).set('name', name).returning("REPLACE(id::text, '-', '') AS id, version");
        if (this.type != null) {
          sql.set('type', this.type);
        }
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      } else {
        sql = this.squel.update().table('templates').set('doc', "$$" + json + "$$", {
          dontQuote: true
        }).where('id = ?', doc.id).where('version = ?', doc.version).returning("REPLACE(id::text, '-', '') AS id, version");
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      }
    };

    Templates.prototype.save_client_template = function(doc, client, cb) {
      var json, ref, sql;
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      json = JSON.stringify(_.omit(doc, 'id', 'version', 'valid', 'name', 'client'));
      if (doc.id == null) {
        sql = this.squel.insert().into('templates').set('doc', "$$" + json + "$$", {
          dontQuote: true
        }).set('client', client).set('name', 'vunotification').returning("REPLACE(id::text, '-', '') AS id, version");
        if (this.type != null) {
          sql.set('type', this.type);
        }
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      } else {
        sql = this.squel.update().table('templates').set('doc', "$$" + json + "$$", {
          dontQuote: true
        }).where('id = ?', doc.id).where('version = ?', doc.version).returning("REPLACE(id::text, '-', '') AS id, version");
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      }
    };

    return Templates;

  })(require('../scd'));

}).call(this);
