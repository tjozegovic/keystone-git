_ = require 'lodash'

module.exports = class Users extends require '../scd'
  @::table = 'users'

  transform: (doc) ->
    doc.username ?= doc.id
    doc

  get: (username, cb) ->
    sql = @squel.select()
      .from 'users_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'username'
      .field 'doc'
      .field 'version'
      .field 'published'
      .where 'username = ?', username
      .limit 1

    @single sql, cb

  getByToken: (token, cb) ->
    sql = @squel.select()
      .from 'users_current'
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'username'
      .field 'doc'
      .field 'version'
      .field 'published'
      .where "doc->>'token' = ?", token
      .limit 1

    @single sql, cb

  save: (doc, cb) ->
    @before_save?.call @, doc

    unless doc.id?
      sql = @squel.insert()
        .into 'users'
        .set 'username', doc.username
        .set 'doc', @jsonb doc
        .returning 'id, version'

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id, version: first.version
    else
      sql = @squel.update()
        .table 'users'
        .set 'doc', @jsonb doc
        .where 'username = ?', doc.username
        .where 'version = ?', doc.version
        .returning 'id, version'

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id, version: first.version

  all: (cb) ->
    sql = @squel.select()
      .from "users_current"
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'published'
      .field 'username'
      .field 'doc'

    @list sql, cb
