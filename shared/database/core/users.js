(function() {
  var Users, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = Users = (function(superClass) {
    extend(Users, superClass);

    function Users() {
      return Users.__super__.constructor.apply(this, arguments);
    }

    Users.prototype.table = 'users';

    Users.prototype.transform = function(doc) {
      if (doc.username == null) {
        doc.username = doc.id;
      }
      return doc;
    };

    Users.prototype.get = function(username, cb) {
      var sql;
      sql = this.squel.select().from('users_current').field("REPLACE(id::text, '-', '')", 'id').field('username').field('doc').field('version').field('published').where('username = ?', username).limit(1);
      return this.single(sql, cb);
    };

    Users.prototype.getByToken = function(token, cb) {
      var sql;
      sql = this.squel.select().from('users_current').field("REPLACE(id::text, '-', '')", 'id').field('username').field('doc').field('version').field('published').where("doc->>'token' = ?", token).limit(1);
      return this.single(sql, cb);
    };

    Users.prototype.save = function(doc, cb) {
      var ref, sql;
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      if (doc.id == null) {
        sql = this.squel.insert().into('users').set('username', doc.username).set('doc', this.jsonb(doc)).returning('id, version');
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      } else {
        sql = this.squel.update().table('users').set('doc', this.jsonb(doc)).where('username = ?', doc.username).where('version = ?', doc.version).returning('id, version');
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      }
    };

    Users.prototype.all = function(cb) {
      var sql;
      sql = this.squel.select().from("users_current").field("REPLACE(id::text, '-', '')", 'id').field('version').field('published').field('username').field('doc');
      return this.list(sql, cb);
    };

    return Users;

  })(require('../scd'));

}).call(this);
