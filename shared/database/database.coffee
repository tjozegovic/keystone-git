config = require 'config'

_ = require 'lodash'
chalk = require 'chalk'
{Console} = require 'console'

process_name = do -> require('shared/utils/process').get_name()

module.exports = class Database
  @slow: 750
  @ignore: ['23505']
  LOGGER: console

  constructor: ->
    _.assign @, _.pick @constructor, 'slow', 'ignore'

  getpath: ->
    path = ['keystone']
    path.unshift "client_#{@client}" if @client
    "SET search_path TO '#{path.join '\', \''}';"

  setpath: (client, cb) ->
    client.query @getpath(), cb

  query: (client, sql, args, meta, cb) ->
    [args, cb] = [null, args] if not cb? and _.isFunction args

    start = Date.now()
    show = process.env.KEYSTONE_SHOWSQL or config.sql.show
    trim = (level) -> level isnt 'error' and config.sql.trimstack and not process.env.KEYSTONE_SHOWSQL

    print_query = (level) =>
      fn = @LOGGER[level]
      fn @getpath()

      if trim level
        fn if sql.length > 250 then sql[..247] + '...' else sql
        fn args.map((a) -> if _.isString(a) and a.length > 400 then a[..397] + '...' else a) if args?
      else
        fn sql
        fn args if args?

    # TODO fix instance Console stuff... new logger class
    print_trace = (level) =>
      fn = @LOGGER[level]
      stack = meta.stack.split '\n'
      if trim level
        stack = stack[1..5].concat('  ... trimmed ...').join '\n'
        if @LOGGER instanceof Console
          fn chalk.gray('query trace (last 5):\n%s'), chalk.gray stack
        else
          fn 'query trace (last 5):\n%s', stack
      else
        stack = stack[1..].join '\n'
        if @LOGGER instanceof Console
          fn chalk.gray('query trace:\n%s'), chalk.gray stack
        else
          fn 'query trace:\n%s', stack

    # TODO fix instance Console stuff... new logger class
    client.query sql, args, (err, rest...) =>
      ms = Date.now() - start
      if @LOGGER instanceof Console
        if err and err.code not in @ignore # unique violations should be handled in userland
          @LOGGER.error chalk.red('sql error: %s, %dms'), process_name, ms
        else if ms > @slow
          @LOGGER.warn chalk.yellow('sql slow query: %s, %dms'), process_name, ms
        else if show
          @LOGGER.info chalk.gray('sql query: %s, %dms'), process_name, ms
      else
        if err and err.code not in @ignore # unique violations should be handled in userland
          @LOGGER.error 'sql error: %s, %dms', process_name, ms
        else if ms > @slow
          @LOGGER.warn 'sql slow query: %s, %dms', process_name, ms
        else if show
          @LOGGER.info 'sql query: %s, %dms', process_name, ms

      err.qstack = meta.stack if err?

      if err and err.code not in @ignore # unique violations should be handled in userland
        print_query 'error'
        @LOGGER.error err
        print_trace 'error'
      else if ms > @slow
        print_query 'warn'
        print_trace 'warn'
      else if show
        print_query 'info'

      return unless cb? and _.isFunction cb
      process.nextTick -> cb err, rest...
