(function() {
  var Console, Database, _, chalk, config, process_name,
    slice = [].slice,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  config = require('config');

  _ = require('lodash');

  chalk = require('chalk');

  Console = require('console').Console;

  process_name = (function() {
    return require('shared/utils/process').get_name();
  })();

  module.exports = Database = (function() {
    Database.slow = 750;

    Database.ignore = ['23505'];

    Database.prototype.LOGGER = console;

    function Database() {
      _.assign(this, _.pick(this.constructor, 'slow', 'ignore'));
    }

    Database.prototype.getpath = function() {
      var path;
      path = ['keystone'];
      if (this.client) {
        path.unshift("client_" + this.client);
      }
      return "SET search_path TO '" + (path.join('\', \'')) + "';";
    };

    Database.prototype.setpath = function(client, cb) {
      return client.query(this.getpath(), cb);
    };

    Database.prototype.query = function(client, sql, args, meta, cb) {
      var print_query, print_trace, ref, show, start, trim;
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      start = Date.now();
      show = process.env.KEYSTONE_SHOWSQL || config.sql.show;
      trim = function(level) {
        return level !== 'error' && config.sql.trimstack && !process.env.KEYSTONE_SHOWSQL;
      };
      print_query = (function(_this) {
        return function(level) {
          var fn;
          fn = _this.LOGGER[level];
          fn(_this.getpath());
          if (trim(level)) {
            fn(sql.length > 250 ? sql.slice(0, 248) + '...' : sql);
            if (args != null) {
              return fn(args.map(function(a) {
                if (_.isString(a) && a.length > 400) {
                  return a.slice(0, 398) + '...';
                } else {
                  return a;
                }
              }));
            }
          } else {
            fn(sql);
            if (args != null) {
              return fn(args);
            }
          }
        };
      })(this);
      print_trace = (function(_this) {
        return function(level) {
          var fn, stack;
          fn = _this.LOGGER[level];
          stack = meta.stack.split('\n');
          if (trim(level)) {
            stack = stack.slice(1, 6).concat('  ... trimmed ...').join('\n');
            if (_this.LOGGER instanceof Console) {
              return fn(chalk.gray('query trace (last 5):\n%s'), chalk.gray(stack));
            } else {
              return fn('query trace (last 5):\n%s', stack);
            }
          } else {
            stack = stack.slice(1).join('\n');
            if (_this.LOGGER instanceof Console) {
              return fn(chalk.gray('query trace:\n%s'), chalk.gray(stack));
            } else {
              return fn('query trace:\n%s', stack);
            }
          }
        };
      })(this);
      return client.query(sql, args, (function(_this) {
        return function() {
          var err, ms, ref1, ref2, ref3, rest;
          err = arguments[0], rest = 2 <= arguments.length ? slice.call(arguments, 1) : [];
          ms = Date.now() - start;
          if (_this.LOGGER instanceof Console) {
            if (err && (ref1 = err.code, indexOf.call(_this.ignore, ref1) < 0)) {
              _this.LOGGER.error(chalk.red('sql error: %s, %dms'), process_name, ms);
            } else if (ms > _this.slow) {
              _this.LOGGER.warn(chalk.yellow('sql slow query: %s, %dms'), process_name, ms);
            } else if (show) {
              _this.LOGGER.info(chalk.gray('sql query: %s, %dms'), process_name, ms);
            }
          } else {
            if (err && (ref2 = err.code, indexOf.call(_this.ignore, ref2) < 0)) {
              _this.LOGGER.error('sql error: %s, %dms', process_name, ms);
            } else if (ms > _this.slow) {
              _this.LOGGER.warn('sql slow query: %s, %dms', process_name, ms);
            } else if (show) {
              _this.LOGGER.info('sql query: %s, %dms', process_name, ms);
            }
          }
          if (err != null) {
            err.qstack = meta.stack;
          }
          if (err && (ref3 = err.code, indexOf.call(_this.ignore, ref3) < 0)) {
            print_query('error');
            _this.LOGGER.error(err);
            print_trace('error');
          } else if (ms > _this.slow) {
            print_query('warn');
            print_trace('warn');
          } else if (show) {
            print_query('info');
          }
          if (!((cb != null) && _.isFunction(cb))) {
            return;
          }
          return process.nextTick(function() {
            return cb.apply(null, [err].concat(slice.call(rest)));
          });
        };
      })(this));
    };

    return Database;

  })();

}).call(this);
