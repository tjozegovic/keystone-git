﻿SET search_path = 'keystone';
SET session_replication_role = replica;

DO $$
DECLARE
  i int;
  schema_record pg_namespace%rowtype;
BEGIN
  FOR schema_record IN
    SELECT * FROM pg_namespace WHERE nspname ~ '^client_.*' ORDER BY nspname
  LOOP
    RAISE NOTICE 'updating leads of %', schema_record.nspname;
    EXECUTE 'SET LOCAL search_path = ' || schema_record.nspname || ', ''keystone''';

    WITH dups AS (
      select * from (
        SELECT id, version, published, (doc->'external'->>'source'), (doc->'external'->>'id'),
        ROW_NUMBER() OVER(PARTITION BY (doc->'external'->>'source'), (doc->'external'->>'id') ORDER BY published desc) AS row
        FROM leads_current
      ) dups
      WHERE dups.row > 1
    ), deletes as (
      DELETE FROM leads WHERE id IN (SELECT id FROM dups)
      RETURNING id, valid, version, 'duplead' as type, doc
    )
    INSERT INTO data (id, valid, version, type, doc) SELECT * FROM deletes;

    EXECUTE '
      SELECT *
      FROM   pg_class c
      JOIN   pg_namespace n ON n.oid = c.relnamespace
      WHERE  c.relname = ''leads_scdcurrent_ix''
      AND    n.nspname = $1' USING schema_record.nspname;
    GET DIAGNOSTICS i = ROW_COUNT;

    IF i = 0 THEN
      RAISE NOTICE 'creating index leads_scdcurrent_ix';
      CREATE INDEX leads_scdcurrent_ix ON leads (id) WHERE upper(valid) = 'infinity';
    END IF;

    EXECUTE '
      SELECT *
      FROM   pg_class c
      JOIN   pg_namespace n ON n.oid = c.relnamespace
      WHERE  c.relname = ''leads_doc_gin''
      AND    n.nspname = $1' USING schema_record.nspname;
    GET DIAGNOSTICS i = ROW_COUNT;

    IF i = 0 THEN
      RAISE NOTICE 'creating index leads_doc_gin';
      CREATE INDEX leads_doc_gin ON leads USING gin (doc);
    END IF;

    EXECUTE '
      SELECT *
      FROM   pg_class c
      JOIN   pg_namespace n ON n.oid = c.relnamespace
      WHERE  c.relname = ''leads_externalid_ix''
      AND    n.nspname = $1' USING schema_record.nspname;
    GET DIAGNOSTICS i = ROW_COUNT;

    IF i = 0 THEN
      RAISE NOTICE 'creating index leads_externalid_ix';
      CREATE UNIQUE INDEX leads_externalid_ix ON leads
        ((doc->'external'->>'source'), (doc->'external'->>'id'))
        WHERE upper(valid) = 'infinity' AND (doc ? 'external');
    END IF;

  END LOOP;
END$$;

SET session_replication_role = DEFAULT;
