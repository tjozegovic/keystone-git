SET search_path = 'keystone';

ALTER TABLE logs
   ADD COLUMN at timestamp with time zone DEFAULT NOW();
