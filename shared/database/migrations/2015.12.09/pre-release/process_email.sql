SET search_path = 'keystone';

-- Function: process_email()

-- DROP FUNCTION process_email();

CREATE OR REPLACE FUNCTION process_email()
  RETURNS trigger AS
$BODY$
BEGIN
  IF ((NEW.doc->>'sample')::bool) THEN
    RETURN NULL;
  END IF;

  IF (NEW.type = 'drip') THEN
    IF (TG_OP = 'INSERT') THEN
      EXECUTE FORMAT('
        INSERT INTO %I_stats_drips (id, lead, status, segment, step, queued, delivered, bounced, optout, clicked, opened)
        VALUES (%L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L)'
        , TG_TABLE_NAME
        , NEW.id
        , COALESCE(NEW.doc->'lead'->>'_id', NEW.doc->'lead'->>'id')
        , COALESCE(NEW.doc->'status'->>'_id', NEW.doc->'status'->>'id')
        , (NEW.doc->'segment'->>'id')
        , (NEW.doc->'step'->>'id')
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened')));
    ELSIF (TG_OP = 'UPDATE') THEN
      EXECUTE FORMAT('
        UPDATE %I_stats_drips SET
            lead = %L
          , status = %L
          , segment = %L
          , step = %L
          , queued = %L
          , delivered = %L
          , bounced = %L
          , optout = %L
          , clicked = %L
          , opened = %L
        WHERE id = %L'
        , TG_TABLE_NAME
        , COALESCE(NEW.doc->'lead'->>'_id', NEW.doc->'lead'->>'id')
        , COALESCE(NEW.doc->'status'->>'_id', NEW.doc->'status'->>'id')
        , (NEW.doc->'segment'->>'id')
        , (NEW.doc->'step'->>'id')
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , NEW.id);
    END IF;
  ELSIF (NEW.type = 'batch') THEN
    IF (TG_OP = 'INSERT') THEN
      EXECUTE FORMAT('
        INSERT INTO %I_stats_batches (id, batch, queued, delivered, bounced, optout, clicked, opened)
        VALUES (%L, %L, %L, %L, %L, %L, %L, %L)'
        , TG_TABLE_NAME
        , NEW.id
        , COALESCE(NEW.doc->'batch'->>'_id', NEW.doc->'batch'->>'id')
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened')));
    ELSIF (TG_OP = 'UPDATE') THEN
      EXECUTE FORMAT('
        UPDATE %I_stats_batches SET
            batch = %L
          , queued = %L
          , delivered = %L
          , bounced = %L
          , optout = %L
          , clicked = %L
          , opened = %L
        WHERE id = %L'
        , TG_TABLE_NAME
        , COALESCE(NEW.doc->'batch'->>'_id', NEW.doc->'batch'->>'id')
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , NEW.id);
    END IF;
  END IF;

  RETURN NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION process_email()
  OWNER TO keystone;
