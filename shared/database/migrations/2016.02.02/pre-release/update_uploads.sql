set search_path = 'keystone';

DO $$
DECLARE
  schema_record pg_namespace%rowtype;
  table_record pg_tables%rowtype;
  matches text[];
  startts timestamp;
BEGIN
  FOR schema_record IN
    SELECT * FROM pg_namespace WHERE nspname ~ '^client_.*'
  LOOP
    RAISE NOTICE '%', schema_record.nspname;
    EXECUTE 'SET LOCAL search_path = ' || schema_record.nspname || ', ''keystone''';

	EXECUTE '
	ALTER TABLE uploads DROP CONSTRAINT uploads_type_check;

	ALTER TABLE uploads
	  ADD CONSTRAINT uploads_type_check CHECK (type = ANY (ARRAY[''lead.import''::text, ''lead.import.error''::text, ''optout.import''::text, ''optout.import.error''::text]));';
  END LOOP;
END$$;
