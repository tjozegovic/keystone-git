SET search_path = 'keystone';

CREATE EXTENSION IF NOT EXISTS "citext";

DROP VIEW users_current;

ALTER TABLE users ALTER COLUMN username TYPE citext;

CREATE OR REPLACE VIEW users_current AS
  SELECT id, username, version, lower(valid) AS published, doc
  FROM users
  WHERE upper(valid) = 'infinity';

