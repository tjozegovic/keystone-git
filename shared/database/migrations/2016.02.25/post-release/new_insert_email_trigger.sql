CREATE OR REPLACE FUNCTION insert_email_trigger() RETURNS trigger AS $body$
DECLARE
  tbl_name TEXT;
  bom TIMESTAMP := date_trunc('month', get_ts_from_epoch(NEW.id));
BEGIN
  /*... create the new partition and set up the redirect Rules ...*/
  tbl_name = to_char(bom, '"emails_y"YYYY"m"MM');

  -- Check if the partition needed for the current record exists
  PERFORM 1
  FROM   pg_catalog.pg_class c
  JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
  WHERE  c.relkind = 'r'
  AND    c.relname = tbl_name
  AND    n.nspname = TG_TABLE_SCHEMA;

  -- If the partition needed does not yet exist, then we create it:
  IF NOT FOUND THEN
    EXECUTE FORMAT('LOCK TABLE %I._emails IN ACCESS EXCLUSIVE MODE;', TG_TABLE_SCHEMA);

    PERFORM 1
    FROM   pg_catalog.pg_class c
    JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relkind = 'r'
    AND    c.relname = tbl_name
    AND    n.nspname = TG_TABLE_SCHEMA;

    IF NOT FOUND THEN
      EXECUTE FORMAT('
        CREATE TABLE IF NOT EXISTS %I.%I (
          CHECK (id >= %s AND id < %s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails);

        CREATE TABLE IF NOT EXISTS %1$I.%2$I_stats (
          CHECK (id >= %3$s AND id < %4$s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails_stats);

        CREATE TABLE IF NOT EXISTS %1$I.%2$I_stats_batches (
          CHECK (id >= %3$s AND id < %4$s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails_stats_batches);

        CREATE TABLE IF NOT EXISTS %1$I.%2$I_stats_drips (
          CHECK (id >= %3$s AND id < %4$s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails_stats_drips);

        CREATE TABLE IF NOT EXISTS %1$I.%2$I_stats_direct (
          CHECK (id >= %3$s AND id < %4$s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails_stats_direct);

        CREATE INDEX %2$I_type ON %1$I.%2$I (type);
        CREATE INDEX %2$I_gin_doc ON %1$I.%2$I USING gin (doc);

        CREATE TRIGGER %2$I_stats
          AFTER INSERT OR UPDATE ON %1$I.%2$I
          FOR EACH ROW EXECUTE PROCEDURE process_email();'
        , TG_TABLE_SCHEMA
        , tbl_name
        , get_epoch_from_ts(bom)
        , get_epoch_from_ts(bom + INTERVAL '1 month'));
    END IF;
  END IF;

  /* Redo the INSERT dynamically.  The new RULE will redirect it to the child table */
  EXECUTE FORMAT('INSERT INTO %I.%I SELECT $1.*', TG_TABLE_SCHEMA, tbl_name) USING NEW;
  RETURN NEW;
END;
$body$ LANGUAGE plpgsql;

