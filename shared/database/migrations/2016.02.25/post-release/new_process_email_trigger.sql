CREATE OR REPLACE FUNCTION process_email() RETURNS TRIGGER AS $body$
DECLARE
    attrs text[];
BEGIN
  IF ((NEW.doc->>'sample')::bool) THEN
    RETURN NULL;
  END IF;

  IF (NEW.type = 'drip') THEN
    IF (NEW.doc ? 'lead' AND (NEW.doc->'lead') ? '$attributes') THEN
        attrs := ARRAY(SELECT jsonb_array_elements_text(NEW.doc#>'{lead,$attributes}'));
    END IF;

    IF (TG_OP = 'INSERT') THEN
      EXECUTE FORMAT('
        INSERT INTO %I_stats_drips (id, lead, status, segment, step, attributes, queued, delivered, bounced, optout, clicked, opened, message, template)
        VALUES (%L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L)'
        , TG_TABLE_NAME
        , NEW.id
        , COALESCE(NEW.doc->'lead'->>'_id', NEW.doc->'lead'->>'id')
        , COALESCE(NEW.doc->'status'->>'_id', NEW.doc->'status'->>'id')
        , (NEW.doc->'segment'->>'id')
        , (NEW.doc->'step'->>'id')
        , attrs
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , COALESCE(NEW.doc->'message'->>'id', NEW.doc->>'message')
        , COALESCE(NEW.doc->'template'->>'value', NEW.doc->>'template'));
    ELSIF (TG_OP = 'UPDATE') THEN
      EXECUTE FORMAT('
        UPDATE %I_stats_drips SET
            lead = %L
          , status = %L
          , segment = %L
          , step = %L
          , attributes = %L
          , queued = %L
          , delivered = %L
          , bounced = %L
          , optout = %L
          , clicked = %L
          , opened = %L
          , message = %L
          , template = %L
        WHERE id = %L'
        , TG_TABLE_NAME
        , COALESCE(NEW.doc->'lead'->>'_id', NEW.doc->'lead'->>'id')
        , COALESCE(NEW.doc->'status'->>'_id', NEW.doc->'status'->>'id')
        , (NEW.doc->'segment'->>'id')
        , (NEW.doc->'step'->>'id')
        , attrs
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , COALESCE(NEW.doc->'message'->>'id', NEW.doc->>'message')
        , COALESCE(NEW.doc->'template'->>'value', NEW.doc->>'template')
        , NEW.id);
    END IF;
  ELSIF (NEW.type = 'batch') THEN
    IF (TG_OP = 'INSERT') THEN
      EXECUTE FORMAT('
        INSERT INTO %I_stats_batches (id, batch, queued, delivered, bounced, optout, clicked, opened, message, template)
        VALUES (%L, %L, %L, %L, %L, %L, %L, %L, %L, %L)'
        , TG_TABLE_NAME
        , NEW.id
        , COALESCE(NEW.doc->'batch'->>'_id', NEW.doc->'batch'->>'id')
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , COALESCE(NEW.doc->'message'->>'id', NEW.doc->>'message')
        , COALESCE(NEW.doc->'template'->>'value', NEW.doc->>'template'));
    ELSIF (TG_OP = 'UPDATE') THEN
      EXECUTE FORMAT('
        UPDATE %I_stats_batches SET
            batch = %L
          , queued = %L
          , delivered = %L
          , bounced = %L
          , optout = %L
          , clicked = %L
          , opened = %L
          , message = %L
          , template = %L
        WHERE id = %L'
        , TG_TABLE_NAME
        , COALESCE(NEW.doc->'batch'->>'_id', NEW.doc->'batch'->>'id')
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , COALESCE(NEW.doc->'message'->>'id', NEW.doc->>'message')
        , COALESCE(NEW.doc->'template'->>'value', NEW.doc->>'template')
        , NEW.id);
    END IF;
  ELSIF (NEW.type = 'direct') THEN
    RAISE NOTICE '%', NEW.doc;
    IF (TG_OP = 'INSERT') THEN
      EXECUTE FORMAT('
        INSERT INTO %I_stats_direct (id, queued, delivered, bounced, optout, clicked, opened)
        VALUES (%L, %L, %L, %L, %L, %L, %L)'
        , TG_TABLE_NAME
        , NEW.id
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened')));
    ELSIF (TG_OP = 'UPDATE') THEN
      EXECUTE FORMAT('
        UPDATE %I_stats_direct SET
            queued = %L
          , delivered = %L
          , bounced = %L
          , optout = %L
          , clicked = %L
          , opened = %L
        WHERE id = %L'
        , TG_TABLE_NAME
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , NEW.id);
    END IF;
  END IF;

  RETURN NULL;
END;
$body$ LANGUAGE plpgsql;
