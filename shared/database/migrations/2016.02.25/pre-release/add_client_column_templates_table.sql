SET search_path = 'keystone';

ALTER TABLE templates ADD COLUMN client uuid;
ALTER TABLE templates ADD CONSTRAINT templates_name_client UNIQUE (name, client);
ALTER TABLE templates DROP CONSTRAINT templates_name_key;
