SET search_path = 'keystone';

CREATE TABLE IF NOT EXISTS _emails_stats_direct (
  PRIMARY KEY (id)
) INHERITS (_emails_stats);

DO $$
DECLARE
  schema_record record;
  rec record;
BEGIN

  FOR schema_record IN
    SELECT * FROM pg_namespace WHERE nspname ~ '^client_.*'
  LOOP
    RAISE NOTICE '%', schema_record.nspname;
    EXECUTE 'SET LOCAL search_path = ' || schema_record.nspname || ', ''keystone''';

    CREATE TABLE IF NOT EXISTS _emails_stats_direct (
      PRIMARY KEY (id)
    ) INHERITS (keystone._emails_stats_direct, _emails_stats);

    CREATE OR REPLACE VIEW direct_stats AS SELECT * FROM _emails_stats_direct;

    FOR rec IN
      EXECUTE '
        SELECT relname, consrc
        FROM   pg_class, pg_constraint
        WHERE  pg_constraint.conrelid = pg_class.oid
            AND relnamespace = $1
            AND relname ~ ''^emails.*_stats$''
            AND contype = ''c''
        ORDER  BY relname;'
      USING schema_record.oid
    LOOP
      RAISE NOTICE 'schema %, table %', rec.relname, rec.consrc;
      EXECUTE FORMAT('
          CREATE TABLE IF NOT EXISTS %1$I.%2$I_direct (
            CHECK (%3$s),
            PRIMARY KEY (id)
          ) INHERITS (%1$I._emails_stats_direct);'
      , schema_record.nspname
  	  , rec.relname
    	, rec.consrc);
    END LOOP;
  END LOOP;
END$$;
