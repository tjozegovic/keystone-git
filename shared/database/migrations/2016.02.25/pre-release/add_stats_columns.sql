SET search_path = 'keystone';

ALTER TABLE _emails_stats
   ADD COLUMN message uuid,
   ADD COLUMN template uuid;

ALTER TABLE _emails_stats_drips
   ADD COLUMN attributes text[];
