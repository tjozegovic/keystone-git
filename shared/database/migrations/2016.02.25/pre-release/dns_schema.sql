CREATE SCHEMA IF NOT EXISTS dns;
CREATE TABLE IF NOT EXISTS dns.job (
  id serial NOT NULL,
  type character varying(50),
  client_id uuid,
  started timestamptz DEFAULT now(),
  CONSTRAINT dns_job_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS dns.records (
  id serial NOT NULL,
  client_id uuid NOT NULL,
  isglobal boolean,
  name text,
  type text,
  domain text,
  value text,
  created timestamptz,
  updated timestamptz DEFAULT now(),
  isdeleted boolean,
  CONSTRAINT dns_records_pkey PRIMARY KEY (id, client_id)
);

CREATE TABLE IF NOT EXISTS dns.settings (
  id serial NOT NULL,
  zone_id text,
  access_key_id text,
  secret_access_key text,
  email_to text,
  CONSTRAINT dns_settings_pkey PRIMARY KEY (id)
);
