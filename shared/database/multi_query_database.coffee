_ = require 'lodash'
pg = require 'pg'
{Database} = require './index'

module.exports = class MultiQueryDatabase extends Database
  constructor: (@url, @client) ->
    super

  query: (sql, args, cb) =>
    meta = {}
    [args, cb] = [null, args] if not cb? and _.isFunction args

    # moving the toString here, which is for squel, will give us a better stack trace
    meta.squel = sql
    sql = sql.toString()

    Error.captureStackTrace meta

    unless @pgclient?
      return pg.connect @url, (err, @pgclient, @done) =>
        @query sql, args, cb

    @setpath @pgclient, (err) =>
      if err?
        console.error err
        done err
        return cb err

      super @pgclient, sql, args, meta, cb

  stream: (sql, args, cb) =>
    meta = {}
    [args, cb] = [null, args] if not cb? and _.isFunction args

    # moving the toString here, which is for squel, will give us a better stack trace
    meta.squel = sql
    sql = sql.toString()

    Error.captureStackTrace meta

    unless @pgclient?
      return pg.connect @url, (err, @pgclient, @done) =>
        @query sql, args, cb

    @setpath @pgclient, (err) =>
      if err?
        console.error err
        done err
        return cb err

      super @pgclient, sql, args, meta, cb

  close: (err) ->
    @done? err
    @done = null

  multiple: (cb) -> cb @
