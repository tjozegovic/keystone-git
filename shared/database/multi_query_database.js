(function() {
  var Database, MultiQueryDatabase, _, pg,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  pg = require('pg');

  Database = require('./index').Database;

  module.exports = MultiQueryDatabase = (function(superClass) {
    extend(MultiQueryDatabase, superClass);

    function MultiQueryDatabase(url, client) {
      this.url = url;
      this.client = client;
      this.stream = bind(this.stream, this);
      this.query = bind(this.query, this);
      MultiQueryDatabase.__super__.constructor.apply(this, arguments);
    }

    MultiQueryDatabase.prototype.query = function(sql, args, cb) {
      var meta, ref;
      meta = {};
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      meta.squel = sql;
      sql = sql.toString();
      Error.captureStackTrace(meta);
      if (this.pgclient == null) {
        return pg.connect(this.url, (function(_this) {
          return function(err, pgclient, done1) {
            _this.pgclient = pgclient;
            _this.done = done1;
            return _this.query(sql, args, cb);
          };
        })(this));
      }
      return this.setpath(this.pgclient, (function(_this) {
        return function(err) {
          if (err != null) {
            console.error(err);
            done(err);
            return cb(err);
          }
          return MultiQueryDatabase.__super__.query.call(_this, _this.pgclient, sql, args, meta, cb);
        };
      })(this));
    };

    MultiQueryDatabase.prototype.stream = function(sql, args, cb) {
      var meta, ref;
      meta = {};
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      meta.squel = sql;
      sql = sql.toString();
      Error.captureStackTrace(meta);
      if (this.pgclient == null) {
        return pg.connect(this.url, (function(_this) {
          return function(err, pgclient, done1) {
            _this.pgclient = pgclient;
            _this.done = done1;
            return _this.query(sql, args, cb);
          };
        })(this));
      }
      return this.setpath(this.pgclient, (function(_this) {
        return function(err) {
          if (err != null) {
            console.error(err);
            done(err);
            return cb(err);
          }
          return MultiQueryDatabase.__super__.stream.call(_this, _this.pgclient, sql, args, meta, cb);
        };
      })(this));
    };

    MultiQueryDatabase.prototype.close = function(err) {
      if (typeof this.done === "function") {
        this.done(err);
      }
      return this.done = null;
    };

    MultiQueryDatabase.prototype.multiple = function(cb) {
      return cb(this);
    };

    return MultiQueryDatabase;

  })(Database);

}).call(this);
