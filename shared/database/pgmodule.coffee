_ = require 'lodash'
moment = require 'moment'
through2 = require 'through2'
uuid = require 'uuid'

squel = require('squel').useFlavour 'postgres'
squel.cls.DefaultQueryBuilderOptions.tableAliasQuoteCharacter = '"'
squel.cls.DefaultQueryBuilderOptions.replaceSingleQuotes = yes

class JsonString
  constructor: (@obj) ->

# TODO can we do something like this for moment too?? that'd be cool
squel.registerValueHandler JsonString, (obj, options) ->
  options.dontQuote = yes
  unless obj.obj?
    'null'
  else
    "$jsonb$#{JSON.stringify obj.obj}$jsonb$"

squel.registerValueHandler Object, (obj, options) ->
  m = moment obj
  unless m.isValid()
    obj
  else
    "'#{m.toISOString()}'"

module.exports = class PgModule
  keywords = ['extended', 'included']

  @extend: (obj) ->
    for key, value of obj when key not in keywords
      @[key] = value

    obj.extended?.apply @
    @

  @include: (obj) ->
    for key, value of obj when key not in keywords
      # Assign properties to the prototype
      @::[key] = value

    obj.included?.apply @
    @

  squel: squel

  constructor: (@db, @ctx) ->

  newid: -> uuid.v4().replace /-/g, ''

  single: (sql, args, cb) ->
    [args, cb] = [null, args] if not cb? and _.isFunction args
    @db.query sql, args, (err, results) ->
      return cb? err if err?
      cb? null, results.rows[0]

  list: (sql, args, cb) ->
    [args, cb] = [null, args] if not cb? and _.isFunction args
    @db.query sql, args, (err, results) ->
      return cb? err if err?
      cb? null, results.rows

  doc_list: (sql, cb) ->
    @db.query sql, (err, results) ->
      return cb? err if err?
      cb? null, _.pluck results.rows, 'doc'

  jsonb: (obj) -> new JsonString obj

  stream:
    transform: (fn) ->
      through2.obj (chunk, enc, cb) ->
        obj = fn chunk
        if _.isArray obj
          _.forEach obj, (x) => @push x
        else
          @push obj

        cb()

    filter: (fn) ->
      through2.obj (chunk, enc, cb) ->
        if fn(chunk) then cb null, chunk else cb null
