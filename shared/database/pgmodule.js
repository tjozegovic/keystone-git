(function() {
  var JsonString, PgModule, _, moment, squel, through2, uuid,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  _ = require('lodash');

  moment = require('moment');

  through2 = require('through2');

  uuid = require('uuid');

  squel = require('squel').useFlavour('postgres');

  squel.cls.DefaultQueryBuilderOptions.tableAliasQuoteCharacter = '"';

  squel.cls.DefaultQueryBuilderOptions.replaceSingleQuotes = true;

  JsonString = (function() {
    function JsonString(obj1) {
      this.obj = obj1;
    }

    return JsonString;

  })();

  squel.registerValueHandler(JsonString, function(obj, options) {
    options.dontQuote = true;
    if (obj.obj == null) {
      return 'null';
    } else {
      return "$jsonb$" + (JSON.stringify(obj.obj)) + "$jsonb$";
    }
  });

  squel.registerValueHandler(Object, function(obj, options) {
    var m;
    m = moment(obj);
    if (!m.isValid()) {
      return obj;
    } else {
      return "'" + (m.toISOString()) + "'";
    }
  });

  module.exports = PgModule = (function() {
    var keywords;

    keywords = ['extended', 'included'];

    PgModule.extend = function(obj) {
      var key, ref, value;
      for (key in obj) {
        value = obj[key];
        if (indexOf.call(keywords, key) < 0) {
          this[key] = value;
        }
      }
      if ((ref = obj.extended) != null) {
        ref.apply(this);
      }
      return this;
    };

    PgModule.include = function(obj) {
      var key, ref, value;
      for (key in obj) {
        value = obj[key];
        if (indexOf.call(keywords, key) < 0) {
          this.prototype[key] = value;
        }
      }
      if ((ref = obj.included) != null) {
        ref.apply(this);
      }
      return this;
    };

    PgModule.prototype.squel = squel;

    function PgModule(db, ctx) {
      this.db = db;
      this.ctx = ctx;
    }

    PgModule.prototype.newid = function() {
      return uuid.v4().replace(/-/g, '');
    };

    PgModule.prototype.single = function(sql, args, cb) {
      var ref;
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      return this.db.query(sql, args, function(err, results) {
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        return typeof cb === "function" ? cb(null, results.rows[0]) : void 0;
      });
    };

    PgModule.prototype.list = function(sql, args, cb) {
      var ref;
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      return this.db.query(sql, args, function(err, results) {
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        return typeof cb === "function" ? cb(null, results.rows) : void 0;
      });
    };

    PgModule.prototype.doc_list = function(sql, cb) {
      return this.db.query(sql, function(err, results) {
        if (err != null) {
          return typeof cb === "function" ? cb(err) : void 0;
        }
        return typeof cb === "function" ? cb(null, _.pluck(results.rows, 'doc')) : void 0;
      });
    };

    PgModule.prototype.jsonb = function(obj) {
      return new JsonString(obj);
    };

    PgModule.prototype.stream = {
      transform: function(fn) {
        return through2.obj(function(chunk, enc, cb) {
          var obj;
          obj = fn(chunk);
          if (_.isArray(obj)) {
            _.forEach(obj, (function(_this) {
              return function(x) {
                return _this.push(x);
              };
            })(this));
          } else {
            this.push(obj);
          }
          return cb();
        });
      },
      filter: function(fn) {
        return through2.obj(function(chunk, enc, cb) {
          if (fn(chunk)) {
            return cb(null, chunk);
          } else {
            return cb(null);
          }
        });
      }
    };

    return PgModule;

  })();

}).call(this);
