_ = require 'lodash'
pg = require 'pg'
{SingleQueryDatabase, MultiQueryDatabase, Repository} = require './index'

module.exports = class RepositoryFactory
  constructor: (@url) ->
    @_tracked = []

  get_database: (opts) ->
    if opts.multiquery
      database = new MultiQueryDatabase @url, @client
      @_tracked.push database
      database
    else
      new SingleQueryDatabase @url, @client

  build: (type, opts = {}) ->
    try
      Repository = require './' + type
      database = @get_database opts

      new Repository database, @
    catch e
      console.warn "could not load data repository (#{type})", e
      console.error e.stack
      null

  cleanup: ->
    @_tracked.forEach (db) -> db.close()
    @_tracked = []

  single: -> new SingleQueryDatabase @url, @client
  multiple: -> new MultiQueryDatabase @url, @client

  use: (types...) ->
    if types.length > 1 and _.isObject _.last types
      opts = types.pop()

    if types.length > 1
      _.map types, (t) => @use t
    else
      if _.isObject types[0]
        _.reduce _.keys(types[0]), (ret, key) =>
          ret[key] = @build types[0][key], opts
          return ret
        , {}
      else
        @build types[0], opts
