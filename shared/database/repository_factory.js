(function() {
  var MultiQueryDatabase, Repository, RepositoryFactory, SingleQueryDatabase, _, pg, ref,
    slice = [].slice;

  _ = require('lodash');

  pg = require('pg');

  ref = require('./index'), SingleQueryDatabase = ref.SingleQueryDatabase, MultiQueryDatabase = ref.MultiQueryDatabase, Repository = ref.Repository;

  module.exports = RepositoryFactory = (function() {
    function RepositoryFactory(url) {
      this.url = url;
      this._tracked = [];
    }

    RepositoryFactory.prototype.get_database = function(opts) {
      var database;
      if (opts.multiquery) {
        database = new MultiQueryDatabase(this.url, this.client);
        this._tracked.push(database);
        return database;
      } else {
        return new SingleQueryDatabase(this.url, this.client);
      }
    };

    RepositoryFactory.prototype.build = function(type, opts) {
      var database, e;
      if (opts == null) {
        opts = {};
      }
      try {
        Repository = require('./' + type);
        database = this.get_database(opts);
        return new Repository(database, this);
      } catch (_error) {
        e = _error;
        console.warn("could not load data repository (" + type + ")", e);
        console.error(e.stack);
        return null;
      }
    };

    RepositoryFactory.prototype.cleanup = function() {
      this._tracked.forEach(function(db) {
        return db.close();
      });
      return this._tracked = [];
    };

    RepositoryFactory.prototype.single = function() {
      return new SingleQueryDatabase(this.url, this.client);
    };

    RepositoryFactory.prototype.multiple = function() {
      return new MultiQueryDatabase(this.url, this.client);
    };

    RepositoryFactory.prototype.use = function() {
      var opts, types;
      types = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      if (types.length > 1 && _.isObject(_.last(types))) {
        opts = types.pop();
      }
      if (types.length > 1) {
        return _.map(types, (function(_this) {
          return function(t) {
            return _this.use(t);
          };
        })(this));
      } else {
        if (_.isObject(types[0])) {
          return _.reduce(_.keys(types[0]), (function(_this) {
            return function(ret, key) {
              ret[key] = _this.build(types[0][key], opts);
              return ret;
            };
          })(this), {});
        } else {
          return this.build(types[0], opts);
        }
      }
    };

    return RepositoryFactory;

  })();

}).call(this);
