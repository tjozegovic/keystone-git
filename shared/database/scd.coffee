_ = require 'lodash'

# SlowyChangingDimension
module.exports = class ScdCollection extends require './pgmodule'
  constructor: (@db, @ctx) ->
    super

  before_save: (doc) ->
    doc.type ?= @type if @type?
    doc.active ?= yes
    doc.created ?= new Date
    doc.updated = new Date

  get_doc: (row) =>
    # remove the doc field from the query, then add all
    # the other fields to the doc from the query, except for doc
    # e.g. SELECT id, version, doc FROM table (doc is {foo: 'bar'})
    #      returns -> { id: '', version: '', foo: 'bar' }
    doc = _.assign row.doc, _.omit row, 'doc'

    # if there's a transform function setup on the collection type,
    # pass the json document through it, then return it
    @transform?.call(@, doc) or doc

  # this method should be called when you have a query where you want a
  # single JSON object returned based on the 'doc' field of a table. it
  # will add all other fields besides 'doc' to the doc json field. args
  # is an optional argument array to pass into a pg prepared statement.
  single: (sql, args, cb) ->
    [args, cb] = [null, args] if not cb? and _.isFunction args

    # if it's a squel query, with the limit function, call it
    sql.limit? 1

    super sql, args, (err, result) =>
      return cb err if err
      return cb null, null unless result?
      cb null, @get_doc result

  # this method should be called when you have a query where you want a
  # list of JSON objects returned based on the 'doc' field of a table. it
  # will add all other fields besides 'doc' to the doc json field. args
  # is an optional argument array to pass into a pg prepared statement.
  list: (sql, args, cb) ->
    [args, cb] = [null, args] if not cb? and _.isFunction args
    super sql, args, (err, rows) =>
      return cb err if err
      cb null, _.map rows, @get_doc

  _select_current: ->
    sql = @squel.select()
      .from "#{@table}_current"
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'version'
      .field 'published'
      .field 'doc'

    if @type?
      sql.where 'type = ?', @type

    sql

  get: (id, cb) ->
    sql = @_select_current()
      .where 'id = ?', id
      .limit 1

    @single sql, cb

  # find a list of documents given an array of guid ids
  fetch: (ids, cb) ->
    return cb null, [] unless _.isArray(ids) and ids.length

    sql = @_select_current()
      .where 'id IN ?', ids

    @list sql, cb

  # return all rows where there is an 'active' property and it is truthy*
  active: (cb) ->
    sql = @_select_current()
      .where "NOT (doc ? 'active') OR (doc->>'active')::bool"

    @list sql, cb

  all: (cb) ->
    @list @_select_current(), cb

  get_valid: (id, valid, cb) ->
    sql = @squel.select()
      .from @table
      .field "REPLACE(id::text, '-', '')", 'id'
      .field 'doc'
      .field 'version'
      .field 'published'
      .where 'id = ?', id
      .where "valid @> '#{valid}'::timestamptz"
      .limit 1

    if @type?
      sql.where 'type = ?', @type

    @single sql, cb

  # TODO obsolete
  getvalid: @::get_valid

  save: (doc, cb) ->
    @before_save?.call @, doc
    json = JSON.stringify _.omit doc, 'id', 'version', 'valid'

    unless doc.id?
      sql = @squel.insert()
        .into @table
        .set 'doc', "$JSON$#{json}$JSON$", dontQuote: yes
        .returning "REPLACE(id::text, '-', '') AS id, version"

      if @type?
        sql.set 'type', @type

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id, version: first.version
    else
      sql = @squel.update()
        .table @table
        .set 'doc', "$JSON$#{json}$JSON$", dontQuote: yes
        .where 'id = ?', doc.id
        .where 'version = ?', doc.version
        .returning "REPLACE(id::text, '-', '') AS id, version"

      @db.query sql, (err, result) ->
        return cb err if err
        first = result.rows[0]
        cb null, id: first.id, version: first.version

  update: (id, doc, cb) ->
    doc.id = id
    @save doc, cb

  delete: (id, cb) ->
    @get id, (err, obj) =>
      return cb err if err

      obj.active = no
      @save obj, cb

  import: (doc, cb) ->
    @before_save?.call @, doc
    json = JSON.stringify _.omit doc, 'id', 'version', 'valid'

    sql = @squel.insert()
      .into @table
      .set 'id', doc.id
      .set 'type', @type
      .set 'doc', "$JSON$#{json}$JSON$", dontQuote: yes
      .returning "REPLACE(id::text, '-', '') AS id, version"

    @db.query sql, (err, result) ->
      return cb err if err
      first = result.rows[0]
      cb null, id: first.id, version: first.version
