(function() {
  var ScdCollection, _,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  module.exports = ScdCollection = (function(superClass) {
    extend(ScdCollection, superClass);

    function ScdCollection(db, ctx) {
      this.db = db;
      this.ctx = ctx;
      this.get_doc = bind(this.get_doc, this);
      ScdCollection.__super__.constructor.apply(this, arguments);
    }

    ScdCollection.prototype.before_save = function(doc) {
      if (this.type != null) {
        if (doc.type == null) {
          doc.type = this.type;
        }
      }
      if (doc.active == null) {
        doc.active = true;
      }
      if (doc.created == null) {
        doc.created = new Date;
      }
      return doc.updated = new Date;
    };

    ScdCollection.prototype.get_doc = function(row) {
      var doc, ref;
      doc = _.assign(row.doc, _.omit(row, 'doc'));
      return ((ref = this.transform) != null ? ref.call(this, doc) : void 0) || doc;
    };

    ScdCollection.prototype.single = function(sql, args, cb) {
      var ref;
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      if (typeof sql.limit === "function") {
        sql.limit(1);
      }
      return ScdCollection.__super__.single.call(this, sql, args, (function(_this) {
        return function(err, result) {
          if (err) {
            return cb(err);
          }
          if (result == null) {
            return cb(null, null);
          }
          return cb(null, _this.get_doc(result));
        };
      })(this));
    };

    ScdCollection.prototype.list = function(sql, args, cb) {
      var ref;
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      return ScdCollection.__super__.list.call(this, sql, args, (function(_this) {
        return function(err, rows) {
          if (err) {
            return cb(err);
          }
          return cb(null, _.map(rows, _this.get_doc));
        };
      })(this));
    };

    ScdCollection.prototype._select_current = function() {
      var sql;
      sql = this.squel.select().from(this.table + "_current").field("REPLACE(id::text, '-', '')", 'id').field('version').field('published').field('doc');
      if (this.type != null) {
        sql.where('type = ?', this.type);
      }
      return sql;
    };

    ScdCollection.prototype.get = function(id, cb) {
      var sql;
      sql = this._select_current().where('id = ?', id).limit(1);
      return this.single(sql, cb);
    };

    ScdCollection.prototype.fetch = function(ids, cb) {
      var sql;
      if (!(_.isArray(ids) && ids.length)) {
        return cb(null, []);
      }
      sql = this._select_current().where('id IN ?', ids);
      return this.list(sql, cb);
    };

    ScdCollection.prototype.active = function(cb) {
      var sql;
      sql = this._select_current().where("NOT (doc ? 'active') OR (doc->>'active')::bool");
      return this.list(sql, cb);
    };

    ScdCollection.prototype.all = function(cb) {
      return this.list(this._select_current(), cb);
    };

    ScdCollection.prototype.get_valid = function(id, valid, cb) {
      var sql;
      sql = this.squel.select().from(this.table).field("REPLACE(id::text, '-', '')", 'id').field('doc').field('version').field('published').where('id = ?', id).where("valid @> '" + valid + "'::timestamptz").limit(1);
      if (this.type != null) {
        sql.where('type = ?', this.type);
      }
      return this.single(sql, cb);
    };

    ScdCollection.prototype.getvalid = ScdCollection.prototype.get_valid;

    ScdCollection.prototype.save = function(doc, cb) {
      var json, ref, sql;
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      json = JSON.stringify(_.omit(doc, 'id', 'version', 'valid'));
      if (doc.id == null) {
        sql = this.squel.insert().into(this.table).set('doc', "$JSON$" + json + "$JSON$", {
          dontQuote: true
        }).returning("REPLACE(id::text, '-', '') AS id, version");
        if (this.type != null) {
          sql.set('type', this.type);
        }
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      } else {
        sql = this.squel.update().table(this.table).set('doc', "$JSON$" + json + "$JSON$", {
          dontQuote: true
        }).where('id = ?', doc.id).where('version = ?', doc.version).returning("REPLACE(id::text, '-', '') AS id, version");
        return this.db.query(sql, function(err, result) {
          var first;
          if (err) {
            return cb(err);
          }
          first = result.rows[0];
          return cb(null, {
            id: first.id,
            version: first.version
          });
        });
      }
    };

    ScdCollection.prototype.update = function(id, doc, cb) {
      doc.id = id;
      return this.save(doc, cb);
    };

    ScdCollection.prototype["delete"] = function(id, cb) {
      return this.get(id, (function(_this) {
        return function(err, obj) {
          if (err) {
            return cb(err);
          }
          obj.active = false;
          return _this.save(obj, cb);
        };
      })(this));
    };

    ScdCollection.prototype["import"] = function(doc, cb) {
      var json, ref, sql;
      if ((ref = this.before_save) != null) {
        ref.call(this, doc);
      }
      json = JSON.stringify(_.omit(doc, 'id', 'version', 'valid'));
      sql = this.squel.insert().into(this.table).set('id', doc.id).set('type', this.type).set('doc', "$JSON$" + json + "$JSON$", {
        dontQuote: true
      }).returning("REPLACE(id::text, '-', '') AS id, version");
      return this.db.query(sql, function(err, result) {
        var first;
        if (err) {
          return cb(err);
        }
        first = result.rows[0];
        return cb(null, {
          id: first.id,
          version: first.version
        });
      });
    };

    return ScdCollection;

  })(require('./pgmodule'));

}).call(this);
