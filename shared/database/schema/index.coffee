fs = require 'fs'

['client', 'softvu'].forEach (type) ->
  Object.defineProperty module.exports, type,
    __proto__: null
    get: -> fs.readFileSync(__dirname + '/' + type + '.sql').toString()
