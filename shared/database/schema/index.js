(function() {
  var fs;

  fs = require('fs');

  ['client', 'softvu'].forEach(function(type) {
    return Object.defineProperty(module.exports, type, {
      __proto__: null,
      get: function() {
        return fs.readFileSync(__dirname + '/' + type + '.sql').toString();
      }
    });
  });

}).call(this);
