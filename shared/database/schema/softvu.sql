﻿/*
SET LOCAL search_path TO 'keystone';

DROP TABLE IF EXISTS apikeys;
DROP TABLE IF EXISTS jobs;
DROP TABLE IF EXISTS optouts;

DROP TABLE IF EXISTS clients CASCADE;
DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS _data CASCADE;

DROP TABLE IF EXISTS emails CASCADE;
DROP TABLE IF EXISTS events CASCADE;
*/

SET LOCAL search_path TO 'keystone';

CREATE EXTENSION IF NOT EXISTS "btree_gin";
CREATE EXTENSION IF NOT EXISTS "btree_gist";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "citext";

--CREATE EXTENSION IF NOT EXISTS "dblink";

--CREATE SCHEMA jobmon;
--CREATE EXTENSION pg_jobmon SCHEMA jobmon;

--CREATE SCHEMA partman;
--CREATE EXTENSION pg_partman SCHEMA partman;

CREATE OR REPLACE FUNCTION get_epoch_from_ts(timestamptz) RETURNS bigint AS $body$
BEGIN
  RETURN FLOOR((EXTRACT(EPOCH FROM $1) - EXTRACT(EPOCH FROM '2010-01-01'::timestamp)) * 1000);
END;
$body$ LANGUAGE plpgsql IMMUTABLE;

CREATE OR REPLACE FUNCTION get_ts_from_epoch(bigint) RETURNS timestamp AS $body$
BEGIN
  RETURN TIMESTAMP 'epoch' + ($1 + EXTRACT(EPOCH FROM '2010-01-01'::timestamp) * 1000) * INTERVAL '1 millisecond';
END;
$body$ LANGUAGE plpgsql IMMUTABLE;

/*
Copyright (c) 2015 Paul Jolly <paul@myitcv.org.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

CREATE OR REPLACE FUNCTION process_timetravel_before() RETURNS TRIGGER AS $timetravel_before$
DECLARE
  time_now TIMESTAMP := now(); -- get the time now just once.... for consistency's sake
BEGIN
  -- http://blog.myitcv.org.uk/2014/02/25/row-level-version-control-with-postgresql.html
  IF (TG_OP = 'UPDATE' OR TG_OP = 'DELETE') THEN
    -- the user should not be able to update historic rows
    IF upper(OLD.valid) != 'infinity' THEN
      RAISE EXCEPTION 'Cannot % old row', TG_OP;
    END IF;

    -- use of TG_TABLE_NAME keeps this generic and non-table specific
    -- see http://www.postgresql.org/docs/9.3/static/plpgsql-statements.html#PLPGSQL-STATEMENTS-EXECUTING-DYN
    EXECUTE 'SELECT * FROM ' || TG_TABLE_NAME::regclass || ' WHERE id = $1 FOR UPDATE' USING OLD.id;

    NEW.version := OLD.version + 1;

    -- if new and old valids aren't equal, the update statement is probably providing a valid value, use it
    IF (lower(NEW.valid) <> lower(OLD.valid)) THEN
      time_now := lower(NEW.valid);
    END IF;

    IF (TG_OP = 'UPDATE') THEN
      -- 'bump' the valid_from and ensure valid_to = 'infinity'
      NEW.valid := tstzrange(time_now, 'infinity');

      -- allow the update to continue... so that the correct number of rows are reported
      -- as having been affected
      RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
      -- we want to allow the delete to continue... so that the correct number of rows are reported
      -- as having been affected
      OLD.valid := tstzrange(lower(OLD.valid), time_now);
      RETURN OLD;
    END IF;
  ELSIF (TG_OP = 'INSERT') THEN
    IF (NEW.valid IS NULL) THEN
      NEW.valid := tstzrange(time_now, 'infinity');
    END IF;

    -- allow the insert to continue... so that the correct number of rows are reported
    -- as having been affected
    RETURN NEW;
  END IF;

  RETURN NULL; -- won't get here if we only create the trigger for insert, update and delete
END;
$timetravel_before$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION process_timetravel_after() RETURNS TRIGGER AS $timetravel_after$
DECLARE
  time_now TIMESTAMP := now(); -- get the time now just once.... for consistency's sake
BEGIN
  -- http://blog.myitcv.org.uk/2014/02/25/row-level-version-control-with-postgresql.html
  IF (TG_OP = 'UPDATE' OR TG_OP = 'DELETE') THEN
    OLD.version := NEW.version - 1;

    -- not sure whether this is strictly required... could we modify OLD without side effects?
    IF (TG_OP = 'UPDATE') THEN
      OLD.valid := tstzrange(lower(OLD.valid), lower(NEW.valid));
    ELSIF (TG_OP = 'DELETE') THEN
      OLD.valid := tstzrange(lower(OLD.valid), time_now);
    END IF;

    -- again, use of TG_TABLE_NAME keeps this generic and non-table specific
    EXECUTE 'INSERT INTO ' || TG_TABLE_NAME::regclass || ' SELECT $1.*' USING OLD;
  END IF;

  RETURN NULL; -- return value doesn't matter in after
END;
$timetravel_after$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION pluck_at(jsonb) RETURNS timestamptz[] AS $body$
  SELECT array_agg((t->>'at')::timestamptz)
  FROM jsonb_array_elements($1) AS t;
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION process_email() RETURNS TRIGGER AS $body$
DECLARE
    attrs text[];
BEGIN
  IF ((NEW.doc->>'sample')::bool) THEN
    RETURN NULL;
  END IF;

  IF (NEW.type = 'drip') THEN
    IF (NEW.doc ? 'lead' AND (NEW.doc->'lead') ? '$attributes') THEN
        attrs := ARRAY(SELECT jsonb_array_elements_text(NEW.doc#>'{lead,$attributes}'));
    END IF;

    IF (TG_OP = 'INSERT') THEN
      EXECUTE FORMAT('
        INSERT INTO %I_stats_drips (id, lead, status, segment, step, attributes, queued, delivered, bounced, optout, clicked, opened, message, template)
        VALUES (%L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L)'
        , TG_TABLE_NAME
        , NEW.id
        , COALESCE(NEW.doc->'lead'->>'_id', NEW.doc->'lead'->>'id')
        , COALESCE(NEW.doc->'status'->>'_id', NEW.doc->'status'->>'id')
        , (NEW.doc->'segment'->>'id')
        , (NEW.doc->'step'->>'id')
        , attrs
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , COALESCE(NEW.doc->'message'->>'id', NEW.doc->>'message')
        , COALESCE(NEW.doc->'template'->>'value', NEW.doc->>'template'));
    ELSIF (TG_OP = 'UPDATE') THEN
      EXECUTE FORMAT('
        UPDATE %I_stats_drips SET
            lead = %L
          , status = %L
          , segment = %L
          , step = %L
          , attributes = %L
          , queued = %L
          , delivered = %L
          , bounced = %L
          , optout = %L
          , clicked = %L
          , opened = %L
          , message = %L
          , template = %L
        WHERE id = %L'
        , TG_TABLE_NAME
        , COALESCE(NEW.doc->'lead'->>'_id', NEW.doc->'lead'->>'id')
        , COALESCE(NEW.doc->'status'->>'_id', NEW.doc->'status'->>'id')
        , (NEW.doc->'segment'->>'id')
        , (NEW.doc->'step'->>'id')
        , attrs
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , COALESCE(NEW.doc->'message'->>'id', NEW.doc->>'message')
        , COALESCE(NEW.doc->'template'->>'value', NEW.doc->>'template')
        , NEW.id);
    END IF;
  ELSIF (NEW.type = 'batch') THEN
    IF (TG_OP = 'INSERT') THEN
      EXECUTE FORMAT('
        INSERT INTO %I_stats_batches (id, batch, queued, delivered, bounced, optout, clicked, opened, message, template)
        VALUES (%L, %L, %L, %L, %L, %L, %L, %L, %L, %L)'
        , TG_TABLE_NAME
        , NEW.id
        , COALESCE(NEW.doc->'batch'->>'_id', NEW.doc->'batch'->>'id')
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , COALESCE(NEW.doc->'message'->>'id', NEW.doc->>'message')
        , COALESCE(NEW.doc->'template'->>'value', NEW.doc->>'template'));
    ELSIF (TG_OP = 'UPDATE') THEN
      EXECUTE FORMAT('
        UPDATE %I_stats_batches SET
            batch = %L
          , queued = %L
          , delivered = %L
          , bounced = %L
          , optout = %L
          , clicked = %L
          , opened = %L
          , message = %L
          , template = %L
        WHERE id = %L'
        , TG_TABLE_NAME
        , COALESCE(NEW.doc->'batch'->>'_id', NEW.doc->'batch'->>'id')
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , COALESCE(NEW.doc->'message'->>'id', NEW.doc->>'message')
        , COALESCE(NEW.doc->'template'->>'value', NEW.doc->>'template')
        , NEW.id);
    END IF;
  ELSIF (NEW.type = 'direct') THEN
    RAISE NOTICE '%', NEW.doc;
    IF (TG_OP = 'INSERT') THEN
      EXECUTE FORMAT('
        INSERT INTO %I_stats_direct (id, queued, delivered, bounced, optout, clicked, opened)
        VALUES (%L, %L, %L, %L, %L, %L, %L)'
        , TG_TABLE_NAME
        , NEW.id
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened')));
    ELSIF (TG_OP = 'UPDATE') THEN
      EXECUTE FORMAT('
        UPDATE %I_stats_direct SET
            queued = %L
          , delivered = %L
          , bounced = %L
          , optout = %L
          , clicked = %L
          , opened = %L
        WHERE id = %L'
        , TG_TABLE_NAME
        , COALESCE(NEW.doc->'queued'->>'at', NEW.doc->>'queued')
        , COALESCE(
            COALESCE(NEW.doc->'sent'->>'at', NEW.doc->>'sent')
            , COALESCE(NEW.doc->'delivered'->>'at', NEW.doc->>'delivered'))
        , COALESCE(NEW.doc->'bounced'->>'at', NEW.doc->>'bounced')
        , pluck_at(jsonb_extract_path(NEW.doc, 'optout'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'clicked'))
        , pluck_at(jsonb_extract_path(NEW.doc, 'opened'))
        , NEW.id);
    END IF;
  END IF;

  RETURN NULL;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insert_event_trigger() RETURNS trigger AS $body$
DECLARE
  tbl_name TEXT;
  start_date DATE = cast(date_trunc('month', NEW.at AT TIME ZONE 'utc') AS DATE);
BEGIN
  /*... create the new partition and set up the redirect Rules ...*/
  tbl_name = to_char(start_date, '"events_y"YYYY"m"MM');

  -- Check if the partition needed for the current record exists
  PERFORM 1
  FROM   pg_catalog.pg_class c
  JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
  WHERE  c.relkind = 'r'
  AND    c.relname = tbl_name
  AND    n.nspname = TG_TABLE_SCHEMA;

  -- If the partition needed does not yet exist, then we create it:
  IF NOT FOUND THEN
    EXECUTE FORMAT('LOCK TABLE %I._events IN ACCESS EXCLUSIVE MODE;', TG_TABLE_SCHEMA);

    PERFORM 1
    FROM   pg_catalog.pg_class c
    JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relkind = 'r'
    AND    c.relname = tbl_name
    AND    n.nspname = TG_TABLE_SCHEMA;

    IF NOT FOUND THEN
      EXECUTE FORMAT('
        CREATE TABLE IF NOT EXISTS %I.%I (
          CHECK (at >= TIMESTAMPTZ %L AND at < TIMESTAMPTZ %L)
        ) INHERITS (%1$I._events);

        CREATE INDEX %2$I_at_ix ON %1$I.%2$I (at);
        CREATE INDEX %2$I_typeat_ix ON %1$I.%2$I (type, at);

        CREATE INDEX %2$I_emails_ix ON %1$I.%2$I ((doc->>''email'')) WHERE doc ? ''email'';
        CREATE INDEX %2$I_leads_ix ON %1$I.%2$I ((doc->>''lead'')) WHERE doc ? ''lead'';'
        , TG_TABLE_SCHEMA
        , tbl_name
        , start_date
        , cast(start_date + INTERVAL '1 month' AS DATE));
    END IF;
  END IF;

  /* Redo the INSERT dynamically.  The new RULE will redirect it to the child table */
  EXECUTE FORMAT('INSERT INTO %I.%I SELECT $1.*', TG_TABLE_SCHEMA, tbl_name) USING NEW;
  RETURN NEW;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insert_email_trigger() RETURNS trigger AS $body$
DECLARE
  tbl_name TEXT;
  bom TIMESTAMP := date_trunc('month', get_ts_from_epoch(NEW.id));
BEGIN
  /*... create the new partition and set up the redirect Rules ...*/
  tbl_name = to_char(bom, '"emails_y"YYYY"m"MM');

  -- Check if the partition needed for the current record exists
  PERFORM 1
  FROM   pg_catalog.pg_class c
  JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
  WHERE  c.relkind = 'r'
  AND    c.relname = tbl_name
  AND    n.nspname = TG_TABLE_SCHEMA;

  -- If the partition needed does not yet exist, then we create it:
  IF NOT FOUND THEN
    EXECUTE FORMAT('LOCK TABLE %I._emails IN ACCESS EXCLUSIVE MODE;', TG_TABLE_SCHEMA);

    PERFORM 1
    FROM   pg_catalog.pg_class c
    JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relkind = 'r'
    AND    c.relname = tbl_name
    AND    n.nspname = TG_TABLE_SCHEMA;

    IF NOT FOUND THEN
      EXECUTE FORMAT('
        CREATE TABLE IF NOT EXISTS %I.%I (
          CHECK (id >= %s AND id < %s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails);

        CREATE TABLE IF NOT EXISTS %1$I.%2$I_stats (
          CHECK (id >= %3$s AND id < %4$s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails_stats);

        CREATE TABLE IF NOT EXISTS %1$I.%2$I_stats_batches (
          CHECK (id >= %3$s AND id < %4$s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails_stats_batches);

        CREATE TABLE IF NOT EXISTS %1$I.%2$I_stats_drips (
          CHECK (id >= %3$s AND id < %4$s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails_stats_drips);

        CREATE TABLE IF NOT EXISTS %1$I.%2$I_stats_direct (
          CHECK (id >= %3$s AND id < %4$s),
          PRIMARY KEY (id)
        ) INHERITS (%1$I._emails_stats_direct);

        CREATE INDEX %2$I_type ON %1$I.%2$I (type);
        CREATE INDEX %2$I_gin_doc ON %1$I.%2$I USING gin (doc);

        CREATE TRIGGER %2$I_stats
          AFTER INSERT OR UPDATE ON %1$I.%2$I
          FOR EACH ROW EXECUTE PROCEDURE process_email();'
        , TG_TABLE_SCHEMA
        , tbl_name
        , get_epoch_from_ts(bom)
        , get_epoch_from_ts(bom + INTERVAL '1 month'));
    END IF;
  END IF;

  /* Redo the INSERT dynamically.  The new RULE will redirect it to the child table */
  EXECUTE FORMAT('INSERT INTO %I.%I SELECT $1.*', TG_TABLE_SCHEMA, tbl_name) USING NEW;
  RETURN NEW;
END;
$body$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS _events (
  id uuid NOT NULL,
  type text NOT NULL,
  at timestamptz NOT NULL,
  doc jsonb
);

CREATE TABLE IF NOT EXISTS _emails (
  id bigint NOT NULL,
  type text,
  doc jsonb,
  html text,
  text text
);

CREATE TABLE IF NOT EXISTS _emails_stats (
  id bigint NOT NULL,
  queued timestamptz,
  delivered timestamptz,
  bounced timestamptz,
  optout timestamptz[],
  opened timestamptz[],
  clicked timestamptz[],
  message uuid,
  template uuid,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS _emails_stats_batches (
  batch uuid,
  PRIMARY KEY (id)
) INHERITS (_emails_stats);

CREATE TABLE IF NOT EXISTS _emails_stats_drips (
  lead uuid,
  status uuid,
  segment uuid,
  step uuid,
  attributes text[],
  PRIMARY KEY (id)
) INHERITS (_emails_stats);

CREATE TABLE IF NOT EXISTS _emails_stats_direct (
  PRIMARY KEY (id)
) INHERITS (_emails_stats);

CREATE TABLE IF NOT EXISTS _data (
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  valid tstzrange NOT NULL DEFAULT tstzrange(NOW(), 'infinity'),
  version int NOT NULL DEFAULT 1,
  doc jsonb,
  EXCLUDE USING gist (cast(id as "text") WITH =, valid WITH &&),
  --CHECK (lower_inc(valid) AND NOT upper_inc(valid)),
  UNIQUE (id, version),
  PRIMARY KEY (id, valid)
);

DROP TRIGGER IF EXISTS _data_before ON _data;
CREATE TRIGGER _data_before
  BEFORE INSERT OR UPDATE OR DELETE ON _data
  FOR EACH ROW EXECUTE PROCEDURE process_timetravel_before();

DROP TRIGGER IF EXISTS _data_after ON _data;
CREATE TRIGGER _data_after
  AFTER UPDATE OR DELETE ON _data
  FOR EACH ROW EXECUTE PROCEDURE process_timetravel_after();

CREATE TABLE IF NOT EXISTS clients (
  active boolean,
  UNIQUE (id, version),
  PRIMARY KEY (id, valid)
) INHERITS (_data);

CREATE OR REPLACE VIEW clients_current AS
  SELECT id, version, lower(valid) AS published, doc
  FROM clients
  WHERE upper(valid) = 'infinity';

DROP TRIGGER IF EXISTS clients_before ON clients;
CREATE TRIGGER clients_before
  BEFORE INSERT OR UPDATE OR DELETE ON clients
  FOR EACH ROW EXECUTE PROCEDURE process_timetravel_before();

DROP TRIGGER IF EXISTS clients_after ON clients;
CREATE TRIGGER clients_after
  AFTER UPDATE OR DELETE ON clients
  FOR EACH ROW EXECUTE PROCEDURE process_timetravel_after();

CREATE TABLE IF NOT EXISTS templates (
  name text,
  client uuid,
  UNIQUE (name, client),
  PRIMARY KEY (id)
) INHERITS (_data);

CREATE TABLE IF NOT EXISTS users (
  username citext,
  UNIQUE (username),
  PRIMARY KEY (id)
) INHERITS (_data);

CREATE OR REPLACE VIEW users_current AS
  SELECT id, username, version, lower(valid) AS published, doc
  FROM users
  WHERE upper(valid) = 'infinity';

CREATE TABLE IF NOT EXISTS settings (
  user_id uuid REFERENCES users,
  client_id uuid --REFERENCES clients (id)
) INHERITS (_data);

DROP TRIGGER IF EXISTS settings_before ON settings;
CREATE TRIGGER settings_before
  BEFORE INSERT OR UPDATE OR DELETE ON settings
  FOR EACH ROW EXECUTE PROCEDURE process_timetravel_before();

DROP TRIGGER IF EXISTS settings_after ON settings;
CREATE TRIGGER settings_after
  AFTER UPDATE OR DELETE ON settings
  FOR EACH ROW EXECUTE PROCEDURE process_timetravel_after();

CREATE OR REPLACE VIEW settings_current AS
  SELECT id, user_id, client_id, version, lower(valid) AS published, doc
  FROM settings
  WHERE upper(valid) = 'infinity';

CREATE TABLE IF NOT EXISTS apikeys (
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  client_id uuid NOT NULL,
  issued timestamptz NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id, client_id)
);

CREATE TABLE IF NOT EXISTS optouts (
  id text PRIMARY KEY,
  created timestamptz DEFAULT NOW(),
  updated timestamptz DEFAULT NOW(),
  doc jsonb
);

DROP INDEX IF EXISTS optouts_client_updated_ix;
CREATE INDEX optouts_client_updated_ix ON optouts USING gin (doc);

-- new schema?
CREATE TABLE IF NOT EXISTS jobs (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v1(),
  action text NOT NULL,
  status text NOT NULL,
  at timestamptz NOT NULL,
  repeat text,
  created timestamptz DEFAULT NOW(),
  updated timestamptz DEFAULT NOW(),
  data jsonb
);

DROP INDEX IF EXISTS jobs_pending_ix;
CREATE INDEX jobs_pending_ix ON jobs (status, at);

CREATE TABLE IF NOT EXISTS logs (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v1(),
  client uuid,
  source text,
  at timestamptz DEFAULT NOW(),
  doc jsonb
);

DROP INDEX IF EXISTS logs_at_ix;
CREATE INDEX logs_at_ix ON logs (at);

CREATE OR REPLACE FUNCTION create_client_trigger() RETURNS trigger AS $body$
DECLARE
  _exists text;
  nsp_name text := 'client_' || REPLACE(NEW.id::text, '-', '');
BEGIN
  EXECUTE FORMAT('SELECT usename FROM pg_catalog.pg_user WHERE usename = %L LIMIT 1', current_database() || '_' || nsp_name)
  INTO _exists;

  IF _exists IS NULL THEN
    EXECUTE FORMAT('CREATE USER %I_%I IN ROLE keystone', current_database(), nsp_name);
  END IF;

  EXECUTE FORMAT('CREATE SCHEMA IF NOT EXISTS %I AUTHORIZATION %2$I_%1$I', nsp_name, current_database());

  EXECUTE 'SET search_path TO ' || nsp_name || ', ''keystone''';
  EXECUTE 'CREATE TABLE IF NOT EXISTS _events (
    PRIMARY KEY (id)
  ) INHERITS (keystone._events)';

  CREATE OR REPLACE VIEW events AS SELECT * FROM _events;
  ALTER VIEW IF EXISTS events ALTER COLUMN id SET DEFAULT uuid_generate_v1();
  ALTER VIEW IF EXISTS events ALTER COLUMN at SET DEFAULT CLOCK_TIMESTAMP();

  -- if we just had a before insert trigger on a regular table, we could not use RETURNING id to get the id back
  -- to the client after an insert
  DROP TRIGGER IF EXISTS events_before ON events;
  CREATE TRIGGER events_before
    INSTEAD OF INSERT ON events
    FOR EACH ROW EXECUTE PROCEDURE insert_event_trigger();

  --SELECT partman.create_parent('client_{client}.events', 'at', 'time', 'monthly', p_premake := 1, p_start_partition := CLOCK_TIMESTAMP()::text);

  CREATE TABLE IF NOT EXISTS data (
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    valid tstzrange NOT NULL DEFAULT tstzrange(NOW(), 'infinity'),
    version int NOT NULL DEFAULT 1,
    type text NOT NULL,
    doc jsonb,
    EXCLUDE USING gist (cast(id as "text") WITH =, valid WITH &&),
    --CHECK (lower_inc(valid) AND NOT upper_inc(valid)),
    UNIQUE (id, version),
    PRIMARY KEY (id, valid)
  );

  DROP INDEX IF EXISTS data_types_ix;
  CREATE UNIQUE INDEX data_types_ix ON data (type, id, valid DESC NULLS LAST);

  DROP INDEX IF EXISTS data_scd_ix;
  CREATE INDEX data_scd_ix ON data USING gist (cast(id as "text"), valid);

  DROP INDEX IF EXISTS data_scdcurrent_ix;
  CREATE INDEX data_scdcurrent_ix ON data (id) WHERE upper(valid) = 'infinity';

  DROP INDEX IF EXISTS data_senderaliases_ix;
  CREATE INDEX data_senderaliases_ix ON data ((doc->>'aliases')) WHERE type = 'sender';

  DROP TRIGGER IF EXISTS data_before ON data;
  CREATE TRIGGER data_before
    BEFORE INSERT OR UPDATE OR DELETE ON data
    FOR EACH ROW EXECUTE PROCEDURE process_timetravel_before();

  DROP TRIGGER IF EXISTS data_after ON data;
  CREATE TRIGGER data_after
    AFTER UPDATE OR DELETE ON data
    FOR EACH ROW EXECUTE PROCEDURE process_timetravel_after();

  CREATE OR REPLACE VIEW data_current AS
    SELECT id, type, version, lower(valid) AS published, doc
    FROM data
    WHERE upper(valid) = 'infinity';

  CREATE TABLE IF NOT EXISTS _emails (
    PRIMARY KEY (id)
  ) INHERITS (keystone._emails);

  CREATE TABLE IF NOT EXISTS _emails_stats (
    PRIMARY KEY (id)
  ) INHERITS (keystone._emails_stats);

  CREATE TABLE IF NOT EXISTS _emails_stats_batches (
    PRIMARY KEY (id)
  ) INHERITS (keystone._emails_stats_batches, _emails_stats);

  CREATE TABLE IF NOT EXISTS _emails_stats_drips (
    PRIMARY KEY (id)
  ) INHERITS (keystone._emails_stats_drips, _emails_stats);

  CREATE TABLE IF NOT EXISTS _emails_stats_direct (
    PRIMARY KEY (id)
  ) INHERITS (keystone._emails_stats_direct, _emails_stats);

  CREATE OR REPLACE VIEW emails AS SELECT * FROM _emails;
  ALTER VIEW IF EXISTS emails ALTER COLUMN id SET DEFAULT get_epoch_from_ts(clock_timestamp());

  CREATE OR REPLACE VIEW batch_stats AS SELECT * FROM _emails_stats_batches;
  CREATE OR REPLACE VIEW drip_stats AS SELECT * FROM _emails_stats_drips;
  CREATE OR REPLACE VIEW direct_stats AS SELECT * FROM _emails_stats_direct;

  -- see information from events trigger
  DROP TRIGGER IF EXISTS emails_before ON emails;
  CREATE TRIGGER emails_before
    INSTEAD OF INSERT ON emails
    FOR EACH ROW EXECUTE PROCEDURE insert_email_trigger();

  --SELECT partman.create_parent('client_{client}.emails', 'at', 'time', 'monthly', p_premake := 1, p_start_partition := CLOCK_TIMESTAMP()::text, p_constraint_cols := '{"id"}');

  CREATE TABLE IF NOT EXISTS batches (
    type text DEFAULT 'batch',
    status text,
    csv bytea,
    CHECK (type = 'batch'),
    UNIQUE (id, version),
    PRIMARY KEY (id, valid)
  ) INHERITS (data);

  DROP TRIGGER IF EXISTS batches_before ON batches;
  CREATE TRIGGER batches_before
    BEFORE INSERT OR UPDATE OR DELETE ON batches
    FOR EACH ROW EXECUTE PROCEDURE process_timetravel_before();

  DROP TRIGGER IF EXISTS batches_after ON batches;
  CREATE TRIGGER batches_after
    AFTER UPDATE OR DELETE ON batches
    FOR EACH ROW EXECUTE PROCEDURE process_timetravel_after();

  CREATE OR REPLACE VIEW batches_current AS
    SELECT id, type, version, lower(valid) AS published, status, doc, csv
    FROM batches
    WHERE upper(valid) = 'infinity';

  CREATE TABLE IF NOT EXISTS uploads (
    status text,
    file bytea,
    CHECK (type IN ('lead.import', 'lead.import.error', 'optout.import', 'optout.import.error')),
    UNIQUE (id, version),
    PRIMARY KEY (id, valid)
  ) INHERITS (data);

  DROP TRIGGER IF EXISTS uploads_before ON uploads;
  CREATE TRIGGER uploads_before
    BEFORE INSERT OR UPDATE OR DELETE ON uploads
    FOR EACH ROW EXECUTE PROCEDURE process_timetravel_before();

  DROP TRIGGER IF EXISTS uploads_after ON uploads;
  CREATE TRIGGER uploads_after
    AFTER UPDATE OR DELETE ON uploads
    FOR EACH ROW EXECUTE PROCEDURE process_timetravel_after();

  CREATE OR REPLACE VIEW uploads_current AS
    SELECT id, type, version, lower(valid) AS published, doc, status, file
    FROM uploads
    WHERE upper(valid) = 'infinity';

  CREATE TABLE IF NOT EXISTS leads (
    type text DEFAULT 'lead',
    CHECK (type = 'lead'),
    UNIQUE (id, version),
    PRIMARY KEY (id, valid)
  ) INHERITS (data);

  DROP TRIGGER IF EXISTS leads_before ON leads;
  CREATE TRIGGER leads_before
    BEFORE INSERT OR UPDATE OR DELETE ON leads
    FOR EACH ROW EXECUTE PROCEDURE process_timetravel_before();

  DROP TRIGGER IF EXISTS leads_after ON leads;
  CREATE TRIGGER leads_after
    AFTER UPDATE OR DELETE ON leads
    FOR EACH ROW EXECUTE PROCEDURE process_timetravel_after();

  CREATE OR REPLACE VIEW leads_current AS
    SELECT id, version, lower(valid) AS published, doc
    FROM leads
    WHERE upper(valid) = 'infinity';

  DROP INDEX IF EXISTS leads_doc_gin;
  CREATE INDEX leads_doc_gin ON leads USING gin (doc);

  DROP INDEX IF EXISTS leads_scdcurrent_ix;
  CREATE INDEX leads_scdcurrent_ix ON data (id) WHERE upper(valid) = 'infinity';

  DROP INDEX IF EXISTS leads_segment_ix;
  CREATE INDEX leads_segment_ix ON leads
    ((doc->>'$status'), (doc->>'segment'), COALESCE((doc->>'segmented'), (doc->>'created')))
    WHERE upper(valid) = 'infinity' AND (doc ? '$status' AND doc ? 'segment');

  DROP INDEX IF EXISTS leads_externalid_ix;
  CREATE UNIQUE INDEX leads_externalid_ix ON leads
    ((doc->'external'->>'source'), (doc->'external'->>'id'))
    WHERE upper(valid) = 'infinity' AND (doc ? 'external');

  RETURN NEW;
END;
$body$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS client_create ON clients;
CREATE TRIGGER client_create
  AFTER INSERT ON clients
  FOR EACH ROW
  WHEN (NEW.version = 1)
  EXECUTE PROCEDURE create_client_trigger();

CREATE SCHEMA IF NOT EXISTS dns;
CREATE TABLE IF NOT EXISTS dns.job (
  id serial NOT NULL,
  type character varying(50),
  client_id uuid,
  started timestamptz DEFAULT now(),
  CONSTRAINT dns_job_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS dns.records (
  id serial NOT NULL,
  client_id uuid NOT NULL,
  isglobal boolean,
  name text,
  type text,
  domain text,
  value text,
  created timestamptz,
  updated timestamptz DEFAULT now(),
  isdeleted boolean,
  CONSTRAINT dns_records_pkey PRIMARY KEY (id, client_id)
);

CREATE TABLE IF NOT EXISTS dns.settings (
  id serial NOT NULL,
  zone_id text,
  access_key_id text,
  secret_access_key text,
  email_to text,
  CONSTRAINT dns_settings_pkey PRIMARY KEY (id)
);
