_ = require 'lodash'
pg = require 'pg'
QueryStream = require 'pg-query-stream'
{Database} = require './index'
MultiQueryDatabase = require './multi_query_database'

module.exports = class SingleQueryDatabase extends Database
  constructor: (@url, @client) ->
    super

  query: (sql, args, cb) ->
    meta = {}
    [args, cb] = [null, args] if not cb? and _.isFunction args

    # moving the toString here, which is for squel, will give us a better stack trace if query is bad
    meta.squel = sql
    sql = sql.toString()

    Error.captureStackTrace meta

    # TODO handle connect error
    pg.connect @url, (err, client, done) =>
      if err?
        console.error err
        return cb err

      @setpath client, (err) =>
        if err?
          console.error err
          done err
          return cb err

        super client, sql, args, meta, (err) ->
          done err
          cb? arguments...

  stream: (sql, args, cb) ->
    sql = sql.toString()

    [args, cb] = [null, args] if not cb? and _.isFunction args

    # TODO return a stream through the callback
    # attach the close to the stream 'end'
    pg.connect @url, (err, client, done) =>
      return cb err if err?

      @setpath client, (err) ->
        if err?
          console.error err
          done err
          return cb err

        stream = client.query new QueryStream sql, args
        stream.on 'end', done
        cb null, stream

  multiple: (cb) ->
    # TODO handle connect error
    cb new MultiQueryDatabase @url, @client
