(function() {
  var Database, MultiQueryDatabase, QueryStream, SingleQueryDatabase, _, pg,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  pg = require('pg');

  QueryStream = require('pg-query-stream');

  Database = require('./index').Database;

  MultiQueryDatabase = require('./multi_query_database');

  module.exports = SingleQueryDatabase = (function(superClass) {
    extend(SingleQueryDatabase, superClass);

    function SingleQueryDatabase(url, client1) {
      this.url = url;
      this.client = client1;
      SingleQueryDatabase.__super__.constructor.apply(this, arguments);
    }

    SingleQueryDatabase.prototype.query = function(sql, args, cb) {
      var meta, ref;
      meta = {};
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      meta.squel = sql;
      sql = sql.toString();
      Error.captureStackTrace(meta);
      return pg.connect(this.url, (function(_this) {
        return function(err, client, done) {
          if (err != null) {
            console.error(err);
            return cb(err);
          }
          return _this.setpath(client, function(err) {
            if (err != null) {
              console.error(err);
              done(err);
              return cb(err);
            }
            return SingleQueryDatabase.__super__.query.call(_this, client, sql, args, meta, function(err) {
              done(err);
              return typeof cb === "function" ? cb.apply(null, arguments) : void 0;
            });
          });
        };
      })(this));
    };

    SingleQueryDatabase.prototype.stream = function(sql, args, cb) {
      var ref;
      sql = sql.toString();
      if ((cb == null) && _.isFunction(args)) {
        ref = [null, args], args = ref[0], cb = ref[1];
      }
      return pg.connect(this.url, (function(_this) {
        return function(err, client, done) {
          if (err != null) {
            return cb(err);
          }
          return _this.setpath(client, function(err) {
            var stream;
            if (err != null) {
              console.error(err);
              done(err);
              return cb(err);
            }
            stream = client.query(new QueryStream(sql, args));
            stream.on('end', done);
            return cb(null, stream);
          });
        };
      })(this));
    };

    SingleQueryDatabase.prototype.multiple = function(cb) {
      return cb(new MultiQueryDatabase(this.url, this.client));
    };

    return SingleQueryDatabase;

  })(Database);

}).call(this);
