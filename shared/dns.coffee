# ________    _______    _________   _________ __          _____  _____
# \______ \   \      \  /   _____/  /   _____//  |_ __ ___/ ____\/ ____\
#  |    |  \  /   |   \ \_____  \   \_____  \\   __\  |  \   __\\   __\
#  |    `   \/    |    \/        \  /        \|  | |  |  /|  |   |  |
# /_______  /\____|__  /_______  / /_______  /|__| |____/ |__|   |__|
#         \/         \/        \/          \/
#
# DNS Files:                                  Q1 2016 - njw
#
# webadmin\app.coffee                       ~ Line 500
# webadmin\public\stylesheets\sb-admin.css  ~ Line 250
# shared\dns.coffee                         ~ Line 1 (this file)
# webadmin\views\index.jade                 ~ Line 775
# webadmin\public\javascripts\index.coffee  ~ Line 950

config = require 'config'

_ = require 'lodash'
async = require 'async'
moment = require 'moment'
dns = require 'native-dns'
keypair = require 'keypair'
# use nodeforge
nodemailer = require 'nodemailer'
pg = require 'pg'
route53 = require 'nice-route53'

zero_id = '00000000-0000-0000-0000-000000000000'

module.exports = class Dns

  # page load
  _connect: (cb) ->
    pg.connect config.db.url, cb

  get_clients: (cb) ->
    @._connect (err, client, done) ->
      return cb err if err?
      q = "select id, doc->'name' as name from keystone.clients_current;"
      client.query q, (err, result) ->
        done()
        return cb err if err?
        return cb { message: 'unable to find any clients' } if result.rows.length is 0
        cb null, result.rows

  get_records: (cb) ->
    @._connect (err, client, done) ->
      return cb err if err?
      q = "select * from dns.records where (isglobal = true or client_id = $1) and isdeleted is not true order by isglobal desc, id;"
      client.query q, [zero_id], (err, result) ->
        done()
        return cb err if err?
        return cb { message: 'unable to find any records' } if result.rows.length is 0
        cb null, result.rows

  # actionqueue

  get_actionqueue: (cb) ->
    @._connect (err, client, done) ->
      return cb err if err?
      q = "select status, repeat from keystone.jobs where action = 'dns.coffee';"
      client.query q, (err, result) ->
        done()
        return cb err if err?
        return cb { message: 'no records matching "dns.coffee"' } if result.rows.length is 0
        cb null, result.rows

  post_actionqueue: (status, cron, cb) ->
    @._connect (err, client, done) ->
      return cb err if err?
      q = "update keystone.jobs set status = $1, repeat = $2 where action = 'dns.coffee';"
      client.query q, [status, cron], (err, result) ->
        done()
        return cb err if err?
        cb null

  # settings

  get_settings: (cb) ->
    @._connect (err, client, done) ->
      return cb err if err?
      q = "select email_to, zone_id from dns.settings where id = 1;"
      client.query q, (err, result) ->
        done()
        return cb err if err?
        return cb 'no dns_settings record for id 1' if result.rows.length is 0
        cb null, result.rows

  post_settings: (email, zone, cb) ->
    @._connect (err, client, done) ->
      return cb err if err?
      q = "update dns.settings set email_to = $1, zone_id = $2 where id = 1;"
      client.query q, [email, zone], (err, result) ->
        done()
        return cb err if err?
        cb null

  # records

  # record_results = []

  post_record: (id, client_id, isglobal, name, type, domain, value, cb) ->
    @._connect (err, client, done) ->
      return cb err if err?
      client_id = if client_id is 'default' then zero_id else client_id
      created = moment().format()
      args = [client_id, isglobal, name, type, domain, value]
      if id is '0'
        q = "insert into dns.records (client_id, isglobal, name, type, domain, value, created) values ($1, $2, $3, $4, $5, $6, $7);"
        args.push new Date()
      else
        q = "update dns.records set client_id = $1, isglobal = $2, name = $3, type = $4, domain = $5, value = $6 where id = $7;"
        args.push id
      client.query q, args, (err, result) ->
        done()
        return cb err if err?
        cb null

  delete_record: (id, cb) ->
    @._connect (err, client, done) ->
      return cb err if err?
      q = "update dns.records set isdeleted = true where id = $1;"
      client.query q, [id], (err, result) ->
        done()
        return cb err if err?
        cb null

  verify_records: (db_records, cb) ->
    get_zone_id (err, zone_id) ->
      return cb err if err?
      get_zone_records zone_id, (err, zone_records) ->
        return cb err if err?
        # return cb null, { db_records: db_records, zone_records: zone_records }
        get_job_id (err, job_id) ->
          return cb err if err?
          # return cb { message: 'job_id = ' + job_id }
          i = 0
          while i < db_records.length
            # return cb { message: db_records[i] }
            get_dns_record db_records[i], (err, dns_record) ->
              return cb err if err?
              return cb { message: 'unable to find record by id' } if dns_record.length is 0
              # return cb { message: 'after get_dns_record...' }
              # return cb { message: 'dns_record = ' + dns_record } if dns_record?
              return cb err if err?
              found_match = false
              # return cb { message: 'ugh damnit' }
              # return cb { message: 'dns_record.type = ' + dns_record.type }
              j = 0
              while j < zone_records.length
                # return cb { message: 'dns_record.type = ' + dns_record.type }
                if dns_record.type is zone_records[j].type
                  # should probably loop through the values,
                  # but should be fine with TXT, MX and CNAME
                  if dns_record.domain is zone_records[j].values[0]
                    found_match = true
                    run_test dns_record, (err, answer) ->
                      cb null, answer
                j++
              # return cb { message: 'found_match = ' + found_match }
              # return cb { message: '!found_match = ' + !found_match }
              if found_match?
                upsert zone_id, dns_record, (err, res) ->
                  return cb err if err?

                  # return cb { message: 'after upsert...' }
                  # cb null, 'upserted!'
            i++
          # cb { message: 'nothing happened...' }
          cb null, 'success'

upsert = (zone_id, test, cb) ->
  # return cb { message: 'inside upsert...' }
  # return cb { message: 'zone_id = ' + zone_id + ', test.domain = ' + test.domain }
  try
    r53 = new route53 config.aws
    console.log 'setting record', test
    test_value = if test.type is 'TXT' then '"' + test.value + '"' else test.value
    # return cb { message: 'test_value = ' + test_value }
    args = {
      zoneId: zone_id
      name: test.domain
      type: test.type
      ttl: 300
      values: [ test_value ]
    }
    r53.setRecord args, (err, res) ->
      return cb { message: err.msg } if err?
      # return cb { message: 'res = ' + res }
      console.log res
      cb null, res
  catch e
    return cb e

get_job_id = (cb) ->
  @._connect (err, client, done) ->
    return cb err if err?
    q = "insert into dns.job (type) values ($1) returning id;"
    client.query q, ['test type'], (err, result) ->
      done()
      return cb err if err?
      cb null, result.rows[0].id

get_dns_record = (record_id, cb) ->
  # cb { message: 'inside get_dns_record...' }
  # cb { message: 'record_id = ' + record_id }
  @._connect (err, client, done) ->
    return cb err if err?
    q = "select * from dns.records where id = $1;"
    client.query q, [record_id], (err, result) ->
      done()
      return cb err if err?
      # return cb { message: 'result.rows = ' + result.rows }
      cb null, result.rows[0]

run_test = (dns_record, cb) ->
  if dns_record.type not in ['TXT', 'MX', 'CNAME']
    cb 'error: bad type', null
  else
    try
      start = Date.now()
      if dns_record.type is 'TXT'
        dns_record = '"{0}"'.f dns_record
      q = question(row.type, row.domain)
      r = request(q)
      r.on 'message', (err, dns) ->
        try
          return cb err if err?
          if dns.answer.length == 0
            cb 'error: no record found', null
          else
            a = dns.answer[0]
            switch row.type
              when 'MX'
                cb null, '{0} {1}'.f(a.priority, a.exchange)
              when 'TXT'
                cb null, '"{0}"'.f(a.data[0])
              when 'CNAME'
                cb null, a.data
              else
                cb 'error: bad type', null
        catch e
          return cb e
    catch e
      return cb e, null
    r.on 'timeout', ->
      return cb 'error: DNS query timeout', null
    r.send()

    #record_results = []
    ##i = 0
    #while i < records.length
    #  get_record records[i], (err, result) ->
    #    return cb err if err?
    #    record_results.push result
    #    console.log 'result.type: ' + result.type
    #  i++
    #console.log 'record_results: ' + record_results if i is records.length
    #cb null, record_results if i is records.length

    #get_zone_id (err, zone_id) ->
    #  r53 = new route53
    #  r53.records zone_id, (err, records) ->
    ##    return err if err?
     #   i = 0
     #   while i < records.length
     #     if records[i]
     ##     i++
      #  return cb null, records
      #return cb err if err?

    # i = 0
    # get_zone_id recor
    # @._connect (err, client, done) ->
    #   return cb err if err?
    #   q = "select * from dns.settings where id = 1;"
    #   client.query q, (err, result) ->
    #     done()
    #     return cb err if err?
    #     zone_id = result.zone_id

get_zone_id = (cb) ->
  @._connect (err, client, done) ->
    return cb err if err?
    q = "select zone_id from dns.settings where id = 1;"
    client.query q, (err, result) ->
      done()
      return cb err if err?
      return cb null, result.rows[0].zone_id

get_records = (records, cb) ->
  @._connect (err, client, done) ->
    return cb err if err?
    # NOT SQL INJECTION SAFE!!! Must fix before going live
    q = "select * from dns.records where id in (#{records});"
    client.query q, (err, result) ->
      return cb err if err?
      return cb null, result.rows

get_zone_records = (zone_id, cb) ->
  r53 = new route53
  r53.records zone_id, (err, records) ->
    return cb err if err?
    return cb null, records

verify_record_old: (records, cb) ->
  try
    console.log records[0]
    i = 0
    while i < records.length
      @._connect (err, client, done) ->
        return cb err if err?
        q = "select * from dns.records where id = $1;"
        client.query q, [records[i]], (err, result) ->
          try
            done()
            return cb err if err?
            # console.log result.rows[0].name
            digest_record result.rows[0], (err, ret) ->
            # return cb err if err?
            # cb null, ret
            # cb null, 'done'
              return cb err, ret
          catch e
            return cb e, null
      i++
    # cb null, 'done'
  catch e
    cb e, null

post_record: (id, client_id, isglobal, name, type, domain, value, cb) ->
  @._connect (err, client, done) ->
    return cb err if err?
    client_id = if client_id is 'default' then zero_id else client_id
    created = moment().format()
    args = [client_id, isglobal, name, type, domain, value]
    if id is '0'
      q = "insert into dns.records (client_id, isglobal, name, type, domain, value, created) values ($1, $2, $3, $4, $5, $6, $7);"
      args.push new Date()
    else
      q = "update dns.records set client_id = $1, isglobal = $2, name = $3, type = $4, domain = $5, value = $6 where id = $7;"
      args.push id
    client.query q, args, (err, result) ->
      done()
      return cb err if err?
      cb null

# private functions

get_record = (record_id, cb) ->
  @._connect (err, client, done) ->
    return cb err if err?
    q = "select * from dns.records where id = $1;"
    client.query q, [record_id], (err, result) ->
      done()
      return cb err if err?
      return result

check_record = (name, cb) ->
  cb null, name

# read_record = (zone_id

digest_record = (row, cb) ->
  # if (r = test.type) is not 'TXT' and r is not 'MX' and r is not 'CNAME'
  if row.type not in ['TXT', 'MX', 'CNAME']
    cb 'error: bad type', null
  else
    try
      start = Date.now()
      if row.type is 'TXT'
        row.value = '"{0}"'.f row.value
      q = question(row.type, row.domain)
      r = request(q)
      r.on 'message', (err, dns) ->
        return cb err if err?
        try
          if dns.answer.length == 0
            cb 'error: no record found', null
          else
            a = dns.answer[0]
            switch row.type
              when 'MX'
                cb null, '{0} {1}'.f(a.priority, a.exchange)
              when 'TXT'
                cb null, '"{0}"'.f(a.data[0])
              when 'CNAME'
                cb null, a.data
              else
                cb 'error: bad type', null
        catch e
          return cb e, null
    catch e
      return cb e, null
    r.on 'timeout', ->
      return cb 'error: DNS query timeout', null
    r.send()



question = (type, domain) ->
  dns.Question
    name: domain
    type: type

request = (question) ->
  dns.Request
    question: question
    server:
      address: '8.8.8.8'
      port: 53
      type: 'udp'
    timeout: 1000

String::f = ->
  args = arguments
  @replace /\{(\S+)\}/g, (m, n) ->
    args[n]

###
read = (test, cb) ->
  if (r = test.type) != 'TXT' and r != 'MX' and r != 'CNAME'
    cb 'error: bad type', null
  else
    start = Date.now()

    if (test.type == 'TXT') {
      test.value = '"{0}"'.f(test.value);
    }


    q = question(test.type, test.domain)
    r = request(q)
    r.on 'message', (err, dns) ->
      if err != null
        cb err, null
      if dns.answer.length == 0
        cb 'error: no record found', null
      else
        a = dns.answer[0]
        switch test.type
          when 'MX'
            cb null, '{0} {1}'.f(a.priority, a.exchange)
          when 'TXT'
            # cb(null, a.data[0]);
            cb null, '"{0}"'.f(a.data[0])
          when 'CNAME'
            cb null, a.data
      return
    r.on 'timeout', ->
      cb 'error: DNS query timeout', null
      return
    r.send()
  return



conn = 'postgres://keystone:keystone@localhost/keystone'
errors = false
htmlRows = ''

row_fmt = '
  <tr>
    <td><b>{0}</b></td>
    <td align="center">{1}</td>
    <td>{2}</td>
    <td>{3}</td>
    <td>{4}</td>
  </tr>'

table_fmt = '
  <table border="1" cellpadding="5" cellspacing="1" width="80%">
    <tr>
      <th>Test</th>
      <th>Type</th>
      <th>Domain</th>
      <th>Report</th>
      <th>Fixing</th>
    </tr>
    {0}
  </table>'


email = (args) ->
  transport = nodemailer.createTransport()
  transport.sendMail
    to: args.to
    from: args.from
    subject: args.subject
    html: args.body
  return

title_fmt = (size) ->
  switch size
    when '3'
      return '<tr style=\'background-color:black; color:white\'> <td colspan=\'5\'><h3>{0}</h3></td></tr>'
    when '4'
      return '<tr style=\'background-color:darkblue; color:white\'> <td colspan=\'5\'><h4>{0}</h4></td></tr>'
  return

add = (type, test) ->
  switch type
    when 'title'
      htmlRows += title_fmt('3').f(test.name)
    when 'client'
      htmlRows += title_fmt('4').f(test.name)
    when 'row'
      htmlRows += row_fmt.f(test.name, test.type, test.domain, test.report, test.fixing)
  return

color = (c, txt) ->
  '<font color={0}>{1}</font>'.f c, txt

digest = (test, cb) ->
  read test, (err, report) ->
    # console.log('report: {0}, test.value: {1}'.f(report, test.value));
    console.log 'aws r53', err, report
    if err != null
      test.report = err
      if ~err.indexOf('no record')
        digestNoRecord test, cb
      else if ~err.indexOf('timeout')
        digestTimeout test, cb
      else
        errors = true
        test.report = color('red', err)
        add 'row', test
        cb err
    else if report == test.value
      test.report = color('green', 'passed')
      test.fixing = ''
      add 'row', test
      cb null, report
    else
      digestOtherError test, cb
    return
  return

digestNoRecord = (test, cb) ->
  test.report = color('red', test.report)
  test.fixing = color('orange', 'adding&nbsp;record...')
  add 'row', test
  upsert test, (err, result) ->
    if err != null
      errors = true
      test.report = color('orange', err.msg)
      test.fixing = ''
      add 'row', test
      cb err
    else
      test.report = color('green', 'record&nbsp;added')
      test.fixing = ''
      add 'row', test
      cb null, result
    return
  return

digestTimeout = (test, cb) ->
  if timedout == false
    timedout = true
    errors = true
    test.report = color('red', test.report)
    test.fixing = color('orange', 'trying&nbsp;again...')
    add 'row', test
    digest test, cb
  else
    test.report = color('red', 'Still&nbsp;timing&nbsp;out')
    test.fixing = color('red', 'Not&nbsp;trying&nbsp;again')
    add 'row', test
    cb()
  return

digestOtherError = (test, cb) ->
  errors = true
  test.report = color('red', 'error: value mismatch')
  test.fixing = color('orange', 'updating&nbsp;record')
  add 'row', test
  upsert test, (err, result) ->
    if err != null
      errors = true
      test.report = color('orange', err.msg)
      test.fixing = ''
      cb err
    else
      test.report = color('green', 'record&nbsp;updated')
      test.fixing = ''
      add 'row', test
      cb null, result
    return
  return

read = (test, cb) ->
  if (r = test.type) != 'TXT' and r != 'MX' and r != 'CNAME'
    cb 'error: bad type', null
  else
    start = Date.now()

    if (test.type == 'TXT') {
      test.value = '"{0}"'.f(test.value);
    }


    q = question(test.type, test.domain)
    r = request(q)
    r.on 'message', (err, dns) ->
      if err != null
        cb err, null
      if dns.answer.length == 0
        cb 'error: no record found', null
      else
        a = dns.answer[0]
        switch test.type
          when 'MX'
            cb null, '{0} {1}'.f(a.priority, a.exchange)
          when 'TXT'
            # cb(null, a.data[0]);
            cb null, '"{0}"'.f(a.data[0])
          when 'CNAME'
            cb null, a.data
      return
    r.on 'timeout', ->
      cb 'error: DNS query timeout', null
      return
    r.send()
  return

upsert = (test, cb) ->
  r53 = new route53
  console.log 'setting record', test
  r53.setRecord {
    accessKeyId: 'AKIAJIJT6KMXVPF6RBZA'
    secretAccessKey: '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'
    zoneId: 'ZURCBJD3MM0T1'
    name: test.domain
    type: test.type
    ttl: 300
    values: [ test.value ]
  }, null, cb
  return

question = (type, domain) ->
  # console.log('type: {0} domain: {1}'.f(type, domain));
  dns.Question
    name: domain
    type: type

request = (question) ->
  dns.Request
    question: question
    server:
      address: '8.8.8.8'
      port: 53
      type: 'udp'
    timeout: 1000

# returns { public: 'keyvalue' private: 'keyvalue' }

getKeys = ->
  keypair()

globalTest = (cb) ->
  processGlobalTests ->
    processClientTests ->
      console.log 'done with global test.'
      htmlRows = table_fmt.f(htmlRows)
      args =
        to: 'nick.peeples@softvu.com'
        from: 'dns_tool@softvu.com'
        subject: 'Domain Report'
        body: htmlRows
      email args
      process.nextTick cb
      return
    return
  return

processGlobalTests = (cb) ->
  add 'title', name: 'Global DNS Test'
  async.each globalTests, digest, cb
  return

processClientTests = (cb) ->
  @._connect (err, client, done) ->
    if err != null
      console.error 'db connection failed', err
      return cb(err)
    client.query 'select doc from keystone.clients_current where (doc->>\'use_svemails\')::bool', (err, result) ->
      done()
      if err != null
        console.error 'db query failed', err
        return cb(err)
      async.eachSeries result.rows, clientRow, cb
      return
    return
  return

clientRow = (client, prefix, cb) ->
  add 'client', name: 'Client DNS Test'
  async.eachSeries getUnique(client, prefix), digest, cb
  return

getUnique = (client, prefix) ->
  prefix = prefix or result.rows[0].doc.svemails.prefix
  uniqueTests = _.cloneDeep(clientTests)
  while i < uniqueTests.length
    test = uniqueTests[i]
    if test.name == 'DKIM'
      key = _.find(client.dkim.keys, (key) ->
        key.domain == prefix + '.svemails.com'
      )
      publickey = key.publicKey.replace(/-[-\w\s]+-/g, '').replace(/\n/g, '')
      test.domain = test.domain.f(prefix, key.selector)
      test.value = test.value.f(publickey)
    else
      test.domain = test.domain.f(prefix)
    i++
  uniqueTests

clientTest = (id, prefix, cb) ->
  if !cb
    cb = prefix
    prefix = null
  @._connect (err, client, done) ->
    if err != null
      console.error 'db connection failed', err
      return cb(err)
    client.query 'select doc from keystone.clients_current where id = $1 limit 1', [ id ], (err, result) ->
      done()
      if err != null
        console.error 'db query failed', err
        return cb(err)
      if !result.rows.length
        return cb('client not found')
      # cb is called with err if err
      # otherwise an array of only things that changed, the tests that changed something
      clientRow result.rows[0].doc, prefix, cb
      return
    return
  return
  ###
