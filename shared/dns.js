(function() {
  var Dns, _, async, check_record, config, digest_record, dns, get_dns_record, get_job_id, get_record, get_records, get_zone_id, get_zone_records, keypair, moment, nodemailer, pg, question, request, route53, run_test, upsert, zero_id;

  config = require('config');

  _ = require('lodash');

  async = require('async');

  moment = require('moment');

  dns = require('native-dns');

  keypair = require('keypair');

  nodemailer = require('nodemailer');

  pg = require('pg');

  route53 = require('nice-route53');

  zero_id = '00000000-0000-0000-0000-000000000000';

  module.exports = Dns = (function() {
    function Dns() {}

    Dns.prototype._connect = function(cb) {
      return pg.connect(config.db.url, cb);
    };

    Dns.prototype.get_clients = function(cb) {
      return this._connect(function(err, client, done) {
        var q;
        if (err != null) {
          return cb(err);
        }
        q = "select id, doc->'name' as name from keystone.clients_current;";
        return client.query(q, function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          if (result.rows.length === 0) {
            return cb({
              message: 'unable to find any clients'
            });
          }
          return cb(null, result.rows);
        });
      });
    };

    Dns.prototype.get_records = function(cb) {
      return this._connect(function(err, client, done) {
        var q;
        if (err != null) {
          return cb(err);
        }
        q = "select * from dns.records where (isglobal = true or client_id = $1) and isdeleted is not true order by isglobal desc, id;";
        return client.query(q, [zero_id], function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          if (result.rows.length === 0) {
            return cb({
              message: 'unable to find any records'
            });
          }
          return cb(null, result.rows);
        });
      });
    };

    Dns.prototype.get_actionqueue = function(cb) {
      return this._connect(function(err, client, done) {
        var q;
        if (err != null) {
          return cb(err);
        }
        q = "select status, repeat from keystone.jobs where action = 'dns.coffee';";
        return client.query(q, function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          if (result.rows.length === 0) {
            return cb({
              message: 'no records matching "dns.coffee"'
            });
          }
          return cb(null, result.rows);
        });
      });
    };

    Dns.prototype.post_actionqueue = function(status, cron, cb) {
      return this._connect(function(err, client, done) {
        var q;
        if (err != null) {
          return cb(err);
        }
        q = "update keystone.jobs set status = $1, repeat = $2 where action = 'dns.coffee';";
        return client.query(q, [status, cron], function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          return cb(null);
        });
      });
    };

    Dns.prototype.get_settings = function(cb) {
      return this._connect(function(err, client, done) {
        var q;
        if (err != null) {
          return cb(err);
        }
        q = "select email_to, zone_id from dns.settings where id = 1;";
        return client.query(q, function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          if (result.rows.length === 0) {
            return cb('no dns_settings record for id 1');
          }
          return cb(null, result.rows);
        });
      });
    };

    Dns.prototype.post_settings = function(email, zone, cb) {
      return this._connect(function(err, client, done) {
        var q;
        if (err != null) {
          return cb(err);
        }
        q = "update dns.settings set email_to = $1, zone_id = $2 where id = 1;";
        return client.query(q, [email, zone], function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          return cb(null);
        });
      });
    };

    Dns.prototype.post_record = function(id, client_id, isglobal, name, type, domain, value, cb) {
      return this._connect(function(err, client, done) {
        var args, created, q;
        if (err != null) {
          return cb(err);
        }
        client_id = client_id === 'default' ? zero_id : client_id;
        created = moment().format();
        args = [client_id, isglobal, name, type, domain, value];
        if (id === '0') {
          q = "insert into dns.records (client_id, isglobal, name, type, domain, value, created) values ($1, $2, $3, $4, $5, $6, $7);";
          args.push(new Date());
        } else {
          q = "update dns.records set client_id = $1, isglobal = $2, name = $3, type = $4, domain = $5, value = $6 where id = $7;";
          args.push(id);
        }
        return client.query(q, args, function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          return cb(null);
        });
      });
    };

    Dns.prototype.delete_record = function(id, cb) {
      return this._connect(function(err, client, done) {
        var q;
        if (err != null) {
          return cb(err);
        }
        q = "update dns.records set isdeleted = true where id = $1;";
        return client.query(q, [id], function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          return cb(null);
        });
      });
    };

    Dns.prototype.verify_records = function(db_records, cb) {
      return get_zone_id(function(err, zone_id) {
        if (err != null) {
          return cb(err);
        }
        return get_zone_records(zone_id, function(err, zone_records) {
          if (err != null) {
            return cb(err);
          }
          return get_job_id(function(err, job_id) {
            var i;
            if (err != null) {
              return cb(err);
            }
            i = 0;
            while (i < db_records.length) {
              get_dns_record(db_records[i], function(err, dns_record) {
                var found_match, j;
                if (err != null) {
                  return cb(err);
                }
                if (dns_record.length === 0) {
                  return cb({
                    message: 'unable to find record by id'
                  });
                }
                if (err != null) {
                  return cb(err);
                }
                found_match = false;
                j = 0;
                while (j < zone_records.length) {
                  if (dns_record.type === zone_records[j].type) {
                    if (dns_record.domain === zone_records[j].values[0]) {
                      found_match = true;
                      run_test(dns_record, function(err, answer) {
                        return cb(null, answer);
                      });
                    }
                  }
                  j++;
                }
                if (found_match != null) {
                  return upsert(zone_id, dns_record, function(err, res) {
                    if (err != null) {
                      return cb(err);
                    }
                  });
                }
              });
              i++;
            }
            return cb(null, 'success');
          });
        });
      });
    };

    return Dns;

  })();

  upsert = function(zone_id, test, cb) {
    var args, e, r53, test_value;
    try {
      r53 = new route53(config.aws);
      console.log('setting record', test);
      test_value = test.type === 'TXT' ? '"' + test.value + '"' : test.value;
      args = {
        zoneId: zone_id,
        name: test.domain,
        type: test.type,
        ttl: 300,
        values: [test_value]
      };
      return r53.setRecord(args, function(err, res) {
        if (err != null) {
          return cb({
            message: err.msg
          });
        }
        console.log(res);
        return cb(null, res);
      });
    } catch (_error) {
      e = _error;
      return cb(e);
    }
  };

  get_job_id = function(cb) {
    return this._connect(function(err, client, done) {
      var q;
      if (err != null) {
        return cb(err);
      }
      q = "insert into dns.job (type) values ($1) returning id;";
      return client.query(q, ['test type'], function(err, result) {
        done();
        if (err != null) {
          return cb(err);
        }
        return cb(null, result.rows[0].id);
      });
    });
  };

  get_dns_record = function(record_id, cb) {
    return this._connect(function(err, client, done) {
      var q;
      if (err != null) {
        return cb(err);
      }
      q = "select * from dns.records where id = $1;";
      return client.query(q, [record_id], function(err, result) {
        done();
        if (err != null) {
          return cb(err);
        }
        return cb(null, result.rows[0]);
      });
    });
  };

  run_test = function(dns_record, cb) {
    var e, q, r, ref, start;
    if ((ref = dns_record.type) !== 'TXT' && ref !== 'MX' && ref !== 'CNAME') {
      return cb('error: bad type', null);
    } else {
      try {
        start = Date.now();
        if (dns_record.type === 'TXT') {
          dns_record = '"{0}"'.f(dns_record);
        }
        q = question(row.type, row.domain);
        r = request(q);
        r.on('message', function(err, dns) {
          var a, e;
          try {
            if (err != null) {
              return cb(err);
            }
            if (dns.answer.length === 0) {
              return cb('error: no record found', null);
            } else {
              a = dns.answer[0];
              switch (row.type) {
                case 'MX':
                  return cb(null, '{0} {1}'.f(a.priority, a.exchange));
                case 'TXT':
                  return cb(null, '"{0}"'.f(a.data[0]));
                case 'CNAME':
                  return cb(null, a.data);
                default:
                  return cb('error: bad type', null);
              }
            }
          } catch (_error) {
            e = _error;
            return cb(e);
          }
        });
      } catch (_error) {
        e = _error;
        return cb(e, null);
      }
      r.on('timeout', function() {
        return cb('error: DNS query timeout', null);
      });
      return r.send();
    }
  };

  get_zone_id = function(cb) {
    return this._connect(function(err, client, done) {
      var q;
      if (err != null) {
        return cb(err);
      }
      q = "select zone_id from dns.settings where id = 1;";
      return client.query(q, function(err, result) {
        done();
        if (err != null) {
          return cb(err);
        }
        return cb(null, result.rows[0].zone_id);
      });
    });
  };

  get_records = function(records, cb) {
    return this._connect(function(err, client, done) {
      var q;
      if (err != null) {
        return cb(err);
      }
      q = "select * from dns.records where id in (" + records + ");";
      return client.query(q, function(err, result) {
        if (err != null) {
          return cb(err);
        }
        return cb(null, result.rows);
      });
    });
  };

  get_zone_records = function(zone_id, cb) {
    var r53;
    r53 = new route53;
    return r53.records(zone_id, function(err, records) {
      if (err != null) {
        return cb(err);
      }
      return cb(null, records);
    });
  };

  ({
    verify_record_old: function(records, cb) {
      var e, i, results;
      try {
        console.log(records[0]);
        i = 0;
        results = [];
        while (i < records.length) {
          this._connect(function(err, client, done) {
            var q;
            if (err != null) {
              return cb(err);
            }
            q = "select * from dns.records where id = $1;";
            return client.query(q, [records[i]], function(err, result) {
              var e;
              try {
                done();
                if (err != null) {
                  return cb(err);
                }
                return digest_record(result.rows[0], function(err, ret) {
                  return cb(err, ret);
                });
              } catch (_error) {
                e = _error;
                return cb(e, null);
              }
            });
          });
          results.push(i++);
        }
        return results;
      } catch (_error) {
        e = _error;
        return cb(e, null);
      }
    },
    post_record: function(id, client_id, isglobal, name, type, domain, value, cb) {
      return this._connect(function(err, client, done) {
        var args, created, q;
        if (err != null) {
          return cb(err);
        }
        client_id = client_id === 'default' ? zero_id : client_id;
        created = moment().format();
        args = [client_id, isglobal, name, type, domain, value];
        if (id === '0') {
          q = "insert into dns.records (client_id, isglobal, name, type, domain, value, created) values ($1, $2, $3, $4, $5, $6, $7);";
          args.push(new Date());
        } else {
          q = "update dns.records set client_id = $1, isglobal = $2, name = $3, type = $4, domain = $5, value = $6 where id = $7;";
          args.push(id);
        }
        return client.query(q, args, function(err, result) {
          done();
          if (err != null) {
            return cb(err);
          }
          return cb(null);
        });
      });
    }
  });

  get_record = function(record_id, cb) {
    return this._connect(function(err, client, done) {
      var q;
      if (err != null) {
        return cb(err);
      }
      q = "select * from dns.records where id = $1;";
      return client.query(q, [record_id], function(err, result) {
        done();
        if (err != null) {
          return cb(err);
        }
        return result;
      });
    });
  };

  check_record = function(name, cb) {
    return cb(null, name);
  };

  digest_record = function(row, cb) {
    var e, q, r, ref, start;
    if ((ref = row.type) !== 'TXT' && ref !== 'MX' && ref !== 'CNAME') {
      return cb('error: bad type', null);
    } else {
      try {
        start = Date.now();
        if (row.type === 'TXT') {
          row.value = '"{0}"'.f(row.value);
        }
        q = question(row.type, row.domain);
        r = request(q);
        r.on('message', function(err, dns) {
          var a, e;
          if (err != null) {
            return cb(err);
          }
          try {
            if (dns.answer.length === 0) {
              return cb('error: no record found', null);
            } else {
              a = dns.answer[0];
              switch (row.type) {
                case 'MX':
                  return cb(null, '{0} {1}'.f(a.priority, a.exchange));
                case 'TXT':
                  return cb(null, '"{0}"'.f(a.data[0]));
                case 'CNAME':
                  return cb(null, a.data);
                default:
                  return cb('error: bad type', null);
              }
            }
          } catch (_error) {
            e = _error;
            return cb(e, null);
          }
        });
      } catch (_error) {
        e = _error;
        return cb(e, null);
      }
      r.on('timeout', function() {
        return cb('error: DNS query timeout', null);
      });
      return r.send();
    }
  };

  question = function(type, domain) {
    return dns.Question({
      name: domain,
      type: type
    });
  };

  request = function(question) {
    return dns.Request({
      question: question,
      server: {
        address: '8.8.8.8',
        port: 53,
        type: 'udp'
      },
      timeout: 1000
    });
  };

  String.prototype.f = function() {
    var args;
    args = arguments;
    return this.replace(/\{(\S+)\}/g, function(m, n) {
      return args[n];
    });
  };


  /*
  read = (test, cb) ->
    if (r = test.type) != 'TXT' and r != 'MX' and r != 'CNAME'
      cb 'error: bad type', null
    else
      start = Date.now()
  
      if (test.type == 'TXT') {
        test.value = '"{0}"'.f(test.value);
      }
  
  
      q = question(test.type, test.domain)
      r = request(q)
      r.on 'message', (err, dns) ->
        if err != null
          cb err, null
        if dns.answer.length == 0
          cb 'error: no record found', null
        else
          a = dns.answer[0]
          switch test.type
            when 'MX'
              cb null, '{0} {1}'.f(a.priority, a.exchange)
            when 'TXT'
               * cb(null, a.data[0]);
              cb null, '"{0}"'.f(a.data[0])
            when 'CNAME'
              cb null, a.data
        return
      r.on 'timeout', ->
        cb 'error: DNS query timeout', null
        return
      r.send()
    return
  
  
  
  conn = 'postgres://keystone:keystone@localhost/keystone'
  errors = false
  htmlRows = ''
  
  row_fmt = '
    <tr>
      <td><b>{0}</b></td>
      <td align="center">{1}</td>
      <td>{2}</td>
      <td>{3}</td>
      <td>{4}</td>
    </tr>'
  
  table_fmt = '
    <table border="1" cellpadding="5" cellspacing="1" width="80%">
      <tr>
        <th>Test</th>
        <th>Type</th>
        <th>Domain</th>
        <th>Report</th>
        <th>Fixing</th>
      </tr>
      {0}
    </table>'
  
  
  email = (args) ->
    transport = nodemailer.createTransport()
    transport.sendMail
      to: args.to
      from: args.from
      subject: args.subject
      html: args.body
    return
  
  title_fmt = (size) ->
    switch size
      when '3'
        return '<tr style=\'background-color:black; color:white\'> <td colspan=\'5\'><h3>{0}</h3></td></tr>'
      when '4'
        return '<tr style=\'background-color:darkblue; color:white\'> <td colspan=\'5\'><h4>{0}</h4></td></tr>'
    return
  
  add = (type, test) ->
    switch type
      when 'title'
        htmlRows += title_fmt('3').f(test.name)
      when 'client'
        htmlRows += title_fmt('4').f(test.name)
      when 'row'
        htmlRows += row_fmt.f(test.name, test.type, test.domain, test.report, test.fixing)
    return
  
  color = (c, txt) ->
    '<font color={0}>{1}</font>'.f c, txt
  
  digest = (test, cb) ->
    read test, (err, report) ->
       * console.log('report: {0}, test.value: {1}'.f(report, test.value));
      console.log 'aws r53', err, report
      if err != null
        test.report = err
        if ~err.indexOf('no record')
          digestNoRecord test, cb
        else if ~err.indexOf('timeout')
          digestTimeout test, cb
        else
          errors = true
          test.report = color('red', err)
          add 'row', test
          cb err
      else if report == test.value
        test.report = color('green', 'passed')
        test.fixing = ''
        add 'row', test
        cb null, report
      else
        digestOtherError test, cb
      return
    return
  
  digestNoRecord = (test, cb) ->
    test.report = color('red', test.report)
    test.fixing = color('orange', 'adding&nbsp;record...')
    add 'row', test
    upsert test, (err, result) ->
      if err != null
        errors = true
        test.report = color('orange', err.msg)
        test.fixing = ''
        add 'row', test
        cb err
      else
        test.report = color('green', 'record&nbsp;added')
        test.fixing = ''
        add 'row', test
        cb null, result
      return
    return
  
  digestTimeout = (test, cb) ->
    if timedout == false
      timedout = true
      errors = true
      test.report = color('red', test.report)
      test.fixing = color('orange', 'trying&nbsp;again...')
      add 'row', test
      digest test, cb
    else
      test.report = color('red', 'Still&nbsp;timing&nbsp;out')
      test.fixing = color('red', 'Not&nbsp;trying&nbsp;again')
      add 'row', test
      cb()
    return
  
  digestOtherError = (test, cb) ->
    errors = true
    test.report = color('red', 'error: value mismatch')
    test.fixing = color('orange', 'updating&nbsp;record')
    add 'row', test
    upsert test, (err, result) ->
      if err != null
        errors = true
        test.report = color('orange', err.msg)
        test.fixing = ''
        cb err
      else
        test.report = color('green', 'record&nbsp;updated')
        test.fixing = ''
        add 'row', test
        cb null, result
      return
    return
  
  read = (test, cb) ->
    if (r = test.type) != 'TXT' and r != 'MX' and r != 'CNAME'
      cb 'error: bad type', null
    else
      start = Date.now()
  
      if (test.type == 'TXT') {
        test.value = '"{0}"'.f(test.value);
      }
  
  
      q = question(test.type, test.domain)
      r = request(q)
      r.on 'message', (err, dns) ->
        if err != null
          cb err, null
        if dns.answer.length == 0
          cb 'error: no record found', null
        else
          a = dns.answer[0]
          switch test.type
            when 'MX'
              cb null, '{0} {1}'.f(a.priority, a.exchange)
            when 'TXT'
               * cb(null, a.data[0]);
              cb null, '"{0}"'.f(a.data[0])
            when 'CNAME'
              cb null, a.data
        return
      r.on 'timeout', ->
        cb 'error: DNS query timeout', null
        return
      r.send()
    return
  
  upsert = (test, cb) ->
    r53 = new route53
    console.log 'setting record', test
    r53.setRecord {
      accessKeyId: 'AKIAJIJT6KMXVPF6RBZA'
      secretAccessKey: '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'
      zoneId: 'ZURCBJD3MM0T1'
      name: test.domain
      type: test.type
      ttl: 300
      values: [ test.value ]
    }, null, cb
    return
  
  question = (type, domain) ->
     * console.log('type: {0} domain: {1}'.f(type, domain));
    dns.Question
      name: domain
      type: type
  
  request = (question) ->
    dns.Request
      question: question
      server:
        address: '8.8.8.8'
        port: 53
        type: 'udp'
      timeout: 1000
  
   * returns { public: 'keyvalue' private: 'keyvalue' }
  
  getKeys = ->
    keypair()
  
  globalTest = (cb) ->
    processGlobalTests ->
      processClientTests ->
        console.log 'done with global test.'
        htmlRows = table_fmt.f(htmlRows)
        args =
          to: 'nick.peeples@softvu.com'
          from: 'dns_tool@softvu.com'
          subject: 'Domain Report'
          body: htmlRows
        email args
        process.nextTick cb
        return
      return
    return
  
  processGlobalTests = (cb) ->
    add 'title', name: 'Global DNS Test'
    async.each globalTests, digest, cb
    return
  
  processClientTests = (cb) ->
    @._connect (err, client, done) ->
      if err != null
        console.error 'db connection failed', err
        return cb(err)
      client.query 'select doc from keystone.clients_current where (doc->>\'use_svemails\')::bool', (err, result) ->
        done()
        if err != null
          console.error 'db query failed', err
          return cb(err)
        async.eachSeries result.rows, clientRow, cb
        return
      return
    return
  
  clientRow = (client, prefix, cb) ->
    add 'client', name: 'Client DNS Test'
    async.eachSeries getUnique(client, prefix), digest, cb
    return
  
  getUnique = (client, prefix) ->
    prefix = prefix or result.rows[0].doc.svemails.prefix
    uniqueTests = _.cloneDeep(clientTests)
    while i < uniqueTests.length
      test = uniqueTests[i]
      if test.name == 'DKIM'
        key = _.find(client.dkim.keys, (key) ->
          key.domain == prefix + '.svemails.com'
        )
        publickey = key.publicKey.replace(/-[-\w\s]+-/g, '').replace(/\n/g, '')
        test.domain = test.domain.f(prefix, key.selector)
        test.value = test.value.f(publickey)
      else
        test.domain = test.domain.f(prefix)
      i++
    uniqueTests
  
  clientTest = (id, prefix, cb) ->
    if !cb
      cb = prefix
      prefix = null
    @._connect (err, client, done) ->
      if err != null
        console.error 'db connection failed', err
        return cb(err)
      client.query 'select doc from keystone.clients_current where id = $1 limit 1', [ id ], (err, result) ->
        done()
        if err != null
          console.error 'db query failed', err
          return cb(err)
        if !result.rows.length
          return cb('client not found')
         * cb is called with err if err
         * otherwise an array of only things that changed, the tests that changed something
        clientRow result.rows[0].doc, prefix, cb
        return
      return
    return
   */

}).call(this);
