email = (args) ->
  transport = nodemailer.createTransport()
  transport.sendMail
    to: args.to
    from: args.from
    subject: args.subject
    html: args.body
  return

title_fmt = (size) ->
  switch size
    when '3'
      return '<tr style=\'background-color:black; color:white\'> <td colspan=\'5\'><h3>{0}</h3></td></tr>'
    when '4'
      return '<tr style=\'background-color:darkblue; color:white\'> <td colspan=\'5\'><h4>{0}</h4></td></tr>'
  return

add = (type, test) ->
  switch type
    when 'title'
      htmlRows += title_fmt('3').f(test.name)
    when 'client'
      htmlRows += title_fmt('4').f(test.name)
    when 'row'
      htmlRows += row_fmt.f(test.name, test.type, test.domain, test.report, test.fixing)
  return

color = (c, txt) ->
  '<font color={0}>{1}</font>'.f c, txt

digest = (test, cb) ->
  read test, (err, report) ->
    # console.log('report: {0}, test.value: {1}'.f(report, test.value));
    console.log 'aws r53', err, report
    if err != null
      test.report = err
      if ~err.indexOf('no record')
        digestNoRecord test, cb
      else if ~err.indexOf('timeout')
        digestTimeout test, cb
      else
        errors = true
        test.report = color('red', err)
        add 'row', test
        cb err
    else if report == test.value
      test.report = color('green', 'passed')
      test.fixing = ''
      add 'row', test
      cb null, report
    else
      digestOtherError test, cb
    return
  return

digestNoRecord = (test, cb) ->
  test.report = color('red', test.report)
  test.fixing = color('orange', 'adding&nbsp;record...')
  add 'row', test
  upsert test, (err, result) ->
    if err != null
      errors = true
      test.report = color('orange', err.msg)
      test.fixing = ''
      add 'row', test
      cb err
    else
      test.report = color('green', 'record&nbsp;added')
      test.fixing = ''
      add 'row', test
      cb null, result
    return
  return

digestTimeout = (test, cb) ->
  if timedout == false
    timedout = true
    errors = true
    test.report = color('red', test.report)
    test.fixing = color('orange', 'trying&nbsp;again...')
    add 'row', test
    digest test, cb
  else
    test.report = color('red', 'Still&nbsp;timing&nbsp;out')
    test.fixing = color('red', 'Not&nbsp;trying&nbsp;again')
    add 'row', test
    cb()
  return

digestOtherError = (test, cb) ->
  errors = true
  test.report = color('red', 'error: value mismatch')
  test.fixing = color('orange', 'updating&nbsp;record')
  add 'row', test
  upsert test, (err, result) ->
    if err != null
      errors = true
      test.report = color('orange', err.msg)
      test.fixing = ''
      cb err
    else
      test.report = color('green', 'record&nbsp;updated')
      test.fixing = ''
      add 'row', test
      cb null, result
    return
  return

read = (test, cb) ->
  if (r = test.type) != 'TXT' and r != 'MX' and r != 'CNAME'
    cb 'error: bad type', null
  else
    start = Date.now()

    ###if (test.type == 'TXT') {
      test.value = '"{0}"'.f(test.value);
    }
    ###

    q = question(test.type, test.domain)
    r = request(q)
    r.on 'message', (err, dns) ->
      if err != null
        cb err, null
      if dns.answer.length == 0
        cb 'error: no record found', null
      else
        a = dns.answer[0]
        switch test.type
          when 'MX'
            cb null, '{0} {1}'.f(a.priority, a.exchange)
          when 'TXT'
            # cb(null, a.data[0]);
            cb null, '"{0}"'.f(a.data[0])
          when 'CNAME'
            cb null, a.data
      return
    r.on 'timeout', ->
      cb 'error: DNS query timeout', null
      return
    r.send()
  return

upsert = (test, cb) ->
  r53 = new route53
  console.log 'setting record', test
  r53.setRecord {
    accessKeyId: 'AKIAJIJT6KMXVPF6RBZA'
    secretAccessKey: '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'
    zoneId: 'ZURCBJD3MM0T1'
    name: test.domain
    type: test.type
    ttl: 300
    values: [ test.value ]
  }, null, cb
  return

question = (type, domain) ->
  # console.log('type: {0} domain: {1}'.f(type, domain));
  dns.Question
    name: domain
    type: type

request = (question) ->
  dns.Request
    question: question
    server:
      address: '8.8.8.8'
      port: 53
      type: 'udp'
    timeout: 1000

# returns { public: 'keyvalue' private: 'keyvalue' }

getKeys = ->
  keypair()

globalTest = (cb) ->
  processGlobalTests ->
    processClientTests ->
      console.log 'done with global test.'
      htmlRows = table_fmt.f(htmlRows)
      args =
        to: 'nick.peeples@softvu.com'
        from: 'dns_tool@softvu.com'
        subject: 'Domain Report'
        body: htmlRows
      email args
      process.nextTick cb
      return
    return
  return

processGlobalTests = (cb) ->
  add 'title', name: 'Global DNS Test'
  async.each globalTests, digest, cb
  return

processClientTests = (cb) ->
  pg.connect conn, (err, client, done) ->
    if err != null
      console.error 'db connection failed', err
      return cb(err)
    client.query 'select doc from keystone.clients_current where (doc->>\'use_svemails\')::bool', (err, result) ->
      done()
      if err != null
        console.error 'db query failed', err
        return cb(err)
      async.eachSeries result.rows, clientRow, cb
      return
    return
  return

clientRow = (client, prefix, cb) ->
  add 'client', name: 'Client DNS Test'
  async.eachSeries getUnique(client, prefix), digest, cb
  return

getUnique = (client, prefix) ->
  prefix = prefix or result.rows[0].doc.svemails.prefix
  uniqueTests = _.cloneDeep(clientTests)
  while i < uniqueTests.length
    test = uniqueTests[i]
    if test.name == 'DKIM'
      key = _.find(client.dkim.keys, (key) ->
        key.domain == prefix + '.svemails.com'
      )
      publickey = key.publicKey.replace(/-[-\w\s]+-/g, '').replace(/\n/g, '')
      test.domain = test.domain.f(prefix, key.selector)
      test.value = test.value.f(publickey)
    else
      test.domain = test.domain.f(prefix)
    i++
  uniqueTests

clientTest = (id, prefix, cb) ->
  if !cb
    cb = prefix
    prefix = null
  pg.connect conn, (err, client, done) ->
    if err != null
      console.error 'db connection failed', err
      return cb(err)
    client.query 'select doc from keystone.clients_current where id = $1 limit 1', [ id ], (err, result) ->
      done()
      if err != null
        console.error 'db query failed', err
        return cb(err)
      if !result.rows.length
        return cb('client not found')
      # cb is called with err if err
      # otherwise an array of only things that changed, the tests that changed something
      clientRow result.rows[0].doc, prefix, cb
      return
    return
  return

_ = require('lodash')
async = require('async')
dns = require('native-dns')
keypair = require('keypair')
# use nodeforge
nodemailer = require('nodemailer')
pg = require('pg')
route53 = require('nice-route53')
module.exports.DnsTool = do ->

  DnsTool = ->
    # DnsTool.prototype.constructor = function () { /* nada */ }
    return

  DnsTool::getKeys = getKeys
  DnsTool::globalTest = globalTest
  DnsTool::clientTest = clientTest
  DnsTool::email = email
  DnsTool
conn = 'postgres://keystone:keystone@localhost/keystone'
errors = false
htmlRows = ''
row_fmt = '' + '<tr>' + '<td><b>{0}</b></td>' + '<td align="center">{1}</td>' + '<td>{2}</td>' + '<td>{3}</td>' + '<td>{4}</td>' + '</tr>'
table_fmt = '' + '<table border="1" cellpadding="5" cellspacing="1" width="80%">' + '<tr>' + '<th>Test</th>' + '<th>Type</th>' + '<th>Domain</th>' + '<th>Report</th>' + '<th>Fixing</th>' + '</tr>' + '{0}' + '</table>'
