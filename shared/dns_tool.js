(function() {
  var _, add, async, clientRow, clientTest, color, conn, digest, digestNoRecord, digestOtherError, digestTimeout, dns, email, errors, getKeys, getUnique, globalTest, htmlRows, keypair, nodemailer, pg, processClientTests, processGlobalTests, question, read, request, route53, row_fmt, table_fmt, title_fmt, upsert;

  email = function(args) {
    var transport;
    transport = nodemailer.createTransport();
    transport.sendMail({
      to: args.to,
      from: args.from,
      subject: args.subject,
      html: args.body
    });
  };

  title_fmt = function(size) {
    switch (size) {
      case '3':
        return '<tr style=\'background-color:black; color:white\'> <td colspan=\'5\'><h3>{0}</h3></td></tr>';
      case '4':
        return '<tr style=\'background-color:darkblue; color:white\'> <td colspan=\'5\'><h4>{0}</h4></td></tr>';
    }
  };

  add = function(type, test) {
    switch (type) {
      case 'title':
        htmlRows += title_fmt('3').f(test.name);
        break;
      case 'client':
        htmlRows += title_fmt('4').f(test.name);
        break;
      case 'row':
        htmlRows += row_fmt.f(test.name, test.type, test.domain, test.report, test.fixing);
    }
  };

  color = function(c, txt) {
    return '<font color={0}>{1}</font>'.f(c, txt);
  };

  digest = function(test, cb) {
    read(test, function(err, report) {
      var errors;
      console.log('aws r53', err, report);
      if (err !== null) {
        test.report = err;
        if (~err.indexOf('no record')) {
          digestNoRecord(test, cb);
        } else if (~err.indexOf('timeout')) {
          digestTimeout(test, cb);
        } else {
          errors = true;
          test.report = color('red', err);
          add('row', test);
          cb(err);
        }
      } else if (report === test.value) {
        test.report = color('green', 'passed');
        test.fixing = '';
        add('row', test);
        cb(null, report);
      } else {
        digestOtherError(test, cb);
      }
    });
  };

  digestNoRecord = function(test, cb) {
    test.report = color('red', test.report);
    test.fixing = color('orange', 'adding&nbsp;record...');
    add('row', test);
    upsert(test, function(err, result) {
      var errors;
      if (err !== null) {
        errors = true;
        test.report = color('orange', err.msg);
        test.fixing = '';
        add('row', test);
        cb(err);
      } else {
        test.report = color('green', 'record&nbsp;added');
        test.fixing = '';
        add('row', test);
        cb(null, result);
      }
    });
  };

  digestTimeout = function(test, cb) {
    var errors, timedout;
    if (timedout === false) {
      timedout = true;
      errors = true;
      test.report = color('red', test.report);
      test.fixing = color('orange', 'trying&nbsp;again...');
      add('row', test);
      digest(test, cb);
    } else {
      test.report = color('red', 'Still&nbsp;timing&nbsp;out');
      test.fixing = color('red', 'Not&nbsp;trying&nbsp;again');
      add('row', test);
      cb();
    }
  };

  digestOtherError = function(test, cb) {
    var errors;
    errors = true;
    test.report = color('red', 'error: value mismatch');
    test.fixing = color('orange', 'updating&nbsp;record');
    add('row', test);
    upsert(test, function(err, result) {
      if (err !== null) {
        errors = true;
        test.report = color('orange', err.msg);
        test.fixing = '';
        cb(err);
      } else {
        test.report = color('green', 'record&nbsp;updated');
        test.fixing = '';
        add('row', test);
        cb(null, result);
      }
    });
  };

  read = function(test, cb) {
    var q, r, start;
    if ((r = test.type) !== 'TXT' && r !== 'MX' && r !== 'CNAME') {
      cb('error: bad type', null);
    } else {
      start = Date.now();

      /*if (test.type == 'TXT') {
        test.value = '"{0}"'.f(test.value);
      }
       */
      q = question(test.type, test.domain);
      r = request(q);
      r.on('message', function(err, dns) {
        var a;
        if (err !== null) {
          cb(err, null);
        }
        if (dns.answer.length === 0) {
          cb('error: no record found', null);
        } else {
          a = dns.answer[0];
          switch (test.type) {
            case 'MX':
              cb(null, '{0} {1}'.f(a.priority, a.exchange));
              break;
            case 'TXT':
              cb(null, '"{0}"'.f(a.data[0]));
              break;
            case 'CNAME':
              cb(null, a.data);
          }
        }
      });
      r.on('timeout', function() {
        cb('error: DNS query timeout', null);
      });
      r.send();
    }
  };

  upsert = function(test, cb) {
    var r53;
    r53 = new route53;
    console.log('setting record', test);
    r53.setRecord({
      accessKeyId: 'AKIAJIJT6KMXVPF6RBZA',
      secretAccessKey: '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',
      zoneId: 'ZURCBJD3MM0T1',
      name: test.domain,
      type: test.type,
      ttl: 300,
      values: [test.value]
    }, null, cb);
  };

  question = function(type, domain) {
    return dns.Question({
      name: domain,
      type: type
    });
  };

  request = function(question) {
    return dns.Request({
      question: question,
      server: {
        address: '8.8.8.8',
        port: 53,
        type: 'udp'
      },
      timeout: 1000
    });
  };

  getKeys = function() {
    return keypair();
  };

  globalTest = function(cb) {
    processGlobalTests(function() {
      processClientTests(function() {
        var args, htmlRows;
        console.log('done with global test.');
        htmlRows = table_fmt.f(htmlRows);
        args = {
          to: 'nick.peeples@softvu.com',
          from: 'dns_tool@softvu.com',
          subject: 'Domain Report',
          body: htmlRows
        };
        email(args);
        process.nextTick(cb);
      });
    });
  };

  processGlobalTests = function(cb) {
    add('title', {
      name: 'Global DNS Test'
    });
    async.each(globalTests, digest, cb);
  };

  processClientTests = function(cb) {
    pg.connect(conn, function(err, client, done) {
      if (err !== null) {
        console.error('db connection failed', err);
        return cb(err);
      }
      client.query('select doc from keystone.clients_current where (doc->>\'use_svemails\')::bool', function(err, result) {
        done();
        if (err !== null) {
          console.error('db query failed', err);
          return cb(err);
        }
        async.eachSeries(result.rows, clientRow, cb);
      });
    });
  };

  clientRow = function(client, prefix, cb) {
    add('client', {
      name: 'Client DNS Test'
    });
    async.eachSeries(getUnique(client, prefix), digest, cb);
  };

  getUnique = function(client, prefix) {
    var key, publickey, test, uniqueTests;
    prefix = prefix || result.rows[0].doc.svemails.prefix;
    uniqueTests = _.cloneDeep(clientTests);
    while (i < uniqueTests.length) {
      test = uniqueTests[i];
      if (test.name === 'DKIM') {
        key = _.find(client.dkim.keys, function(key) {
          return key.domain === prefix + '.svemails.com';
        });
        publickey = key.publicKey.replace(/-[-\w\s]+-/g, '').replace(/\n/g, '');
        test.domain = test.domain.f(prefix, key.selector);
        test.value = test.value.f(publickey);
      } else {
        test.domain = test.domain.f(prefix);
      }
      i++;
    }
    return uniqueTests;
  };

  clientTest = function(id, prefix, cb) {
    if (!cb) {
      cb = prefix;
      prefix = null;
    }
    pg.connect(conn, function(err, client, done) {
      if (err !== null) {
        console.error('db connection failed', err);
        return cb(err);
      }
      client.query('select doc from keystone.clients_current where id = $1 limit 1', [id], function(err, result) {
        done();
        if (err !== null) {
          console.error('db query failed', err);
          return cb(err);
        }
        if (!result.rows.length) {
          return cb('client not found');
        }
        clientRow(result.rows[0].doc, prefix, cb);
      });
    });
  };

  _ = require('lodash');

  async = require('async');

  dns = require('native-dns');

  keypair = require('keypair');

  nodemailer = require('nodemailer');

  pg = require('pg');

  route53 = require('nice-route53');

  module.exports.DnsTool = (function() {
    var DnsTool;
    DnsTool = function() {};
    DnsTool.prototype.getKeys = getKeys;
    DnsTool.prototype.globalTest = globalTest;
    DnsTool.prototype.clientTest = clientTest;
    DnsTool.prototype.email = email;
    return DnsTool;
  })();

  conn = 'postgres://keystone:keystone@localhost/keystone';

  errors = false;

  htmlRows = '';

  row_fmt = '' + '<tr>' + '<td><b>{0}</b></td>' + '<td align="center">{1}</td>' + '<td>{2}</td>' + '<td>{3}</td>' + '<td>{4}</td>' + '</tr>';

  table_fmt = '' + '<table border="1" cellpadding="5" cellspacing="1" width="80%">' + '<tr>' + '<th>Test</th>' + '<th>Type</th>' + '<th>Domain</th>' + '<th>Report</th>' + '<th>Fixing</th>' + '</tr>' + '{0}' + '</table>';

}).call(this);
