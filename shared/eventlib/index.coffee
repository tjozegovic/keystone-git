config = require 'config'
{EventEmitter} = require 'events'

_ = require 'lodash'
amqp = require 'amqplib'

module.exports.Eventlib = class Eventlib
  states =
    DISCONNECTED: 0
    CONNECTING: 1
    CONNECTED: 2

  constructor: (@url) ->
    @state = states.DISCONNECTED
    @events = new EventEmitter
    @events.setMaxListeners Infinity

  _connect: ->
    return unless @state is states.DISCONNECTED
    return if @state is states.CONNECTING
    @state = states.CONNECTING

    amqp.connect(@url).then (@conn) =>
      @conn.createConfirmChannel().then (@channel) =>
        opts =
          type: 'topic'
          durable: true
          autoDelete: false
          confirm: true

        @channel.assertExchange('events.hose', 'topic', opts).then =>
          @state = states.CONNECTED
          @events.emit 'connected'

  emit: (type, client, data, cb) ->
    event = _.defaults
      at: new Date
      type: type
      client: client if client
    , data

    send = =>
      buf = new Buffer JSON.stringify event
      @channel.publish 'events.hose', "#{client}.#{type}.event", buf, null, cb

    @_connect()
    unless @state is states.CONNECTED
      @events.once 'connected', send
    else
      send()

  heartbeat: (type, getter) ->
    clearInterval @_timer if @_timer?

    @_timer = setInterval =>
      @emit type, null, _.assign at: new Date, getter?()
    , 5000
    .unref()

module.exports = new Eventlib config.queues.url
