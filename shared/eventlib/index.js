(function() {
  var EventEmitter, Eventlib, _, amqp, config;

  config = require('config');

  EventEmitter = require('events').EventEmitter;

  _ = require('lodash');

  amqp = require('amqplib');

  module.exports.Eventlib = Eventlib = (function() {
    var states;

    states = {
      DISCONNECTED: 0,
      CONNECTING: 1,
      CONNECTED: 2
    };

    function Eventlib(url) {
      this.url = url;
      this.state = states.DISCONNECTED;
      this.events = new EventEmitter;
      this.events.setMaxListeners(Infinity);
    }

    Eventlib.prototype._connect = function() {
      if (this.state !== states.DISCONNECTED) {
        return;
      }
      if (this.state === states.CONNECTING) {
        return;
      }
      this.state = states.CONNECTING;
      return amqp.connect(this.url).then((function(_this) {
        return function(conn) {
          _this.conn = conn;
          return _this.conn.createConfirmChannel().then(function(channel) {
            var opts;
            _this.channel = channel;
            opts = {
              type: 'topic',
              durable: true,
              autoDelete: false,
              confirm: true
            };
            return _this.channel.assertExchange('events.hose', 'topic', opts).then(function() {
              _this.state = states.CONNECTED;
              return _this.events.emit('connected');
            });
          });
        };
      })(this));
    };

    Eventlib.prototype.emit = function(type, client, data, cb) {
      var event, send;
      event = _.defaults({
        at: new Date,
        type: type,
        client: client ? client : void 0
      }, data);
      send = (function(_this) {
        return function() {
          var buf;
          buf = new Buffer(JSON.stringify(event));
          return _this.channel.publish('events.hose', client + "." + type + ".event", buf, null, cb);
        };
      })(this);
      this._connect();
      if (this.state !== states.CONNECTED) {
        return this.events.once('connected', send);
      } else {
        return send();
      }
    };

    Eventlib.prototype.heartbeat = function(type, getter) {
      if (this._timer != null) {
        clearInterval(this._timer);
      }
      return this._timer = setInterval((function(_this) {
        return function() {
          return _this.emit(type, null, _.assign({
            at: new Date
          }, typeof getter === "function" ? getter() : void 0));
        };
      })(this), 5000).unref();
    };

    return Eventlib;

  })();

  module.exports = new Eventlib(config.queues.url);

}).call(this);
