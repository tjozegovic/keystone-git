_ = require 'lodash'
express = require 'express'
request = require 'request'
{Parser} = require 'xml2js'
{RepositoryFactory} = require 'shared/database'

module.exports = app = express()

app.use (req, res, next) ->
  {key} = req.query
  return res.status(500).send 'Missing API key' unless key?

  [keys, clients] = req.db.use 'core/apikeys', 'core/clients'
  keys.get key, (err, body) ->
    return res.status(500).send 'Invalid API key' if err or not body

    clients.get body.client, (err, body) ->
      req._client = body
      next()

app.use (req, res, next) ->
  return next() unless req.method is 'POST'

  # idk if this is a bug or something, but text/xml doesn't satisfy req.is('xml')
  return next() unless req.is('xml') or req.is('text/xml')

  data = ''
  req.setEncoding 'utf8'
  req.on 'data', (chunk) ->
    data += chunk

  req.on 'end', ->
    parser = new Parser explicitArray: no
    parser.parseString data, (err, res) ->
      req.rawxml = data
      req.body = res
      next err

app.use (req, res, next) ->
  return next() unless req.method is 'POST'

  logs = req.db.use 'core/logs'
  logs.insert
    source: 'mortech'
    at: new Date()
    raw: req.rawxml
    request: _.pick req, ['headers', 'query', 'originalUrl', 'ip']
    data: req.body.mortech
  , (err, body) ->
    req.logid = body.id
    res.locals.mortech =
      getprospect: (id, cb) ->
        mortech_request =
          method: 'GET'
          url: 'https://thirdparty.mortech-inc.com/thirdpartyintegration/getprospect'
          body: """<?xml version="1.0" encoding="utf-8"?>
<mortech>
  <Auth>
    <thirdPartyName>softvu</thirdPartyName>
    <clientId>#{req._client.integrations.marksman.clientId}</clientId>
    <licenseKey>RE$3SdT!Q6</licenseKey>
  </Auth>
  <prospectId>#{id}</prospectId>
  <dataSegments>
    <dataSegment>1</dataSegment>
    <dataSegment>3</dataSegment>
    <dataSegment>4</dataSegment>
  </dataSegments>
</mortech>"""
        log = (extra, done) ->
          logs.insert _.assign
            target: 'mortech'
            parent: req.logid
            at: new Date()
            req: mortech_request
          , extra
          process.nextTick done

        request mortech_request, (rerr, qres, body) ->
          if rerr
            return log
              request_err: rerr
              body: body
            , -> cb rerr

          parser = new Parser explicitArray: no
          parser.parseString body, (xerr, result) ->
            if xerr
              log body: body, xml_err: xerr, -> cb xerr, null
            else
              {response} = result.mortech
              log body: body, ->
                if response.errorCode isnt '0'
                  cb response.errorDesc, null
                else
                  cb null, result.mortech

    next err

app.get '/event', (req, res) ->
  res.send 'ok'

app.post '/event', (req, res) ->
  prospectid = req.body.result.row.ProspectId
  res.locals.mortech.getprospect prospectid, (err, mortech) ->
    # TODO unauthenticated api calls should be logged and available to the CMs

    # if there are multiple borrowers, this will ben an array and we want the borrow position, not co-borrower
    borrower = _.first _.where mortech.MISMOClosing.LOAN._APPLICATION.BORROWER, $: _PrintPositionType: 'Borrower'
    borrower ?= mortech.MISMOClosing.LOAN._APPLICATION.BORROWER

    json =
      status: mortech.statusName
      attributes:
        leadsource: mortech.sourceName
        product: mortech.MISMOClosing.LOAN._APPLICATION.LOAN_PURPOSE.$._Type
      agent: mortech.assignedLO
      recipient:
        firstname: borrower.$._FirstName
        lastname: borrower.$._LastName
        address:
          line1: borrower._RESIDENCE.$._StreetAddress
          line2: borrower._RESIDENCE.$._StreetAddress2
          city: borrower._RESIDENCE.$._City
          state: borrower._RESIDENCE.$._State
          zip: borrower._RESIDENCE.$._PostalCode
        phones: {}

    if email = _.first(_.where borrower.CONTACT_POINT, $: _Type: 'Email')
      json.recipient.email = email.$._Value
    else
      json.recipient.email = borrower.CONTACT_POINT.$._Value

    _.forEach _.where(borrower.CONTACT_POINT, $: _Type: 'Phone'), (p) ->
      json.recipient.phones[p.$._RoleType.toLowerCase()] = p.$._Value

    json.external =
      source: 'marksman'
      id: prospectid

    request
      method: 'POST'
      url: app.get('api url') + '/v1/leads'
      qs: key: req.query.key
      json: json
    , (err, qres, body) ->
      res.status(200).send '''<?xml version="1.0" encoding="utf-8"?>
<mortech>
  <response>
    <errorCode>0</errorCode>
    <errorDesc>Success</errorDesc>
  </response>
</mortech>'''
