XmlRequest = require '../../xmlrequest'

module.exports = class MarksmanXmlRequest extends XmlRequest
  constructor: -> super 'mortech.marksman'

  request: (info, id) ->
    method: 'GET'
    url: 'https://thirdparty.mortech-inc.com/thirdpartyintegration/getprospect'
    body: """<?xml version="1.0" encoding="utf-8"?>
<mortech>
  <Auth>
    <clientId>#{info.clientId}</clientId>
    <thirdPartyName>softvu</thirdPartyName>
    <licenseKey>RE$3SdT!Q6</licenseKey>
  </Auth>
  <prospectId>#{id}</prospectId>
  <dataSegments/>
</mortech>"""

  parser: ({mortech}) ->
    status: mortech.statusName
    agent: mortech.assignedLO
