(function() {
  var MarksmanXmlRequest, XmlRequest,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  XmlRequest = require('../../xmlrequest');

  module.exports = MarksmanXmlRequest = (function(superClass) {
    extend(MarksmanXmlRequest, superClass);

    function MarksmanXmlRequest() {
      MarksmanXmlRequest.__super__.constructor.call(this, 'mortech.marksman');
    }

    MarksmanXmlRequest.prototype.request = function(info, id) {
      return {
        method: 'GET',
        url: 'https://thirdparty.mortech-inc.com/thirdpartyintegration/getprospect',
        body: "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<mortech>\n  <Auth>\n    <clientId>" + info.clientId + "</clientId>\n    <thirdPartyName>softvu</thirdPartyName>\n    <licenseKey>RE$3SdT!Q6</licenseKey>\n  </Auth>\n  <prospectId>" + id + "</prospectId>\n  <dataSegments/>\n</mortech>"
      };
    };

    MarksmanXmlRequest.prototype.parser = function(arg) {
      var mortech;
      mortech = arg.mortech;
      return {
        status: mortech.statusName,
        agent: mortech.assignedLO
      };
    };

    return MarksmanXmlRequest;

  })(XmlRequest);

}).call(this);
