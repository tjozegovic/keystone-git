_ = require 'lodash'
express = require 'express'
request = require 'request'
{Parser} = require 'xml2js'
{RepositoryFactory} = require 'shared/database'

module.exports = app = express()

app.use (req, res, next) ->
  factory = new RepositoryFactory req.app.get 'db url'
  req.db = factory

  db = factory.use 'core/clients'
  db.get req.client, (err, body) ->
    req._client = body
    next()

app.use (req, res, next) ->
  return next() unless req.method is 'POST'
  logs = req.db.use 'core/logs'
  logs.insert
    source: 'mortech'
    at: new Date()
    raw: req.rawxml
    request: _.pick req, ['headers', 'query', 'originalUrl', 'ip']
    data: req.body.mortech
  , (err) ->
    # TODO req.logid was an attempt to tie the request to a specific log item
    # postgres insert does not return anything presently
    # req.logid = body.id
    res.locals.mortech =
      getprospect: (id, cb) ->
        mortech_request =
          method: 'GET'
          url: 'https://thirdparty.mortech-inc.com/thirdpartyintegration/getprospect'
          body: """<?xml version="1.0" encoding="utf-8"?>
<mortech>
  <Auth>
    <thirdPartyName>softvu</thirdPartyName>
    <clientId>#{req._client.integrations.marksman.clientId}</clientId>
    <licenseKey>RE$3SdT!Q6</licenseKey>
  </Auth>
  <prospectId>#{id}</prospectId>
  <dataSegments>
    <dataSegment>1</dataSegment>
    <dataSegment>3</dataSegment>
    <dataSegment>4</dataSegment>
  </dataSegments>
</mortech>"""
        log = (extra, done) ->
          logs.insert (_.assign
            target: 'mortech'
            parent: req.logid
            at: new Date()
            req: mortech_request
          , extra), (err) ->
            console.error err if err?
            done err

        request mortech_request, (rerr, qres, body) ->
          if rerr
            return log
              request_err: rerr
              body: body
            , -> cb rerr

          parser = new Parser explicitArray: no
          parser.parseString body, (xerr, result) ->
            if xerr
              log body: body, xml_err: xerr, -> cb xerr, null
            else
              {response} = result.mortech
              log body: body, ->
                if response.errorCode isnt '0'
                  cb response.errorDesc, null
                else
                  cb null, result.mortech

    next err

app.get '/notification', (req, res) ->
  res.send 'ok'

app.post '/notification', (req, res) ->
  return res.status(200).send '''<?xml version="1.0" encoding="utf-8"?>
    <mortech>
      <response>
        <errorCode>0</errorCode>
        <errorDesc>Success</errorDesc>
      </response>
    </mortech>''' unless req.body.notificationType in ['1', '2']
  prospectid = req.body.prospectId
  res.locals.mortech.getprospect prospectid, (err, mortech) ->
    # TODO unauthenticated api calls should be logged and available to the CMs

    # if there are multiple borrowers, this will ben an array and we want the borrow position, not co-borrower
    borrower = _.first _.where mortech.MISMOClosing.LOAN._APPLICATION.BORROWER, $: _PrintPositionType: 'Borrower'
    borrower ?= mortech.MISMOClosing.LOAN._APPLICATION.BORROWER

    json =
      status: mortech.statusName.trim()
      attributes:
        leadsource: mortech.sourceName
        product: mortech.MISMOClosing.LOAN._APPLICATION.LOAN_PURPOSE.$._Type
      agent: mortech.assignedLO
      recipient:
        firstname: borrower.$._FirstName
        lastname: borrower.$._LastName
        address:
          line1: borrower._RESIDENCE.$._StreetAddress
          line2: borrower._RESIDENCE.$._StreetAddress2
          city: borrower._RESIDENCE.$._City
          state: borrower._RESIDENCE.$._State
          zip: borrower._RESIDENCE.$._PostalCode
        phones: {}

    if email = _.first(_.where borrower.CONTACT_POINT, $: _Type: 'Email')
      json.recipient.email = email.$._Value
    else
      json.recipient.email = borrower.CONTACT_POINT.$._Value

    _.forEach _.where(borrower.CONTACT_POINT, $: _Type: 'Phone'), (p) ->
      json.recipient.phones[p.$._RoleType.toLowerCase()] = p.$._Value

    json.external =
      source: 'marksman'
      id: prospectid

    request
      method: 'POST'
      url: req.app.get('api url') + '/v1/leads'
      qs: key: req.query.key
      json: json
    , (err, qres, body) ->
      res.status(200).send '''<?xml version="1.0" encoding="utf-8"?>
<mortech>
  <response>
    <errorCode>0</errorCode>
    <errorDesc>Success</errorDesc>
  </response>
</mortech>'''
