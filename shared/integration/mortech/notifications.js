(function() {
  var Parser, RepositoryFactory, _, app, express, request;

  _ = require('lodash');

  express = require('express');

  request = require('request');

  Parser = require('xml2js').Parser;

  RepositoryFactory = require('shared/database').RepositoryFactory;

  module.exports = app = express();

  app.use(function(req, res, next) {
    var db, factory;
    factory = new RepositoryFactory(req.app.get('db url'));
    req.db = factory;
    db = factory.use('core/clients');
    return db.get(req.client, function(err, body) {
      req._client = body;
      return next();
    });
  });

  app.use(function(req, res, next) {
    var logs;
    if (req.method !== 'POST') {
      return next();
    }
    logs = req.db.use('core/logs');
    return logs.insert({
      source: 'mortech',
      at: new Date(),
      raw: req.rawxml,
      request: _.pick(req, ['headers', 'query', 'originalUrl', 'ip']),
      data: req.body.mortech
    }, function(err) {
      res.locals.mortech = {
        getprospect: function(id, cb) {
          var log, mortech_request;
          mortech_request = {
            method: 'GET',
            url: 'https://thirdparty.mortech-inc.com/thirdpartyintegration/getprospect',
            body: "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<mortech>\n  <Auth>\n    <thirdPartyName>softvu</thirdPartyName>\n    <clientId>" + req._client.integrations.marksman.clientId + "</clientId>\n    <licenseKey>RE$3SdT!Q6</licenseKey>\n  </Auth>\n  <prospectId>" + id + "</prospectId>\n  <dataSegments>\n    <dataSegment>1</dataSegment>\n    <dataSegment>3</dataSegment>\n    <dataSegment>4</dataSegment>\n  </dataSegments>\n</mortech>"
          };
          log = function(extra, done) {
            return logs.insert(_.assign({
              target: 'mortech',
              parent: req.logid,
              at: new Date(),
              req: mortech_request
            }, extra), function(err) {
              if (err != null) {
                console.error(err);
              }
              return done(err);
            });
          };
          return request(mortech_request, function(rerr, qres, body) {
            var parser;
            if (rerr) {
              return log({
                request_err: rerr,
                body: body
              }, function() {
                return cb(rerr);
              });
            }
            parser = new Parser({
              explicitArray: false
            });
            return parser.parseString(body, function(xerr, result) {
              var response;
              if (xerr) {
                return log({
                  body: body,
                  xml_err: xerr
                }, function() {
                  return cb(xerr, null);
                });
              } else {
                response = result.mortech.response;
                return log({
                  body: body
                }, function() {
                  if (response.errorCode !== '0') {
                    return cb(response.errorDesc, null);
                  } else {
                    return cb(null, result.mortech);
                  }
                });
              }
            });
          });
        }
      };
      return next(err);
    });
  });

  app.get('/notification', function(req, res) {
    return res.send('ok');
  });

  app.post('/notification', function(req, res) {
    var prospectid, ref;
    if ((ref = req.body.notificationType) !== '1' && ref !== '2') {
      return res.status(200).send('<?xml version="1.0" encoding="utf-8"?>\n<mortech>\n  <response>\n    <errorCode>0</errorCode>\n    <errorDesc>Success</errorDesc>\n  </response>\n</mortech>');
    }
    prospectid = req.body.prospectId;
    return res.locals.mortech.getprospect(prospectid, function(err, mortech) {
      var borrower, email, json;
      borrower = _.first(_.where(mortech.MISMOClosing.LOAN._APPLICATION.BORROWER, {
        $: {
          _PrintPositionType: 'Borrower'
        }
      }));
      if (borrower == null) {
        borrower = mortech.MISMOClosing.LOAN._APPLICATION.BORROWER;
      }
      json = {
        status: mortech.statusName.trim(),
        attributes: {
          leadsource: mortech.sourceName,
          product: mortech.MISMOClosing.LOAN._APPLICATION.LOAN_PURPOSE.$._Type
        },
        agent: mortech.assignedLO,
        recipient: {
          firstname: borrower.$._FirstName,
          lastname: borrower.$._LastName,
          address: {
            line1: borrower._RESIDENCE.$._StreetAddress,
            line2: borrower._RESIDENCE.$._StreetAddress2,
            city: borrower._RESIDENCE.$._City,
            state: borrower._RESIDENCE.$._State,
            zip: borrower._RESIDENCE.$._PostalCode
          },
          phones: {}
        }
      };
      if (email = _.first(_.where(borrower.CONTACT_POINT, {
        $: {
          _Type: 'Email'
        }
      }))) {
        json.recipient.email = email.$._Value;
      } else {
        json.recipient.email = borrower.CONTACT_POINT.$._Value;
      }
      _.forEach(_.where(borrower.CONTACT_POINT, {
        $: {
          _Type: 'Phone'
        }
      }), function(p) {
        return json.recipient.phones[p.$._RoleType.toLowerCase()] = p.$._Value;
      });
      json.external = {
        source: 'marksman',
        id: prospectid
      };
      return request({
        method: 'POST',
        url: req.app.get('api url') + '/v1/leads',
        qs: {
          key: req.query.key
        },
        json: json
      }, function(err, qres, body) {
        return res.status(200).send('<?xml version="1.0" encoding="utf-8"?>\n<mortech>\n  <response>\n    <errorCode>0</errorCode>\n    <errorDesc>Success</errorDesc>\n  </response>\n</mortech>');
      });
    });
  });

}).call(this);
