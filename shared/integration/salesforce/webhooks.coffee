config = require 'config'

_ = require 'lodash'
http = require 'http'
director = require 'director'
querystring = require 'querystring'
nano = require 'nano'
jsforce = require 'jsforce'
{RepositoryFactory} = require 'shared/database'

action_types =
  'email.queued': 'SoftVu Send'
  'email.opened': 'SoftVu Open'
  'email.clicked': 'SoftVu ClickVu'

salesforceProcess = (req, res, done) ->
  data = req.body
  return done null, response = statusCode: 200 unless data.type in
    ['email.queued', 'email.opened', 'email.clicked', 'optout']
  return done null, response = statusCode: 200 unless data.email?

  data.client = querystring.parse(req.url.split("?").pop()).client
  factory = new RepositoryFactory config.db.url
  factory.client = data.client
  softvu = factory.use 'core/clients'
  leads = factory.use 'client/leads'
  emails = factory.use 'client/emails'

  softvu.get data.client, (err, client) ->
    return done null, response = statusCode: 200 unless data.type in (client.webhooks?.enabled_webhooks or [])

    emails.get data.email, (err, message) ->
      return done err if err
      return done() unless message.lead

      leads.get message.lead.id, (err, lead) ->
        return done err if err or not lead

        info = client.integrations.salesforce
        conn = new jsforce.Connection()
        conn.login info.username, info.password, (err, res) ->
          return done err if err

          if data.type isnt 'optout'
            if !message.status?
              note = "Message: Direct Send - #{message.message.name}"
            else
              note = "Message: #{message.status.name} - #{message.segment.name} - #{message.step.name}"
            console.log 'sending action to salesforce', action_types[data.type], note
            conn.sobject("Task").create {
              Subject: note,
              Type: action_types[data.type],
              Status: "Completed",
              OwnerId: lead.external.ownerid,
              WhoId: lead.external.id,
              Description: note
            }, (err, response) ->
              done err, response
          else
            console.log 'sending email optout to salesforce', lead.recipient.email
            conn.sobject("Lead").update {
              Id: lead.external.id,
              HasOptedOutOfEmail: true
            }, (err, response) ->
              done err, response

postSalesforce = ->
  salesforceProcess(@req, @res, (err, response) =>
    return unless response or err
    if err?
      @res.writeHead 502
      @res.write err.message
      @res.end()
    else
      @res.writeHead 200
      @res.end()
  )

router = new director.http.Router {
  '/salesforce': {
    post: postSalesforce
  }
}

server = http.createServer((req, res) ->
  req.chunks = []
  req.on 'data', (chunk) ->
    req.chunks.push chunk.toString()

  router.dispatch req, res, (err) ->
    if err
      res.writeHead 404
      res.end()

    console.log 'Served ' + req.url
)

server.listen 9997, ->
  console.log "Salesforce Integration started on PORT 9997"
