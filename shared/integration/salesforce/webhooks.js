(function() {
  var RepositoryFactory, _, action_types, config, director, http, jsforce, nano, postSalesforce, querystring, router, salesforceProcess, server,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  config = require('config');

  _ = require('lodash');

  http = require('http');

  director = require('director');

  querystring = require('querystring');

  nano = require('nano');

  jsforce = require('jsforce');

  RepositoryFactory = require('shared/database').RepositoryFactory;

  action_types = {
    'email.queued': 'SoftVu Send',
    'email.opened': 'SoftVu Open',
    'email.clicked': 'SoftVu ClickVu'
  };

  salesforceProcess = function(req, res, done) {
    var data, emails, factory, leads, ref, response, softvu;
    data = req.body;
    if ((ref = data.type) !== 'email.queued' && ref !== 'email.opened' && ref !== 'email.clicked' && ref !== 'optout') {
      return done(null, response = {
        statusCode: 200
      });
    }
    if (data.email == null) {
      return done(null, response = {
        statusCode: 200
      });
    }
    data.client = querystring.parse(req.url.split("?").pop()).client;
    factory = new RepositoryFactory(config.db.url);
    factory.client = data.client;
    softvu = factory.use('core/clients');
    leads = factory.use('client/leads');
    emails = factory.use('client/emails');
    return softvu.get(data.client, function(err, client) {
      var ref1, ref2;
      if (ref1 = data.type, indexOf.call(((ref2 = client.webhooks) != null ? ref2.enabled_webhooks : void 0) || [], ref1) < 0) {
        return done(null, response = {
          statusCode: 200
        });
      }
      return emails.get(data.email, function(err, message) {
        if (err) {
          return done(err);
        }
        if (!message.lead) {
          return done();
        }
        return leads.get(message.lead.id, function(err, lead) {
          var conn, info;
          if (err || !lead) {
            return done(err);
          }
          info = client.integrations.salesforce;
          conn = new jsforce.Connection();
          return conn.login(info.username, info.password, function(err, res) {
            var note;
            if (err) {
              return done(err);
            }
            if (data.type !== 'optout') {
              if (message.status == null) {
                note = "Message: Direct Send - " + message.message.name;
              } else {
                note = "Message: " + message.status.name + " - " + message.segment.name + " - " + message.step.name;
              }
              console.log('sending action to salesforce', action_types[data.type], note);
              return conn.sobject("Task").create({
                Subject: note,
                Type: action_types[data.type],
                Status: "Completed",
                OwnerId: lead.external.ownerid,
                WhoId: lead.external.id,
                Description: note
              }, function(err, response) {
                return done(err, response);
              });
            } else {
              console.log('sending email optout to salesforce', lead.recipient.email);
              return conn.sobject("Lead").update({
                Id: lead.external.id,
                HasOptedOutOfEmail: true
              }, function(err, response) {
                return done(err, response);
              });
            }
          });
        });
      });
    });
  };

  postSalesforce = function() {
    return salesforceProcess(this.req, this.res, (function(_this) {
      return function(err, response) {
        if (!(response || err)) {
          return;
        }
        if (err != null) {
          _this.res.writeHead(502);
          _this.res.write(err.message);
          return _this.res.end();
        } else {
          _this.res.writeHead(200);
          return _this.res.end();
        }
      };
    })(this));
  };

  router = new director.http.Router({
    '/salesforce': {
      post: postSalesforce
    }
  });

  server = http.createServer(function(req, res) {
    req.chunks = [];
    req.on('data', function(chunk) {
      return req.chunks.push(chunk.toString());
    });
    return router.dispatch(req, res, function(err) {
      if (err) {
        res.writeHead(404);
        res.end();
      }
      return console.log('Served ' + req.url);
    });
  });

  server.listen(9997, function() {
    return console.log("Salesforce Integration started on PORT 9997");
  });

}).call(this);
