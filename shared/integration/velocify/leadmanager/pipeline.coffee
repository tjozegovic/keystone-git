XmlRequest = require '../../xmlrequest'
_ = require 'lodash'

module.exports = class LeadManagerXmlRequest extends XmlRequest
  constructor: -> super 'velocify.leadmanager'

  request: (info, id) ->
    method: 'POST'
    url: 'https://service.leads360.com/ClientService.asmx/GetLead'
    form:
      username: info.username
      password: info.password
      leadId: id

  parser: ({Leads}) ->
    {Status, Agent} = Leads.Lead
    status: Status.$.StatusTitle
    agent: Agent?.$.AgentEmail or null
    opted_out: _.any Leads.Lead.Fields.Field, (f) ->
      f.$.Value is '(Opt-Out)'
