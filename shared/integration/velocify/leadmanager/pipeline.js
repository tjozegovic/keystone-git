(function() {
  var LeadManagerXmlRequest, XmlRequest, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  XmlRequest = require('../../xmlrequest');

  _ = require('lodash');

  module.exports = LeadManagerXmlRequest = (function(superClass) {
    extend(LeadManagerXmlRequest, superClass);

    function LeadManagerXmlRequest() {
      LeadManagerXmlRequest.__super__.constructor.call(this, 'velocify.leadmanager');
    }

    LeadManagerXmlRequest.prototype.request = function(info, id) {
      return {
        method: 'POST',
        url: 'https://service.leads360.com/ClientService.asmx/GetLead',
        form: {
          username: info.username,
          password: info.password,
          leadId: id
        }
      };
    };

    LeadManagerXmlRequest.prototype.parser = function(arg) {
      var Agent, Leads, Status, ref;
      Leads = arg.Leads;
      ref = Leads.Lead, Status = ref.Status, Agent = ref.Agent;
      return {
        status: Status.$.StatusTitle,
        agent: (Agent != null ? Agent.$.AgentEmail : void 0) || null,
        opted_out: _.any(Leads.Lead.Fields.Field, function(f) {
          return f.$.Value === '(Opt-Out)';
        })
      };
    };

    return LeadManagerXmlRequest;

  })(XmlRequest);

}).call(this);
