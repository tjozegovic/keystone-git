config = require 'config'

_ = require 'lodash'
http = require 'http'
director = require 'director'
querystring = require 'querystring'
nano = require 'nano'
request = require 'request'
xml2js = require 'xml2js'
{RepositoryFactory} = require 'shared/database'

action_types =
  'email.queued': 'SoftVu Send'
  'email.opened': 'SoftVu Open'
  'email.clicked': 'SoftVu ClickVu'

velocifyProcess = (req, res, done) ->
  data = req.body
  return done null, response = statusCode: 200 unless data.type in
    ['email.queued', 'email.opened', 'email.clicked', 'optout']
  return done null, response = statusCode: 200 unless data.email?

  data.client = querystring.parse(req.url.split("?").pop()).client
  factory = new RepositoryFactory config.db.url
  factory.client = data.client
  softvu = factory.use 'core/clients'
  leads = factory.use 'client/leads'
  emails = factory.use 'client/emails'

  softvu.get data.client, (err, client) ->
    return done null, response = statusCode: 200 unless data.type in (client.webhooks?.enabled_webhooks or [])

    emails.get data.email, (err, message) ->
      return done err if err
      return done() unless message?.lead

      id = message.lead._id or message.lead.id
      return done() unless id?

      leads.get id, (err, lead) ->
        return done err if err or not lead

        info = client.integrations.velocify
        request.get 'https://service.leads360.com/ClientService.asmx/GetActionTypes',
          qs: username: info.username, password: info.password
        , (err, response, body) ->
          return done err if err

          parser = new xml2js.Parser()
          parser.parseString body, (err, result) ->
            return done err if err

            types = _.pluck result.ActionTypes.ActionType, '$'
            type = _.find(types, ActionTypeTitle: action_types[data.type])?.ActionTypeId
            if !message.status?
              note = "Message: Direct Send - #{message.message.name}"
            else
              note = "Message: #{message.status.name} - #{message.segment.name} - #{message.step.name}"

            if data.type isnt 'optout'
              console.log 'sending action to velocify', action_types[data.type], note
              request.post 'https://service.leads360.com/ClientService.asmx/AddLeadAction',
                form:
                  username: info.username
                  password: info.password
                  leadId: lead.external.id
                  actionTypeId: type
                  actionNote: note
              , (err, response, body) ->
                done err, response
            else
              console.log 'sending email optout to velocify', lead.recipient.email
              request.post 'https://service.leads360.com/ClientService.asmx/AddOptOut',
                form:
                  username: info.username
                  password: info.password
                  leadId: lead.external.id
                  email: lead.recipient.email
              , (err, response, body) ->
                done err, response

postVelocify = ->
  velocifyProcess(@req, @res, (err, response) =>
    if err?
      @res.writeHead 502
      @res.write err.message
      @res.end()
    else if response? and response.statusCode isnt 200
      @res.writeHead response.statusCode, response.headers
      @res.write response.body
      @res.end()
    else
      @res.writeHead 200
      @res.end()
  )

router = new director.http.Router {
  '/velocify': {
    post: postVelocify
  }
}

server = http.createServer((req, res) ->
  req.chunks = []
  req.on 'data', (chunk) ->
    req.chunks.push chunk.toString()

  router.dispatch req, res, (err) ->
    if err
      res.writeHead 404
      res.end()

    console.log 'Served ' + req.url
)

server.listen 9998, ->
  console.log "Velocify Integration started on PORT 9998"
