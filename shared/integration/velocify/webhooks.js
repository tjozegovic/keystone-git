(function() {
  var RepositoryFactory, _, action_types, config, director, http, nano, postVelocify, querystring, request, router, server, velocifyProcess, xml2js,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  config = require('config');

  _ = require('lodash');

  http = require('http');

  director = require('director');

  querystring = require('querystring');

  nano = require('nano');

  request = require('request');

  xml2js = require('xml2js');

  RepositoryFactory = require('shared/database').RepositoryFactory;

  action_types = {
    'email.queued': 'SoftVu Send',
    'email.opened': 'SoftVu Open',
    'email.clicked': 'SoftVu ClickVu'
  };

  velocifyProcess = function(req, res, done) {
    var data, emails, factory, leads, ref, response, softvu;
    data = req.body;
    if ((ref = data.type) !== 'email.queued' && ref !== 'email.opened' && ref !== 'email.clicked' && ref !== 'optout') {
      return done(null, response = {
        statusCode: 200
      });
    }
    if (data.email == null) {
      return done(null, response = {
        statusCode: 200
      });
    }
    data.client = querystring.parse(req.url.split("?").pop()).client;
    factory = new RepositoryFactory(config.db.url);
    factory.client = data.client;
    softvu = factory.use('core/clients');
    leads = factory.use('client/leads');
    emails = factory.use('client/emails');
    return softvu.get(data.client, function(err, client) {
      var ref1, ref2;
      if (ref1 = data.type, indexOf.call(((ref2 = client.webhooks) != null ? ref2.enabled_webhooks : void 0) || [], ref1) < 0) {
        return done(null, response = {
          statusCode: 200
        });
      }
      return emails.get(data.email, function(err, message) {
        var id;
        if (err) {
          return done(err);
        }
        if (!(message != null ? message.lead : void 0)) {
          return done();
        }
        id = message.lead._id || message.lead.id;
        if (id == null) {
          return done();
        }
        return leads.get(id, function(err, lead) {
          var info;
          if (err || !lead) {
            return done(err);
          }
          info = client.integrations.velocify;
          return request.get('https://service.leads360.com/ClientService.asmx/GetActionTypes', {
            qs: {
              username: info.username,
              password: info.password
            }
          }, function(err, response, body) {
            var parser;
            if (err) {
              return done(err);
            }
            parser = new xml2js.Parser();
            return parser.parseString(body, function(err, result) {
              var note, ref3, type, types;
              if (err) {
                return done(err);
              }
              types = _.pluck(result.ActionTypes.ActionType, '$');
              type = (ref3 = _.find(types, {
                ActionTypeTitle: action_types[data.type]
              })) != null ? ref3.ActionTypeId : void 0;
              if (message.status == null) {
                note = "Message: Direct Send - " + message.message.name;
              } else {
                note = "Message: " + message.status.name + " - " + message.segment.name + " - " + message.step.name;
              }
              if (data.type !== 'optout') {
                console.log('sending action to velocify', action_types[data.type], note);
                return request.post('https://service.leads360.com/ClientService.asmx/AddLeadAction', {
                  form: {
                    username: info.username,
                    password: info.password,
                    leadId: lead.external.id,
                    actionTypeId: type,
                    actionNote: note
                  }
                }, function(err, response, body) {
                  return done(err, response);
                });
              } else {
                console.log('sending email optout to velocify', lead.recipient.email);
                return request.post('https://service.leads360.com/ClientService.asmx/AddOptOut', {
                  form: {
                    username: info.username,
                    password: info.password,
                    leadId: lead.external.id,
                    email: lead.recipient.email
                  }
                }, function(err, response, body) {
                  return done(err, response);
                });
              }
            });
          });
        });
      });
    });
  };

  postVelocify = function() {
    return velocifyProcess(this.req, this.res, (function(_this) {
      return function(err, response) {
        if (err != null) {
          _this.res.writeHead(502);
          _this.res.write(err.message);
          return _this.res.end();
        } else if ((response != null) && response.statusCode !== 200) {
          _this.res.writeHead(response.statusCode, response.headers);
          _this.res.write(response.body);
          return _this.res.end();
        } else {
          _this.res.writeHead(200);
          return _this.res.end();
        }
      };
    })(this));
  };

  router = new director.http.Router({
    '/velocify': {
      post: postVelocify
    }
  });

  server = http.createServer(function(req, res) {
    req.chunks = [];
    req.on('data', function(chunk) {
      return req.chunks.push(chunk.toString());
    });
    return router.dispatch(req, res, function(err) {
      if (err) {
        res.writeHead(404);
        res.end();
      }
      return console.log('Served ' + req.url);
    });
  });

  server.listen(9998, function() {
    return console.log("Velocify Integration started on PORT 9998");
  });

}).call(this);
