_ = require 'lodash'
{Parser} = require 'xml2js'
request = require 'request'

module.exports = class XmlRequest
  constructor: (@name) ->

  validate: (ctx, {agent, lead, client, mail}, info, id, done) ->
    @log ?= do (logs = ctx.use 'core/logs') => (extra) =>
      logs.insert _.assign
        target: @name
        at: new Date()
      , extra

    request req = @request(info, id), (err, res, body) =>
      if err?
        @log body: body, req: req, request_err: err
        return done err

      parser = new Parser explicitArray: no
      parser.parseString body, (err, result) =>
        @log body: body, req: req, data: result, xml_err: (err if err?)

        if err?
          err.body = body
          return done err

        props = @parser result

        statuses = ctx.use 'client/statuses'
        statuses.byalias props.status.trim(), (err, status) ->
          # TODO retry...
          return done err if err

          if props.opted_out is true
            ctx.events.emit 'optout.reconciliation',
              lead: lead.id
              from: ctx.client.integrations._current
              optout: mail.to
            action = 'cancel'

          unless status?
            ctx.events.emit 'lead.invalidated', lead: lead.id, reason: 'unknown status'
            action = 'cancel'
          else if (lead.$status or lead.status) isnt status.id
            ctx.events.emit 'lead.status.changed', lead: lead.id, status: status.id
            action = 'cancel'

          if lead.agent isnt props.agent
            ctx.events.emit 'lead.agent.changed', lead: lead.id, agent: props.agent
            action ?= 'requeue'

          done action
