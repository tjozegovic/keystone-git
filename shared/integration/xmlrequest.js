(function() {
  var Parser, XmlRequest, _, request;

  _ = require('lodash');

  Parser = require('xml2js').Parser;

  request = require('request');

  module.exports = XmlRequest = (function() {
    function XmlRequest(name) {
      this.name = name;
    }

    XmlRequest.prototype.validate = function(ctx, arg, info, id, done) {
      var agent, client, lead, mail, req;
      agent = arg.agent, lead = arg.lead, client = arg.client, mail = arg.mail;
      if (this.log == null) {
        this.log = (function(_this) {
          return function(logs) {
            return function(extra) {
              return logs.insert(_.assign({
                target: _this.name,
                at: new Date()
              }, extra));
            };
          };
        })(this)(ctx.use('core/logs'));
      }
      return request(req = this.request(info, id), (function(_this) {
        return function(err, res, body) {
          var parser;
          if (err != null) {
            _this.log({
              body: body,
              req: req,
              request_err: err
            });
            return done(err);
          }
          parser = new Parser({
            explicitArray: false
          });
          return parser.parseString(body, function(err, result) {
            var props, statuses;
            _this.log({
              body: body,
              req: req,
              data: result,
              xml_err: (err != null ? err : void 0)
            });
            if (err != null) {
              err.body = body;
              return done(err);
            }
            props = _this.parser(result);
            statuses = ctx.use('client/statuses');
            return statuses.byalias(props.status.trim(), function(err, status) {
              var action;
              if (err) {
                return done(err);
              }
              if (props.opted_out === true) {
                ctx.events.emit('optout.reconciliation', {
                  lead: lead.id,
                  from: ctx.client.integrations._current,
                  optout: mail.to
                });
                action = 'cancel';
              }
              if (status == null) {
                ctx.events.emit('lead.invalidated', {
                  lead: lead.id,
                  reason: 'unknown status'
                });
                action = 'cancel';
              } else if ((lead.$status || lead.status) !== status.id) {
                ctx.events.emit('lead.status.changed', {
                  lead: lead.id,
                  status: status.id
                });
                action = 'cancel';
              }
              if (lead.agent !== props.agent) {
                ctx.events.emit('lead.agent.changed', {
                  lead: lead.id,
                  agent: props.agent
                });
                if (action == null) {
                  action = 'requeue';
                }
              }
              return done(action);
            });
          });
        };
      })(this));
    };

    return XmlRequest;

  })();

}).call(this);
