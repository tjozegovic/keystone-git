fs = require 'fs'
path = require 'path'
winston = require 'winston'

module.exports =
  default: ->
    # called directly or from libRequire
    parent = if ~module.parent.id.indexOf 'roots.js'
      module.parent.parent
    else
      module.parent

    dir = path.join path.dirname(parent.filename), '..', 'log'
    fs.mkdirSync dir unless fs.existsSync dir

    new winston.Logger
      transports: [
        new winston.transports.Console
          colorize: yes
          level: 'debug'
          json: no

        new winston.transports.File
          filename: dir + '/service.log'
          maxsize: 10 * 1024 * 1024
          maxFiles: 10
          timestamp: yes
          level: 'debug'
          json: no
      ]
