(function() {
  var fs, path, winston;

  fs = require('fs');

  path = require('path');

  winston = require('winston');

  module.exports = {
    "default": function() {
      var dir, parent;
      parent = ~module.parent.id.indexOf('roots.js') ? module.parent.parent : module.parent;
      dir = path.join(path.dirname(parent.filename), '..', 'log');
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      return new winston.Logger({
        transports: [
          new winston.transports.Console({
            colorize: true,
            level: 'debug',
            json: false
          }), new winston.transports.File({
            filename: dir + '/service.log',
            maxsize: 10 * 1024 * 1024,
            maxFiles: 10,
            timestamp: true,
            level: 'debug',
            json: false
          })
        ]
      });
    }
  };

}).call(this);
