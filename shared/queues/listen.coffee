_ = require 'lodash'
amqp = require 'amqplib'
{EventEmitter} = require 'events'

_.padLeft = (n, width, z) ->
  z = z or '0'
  n = n + ''
  return if n.length >= width then n else new Array(width - n.length + 1).join(z) + n

class Connection extends EventEmitter
  constructor: (@url, @opts) ->
    super()
    @setMaxListeners Infinity

    @connected = no
    @on 'connected', => @connected = yes
    @on 'disconnect', => @connected = no

    process.nextTick @connect

  connect: (done) =>
    amqp.connect(@url)
      .then (@conn) =>
        @conn.createChannel().then (@channel) =>
          @channel.prefetch @opts.limit or 4
          @emit 'connected'
          done?()

      .then null, (err) =>
        @emit 'error', err
        process.nextTick =>
          @conn?.close()
          @emit 'disconnect'
          setTimeout @connect, 2000

  close: (done) ->
    @conn?.close().then done

  exchange: (exchange) ->
    new Exchange @, @opts, exchange

  consume: (queue, consumer, done) ->
    @on 'connected', =>
      ok = @channel.assertQueue queue, durable: @opts.durable or yes # autoDelete, exclusive

      listener = if @opts.retries?.enabled
        ok = ok.then => @channel.assertExchange "#{queue}"
        ok = ok.then => @channel.assertQueue "#{queue}.fault", durable: yes
        @opts.fault_queue = "#{queue}.fault"

        len = ('' + _.last @opts.retries.waits).length
        # TODO _waits should probably just be passed in to the listeners instead of modifying the options...
        @opts.retries._waits = @opts.retries.waits.map (wait) -> ttl: wait, name: _.padLeft wait, len

        @opts.retries._waits.forEach (wait) =>
          name = "#{queue}.retry.#{wait.name}ms"
          ok = ok.then => @channel.assertQueue name, deadLetterExchange: queue, messageTtl: wait.ttl
          ok = ok.then => @channel.bindQueue "#{queue}", "#{queue}", name

        new RetryListener @channel, @opts, queue, consumer
      else
        new BasicListener @channel, @opts, consumer

      ok = ok.then => @channel.consume queue, listener.consume, noAck: false
      ok.then -> done? listener

class Exchange
  constructor: (@connection, @opts, @name) ->
    @connection.on 'connected', =>
      @channel = @connection.channel
      ok = @channel.assertExchange @name, 'topic', durable: yes, autoDelete: no
      ok = ok.then => @channel.assertQueue "#{@name}.fault", durable: yes

  route: (route, queue, opts, consumer, done) ->
    unless @connection.connected
      return @connection.on 'connected', =>
        @route route, queue, opts, consumer, done

    _.assign @opts, opts, fault_queue: "#{@name}.fault"
    ok = @channel.assertQueue queue, durable: @opts.durable, autoDelete: not @opts.durable
    ok = ok.then => @channel.bindQueue queue, @name, route

    listener = new RequeueListener @channel, @opts, queue, consumer
    ok = ok.then => @channel.consume queue, listener.consume, noAck: false
    ok.then -> done? listener

class BasicListener extends EventEmitter
  ###
  # a basic listener will only ack/nack messages
  # based on their success or not. nacked messages are
  # picked up by the next available consumer, and not moved
  # to the back of the queue.
  ###
  constructor: (@channel, @opts, @consumer) ->
    super

  consume: (msg) =>
    headers = msg.properties.headers or {}
    body = JSON.parse msg.content.toString()
    _original = _.cloneDeep body

    do (timer = null) =>
      timer = setTimeout =>
        @emit 'timeout', body
        @requeue _original, headers, msg
      , 60000
      timer.unref()

      @consumer.call @, body, (requeue) =>
        clearTimeout timer

        unless requeue
          @channel.ack msg
        else
          @requeue _original, headers, msg

  requeue: (..., msg) =>
    @channel.nack msg, no, not @opts.deadLetterExchange

class RequeueListener extends BasicListener
  ###
  # this queue listener will requeue messages to the same queue
  # basically moving them to the back of the line. it does so by ack-ing
  # the rabbit message, then calling sendToQueue to return it to a queue,
  # while incrementing the x-attempts header
  ###
  constructor: (@channel, @opts, @queue, @consumer) ->
    super @channel, @opts, @consumer
    @opts.attempts ?= 3

  requeue: (body, headers, msg) =>
    if @hasattempts body, headers, msg
      @emit 'retry', body
      @send body, @queue, headers
    else if @opts.fault_queue?
      @emit 'fault', body
      @send body, @opts.fault_queue

  hasattempts: (body, headers, msg) =>
    @channel.ack msg

    headers['x-attempts'] = (headers?['x-attempts'] or 0) + 1
    return headers['x-attempts'] < @opts.attempts

  send: (body, queue, headers) ->
    process.nextTick =>
      message = JSON.stringify body
      @channel.sendToQueue queue, new Buffer(message), headers: headers

class RetryListener extends RequeueListener
  ###
  # this queue listener will mimic the RequeueListener, but instead of
  # moving the message to the back of the queue line, it will send the message
  # to a TTL queue. this allows for retrying messages that having a sliding scale
  # built into the retry process
  ###
  constructor: (@channel, @opts = {}, @queue, @consumer) ->
    super @channel, @opts, @queue, @consumer
    @expirations = @opts.retries?._waits

  requeue: (body, headers, msg) =>
    unless @opts.retries?.enabled
      @channel.nack msg, no, not @opts.deadLetterExchange
    else
      if @hasattempts body, headers, msg
        @emit 'retry', body
        queue = "#{@queue}.retry.#{@expirations[headers['x-attempts']].name}ms"
        @send body, queue, headers
      else if @opts.fault_queue?
        @emit 'fault', body
        @send body, @opts.fault_queue

module.exports = (url, opts = {}) -> new Connection url + '?heartbeat=5', opts
