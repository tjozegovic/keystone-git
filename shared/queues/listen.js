(function() {
  var BasicListener, Connection, EventEmitter, Exchange, RequeueListener, RetryListener, _, amqp,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('lodash');

  amqp = require('amqplib');

  EventEmitter = require('events').EventEmitter;

  _.padLeft = function(n, width, z) {
    z = z || '0';
    n = n + '';
    if (n.length >= width) {
      return n;
    } else {
      return new Array(width - n.length + 1).join(z) + n;
    }
  };

  Connection = (function(superClass) {
    extend(Connection, superClass);

    function Connection(url1, opts1) {
      this.url = url1;
      this.opts = opts1;
      this.connect = bind(this.connect, this);
      Connection.__super__.constructor.call(this);
      this.setMaxListeners(Infinity);
      this.connected = false;
      this.on('connected', (function(_this) {
        return function() {
          return _this.connected = true;
        };
      })(this));
      this.on('disconnect', (function(_this) {
        return function() {
          return _this.connected = false;
        };
      })(this));
      process.nextTick(this.connect);
    }

    Connection.prototype.connect = function(done) {
      return amqp.connect(this.url).then((function(_this) {
        return function(conn) {
          _this.conn = conn;
          return _this.conn.createChannel().then(function(channel) {
            _this.channel = channel;
            _this.channel.prefetch(_this.opts.limit || 4);
            _this.emit('connected');
            return typeof done === "function" ? done() : void 0;
          });
        };
      })(this)).then(null, (function(_this) {
        return function(err) {
          _this.emit('error', err);
          return process.nextTick(function() {
            var ref;
            if ((ref = _this.conn) != null) {
              ref.close();
            }
            _this.emit('disconnect');
            return setTimeout(_this.connect, 2000);
          });
        };
      })(this));
    };

    Connection.prototype.close = function(done) {
      var ref;
      return (ref = this.conn) != null ? ref.close().then(done) : void 0;
    };

    Connection.prototype.exchange = function(exchange) {
      return new Exchange(this, this.opts, exchange);
    };

    Connection.prototype.consume = function(queue, consumer, done) {
      return this.on('connected', (function(_this) {
        return function() {
          var len, listener, ok, ref;
          ok = _this.channel.assertQueue(queue, {
            durable: _this.opts.durable || true
          });
          listener = ((ref = _this.opts.retries) != null ? ref.enabled : void 0) ? (ok = ok.then(function() {
            return _this.channel.assertExchange("" + queue);
          }), ok = ok.then(function() {
            return _this.channel.assertQueue(queue + ".fault", {
              durable: true
            });
          }), _this.opts.fault_queue = queue + ".fault", len = ('' + _.last(_this.opts.retries.waits)).length, _this.opts.retries._waits = _this.opts.retries.waits.map(function(wait) {
            return {
              ttl: wait,
              name: _.padLeft(wait, len)
            };
          }), _this.opts.retries._waits.forEach(function(wait) {
            var name;
            name = queue + ".retry." + wait.name + "ms";
            ok = ok.then(function() {
              return _this.channel.assertQueue(name, {
                deadLetterExchange: queue,
                messageTtl: wait.ttl
              });
            });
            return ok = ok.then(function() {
              return _this.channel.bindQueue("" + queue, "" + queue, name);
            });
          }), new RetryListener(_this.channel, _this.opts, queue, consumer)) : new BasicListener(_this.channel, _this.opts, consumer);
          ok = ok.then(function() {
            return _this.channel.consume(queue, listener.consume, {
              noAck: false
            });
          });
          return ok.then(function() {
            return typeof done === "function" ? done(listener) : void 0;
          });
        };
      })(this));
    };

    return Connection;

  })(EventEmitter);

  Exchange = (function() {
    function Exchange(connection, opts1, name1) {
      this.connection = connection;
      this.opts = opts1;
      this.name = name1;
      this.connection.on('connected', (function(_this) {
        return function() {
          var ok;
          _this.channel = _this.connection.channel;
          ok = _this.channel.assertExchange(_this.name, 'topic', {
            durable: true,
            autoDelete: false
          });
          return ok = ok.then(function() {
            return _this.channel.assertQueue(_this.name + ".fault", {
              durable: true
            });
          });
        };
      })(this));
    }

    Exchange.prototype.route = function(route, queue, opts, consumer, done) {
      var listener, ok;
      if (!this.connection.connected) {
        return this.connection.on('connected', (function(_this) {
          return function() {
            return _this.route(route, queue, opts, consumer, done);
          };
        })(this));
      }
      _.assign(this.opts, opts, {
        fault_queue: this.name + ".fault"
      });
      ok = this.channel.assertQueue(queue, {
        durable: this.opts.durable,
        autoDelete: !this.opts.durable
      });
      ok = ok.then((function(_this) {
        return function() {
          return _this.channel.bindQueue(queue, _this.name, route);
        };
      })(this));
      listener = new RequeueListener(this.channel, this.opts, queue, consumer);
      ok = ok.then((function(_this) {
        return function() {
          return _this.channel.consume(queue, listener.consume, {
            noAck: false
          });
        };
      })(this));
      return ok.then(function() {
        return typeof done === "function" ? done(listener) : void 0;
      });
    };

    return Exchange;

  })();

  BasicListener = (function(superClass) {
    extend(BasicListener, superClass);


    /*
     * a basic listener will only ack/nack messages
     * based on their success or not. nacked messages are
     * picked up by the next available consumer, and not moved
     * to the back of the queue.
     */

    function BasicListener(channel, opts1, consumer1) {
      this.channel = channel;
      this.opts = opts1;
      this.consumer = consumer1;
      this.requeue = bind(this.requeue, this);
      this.consume = bind(this.consume, this);
      BasicListener.__super__.constructor.apply(this, arguments);
    }

    BasicListener.prototype.consume = function(msg) {
      var _original, body, headers;
      headers = msg.properties.headers || {};
      body = JSON.parse(msg.content.toString());
      _original = _.cloneDeep(body);
      return (function(_this) {
        return function(timer) {
          timer = setTimeout(function() {
            _this.emit('timeout', body);
            return _this.requeue(_original, headers, msg);
          }, 60000);
          timer.unref();
          return _this.consumer.call(_this, body, function(requeue) {
            clearTimeout(timer);
            if (!requeue) {
              return _this.channel.ack(msg);
            } else {
              return _this.requeue(_original, headers, msg);
            }
          });
        };
      })(this)(null);
    };

    BasicListener.prototype.requeue = function() {
      var msg;
      msg = arguments[arguments.length - 1];
      return this.channel.nack(msg, false, !this.opts.deadLetterExchange);
    };

    return BasicListener;

  })(EventEmitter);

  RequeueListener = (function(superClass) {
    extend(RequeueListener, superClass);


    /*
     * this queue listener will requeue messages to the same queue
     * basically moving them to the back of the line. it does so by ack-ing
     * the rabbit message, then calling sendToQueue to return it to a queue,
     * while incrementing the x-attempts header
     */

    function RequeueListener(channel, opts1, queue1, consumer1) {
      var base;
      this.channel = channel;
      this.opts = opts1;
      this.queue = queue1;
      this.consumer = consumer1;
      this.hasattempts = bind(this.hasattempts, this);
      this.requeue = bind(this.requeue, this);
      RequeueListener.__super__.constructor.call(this, this.channel, this.opts, this.consumer);
      if ((base = this.opts).attempts == null) {
        base.attempts = 3;
      }
    }

    RequeueListener.prototype.requeue = function(body, headers, msg) {
      if (this.hasattempts(body, headers, msg)) {
        this.emit('retry', body);
        return this.send(body, this.queue, headers);
      } else if (this.opts.fault_queue != null) {
        this.emit('fault', body);
        return this.send(body, this.opts.fault_queue);
      }
    };

    RequeueListener.prototype.hasattempts = function(body, headers, msg) {
      this.channel.ack(msg);
      headers['x-attempts'] = ((headers != null ? headers['x-attempts'] : void 0) || 0) + 1;
      return headers['x-attempts'] < this.opts.attempts;
    };

    RequeueListener.prototype.send = function(body, queue, headers) {
      return process.nextTick((function(_this) {
        return function() {
          var message;
          message = JSON.stringify(body);
          return _this.channel.sendToQueue(queue, new Buffer(message), {
            headers: headers
          });
        };
      })(this));
    };

    return RequeueListener;

  })(BasicListener);

  RetryListener = (function(superClass) {
    extend(RetryListener, superClass);


    /*
     * this queue listener will mimic the RequeueListener, but instead of
     * moving the message to the back of the queue line, it will send the message
     * to a TTL queue. this allows for retrying messages that having a sliding scale
     * built into the retry process
     */

    function RetryListener(channel, opts1, queue1, consumer1) {
      var ref;
      this.channel = channel;
      this.opts = opts1 != null ? opts1 : {};
      this.queue = queue1;
      this.consumer = consumer1;
      this.requeue = bind(this.requeue, this);
      RetryListener.__super__.constructor.call(this, this.channel, this.opts, this.queue, this.consumer);
      this.expirations = (ref = this.opts.retries) != null ? ref._waits : void 0;
    }

    RetryListener.prototype.requeue = function(body, headers, msg) {
      var queue, ref;
      if (!((ref = this.opts.retries) != null ? ref.enabled : void 0)) {
        return this.channel.nack(msg, false, !this.opts.deadLetterExchange);
      } else {
        if (this.hasattempts(body, headers, msg)) {
          this.emit('retry', body);
          queue = this.queue + ".retry." + this.expirations[headers['x-attempts']].name + "ms";
          return this.send(body, queue, headers);
        } else if (this.opts.fault_queue != null) {
          this.emit('fault', body);
          return this.send(body, this.opts.fault_queue);
        }
      }
    };

    return RetryListener;

  })(RequeueListener);

  module.exports = function(url, opts) {
    if (opts == null) {
      opts = {};
    }
    return new Connection(url + '?heartbeat=5', opts);
  };

}).call(this);
