_ = require 'lodash'

module.exports =
  segment: (path, segments) ->
    some = (pairs) ->
      _.every pairs, (pair) ->
        pair in path

    found = _.find segments, (segment) ->
      _.some segment.products, some

    found or throw new Error 'Lead could not be segmented, no filters match.'
