(function() {
  var _,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  _ = require('lodash');

  module.exports = {
    segment: function(path, segments) {
      var found, some;
      some = function(pairs) {
        return _.every(pairs, function(pair) {
          return indexOf.call(path, pair) >= 0;
        });
      };
      found = _.find(segments, function(segment) {
        return _.some(segment.products, some);
      });
      return found || (function() {
        throw new Error('Lead could not be segmented, no filters match.');
      })();
    }
  };

}).call(this);
