config = require 'config'

amqp = require 'amqplib'
sockjs = require 'sockjs'

module.exports = (server) ->
  connections = {}

  events = sockjs.createServer sockjs_url: '/bower_components/sockjs/sockjs.min.js'
  events.on 'connection', (conn) ->
    connections[conn.id] = conn

    conn.on 'close', ->
      delete connections[conn.id]

  # TODO use shared/queues/listen
  amqp.connect(config.queues.url).then (conn) ->
    conn.createChannel().then (ch) ->
      ok = ch.assertExchange 'events.hose', 'topic', durable: yes, autoDelete: no
      ok = ok.then -> ch.assertQueue "events.sockjs-#{process.pid}", exclusive: yes
      ok = ok.then (qok) ->
        q = qok.queue
        ch.bindQueue(q, 'events.hose', '#.event').then -> q

      ok.then (queue) ->
        ch.consume queue, (msg) ->
          data = msg.content.toString()
          conn.write data for own id, conn of connections
        , noAck: yes

  events.installHandlers server, prefix: '/stream/events'
  null
