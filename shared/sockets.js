(function() {
  var amqp, config, sockjs,
    hasProp = {}.hasOwnProperty;

  config = require('config');

  amqp = require('amqplib');

  sockjs = require('sockjs');

  module.exports = function(server) {
    var connections, events;
    connections = {};
    events = sockjs.createServer({
      sockjs_url: '/bower_components/sockjs/sockjs.min.js'
    });
    events.on('connection', function(conn) {
      connections[conn.id] = conn;
      return conn.on('close', function() {
        return delete connections[conn.id];
      });
    });
    amqp.connect(config.queues.url).then(function(conn) {
      return conn.createChannel().then(function(ch) {
        var ok;
        ok = ch.assertExchange('events.hose', 'topic', {
          durable: true,
          autoDelete: false
        });
        ok = ok.then(function() {
          return ch.assertQueue("events.sockjs-" + process.pid, {
            exclusive: true
          });
        });
        ok = ok.then(function(qok) {
          var q;
          q = qok.queue;
          return ch.bindQueue(q, 'events.hose', '#.event').then(function() {
            return q;
          });
        });
        return ok.then(function(queue) {
          return ch.consume(queue, function(msg) {
            var data, id, results;
            data = msg.content.toString();
            results = [];
            for (id in connections) {
              if (!hasProp.call(connections, id)) continue;
              conn = connections[id];
              results.push(conn.write(data));
            }
            return results;
          }, {
            noAck: true
          });
        });
      });
    });
    events.installHandlers(server, {
      prefix: '/stream/events'
    });
    return null;
  };

}).call(this);
