_ = require 'lodash'
crypto = require 'crypto'

module.exports =
  md5: (data) ->
    hash = crypto.createHash 'md5'
    hash.update data, 'utf8'
    hash.digest 'hex'

  # http://stackoverflow.com/a/12628791/10669
  xproduct: (args...) ->
    _.reduce(args, (a, b) ->
      _.flatten _.map(a, (x) ->
        _.map b, (y) ->
          x.concat [y]
        ), no
    , [[]])

  # given an object, remove every value from that object
  # that is not in the keys 'whitelist'
  # e.g. filter {a: 'a', b: 'b'}, ['a'] => {a: 'a'}
  # e.g. filter {a: b: 'b', c: 'c'}, ['a.b'] => {a: b: 'b'}
  # see testbdd/shared/util.coffee for more examples
  filter: (object, keys, sep = '.') ->
    _.reduce keys, (result, key) ->
      branch = {}
      leaf = object
      step = branch

      for key in key.split sep when _.has leaf, key
        step[key] = if _.isPlainObject leaf[key] then {} else leaf[key]

        step = step[key]
        leaf = leaf[key]

      _.merge result, branch
    , {}
