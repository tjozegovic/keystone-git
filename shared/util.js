(function() {
  var _, crypto,
    slice = [].slice;

  _ = require('lodash');

  crypto = require('crypto');

  module.exports = {
    md5: function(data) {
      var hash;
      hash = crypto.createHash('md5');
      hash.update(data, 'utf8');
      return hash.digest('hex');
    },
    xproduct: function() {
      var args;
      args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      return _.reduce(args, function(a, b) {
        return _.flatten(_.map(a, function(x) {
          return _.map(b, function(y) {
            return x.concat([y]);
          });
        }), false);
      }, [[]]);
    },
    filter: function(object, keys, sep) {
      if (sep == null) {
        sep = '.';
      }
      return _.reduce(keys, function(result, key) {
        var branch, i, leaf, len, ref, step;
        branch = {};
        leaf = object;
        step = branch;
        ref = key.split(sep);
        for (i = 0, len = ref.length; i < len; i++) {
          key = ref[i];
          if (!(_.has(leaf, key))) {
            continue;
          }
          step[key] = _.isPlainObject(leaf[key]) ? {} : leaf[key];
          step = step[key];
          leaf = leaf[key];
        }
        return _.merge(result, branch);
      }, {});
    }
  };

}).call(this);
