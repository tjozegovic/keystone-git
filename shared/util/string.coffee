# loosely based on https://github.com/jprichardson/string.js/pull/57
S = require 'string'

# localSObj = S ''
#
# LocalString = ->
# LocalString:: = localSObj
# LocalString::constructor = LocalString
#
# localStringMaker = (value) ->
#   return value if value instanceof LocalString
#   return new LocalString value
#
# LocalString::indexable = ->
#   new @constructor @s.replace(/[^A-Za-z0-9]/g, '').toLowerCase()

Object.getPrototypeOf(S '').indexable = ->
  throw new Error "String is not indexable because it is undefined" unless @s?
  new @constructor @s.replace(/[^A-Za-z0-9]/g, '').toLowerCase()

module.exports = (val) ->
  return val if val instanceof S
  return new S val
