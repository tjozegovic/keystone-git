(function() {
  var S;

  S = require('string');

  Object.getPrototypeOf(S('')).indexable = function() {
    if (this.s == null) {
      throw new Error("String is not indexable because it is undefined");
    }
    return new this.constructor(this.s.replace(/[^A-Za-z0-9]/g, '').toLowerCase());
  };

  module.exports = function(val) {
    if (val instanceof S) {
      return val;
    }
    return new S(val);
  };

}).call(this);
