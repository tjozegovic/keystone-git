module.exports = class Timer
  constructor: (@fn, @interval, @opts = {}) ->
    @reset() unless @opts.delay

  clear: ->
    @cleared = yes
    clearTimeout @id unless @called

  start: ->
    return unless @id?
    @reset()

  reset: (immediate) ->
    clearTimeout @id if @id?

    @called = @cleared = no
    @id = setTimeout =>
      @called = yes
      @fn()
    , if immediate then 0 else @interval
