(function() {
  var Timer;

  module.exports = Timer = (function() {
    function Timer(fn, interval, opts) {
      this.fn = fn;
      this.interval = interval;
      this.opts = opts != null ? opts : {};
      if (!this.opts.delay) {
        this.reset();
      }
    }

    Timer.prototype.clear = function() {
      this.cleared = true;
      if (!this.called) {
        return clearTimeout(this.id);
      }
    };

    Timer.prototype.start = function() {
      if (this.id == null) {
        return;
      }
      return this.reset();
    };

    Timer.prototype.reset = function(immediate) {
      if (this.id != null) {
        clearTimeout(this.id);
      }
      this.called = this.cleared = false;
      return this.id = setTimeout((function(_this) {
        return function() {
          _this.called = true;
          return _this.fn();
        };
      })(this), immediate ? 0 : this.interval);
    };

    return Timer;

  })();

}).call(this);
