fs = require 'fs'
path = require 'path'

module.exports.lockfile = lockfile = (root) ->
  # assume this is being called from a main module (actionqueue/main, pipeline/main, etc.)
  # create a file in the application directory .lock, write the pid to it
  # update the modified time every so often
  do (name = '.lock') ->
    moddir = root or path.dirname module.parent.filename
    name = path.join moddir, '..', name

    fs.writeFileSync name, process.pid
    setInterval ->
      stat = fs.statSync name
      fs.utimesSync name, stat.atime, new Date()
    , 5000
    .unref()

module.exports.get_name = get_name = ->
  {filename} = require.main
  dirname = path.dirname filename

  for i in dirname.split(path.sep).reverse()
    return i unless i in ['bin', 'lib', 'src']
