(function() {
  var fs, get_name, lockfile, path;

  fs = require('fs');

  path = require('path');

  module.exports.lockfile = lockfile = function(root) {
    return (function(name) {
      var moddir;
      moddir = root || path.dirname(module.parent.filename);
      name = path.join(moddir, '..', name);
      fs.writeFileSync(name, process.pid);
      return setInterval(function() {
        var stat;
        stat = fs.statSync(name);
        return fs.utimesSync(name, stat.atime, new Date());
      }, 5000).unref();
    })('.lock');
  };

  module.exports.get_name = get_name = function() {
    var dirname, filename, i, j, len, ref;
    filename = require.main.filename;
    dirname = path.dirname(filename);
    ref = dirname.split(path.sep).reverse();
    for (j = 0, len = ref.length; j < len; j++) {
      i = ref[j];
      if (i !== 'bin' && i !== 'lib' && i !== 'src') {
        return i;
      }
    }
  };

}).call(this);
