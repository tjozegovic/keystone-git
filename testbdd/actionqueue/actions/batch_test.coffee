fs = require 'fs'
path = require 'path'

batcher = require '../../../actionqueue/src/actions/batch'

describe 'actionqueue/actions', ->
  describe 'batches', ->
    before (done) ->
      @stream = -> fs.createReadStream path.join __dirname, 'fixtures/test.csv'

      fs.readFile path.join(__dirname, 'fixtures/test.csv'), (err, file) =>
        batches = factory.use 'client/batches'
        batches.save name: 'batch', file: file, (err, body) =>
          @batch = body.id
          done()

    describe 'parser', ->
      it 'csv parser can parse csv', (done) ->
        batcher.parse @stream()
          .on 'record', (record, idx) ->
            if idx is 0
              expect(record.agent).to.equal 'nick.peeples@softvu.com'
              expect(record.to).to.equal 'nick.peeples@gmail.com'
              expect(record.recipient.name.first).to.equal 'Nick'
              expect(record.recipient.name.last).to.equal 'Peeples'
              expect(record.recipient.phones.primary).to.equal '(816) 645-9566'
          .on 'error', (err) ->
            done err
          .on 'end', (rows) ->
            expect(rows).to.equal 2
            done()

    describe 'batcher', ->
      it 'should call correct things', (done) ->
        emails = []
        events = []

        ctx =
          id: '48c5bc219e7f4335bb50ca2806305c80'
          use: factory.use.bind factory

          events:
            emit: (event) -> events.push event
          queues:
            push: (queue, data, cb) ->
              emails.push data if queue is 'email'
              process.nextTick cb

        batcher ctx, batch: @batch, client: factory.client, (err) =>
          expect(emails[0].batch).to.equal @batch
          expect(emails[0].client).to.equal factory.client
          expect(emails[0].to).to.equal 'nick.peeples@gmail.com'
          expect(emails[0].jobid).to.equal ctx.id
          done()
