_ = require 'lodash'
async = require 'async'
moment = require 'moment-timezone'

dailyvu = require '../../../actionqueue/src/actions/generate_dailyvus'

describe 'actionqueue/actions', ->
  describe.skip 'dailyvu', ->
    before (done) ->
      @activity = [ message: '1' ]
      @senders = [
        {_id: 1}
        {_id: 2, dailyvu: on: no}
      ]

      [@events, @jobs, @queries] = ([] for x in [0..3])

      @ctx =
        use: factory.use.bind factory
        db:
          jobs:
            set: (id, data, cb) =>
              @jobs.push id: id, data: data
              cb?()
          get_client: (client) =>
            data:
              view: (doc, view, query, cb) =>
                cb null, rows: @senders.map (sender) -> id: sender._id, doc: sender
            messages:
              view: (doc, view, query, cb) =>
                @queries.push doc: doc, view: view, query: query
                cb null, rows: @activity
          softvu:
            view: (doc, view, query, cb) =>
              cb null, rows: [
                {id: 1, doc: @client}
              ]
        events:
          emit: (type, client, data, cb) =>
            @events.push type: type, client: client, data: data
            cb?()

      @data =

      @client =
        _id: 1
        dailyvu: on: yes

      done()

    describe 'account', ->
      it 'skip dailyvu', (done) ->
        clients = factory.use 'core/clients'
        clients.get factory.client, (err, client) ->
          client.dailyvu ?= {}
          client.dailyvu.on = no
          clients.save client, ->
            dailyvu @ctx, @data, =>
              expect(@events[0].type).to.equal 'dailyvu.skipped'
              expect(@events[0].client).to.equal factory.client
              done()

      it 'sends to right senders', (done) ->
        dailyvu @ctx, @data, =>
          test.equals @events.length, 2
          test.equals _.find(@events, type: 'dailyvu.scheduled').data.sender, 1
          test.equals _.find(@events, type: 'dailyvu.skipped').data.sender, 2
          done()

    describe 'sender', ->
      it 'skip dailyvu', (done) ->
        @senders = [dailyvu: on: no]
        dailyvu @ctx, @data, =>
          test.equals @events[0].type, 'dailyvu.skipped'
          test.equals @events[0].client, '1'
          done()

      it 'skip dailyvu no activity', (done) ->
        @activity = []
        dailyvu @ctx, @data, =>
          test.equals @events[0].type, 'dailyvu.skipped'
          test.equals @events[0].client, '1'
          done()

      it 'insert job without date (yesterday)', (done) ->
        sender =
          _id: 's1'
          timezone: 'America/Chicago'
          dailyvu:
            on: yes
            time: '09:00'

        yesterdaychicago = moment.tz('America/Chicago').subtract 1, 'day'
        nineamchicago = moment.tz('America/Chicago').startOf('day').add 9, 'hours'
        @ctx.date = yesterdaychicago
        dailyvu.get_activity @ctx, @client, sender, (err, activity) =>
          dailyvu.create_job @ctx, @client, sender, activity, =>
            test.equals @queries[0].query.startkey[1], yesterdaychicago.clone().startOf('day').toISOString()
            test.equals @queries[0].query.endkey[1], yesterdaychicago.clone().endOf('day').toISOString()

            test.equals @jobs[0].data.at, nineamchicago.toISOString()
            test.equals @jobs[0].data.data.sender, 's1'
            test.equals @jobs[0].data.data.emails.length, 1

            test.equals @events[0].type, 'dailyvu.scheduled'

            done()
