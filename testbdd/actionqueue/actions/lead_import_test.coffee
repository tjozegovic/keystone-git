fs = require 'fs'
path = require 'path'

parser = require '../../../actionqueue/src/actions/parse'
importer = require '../../../actionqueue/src/actions/import'

describe 'actionqueue/actions', ->
  describe 'leads', ->
    describe 'parse import', ->
      before (done) ->
        @event = {}
        @ctx =
          logger:
            warn: ->
            error: ->
            info: ->
            silly: ->
          events:
            emit: (event, result, cb) =>
              # TODO better way to handle this? Just need to use for checking lead.import.finished
              @event[event] = result
              cb?()
          use: factory.use.bind factory

        [attributes, statuses, @uploads, @imports, @leads] = factory.use 'client/attributes', 'client/statuses', 'client/uploads', 'client/imports', 'client/leads'

        async.auto
          attribute: (cb) ->
            attributes.all cb
          import: ['attribute', (cb, results) ->
            if results.attribute.length
              fs.readFile path.join(__dirname, 'fixtures/lead_import_test2.csv'), (err, file) =>
                @uploads = factory.use 'client/uploads'
                @uploads.save type: 'lead.import', status: 'pending', file, cb
            else
              fs.readFile path.join(__dirname, 'fixtures/lead_import_test.csv'), (err, file) =>
                @uploads = factory.use 'client/uploads'
                @uploads.save type: 'lead.import', status: 'pending', file, cb
          ]
          attribute1: ['attribute', (cb, results) =>
            attributes.save
              name: 'productleadimport'
              values:
                1:
                  name: 'Purchase',
                  id: 1
                2:
                  name: 'Refinance',
                  id: 2
            , cb
          ]
          attribute2: ['attribute', 'attribute1', (cb, results) =>
            attributes.save
              name: 'leadsourceleadimport'
              values:
                3:
                  name: 'Source 1',
                  id: 3
                4:
                  name: 'Source 2',
                  id: 4
            , cb
          ]
          status: ['attribute', 'attribute1', 'attribute2', (cb, results) ->
            statuses.save name: 'LeadImport', aliases: ['LeadImportAlias'], segments: [
              {id: '1'
              products: [
                [results.attribute1.id + '/1'],
                [results.attribute1.id + '/2']
              ]
              steps: [
                { id: '11', message: 'm1', delay: units: 'day', value: 0 }
              ]},
              {id: '2'
              products: [
                [results.attribute2.id + '/3'],
                [results.attribute2.id + '/4']
              ]
              steps: [
                { id: '11', message: 'm1', delay: units: 'day', value: 0 }
              ]}
            ], cb
          ]
        , (err, results) =>
          _.assign @, results
          at = new Date()
          parser @ctx, import: @import.id, import_type: 'lead', at: at, =>
            date = moment(at).format('YYYYMMDDHHmmss')
            importer @ctx, import: @import.id, import_type: 'lead', import_table: "uploads_lead_#{date}", =>
              done()

      it 'should parse the lead import', (done) ->
        expect(@event['import.parsed']).to.exist
        done()

      it 'should create a new uploads import table', (done) ->
        @imports.import_lines table: @event['import.parsed'].import_table, limit: 20, (err, results) ->
          expect(results.length).to.eql 5
          done()

      # describe 'import leads', ->
      it 'should finish lead import', (done) ->
        # TODO is this enough of a test??
        expect(@event['lead.import.finished']).to.exist
        done()

      it 'should produce 3 leads', (done) ->
        # TODO check leads in db for leads created??
        # TODO run leads through again to check for updated??
        expect(@event['lead.import.finished'].stats.imported).to.equal 3
        done()

      it 'should find a new lead by external id', (done) ->
        @leads.getByExternalId 'Velocify', '0001', (err, result) ->
          expect(result.recipient.email).to.eql 'nick.peeples@softvu.com'
          expect(result.status).to.eql 'LeadImport'
          expect(result.active).to.be.true
          done()

      it 'should have uploaded lead.import.error', (done) ->
        @uploads.get @event['lead.import.finished'].errorId, (err, result) =>
          expect(result.type).to.eql 'lead.import.error'
          expect(result.importId).to.eql @event['import.parsed'].import
          done()

      it 'should handle 2 errors properly', (done) ->
        @imports.import_lines table: @event['import.parsed'].import_table, status: 'errored', limit: 20, order: true, (err, results) ->
          expect(results[0].doc.err).to.eql { message: 'Leadimport is not a valid status name' }
          expect(results[1].doc.err).to.eql 'Lead Attribute value (leadsourceleadimport, Hello) could not be mapped.'
          done()
