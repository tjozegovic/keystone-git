fs = require 'fs'
path = require 'path'

parser = require '../../../actionqueue/src/actions/parse'
importer = require '../../../actionqueue/src/actions/import'

describe 'actionqueue/actions', ->
  describe 'opt-out import', ->
    before (done) ->
      @event = {}
      @ctx =
        logger:
          warn: ->
          error: ->
          info: ->
          silly: ->
        events:
          emit: (event, result, cb) =>
            # TODO better way to handle this? Just need to use for checking optout.import.finished
            @event[event] = result
            cb?()
        use: factory.use.bind factory
      @req =
        headers: {}
        query: {}
        originalUrl: '/api/optout/upload'
        ip: '::1'
        session:
          username: "root@softvu.com"
          client_id: factory.client.id

      [@uploads, @optouts, @imports] = factory.use 'client/uploads', 'core/optouts', 'client/imports'

      async.auto
        import: (cb, results) =>
          fs.readFile path.join(__dirname, 'fixtures/optout_test.csv'), (err, file) =>
            @uploads.save type: 'optout.import', status: 'pending', file, cb
      , (err, results) =>
        _.assign @, results
        at = new Date()
        parser @ctx, import: @import.id, import_type: 'optout', at: at, =>
          date = moment(at).format('YYYYMMDDHHmmss')
          importer @ctx, client: factory.client, user: 'root@softvu.com', import: @import.id, import_type: 'optout', import_table: "uploads_optout_#{date}", =>
            done()

    it 'should parse the optout import', (done) ->
      expect(@event['import.parsed']).to.exist
      done()

    it 'should create a new uploads import table', (done) ->
      @imports.import_lines table: @event['import.parsed'].import_table, limit: 20, (err, results) ->
        expect(results.length).to.eql 5
        done()

    it 'should finish optout import', (done) ->
      # TODO is this enough of a test??
      expect(@event['optout.import.finished']).to.exist
      done()

    it 'should opt-out 3 email addresses', (done) ->
      # TODO check opt-outs in db for opt-outs created??
      expect(@event['optout.import.finished'].stats.imported).to.equal 3
      done()

    it 'should have uploaded optout.import.error', (done) ->
      @uploads.get @event['optout.import.finished'].errorId, (err, result) =>
        @errors = result
        expect(result.type).to.eql 'optout.import.error'
        expect(result.importId).to.eql @event['import.parsed'].import
        done()

    it 'should handle 2 failures', (done) ->
      expect(@event['optout.import.finished'].stats.errors).to.equal 2
      expect(@event['optout.import.finished'].errorId).to.be.num
      expect(@errors.file.toString()).to.eql 'optout,error\nchris.fisher,row import failed,"invalid email address"\njustin.pacubas.softvu.com,row import failed,"invalid email address"'
      done()
