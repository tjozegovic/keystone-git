any_leads = require '../../../eventhose/src/any/leads'

describe 'Eventhose', ->
  describe 'Leads', ->
    before 'data add', (done) ->
      @client = factory.client
      [statuses, @leads, attributes, jobs] = factory.use 'client/statuses', 'client/leads', 'client/attributes', 'core/jobs'

      async.auto
        attribute: (cb) ->
          attributes.save
            name: '1'
            values:
              '1':
                name: '1'
          , cb
        lead: ['initialStatus', (cb, results) =>
          @leads.save
            $status: results.initialStatus.id
            agent: 'agent'
            attributes: {'1': '1'}
            recipient: { email: 'remail' }
          , cb
        ]
        lead1: ['initialStatus', (cb, results) =>
          @leads.save { $status: results.initialStatus.id, agent: 'agent', recipient: { email: 'remail'} }
          , cb
        ]
        initialStatus: ['attribute', (cb, results) ->
          statuses.save segments: [
            id: '1'
            products: [
              [results.attribute.id + '/1']
            ]
            steps: [
              { id: '11', message: 'm1', delay: units: 'day', value: 0 }
            , { id: '12', message: 'm2', delay: units: 'day', value: 2 }
            , { id: '13', message: 'm3', delay: units: 'day', value: 5 }
            , { id: '14', message: 'm4', delay: units: 'day', value: 8 }
            ]
          ], cb
        ]
        newStatus: (cb) ->
          statuses.save segments: [
            id: '2'
            products: [
              ['1/1']
            ]
            steps: [
              { id: '15', message: 'm1', delay: units: 'day', value: 0 }
            ]
          ], cb
        job: (cb) ->
          jobs.save
            action: 'test'
            status: 'pending'
          , cb
      , (err, results) =>
        _.assign @, results
        done()

      @ctx =
        logger:
          warn: ->
        events:
          emit: (type, client, event, cb) =>
            @events.emit type, _.assign event, client: client
            cb?()
        use: factory.use.bind factory

    beforeEach ->
      @events = new EventEmitter

    it 'should create a segmented lead', (done) ->
      @events.on 'lead.segmented', (obj) =>
        expect(obj.status).to.equal @initialStatus.id
        expect(obj.segment).to.equal '1'
        expect(obj.initial).to.be.true
        done()

      now = moment()
      event = client: @client, lead: @lead.id, at: now

      any_leads['#.lead.created'].call @ctx, event, (err) =>
        @leads.get event.lead, (err, body) ->
          expect(moment(body.segmented).isSame(now)).to.be.true

    it 'removes segmented on status.changed', (done) ->
      event = client: @client, lead: @lead.id, status: @newStatus.id

      any_leads['#.lead.status.changed'].call @ctx, event, (err) =>
        @leads.get event.lead, (err, body) ->
          expect(body.segmented).to.not.exist
          done()

    it 'updates the lead agent', (done) ->
      event = client: @client, lead: @lead.id, agent: 'newAgent'

      any_leads['#.lead.agent.changed'].call @ctx, event, (err) =>
        @leads.get event.lead, (err, body) ->
          expect(body.agent).to.equal 'newAgent'
          done()

    it 'schedules a drip', (done) ->
      event = client: @client, lead: @lead.id, step: '11', jobid: @job.id

      any_leads['#.drip.scheduled'].call @ctx, event, (err) =>
        @leads.get event.lead, (err, body) ->
          expect(body.$job).to.equal event.jobid
          done()
