_ = require 'lodash'
async = require 'async'
moment = require 'moment'
{EventEmitter} = require 'events'

logger = require '../../../eventhose/src/any/logger'

describe 'Eventhose', ->
  describe 'Logger', ->
    before 'data add', (done) ->
      @client = factory.client

      @repos =
        emails: factory.use 'client/emails'
        events: factory.use 'client/events'

      async.parallel [
        (cb) =>
          @repos.emails.save
            type: 'direct'
          , cb
        (cb) =>
          @repos.emails.save
            type: 'direct'
            status: id: '23'
          , cb
      ], (err, results) =>
        @emails = results
        done err

    beforeEach ->
      @events = new EventEmitter
      @ctx =
        events:
          emit: (type, client, event) =>
            @events.emit type, _.assign event, client: client

        use: factory.use.bind factory

    describe 'email updates', ->
      it 'should not touch email', (done) ->
        event = client: @client, email: @emails[0].id, type: 'email.somethingelse'

        @repos.emails.get @emails[0].id, (err, email) =>
          updated = email.updated
          logger['#'].call @ctx, event, (err) =>
            @repos.emails.get @emails[0].id, (err, email) =>
              expect(err).to.not.exist
              expect(updated).to.eql email.updated
              done()

    describe 'event updates', ->
      before (done) ->
        @repos.emails.save
          type: 'direct'
          status: id: '23'
        , (err, {id: @email_id}) =>
          event = client: @client, email: @emails[1].id, type: 'email.clicked'
          logger['#'].call @ctx, event, done

      it 'should change clicked for email reporting', (done) ->
        @repos.emails.get @emails[1].id, (err, email) =>
          expect(err).to.not.exist
          expect(email.clicked).to.have.length 1
          done()

      it 'should add first to event', (done) ->
        @repos.emails.get @emails[1].id, (err, email) =>
          @repos.events.get email.clicked[0].event, (err, event) ->
            expect(event.first).to.be.true
            expect(event.status).to.eql '23'
            done()
