_ = require 'lodash'
async = require 'async'
moment = require 'moment'
chaiDatetime = require 'chai-datetime'
{EventEmitter} = require 'events'

segmented = require '../../../eventhose/src/any/segmented'

describe 'Eventhose', ->
  describe 'Segmented', ->
    before 'context setup', (done) ->
      @events = new EventEmitter
      @client = timezone: 'America/Los_Angeles'

      @ctx =
        # events: @events
        get_client_info: (cb) =>
          process.nextTick => cb null, @client
        logger:
          log: ->
          info: ->
          debug: ->
        use: factory.use.bind factory

      done()

    describe '#.email.delivered', ->
      before (done) ->
        [emails, @leads, @statuses] = @ctx.use 'client/emails', 'client/leads', 'client/statuses'

        async.auto
          status: (cb) =>
            @statuses.save
              segments: [
                id: 'segment'
                steps: [
                  { id: 'step1', notify: yes, delay: value: 0, units: 'hour', delayInHours: 0}
                  { id: 'step2', notify: yes, delay: value: 4, units: 'hour', delayInHours: 4}
                  { id: 'step3', notify: yes, delay: value: 1, units: 'day', delayInHours: 24}
                  { id: 'step4', notify: yes, delay: value: 2, units: 'day', delayInHours: 48}
                ]
              ]
            , (err, body) =>
              cb err, body?.id
          badsortstatus: (cb) =>
            @statuses.save
              segments: [
                id: 'segment'
                steps: [
                  { id: 'step2', notify: yes, delay: value: 4, units: 'hour', delayInHours: 4}
                  { id: 'step1', notify: yes, delay: value: 1, units: 'hour', delayInHours: 1}
                  { id: 'step4', notify: yes, delay: value: 2, units: 'day', delayInHours: 48}
                  { id: 'step3', notify: yes, delay: value: 1, units: 'day', delayInHours: 24}
                ]
              ]
            , (err, body) =>
              cb err, body?.id
          statusWithInactiveSteps: (cb) =>
            @statuses.save
              segments: [
                id: 'segment'
                steps: [
                  { id: 'step1', active: false, notify: yes, delay: value: 0, units: 'hour', delayInHours: 0}
                  { id: 'step2', notify: yes, delay: value: 4, units: 'hour', delayInHours: 4}
                  { id: 'step3', notify: yes, delay: value: 1, units: 'day', delayInHours: 24}
                  { id: 'step4', notify: yes, delay: value: 2, units: 'day', delayInHours: 48}
                ]
              ]
            , (err, body) =>
              cb err, body?.id
          statusWithDelayedFirstStep: (cb) =>
            @statuses.save
              segments: [
                id: 'segment'
                steps: [
                  { name: 'step1', id: 'b907ef876f434dc9bfbceee42d6091a5', notify: yes, delay: value: 1, units: 'day', delayInHours: 24}
                  { name: 'step2', id: '395a528897914ff4a2424a02baca47a6', notify: yes, delay: value: 2, units: 'day', delayInHours: 48}
                  { name: 'step3', id: 'c37b7a30e59242a2a2fae59b428e58c0', notify: yes, delay: value: 3, units: 'day', delayInHours: 72}
                  { name: 'step4', id: 'f28de48154f34f879c721637c726c651', notify: yes, delay: value: 7, units: 'day', delayInHours: 168}
                ]
              ]
            , (err, body) =>
              cb err, body?.id
          lead: ['status', (cb, {status}) =>
            @leads.save agent: 'sender@none', status: 'status', $status: status, recipient: email: 'recipient@none', (err, body) ->
              cb err, body?.id
          ]
          badlead: ['badsortstatus', (cb, {badsortstatus}) =>
            @leads.save agent: 'sender@none', status: 'status', $status: badsortstatus, recipient: email: 'recipient@none', (err, body) ->
              cb err, body?.id
          ]
          oldlead: ['status', (cb, {status}) =>
            @leads.save created: moment().subtract(10, 'days'), agent: 'sender@none', status: 'status', $status: status, recipient: email: 'recipient@none', (err, body) ->
              cb err, body?.id
          ]
          leadInactiveStepStatus: ['status', (cb, {statusWithInactiveSteps}) =>
            @leads.save agent: 'sender@none', status: 'status', $status: statusWithInactiveSteps, recipient: email: 'recipient@none', (err, body) ->
              cb err, body?.id
          ]
          badleadnosegment: ['badsortstatus', (cb, {badsortstatus}) =>
            @leads.save agent: 'sender@none', status: 'status', $status: badsortstatus, recipient: email: 'recipient@none', segmented: null, (err, body) ->
              cb err, body?.id
          ]
          triggerlead: ['statusWithDelayedFirstStep', (cb, {statusWithDelayedFirstStep}) =>
            @leads.save agent: 'sender@none', status: 'status', $status: statusWithDelayedFirstStep, recipient: email: 'recipient@none', (err, body) ->
              cb err, body?.id
          ]
          nostep: ['status', (cb, {status}) ->
            emails.save
              sender: email: 'sender@none'
              status: status
            , cb
          ]
          nolead: ['status', (cb, {status}) ->
            emails.save
              sender: email: 'sender@none'
              status: status
              step: 'step'
            , cb
          ]
          withlead: ['lead', 'status', (cb, {lead, status}) ->
            emails.save
              sender: email: 'sender@none'
              status: status
              segment: 'segment'
              step: 'step1'
              lead: lead
            , cb
          ],
          withbadlead: ['badlead', 'status', (cb, {badlead, status}) ->
            emails.save
              sender: email: 'sender@none'
              status: status
              segment: 'segment'
              step: 'step2'
              lead: badlead
            , cb
          ],
          withleadtriggered: ['triggerlead', 'status', (cb, {triggerlead, status}) ->
            emails.save
              sender: email: 'sender@none'
              status: status
              segment: 'segment'
              step: name: 'step1', id: 'b907ef876f434dc9bfbceee42d6091a5'
              lead: triggerlead
              triggered: yes
            , cb
          ]
        , (err, results) =>
          @emails = results
          done err

      beforeEach ->
        @ctx.events = @events = new EventEmitter

      it 'ignore message with no step', (done) ->
        event = client: 'c', email: @emails.nostep.id

        @events.on 'drip.scheduled', ->
          throw 'fail'

        segmented['#.email.delivered'].call @ctx, event, (err) ->
          expect(err).to.not.exist
          process.nextTick done

      it 'ignore message with no lead', (done) ->
        event = client: 'c', email: @emails.nolead.id

        @events.on 'drip.scheduled', ->
          throw 'fail'

        segmented['#.email.delivered'].call @ctx, event, (err) ->
          expect(err).to.not.exist
          process.nextTick done

      it 'queue the next drip', (done) ->
        delivered = moment.utc().add(1, 'second')
        event = client: 'c', at: delivered.format(), email: @emails.withlead.id
        @leads.get @emails.lead, (err, lead) =>
          lead.segmented = moment.utc()
          @leads.save lead, =>
            @events.on 'drip.scheduled', (_, message) ->
              expect(message.step).to.be.equal 'step2'
              expect(message.delay.units).to.be.equal 'hour'
              expect(message.delay.value).to.be.equal 4
              done()

            segmented['#.email.delivered'].call @ctx, event, (err) ->
              expect(err).to.not.exist

      it 'queue the next drip ($status)', (done) ->
        delivered = moment.utc().add(1, 'second')
        event = client: 'c', at: delivered.format(), email: @emails.withlead.id
        @leads.get @emails.lead, (err, lead) =>
          lead.segmented = moment.utc()
          lead.$status = @emails.status
          @leads.save lead, =>
            @events.on 'drip.scheduled', (_, message) ->
              expect(message.step).to.be.equal 'step2'
              expect(message.delay.units).to.be.equal 'hour'
              expect(message.delay.value).to.be.equal 4
              done()

            segmented['#.email.delivered'].call @ctx, event, (err) ->
              expect(err).to.not.exist

      it 'queue the right drip', (done) ->
        event = client: 'c', email: @emails.withbadlead.id
        @leads.get @emails.badlead, (err, lead) =>
          lead.segmented = moment.utc().subtract 8, 'hours'
          @leads.save lead, =>
            @events.on 'drip.scheduled', (_, message) ->
              expect(message.step).to.be.equal 'step3'
              expect(message.delay.value).to.be.equal 1
              expect(message.delay.units).to.be.equal 'day'
              done()

            segmented['#.email.delivered'].call @ctx, event, (err) ->
              expect(err).to.not.exist

      it 'leads segmented to first step', (done) ->
        event = client: 'c', at: moment().add(-1, 'seconds').format(), lead: @emails.lead, status: @emails.status, segment: 'segment'
        @events.on 'drip.scheduled', (_, message) ->
          expect(message.step).to.be.equal 'step1'
          done()

        segmented['#.lead.segmented'].call @ctx, event, (err) ->
          expect(err).to.not.exist

      it 'leads unsegmented to first step', (done) ->
        event = client: 'c', lead: @emails.badleadnosegment, status: @emails.badsortstatus, segment: 'segment'

        @events.on 'drip.scheduled', (_, message) ->
          expect(message.step).to.be.equal 'step1'
          done()

        segmented['#.lead.segmented'].call @ctx, event, (err) ->
          expect(err).to.not.exist

      it 'drips time out', (done) ->
        event = client: 'c', email: @emails.withlead.id
        @leads.get @emails.lead, (err, lead) =>
          lead.segmented = moment.utc().subtract 3, 'days'
          @leads.save lead, =>
            @events.on 'drip.scheduled', ->
              throw 'drip.scheduled snot hot be raised'

            @events.on 'lead.timedout', (_, message) =>
              expect(message.lead).to.be.equal @emails.lead
              expect(message.status).to.be.equal 'status'
              expect(message.segment).to.be.equal 'segment'
              done()

            segmented['#.email.delivered'].call @ctx, event, (err) ->
              expect(err).to.not.exist

      it 'leads can have a segmented time, take 2', (done) ->
        event = client: 'c', lead: @emails.lead, status: @emails.status, segment: 'segment'
        @leads.get @emails.lead, (err, lead) =>
          lead.segmented = moment().add -25, 'h'
          @leads.save lead, =>
            @events.on 'drip.scheduled', (_, message) ->
              expect(message.step).to.equal 'step4'
              expect(moment(new Date message.at).isSame moment().add(23, 'h'), 'minute').to.be.true
              done()

            segmented['#.lead.segmented'].call @ctx, event, (err) ->
              expect(err).to.not.exist

      # TODO annoying moment deprecation warning is thrown
      it.skip 'leads can have a segmented time, take 3 (velocify)', (done) ->
        event = client: 'c', lead: @emails.lead, status: @emails.status, segment: 'segment'
        @client.timezone = 'America/Los_Angeles'

        @leads.get @emails.lead, (err, lead) =>
          # LA is 2 hours before CDT, step4 has a 2 day delay
          # 23hrs ago, +2hrs for TZ change, means that the next message should go out in 23hrs (23 + 2 + 23) = 48
          lead.segmented = moment().tz('America/Los_Angeles').add(-23, 'h').format 'L LT'
          @leads.save lead, =>
            @events.on 'drip.scheduled', (_, message) ->
              expect(message.step).to.equal 'step4'
              expect moment(new Date message.at).isSame moment().add(23, 'h'), 'minute'
              done()

            segmented['#.lead.segmented'].call @ctx, event, (err) ->
              expect(err).to.not.exist

      it 'queues the correct drip after after being resegmented', (done) ->
        event = client: 'c', lead: @emails.oldlead, status: @emails.badsortstatus, segment: 'segment'

        @events.on 'drip.scheduled', (_, message) ->
          expect(message.step).to.be.equal 'step1'
          expect(moment(new Date message.at).isSame moment().add(1, 'h'), 'minute').to.be.true
          done()

        segmented['#.lead.segmented'].call @ctx, event, (err) ->
          expect(err).to.not.exist

      it 'queues the correct drip - skip over inactive first step', (done) ->
        event = client: 'c', lead: @emails.leadInactiveStepStatus, status: @emails.statusWithInactiveSteps, segment: 'segment'

        @events.on 'drip.scheduled', (_, message) ->
          expect(message.step).to.be.equal 'step2'
          done()

        segmented['#.lead.segmented'].call @ctx, event, (err) ->
          expect(err).to.not.exist

      it 'queues the right drip when previous email is triggered', (done) ->
        # first drip is originally scheduled for 24h, fake triggering e-mail at 6h
        delivered = moment().add(6, 'h')
        event = client: 'c', at: delivered.format(), email: @emails.withleadtriggered.id

        @leads.get @emails.triggerlead, (err, lead) =>
          lead.segmented = moment()
          @leads.save lead, =>
            @events.on 'drip.scheduled', (_, message) ->
              expect(message.step).to.be.equal '395a528897914ff4a2424a02baca47a6'
              done()

            segmented['#.email.delivered'].call @ctx, event, (err) ->
              expect(err).to.not.exist
