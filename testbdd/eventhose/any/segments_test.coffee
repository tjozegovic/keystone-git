_ = require 'lodash'
async = require 'async'
moment = require 'moment'
{EventEmitter} = require 'events'

segments = require '../../../eventhose/src/any/segments'

describe 'Eventhose', ->
  describe 'Segments', ->
    @status = {}
    @leads = []

    beforeEach ->
      @events = new EventEmitter
      @ctx =
        events:
          emit: (type, client, event) =>
            @events.emit type, _.assign event, client: client

        use: factory.use.bind factory

    before 'data add', (done) ->
      @client = factory.client
      # db = factory.single()
      # db.query 'TRUNCATE leads'
      # db.query 'TRUNCATE jobs'

      [jobs, statuses, leads] = factory.use 'core/jobs', 'client/statuses', 'client/leads'

      async.auto
        status: (cb) =>
          statuses.save segments: [
            id: '1'
            steps: [
              { id: '11', message: 'm1', delay: units: 'day', value: 0 }
            , { id: '12', message: 'm2', delay: units: 'day', value: 2 }
            , { id: '13', message: 'm3', delay: units: 'day', value: 5 }
            , { id: '14', message: 'm4', delay: units: 'day', value: 8 }
            ]
          ], (err, body) =>
            return cb err if err?
            cb null, body.id # @status = body.id
        leads: ['status', (cb, results) ->
          async.parallel
            'lead1': (cb) ->
              leads.save
                name: 'lead1'
                agent: 'agent'
                $status: results.status
                segment: '1'
                recipient: { email: 'remail' }
                segmented: moment().subtract 10, 'days'
              , cb
            'lead2': (cb) ->
              leads.save
                name: 'lead2'
                agent: 'agent'
                $status: results.status
                segment: '1'
                recipient: { email: 'remail' }
                segmented: moment().subtract 6, 'days'
              , cb
            'lead3': (cb) ->
              leads.save
                name: 'lead3'
                agent: 'agent'
                $status: results.status
                segment: '1'
                recipient: { email: 'remail' }
                segmented: moment().subtract 3, 'days'
              , cb
            'lead4': (cb) ->
              leads.save
                name: 'lead4'
                agent: 'agent'
                $status: results.status
                segment: '1'
                recipient: { email: 'remail' }
                segmented: moment().subtract 1, 'days'
              , cb
          , cb
        ]
        jobs: ['leads', (cb, results) ->
          async.map [
              { action: 'email', data: { lead: results.leads['lead2'].id }, at: moment().add 2, 'days' }
            , { action: 'email', data: { lead: results.leads['lead3'].id }, at: moment().add 2, 'days' }
            , { action: 'email', data: { lead: results.leads['lead4'].id }, at: moment().add 1, 'days' }
          ], jobs.save.bind(jobs), cb
        ]
        leadjobs: ['jobs', (cb, results) ->
          async.each results.jobs, (job, cb) ->
            jobs.get job.id, (err, job) ->
              leads.get job.data.lead, (err, lead) ->
                lead.$job = job.id
                leads.save lead, (err, body) ->
                  cb err
          , cb
        ]
      , (err, results) =>
        _.assign @, results
        done err

    describe '#.status.updated', ->
      it 'should have no changes', (done) ->
        event = client: @client, status: @status

        @events.on 'drip.canceled', -> expect no
        @events.on 'drip.scheduled', -> expect no

        segments['#.status.updated'].call @ctx, event, (err) ->
          expect(err).to.not.exist
          done()

      it 'should add an additional message', (done) ->
        event = client: @client, status: @status

        @events.on 'drip.scheduled', (event) =>
          expect(event.client).to.equal @client
          expect(event.lead).to.equal @leads['lead1'].id
          done()

        statuses = factory.use 'client/statuses'
        statuses.get @status, (err, status) =>
          status.segments[0].steps.push { delay: units: 'day', value: 12 }
          statuses.save status, (err) =>
            return done err if err?

            segments['#.status.updated'].call @ctx, event, (err) ->
              expect(err).to.not.exist

      it 'should cancel the existing message and change to an earlier delay value', (done) ->
        event = client: @client, status: @status

        @events.on 'drip.canceled', (event) =>
          expect(event.client).to.equal @client
          expect(event.lead).to.equal @leads['lead2'].id

        @events.on 'drip.scheduled', (event) =>
          expect(event.client).to.equal @client
          expect([@leads['lead1'].id, @leads['lead2'].id]).to.include event.lead

        statuses = factory.use 'client/statuses'
        statuses.get @status, (err, status) =>
          status.segments[0].steps[3].delay.value = 7
          statuses.save status, (err) =>
            return done err if err?

            segments['#.status.updated'].call @ctx, event, (err) ->
              process.nextTick done

      it 'should cancel the existing message and change to a later delay value', (done) ->
        event = client: @client, status: @status

        @events.on 'drip.canceled', (event) =>
          expect(event.client).to.equal @client
          expect([@leads['lead1'].id, @leads['lead2'].id, @leads['lead3'].id]).to.include event.lead

        @events.on 'drip.scheduled', (event) =>
          expect(event.client).to.equal @client
          expect([@leads['lead1'].id, @leads['lead2'].id, @leads['lead3'].id]).to.include event.lead

        statuses = factory.use 'client/statuses'
        statuses.get @status, (err, status) =>
          status.segments[0].steps[2].delay.value = 7
          statuses.save status, (err) =>
            return done err if err?

            segments['#.status.updated'].call @ctx, event, (err) ->
              process.nextTick done
