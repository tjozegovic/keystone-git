_ = require 'lodash'
async = require 'async'
moment = require 'moment'
uuid = require 'uuid'
{EventEmitter} = require 'events'

vunotifications = require '../../../eventhose/src/any/vunotifications'

process.on 'uncaughtException', (err) ->
  console.error err.stack

#TODO After discussing with Nick and doing some manual testing, these tests may be broken...
describe 'Eventhose', ->
  describe 'VuNotifications', ->
    @emails = {}
    segment_id = null
    step_id = null

    before 'data add', (done) ->
      @client = factory.client

      [statuses, messages, emails, leads, senders] = factory.use 'client/statuses', 'client/messages', 'client/emails', 'client/leads', 'client/senders'
      async.auto
        messages: (cb) =>
          async.parallel
            'notify': (cb) ->
              messages.save notify: yes, cb
            'nonotify': (cb) ->
              messages.save {}, cb
          , cb
        sender: (cb) ->
          senders.save
            firstname: "SoftVu"
            lastname: "Agent"
            email: "softvu.agent@softvu.com"
            aliases: []
          , cb
        lead: (cb) ->
          leads.save
            recipient:
              firstname: "John"
              lastname: "Doe"
              email: "john.doe@email.com"
            agent: "softvu.agent@softvu.com"
            external:
              source: 'source'
              id: '12345'
          , cb
        status: (cb) ->
          statuses.save
            segments: [
              id: segment_id = uuid.v4().replace /-/g, ''
              steps: [{ id: step_id = (uuid.v4().replace /-/g, ''), notify: yes, message: id: uuid.v4().replace /-/g, '' }]
            ]
          , (err, body) ->
            cb err, body?.id
        'sample w/ notification': (cb) ->
          emails.save
            name: 'sample w/ notification'
            sender: email: 'sender@none'
            sample: yes
            notification: yes
          , cb
        'sample w/o notification': (cb) ->
          emails.save
            name: 'sample w/o notification'
            sender: email: 'sender@none'
            sample: yes
          , cb
        'drip w/ notification': ['status', (cb, results) ->
          emails.save
            type: 'drip'
            name: 'drip w/ notification'
            sender: email: 'sender@none'
            status: id: results.status
            segment: id: segment_id
            step: id: step_id
          , cb
        ]
        'drip w/ notification & lead': ['status', 'lead', (cb, results) ->
          emails.save
            type: 'drip'
            name: 'drip w/ notification'
            sender: email: 'softvu.agent@softvu.com'
            status: id: results.status
            segment: id: segment_id
            step: id: step_id
            lead: id: results.lead.id
          , cb
        ]
        'direct w/o notification': ['messages', (cb, results) ->
          emails.save
            type: 'direct'
            sender: email: 'sender@none'
            message: id: results.messages['nonotify'].id
          , cb
        ]
        'direct w/ notification': ['messages', 'lead', (cb, results) ->
          emails.save
            type: 'direct'
            sender: email: 'sender@none'
            message: id: results.messages['notify'].id
            lead: id: results.lead.id
          , cb
        ]
        'batch w/ notification': (cb, results) ->
          emails.save
            type: 'batch'
            name: 'batch w/ notification'
            sender: email: 'sender@none'
            batch: vunotification: true
          , cb
        'batch w/o notification': (cb, results) ->
          emails.save
            type: 'batch'
            name: 'batch w/o notification'
            sender: email: 'sender@none'
            batch: vunotification: false
          , cb
      , (err, results) =>
        @emails = results
        done err

    beforeEach ->
      @events = new EventEmitter

      @ctx =
        events: @events
        logger: log: ->
        publish: (queue, mail, cb) =>
          @events.emit 'sendMail', mail
          cb?()
        use: factory.use.bind factory

    afterEach (done) ->
      db = factory.single()
      db.query 'TRUNCATE _events', done

    describe '#.email.clicked', ->
      it 'sampleWithNotificationSet', (done) ->
        event =
          client: @client
          email: @emails['sample w/ notification'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', (message) ->
          expect(message.mail.to).to.be.equal 'sender@none'

        vunotifications['#.email.clicked'].call @ctx, event, (err) ->
          expect(err).to.be.undefined
          done()

      it 'sampleWithoutNotificationSet', (done) ->
        event =
          client: @client
          email: @emails['sample w/o notification'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', -> assert.fail 'sendMail called'

        vunotifications['#.email.clicked'].call @ctx, event, (err) ->
          expect(err).to.be.undefined
          done()

      it 'notification sent for drip notification without lead', (done) ->
        event =
          client: @client
          at: moment().add(2, 'days').toISOString()
          email: @emails['drip w/ notification'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', (message) ->
          expect(message.mail.to).to.equal 'sender@none'

        vunotifications['#.email.clicked'].call @ctx, event, (err) ->
          expect(err).to.be.undefined
          done()

      it 'notification sent for drip notification with lead', (done) ->
        event =
          client: @client
          at: moment().add(2, 'days').toISOString()
          email: @emails['drip w/ notification & lead'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', (message) ->
          expect(message.mail.to).to.equal 'softvu.agent@softvu.com'

        vunotifications['#.email.clicked'].call @ctx, event, (err) ->
          expect(err).to.be.undefined
          done()

      it 'batchWithNotificationSet', (done) ->
        event =
          client: @client
          at: moment().add(2, 'days').toISOString()
          email: @emails['batch w/ notification'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', (message) ->
          expect(message.mail.to).to.equal 'sender@none'

        vunotifications['#.email.clicked'].call @ctx, event, (err) ->
          expect(err).to.be.undefined
          done()

      it 'batchWithoutNotificationSet', (done) ->
        event =
          client: @client
          at: moment().add(2, 'days').toISOString()
          email: @emails['batch w/o notification'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', -> assert.fail 'sendMail called'

        vunotifications['#.email.clicked'].call @ctx, event, (err, a) ->
          expect(err).to.be.undefined
          done()

      # TODO need to insert the proper notification events to check against
      it 'notification skips', (done) ->
        events = @ctx.use 'client/events'
        events.save
          type: 'vunotification.queued'
          at: moment().toISOString()
          to: 'sender@none'
          email: @emails['drip w/ notification'].id
        , =>
          event =
            client: @client
            at: moment().add(2, 'hours').toISOString()
            email: @emails['drip w/ notification'].id
            link: 'link1'
            redirect: 'http://www.google.com'

          @events.on 'sendMail', sendMail = sinon.spy()
          @events.on 'notification.skipped', (client, message) ->
            expect(message.reason).to.contain 'delay settings'

          vunotifications['#.email.clicked'].call @ctx, event, (err) ->
            expect(err).to.be.undefined
            expect(sendMail).to.not.be.called
            done()

      it 'check html link complied correctly', (done) ->
        event =
          client: @client
          at: moment().add(2, 'days').toISOString()
          email: @emails['drip w/ notification'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', (message) ->
          expect(message.link).to.equal 'http://www.google.com'

        @events.on 'notification.skipped', notificationSkipped = sinon.spy()
        vunotifications['#.email.clicked'].call @ctx, event, (err) ->
          expect(err).to.be.undefined
          expect(notificationSkipped).to.not.be.called
          done()

      it 'notification skips for direct message', (done) ->
        event =
          client: @client
          email: @emails['direct w/o notification'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', sendMail = sinon.spy()
        vunotifications['#.email.clicked'].call @ctx, event, (err) ->
          expect(err).to.be.undefined
          expect(sendMail).to.not.be.called
          done()

      it 'notification sent for direct', (done) ->
        event =
          client: @client
          email: @emails['direct w/ notification'].id
          link: 'link1'
          redirect: 'http://www.google.com'

        @events.on 'sendMail', (message) ->
          expect(message.mail.to).to.equal 'sender@none'

        vunotifications['#.email.clicked'].call @ctx, event, (err) ->
          expect(err).to.be.undefined
          done()
