module.exports = (ctx, data, done) ->
  process.nextTick ->
    return done data._error if data._error?

    return done 'requeue' if data._requeue?
    return done 'cancel' if data._cancel?

    done()
