{EventEmitter} = require 'events'
fs = require 'fs'
proxyquire = require 'proxyquire'

MarksmanXmlRequest = proxyquire 'shared/integration/mortech/marksman/pipeline',
  request: do ->
    fn = (_, cb) ->
      fs.readFile __dirname + '/mortech.xml', (err, file) ->
        cb null, null, file.toString()
    fn['@global'] = yes
    fn

describe 'integration', ->
  describe 'mortech', ->
    describe 'pipeline_test', ->
      beforeEach ->
        @events = new EventEmitter()

      before (done) ->
        @ctx =
          use: factory.use.bind factory
          events:
            emit: (type, data) =>
              @events.emit 'events.emit', type: type, data: data

        @info = clientId: factory.client

        db = factory.single()
        db.query 'TRUNCATE data', (err) =>
          statuses = factory.use 'client/statuses'
          async.parallel
            new: (cb) -> statuses.save name: 'New', cb
            viewed: (cb) -> statuses.save name: 'Viewed', cb
          , (err, results) =>
            @statuses = results
            done()

      it 'should have no change', (done) ->
        spy = sinon.spy()

        mxr = new MarksmanXmlRequest()
        lead =
          client: 'client'
          id: '12345'
          agent: 'dani.johnson@softvu.com'
          status: 'New'
          $status: @statuses['new'].id

        @events.on 'events.emit', spy

        mxr.validate @ctx, lead: lead, @info, '8992436', (err) =>
          expect(err).to.not.exist

          setImmediate ->
            expect(spy).to.not.have.been.called
            done()

      it 'should change the agent', (done) ->
        mxr = new MarksmanXmlRequest()
        lead =
          client: 'client'
          id: '12345'
          agent: 'nick@softvu.com'
          status: 'Viewed'
          $status: @statuses['new'].id

        @events.on 'events.emit', (event) ->
          if event.type is 'lead.agent.changed'
            expect(event.data.agent).to.equal 'dani.johnson@softvu.com'

        mxr.validate @ctx, lead: lead, @info, '8992436', (err) =>
          expect(err).to.equal 'requeue'
          done()

      it 'should change the status', (done) ->
        mxr = new MarksmanXmlRequest()
        lead =
          client: 'client'
          id: '12345'
          agent: 'dani.johnson@softvu.com'
          status: 'Nurture'
          $status: @statuses['viewed'].id

        @events.on 'events.emit', (event) =>
          if event.type is 'lead.status.changed'
            expect(event.data.status).to.equal @statuses['new'].id

        mxr.validate @ctx, lead: lead, @info, '8992436', (err) =>
          expect(err).to.equal 'cancel'
          done()
