{EventEmitter} = require 'events'
fs = require 'fs'
proxyquire = require 'proxyquire'

VelocifyXmlRequest = proxyquire 'shared/integration/velocify/leadmanager/pipeline',
  request: do ->
    fn = (_, cb) ->
      fs.readFile __dirname + '/velocify.xml', (err, file) ->
        cb null, null, file.toString()
    fn['@global'] = yes
    fn

VelocifyXmlRequestNoAgent = proxyquire 'shared/integration/velocify/leadmanager/pipeline',
  request: do ->
    fn = (_, cb) ->
      fs.readFile __dirname + '/velocify_noagent.xml', (err, file) ->
        cb null, null, file.toString()
    fn['@global'] = yes
    fn

describe 'integration', ->
  describe 'velocify', ->
    describe 'pipeline_test', ->
      beforeEach ->
        @events = new EventEmitter()

      before (done) ->
        @ctx =
          use: factory.use.bind factory
          events:
            emit: (type, data) =>
              @events.emit 'events.emit', type: type, data: data

        @info =
          username: 'username'
          password: 'password'

        db = factory.single()
        db.query 'TRUNCATE data', (err) =>
          statuses = factory.use 'client/statuses'
          async.parallel
            new: (cb) -> statuses.save name: 'New', cb
            viewed: (cb) -> statuses.save name: 'Viewed', cb
          , (err, results) =>
            @statuses = results
            done()

      it 'should have no change', (done) ->
        spy = sinon.spy()

        vxr = new VelocifyXmlRequest()
        lead =
          client: 'client'
          _id: '12345'
          agent: 'dani.johnson@softvu.com'
          status: 'New'
          $status: @statuses['new'].id

        @events.on 'events.emit', spy

        vxr.validate @ctx, lead: lead, @info, '59', (err) =>
          expect(err).to.be.undefined

          setImmediate ->
            expect(spy).to.not.have.been.called
            done()

      it 'should change the agent', (done) ->
        vxr = new VelocifyXmlRequest()
        lead =
          client: 'client'
          _id: '12345'
          agent: 'nick.peeples@softvu.com'
          status: 'New'
          $status: @statuses['new'].id

        @events.on 'events.emit', (event) ->
          if event.type is 'lead.agent.changed'
            expect(event.data.agent).to.equal 'dani.johnson@softvu.com'

        vxr.validate @ctx, lead: lead, @info, '59', (err) ->
          expect(err).to.equal 'requeue'
          done()

      it 'should change the status', (done) ->
        vxr = new VelocifyXmlRequest()
        lead =
          client: 'client'
          _id: '12345'
          agent: 'nick.peeples@softvu.com'
          status: 'Old'

        @events.on 'events.emit', (event) =>
          if event.type is 'lead.status.changed'
            expect(event.data.status).to.equal @statuses['new'].id

        vxr.validate @ctx, lead: lead, @info, '59', (err) =>
          expect(err).to.equal 'cancel'
          done()

      it 'should change agent is unassigned', (done) ->
        vxr = new VelocifyXmlRequestNoAgent()
        lead =
          client: 'client'
          _id: '12345'
          agent: 'nick.peeples@softvu.com'
          status: 'Old'

        @events.on 'events.emit', (event) ->
          if event.type is 'lead.status.changed'
            expect(event.data.agent).to.be.undefined

        vxr.validate @ctx, lead: lead, @info, '59', (err) =>
          expect(err).to.equal 'cancel'
          done()
