_ = require 'lodash'

attributemapper = require 'shared/attributemapper'

describe 'lib', ->
  describe 'attributemapper_test', ->
    beforeEach ->
      @groups = {}

      add = (name, values, _default) =>
        $ = @groups[name] =
          name: name, active: yes, values: {}, aliases: [], default: _default

        values.forEach (name) ->
          if _.isArray name
            val = $.values[name[0]] =
              name: name[0], active: yes
            name[1].forEach (alias) ->
              $.aliases.push name: alias, value: name[0]
          else
            $.values[name] =
              name: name, active: yes

      add 'Lead Source', [
        ['Costco', ['CostcoGold', 'CostcoSilver']]
        ['Lending Tree', ['LendingTree Short', 'LendingTree']]
      ]
      add 'Product', ['Purchase', 'Refinance']
      add 'VA', ['Yes', 'No'], 'No'

    it 'should successfully map a filter',  ->
      filter = [
        name: 'Lead Source', values: ['Costco']
      ,
        name: 'Product', values: ['Purchase', 'Refinance']
      ]

      map = attributemapper.mapFilters filter, @groups

      expect(map).to.eql [['Lead Source/Costco', 'Product/Purchase'], ['Lead Source/Costco', 'Product/Refinance']], 'filter did not map correctly'


    it 'should error when mapping a bad filter', ->
      filter = [
        name: 'asdf', values: ['Costco']
      ,
        name: 'Product', values: ['Purchase', 'Refinance']
      ]

      fn = =>
        attributemapper.mapFilters filter, @groups

      expect(fn).to.throw 'Lead Attribute values could not be mapped.'


    it 'should successfully map a path',  ->
      attributes =
        leadsource: 'Costco'
        product: 'Refinance'
        va: 'No'

      path = attributemapper.mapPath attributes, @groups

      expect(path).to.eql ['Lead Source/Costco', 'Product/Refinance', 'VA/No']


    it 'should map a path using a default attribute',  ->
      attributes =
        leadsource: 'Costco'
        product: 'Purchase'
        va: ''

      path = attributemapper.mapPath attributes, @groups

      expect(path).to.eql ['Lead Source/Costco', 'Product/Purchase', 'VA/No']

    it 'should map a path using aliases',  ->
      attributes =
        leadsource: 'LendingTree Short'
        product: 'Refinance'
        va: 'No'

      path = attributemapper.mapPath attributes, @groups

      expect(path).to.eql ['Lead Source/Lending Tree', 'Product/Refinance', 'VA/No']


    it 'should error when mapping a bad path',  ->
      attributes =
        leadsource: 'Costco'
        product: 'asdf'
        va: 'No'

      fn = =>
        attributemapper.mapPath attributes, @groups

      expect(fn).to.throw "Lead Attribute value (Product, asdf) could not be mapped."
