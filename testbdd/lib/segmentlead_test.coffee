_         = require 'lodash'
segmenter = require 'shared/segmenter'

describe 'lib', ->
  describe 'segmentlead_test', ->
    beforeEach ->
      @segments = [
        name: 'first', segment: 'first', products: [['0/1', '1/1'], ['0/1', '1/2']]
      ,
        name: 'third', segment: 'third', products: [['0/0', '1/1']]
      ,
        name: 'second', segment: 'second', products: [['1/1']]
      ]

    it 'should segment with good attributes', ->
      path = ['0/0', '1/1', '2/1']

      segment = segmenter.segment path, @segments
      expect(segment.name).to.equal 'third'

    it 'should error for segment not found', ->
      path = ['0/0', '1/0', '2/1']

      fn = =>
        segmenter.segment path, @segments

      expect(fn).to.throw 'Lead could not be segmented, no filters match.'
