{EventEmitter} = require 'events'

_ = require 'lodash'
uuid = require 'uuid'

{PermanentError} = require '../pipeline/src/errors'
{PipelineConsumer, PipelineConsumerFactory} = require '../pipeline/src/consumers'
{context_builder} = require '../pipeline/src/main'

describe 'Pipeline', ->
  describe 'PipelineConsumer', ->
    consumer = fetched = created = null

    beforeEach ->
      @events = [ ]
      fetched = created = no
      consumer = new PipelineConsumer
        filter_directory: '../../testbdd/fixtures/'
      ,
        events:
          emit: (type, data) =>
            @events.push _.assign { }, data, type: type
        use: factory.use.bind factory
        router: -> ['a', 'b']
      ,
        type: 'drip'

    it 'should emit proper events', (done) ->
      consumer.process =>
        expect(_.first(@events).type).to.be.equal 'pipeline.started'
        expect(_.last(@events).type).to.be.equal 'email.queued'
        done()

    it 'should requeue', (done) ->
      consumer.data._requeue = yes
      consumer.process (requeue) ->
        expect(requeue).to.be.true
        done()

    it 'should cancel', (done) ->
      consumer.data._cancel = yes
      consumer.process (requeue) =>
        expect(requeue).to.be.false
        expect(@events[1].type).to.be.equal 'email.canceled'
        done()

    it 'should not requeue on permanent error', (done) ->
      consumer.data._error = new PermanentError 'A permanent error'
      consumer.process (requeue) =>
        expect(requeue).to.be.false
        expect(@events[1].type).to.be.equal 'email.errored'
        done()

    it 'should requeue on non permanent error', (done) ->
      consumer.data._error = new Error 'An error'
      consumer.process (requeue) =>
        expect(requeue).to.be.true
        expect(@events[1].type).to.be.equal 'email.errored'
        done()

  describe 'FullIntegration', ->
    before (done) ->
      @repos = factory.use
        messages: 'client/messages'
        templates: 'client/templates'
        emails: 'client/emails'
        senders: 'client/senders'
        statuses: 'client/statuses'

      @objects = {}
      async.auto
        message: (cb) =>
          @repos.messages.save
            subject: 'A Subject'
            html:
              blocks: {}
            text:
              blocks: {}
          , cb
        template: (cb) =>
          @repos.templates.save
            html: '<p>an email</p>'
            text: 'an email'
          , cb
        sender: (cb) =>
          @repos.senders.save
            email: 'nick@sender.com'
            firstname: 'Nick'
            lastname: 'Sender'
          , cb
        status: ['message', (cb, results) =>
          @repos.statuses.save
            name: 'FullPipelineTest'
            segments: [
              id: @objects.segment = uuid.v4().replace /-/g, ''
              steps: [
                id: @objects.step = uuid.v4().replace /-/g, ''
                message: results.message.id
              ]
            ]
          , cb
        ]
      , (err, result) =>
        _.assign @objects, result
        done()

    it 'should send an email', (done) ->
      Transport = require '../pipeline/src/transport'

      Events = require '../pipeline/src/events'
      Events.lib = events = new EventEmitter()

      ['errored', 'canceled'].forEach (evt) ->
        events.on "email.#{evt}", -> console.log '%s:', evt, arguments...

      events.on 'pipeline.completed', (client, data) =>
        @repos.emails.get data.email, (err, email) ->
          expect(email.mail.subject).to.equal 'A Subject'
          done()

      pcf = new PipelineConsumerFactory
        filter_directory: './filters/' # relative to pipeline consumer directory
        context_builder: (data) ->
          ctx = { }

          _.assign ctx,
            config: config
            use: factory.use.bind factory
            events: new Events data.client, data
            mailer: new Transport ctx, data
            logger:
              debug: console.log.bind console
            router: (route_type) ->
              switch route_type
                when 'vunotification', 'form.response', 'dailyvu', 'clickvu'
                  ['notifications/setup', 'notifications/compile', 'validate', 'send']
                when 'drip', 'batch', 'direct'
                  ['setup', 'fetch', 'check', 'compile', 'validate', 'linkify', 'send']

      pcf.consume
        client: factory.client
        type: 'drip'
        sender: 'nick@sender.com'
        template: @objects.template.id
        status: @objects.status.id
        segment: @objects.segment
        step: @objects.step
        message: @objects.message.id
        recipient:
          email: 'nick.peeples@gmail.com'
      , (err) ->
        # console.error 'pipeline send', err if err?
