fs = require 'fs'
proxyquire = require 'proxyquire'
{EventEmitter} = require 'events'

util = require 'util'

check = require '../../pipeline/src/filters/check'
{PermanentError} = require '../../pipeline/src/errors'

describe 'pipeline', ->
  describe 'check_test', ->
    before ->
      @checkmortech = proxyquire '../../pipeline/src/filters/check',
        request: do =>
          fn = (_, cb) =>
            fs.readFile __dirname + '/marksman.xml', (err, file) =>
              @events.emit 'download', 'marksman.xml'
              cb null, null, file.toString()
          fn['@global'] = yes
          fn

      @checkvelocify = proxyquire '../../pipeline/src/filters/check',
        request: do =>
          fn = (_, cb) =>
            fs.readFile __dirname + '/velocify.xml', (err, file) =>
              @events.emit 'download', 'velocify.xml'
              cb null, null, file.toString()
          fn['@global'] = yes
          fn

      @ctx =
        use: factory.use.bind factory
        integrations:
          get: (name) => if @integrations[name]? then new @integrations[name] else null
        events:
          emit: (type, data) ->
        logger:
          debug: ->
        client:
          integrations:
            _current: 'velocify'
            velocify: username: 'softvu', password: 'softvu'
            marksman: clientId: '12345'

      @data =
        client: @client = factory.client
        mail:
          to: 'nick.peeples-23@gmail.com'
        lead:
          status: 'New'
          agent: 'dani.johnson@softvu.com'
          external: { }

    beforeEach ->
      @integrations = {}
      @events = new EventEmitter
      @data.lead.external = {}

    describe 'integrations', ->
      before (done) ->
        statuses = factory.use 'client/statuses'
        statuses.save name: 'New', (err, body) =>
          @data.lead.$status = body.id
          done()

      it 'quick mortech', (done) ->
        @events.on 'download', (file) ->
          expect(file).to.equal 'marksman.xml'

        @data.lead.external = source: 'marksman', id: '12345'
        @checkmortech @ctx, @data, (err) ->
          expect(err).to.not.exist
          done()

      it 'quick velocify', (done) ->
        @events.on 'download', (file) ->
          expect(file).to.equal 'velocify.xml'

        @data.lead.external = source: 'velocify', id: '12345'
        @checkvelocify @ctx, @data, (err) ->
          expect(err).to.not.exist
          done()

    describe 'email checks', ->
      it 'should fail with invalid email address', (done) ->
        @data.mail.to = '(Hard Bounce)'
        check @ctx, @data, (err) ->
          expect(err).to.be.an.instanceof PermanentError, 'Error is not PermanentError'
          done()

      it 'should fail if status changes', (done) ->
        @integrations['integrations/check/velocify'] = class
          validate: (..., cb) ->
            cb 'cancel'

        @data.mail.to = 'nick.peeples-23@gmail.com'
        @data.lead.external = source: 'velocify', id: '12345'

        check @ctx, @data, (err) ->
          expect(err).to.equal 'cancel'
          done()

    describe 'optouts', ->
      before (done) ->
        optouts = @ctx.use 'core/optouts'
        async.parallel [
          _.bind optouts.optout, optouts, 'nick.peeples-23@gmail.com', @client
          _.bind optouts.optout, optouts, 'nick.peeples-global@gmail.com', null
        ], done

      after (done) ->
        db = factory.single()
        db.query 'TRUNCATE optouts', done

      # Following two tests have issues with getting the message from PermanentError
      it 'optout test', (done) ->
        @data.mail.to = 'nick.peeples-23@gmail.com'
        check @ctx, @data, (err) ->
          expect(err).to.be.an.instanceof PermanentError, 'Error is not PermanentError'

          expect(err.message).to.include 'opted out', 'opted out is not in err.message'
          expect(err.message).to.include 'nick.peeples-23@gmail.com', 'email address is not in err.message'
          done()

      it 'optout case test', (done) ->
        @data.mail.to = 'Nick.Peeples-23@gmail.com'
        check @ctx, @data, (err) ->
          expect(err).to.be.an.instanceof PermanentError, 'Error is not PermanentError'

          expect(err.message).to.include 'opted out', 'opted out is not in err.message'
          expect(err.message).to.include 'Nick.Peeples-23@gmail.com', 'email address is not in err.message'
          done()

      it 'optout global case test', (done) ->
        @data.mail.to = 'nick.peeples-global@gmail.com'
        check @ctx, @data, (err) ->
          expect(err).to.be.an.instanceof PermanentError, 'Error is not PermanentError'

          expect(err.message).to.include 'opted out', 'opted out is not in err.message'
          expect(err.message).to.include 'nick.peeples-global@gmail.com', 'email address is not in err.message'
          expect(err.message).to.include 'globally', 'globally is not in err.message'
          done()
