moment = require 'moment'

compiler = require '../../pipeline/src/filters/compile'

describe 'pipeline', ->
  describe 'compile_test', ->
    beforeEach (done) ->
      @ctx =
        use: factory.use.bind factory
        urls:
          unsub: ->
            "http://localhost:3001/unsub/12345/54321"
          view: ->
            "http://localhost:3001/view/12345/54321"
          page: (id) ->
            "http://localhost:3001/page/#{id}?e=54321"
        policies:
          htmlpolicy1: value: 'An html policy.'
          textpolicy1: value: 'A text policy.'
        client:
          credentials:
            aws:
              bucket: 'softvu'
          name: 'Test'

      @templates = factory.use 'client/templates'

      @template =
        html: '<div data-block="test"></div>'
        text: '{{> block.test}}'

      async.auto
        template: (cb) =>
          @templates.save @template
          , cb
        data: ['template', (cb, result) ->
          cb null ,
            id: '54321'
            template: result.template.id
            lead: '4321'
            html:
              blocks: test: '<div>inner</div>'
              policies: test: 'htmlpolicy1'
              links: {}
            text:
              blocks: test: 'inner'
              policies: test: value: 'textpolicy1'
              links: {}
            sender: name: 'Nick', signature: '{{sender.name}}'
            recipient: name: 'Jeff'
            mail:
              subject: 'Test {{recipient.name}}'
        ]
      , (err, result) =>
        done err if err
        _.assign @, result
        done()

    it 'should compile test with message', (done) ->
      compiler @ctx, @data, (err) =>
        expect(@data.mail.html).to.equal '<div><div>inner</div></div>'
        done()

    it 'should compile test replacements', (done) ->
      @template.html = '{{sender.name}} {{recipient.name}}'
      @template.text = '{{sender.name}} {{recipient.name}}'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal 'Nick Jeff'
          done()

    it 'should compile test replacement blocks with variables', (done) ->
      @template.html = '<div data-block="test"></div>'
      @template.text =
      @data.html.blocks.test = '<div>{{sender.name}} {{recipient.name}}</div>'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal '<div><div>Nick Jeff</div></div>'
          done()

    it 'should compile test replacement blocks with space in name', (done) ->
      @template.html = '<div data-block="test block"></div>'
      @template.text=
      @data.html.blocks['test block'] = '<div>{{sender.name}} {{recipient.name}}</div>'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal '<div><div>Nick Jeff</div></div>'
          done()

    it 'should compile test replacement policies', (done) ->
      @template.html = '<div data-policy="test"></div>'
      @template.text =  '{{> policy.test}}'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal '<div>An html policy.</div>'
          done()

    it 'should compile test with link blocks', (done) ->
      @template.html = '<a href data-link="test"></a>'
      @template.text =
      @data.html.links.test = type: 'page', value: '12345', text: 'Test'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.include '/page/12345?e=54321'
          expect(@data.mail.html).to.include 'Test'
          done()

    it 'should compile test with link external', (done) ->
      @template.html = '<a href data-link="test">Test</a>'
      @template.text =
      @data.html.links.test = type: 'url', value: 'https://www.google.com'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.include 'https://www.google.com'
          done()

    it 'should compile links with just url variable', (done) ->
      @template.html = '{{link.[main cta].url}}'
      @template.text = ''
      @data.html.links["main cta"] = type: 'page', value: '12345', text: 'Test'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal 'http://localhost:3001/page/12345?e=54321'
          done()

    it 'should compile links with just url variable for url', (done) ->
      @template.html = '{{link.[main cta].url}}'
      @template.text =
      @data.html.links["main cta"] = type: 'url', value: 'www.google.com'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal 'www.google.com'
          done()

    it 'should compile test with text', (done) ->
      compiler @ctx, @data, =>
        expect(@data.mail.text).to.equal 'inner'
        done()

    it 'should compile test with text links', (done) ->
      @template.html = ''
      @template.text = '{{> link.cta}}'
      @data.text.links.cta = type: 'page', value: '12345'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.text).to.equal 'http://localhost:3001/page/12345?e=54321'
          done()

    it 'should compile test with text links external', (done) ->
      @template.html = ''
      @template.text = '{{> link.cta}}'
      @data.text.links.cta = type: 'url', value: 'http://www.google.com'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.text).to.equal 'http://www.google.com'
          done()

    it 'should compile test with text policy', (done) ->
      @template.html = ''
      @template.text = '{{> policy.test}}'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.text).to.equal 'A text policy.'
          done()

    it 'should compile assets base url', (done) ->
      @template.html = '{{assets.baseurl}}/logo.png'
      @template.text = ''

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal 'https://softvu.s3.amazonaws.com/logo.png'
          done()

    it 'should compile subject test', (done) ->
      compiler @ctx, @data, =>
        expect(@data.mail.subject).to.equal 'Test Jeff'
        done()

    it 'should compile test with subject', (done) ->
      @data.recipient.firstname = 'jeff'
      @data.mail.subject = '{{capitalize recipient.firstname}}'

      compiler @ctx, @data, =>
        expect(@data.mail.subject).to.equal 'Jeff'
        done()

    it 'should compile unsub link html', (done) ->
      @template.html = '{{unsub}}'
      @template.text = ''

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal 'http://localhost:3001/unsub/12345/54321'
          done()

    it 'should compile unsub link text', (done) ->
      @template.html = ''
      @template.text = '{{unsub}}'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.text).to.equal 'http://localhost:3001/unsub/12345/54321'
          done()

    it 'should compile test with signatures', (done) ->
      @template.html = '{{> sender.signature}}'
      @template.text = ''

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal 'Nick'
          done()

    it 'should compile test with filters', (done) ->
      @template.html = '{{capitalize recipient.firstname}}'
      @template.text = ''
      @data.recipient.firstname = 'nick'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal 'Nick'
          done()

    it 'should compile test with moment', (done) ->
      now = moment '2014-05-30'
      tpl = '{{moment now format="MMMM"}} {{moment now format="Do"}} {{moment now format="YYYY"}}'
      @template.html = tpl
      @template.text = tpl
      @data.now = now

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal "May 30th 2014"
          expect(@data.mail.text).to.equal "May 30th 2014"
          done()

    it 'should compile test adds pixel bug', (done) ->
      @template.html = '<body></body>'
      @template.text = ''
      @ctx.urls.bug = (id) ->
        "http://localhost:3001/pb/54321/#{id}"
      @data.id = '12345'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal '<body><img src="http://localhost:3001/pb/54321/12345"></body>'
          done()

    it 'should compile test adds pixel bug without body tag', (done) ->
      @template.html = ''
      @template.text = ''
      @ctx.urls.bug = (id) ->
        "http://localhost:3001/pb/54321/#{id}"
      @data.id = '12345'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal '<img src="http://localhost:3001/pb/54321/12345">'
          done()

    it 'should compile test adds client name', (done) ->
      @template.html = '{{account.name}}'
      @template.text = '{{account.name}}'
      @data.account = name: 'Test'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal 'Test'
          done()

    it 'should compile test HTML URL blocks', (done) ->
      @template.html = '<div data-block="test"></div>'
      @template.text = ''
      @data.html.blocks.test = '<div>{{link.[main cta].url}}</div>'
      @data.html.links["main cta"] = type: 'url', value: 'www.google.com/?id={{id}}'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.html).to.equal '<div><div>www.google.com/?id=54321</div></div>'
          done()

    it 'should compile test text URL blocks', (done) ->
      @template.html = ''
      @template.text = '{{link.[main cta].url}}'
      @data.text.links["main cta"] = type: 'url', value: 'www.google.com/?id={{id}}'

      @templates.save @template, (err) =>

        compiler @ctx, @data, =>
          expect(@data.mail.text).to.equal 'www.google.com/?id=54321'
          done()
