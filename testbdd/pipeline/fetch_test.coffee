uuid = require 'uuid'

fetcher = require '../../pipeline/src/filters/fetch'

describe 'pipeline', ->
  describe 'fetch_test', ->
    before 'data add', (done) ->
      {messages, groups, senders, leads, policies, statuses, batches} = factory.use
        messages: 'client/messages'
        groups: 'client/groups'
        senders: 'client/senders'
        leads: 'client/leads'
        policies: 'client/policies'
        statuses: 'client/statuses'
        batches: 'client/batches'

      async.auto
        policies: (cb) ->
          save = _.bind policies.save, policies
          async.parallel [
            _.partial save, value: 'policy a text'
            _.partial save, value: 'policy b text'
          ], cb
        messages: ['policies', (cb, results) ->
          save = _.bind messages.save, messages
          async.parallel [
            _.partial save,
              template: 'template'
              html:
                blocks: {}
                links:
                  test: '4321'
                policies:
                  main: results.policies[0].id
            _.partial save,
              template: 'template2'
              html:
                blocks: {}
                links:
                  test: '4321'
                policies:
                  main: results.policies[1].id
          ], cb
        ]
        status: ['messages', (cb, results) =>
          statuses.save
            name: 'status'
            segments: [
              id: 'segmentid'
              name: 'segment'
              steps: [
                id: 'step1id'
                name: 'step 1'
                message: id: results.messages[1].id
              ]
            ]
          , cb
        ]
        versioned_message: ['policies', (cb, results) ->
          messages.save
            template: 'template'
            subject: 'message subject'
            html:
              blocks: main: 'main html block'
              links: test: '4321'
              policies: {}
            text:
              blocks: main: 'main text block'
              links: test: '4321'
              policies: {}
            versions: [
                subject: ref: 'default'
                html:
                  blocks: main: ref: 'default'
                  links: test: '1234 foo/bar and bar/baz'
                  policies: main: value: results.policies[1].id
                text:
                  blocks: main: 'asdf'
                  links: test: ref: 'default'
                  policies: main: value: results.policies[1].id
                hidden: false
                products: [['foo/bar', 'bar/baz']]
                template: ref: 'default'
              ,
                subject: ref: 'default'
                html:
                  blocks: main: ref: 'default'
                  links: test: '1234 only foo/bar'
                  policies: main: uuid.v4()
                text:
                  blocks: main: 'asdf'
                  links: test: ref: 'default'
                  policies: main: uuid.v4()
                hidden: false
                products: [['foo/bar']]
                template: ref: 'default'
              ,
                subject: ref: 'default'
                html:
                  blocks: main: ref: 'default'
                  links: test: '1234 foo/bar and foo/baz'
                  policies: main: uuid.v4()
                text:
                  blocks: main: 'asdf'
                  links: test: ref: 'default'
                  policies: main: uuid.v4()
                hidden: false
                products: [['foo/bar'], ['foo/baz']]
                template: ref: 'default'
              ,
                subject: value: 'different message subject'
                html:
                  blocks: main: ref: 'default'
                  links: test: ref: 'default'
                  policies: main: uuid.v4()
                text:
                  blocks: main: 'asdf'
                  links: test: ref: 'default'
                  policies: main: uuid.v4()
                hidden: false
                products: [['foo/zab'], ['oof/foo']]
                template: ref: 'default'
              ,
                subject: ref: 'default'
                html:
                  blocks: main: ref: 'default'
                  links: test: ref: 'default'
                  policies: main: uuid.v4()
                text:
                  blocks: main: 'asdf'
                  links: test: ref: 'default'
                  policies: main: uuid.v4()
                hidden: false
                products: [['foo/abc'], ['oof/abc']]
                template: value: 'different template'
            ]
          , cb
        ]
        group: (cb) ->
          groups.save signature: 'group signature', role: 'other', cb
        senders: ['group', (cb, results) ->
          save = _.bind senders.save, senders
          async.parallel [
            _.partial save,
              email: 'nick.peeples@softvu.com'
              group: results.group.id
              lastname: 'Peeples'
              firstname: 'Nick'
            _.partial save,
              email: 'nick.peeples@gmail.com'
              group: results.group.id
              signature: 'sender signature'
              override: yes
              lastname: 'Peeples'
              firstname: 'Nick'
            _.partial save,
              email: 'nick.peeples@gmail2.com'
              group: results.group.id
              signature: 'sender signature'
              override: yes
              lastname: 'Peeples'
              firstname: 'Nick'
              friendlyfrom: 'Nick Peeples (from)'
            _.partial save,
              email: 'jennifer.brown@gmail.com'
              active: no
              aliases: ['nick.peeples.active@gmail.com']
              group: results.group.id
              signature: 'sender signature'
              override: yes
              lastname: 'Brown'
              firstname: 'Jennifer'
            _.partial save,
              email: 'nick.peeples.active@gmail.com'
              group: results.group.id
              signature: 'sender signature'
              override: yes
              lastname: 'Peeples'
              firstname: 'Nick'
          ], cb
        ],
        leads: ['senders', (cb, results) ->
          # TODO rewrite these attributes to be more readable
          save = _.bind leads.save, leads
          async.parallel
            lead_baz: _.partial save, agent: 'nick.peeples@softvu.com', $attributes: ['foo/bar', 'bar/baz']
            lead_asdf: _.partial save, agent: 'nick.peeples@softvu.com', $attributes: ['foo/bar', 'bar/asdf']
            lead_foos: _.partial save, agent: 'nick.peeples@softvu.com', $attributes: ['foo/baz']
            lead_oof: _.partial save, agent: 'nick.peeples@softvu.com', $attributes: ['foo/zab', 'oof/foo']
            lead_abc: _.partial save, agent: 'nick.peeples@softvu.com', $attributes: ['foo/abc', 'oof/abc']
          , cb
        ]
        batch: ['messages', (cb, results) ->
          batches.save
            html:
              blocks: {}
              links:
                test: '4321'
              policies:
                main: results.policies[0].id
            name: 'Fetch Batch Test'
            message: results.messages[0].id
            scheduled: now: yes
            template: 'template'
          , cb
        ]
      , (err, results) =>
        _.assign @, results
        done err

    beforeEach ->
      @ctx =
        use: factory.use.bind factory
        client: {}
        logger: debug: ->

      @data = client: factory.client, sender: 'nick.peeples@softvu.com', message: @messages[0].id

    it 'should fetch with tagline', (done) ->
      fetcher @ctx, @data, (err) =>
        expect(err).to.not.exist
        expect(@data.mail.from).to.equal 'Nick Peeples <nick.peeples@softvu.com>'
        done()

    it 'should fetch with tagline (svemails)', (done) ->
      @ctx.client.use_svemails = yes
      @ctx.client.svemails = prefix: 'softvutest'
      fetcher @ctx, @data, (err) =>
        expect(err).to.not.exist
        expect(@data.mail.from).to.equal 'Nick Peeples <other@softvutest.svemails.com>'
        done()

    it 'should fetch with tagline (svemails)', (done) ->
      @ctx.client.use_svemails = yes
      @ctx.client.svemails = prefix: 'softvutest'
      @data.sender = 'nick.peeples@gmail2.com'
      fetcher @ctx, @data, (err) =>
        expect(err).to.not.exist
        expect(@data.mail.from).to.equal 'Nick Peeples (from) <other@softvutest.svemails.com>'
        done()

    it 'should fetch message info', (done) ->
      fetcher @ctx, @data, (err) =>
        expect(err).to.not.exist
        expect(@data.html.links.test).to.equal '4321'
        done()

    it 'should fetch group signature', (done) ->
      fetcher @ctx, @data, (err) =>
        expect(err).to.not.exist
        expect(@data.sender.signature).to.equal 'group signature'
        done()

    it 'should fetch sender override signature', (done) ->
      @data.sender = 'nick.peeples@gmail.com'
      fetcher @ctx, @data, (err) =>
        expect(err).to.not.exist
        expect(@data.sender.signature).to.equal 'sender signature'
        done()

    it 'should fetch the correct sender, even if an inactive sender has the same alias', (done) ->
      @data.sender = 'nick.peeples.active@gmail.com'
      fetcher @ctx, @data, (err) =>
        expect(err).to.not.exist
        expect(@data.sender.email).to.not.equal 'jennifer.brown@gmail.com'
        expect(@data.sender.email).to.equal 'nick.peeples.active@gmail.com'
        done()

    it 'should fetch the correct versioned message', (done) ->
      data = _.clone @data
      data.sender = 'nick.peeples@gmail.com'
      data.message = @versioned_message.id
      data.lead = @leads.lead_baz.id
      fetcher @ctx, data, (err) =>
        expect(data.mail.subject).to.equal 'message subject'
        expect(data.template).to.equal 'template'
        expect(data.html.links.test).to.equal '1234 foo/bar and bar/baz'
        expect(data.text.links.test).to.equal '4321'
        done()

    it 'should fetch the correct versioned message (single attribute)', (done) ->
      data = _.clone @data
      data.sender = 'nick.peeples@gmail.com'
      data.message = @versioned_message.id
      data.lead = @leads.lead_asdf.id
      fetcher @ctx, data, (err) =>
        expect(data.mail.subject).to.equal 'message subject'
        expect(data.template).to.equal 'template'
        expect(data.html.links.test).to.equal '1234 only foo/bar'
        expect(data.text.links.test).to.equal '4321'
        done()

    it 'should fetch the correct versioned message (multiple values, same attribute group)', (done) ->
      data = _.clone @data
      data.sender = 'nick.peeples@gmail.com'
      data.message = @versioned_message.id
      data.lead = @leads.lead_foos.id
      fetcher @ctx, data, (err) =>
        expect(data.mail.subject).to.equal 'message subject'
        expect(data.template).to.equal 'template'
        expect(data.html.links.test).to.equal '1234 foo/bar and foo/baz'
        expect(data.text.links.test).to.equal '4321'
        done()

    it 'should fetch the correct versioned message (different version subject)', (done) ->
      data = _.clone @data
      data.sender = 'nick.peeples@gmail.com'
      data.message = @versioned_message.id
      data.lead = @leads.lead_oof.id
      fetcher @ctx, data, (err) =>
        expect(data.mail.subject).to.equal 'different message subject'
        expect(data.template).to.equal 'template'
        expect(data.html.links.test).to.equal '4321'
        expect(data.text.links.test).to.equal '4321'
        done()

    it 'should fetch the correct versioned message (different version template)', (done) ->
      data = _.clone @data
      data.sender = 'nick.peeples@gmail.com'
      data.message = @versioned_message.id
      data.lead = @leads.lead_abc.id
      fetcher @ctx, data, (err) =>
        expect(data.mail.subject).to.equal 'message subject'
        expect(data.template).to.equal 'different template'
        expect(data.html.links.test).to.equal '4321'
        expect(data.text.links.test).to.equal '4321'
        done()

    it 'should fetch the correct policy', (done) ->
      @data.sender = 'nick.peeples@gmail.com'
      fetcher @ctx, @data, (err) =>
        expect(@ctx.policies[@policies[0].id]?.value).to.equal 'policy a text'
        done()

    it 'should fetch the correct policy (versioned)', (done) ->
      data = _.clone @data
      data.sender = 'nick.peeples@gmail.com'
      data.message = @versioned_message.id
      data.lead = @leads.lead_baz.id
      fetcher @ctx, data, (err) =>
        expect(@ctx.policies[@policies[1].id]?.value).to.equal 'policy b text'
        done()

    it 'should fetch the right message for a drip', (done) ->
      data = _.clone @data
      data.status = @status.id
      data.segment = 'segmentid'
      data.step = 'step1id'
      data.message = @messages[0].id
      fetcher @ctx, data, (err) =>
        expect(data.template).to.equal 'template2'
        done()

    it 'should fetch the correct policy for a batch', (done) ->
      data = client: factory.client
      data.batch = @batch.id
      data.agent = 'nick.peeples@softvu.com'
      data.recipient = email: 'nick.peeples@batch.com', firstname: 'Nick'
      fetcher @ctx, data, (err) =>
        expect(@ctx.policies[@policies[0].id]?.value).to.equal 'policy a text'
        done()
