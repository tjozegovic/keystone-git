EventEmitter = require('events').EventEmitter
linkify = require '../../pipeline/src/filters/linkify'

class TestContext extends EventEmitter
  constructor: ->
    @urls =
      bug: (id) ->
        "http://localhost:3001/pb/client/#{id}"
      link: (id) ->
        "http://localhost:3001/click/client/#{id}"

describe 'pipeline', ->
  describe 'linkify_test', ->
    before ->
      @ctx = new TestContext

    it 'should test a single link', (done) ->
      data = mail: html: '<a href="#">Test</a>'
      linkify @ctx, data, ->
        expect(data.mail.html).to.include data.links[0].url
        expect(data.links[0].original_url).to.equal '#'
        done()

    it 'should add a single email', (done) ->
      data = id: 1, mail: html: '<a href="#" data-link="link">Test</a><a href="#" data-link="link2">Test</a>'
      linkify @ctx, data, ->
        expect(data.mail.html).to.include data.links[0].url
        expect(data.links[0].url).to.include 'click/client'
        expect(data.links[0].url).to.not.include 'data-link'
        expect(data.links[0].name).to.equal 'link'
        expect(data.links[0].original_url).to.equal '#'
        done()

    it 'should test multiple lines', (done) ->
      data = mail: html: '<a href="/1">1</a><a href="/2">2</a>'

      linkify @ctx, data, ->
        data.links.forEach (link) -> expect(data.mail.html).to.include link.url
        done()

    # TODO need to rewrite, or reconsider?
    it.skip 'should ignore unsub', (done) ->
      data = mail: html: '<a href="/unsub/1/2">Test</a>'

      linkify @ctx, data, ->
        expect(data.mail.html).to.include '/unsub/1/2'
        done()

    it 'should ignore directive', (done) ->
      data = mail: html: '<a href="www.google.com" data-donottrack>Test</a>'

      linkify @ctx, data, ->
        expect(data.mail.html).to.include 'www.google.com'
        done()
