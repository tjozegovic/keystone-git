Processor = require '../../pipeline/src/processor'

describe 'pipeline', ->
  describe 'processor_test', ->
    beforeEach ->
      @doneSpy = sinon.spy()

    it 'basic test', (_done) ->
      processor = new Processor()

      processor.on 'done', => @doneSpy()

      processor.process [
        (done) -> done()
      ]
      setImmediate =>
        expect(@doneSpy).to.have.been.called
        _done()

    it 'cancel test', (_done) ->
      processor = new Processor()

      cancelSpy = sinon.spy()

      processor.on 'cancel', -> cancelSpy()

      processor.on 'done', => @doneSpy()

      processor.process [
        (done) -> done 'cancel'
      ]

      setImmediate =>
        expect(@doneSpy).to.not.have.been.called
        expect(cancelSpy).to.have.been.called
        _done()

    it 'restart test', (_done) ->
      processor = new Processor()

      cancelSpy = sinon.spy()
      errorSpy = sinon.spy()
      requeueSpy = sinon.spy()

      processor.on 'cancel', -> cancelSpy()
      processor.on 'done', => @doneSpy()
      processor.on 'error', -> errorSpy()

      processor.on 'requeue', -> requeueSpy()

      processor.process [
        (done) -> done()
        (done) -> done 'requeue'
      ]

      setImmediate =>
        expect(requeueSpy).to.have.been.called
        expect(@doneSpy).to.not.have.been.called
        expect(cancelSpy).to.not.have.been.called
        expect(errorSpy).to.not.have.been.called
        _done()

    it 'string error test', (_done) ->
      processor = new Processor()

      cancelSpy = sinon.spy()

      processor.on 'cancel', -> cancelSpy()
      processor.on 'done', => @doneSpy()
      processor.on 'error', (err) ->
        expect(err.error).to.equal 'error'

      processor.process [
        (done) -> done 'error'
      ]

      setImmediate =>
        expect(@doneSpy).to.not.have.been.called
        expect(cancelSpy).to.not.have.been.called
        _done()

    it 'sync domain error test', (_done) ->
      processor = new Processor()

      cancelSpy = sinon.spy()

      processor.on 'cancel', -> cancelSpy()
      processor.on 'done', => @doneSpy()
      processor.on 'error', (err) ->
        expect(err.error.message).to.equal 'error'

      processor.process [
        (done) -> throw new Error 'error'
      ]

      setImmediate =>
        expect(@doneSpy).to.not.have.been.called
        expect(cancelSpy).to.not.have.been.called
        _done()

    it 'async domain error test', (_done) ->
      processor = new Processor()

      cancelSpy = sinon.spy()

      processor.on 'cancel', -> cancelSpy()
      processor.on 'done', => @doneSpy()
      processor.on 'error', (err) ->
        expect(err.error.message).to.equal 'error'

      processor.process [
        (done) ->
          setTimeout ->
            throw new Error 'error'
          , 1
      ]

      setImmediate =>
        expect(@doneSpy).to.not.have.been.called
        expect(cancelSpy).to.not.have.been.called
        _done()
