fs = require 'fs'

Transport = require '../../pipeline/src/transport'
sender = require '../../pipeline/src/filters/send'

describe 'pipeline', ->
  describe 'send_test', ->
    before (done) ->
      @ctx =
        client:
          debug: no
          dkim:
            enabled: yes
            domain_selector: 'example.com': 'test'
            keys: [
              domain: 'example.com'
              selector: 'test'
              privateKey: """-----BEGIN RSA PRIVATE KEY-----
      MIICXgIBAAKBgQDA9QQI96hFYxhcIHxCJ7VGsgRqPWfVUeJqIjDuuaFGBTwOWSKO
      GUmUaU5xLM2APUPtpWPc/NZxu9r1goQnTuDSwdyfvfBXRI/I6DUlVuHJhSvFvT3o
      2BixsvuQZM4Ew7hiRqKia2RZzip3tdbN6kKVffPvxqHvG0QYv3vCNftEIQIDAQAB
      AoGAO+aUJxiC429863SmPn9HNjgjSLvhVQbDjAErQaDc4PjVCPtAz+4numvqjxXC
      gfIC+W5jjwHqrIoN0jN/RRimkBx7tsdRr+rHgS7bYwIRxHFytXdqdbPKQWXG0PtL
      KbWGBG2us+uM8RAm3OaT89vq0sy+eFOliOdQ+9oxO6fqRCUCQQDv2clVC28LgOpk
      /OXI/+kO57G/m9ctKV0DSktgWltV7xBHcOFmj0K7Vx9IyH/WGucMZQRGu2lGGef/
      5K1wi6zfAkEAzfLxJejHmh4bjEJ62hZkPY188YqzdMDVn5IuIKxsZ9Adnipl4J7T
      IxVq9INbzvCykEuGWAbhEs9gj7YDDXYu/wJBALlCApxJpI/CF0nBfMrvv1ff5g8V
      k/IyDMHJ1Marg/iwGmbT7YWGCgkhlexl0TfSt7t/Gvp3Ly0SPXwNaVmQyQkCQQCl
      cg0XjzEKNjWbL6zRljtg11EQGYSgvmjej+hOhzVvEFk6voSpNe4XuRT66KvQSryn
      /oQmIyCco8vGp0k8fzyxAkEAi2JSPQFjS90BfFtu5rD5FV8O3Hrv4WscO0bp7iQ1
      kRj5tpQD2/feKeNxQTOoADnRvMLZNJ+hmg6PbbJymqEang==
      -----END RSA PRIVATE KEY-----
      """
            ]
        events:
          emit: (type, data, cb) ->
            [data, cb] = [null, data] if typeof data is 'function'
            cb?()
        mailer:
          send: (obj, cb) ->
            cb()

      @data =
        id: '1'
        client: '1'
        type: 'drip'
        segment: 'Foo'
        step: '4321'
        message: '1234'
        sender:
          domain: 'example.com'
          replyto: 'reply@to.com'
        mail:
          from: 'testfrom@example.com'
          to: 'testto@example.com'
          subject: 'subject'
          html: 'html'
          text: 'text'

      done()

    it 'send save html test', (done) ->
      @ctx.mailer.send = (obj, cb) ->
        expect(obj.subject).to.equal 'subject'
        cb()

      sender @ctx, @data, -> done()

    it 'should create return path address encoded', (done) ->
      @ctx.mailer.send = (obj, cb) ->
        expect(obj.envelope.from).to.equal '1.1.testto=example.com@example.com'
        cb()

      sender @ctx, @data, done

    it 'should create replyto address header', (done) ->
      @ctx.mailer.send = (obj, cb) ->
        expect(obj.headers['Reply-To']).to.equal '<reply@to.com>'
        cb()

      sender @ctx, @data, done

    # test has occasionally failed.. for unknown reasons. done() doesn't get called
    it 'send choose DKIM', (done) ->
      @ctx.mailer = new Transport @ctx, @data.mail
      sender @ctx, @data, (err, response) =>
        expect(response.response.toString()).to.contain 'DKIM-Signature'
        done()
