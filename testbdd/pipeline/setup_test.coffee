setup = require '../../pipeline/src/filters/setup'

describe 'pipeline', ->
  describe 'setup_test', ->
    before (done) ->
      @ctx =
        use: factory.use.bind factory

      @clients = factory.use 'core/clients'
      @clients.get factory.client, (err, client) =>
        @client = _.assign client,
          privacy_url: 'http://a.url'
          use_svemails: yes
          svemails: prefix: 'prefix'
        @clients.save @client, (err, result) =>
          return done err if err?
          _.assign @client, result
          done()

    it 'setup check account values', (done) ->
      data = client: @client.id
      setup @ctx, data, (err) ->
        expect(err).to.be.null
        expect(data.account.privacy_url).to.equal 'http://a.url'
        done()

    it 'sets up the proper urls for svemails', (done) ->
      data = client: @client.id, id: 'e1'
      setup @ctx, data, (err) =>
        expect(err).to.be.null
        expect(@ctx.urls.bug()).to.equal 'http://go.prefix.svemails.com/pb/e1'
        expect(@ctx.urls.link('l1')).to.equal 'http://go.prefix.svemails.com/click/e1/l1'
        done()

    describe 'cnames', ->
      before (done) ->
        client = _.assign {}
          , _.omit(@client, 'id', 'version', 'svemails')

        @clients.save client, (err, result) =>
          @cid = result.id
          done()

      beforeEach (done) ->
        @clients.get @cid, (err, client) =>
          @client = client
          done()

      it 'sets up the proper urls for cnames', (done) ->
        client = _.assign @client
          , use_svemails: no, cnames: ['testing.domain']

        @clients.save client, (err, result) =>
          data = client: result.id, id: 'e1'
          setup @ctx, data, (err) =>
            expect(err).to.be.null
            expect(@ctx.urls.bug()).to.equal 'https://testing.domain/pb/e1'
            expect(@ctx.urls.link('l1')).to.equal 'https://testing.domain/click/e1/l1'
            done()

      it 'sets up proper urls for cnames 2', (done) ->
        client = _.assign @client
          , use_svemails: no, cnames: [use_https: no, host: 'testing.domain']

        @clients.save client, (err, result) =>
          data = client: result.id, id: 'e1'
          setup @ctx, data, (err) =>
            expect(err).to.be.null
            expect(@ctx.urls.bug()).to.equal 'http://testing.domain/pb/e1'
            expect(@ctx.urls.link('l1')).to.equal 'http://testing.domain/click/e1/l1'
            done()

      it 'should read proper assets url', (done) ->
        client = _.assign @client
          , use_svemails: no, assets: 'http://cdn.assets'

        @clients.save client, (err, result) =>
          data = client: result.id, id: 'e1'
          setup @ctx, data, (err) =>
            expect(err).to.be.null
            expect(data.assets.baseurl).to.equal 'http://cdn.assets'
            done()
