{assert, expect, should} = require 'chai'
global.assert = assert
global.expect = expect
global.should = should

sinon = require 'sinon'
sinonChai = require 'sinon-chai'
chaiDatetime = require 'chai-datetime'
chai = require 'chai'
chai.use sinonChai
chai.use chaiDatetime
global.sinon = sinon
global.chai = chai

config = require 'config'

global._ = require 'lodash'
global.async = require 'async'
global.moment = require 'moment'

pg = require 'pg'
uuid = require 'uuid'
{EventEmitter} = require 'events'

global.EventEmitter = EventEmitter

{RepositoryFactory, SingleQueryDatabase, MultiQueryDatabase} = require 'shared/database'
schema = require 'shared/database/schema'

do ->
  id = undefined
  db = "keystone_test_#{Date.now()}"
  url = config.db.url[0..config.db.url.lastIndexOf '/'] + db
  global.factory = new RepositoryFactory url

  before 'db setup', (done) ->
    @timeout 0
    async.series [
      require('../gulp-tasks/pg/create').bind(null, url, db)
      (cb) ->
        clients = factory.use 'core/clients'
        clients.save name: 'Testing Client', short: 'tc', cnames: ['tc.localhost'], (err, body) ->
          return done err if err?

          {id} = body
          factory.client = id
          done()
    ], (err) ->
      return done err if err?
      done()

  after 'db cleanup', ->
    @timeout 0
    pg.end()
