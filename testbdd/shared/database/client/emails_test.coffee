async = require 'async'
uuid = require 'uuid'

describe 'shared/database/repositories', ->
  describe 'emails', ->
    beforeEach ->
      @emails = factory.use 'client/emails'

    it 'should save new email', (done) ->
      @emails.save
        type: 'direct'
        sender: email: 'sender@none'
      , (err, body) =>
        @emails.get body.id, (err, email) ->
          expect(email.type).to.equal 'direct'
          expect(email.sender.email).to.equal 'sender@none'
          done err

    it 'should update email', (done) ->
      @emails.save
        type: 'direct'
        sender: email: 'sender@none'
      , (err, body) =>
        @emails.get body.id, (err, email) =>
          email.sender.email = 'sender@none.com'
          @emails.save email, (err) =>
            @emails.get body.id, (err, email) =>
              expect(email.sender.email).to.equal 'sender@none.com'
              done err

    it 'should insert html w/ commas', (done) ->
      @emails.save
        type: 'direct'
        mail: html: '<html><head><style>font: \'Arial\'</style></head><body></body></html>'
      , (err, body) ->
        done err

    it 'should not return html when using onlymeta query option', (done) ->
      @emails.save
        type: 'direct'
        mail: html: '<html><head><style>font: \'Arial\'</style></head><body></body></html>'
      , (err, body) =>
        @emails.get body.id, onlymeta: yes, (err, mail) ->
          expect(mail.mail.html).to.be.undefined
          done err

    it 'should save some stats', (done) ->
      @emails.save
        type: 'drip'
        queued: dt = new Date()
        message: id: id = uuid.v4()
        lead:
          id: lid = uuid.v4()
          '$attributes': attrs = ['1/2', '3/4']
      , (err, body) =>
        @emails.get body.id, onlymeta: yes, (err, mail) ->
          db = factory.single()
          db.query 'SELECT * FROM _emails_stats_drips WHERE id = $1', [body.id], (err, result) ->
            expect(result.rows[0].message).to.equal id
            expect(result.rows[0].lead).to.equal lid
            expect(result.rows[0].queued).to.eql dt
            expect(result.rows[0].attributes).to.eql attrs
            done err

    it 'should perform well, even with uniques', (done) ->
      @timeout 0
      @slow 600

      db = factory.single()
      async.map [1..100], (i, cb) =>
        @emails.save
          type: 'drip'
          sender: email: 'sender@none'
          status: 'a status'
          step: 'step'
        , cb
      , (err, results) ->
        first = _.min _.pluck results, 'id'
        db.query 'SELECT * FROM emails WHERE id >= $1 ORDER BY id', [first], (err, result) ->
          # console.dir result.rows.map (r) -> r.id
          expect(result.rows.length).to.equal 100
          done()
