async = require 'async'

describe 'shared/database/repositories', ->
  describe 'events', ->
    beforeEach ->
      @events = factory.use 'client/events'

    it 'should perform well, even with uniques', (done) ->
      @timeout 0
      @slow 600

      db = factory.single()
      start = moment.utc()
      async.each [1..100], (i, cb) =>
        @events.save
          type: 'an.event'
          client: factory.client
          foo: 'bar'
        , cb
      , (err) ->
        db.query 'SELECT * FROM events WHERE at > $1 ORDER BY id', [start.toISOString()], (err, result) ->
          # console.dir result.rows.map (r) -> r.id
          expect(result.rows.length).to.equal 100
          done()
