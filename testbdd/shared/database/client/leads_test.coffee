async = require 'async'
fs = require 'fs'

describe 'shared/database/repositories', ->
  describe 'leads', ->
    beforeEach ->
      @leads = factory.use 'client/leads'

    it 'should save new lead', (done) ->
      @leads.save
        foo: bar: 'baz'
      , (err, body) =>
        @leads.get body.id, (err, lead) ->
          expect(lead.id).to.match /\w{32}/
          expect(lead.foo.bar).to.equal 'baz'
          done err

    it 'should update lead', (done) ->
      @leads.save
        foo: bar: 'baz'
      , (err, body) =>
        @leads.get body.id, (err, lead) =>
          lead.something = 'other'
          @leads.save lead, (err) =>
            @leads.get body.id, (err, lead) =>
              expect(lead.something).to.equal 'other'
              done err

    it 'should fetch by external information', (done) ->
      @leads.save
        recipient: email: 'by@external'
        external: source: 'asdf', id: 12345
      , (err, body) =>
        @leads.getByExternalId 'asdf', 12345, (err, lead) =>
          expect(lead.recipient.email).to.equal 'by@external'
          done()

    it 'should fetch by recipient email', (done) ->
      @leads.save
        recipient: email: 'by@email'
      , (err, body) =>
        @leads.by_recipient_email 'by@email', (err, leads) =>
          expect(leads).to.have.length 1
          expect(leads[0].recipient.email).to.equal 'by@email'
          done()

    it 'should not allow multiple leads with same external ids', (done) ->
      @leads.save
        recipient: email: 'by@email'
        external: source: 'asdf', id: 12346
      , (err, body) =>
        @leads.save
          recipient: email: 'by@email'
          external: source: 'asdf', id: 12346
        , (err, body) =>
          expect(err).to.not.be.undefined
          done()
