async = require 'async'
fs = require 'fs'

describe 'shared/database/repositories', ->
  describe 'senders', ->
    beforeEach (done) ->
      @senders = factory.use 'client/senders'
      db = factory.single()

      @senders.save
        email: 'Nick.Peeples@gmail.com'
        aliases: ['Nick.Peeples@softvu.com']
      , done

    it 'should retrieve a sender by name or alias', (done) ->
      @senders.by_name_or_alias 'Nick.Peeples@gmail.com', (err, sender) ->
        expect(sender?.email).to.eql 'Nick.Peeples@gmail.com'
        done()

    it 'should retrieve a sender by name or alias case-insensitive', (done) ->
      @senders.by_name_or_alias 'nick.peeples@gmail.com', (err, sender) ->
        expect(sender?.email).to.eql 'Nick.Peeples@gmail.com'
        done()
