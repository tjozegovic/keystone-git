async = require 'async'

describe 'shared/database/repositories', ->
  describe 'settings', ->
    before (done) ->
      users = factory.use 'core/users'
      users.save
        username: 'nick.peeples@softvu.com'
      , (err, body) =>
        @user = body.id
        done()

    beforeEach (done) ->
      @settings = factory.use 'client/settings'

      db = factory.single()
      db.query 'TRUNCATE settings', done

    it 'should save settings', (done) ->
      @settings.update @user,
        foo: 'bar'
      , (err, body) =>
        @settings.get @user, (err, settings) ->
          expect(settings.foo).to.equal 'bar'
          done err

    it 'should 2x update settings', (done) ->
      @settings.update @user,
        foo: 'bar'
      , (err, body) =>
        @settings.update @user,
          foo: 'baz'
        , (err, body) =>
          @settings.get @user, (err, settings) ->
            expect(settings.foo).to.equal 'baz'
            expect(settings.version).to.equal 2
            done err
