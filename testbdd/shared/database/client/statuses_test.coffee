async = require 'async'
fs = require 'fs'

describe 'shared/database/repositories', ->
  describe 'leads', ->
    beforeEach (done) ->
      @statuses = factory.use 'client/statuses'
      db = factory.single()
      db.query 'TRUNCATE data', done

    it 'should retrieve a status by name or alias', (done) ->
      @statuses.save
          name: 'Nurture'
          aliases: ['New Prospect', 'Viewed']
      , (err, body) =>
        @statuses.byalias 'Viewed', (err, status) ->
          expect(status.name).to.equal 'Nurture'

        @statuses.byalias 'Nurture', (err, status) ->
          expect(status.aliases[0]).to.equal 'New Prospect'
          done err
