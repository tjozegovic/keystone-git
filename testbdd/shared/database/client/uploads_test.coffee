async = require 'async'
fs = require 'fs'

describe 'shared/database/repositories', ->
  describe 'uploads', ->
    beforeEach ->
      @uploads = factory.use 'client/uploads'

    it 'should save new upload', (done) ->
      @uploads.save
        type: 'lead.import'
        foo: bar: 'baz'
      , fs.createReadStream('testbdd/shared/database/client/leads.csv')
      , (err, body) =>
        @uploads.get body.id, (err, upload) ->
          expect(upload.type).to.equal 'lead.import'
          expect(upload.foo.bar).to.equal 'baz'
          expect(upload.file.toString()).to.contain 'nick,peeples'
          expect(upload.file.toString()).to.contain 'velocify,1235'
          done err

    it 'should update upload', (done) ->
      @uploads.save
        type: 'lead.import',
        foo: bar: 'baz'
      , (err, body) =>
        @uploads.get body.id, (err, upload) =>
          upload.status = 'done'
          @uploads.save upload, (err) =>
            @uploads.get body.id, (err, upload) =>
              expect(upload.status).to.equal 'done'
              done err
