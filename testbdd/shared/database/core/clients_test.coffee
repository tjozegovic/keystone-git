async = require 'async'

describe 'shared/database/repositories', ->
  describe 'clients', ->
    before ->
      @clients = factory.use 'core/clients'

    it 'should get client by shortname', (done) ->
      # relys on the shortname of the client that is created by default
      @clients.by_shortname 'tc', (err, client) ->
        expect(client.name).to.equal 'Testing Client'
        done err

    it 'should get client by svemails prefix', (done) ->
      @clients.get factory.client, (err, client) =>
        _.assign client, use_svemails: yes, svemails: prefix: 'test'
        @clients.save client, =>
          @clients.by_svemails_prefix 'test', (err, client) ->
            expect(client.name).to.equal 'Testing Client'
            done err

    it 'should not get disabled svemails client', (done) ->
      @clients.get factory.client, (err, client) =>
        _.assign client, use_svemails: no
        @clients.save client, =>
          @clients.by_svemails_prefix 'test', (err, client) ->
            expect(client).to.not.exist
            done err

  describe 'cnames', ->
    before ->
      [@cnames, @clients] = factory.use 'core/cnames', 'core/clients'

    it 'should get client by shortname', (done) ->
      # relys on the shortname of the client that is created by default
      @cnames.get 'tc.localhost', (err, client) ->
        expect(client).to.equal factory.client
        done err

    it 'should get client by shortname', (done) ->
      @clients.get factory.client, (err, client) =>
        _.assign client, cnames: [host: 'tc.localhost', use_https: no]
        @clients.save client, =>
          # relys on the shortname of the client that is created by default
          @cnames.get 'tc.localhost', (err, client) ->
            expect(client).to.equal factory.client
            done err
