async = require 'async'

describe 'shared/database/repositories', ->
  describe 'core/jobs', ->
    beforeEach ->
      @jobs = factory.use 'core/jobs'

    it 'should save new job', (done) ->
      @jobs.save
        action: 'test'
        status: 'pending'
      , (err, body) =>
        @jobs.get body.id, (err, job) ->
          expect(job.action).to.equal 'test'
          done err

    it 'should update job', (done) ->
      @jobs.save
        action: 'test'
        status: 'pending'
      , (err, body) =>
        @jobs.get body.id, (err, job) =>
          job.running (err) =>
            @jobs.get body.id, (err, job) =>
              expect(job.status).to.equal 'running'
              done err

    it 'should get pending jobs', (done) ->
      @jobs.save
        action: 'test'
        status: 'pending'
      , (err, body) =>
        id = body.id
        @jobs.pending (err, jobs) ->
          [..., last] = jobs
          expect(last.id).to.equal id
          expect(last.status).to.equal 'pending'
          done err

    it 'should cancel jobs', (done) ->
      @jobs.save
        action: 'canceltest'
        status: 'pending'
      , (err, body) =>
        @jobs.get body.id, (err, job) =>
          job.canceled =>
            @jobs.get body.id, (err, job) =>
              expect(job.status).to.equal 'canceled'
              done err
