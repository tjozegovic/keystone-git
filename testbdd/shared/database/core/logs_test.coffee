moment = require 'moment'

describe 'shared/database/repositories', ->
  describe 'core/logs', ->
    beforeEach ->
      @logs = factory.use 'core/logs'

    it 'should save a log', (done) ->
      @logs.insert
        client: factory.client
        at: '2012-01-01T13:05'
      , (err, body) =>
        @logs.get body.id, (err, job) ->
          expect(job.client).to.equal factory.client
          expect(moment.utc('2012-01-01T13:05').isSame job.at).to.be.ok
          done err
