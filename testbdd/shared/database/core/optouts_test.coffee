async = require 'async'

describe 'shared/database/repositories', ->
  describe 'optouts', ->
    before (done) ->
      @optouts = factory.use 'core/optouts'
      @optouts.optout 'nick.peeples@softvu.com', factory.client, done

    it 'should save new optout', (done) ->
      @optouts.get 'nick.peeples@softvu.com', (err, optout) ->
        expect(optout.clients).to.contain factory.client
        done err

    it 'should update optout', (done) ->
      @optouts.optout 'nick.peeples@softvu.com', '12345'
      , (err, body) =>
        @optouts.get 'nick.peeples@softvu.com', (err, optout) ->
          expect(optout.clients).to.contain '12345'
          done err

    it 'should handle case sensitivity', (done) ->
      @optouts.get 'Nick.Peeples@softvu.com', (err, optout) ->
        expect(optout.clients).to.contain factory.client
        done err
