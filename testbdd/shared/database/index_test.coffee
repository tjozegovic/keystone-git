_ = require 'lodash'

{SingleQueryDatabase, MultiQueryDatabase} = require 'shared/database'

describe 'shared/database', ->
  before ->
    factory.cleanup()

  describe 'factory gives back repositories with proper drivers', ->
    it 'should allow use with correct version, singlquery', ->
      clients = factory.use 'core/clients'
      expect(clients.db).to.be.instanceof SingleQueryDatabase
      expect(factory._tracked).to.be.empty

    it 'should allow use with correct version, multiquery', ->
      clients = factory.use 'core/clients', multiquery: yes
      expect(clients.db).to.be.instanceof MultiQueryDatabase
      expect(factory._tracked).to.have.length 1

    it 'should close db connections', ->
      clients = factory.use 'core/clients', multiquery: yes
      factory.cleanup()
      expect(factory._tracked).to.be.empty
