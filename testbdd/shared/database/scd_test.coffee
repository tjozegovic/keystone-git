_ = require 'lodash'

describe 'shared/database', ->
  describe 'slowly changing dimension (scd)', ->
    emails = {}

    it 'should allow updates with correct version', (done) ->
      status = segments: [
        id: 'segment'
        steps: [{ id: 'step', notify: yes, message: id: 'message' }]
      ]

      statuses = factory.use 'client/statuses'
      statuses.save status, (err, body) ->
        _.assign status, body
        statuses.save status, ->
          statuses.get status.id, (err, status) ->
            expect(status.version).to.equal 2
            done err

    it 'should allow fetching', (done) ->
      status = segments: [
        id: 'segment'
        steps: [{ id: 'step', notify: yes, message: id: 'message' }]
      ]

      statuses = factory.use 'client/statuses'
      async.parallel [
        (cb) -> statuses.save status, cb
        (cb) -> statuses.save status, cb
        (cb) -> statuses.save status, cb
      ], (err, results) ->
        statuses.fetch _.pluck(results, 'id'), (err, results) ->
          expect(results).to.have.length 3
          done err
