{filter} = require 'shared/util'

describe 'shared/util', ->
  describe 'filter', ->
    it 'should work', ->
      obj =
        a: 'a'
        b: 'b'

      out = filter obj, ['a']
      expect(out.a).to.equal 'a'
      expect(out.b).to.be.not.defined

    it 'should work with nested objects', ->
      obj =
        a: 'a'
        b: 'b'
        c:
          a: 'a'
          b: 'b'

      out = filter obj, ['a', 'c.a']
      expect(out.c.a).to.equal 'a'
      expect(out.c.b).to.be.not.defined

    it 'should auto include arrays', ->
      obj =
        a: 'a'
        b: 'b'
        c:
          a: 'a'
          b: 'b'
        d: ['a', 'b']

      out = filter obj, ['a', 'd']
      expect(out.d).to.have.members ['a', 'b']

    it 'should not add properties due to branches', ->
      obj =
        a: 'a'
        b: 'b'
        c:
          a: 'a'
          b: 'b'
        d: ['a', 'b']

      out = filter obj, ['a', 'd', 'e.a']
      expect(out.e).to.not.be.defined
