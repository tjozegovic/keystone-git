config = require 'config'

async = require 'async'
faker = require 'faker/locale/en_US'
request = require 'request'
_ = require 'lodash'
inquirer = require 'inquirer'
{RepositoryFactory} = require 'shared/database'

factory = new RepositoryFactory config.db.url
softvu = factory.use 'core/clients'

softvu.all (err, body) ->
  return err if err?

  activeClients = _(body)
    .reject 'archive'
    .sortBy 'name'
    .map _.partial _.pick, _, ['id', 'name']
    .value()

  activeClientsNames = _(activeClients).pluck('name').value()

  questions = [
    {
      type: 'list'
      name: 'client'
      message: 'Choose a client.'
      choices: activeClientsNames
    }
  ]

  inquirer.prompt(questions, (answers) ->
    clientId = _.result(_.find(activeClients, 'name', answers.client), 'id')
    factory.client = clientId
    apikeys = factory.use 'core/apikeys'
    apikeys.byclient clientId, (err, body) ->
      return err if err?

      clientKey = body[0].key
      directApi = config.api.url + '/v1/send/_bulk?key=' + clientKey

      senders = factory.use 'client/senders'
      senders.all (err, body) ->
        sendersEmails = _(body)
          .filter 'active'
          .sortBy 'email'
          .pluck 'email'
          .value()

        clientQuestions = [
          {
            type: 'list'
            name: 'sender'
            message: 'Choose a sender.'
            choices: sendersEmails
          }
          {
            type: 'input'
            name: 'slug'
            message: 'Enter the slug name.'
          }
          {
            type: 'input'
            name: 'quantity'
            message: 'How many direct sends would you like to create?'
            validate: (value) ->
              valid = !isNaN(parseFloat(value))
              valid || 'Please enter a number'
          }
        ]

        inquirer.prompt(clientQuestions, (answers) ->
          total = answers.quantity
          complete = 0
          iterations = _.ceil(answers.quantity/100)

          send = (input) ->
            json = []
            async.series [
              (cb) ->
                _.times(input, ->
                  json.push
                    message: answers.slug
                    agent: answers.sender
                    lead: email: faker.internet.email()
                )
                cb null
              (cb) ->
                request.post directApi,
                  json: json
                , (err, response, body) ->
                  complete += json.length
                  console.log _.ceil((complete/total) * 100) + '% sent'
                  console.log response.statusCode
            ]

          calculate = (i) ->
            if answers.quantity >= 100
              answers.quantity -= 100
              input = 100
              setTimeout ->
                send(input)
              , i * 1000
            else
              input = answers.quantity
              answers.quantity = 0
              setTimeout ->
                send(input)
              , i * 1000

          calculate(i) for i in [0...iterations]
        )
  )
