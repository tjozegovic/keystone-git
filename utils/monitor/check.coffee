fs = require 'fs'
os = require 'os'
path = require 'path'

_ = require 'lodash'
moment = require 'moment'
nodemailer = require 'nodemailer'

ROOT = path.join __dirname, '../..'

module.exports.get_info = get_info = (rpath) ->
  apath = path.join ROOT, rpath
  name: rpath.split('/')[0]
  file: apath
  last: moment (fs.statSync apath).mtime

module.exports.get_files = get_files = (items) ->
  unless _.isArray items
    items = [items]

  files = _(items).map (i) ->
    stat = fs.statSync path.join ROOT, i
    if stat.isDirectory()
      _(fs.readdirSync path.join ROOT, i)
        .map (file) ->
          get_info path.join i, file
        .sortBy 'last'
        .reverse()
        .first()
    else
      get_info i

  files
    .filter (file) ->
      moment().diff(file.last, 'minutes') >= 12
    .value()

if require.main is module
  argv = (require 'yargs')
    .boolean('d', describe: 'debug')
    .argv

  files = get_files argv._

  if files.length
    ip = _(os.networkInterfaces()).values().flatten()
      .filter((iface) -> iface.family is 'IPv4' and not iface.internal)
      .first().address

    tonotice = (file) -> """
service #{file.name} might be down?
#{file.file} was lasted updated #{file.last.calendar()}
first step is to try restarting "keystone.#{file.name}" on #{os.hostname()} (#{ip})
"""

    if argv.d
      console.log _.map(files, tonotice).join '\n\n'
    else
      transport = nodemailer.createTransport()
      transport.sendMail
        to: 'notices@softvu.com'
        from: 'job+keystone-check@softvu.com'
        subject: "#{os.hostname()} (#{ip}) keystone services offline"
        text: _.map(files, tonotice).join '\n\n'
