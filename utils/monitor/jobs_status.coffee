_ = require 'lodash'
async = require 'async'
moment = require 'moment'
nano = require 'nano'
nodemailer = require 'nodemailer'

module.exports.jobs_status = jobs_status = (url, cb) ->
  db = nano
    url: url
    requestDefaults: timeout: 5000

  handle_error = (fail, success) ->
    (err, body) ->
      return success body unless err?

      return fail null, 'timeout' if err.code in ['ETIMEDOUT', 'ESOCKETTIMEDOUT']
      return fail null, 0 if err.message in ['missing', 'deleted']
      fail err

  async.parallel
    pending: (cb) ->
      # when emails are scheduled, sometimes the jobs are given times way in the past based on segmentation and a few
      # other rules... make sure we have no more than 10 pending jobs that are older than 5 minutes
      endkey = moment.utc().subtract(5, 'minutes').toISOString()
      db.view 'jobs', 'pending_by_time', limit: 10, endkey: endkey, handle_error cb, (body) ->
        cb null, body.rows.length >= 10
    running: (cb) ->
      endkey = moment.utc().subtract(15, 'minutes').toISOString()
      db.view 'jobs', 'still_running', limit: 1, endkey: endkey, handle_error cb, (body) ->
        cb null, body.rows.length > 0
  , (err, results) ->
    return cb err if err
    cb null, results

if require.main is module
  argv = (require 'yargs')
    .option('u', describe: 'url')
    .boolean('d', describe: 'debug')
    .argv

  jobs_status argv.u, (err, results) ->
    return console.error err if err?
    return unless results.pending or results.running
    notice = 'jobs status:\n\n'

    if results.pending
      if results.pending is 'timeout'
        notice += '  * pending jobs TIMEOUT\n'
      else
        notice += '  * there are still pending jobs\n'

      notice += "    #{argv.u}/_design/jobs/_view/pending_by_time?limit=1\n"

    if results.running
      if results.pending is 'timeout'
        notice += '  * running jobs TIMEOUT\n'
      else
        notice += '  * there are still running jobs\n'

      notice += "    #{argv.u}/_design/jobs/_view/still_running\n"

    notice += '\nplease investigate.. check superadmin jobs, and restart \'actionqueue\' if necessary'

    if argv.d
      console.log notice
    else
      transport = nodemailer.createTransport()
      transport.sendMail
        to: 'notices@softvu.com'
        from: 'job+keystone-check@softvu.com'
        subject: "#{argv.u} keystone jobs alert"
        text: notice
