#!/usr/bin/env coffee

_ = require 'lodash'
mailer = require 'nodemailer'
moment = require 'moment'
request = require 'request'

argv = (require 'yargs')
  .describe 'h', 'Host of rabbit server (domain/ip)'
  .options 'a', default: 'guest:guest', describe: 'Rabbit credentials (username:password)'
  .options 't', default: 'notices@softvu.com', describe: 'Notify email address(es)'
  .boolean 'd', describe: 'debug'
  .demand 'h'
  .argv

patterns = _.map argv._, (p) -> new RegExp p
matches = (name) -> _.some patterns, (p) -> p.test name

request "http://#{argv.a}@#{argv.h}:15672/api/queues", (err, res, body) ->
  queues = JSON.parse body

  data = _.filter queues, (q) -> matches q.name
  size = _.reduce _.pluck(data, 'messages'), (sum, num) -> sum += num

  console.log 'check queues (%s) on http://%s:15672', argv._.join(', '), argv.h if argv.d

  if size > 1000
    if argv.d
      console.log 'total size: %s', size
      console.log '\n%s', JSON.stringify _.zipObject(_.pluck(data, 'name'), _.pluck(data, 'messages')), null, '  '
    else
      transport = mailer.createTransport()
      transport.sendMail
        to: argv.t
        from: 'job+queue-check@softvu.com'
        subject: "Queues are large: #{size}"
        text: "check queues (#{argv._.join ', '}) on http://#{argv.h}:15672"
