#!/usr/bin/env coffee

process.env.NODE_CONFIG_DIR ?= '../../config'
config = require 'config'
console.dir config

fs = require 'fs'
async = require 'async'
byline = require 'byline'
pg = require 'pg'

# {RepositoryFactory} = require 'shared/factory'
# factory = new RepositoryFactory 'postgres://keystone:s0ftvu@SOFTVUPG01/keystone'

isValidEmailAddress = (email) ->
  atidx = email.indexOf '@'
  email.length >= 3 && ~atidx && atidx < email.length - 1

stats =
  total: 0
  updates: 0
  inserts: 0
  errors: []

# TODO generate a start event? create a name based on session user and file name?
start = new Date

client_id = '538fc8b0242511e5a8d231652d8b8fea'
file = '/Users/nick/Documents/softvu/clients/loanone optouts/Loan One-Opt-Out Upload (10001-end).csv'

stream = fs.createReadStream file
through = byline.createStream(stream).on 'readable', ->
  client = new pg.Client 'postgres://keystone:s0ftvu@SOFTVUPG01/keystone'
  client.connect (err) ->
    go = (line, next) ->
      ++ stats.total

      email = line.toString().trim().toLowerCase()

      unless isValidEmailAddress email
        stats.errors.push address: email, message: 'invalid email address'

      client.query 'SELECT doc FROM optouts WHERE id = $1 LIMIT 1', [email], (err, results) ->
        if row = results.rows[0]
          {doc} = row

          unless client_id in doc.clients ?= []
            doc.clients.push client_id
            doc.clients.sort()

          doc.history.unshift
            type: 'optout'
            at: new Date()
            client: client_id
            from: 'website'

          console.log 'updating: %s', email
          client.query 'UPDATE optouts SET updated = $2, doc = $3 WHERE id = $1', [email, new Date, doc], (err) ->
            ++ stats.updates
            next()
        else
          doc = clients: [], history: []
          doc.clients.push client_id
          doc.history.unshift
            type: 'optout'
            at: new Date()
            client: client_id
            from: 'website'

          console.log 'creating: %s', email
          client.query 'INSERT INTO optouts (id, doc) VALUES ($1, $2)', [email, doc], (err) ->
            ++ stats.inserts
            next()

    queue = async.queue go, 100
    queue.drain = ->
      console.log stats
      client.end()

    while (line = through.read())?
      queue.push line
