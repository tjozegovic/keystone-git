#!/usr/bin/env coffee

if ~process.cwd().indexOf 'pg-migration'
  process.env.NODE_CONFIG_DIR ?= '../../config'

config = require 'config'
{COUCH_URL} = require './config'

_ = require 'lodash'
async = require 'async'
crypto = require 'crypto'
nano = require 'nano'

pg = require 'pg'
squel = require('squel').useFlavour 'postgres'
squel.cls.DefaultQueryBuilderOptions.tableAliasQuoteCharacter = '"'

{RepositoryFactory} = require 'shared/database'

console.time 'import core'
server = nano COUCH_URL
couchdb = server.use 'softvu'

factory = new RepositoryFactory config.db.url
pgdb = factory.single()

couchdb.list include_docs: yes, (err, list) ->
  return cb err if err

  clients = _(list.rows)
    .pluck 'doc'
    .filter (doc) -> /^[A-Za-z0-9]{32}$/.test doc._id
    .filter (doc) -> doc.type is 'client'
    .value()

  users = _(list.rows)
    .pluck 'doc'
    .filter (doc) -> doc.type is 'user'
    .value()

  apikeys = _(list.rows)
    .pluck 'doc'
    .filter (doc) -> /^[A-Za-z0-9]{32}$/.test doc._id
    .filter (doc) -> doc.type is 'apikey'
    .value()

  templates = _(list.rows)
    .pluck 'doc'
    .filter (doc) -> /^template-.*/.test doc._id
    .value()

  console.log 'clients', clients.length #, clients
  console.log 'users', users.length #, users
  console.log 'keys', apikeys.length #, apikey
  console.log 'templates', templates.length #, apikey

  _md5 = (obj) ->
    sort = (obj) ->
      _.keys(obj).sort().reduce (memo, key) ->
        if _.isObject obj[key]
          memo[key] = sort obj[key]
        else
          memo[key] = obj[key]
        memo
      , {}

    ordered = JSON.stringify sort obj
    crypto.createHash('md5').update(ordered).digest 'hex'

  async.parallel [
    (next) ->
      async.each clients, (client, cb) ->
        id = client._id
        client = _.omit client, '_id', '_rev', '_attachments'

        sql = squel.select()
          .from 'clients_current'
          .where 'id = ?', id
          .limit 1
        pgdb.query sql, (err, results) ->
          return cb err if err?

          sql = undefined
          json = JSON.stringify client
          if results.rows.length
            return cb() if _md5(client) is _md5(results.rows[0].doc)

            sql = squel.update()
              .table 'clients'
              .set 'doc', "$$#{json}$$", dontQuote: yes
              .where 'id = ?', id
              .where 'version = ?', results.rows[0].version
              .returning 'id, version'
          else
            sql = squel.insert()
              .into 'clients'
              .set 'id', id
              .set 'doc', "$$#{json}$$", dontQuote: yes
              .returning 'id, version'

          pgdb.query sql, cb
      , next
    (next) ->
      async.each users, (user, cb) ->
        id = user._id
        user = _.omit user, '_id', '_rev', '_attachments'

        sql = squel.select()
          .from 'users'
          .where 'username = ?', id
        pgdb.query sql, (err, results) ->
          return cb err if err?
          return cb() if results.rows.length

          sql = squel.insert()
            .into 'users'
            .set 'username', id
            .set 'doc', "$$#{JSON.stringify user}$$", dontQuote: yes

          pgdb.query sql, cb
      , next
    (next) ->
      async.each templates, (template, cb) ->
        id = template._id.split('-')[1]
        template = _.omit template, '_id', '_rev', '_attachments'

        sql = squel.select()
          .from 'templates'
          .where 'name = ?', id
        pgdb.query sql, (err, results) ->
          return cb err if err?
          return cb() if results.rows.length

          sql = squel.insert()
            .into 'templates'
            .set 'name', id
            .set 'doc', "$$#{JSON.stringify template}$$", dontQuote: yes

          pgdb.query sql, cb
      , next
    (next) ->
      async.each apikeys, (key, cb) ->
        id = key._id
        key = _.omit key, '_id', '_rev', '_attachments'

        sql = squel.select()
          .from 'apikeys'
          .where 'id = ? and client_id = ?', id, key.client
        pgdb.query sql, (err, results) ->
          return cb err if err?
          return cb() if results.rows.length

          sql = squel.insert()
            .into 'apikeys'
            .set 'id', id
            .set 'client_id', key.client
            .set 'issued', key.issued

          pgdb.query sql, cb
      , next
  ], (err) ->
    pg.end()
    console.error err if err?
    console.timeEnd 'import core'
