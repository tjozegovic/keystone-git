#!/usr/bin/env coffee

if ~process.cwd().indexOf 'pg-migration'
  process.env.NODE_CONFIG_DIR ?= '../../config'

config = require 'config'
{COUCH_URL, EXCLUDE, EPOCH} = require './config'

_ = require 'lodash'
async = require 'async'
cluster = require 'cluster'
moment = require 'moment'
nano = require 'nano'
readline = require 'readline'
stringify = require 'fast-stable-stringify'
uuid = require 'uuid'

pg = require 'pg'
{Database, RepositoryFactory} = require 'shared/database'

_throttle = 500
id_re = /^[A-Za-z0-9]{32}$/

console.time 'import data'
server = nano
  url: COUCH_URL
  requestDefaults:
    agent: new require('agentkeepalive')
      maxSockets: 20
      maxKeepAliveRequests: 0
      maxKeepAliveTime: 30000
couchdb = server.use 'softvu'

factory = new RepositoryFactory config.db.url

if cluster.isMaster
  rl = readline.createInterface
    input: process.stdin
    output: process.stdout
  rl.setPrompt 'paused [resume, skip, quit] > '

  create_worker = ->
    worker = cluster.fork()
    worker.send = do (oldsend = worker.send.bind(worker)) ->
      (type, msg, cb) ->
        oldsend _.assign(type: type, msg or {}), null, (err) ->
          console.error 'worker message send error', err, err.stack if err?
          cb?()

    worker

  couchdb.view 'objects', 'clients', startkey: ['all'], endkey: ['all', {}], include_docs: yes, (err, body) ->
    # loop through all clients in series
    async.eachSeries body.rows, (client, nextclient) ->
      _worker = null
      _feed = null
      _pgdb = null

      _curr_seq = 0
      _migration_id = -1

      _state = 'running'
      _workerstart = undefined

      _stats = sent: 0, acked: 0

      rl.on 'SIGINT', on_sigint = ->
        console.log 'pausing... togo: %d', _stats.sent - _stats.acked
        _state = 'pausing'
        _feed.pause()

        if _stats.sent is _stats.acked
          _state = 'paused'
          rl.prompt()

      rl.on 'line', on_line = (line) ->
        return unless _state is 'paused'

        switch line.trim()
          when 'resume'
            _state = 'running'
            _feed.resume()
          when 'skip'
            console.log 'skipping to next client'
            _state = 'skipping'
            _feed.stop() if _state is 'running'
            nextclient() if _stats.sent is _stats.acked
          when 'quit'
            console.log 'quitting...'
            _state = 'quitting'
            _feed.stop() if _state is 'running'
            nextclient 'shutdown' if _stats.sent is _stats.acked
          else
            console.log 'bad command %s', line.trim()
            rl.prompt()

      process.on 'uncaughtException', on_uncaught = (ex) ->
        console.error 'master exception %s\n%s', ex.message, ex.stack
        _state = 'quitting'
        _feed.stop() if _state is 'running'
        nextclient 'shutdown' if _stats.sent is _stats.acked

      nextclient = do (next = nextclient) -> (args...) ->
        cont = ->
          rl.removeListener 'SIGINT', on_sigint
          rl.removeListener 'line', on_line
          process.removeListener 'uncaughtException', on_uncaught
          return next args... unless _pgdb?

          console.log 'update migrations id: %s seq: %s', _migration_id, _curr_seq
          _pgdb.query 'UPDATE migrations SET "end" = NOW(), end_seq = $2 WHERE id = $1', [_migration_id, _curr_seq], (err, results) ->
            return next err if err?
            next args...

        if _worker?
          _worker.send 'finalize'
          _worker.on 'exit', cont
        else
          cont()

      if client.id in EXCLUDE
        console.log 'skipping client', client.id, client.doc.name
        return nextclient()

      start_migration = ->
        _worker = create_worker()
        clientdb = server.use "client-#{client.id}-data"
        server.db.get "client-#{client.id}-data", (err, info) ->
          info.id = client.id

          console.log 'starting migration (%s - %s) id: %d seq: %d', client.id, client.doc.name, _migration_id, _curr_seq
          _feed = clientdb.follow since: _curr_seq, include_docs: false

          _feed.filter = (doc, req) ->
            return /^[A-Za-z0-9]{32}$/.test doc._id

          _feed.on 'change', (change) ->
            _curr_seq = change.seq
            _worker.send 'item', change

            if ++_stats.sent - _stats.acked > _throttle * 2
              _state = 'throttled'
              _feed.pause()

          _feed.on 'catchup', (seq) ->
            _state = 'stopping'
            _feed.stop()
            nextclient() unless _stats.sent

          ['retry', 'timeout', 'error'].forEach (evt) ->
            _feed.on evt, -> console.log evt, arguments...

          _worker.on 'message', (msg) ->
            switch msg.type
              when 'ready'
                _workerstart = process.hrtime()
                _feed.follow()
              when 'resume'
                _feed.resume()
              when 'ack'
                _stats.acked++

                # TODO move this to master..?
                if _state isnt 'pausing' and _stats.acked % _throttle is 0
                  end = process.hrtime _workerstart
                  totalsec = end[0] + end[1] / 1e9

                  ravg = (_stats.acked / totalsec).toFixed(4)
                  left = info.update_seq - msg.seq # approx.. msg.seq may not be the max sequence
                  console.log 'processed %d, %d/s avg. (%d remaining, est. %d min. remaining)', _stats.acked, ravg, left, ((left / ravg) / 60).toFixed(2)

                if _state is 'pausing' and _stats.sent is _stats.acked
                  _state = 'paused'
                  rl.prompt()
                if _state is 'throttled' and _stats.sent - _stats.acked <= _throttle
                  _state = 'running'
                  _feed.resume()
                else if _state in ['stopping', 'quitting'] and _stats.sent is _stats.acked
                  end = process.hrtime _workerstart
                  totalsec = end[0] + end[1] / 1e9
                  console.log 'client processed, total: %d', _stats.sent
                  console.log 'processed avg. %d per second', _stats.sent / totalsec

                  nextclient if _state is 'quitting' then 'shutdown' else null
                  _state = 'stopped'

          _worker.send 'info', info

      factory.client = client.id
      _pgdb = factory.single()

      _pgdb.query """
        CREATE TABLE IF NOT EXISTS migrations (
          id serial,
          "table" text NOT NULL,
          start timestamptz DEFAULT NOW(),
          "end" timestamptz,
          end_seq int
        );
      """, (err, result) ->
        return nextclient err if err?

        _pgdb.query 'SELECT end_seq FROM migrations WHERE "table" = $1 AND end_seq IS NOT NULL ORDER BY id DESC LIMIT 1', ['data'], (err, results) ->
          return nextclient err if err?

          _curr_seq = if results.rows.length
            results.rows[0].end_seq || 0
          else
            0

          _pgdb.query "INSERT INTO migrations (\"table\") VALUES ('data') RETURNING id", (err, results) ->
            return nextclient err if err?

            _migration_id = results.rows[0].id
            start_migration()
    , (err) ->
      pg.end()

      console.timeEnd 'import data'
      process.exit()
else
  if process.platform is 'win32'
    rl = require('readline').createInterface
      input: process.stdin,
      output: process.stdout

    rl.on 'SIGINT', ->
      # process.emit 'SIGINT'

  process.send = do (oldsend = process.send.bind(process)) ->
    (type, msg, cb) ->
      oldsend _.assign(type: type, msg or {}), null, (err) ->
        console.error 'worker message send error', err, err.stack if err?
        cb?()

  _pgdb = undefined
  clientdb = undefined

  # set pg client to root to the new client schema
  _info = undefined
  _processed = 0

  wocouch = (doc) -> _.omit doc, '_id', '_rev', '_attachments'

  start = ->
    _pgdb.query """
      DO $$BEGIN
        DROP INDEX IF EXISTS leads_segment_ix;
        DROP INDEX IF EXISTS data_types_ix;
        DROP INDEX IF EXISTS data_scd_ix;
        DROP INDEX IF EXISTS data_scdcurrent_ix;
        DROP INDEX IF EXISTS data_senderaliases_ix;

        CREATE TABLE IF NOT EXISTS oldrevs (
          db text NOT NULL,
          id text NOT NULL,
          rev text NOT NULL
        );

        IF (SELECT to_regclass('oldrevs_ix') IS NULL) THEN
          CREATE INDEX oldrevs_ix ON oldrevs (db, id, rev);
        END IF;
      END$$;
    """, (err, result) ->
      # move over attributes from old couchdb doc
      clientdb.get 'attributes', (err, body) ->
        async.forEachOf body?.attributes or {}, (attribute, id, cb) ->
          attribute.active = yes
          sql = 'SELECT * FROM data_current WHERE id = $1 LIMIT 1'
          _pgdb.query sql, [id], (err, results) ->
            sql = if results.rows.length > 0
              "UPDATE data SET doc = $2::jsonb WHERE id = $1 AND version = #{results.rows[0].version}"
            else
              'INSERT INTO data (id, type, doc) VALUES ($1, \'attribute\', $2::jsonb)'

            _pgdb.query sql, [id, stringify attribute], cb,
        , (err) ->
          console.error 'could not insert attributes', err if err?
          process.send 'ready'

  q =
    select_oldrevs: 'SELECT 1 FROM oldrevs WHERE db = $1 AND id = $2 AND rev = $3 LIMIT 1'
    insert_oldrevs: 'INSERT INTO oldrevs (db, id, rev) VALUES ($1, $2, $3)'

    select: 'SELECT 1 FROM data WHERE id = $1 LIMIT 1'
    delete: 'DELETE FROM data WHERE id = $1'
    template:
      ins: 'INSERT INTO data (id, type, doc) VALUES ($1, \'template\', $2::jsonb)'
      upd: 'UPDATE data SET doc = $2::jsonb WHERE id = $1 AND version = (SELECT MAX(version) FROM data_current WHERE id = $1)'
      insv: 'INSERT INTO data (id, type, valid, doc) VALUES ($1, \'template\', $2, $3::jsonb)'
      updv: 'UPDATE data SET valid = $2, doc = $3::jsonb WHERE id = $1 AND version = (SELECT MAX(version) FROM data_current WHERE id = $1)'
    batch:
      ins: 'INSERT INTO batches (id, type, status, doc) VALUES ($1, \'batch\', $2, $3::jsonb)'
      upd: 'UPDATE batches SET status = $2, doc = $3::jsonb WHERE id = $1 AND version = (SELECT MAX(version) FROM batches_current WHERE id = $1)'
      insv: 'INSERT INTO batches (id, type, valid, status, doc) VALUES ($1, \'batch\', $2, $3, $4::jsonb)'
      updv: 'UPDATE batches SET valid = $2, status = $3, doc = $4::jsonb WHERE id = $1 AND version = (SELECT MAX(version) FROM batches_current WHERE id = $1)'
    lead:
      ins: 'INSERT INTO leads (id, type, doc) VALUES ($1, \'lead\', $2::jsonb)'
      upd: 'UPDATE leads SET doc = $2::jsonb WHERE id = $1 AND version = (SELECT MAX(version) FROM leads_current WHERE id = $1)'
      insv: 'INSERT INTO leads (id, type, valid, doc) VALUES ($1, \'lead\', $2, $3::jsonb)'
      updv: 'UPDATE leads SET valid = $2, doc = $3::jsonb WHERE id = $1 AND version = (SELECT MAX(version) FROM leads_current WHERE id = $1)'
    data:
      ins: 'INSERT INTO data (id, type, doc) VALUES ($1, $2, $3::jsonb)'
      upd: 'UPDATE data SET type = $2, doc = $3::jsonb WHERE id = $1 AND version = (SELECT MAX(version) FROM data_current WHERE id = $1)'
      insv: 'INSERT INTO data (id, type, valid, doc) VALUES ($1, $2, $3, $4::jsonb)'
      updv: 'UPDATE data SET type = $2, valid = $3, doc = $4::jsonb WHERE id = $1 AND version = (SELECT MAX(version) FROM data_current WHERE id = $1)'

  process_rows = (row) ->
    cb = -> process.send 'ack', seq: row.seq

    # console.log row
    {doc, deleted} = row
    _pgdb.query q.select_oldrevs, ['data', row.id, row.changes[0].rev], (err, result) ->
      # we've already processed this message, just ignore it
      return cb null if result.rows.length

      if deleted
        return _pgdb.query q.delete, [id], (err) ->
          console.error err if err?
          cb()

      id = doc._id
      {created, updated} = doc
      valid = if updated? or created?
        "[#{updated or created}, Infinity)"

      insert_template = (template, next) ->
        _processed.templates += 1

        async.parallel
          html: (cb) -> clientdb.attachment.get template._id, 'template.html', (err, body) -> cb null, body
          text: (cb) -> clientdb.attachment.get template._id, 'template.txt', (err, body) -> cb null, body
        , (err, results) ->
          template[name] = buffer?.toString() or '' for name, buffer of results

          # TODO handle errors? put in an array, try again?
          # TODO csv files
          template = _.omit template, '_id', '_rev', '_attachments'
          if valid?
            _pgdb.query q.template[if _exists then 'updv' else 'insv'], [id, valid, stringify template], next
          else
            _pgdb.query q.template[if _exists then 'upd' else 'ins'], [id, stringify template], next

      insert_batch = (batch, next) ->
        _processed.batches += 1

        {status} = batch
        batch = _.omit batch, '_id', '_rev', '_attachments', 'status'

        if valid?
          _pgdb.query q.batch[if _exists then 'updv' else 'insv'], [id, valid, status, stringify batch], next
        else
          _pgdb.query q.batch[if _exists then 'upd' else 'ins'], [id, status, stringify batch], next

      insert_lead = (lead, next) ->
        _processed.leads += 1

        lead.$status ?= lead.status if lead.status? and id_re.test lead.status

        lead = _.omit lead, '_id', '_rev', '_attachments'
        if valid?
          _pgdb.query q.lead[if _exists then 'updv' else 'insv'], [id, valid, stringify lead], next
        else
          _pgdb.query q.lead[if _exists then 'upd' else 'ins'], [id, stringify lead], next

      insert_data = (data, next) ->
        _processed.data += 1

        data = _.omit data, '_id', '_rev', '_attachments'
        if valid?
          _pgdb.query q.data[if _exists then 'updv' else 'insv'], [id, data.type or 'n/a', valid, stringify data], next
        else
          _pgdb.query q.data[if _exists then 'upd' else 'ins'], [id, data.type or 'n/a', stringify data], next

      _exists = no
      _pgdb.query q.select, [id], (err, results) ->
        _exists = results.rows.length > 0

        fn = switch doc.type
          when 'batch' then insert_batch
          when 'lead' then insert_lead
          when 'template' then insert_template
          else insert_data

        fn doc, (err) ->
          console.err if err?
          _pgdb.query q.insert_oldrevs, ['data', doc._id, doc._rev], (err) ->
            console.log 'could not save revision', err if err?

            # TODO handle errors? put in an array, try again?
            console.err if err?
            cb()

  finalize = ->
    _pgdb.query """
      SET LOCAL maintenance_work_mem = '512MB';

      CREATE UNIQUE INDEX data_types_ix ON data USING btree (type, id, valid DESC NULLS LAST);
      CREATE INDEX data_scd_ix ON data USING gist (cast(id as "text"), valid);
      CREATE INDEX data_scdcurrent_ix ON data USING btree (id) WHERE upper(valid) = 'infinity';
      CREATE INDEX data_senderaliases_ix ON data USING btree ((doc->>'aliases')) WHERE type = 'sender';
      CREATE INDEX leads_segment_ix ON leads
        USING btree ((doc->>'$status'), (doc->>'segment'), COALESCE((doc->>'segmented'), (doc->>'created')))
        WHERE upper(valid) = 'infinity' AND (doc ? '$status' AND doc ? 'segment');
    """, (err) ->
      console.error err if err?
      async.each ['data', 'batches', 'leads'], (tbl, cb) ->
        _pgdb.query "VACUUM ANALYZE #{tbl};", cb
      , (err) ->
        console.error err if err?
        process.exit()

  process.on 'message', (msg) ->
    switch msg.type
      when 'finalize'
        finalize()
      when 'item'
        process_rows msg
      when 'info'
        _info = msg
        clientdb = server.use _info.db_name

        factory.client = _info.id
        _pgdb = factory.single()

        # only log slow queries over 5 seconds
        _pgdb.slow = 5000
        _pgdb.ignore.push '42P07'

        start()

  # ignore CTRL+C in workers
  process.on 'SIGINT', ->

  process.on 'uncaughtException', (ex) ->
    console.error 'worker exception %s\n%s', ex.message, ex.stack
