#!/usr/bin/env coffee

if ~process.cwd().indexOf 'pg-migration'
  process.env.NODE_CONFIG_DIR ?= '../../config'

config = require 'config'
{COUCH_URL, EXCLUDE, EPOCH} = require './config'

_ = require 'lodash'
async = require 'async'
cluster = require 'cluster'
moment = require 'moment'
nano = require 'nano'
readline = require 'readline'
stringify = require 'fast-stable-stringify'
uuid = require 'uuid'

pg = require 'pg'
squel = require('squel').useFlavour 'postgres'
squel.cls.DefaultQueryBuilderOptions.tableAliasQuoteCharacter = '"'

{Database, RepositoryFactory} = require 'shared/database'


_throttle = 500

console.time 'import messages'
server = nano
  url: COUCH_URL
  requestDefaults:
    agent: new require('agentkeepalive')
      maxSockets: 20
      maxKeepAliveRequests: 0
      maxKeepAliveTime: 30000
couchdb = server.use 'softvu'

factory = new RepositoryFactory config.db.url

queries =
  select_oldrevs: """
SELECT 1 FROM oldrevs WHERE db = $1 AND id = $2 AND rev = $3 LIMIT 1
"""
  insert_oldrevs: """
INSERT INTO oldrevs (db, id, rev) VALUES ($1, $2, $3)
"""
  select_legacyids: """
SELECT newid FROM emails_legacyids WHERE oldid = $1 LIMIT 1
"""
  insert_legacyids: """
INSERT INTO emails_legacyids (oldid, newid) VALUES ($1, $2)
"""
  select_emails: """
SELECT 1 FROM {tbl} WHERE id = $1 LIMIT 1
"""
  update_emails: """
UPDATE {tbl} SET html = $2, text = $3, doc = $4 WHERE id = $1 RETURNING id
"""
  insert_emails: """
INSERT INTO {tbl} (id, type, html, text, doc) VALUES ($1, $2, $3, $4, $5::jsonb) RETURNING id
"""

if cluster.isMaster
  rl = readline.createInterface
    input: process.stdin
    output: process.stdout
  rl.setPrompt 'paused [resume, skip, quit] > '

  create_worker = ->
    worker = cluster.fork()
    worker.send = do (oldsend = worker.send.bind(worker)) ->
      (type, msg, cb) ->
        oldsend _.assign(type: type, msg or {}), null, (err) ->
          console.error 'worker message send error', err, err.stack if err?
          cb?()

    worker

  couchdb.view 'objects', 'clients', startkey: ['all'], endkey: ['all', {}], include_docs: yes, (err, body) ->
    # loop through all clients in series
    async.eachSeries body.rows, (client, nextclient) ->
      _worker = null
      _feed = null
      _pgdb = null

      _curr_seq = 0
      _migration_id = -1

      _state = 'running'
      _workerstart = undefined

      _stats = sent: 0, acked: 0

      rl.on 'SIGINT', on_sigint = ->
        console.log 'pausing... togo: %d', _stats.sent - _stats.acked
        _state = 'pausing'
        _feed.pause()

        if _stats.sent is _stats.acked
          _state = 'paused'
          rl.prompt()

      rl.on 'line', on_line = (line) ->
        return unless _state is 'paused'

        switch line.trim()
          when 'resume'
            _state = 'running'
            _feed.resume()
          when 'skip'
            console.log 'skipping to next client'
            _state = 'skipping'
            _feed.stop() if _state is 'running'
            nextclient() if _stats.sent is _stats.acked
          when 'quit'
            console.log 'quitting...'
            _state = 'quitting'
            _feed.stop() if _state is 'running'
            nextclient 'shutdown' if _stats.sent is _stats.acked
          else
            console.log 'bad command %s', line.trim()
            rl.prompt()

      process.on 'uncaughtException', on_uncaught = (ex) ->
        console.error 'master exception %s\n%s', ex.message, ex.stack
        _state = 'quitting'
        _feed.stop() if _state is 'running'
        nextclient 'shutdown' if _stats.sent is _stats.acked

      nextclient = do (next = nextclient) -> (args...) ->
        cont = ->
          rl.removeListener 'SIGINT', on_sigint
          rl.removeListener 'line', on_line
          process.removeListener 'uncaughtException', on_uncaught
          return next args... unless _pgdb?

          console.log 'update migrations id: %s seq: %s', _migration_id, _curr_seq
          _pgdb.query 'UPDATE migrations SET "end" = NOW(), end_seq = $2 WHERE id = $1', [_migration_id, _curr_seq], (err, results) ->
            return next err if err?
            next args...

        if _worker?
          _worker.send 'finalize'
          _worker.on 'exit', cont
        else
          cont()

      if client.id in EXCLUDE
        console.log 'skipping client', client.id, client.doc.name
        return nextclient()

      start_migration = ->
        _worker = create_worker()
        clientdb = server.use "client-#{client.id}-messages"
        server.db.get "client-#{client.id}-messages", (err, info) ->
          info.id = client.id

          console.log 'starting migration (%s - %s) id: %d seq: %d', client.id, client.doc.name, _migration_id, _curr_seq
          _feed = clientdb.follow since: _curr_seq, include_docs: false

          _feed.filter = (doc, req) ->
            return /^[A-Za-z0-9]{32}$/.test doc._id

          _feed.on 'change', (change) ->
            _curr_seq = change.seq
            _worker.send 'item', change

            if ++_stats.sent - _stats.acked > _throttle * 2
              _state = 'throttled'
              _feed.pause()

          _feed.on 'catchup', (seq) ->
            _state = 'stopping'
            _feed.stop()
            nextclient() unless _stats.sent

          ['retry', 'timeout', 'error'].forEach (evt) ->
            _feed.on evt, -> console.log evt, arguments...

          _worker.on 'message', (msg) ->
            switch msg.type
              when 'ready'
                _workerstart = process.hrtime()
                _feed.follow()
              when 'resume'
                _feed.resume()
              when 'ack'
                _stats.acked++

                # TODO move this to master..?
                if _state isnt 'pausing' and _stats.acked % _throttle is 0
                  end = process.hrtime _workerstart
                  totalsec = end[0] + end[1] / 1e9

                  ravg = (_stats.acked / totalsec).toFixed(4)
                  left = info.update_seq - msg.seq # approx.. msg.seq may not be the max sequence
                  console.log 'processed %d, %d/s avg. (%d remaining, est. %d min. remaining)', _stats.acked, ravg, left, ((left / ravg) / 60).toFixed(2)

                if _state is 'pausing' and _stats.sent is _stats.acked
                  _state = 'paused'
                  rl.prompt()
                if _state is 'throttled' and _stats.sent - _stats.acked <= _throttle
                  _state = 'running'
                  _feed.resume()
                else if _state in ['stopping', 'quitting'] and _stats.sent is _stats.acked
                  end = process.hrtime _workerstart
                  totalsec = end[0] + end[1] / 1e9
                  console.log 'client processed, total: %d', _stats.sent
                  console.log 'processed avg. %d per second', _stats.sent / totalsec

                  nextclient if _state is 'quitting' then 'shutdown' else null
                  _state = 'stopped'

          _worker.send 'info', info

      factory.client = client.id
      _pgdb = factory.single()

      _pgdb.query """
        CREATE TABLE IF NOT EXISTS migrations (
          id serial,
          "table" text NOT NULL,
          start timestamptz DEFAULT NOW(),
          "end" timestamptz,
          end_seq int
        );
      """, (err, result) ->
        return nextclient err if err?

        _pgdb.query 'SELECT end_seq FROM migrations WHERE "table" = $1 AND end_seq IS NOT NULL ORDER BY id DESC LIMIT 1', ['emails'], (err, results) ->
          return nextclient err if err?

          _curr_seq = if results.rows.length
            results.rows[0].end_seq || 0
          else
            0

          _pgdb.query "INSERT INTO migrations (\"table\") VALUES ('emails') RETURNING id", (err, results) ->
            return nextclient err if err?

            _migration_id = results.rows[0].id
            start_migration()
    , (err) ->
      pg.end()

      console.timeEnd 'import messages'
      process.exit()
else
  if process.platform is 'win32'
    rl = require('readline').createInterface
      input: process.stdin,
      output: process.stdout

    rl.on 'SIGINT', ->
      # process.emit 'SIGINT'

  process.send = do (oldsend = process.send.bind(process)) ->
    (type, msg, cb) ->
      oldsend _.assign(type: type, msg or {}), null, (err) ->
        console.error 'worker message send error', err, err.stack if err?
        cb?()

  _pgdb = undefined
  clientdb = undefined

  # set pg client to root to the new client schema
  _info = undefined
  _processed = 0
  _exists = {}

  wocouch = (doc) -> _.omit doc, '_id', '_rev', '_attachments'

  start = ->
    _pgdb.query """
      DO $$BEGIN
        CREATE TABLE IF NOT EXISTS bademails (
          id uuid NOT NULL,
          type text NOT NULL,
          doc jsonb,
          html text,
          text text
        );

        CREATE TABLE IF NOT EXISTS emails_legacyids (
          oldid uuid,
          newid bigint,
          UNIQUE (oldid, newid),
          PRIMARY KEY (oldid, newid)
        );

        CREATE TABLE IF NOT EXISTS oldrevs (
          db text NOT NULL,
          id text NOT NULL,
          rev text NOT NULL
        );

        IF (SELECT to_regclass('emails_legacyids_ix') IS NULL) THEN
          CREATE INDEX emails_legacyids_ix ON emails_legacyids (oldid);
        END IF;

        IF (SELECT to_regclass('oldrevs_ix') IS NULL) THEN
          CREATE INDEX oldrevs_ix ON oldrevs (db, id, rev);
        END IF;
      END$$;
    """, (err, result) ->
      process.send 'ready'

  process_rows = (row) ->
    cb = -> process.send 'ack', seq: row.seq

    # console.log row
    _pgdb.query queries.select_oldrevs, ['messages', row.id, row.changes[0].rev], (err, result) ->
      # we've already processed this message, just ignore it
      return cb null if result.rows.length

      clientdb.get row.id, attachments: yes, (err, doc) ->
        console.error err if err?
        return cb null unless doc?

        [html, text] = for x in ['html', 'text']
          data = doc._attachments?[x]?.data or null
          unless data? then null else new Buffer(data, 'base64').toString()

        # console.log 'html: %d text: %d', html?.length, text?.length
        _pgdb.query queries.select_legacyids, [row.id], (err, result) ->
          unless doc.queued? or doc.sent? or doc.errored? or doc.canceled?
            # there is a tiny handful of these emails that are just, bunk
            # some of theses are cancelations due to optouts etc
            return unless result.rows.length
              console.log 'found bad email, could not generate id', doc._id, doc.type
              query = squel.insert()
                .into 'bademails'
                .set 'id', doc._id
                .set 'type', doc.type
                .set 'html', "$html$#{html or 'null'}$html$", dontQuote: yes
                .set 'text', "$text$#{text or 'null'}$text$", dontQuote: yes
                .set 'doc', "$json$#{stringify wocouch doc}$json$", dontQuote: yes
                .returning 'id'
              _pgdb.query query.toString(), (err, results) ->
                console.error 'insert bademails error', err if err?
                cb null
            else
              console.log 'found bad email, could not generate id', doc._id, doc.type
              query = squel.update()
                .into 'bademails'
                .set 'type', doc.type
                .set 'html', "$html$#{html or 'null'}$html$", dontQuote: yes
                .set 'text', "$text$#{text or 'null'}$text$", dontQuote: yes
                .set 'doc', "$json$#{stringify wocouch doc}$json$", dontQuote: yes
                .where 'id', doc._id
                .returning 'id'
              _pgdb.query query.toString(), (err, results) ->
                console.error 'update bademails error', err if err?
                cb null

          dt_for_id = moment.utc doc.queued or doc.sent or doc.errored?.at or doc.canceled
          id = dt_for_id.valueOf() - EPOCH

          bom = dt_for_id.startOf 'month'
          tbl = bom.format '[emails_y]YYYY[m]MM'

          if result.rows.length
            id = result.rows[0].newid
            query = queries.update_emails.replace '{tbl}', tbl
            return _pgdb.query query, [id, html, text, stringify wocouch doc], (err, results) ->
              if err?
                console.error 'email update error', err
                return cb null

              _pgdb.query queries.insert_oldrevs, ['messages', doc._id, doc._rev], (err) ->
                console.log 'could not save revision', err if err?
                cb null

          do insert = ->
            unless _exists[tbl]
              checkstart = bom.valueOf() - EPOCH
              checkend = bom.add(1, 'month').valueOf() - EPOCH

              return _pgdb.query """
                BEGIN;
                LOCK TABLE _emails IN ACCESS EXCLUSIVE MODE;

                DO $$BEGIN
                  IF (SELECT to_regclass('#{tbl}') IS NULL) THEN
                    CREATE TABLE IF NOT EXISTS #{tbl} (
                      CHECK (id >= #{checkstart} AND id < #{checkend}),
                      PRIMARY KEY (id)
                    ) INHERITS (_emails);

                    CREATE TABLE IF NOT EXISTS #{tbl}_stats (
                      CHECK (id >= #{checkstart} AND id < #{checkend}),
                      PRIMARY KEY (id)
                    ) INHERITS (_emails_stats);

                    CREATE TABLE IF NOT EXISTS #{tbl}_stats_batches (
                      CHECK (id >= #{checkstart} AND id < #{checkend}),
                      PRIMARY KEY (id)
                    ) INHERITS (_emails_stats_batches);

                    CREATE TABLE IF NOT EXISTS #{tbl}_stats_drips (
                      CHECK (id >= #{checkstart} AND id < #{checkend}),
                      PRIMARY KEY (id)
                    ) INHERITS (_emails_stats_drips);

                    DROP INDEX IF EXISTS #{tbl}_type;
                    DROP INDEX IF EXISTS #{tbl}_gin_doc;

                    DROP TRIGGER IF EXISTS #{tbl}_stats ON #{tbl};
                    CREATE TRIGGER #{tbl}_stats
                      AFTER INSERT OR UPDATE ON #{tbl}
                      FOR EACH ROW EXECUTE PROCEDURE process_email();
                  END IF;
                END$$;
                COMMIT;
              """, (err, results) ->
                _exists[tbl] = yes
                insert()

            query = queries.insert_emails.replace '{tbl}', tbl
            _pgdb.query query, [id, doc.type, html, text, stringify wocouch doc], (err, results) ->
              # 23505 is a unique constraint violation... this means we have two emails with the exact same date
              # just increment one of them
              if err?.code is '23505'
                id += 1
                return insert()
              else if err?
                console.error 'insert email error', err
                return cb null

              _pgdb.query queries.insert_oldrevs, ['messages', doc._id, doc._rev], (err) ->
                console.log 'could not save revision', err if err?

                _pgdb.query queries.insert_legacyids, [doc._id, id], (err) ->
                  console.error 'insert legacyids error', err if err?
                  cb null

  finalize = ->
    async.eachSeries Object.keys(_exists), (tbl, cb) ->
      console.log 're-adding indexes on %s...', tbl
      _pgdb.query """
        SET LOCAL maintenance_work_mem = '512MB';
        CREATE INDEX #{tbl}_type ON #{tbl} (type);
        CREATE INDEX #{tbl}_gin_doc ON #{tbl} USING gin (doc);
      """, ->
        _pgdb.query "VACUUM ANALYZE #{tbl};", cb
    , (err) ->
      process.exit()

  process.on 'message', (msg) ->
    switch msg.type
      when 'finalize'
        finalize()
      when 'item'
        process_rows msg
      when 'info'
        _info = msg
        clientdb = server.use _info.db_name

        factory.client = _info.id
        _pgdb = factory.single()

        # only log slow queries over 5 seconds
        _pgdb.slow = 5000
        _pgdb.ignore.push '42P07'

        start()

  # ignore CTRL+C in workers
  process.on 'SIGINT', ->

  process.on 'uncaughtException', (ex) ->
    console.error 'worker exception %s\n%s', ex.message, ex.stack
