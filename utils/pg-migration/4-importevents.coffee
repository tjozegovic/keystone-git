#!/usr/bin/env coffee

if ~process.cwd().indexOf 'pg-migration'
  process.env.NODE_CONFIG_DIR ?= '../../config'

config = require 'config'
{COUCH_URL, EXCLUDE, EPOCH} = require './config'

_ = require 'lodash'
async = require 'async'
cluster = require 'cluster'
moment = require 'moment'
nano = require 'nano'
readline = require 'readline'
stringify = require 'fast-stable-stringify'
uuid = require 'uuid'

pg = require 'pg'
squel = require('squel').useFlavour 'postgres'
squel.cls.DefaultQueryBuilderOptions.tableAliasQuoteCharacter = '"'

{Database, RepositoryFactory} = require 'shared/database'


_throttle = 500 # on windows... this cannot be bigger. tried at 2k, the channel between the master and worker hits a hard limit

console.time 'import events'
server = nano COUCH_URL
couchdb = server.use 'softvu'

factory = new RepositoryFactory config.db.url

queries =
  select_oldrevs: """
SELECT 1 FROM oldrevs WHERE db = $1 AND id = $2 AND rev = $3 LIMIT 1
"""
  insert_oldrevs: """
INSERT INTO oldrevs (db, id, rev) VALUES ($1, $2, $3)
"""
  select_legacyids: """
SELECT newid FROM emails_legacyids WHERE oldid = $1 LIMIT 1
"""
  insert_events: """
INSERT INTO {tbl} (id, type, at, doc) VALUES ($1, $2, $3, $4::jsonb) RETURNING id
"""

if cluster.isMaster
  rl = readline.createInterface
    input: process.stdin
    output: process.stdout
  rl.setPrompt 'paused [resume, skip, quit] > '

  create_worker = ->
    worker = cluster.fork()
    worker.send = do (oldsend = worker.send.bind(worker)) ->
      (type, msg, cb) ->
        oldsend _.assign(type: type, msg or {}), null, (err) ->
          console.error 'worker message send error', err, err.stack if err?
          cb?()

    worker

  couchdb.view 'objects', 'clients', startkey: ['all'], endkey: ['all', {}], include_docs: yes, (err, body) ->
    # loop through all clients in series
    async.eachSeries body.rows, (client, nextclient) ->
      _worker = null
      _feed = null
      _pgdb = null

      _curr_seq = 0
      _migration_id = -1

      _state = 'running'

      _stats = sent: 0, acked: 0
      _workerstart = process.hrtime()

      rl.on 'SIGINT', on_sigint = ->
        console.log 'pausing...', _stats
        _state = 'pausing'
        _feed.pause()

        if _stats.sent is _stats.acked
          _state = 'paused'
          rl.prompt()

      rl.on 'line', on_line = (line) ->
        return unless _state is 'paused'

        switch line.trim()
          when 'resume'
            _state = 'running'
            _feed.resume()
          when 'skip'
            console.log 'skipping to next client'
            _state = 'skipping'
            _feed.stop() if _state is 'running'
            nextclient() if _stats.sent is _stats.acked
          when 'quit'
            console.log 'quitting...'
            _state = 'quitting'
            _feed.stop() if _state is 'running'
            nextclient 'shutdown' if _stats.sent is _stats.acked
          else
            console.log 'bad command %s', line.trim()
            rl.prompt()

      process.on 'uncaughtException', on_uncaught = (ex) ->
        console.error 'master exception %s\n%s', ex.message, ex.stack
        _state = 'quitting'
        _feed.stop() if _state is 'running'
        nextclient 'shutdown' if _stats.sent is _stats.acked

      nextclient = do (next = nextclient) -> (args...) ->
        cont = ->
          rl.removeListener 'SIGINT', on_sigint
          rl.removeListener 'line', on_line
          process.removeListener 'uncaughtException', on_uncaught
          return next args... unless _pgdb?

          console.log 'update migrations id: %s seq: %s', _migration_id, _curr_seq
          _pgdb.query 'UPDATE migrations SET "end" = NOW(), end_seq = $2 WHERE id = $1', [_migration_id, _curr_seq], (err, results) ->
            return next err if err?
            next args...

        if _worker?
          _worker.send 'finalize'
          _worker.on 'exit', cont
        else
          cont()

      if client.id in EXCLUDE
        console.log 'skipping client', client.id, client.doc.name
        return nextclient()

      start_migration = ->
        _worker = create_worker()
        clientdb = server.use "client-#{client.id}-events"
        server.db.get "client-#{client.id}-events", (err, info) ->
          info.id = client.id

          console.log 'starting migration (%s - %s) id: %d seq: %d', client.id, client.doc.name, _migration_id, _curr_seq
          _feed = clientdb.follow since: _curr_seq, include_docs: yes

          _feed.filter = (doc, req) ->
            return /^[A-Za-z0-9]{32}$/.test doc._id

          _feed.on 'change', (change) ->
            _curr_seq = change.seq
            _worker.send 'item', change

            if ++_stats.sent - _stats.acked > _throttle * 2
              _state = 'throttled'
              _feed.pause()

          _feed.on 'catchup', (seq) ->
            _state = 'stopping'
            _feed.stop()
            nextclient() unless _stats.sent

          ['retry', 'timeout', 'stop', 'error'].forEach (evt) ->
            _feed.on evt, -> console.log evt, arguments...

          _worker.on 'message', (msg) ->
            switch msg.type
              when 'ready'
                _feed.follow()
              when 'resume'
                _feed.resume()
              when 'ack'
                _stats.acked++

                # TODO move this to master..?
                if _state isnt 'pausing' and _stats.acked % _throttle is 0
                  end = process.hrtime _workerstart
                  totalsec = end[0] + end[1] / 1e9

                  ravg = (_stats.acked / totalsec).toFixed(4)
                  left = info.update_seq - msg.seq # approx.. msg.seq may not be the max sequence
                  console.log 'processed %d, %d/s avg. (%d remaining, est. %d min. remaining)', _stats.acked, ravg, left, ((left / ravg) / 60).toFixed(2)

                if _state is 'pausing' and _stats.sent is _stats.acked
                  _state = 'paused'
                  rl.prompt()
                if _state is 'throttled' and _stats.sent - _stats.acked <= _throttle
                  _state = 'running'
                  _feed.resume()
                else if _state in ['stopping', 'quitting'] and _stats.sent is _stats.acked
                  end = process.hrtime _workerstart
                  totalsec = end[0] + end[1] / 1e9
                  console.log 'client processed, total: %d', _stats.sent
                  console.log 'processed avg. %d per second', _stats.sent / totalsec

                  nextclient if _state is 'quitting' then 'shutdown' else null
                  _state = 'stopped'

          _worker.send 'info', info

      factory.client = client.id
      _pgdb = factory.single()

      _pgdb.query """
        CREATE TABLE IF NOT EXISTS migrations (
          id serial,
          "table" text NOT NULL,
          start timestamptz DEFAULT NOW(),
          "end" timestamptz,
          end_seq int
        );
      """, (err, result) ->
        return nextclient err if err?

        _pgdb.query 'SELECT end_seq FROM migrations WHERE "table" = $1 AND end_seq IS NOT NULL ORDER BY id DESC LIMIT 1', ['events'], (err, results) ->
          return nextclient err if err?

          _curr_seq = if results.rows.length
            results.rows[0].end_seq || 0
          else
            0

          _pgdb.query "INSERT INTO migrations (\"table\") VALUES ('events') RETURNING id", (err, results) ->
            return nextclient err if err?

            _migration_id = results.rows[0].id
            start_migration()
    , (err) ->
      pg.end()

      console.timeEnd 'import events'
      process.exit()
else
  if process.platform is 'win32'
    rl = require('readline').createInterface
      input: process.stdin
      output: process.stdout

    rl.on 'SIGINT', ->
      # process.emit 'SIGINT'

  process.send = do (oldsend = process.send.bind(process)) ->
    (type, msg, cb) ->
      oldsend _.assign(type: type, msg or {}), null, (err) ->
        console.error 'worker message send error', err, err.stack if err?
        cb?()

  _pgdb = undefined

  # set pg client to root to the new client schema
  _workerstart = process.hrtime()
  _info = undefined
  _processed = 0
  _exists = {}

  wocouch = (doc) -> _.omit doc, '_id', '_rev', '_attachments'

  process_rows = (row) ->
    cb = -> process.send 'ack', seq: row.seq

    {doc} = row
    _pgdb.query queries.select_oldrevs, ['events', row.id, doc._rev], (err, result) ->
      # we've already processed this message, just ignore it
      return cb null if result.rows.length

      bom = moment.utc(doc.at).startOf('month')
      tbl = bom.format '[events_y]YYYY[m]MM'

      do insert = ->
        unless _exists[tbl]
          checkstart = bom.format()
          checkend = bom.add(1, 'month').format()

          _pgdb.query """
            CREATE TABLE IF NOT EXISTS #{tbl} (
              CHECK (at >= TIMESTAMPTZ '#{checkstart}' AND at < TIMESTAMPTZ '#{checkend}'),
              PRIMARY KEY (id)
            ) INHERITS (_events);

            DROP INDEX IF EXISTS #{tbl}_at_ix;
            DROP INDEX IF EXISTS #{tbl}_typeat_ix;

            DROP INDEX IF EXISTS #{tbl}_emails_ix;
            DROP INDEX IF EXISTS #{tbl}_leads_ix;
          """, (err, results) ->
            _exists[tbl] = yes
            return insert()
        else
          done = ->
            query = queries.insert_events.replace '{tbl}', tbl
            _pgdb.query query, [doc._id, doc.type, doc.at, stringify wocouch doc], (err) ->
              console.error 'could not save event', err if err?

              _pgdb.query queries.insert_oldrevs, ['events', doc._id, doc._rev], (err) ->
                console.error 'could not save revision', err if err?
                cb null

          return done() unless /^[A-Za-z0-9]{32}$/.test doc.email

          oldid = doc.email
          _pgdb.query queries.select_legacyids, [oldid], (err, results) ->
            if results.rows[0]?
              doc.emailoldid = oldid
              doc.email = results.rows[0].newid

            done()

  finalize = ->
    async.each Object.keys(_exists), (tbl, cb) ->
      _pgdb.query """
        CREATE INDEX #{tbl}_at_ix ON #{tbl} (at);
        CREATE INDEX #{tbl}_typeat_ix ON #{tbl} (type, at);

        CREATE INDEX #{tbl}_emails_ix ON #{tbl} ((doc->>'email')) WHERE doc ? 'email';
        CREATE INDEX #{tbl}_leads_ix ON #{tbl} ((doc->>'lead')) WHERE doc ? 'lead';
      """, cb
    , (err) ->
      process.exit()

  process.on 'message', (msg) ->
    switch msg.type
      when 'finalize'
        finalize()
      when 'item'
        process_rows msg
      when 'info'
        _info = msg

        factory.client = _info.id
        _pgdb = factory.single()

        # only log slow queries over 5 seconds
        _pgdb.slow = 5000
        _pgdb.ignore.push '42P07'

        process.send 'ready'

  # ignore CTRL+C in workers
  process.on 'SIGINT', ->

  process.on 'uncaughtException', (ex) ->
    console.error 'worker exception %s\n%s', ex.message, ex.stack
