#!/usr/bin/env coffee

if ~process.cwd().indexOf 'pg-migration'
  process.env.NODE_CONFIG_DIR ?= '../../config'

config = require 'config'
{COUCH_URL} = require './config'

_ = require 'lodash'
async = require 'async'
nano = require 'nano'
uuid = require 'uuid'

pg = require 'pg'
squel = require('squel').useFlavour 'postgres'
squel.cls.DefaultQueryBuilderOptions.tableAliasQuoteCharacter = '"'

{RepositoryFactory} = require 'shared/database'

_processed = 0
_start = process.hrtime()
_limit = 1601
startkey = undefined

console.time 'import optouts'
server = nano COUCH_URL
optouts = server.use 'optouts'

factory = new RepositoryFactory config.db.url
pgdb = factory.single()

class JsonString
  constructor: (@obj) ->

squel.registerValueHandler JsonString, (obj, options) ->
  options.dontQuote = yes
  "$jsonb$#{JSON.stringify obj.obj}$jsonb$"

print_progress = ->
  end = process.hrtime _start
  totalsec = end[0] + end[1] / 1e9
  console.log 'processed %d, %d/s avg.', _processed, (_processed / totalsec).toFixed(4)

async.forever (forever) ->
  optouts.list limit: _limit, startkey_docid: startkey, include_docs: yes, (err, body) ->
    return forever err if err

    # store last startkey for next query
    startkey = _.last(body.rows).id

    # breakout all documents that are leads, batches, or data
    list = _(body.rows)
      .take _limit - 1
      .pluck 'doc'
      .filter (doc) -> ~doc._id.indexOf '@'
      .value()

    rows = _.map list, (optout) ->
      id: optout._id
      created: optout.created ? null
      updated: optout.updated ? optout.created ? null
      doc: new JsonString _.omit optout, '_id', '_rev', 'created', 'updated', 'type'

    query = squel.insert()
      .into 'optouts'
      .setFieldsRows rows
    pgdb.query query, (err) ->
      console.error err if err?

      _processed += rows.length
      print_progress()

      forever if body.rows.length < _limit then 'done' else null
, ->
  # close all connection pools
  pg.end()
  console.timeEnd 'import optouts'
