#!/usr/bin/env coffee

if ~process.cwd().indexOf 'pg-migration'
  process.env.NODE_CONFIG_DIR ?= '../../config'

config = require 'config'
{COUCH_URL} = require './config'

_ = require 'lodash'
async = require 'async'
nano = require 'nano'
uuid = require 'uuid'

pg = require 'pg'
squel = require('squel').useFlavour 'postgres'
squel.cls.DefaultQueryBuilderOptions.tableAliasQuoteCharacter = '"'

{RepositoryFactory} = require 'shared/database'

_processed = 0
_start = process.hrtime()
_limit = 1601
startkey = undefined
startkey_docid = undefined

console.time 'import jobs'
server = nano COUCH_URL
jobs = server.use 'jobs'

factory = new RepositoryFactory config.db.url
pgdb = factory.single()

class JsonString
  constructor: (@obj) ->

squel.registerValueHandler JsonString, (obj, options) ->
  options.dontQuote = yes
  "$jsonb$#{JSON.stringify obj.obj}$jsonb$"

print_progress = ->
  end = process.hrtime _start
  totalsec = end[0] + end[1] / 1e9
  console.log 'processed %d, %d/s avg.', _processed, (_processed / totalsec).toFixed(4)

pgdb.query 'DROP INDEX IF EXISTS jobs_pending_ix;', ->
  async.forever (forever) ->
    jobs.view 'jobs', 'pending_by_time', limit: _limit, startkey: startkey, startkey_docid: startkey_docid, include_docs: yes, (err, body) ->
      return forever err if err

      # store last startkey for next query
      startkey = _.last(body.rows).key
      startkey_docid = _.last(body.rows).id

      # breakout all documents that are leads, batches, or data
      list = _(body.rows)
        .take _limit - 1
        .pluck 'doc'
        .reject (doc) -> ~doc._id.indexOf '_design'
        .value()

      rows = _.map list, (job) ->
        id: job._id.replace /^job:/, ''
        at: job.at
        action: job.action
        status: job.status
        created: job.created ? null
        updated: job.updated ? job.created ? null
        repeat: job.repeat ? null
        data: new JsonString job.data or {}

      query = squel.insert()
        .into 'jobs'
        .setFieldsRows rows
      pgdb.query query, (err) ->
        console.error err if err?

        _processed += rows.length
        print_progress()

        forever if body.rows.length < _limit then 'done' else null
  , ->
    pgdb.query 'CREATE INDEX jobs_pending_ix ON jobs USING btree (status, at);', ->
      # close all connection pools
      pg.end()
      console.timeEnd 'import jobs'
