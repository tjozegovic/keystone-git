#!/usr/bin/env coffee

if ~process.cwd().indexOf 'pg-migration'
  process.env.NODE_CONFIG_DIR ?= '../../config'

config = require 'config'
{COUCH_URL} = require './config'

_ = require 'lodash'
async = require 'async'
nano = require 'nano'
uuid = require 'uuid'

{RepositoryFactory} = require 'shared/database'

request = require 'request'

_processed = 0
_start = process.hrtime()
_limit = 1601
startkey = undefined

console.time 'process offline_leads'
server = nano COUCH_URL
offline_leads = server.use 'offline_leads'

factory = new RepositoryFactory config.db.url

class JsonString
  constructor: (@obj) ->

print_progress = ->
  end = process.hrtime _start
  totalsec = end[0] + end[1] / 1e9
  console.log 'processed %d, %d/s avg.', _processed, (_processed / totalsec).toFixed(4)

async.forever (forever) ->
  offline_leads.list limit: _limit, startkey_docid: startkey, include_docs: yes, (err, body) ->
    return forever err if err

    # store last startkey for next query
    startkey = _.last(body.rows).id

    # breakout all documents that are leads, batches, or data
    rows = _(body.rows)
      .take _limit - 1
      .pluck 'doc'
      .value()

    async.each rows, (row, cb) ->
      request.post 'https://api.softvu.com/v1/leads',
        json: row.body
        headers:
          'Authorization': 'Key ' + row.api_key
      , (err, response) ->
        console.log err if err?
        cb()
    , (err) ->
      _processed += rows.length
      print_progress()

      forever if body.rows.length < _limit then 'done' else null
, ->
  # close all connection pools
  console.timeEnd 'process offline_leads'
