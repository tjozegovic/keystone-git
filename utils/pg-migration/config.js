var moment = require('moment')

module.exports.COUCH_URL = 'http://admin:admin@localhost:5984'
module.exports.EXCLUDE = ['bdede430b79211e48c85034b0c695ed1']
module.exports.EPOCH = moment.utc('2010-01-01').valueOf()
