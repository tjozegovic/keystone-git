set search_path = 'keystone';

DO $$
DECLARE
  schema_record pg_namespace%rowtype;
  table_record pg_tables%rowtype;
  matches text[];
  startts timestamp;
BEGIN
  FOR schema_record IN
    SELECT * FROM pg_namespace WHERE nspname ~ '^client_.*'
  LOOP
    RAISE NOTICE '%', schema_record.nspname;
    EXECUTE 'SET LOCAL search_path = ' || schema_record.nspname || ', ''keystone''';

    FOR table_record IN
      EXECUTE format('
        SELECT *
        FROM pg_tables
        WHERE tablename ~ ''emails_.*\d$''
          AND schemaname = %L'
        , schema_record.nspname)
    LOOP
      RAISE NOTICE 'table: %', table_record.tablename;

      -- EXECUTE format('DROP TABLE IF EXISTS %I_stats_batches', table_record.tablename);

      matches := regexp_matches(table_record.tablename, 'emails_y(\d+)m(\d+)');
      startts := (matches[1] || '-' || matches[2] || '-01')::timestamp;
      -- RAISE NOTICE '%, %, %', matches, get_epoch_from_ts(startts), get_epoch_from_ts(startts + '1 month');

      EXECUTE format('
      CREATE TABLE IF NOT EXISTS %I_stats_batches (
        CHECK (id >= %s AND id < %s),
        PRIMARY KEY (id)
      ) INHERITS (_emails_stats_batches);

      INSERT INTO %1$I_stats_batches (id, batch, queued, delivered, bounced, optout, opened, clicked)
      SELECT
          id
        , (doc->''batch''->>''_id'')::uuid batch
        , COALESCE((doc->''queued''->>''at'')::timestamp, (doc->>''queued'')::timestamp) queued
        , COALESCE(
            COALESCE((doc->''sent''->>''at'')::timestamp, (doc->>''sent'')::timestamp)
            , COALESCE((doc->''delivered''->>''at'')::timestamp, (doc->>''delivered'')::timestamp)) delivered
        , COALESCE((doc->''bounced''->>''at'')::timestamp, (doc->>''bounced'')::timestamp) bounced
        , pluck_at(jsonb_extract_path(doc, ''optout'')) optout
        , pluck_at(jsonb_extract_path(doc, ''opened'')) opened
        , pluck_at(jsonb_extract_path(doc, ''clicked'')) clicked
      FROM %1$I
      WHERE type = ''batch'''
      , table_record.tablename
      , get_epoch_from_ts(startts)
      , get_epoch_from_ts(startts + '1 month'));
    END LOOP;
  END LOOP;
END$$;
