config = require 'config'

require 'newrelic' if config.env is 'production'

_ = require 'lodash'
async = require 'async'
chalk = require 'chalk'
domain = require 'domain'
moment = require 'moment'
pluralize = require 'pluralize'

# middlewares
bodyParser = require 'body-parser'
compression = require 'compression'
cookieParser = require 'cookie-parser'
errorHandler = require 'errorhandler'
favicon = require 'serve-favicon'
methodOverride = require 'method-override'
helmet = require 'helmet'
httpProxy = require 'http-proxy'
# session = require 'express-session'
session = require 'cookie-session'
timeout = require 'connect-timeout'

express = require 'express'
path = require 'path'
app = express()

{RepositoryFactory} = require 'shared/database'
{loadclient} = require './middleware'

# all environments
app.set 'port', process.env.PORT or 3001
app.set 'views', path.join __dirname, 'views'
app.set 'view engine', 'jade'
app.locals.env = config.env
app.locals.moment = require 'moment'

app.set 'db url', config.db.url
app.set 'queue url', config.queues.url
app.set 'api url', config.api.url
app.set 'tmpdir', config.tmpdir

app.set 'optout db', 'optouts'
app.set 'job db', 'jobs'

app.use helmet()

haltTimeouts = (time, middleware) ->
  app.use timeout time
  app.use middleware

  # we need to do two things to make sure that these timeouts are only wrapping these specific middlewares
  # first, clear the timeout timer, so that it doesn't kill the request later if it's taking a long time
  #   api/* requests can tend to do this, timeouts and such will be handled on the server
  # second, continue processing if we didn't come across a timeout. if we did, just drop on the floor
  app.use (req, res, next) ->
    unless req.timedout
      req.clearTimeout()
      next()

haltTimeouts '30s', bodyParser.urlencoded extended: yes, limit: '50mb'
haltTimeouts '30s', bodyParser.json limit: '50mb'
haltTimeouts '30s', cookieParser()

app.use methodOverride()
app.use favicon __dirname + '/public/images/favicon.png'

# ConnectCouchDB = require('connect-couchdb') session
# app.use session
#   secret: '$oftVu7381'
#   store: new ConnectCouchDB
#     name: 'sessions'
#     host: config.sessions.host
#   resave: no
#   saveUninitialized: yes

app.use session secret: '$oftVu7381'

if config.env is 'production'
  app.use compression()
  app.enable 'trust proxy'
  app.enable 'load analytics'
  app.disable 'x-powered-by'

app.use (req, res, next) ->
  # this is used in conjuction with an IIS rewrite
  # http://stackoverflow.com/questions/15904482/iis-7-5-redwrite-rule-to-handle-request-based-on-user-agent
  # http://serverfault.com/questions/588604/office-for-mac-hyperlinks-that-redirect-to-https-fail-to-load-with-unable-to-op
  USER_AGENTS_REGEX = /[^\w](Word|Excel|PowerPoint|ms-office)([^\w]|\z)/
  EXCLUDE_USER_AGENTS_REGEX = /Microsoft Outlook/

  url = 'https://' + req.hostname + req.originalUrl
  auto_refresh = "<html><head><meta http-equiv='refresh' content='0;url=#{url}'/></head><body></body></html>"

  if USER_AGENTS_REGEX.exec(req.headers['user-agent']) and not EXCLUDE_USER_AGENTS_REGEX.exec(req.headers['user-agent'])
    res.set 'Content-Type', 'text/html'
    return res.send auto_refresh

  next()

app.use express.static path.join __dirname, 'public'
app.use '/bower_components', express.static path.join __dirname, 'bower_components'

socketProxy = httpProxy.createProxyServer()
socketProxy.on 'error', (err, req, res) ->
  console.error 'proxy error', err
  if res.headersSent
    res.writeHead 500, 'content-type': 'application/json'
  # TODO not sure what if anything to send to the client here...
  res.end JSON.stringify error: 'proxy_error', reason: err.message

app.all '/stream*', (req, res) ->
  socketProxy.web req, res, target: 'http://localhost:9999'

if app.get('env') is 'development'
  app.use require('morgan') 'dev'
  app.locals.pretty = yes

do (fs = require 'fs') ->
  file = path.join __dirname, '../.version'
  return unless fs.existsSync file

  version = fs.readFileSync(file).toString()
  app.locals.version =
    long: version
    short: version.split(' ')[0]

app.use (req, res, next) ->
  d = domain.create()
  d.on 'error', next
  d.run next

app.use (req, res, next) ->
  req.locals ?= {}
  req.isasset = _.any ['favicon.ico', 'stylesheets/', 'javascripts/', 'images/'], (path) -> ~req.url.indexOf path
  next()

app.use (req, res, next) ->
  req.db = new RepositoryFactory app.get 'db url'
  next()

app.use (req, res, next) ->
  {query} = req
  query.whitelist = query.whitelist.split ',' if query.whitelist?
  next()

app.use (req, res, next) ->
  # TODO need to put this into configuration
  return next() if req.isasset or req.hostname in ['localhost', 'appstaging.softvu.com', 'app.softvu.com']

  tst = /^(\w+)?\.?(\w+)\.svemails\.com/.exec req.hostname
  if tst?
    # TODO what do we do if we have this url, but the shortname doesn't exist?
    clients = req.db.use 'core/clients'
    clients.by_svemails_prefix tst[2], (err, client) ->
      return next() if err or not client?

      req._iscname = yes
      req.session.client_id = client.id
      next()
  else
    cnames = req.db.use 'core/cnames'
    cnames.get req.hostname, (err, client) ->
      # TODO log error?
      return next() if err

      req._iscname = yes
      req.session.client_id = client
      next()

app.use (req, res, next) ->
  return next() if req.isasset or req.user
  return next() unless token = req.cookies.token

  users = req.db.use 'core/users'
  users.getByToken token, (user) ->
    return next() unless user
    req.session.username = user.username
    next()

# TODO caching?
app.use (req, res, next) ->
  return next() if req.isasset or req.user
  return next() unless username = req.session.username

  users = req.db.use 'core/users'
  users.get username, (err, user) ->
    return next err if err
    return next() unless user?

    req.user = _.omit user, 'password'
    req.session.client_id ?= user.client
    next()

app.use loadclient

app.use (req, res, next) ->
  events = require 'shared/eventlib'

  res.locals.events =
    publish: (type, data, cb) ->
      {client} = req.params
      client ?= req.session.client_id
      events.emit type, client, data, cb
    emit: events.emit.bind events

  next()

app.use (req, res, next) ->
  # load all client information for admin switch
  return next() unless req.user?.isAdmin
  return next() if ~req.url.indexOf('api/') or ~req.url.indexOf('html/')

  clients = req.db.use 'core/clients'
  clients.all (err, body) ->
    return next err if err

    res.locals.clients = _(body).reject('archive').sortBy('name').value()
    next()

app.use (req, res, next) ->
  res.locals.req = req
  res.locals.session = req.session
  next()

app.use '/integration/mortech', require 'shared/integration/mortech/events'

if app.get('env') is 'staging'
  app.post '/bitbucket/update', (req, res) ->
    spawn = require('child_process').spawn
    hg = spawn 'hg', ['pull', '-u', 'https://softvullc:64UL5xGPjRYw9eUp@bitbucket.org/softvullc/keystone']
    hg.stderr.on 'data', (data) -> console.log data.toString()
    hg.stdout.on 'data', (data) -> console.log data.toString()
    hg.on 'close', ->
      # this will cause the apppool to crash (on IIS), forcing it to restart
      process.nextTick -> process.exit -1
    res.send 'OK'

#
# ROUTES
#
app.get /^\/html\/(.+)/, (req, res, next) ->
  page = req.params[0]
  view = if ~(dot = page.indexOf '.') then page[...dot] else page
  res.render view, (err, html) ->
    if html
      res.status(200).send html
    else
      res.status(404).render '404'

# html2canvas uses this proxy url to fetch images w/ js to get around CORS
app.get '/proxy', (req, res) ->
  res.jsonp "/proxy/img?url=#{req.query.url}"
app.get '/proxy/img', (req, res) ->
  request = require 'request'
  request(req.query.url).pipe res

app.use require './routes/emails'
app.use require './routes/pages'
app.use require './routes/users'

# all routes below this point will require authentication
app.all '*', (req, res, next) ->
  return next() if req.isasset or req.user

  if req.xhr or req.method isnt 'GET'
    res.status(401).end 'Unauthorized'
  else
    res.redirect "/login?return=#{req.originalUrl}"

app.post '/setclient', (req, res, next) ->
  return next() unless req.user.isAdmin

  # TODO we should change the users "default" client too
  req.session.client_id = req.body.id
  res.redirect '/'

app.all '/api/*', (req, res, next) ->
  start = process.hrtime()

  fix = (names...) ->
    names.forEach (name) ->
      _orig = res[name]
      res[name] = ->
        unless res.headersSent
          duration = process.hrtime start
          res.setHeader 'X-Response-Time', "#{Math.floor duration[0] * 1000 + duration[1] / 1e6}ms"

        # put the original res.write back, and forward call
        (res[name] = _orig).apply res, arguments

  fix 'write', 'end'
  next()

app.use require './routes/api'
app.use require './routes/accounts'
app.use require './routes/assets'
app.use require './routes/attributes'
app.use require './routes/batches'
app.use require './routes/contacts'
app.use require './routes/leads'
app.use require './routes/messages'
app.use require './routes/optouts'
app.use require './routes/profiles'
app.use require './routes/proofs'
app.use require './routes/statuses'
app.use require './routes/templates'

app.get '/api/system', (req, res, next) ->
  {db} = req
  async.parallel
    groups: (cb) -> db.use('client/groups').all cb
    senders: (cb) -> db.use('client/senders').active cb
    attributes: (cb) -> db.use('client/attributes').active cb
    statuses: (cb) -> db.use('client/statuses').active cb
  , (err, results) ->
    return next err if err?

    # calculate the totals form groups, senders, attributes, and statuses
    sys = _.assign {}
      , _.mapValues(results, _.size)
      , _.pick(req._client, 'debug', 'email_mode', 'dailyvu', 'timezone', 'id', 'name', 'short')
      , env: config.env
      , online: _.every _.values(results), (val) -> _.size(val) > 0

    return res.json sys unless req.user?.isAdmin

    clients = req.db.use 'core/clients'
    clients.all (err, body) ->
      return next err if err?

      sys.client = req._client.id
      sys.clients = _(body)
        .reject 'archive'
        .sortBy 'name'
        .map _.partial _.pick, _, ['id', 'name', 'short']
        .value()
      res.json sys

app.get '/api/reports/dashboard', (req, res, next) ->
  [events, senders] = req.db.use 'client/events', 'client/senders'
  async.parallel
    counts: events.dashboard.bind events
    senders: senders.active.bind senders
  , (err, results) ->
    return next err if err?
    {counts, senders} = results
    counts.users = senders.length
    res.json counts

app.get '/api/reports/sends/:status', (req, res, next) ->
  evts = req.db.use 'client/events'
  evts.sends req.params.status, (err, rows) ->
    return next err if err?
    res.json rows

app.get '/api/trend/:status', (req, res, next) ->
  summaries = req.db.use 'client/summaries'
  opts =
    status: req.params.status
    daily: yes
    from: moment().subtract 30, 'days'
    to: moment().subtract 1, 'day'
  summaries.drips opts, (err, rows) ->
    return next err if err?
    res.json rows

app.get /\/api\/(?:events|reports)\/(.+)/, (req, res) ->
  {stringify} = require 'JSONStream'

  send = (err, rows = []) ->
    if err
      res.status(500).json err: reason: err.message or err
    else if _.isFunction rows.pipe
      rows.pipe(stringify()).pipe(res)
    else if _.isArray(rows) and rows.length
      first = yes
      async.eachSeries rows, (i, done) ->
        res.write if first then first = no; '[' else ','
        res.write JSON.stringify i
        done()
      , -> res.end ']'
    else
      res.json rows

  [type] = req.params
  evts = req.db.use 'client/events'
  if type of evts
    evts[type] req.query, send
  else
    evts.oftype type, req.query, send

app.use '/api/settings/:sub?', (req, res, next) ->
  settings = req.db.use 'client/settings'

  id = req.user.id
  req.settings =
    get: settings.get.bind settings, id
    update: settings.update.bind settings, id

  next()

app.get '/api/settings/:sub?', (req, res, next) ->
  req.settings.get req.params.sub, (err, settings) ->
    return next err if err?
    res.json settings

app.put '/api/settings/:sub?', (req, res, next) ->
  req.settings.update req.params.sub, req.body, (err, body) ->
    return next err if err?
    res.json body

app.use '/api/aws', require './routes/aws'
app.use require './routes/collections'

app.use (err, req, res, next) ->
  lines = err.stack.split '\n'
  console.error chalk.yellow lines[...15].join '\n'
  console.error chalk.yellow '  [stack trimmed]...' if lines.length > 15

  # TODO logging and raising an error
  res.status(500).send err.message

app.use errorHandler()

app.use (req, res, next) ->
  req.db?.cleanup?()
  next()

#if require.main is module or process.env.IISNODE_VERSION
app.use '/superadmin', require '../webadmin/app'
app.get '*', (req, res) -> res.render 'app'

server = app.listen app.get('port'), ->
  console.log "Express server listening on port #{app.get('port')}"

wsSocketProxy = httpProxy.createProxyServer target: 'ws://localhost:9999', ws: yes
wsSocketProxy.on 'error', (err, req, res) ->
  console.error err
  # TODO not sure what if anything to send to the client here...
server.on 'upgrade', (req, res, head) -> wsSocketProxy.ws req, res, head
