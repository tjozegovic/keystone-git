if (~process.cwd().indexOf('/web') && process.env.NODE_CONFIG_DIR == null) {
  console.log(process.cwd())
  process.env.NODE_CONFIG_DIR = '../config'
}

require('coffee-script/register');
require('./app');
