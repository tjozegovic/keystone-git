_ = require 'lodash'

module.exports.correctids = (req, res, next) ->
  # if there is a cname used, then the :client parameter
  # will be set to the id needed for the item
  {client, id} = req.params

  if req._iscname and client? and not id?
    req.locals.client = req._client.id
    req.locals.id = client
  else if client isnt req._client.id
    # we have a client id, we should reset _client to req.client
    delete req._client

  next()

module.exports.nocache = (req, res, next) ->
  res.set 'Cache-Control', 'private,no-cache,no-store,must-revalidate'
  res.set 'Expires', -1
  res.set 'Pragma', 'no-cache'
  next()

module.exports.loadclient = (req, res, next) ->
  return next() if req.isasset or req._client
  return next() unless client_id = req.locals.client or req.session.client_id

  req.db.client = client_id
  clients = req.db.use 'core/clients'
  clients.get client_id, (err, body) ->
    return next err if err and not err.message in ['missing', 'deleted']

    req._client = body
    next()
