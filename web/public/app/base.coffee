uuid =
  v4: ->
    'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace /[xy]/g, (c) ->
      r = Math.random() * 16 | 0
      v = if c == 'x' then r else (r & 0x3 | 0x8)
      return v.toString 16

_.extend window, uuid: uuid

Array::compare = (array) ->
  return false unless array
  return false if @length isnt array.length

  for i, idx in @
    if i instanceof Array and array[idx] instanceof Array
      return false unless i.compare array[idx]
    return false unless i is array[idx]

  true

Array::swap = (a, b) ->
  @[a] = @splice(b, 1, @[a])[0]
  @

angular.module 'app', [
  'ngAnimate'
  'ngCookies'
  'ngResource'
  # 'ngRoute'
  'ngCkeditor'
  'ngSanitize'
  'ngTagsInput'
  'ui.bootstrap'
  'ui.ace'
  'ui.router'
  'ui.select'
  'ui.sortable'
  'angularMoment'
  'angularFileUpload'
  'angular.filter'
  # 'blueimp.fileupload'
  'infinite-scroll'
  'truncate'
  'app.reports'
  'app.manage'
  'app.admin'
]
  .config [
    'tagsInputConfigProvider',
    (tagsInputConfigProvider) ->
      tagsInputConfigProvider.setDefaults 'tagsInput', {
        addOnPaste: true,
        pasteSplitPattern: "\n"
      }
  ]

  .constant 'ckeditor.config',
    extraPlugins: ''
    entities: false
    skin: 'bootstrapck,/bower_components/bootstrapck4-skin/skins/bootstrapck/'

    # override ng-ckeditor
    toolbar: null
    toolbar_full: null

    allowedContent: yes
    customConfig: ''
    contentsCss: ''

    # handlebar templates are protected source
    #protectedSource: [/\{\{[\s\S]*?\}\}/g]

    # The toolbar groups arrangement, optimized for two toolbar rows.
    toolbarGroups: [
      { name: 'clipboard', groups: ['clipboard', 'undo'] }
      { name: 'editing', groups: ['find', 'selection', 'spellchecker'] }
      { name: 'links' }
      { name: 'insert' }
      { name: 'forms' }
      { name: 'tools' }
      { name: 'document', groups: ['mode', 'document', 'doctools'] }
      { name: 'others' }
      '/'
      { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] }
      { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] }
      { name: 'styles' }
      { name: 'colors' }
      { name: 'strinsert' }
    ]

    # Remove some buttons provided by the standard plugins, which are
    # not needed in the Standard(s) toolbar.
    removeButtons: 'Underline,Subscript,Superscript'

    # Set the most common block elements.
    format_tags: 'p;h1;h2;h3;pre'

    # Simplify the dialog windows.
    removeDialogTabs: 'image:advanced;link:advanced'

    strinsert_button_label: 'Replace'
    strinsert_strings: [
      { name: 'Lead' }
      { name: 'First Name', value: '{{recipient.firstname}}' }
      { name: 'Last Name', value: '{{recipient.lastname}}' }
      { name: 'Email', value: '{{recipient.email}}' }
      { name: 'Address 1', value: '{{recipient.address.address1}}' }
      { name: 'Address 2', value: '{{recipient.address.address2}}' }
      { name: 'City', value: '{{recipient.address.city}}' }
      { name: 'State', value: '{{recipient.address.state}}' }
      { name: 'Zip', value: '{{recipient.address.zip}}' }
      { name: 'Sender' }
      { name: 'First Name', value: '{{sender.firstname}}' }
      { name: 'Last Name', value: '{{sender.lastname}}' }
      { name: 'Email', value: '{{sender.email}}' }
      { name: 'Address 1', value: '{{sender.address.address1}}' }
      { name: 'Address 2', value: '{{sender.address.address2}}' }
      { name: 'City', value: '{{sender.address.city}}' }
      { name: 'State', value: '{{sender.address.state}}' }
      { name: 'Zip', value: '{{sender.address.zip}}' }
    ]

  .run ->
    # CKEDITOR mangles the source view... make it a little more reasonable
    CKEDITOR.on 'instanceReady', (ev) ->
      {dtd} = CKEDITOR
      for e of CKEDITOR.tools.extend {}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent
        ev.editor.dataProcessor.writer.setRules e,
          indent: false
          breakBeforeOpen: false
          breakAfterOpen: false
          breakBeforeClose: false
          breakAfterClose: true

      for e of CKEDITOR.tools.extend {}, dtd.$list, dtd.$listItem, dtd.$tableContent
        ev.editor.dataProcessor.writer.setRules e,
          indent: true


  .config [
    '$locationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider',
    ($locationProvider, $stateProvider, $urlRouterProvider, $httpProvider) ->
      $locationProvider.html5Mode yes

      $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest'
      $httpProvider.interceptors.push 'httpRecoverInterceptor'

      $urlRouterProvider.otherwise '/reports/dashboard'

      $stateProvider
        .state 'sections',
          url: '/:section/:page'
          templateUrl: (params) -> "html/#{params.section}/#{params.page}.html"
  ]

  .config ['$animateProvider', ($animateProvider) ->
    # TODO there are some items that shouldn't fade...
    # angular doesn't provide an all but these method
    $animateProvider.classNameFilter /(slide|fade)/
  ]

  # TODO BUG this is a ui.bootstrap bug
  # https://github.com/angular-ui/bootstrap/issues/2828
  .config ['$uibTooltipProvider', ($uibTooltipProvider) ->
    $uibTooltipProvider.options animation: false
  ]

  .run ['$rootScope', ($rootScope) ->
    # support naming forms
    # e.g. form(name='forms.templates')
    $rootScope.form = $rootScope.forms = {}

    $rootScope.$on '$stateChangeSuccess', ->
      $("body").animate({ scrollTop: 0 })

    $rootScope.settings =
      try
        settings = Cookies.get 'settings'
        JSON.parse atob settings
      catch
        {}

    $rootScope.$watchCollection 'settings', (settings, old) ->
      return unless settings isnt old
      Cookies.set 'settings', btoa JSON.stringify settings
  ]

  .run ['$rootScope', '$state', ($rootScope, $state) ->
    # adding some view stuff
    $rootScope.go = $state.go.bind $state
  ]

  .run [
    '$rootScope', '$location', '$http', '$timeout',
    ($rootScope, $location, $http, $timeout) ->
      $rootScope.route = {}
      $rootScope.system = {}
      $http.get('api/system').success $rootScope.setSystem = (check) ->
        check.dailyvu ?= {}
        check.dailyvu.prettyTime ?= moment(check.dailyvu.time, 'HHmm').format('h:mm a')
        _.assign $rootScope.system, check

      $rootScope.$watch 'system', (nv, ov) ->
        # make sure all the keys we need are present before checking and potentially setting offline
        attrs = ['groups', 'senders', 'attributes', 'statuses']
        return unless nv? and _.every attrs, (key) -> key of nv

        nv.online = _.every attrs, (key) -> nv[key] > 0
        nv.offline = !nv.online
      , yes

      do (_start = null) ->
        $rootScope.$on '$stateChangeStart', ->
          _start = Date.now()
          $rootScope.$viewLoading = yes
        $rootScope.$on '$stateChangeSuccess', ->
          $timeout ->
            $rootScope.$viewLoading = no
          , Math.max 0, 650 - (Date.now() - _start)
  ]

  .run ['$timeout', ($timeout) -> do (ace) ->
    dom = ace.require 'ace/lib/dom'
    commands = ace.require('ace/commands/default_commands').commands

    commands.push
      name: 'Toggle Fullscreen'
      bindKey: 'F11'
      exec: (editor) ->
        dom.toggleCssClass document.body, 'fullScreen'
        dom.toggleCssClass editor.container, 'fullScreen-editor'
        editor.resize()

    ace.config.set 'basePath', '/bower_components/ace-builds/src-min-noconflict'
    ace.config.set 'workerPath', '/bower_components/ace-builds/src-min-noconflict'

    ace.require 'ace/ext/language_tools'
    ace.config.on 'editor', (editor) ->
      $timeout ->
        editor.setTheme 'ace/theme/monokai'
        editor.getSession().setMode 'ace/mode/html'
        editor.setShowPrintMargin no
        editor.getSession().setUseWrapMode yes
  ]



  .value 'type', ''
  .controller 'ConfirmModal', ['$scope', '$uibModal', '$uibModalInstance', 'type', ($scope, $uibModal, $uibModalInstance, type) ->
    $scope.type = type

    $scope.confirm = -> $uibModalInstance.close()
    $scope.dismiss = -> $uibModalInstance.dismiss()
  ]

angular.module('app.admin', [])
angular.module('app.manage', [])
angular.module('app.reports', [])

$ -> angular.bootstrap document, ['app']

do (open = 0) ->
  $(document).on 'hidden.bs.modal', (e) ->
    $(e.target).removeClass 'fv-modal-stack'
    --open

  $(document).on 'shown.bs.modal', (e) ->
    el = $ e.target

    # if the z-index of this modal has been set, ignore.
    return if el.is '.fv-modal-stack'

    ++open
    el.css 'z-index', 1040 + (10 * open)
      .addClass 'fv-modal-stack'

    $('.modal-backdrop').not('.fv-modal-stack')
      .css 'z-index', 1039 + (10 * open)
      .addClass 'fv-modal-stack'

do ($ = jQuery) ->
  $(document).on 'mouseover', '.nav-tabs[data-toggle="tab-hover"] > li > a', (e) ->
    $(e.target).tab 'show'

  $(document).on 'change', '#admin-setclient select', ->
    $(@).closest('form').submit()

do ($ = jQuery) ->
  # Extend jQuery with functions for PUT and DELETE requests.
  _ajax_request = (url, data, callback, type, method) ->
    if $.isFunction data
      callback = data
      data = {}

    $.ajax
      type: method
      url: url
      data: data
      success: callback
      dataType: type

  $.extend
    postJSON: (url, data, callback, type) ->
      $.ajax
        url: url
        type: 'POST'
        data: JSON.stringify data
        success: callback
        contentType: 'application/json'
        dataType: type
        processData: false

    put: (url, data, callback, type) ->
      _ajax_request url, data, callback, type, 'PUT'

    delete_: (url, data, callback, type) ->
      _ajax_request url, data, callback, type, 'DELETE'

do ($ = jQuery) ->
  ua = navigator.userAgent.toLowerCase()
  if ~ua.indexOf('safari') and !~ua.indexOf('chrome')
    cssvhBug = ->
      rules = [
        '.fixed-height .columns-sv'
        '.fixed-height .columns-sv > div > div'
      ]
      _.forEach rules, (rule) ->
        $(rule).css 'height', $(window).height() - 200

      rules = [
        '#container.has-alerts .fixed-height .columns-sv'
        '#container.has-alerts .fixed-height .columns-sv > div > div'
      ]
      _.forEach rules, (rule) ->
        $(rule).css 'height', $(window).height() - 272

    cssvhBug()
    $(window).bind 'resize', cssvhBug

do ($ = jQuery) ->
  clips = new Clipboard '.copyable'
  clips.on 'success', (e) ->
    tip = $ e.trigger
    tip.tooltip 'show'
    setTimeout (-> tip.tooltip 'destroy'), 2000
