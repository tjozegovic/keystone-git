(function() {
  var uuid;

  uuid = {
    v4: function() {
      return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r, v;
        r = Math.random() * 16 | 0;
        v = c === 'x' ? r : r & 0x3 | 0x8;
        return v.toString(16);
      });
    }
  };

  _.extend(window, {
    uuid: uuid
  });

  Array.prototype.compare = function(array) {
    var i, idx, j, len;
    if (!array) {
      return false;
    }
    if (this.length !== array.length) {
      return false;
    }
    for (idx = j = 0, len = this.length; j < len; idx = ++j) {
      i = this[idx];
      if (i instanceof Array && array[idx] instanceof Array) {
        if (!i.compare(array[idx])) {
          return false;
        }
      }
      if (i !== array[idx]) {
        return false;
      }
    }
    return true;
  };

  Array.prototype.swap = function(a, b) {
    this[a] = this.splice(b, 1, this[a])[0];
    return this;
  };

  angular.module('app', ['ngAnimate', 'ngCookies', 'ngResource', 'ngCkeditor', 'ngSanitize', 'ngTagsInput', 'ui.bootstrap', 'ui.ace', 'ui.router', 'ui.select', 'ui.sortable', 'angularMoment', 'angularFileUpload', 'angular.filter', 'infinite-scroll', 'truncate', 'app.reports', 'app.manage', 'app.admin']).config([
    'tagsInputConfigProvider', function(tagsInputConfigProvider) {
      return tagsInputConfigProvider.setDefaults('tagsInput', {
        addOnPaste: true,
        pasteSplitPattern: "\n"
      });
    }
  ]).constant('ckeditor.config', {
    extraPlugins: '',
    entities: false,
    skin: 'bootstrapck,/bower_components/bootstrapck4-skin/skins/bootstrapck/',
    toolbar: null,
    toolbar_full: null,
    allowedContent: true,
    customConfig: '',
    contentsCss: '',
    toolbarGroups: [
      {
        name: 'clipboard',
        groups: ['clipboard', 'undo']
      }, {
        name: 'editing',
        groups: ['find', 'selection', 'spellchecker']
      }, {
        name: 'links'
      }, {
        name: 'insert'
      }, {
        name: 'forms'
      }, {
        name: 'tools'
      }, {
        name: 'document',
        groups: ['mode', 'document', 'doctools']
      }, {
        name: 'others'
      }, '/', {
        name: 'basicstyles',
        groups: ['basicstyles', 'cleanup']
      }, {
        name: 'paragraph',
        groups: ['list', 'indent', 'blocks', 'align', 'bidi']
      }, {
        name: 'styles'
      }, {
        name: 'colors'
      }, {
        name: 'strinsert'
      }
    ],
    removeButtons: 'Underline,Subscript,Superscript',
    format_tags: 'p;h1;h2;h3;pre',
    removeDialogTabs: 'image:advanced;link:advanced',
    strinsert_button_label: 'Replace',
    strinsert_strings: [
      {
        name: 'Lead'
      }, {
        name: 'First Name',
        value: '{{recipient.firstname}}'
      }, {
        name: 'Last Name',
        value: '{{recipient.lastname}}'
      }, {
        name: 'Email',
        value: '{{recipient.email}}'
      }, {
        name: 'Address 1',
        value: '{{recipient.address.address1}}'
      }, {
        name: 'Address 2',
        value: '{{recipient.address.address2}}'
      }, {
        name: 'City',
        value: '{{recipient.address.city}}'
      }, {
        name: 'State',
        value: '{{recipient.address.state}}'
      }, {
        name: 'Zip',
        value: '{{recipient.address.zip}}'
      }, {
        name: 'Sender'
      }, {
        name: 'First Name',
        value: '{{sender.firstname}}'
      }, {
        name: 'Last Name',
        value: '{{sender.lastname}}'
      }, {
        name: 'Email',
        value: '{{sender.email}}'
      }, {
        name: 'Address 1',
        value: '{{sender.address.address1}}'
      }, {
        name: 'Address 2',
        value: '{{sender.address.address2}}'
      }, {
        name: 'City',
        value: '{{sender.address.city}}'
      }, {
        name: 'State',
        value: '{{sender.address.state}}'
      }, {
        name: 'Zip',
        value: '{{sender.address.zip}}'
      }
    ]
  }).run(function() {
    return CKEDITOR.on('instanceReady', function(ev) {
      var dtd, e, results;
      dtd = CKEDITOR.dtd;
      for (e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        ev.editor.dataProcessor.writer.setRules(e, {
          indent: false,
          breakBeforeOpen: false,
          breakAfterOpen: false,
          breakBeforeClose: false,
          breakAfterClose: true
        });
      }
      results = [];
      for (e in CKEDITOR.tools.extend({}, dtd.$list, dtd.$listItem, dtd.$tableContent)) {
        results.push(ev.editor.dataProcessor.writer.setRules(e, {
          indent: true
        }));
      }
      return results;
    });
  }).config([
    '$locationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider', function($locationProvider, $stateProvider, $urlRouterProvider, $httpProvider) {
      $locationProvider.html5Mode(true);
      $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
      $httpProvider.interceptors.push('httpRecoverInterceptor');
      $urlRouterProvider.otherwise('/reports/dashboard');
      return $stateProvider.state('sections', {
        url: '/:section/:page',
        templateUrl: function(params) {
          return "html/" + params.section + "/" + params.page + ".html";
        }
      });
    }
  ]).config([
    '$animateProvider', function($animateProvider) {
      return $animateProvider.classNameFilter(/(slide|fade)/);
    }
  ]).config([
    '$uibTooltipProvider', function($uibTooltipProvider) {
      return $uibTooltipProvider.options({
        animation: false
      });
    }
  ]).run([
    '$rootScope', function($rootScope) {
      var settings;
      $rootScope.form = $rootScope.forms = {};
      $rootScope.$on('$stateChangeSuccess', function() {
        return $("body").animate({
          scrollTop: 0
        });
      });
      $rootScope.settings = (function() {
        try {
          settings = Cookies.get('settings');
          return JSON.parse(atob(settings));
        } catch (_error) {
          return {};
        }
      })();
      return $rootScope.$watchCollection('settings', function(settings, old) {
        if (settings === old) {
          return;
        }
        return Cookies.set('settings', btoa(JSON.stringify(settings)));
      });
    }
  ]).run([
    '$rootScope', '$state', function($rootScope, $state) {
      return $rootScope.go = $state.go.bind($state);
    }
  ]).run([
    '$rootScope', '$location', '$http', '$timeout', function($rootScope, $location, $http, $timeout) {
      $rootScope.route = {};
      $rootScope.system = {};
      $http.get('api/system').success($rootScope.setSystem = function(check) {
        var base;
        if (check.dailyvu == null) {
          check.dailyvu = {};
        }
        if ((base = check.dailyvu).prettyTime == null) {
          base.prettyTime = moment(check.dailyvu.time, 'HHmm').format('h:mm a');
        }
        return _.assign($rootScope.system, check);
      });
      $rootScope.$watch('system', function(nv, ov) {
        var attrs;
        attrs = ['groups', 'senders', 'attributes', 'statuses'];
        if (!((nv != null) && _.every(attrs, function(key) {
          return key in nv;
        }))) {
          return;
        }
        nv.online = _.every(attrs, function(key) {
          return nv[key] > 0;
        });
        return nv.offline = !nv.online;
      }, true);
      return (function(_start) {
        $rootScope.$on('$stateChangeStart', function() {
          _start = Date.now();
          return $rootScope.$viewLoading = true;
        });
        return $rootScope.$on('$stateChangeSuccess', function() {
          return $timeout(function() {
            return $rootScope.$viewLoading = false;
          }, Math.max(0, 650 - (Date.now() - _start)));
        });
      })(null);
    }
  ]).run([
    '$timeout', function($timeout) {
      return (function(ace) {
        var commands, dom;
        dom = ace.require('ace/lib/dom');
        commands = ace.require('ace/commands/default_commands').commands;
        commands.push({
          name: 'Toggle Fullscreen',
          bindKey: 'F11',
          exec: function(editor) {
            dom.toggleCssClass(document.body, 'fullScreen');
            dom.toggleCssClass(editor.container, 'fullScreen-editor');
            return editor.resize();
          }
        });
        ace.config.set('basePath', '/bower_components/ace-builds/src-min-noconflict');
        ace.config.set('workerPath', '/bower_components/ace-builds/src-min-noconflict');
        ace.require('ace/ext/language_tools');
        return ace.config.on('editor', function(editor) {
          return $timeout(function() {
            editor.setTheme('ace/theme/monokai');
            editor.getSession().setMode('ace/mode/html');
            editor.setShowPrintMargin(false);
            return editor.getSession().setUseWrapMode(true);
          });
        });
      })(ace);
    }
  ]).value('type', '').controller('ConfirmModal', [
    '$scope', '$uibModal', '$uibModalInstance', 'type', function($scope, $uibModal, $uibModalInstance, type) {
      $scope.type = type;
      $scope.confirm = function() {
        return $uibModalInstance.close();
      };
      return $scope.dismiss = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]);

  angular.module('app.admin', []);

  angular.module('app.manage', []);

  angular.module('app.reports', []);

  $(function() {
    return angular.bootstrap(document, ['app']);
  });

  (function(open) {
    $(document).on('hidden.bs.modal', function(e) {
      $(e.target).removeClass('fv-modal-stack');
      return --open;
    });
    return $(document).on('shown.bs.modal', function(e) {
      var el;
      el = $(e.target);
      if (el.is('.fv-modal-stack')) {
        return;
      }
      ++open;
      el.css('z-index', 1040 + (10 * open)).addClass('fv-modal-stack');
      return $('.modal-backdrop').not('.fv-modal-stack').css('z-index', 1039 + (10 * open)).addClass('fv-modal-stack');
    });
  })(0);

  (function($) {
    $(document).on('mouseover', '.nav-tabs[data-toggle="tab-hover"] > li > a', function(e) {
      return $(e.target).tab('show');
    });
    return $(document).on('change', '#admin-setclient select', function() {
      return $(this).closest('form').submit();
    });
  })(jQuery);

  (function($) {
    var _ajax_request;
    _ajax_request = function(url, data, callback, type, method) {
      if ($.isFunction(data)) {
        callback = data;
        data = {};
      }
      return $.ajax({
        type: method,
        url: url,
        data: data,
        success: callback,
        dataType: type
      });
    };
    return $.extend({
      postJSON: function(url, data, callback, type) {
        return $.ajax({
          url: url,
          type: 'POST',
          data: JSON.stringify(data),
          success: callback,
          contentType: 'application/json',
          dataType: type,
          processData: false
        });
      },
      put: function(url, data, callback, type) {
        return _ajax_request(url, data, callback, type, 'PUT');
      },
      delete_: function(url, data, callback, type) {
        return _ajax_request(url, data, callback, type, 'DELETE');
      }
    });
  })(jQuery);

  (function($) {
    var cssvhBug, ua;
    ua = navigator.userAgent.toLowerCase();
    if (~ua.indexOf('safari') && !~ua.indexOf('chrome')) {
      cssvhBug = function() {
        var rules;
        rules = ['.fixed-height .columns-sv', '.fixed-height .columns-sv > div > div'];
        _.forEach(rules, function(rule) {
          return $(rule).css('height', $(window).height() - 200);
        });
        rules = ['#container.has-alerts .fixed-height .columns-sv', '#container.has-alerts .fixed-height .columns-sv > div > div'];
        return _.forEach(rules, function(rule) {
          return $(rule).css('height', $(window).height() - 272);
        });
      };
      cssvhBug();
      return $(window).bind('resize', cssvhBug);
    }
  })(jQuery);

  (function($) {
    var clips;
    clips = new Clipboard('.copyable');
    return clips.on('success', function(e) {
      var tip;
      tip = $(e.trigger);
      tip.tooltip('show');
      return setTimeout((function() {
        return tip.tooltip('destroy');
      }), 2000);
    });
  })(jQuery);

}).call(this);
