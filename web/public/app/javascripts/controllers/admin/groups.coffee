angular.module('app.admin')
  .controller 'AdminGroups', ['$scope', '$http', '$uibModal', 'growl', ($scope, $http, $uibModal, growl) ->
    $http.get('api/groups?q=active')
    .success (groups) ->
      $scope.groups = groups
    .error (err) ->
      growl.danger 'Failed to load groups. ' + err

    $scope.create = ->
      inst = $uibModal.open
        templateUrl: 'group-modal.html'
        controller: 'GroupModal'
        size: 'lg'
        backdrop: false
        resolve: group: -> {}

      inst.result.then (group) ->
        return unless group.id?

        $scope.groups.push group
        $scope.system.groups += 1

    $scope.edit = (ref) ->
      inst = $uibModal.open
        templateUrl: 'group-modal.html'
        controller: 'GroupModal'
        size: 'lg'
        backdrop: false
        resolve: group: -> angular.copy ref

      inst.result.then (group) ->
        angular.copy group, ref

    $scope.delete = (group) ->
      inst = $uibModal.open
        templateUrl: 'confirm-modal.html'
        controller: 'ConfirmModal'

      inst.result.then ->
        $http.delete('api/groups/' + group.id)
        .success ->
          $scope.groups.splice $scope.groups.indexOf(group), 1
          $scope.system.groups -= 1
          growl.success 'Deleted!'
        .error (err) ->
          growl.danger err
  ]

  .controller 'GroupModal', [
    '$scope', '$http', '$uibModalInstance', '$timeout', 'group', 'modalCheck',
    'dismissCheck', 'saveTimeout', 'errorTimeout',
    ($scope, $http, $uibModalInstance, $timeout, group, modalCheck,
    dismissCheck, saveTimeout, errorTimeout) ->
      $scope.forms = {}
      $scope.group = group
      group_ref = {}
      angular.copy $scope.group, group_ref
      $scope.phones = _.map $scope.group.phones, (number, type) -> type: type, number: number

      $scope.save = (cb) ->
        start = Date.now()
        $scope.forms.group.saving = yes
        $scope.group.phones = _.object _.pluck($scope.phones, 'type'), _.pluck($scope.phones, 'number')

        promise = unless $scope.group.id?
          $http.post 'api/groups', $scope.group
        else
          $http.put "api/groups/#{$scope.group.id}", $scope.group

        promise
          .success (data) ->
            _.assign $scope.group, data
            angular.copy $scope.group, group_ref
            if cb? then cb null, $scope.group
            saveTimeout.begin start, $scope.forms.group
          .error (err) ->
            $scope.errorMessage = err
            if cb? then cb err
            errorTimeout.begin $scope.forms.group, start

      $scope.close = ->
        if $scope.forms.group.saved and $scope.forms.group.$pristine
          $uibModalInstance.close $scope.group
        else if $scope.forms.group.$dirty or $scope.forms.group.submitted
          dismissCheck.dismiss $uibModalInstance, item: $scope.group, ref: group_ref, save: $scope.save
        else
          $uibModalInstance.dismiss()
    ]
