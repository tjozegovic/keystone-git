(function() {
  angular.module('app.admin').controller('AdminGroups', [
    '$scope', '$http', '$uibModal', 'growl', function($scope, $http, $uibModal, growl) {
      $http.get('api/groups?q=active').success(function(groups) {
        return $scope.groups = groups;
      }).error(function(err) {
        return growl.danger('Failed to load groups. ' + err);
      });
      $scope.create = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'group-modal.html',
          controller: 'GroupModal',
          size: 'lg',
          backdrop: false,
          resolve: {
            group: function() {
              return {};
            }
          }
        });
        return inst.result.then(function(group) {
          if (group.id == null) {
            return;
          }
          $scope.groups.push(group);
          return $scope.system.groups += 1;
        });
      };
      $scope.edit = function(ref) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'group-modal.html',
          controller: 'GroupModal',
          size: 'lg',
          backdrop: false,
          resolve: {
            group: function() {
              return angular.copy(ref);
            }
          }
        });
        return inst.result.then(function(group) {
          return angular.copy(group, ref);
        });
      };
      return $scope["delete"] = function(group) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal'
        });
        return inst.result.then(function() {
          return $http["delete"]('api/groups/' + group.id).success(function() {
            $scope.groups.splice($scope.groups.indexOf(group), 1);
            $scope.system.groups -= 1;
            return growl.success('Deleted!');
          }).error(function(err) {
            return growl.danger(err);
          });
        });
      };
    }
  ]).controller('GroupModal', [
    '$scope', '$http', '$uibModalInstance', '$timeout', 'group', 'modalCheck', 'dismissCheck', 'saveTimeout', 'errorTimeout', function($scope, $http, $uibModalInstance, $timeout, group, modalCheck, dismissCheck, saveTimeout, errorTimeout) {
      var group_ref;
      $scope.forms = {};
      $scope.group = group;
      group_ref = {};
      angular.copy($scope.group, group_ref);
      $scope.phones = _.map($scope.group.phones, function(number, type) {
        return {
          type: type,
          number: number
        };
      });
      $scope.save = function(cb) {
        var promise, start;
        start = Date.now();
        $scope.forms.group.saving = true;
        $scope.group.phones = _.object(_.pluck($scope.phones, 'type'), _.pluck($scope.phones, 'number'));
        promise = $scope.group.id == null ? $http.post('api/groups', $scope.group) : $http.put("api/groups/" + $scope.group.id, $scope.group);
        return promise.success(function(data) {
          _.assign($scope.group, data);
          angular.copy($scope.group, group_ref);
          if (cb != null) {
            cb(null, $scope.group);
          }
          return saveTimeout.begin(start, $scope.forms.group);
        }).error(function(err) {
          $scope.errorMessage = err;
          if (cb != null) {
            cb(err);
          }
          return errorTimeout.begin($scope.forms.group, start);
        });
      };
      return $scope.close = function() {
        if ($scope.forms.group.saved && $scope.forms.group.$pristine) {
          return $uibModalInstance.close($scope.group);
        } else if ($scope.forms.group.$dirty || $scope.forms.group.submitted) {
          return dismissCheck.dismiss($uibModalInstance, {
            item: $scope.group,
            ref: group_ref,
            save: $scope.save
          });
        } else {
          return $uibModalInstance.dismiss();
        }
      };
    }
  ]);

}).call(this);
