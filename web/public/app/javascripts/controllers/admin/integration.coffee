angular.module('app.admin')
  .controller 'AdminIntegration', ['$scope', '$http', '$q', ($scope, $http, $q) ->
    $scope.forms = {}
    $scope.showing = {}

    $scope.egutc = moment().utc().toISOString()
    $scope.egexternal = encodeURIComponent JSON.stringify source: "source", id: 12345

    apiurl = (url) -> $http.get 'api/' + url
    $q.all(['integration', 'attributes?q=active', 'statuses?q=active'].map apiurl).then (results) ->
      [integration, attributes, statuses] = (x.data for x in results)
      _.assign $scope, integration

      $scope.examples =
        lead:
          status: '[' + _.pluck(statuses, 'name').join(', ') + ']'
          attributes: {}
          agent: 'jane.agent@email.com'
          recipient:
            firstname: 'John'
            lastname: 'Doe'
            email: 'john.doe@email.com'
            address:
              line1: '12345 Easy St.'
              line2: 'address2'
              city: 'Nowhere'
              state: 'AK'
              zip: '12345'
            phones:
              'primary': '1234567890'
              'cell': '0987654321'
              'group': '1236547890'
          external:
            source: 'source'
            id: 12345

        optout:
          email: 'email@example.com'
          source: 'website, phone, etc. // optional field'

        group:
          name: 'Group Name'
          email: 'group@domain.com'
          address:
            address1: '12345 Easy St.'
            address2: ''
            city: 'Somewhere'
            state: 'AK'
            zip: '12345'

        sender:
          email: 'sender@domain.com'
          group: '<guid>'
          active: yes
          firstname: 'sender'
          lastname: 'lastname'
          phones:
            main: '(123) 456-7890'
            cell: '123-456-7890'
            fax: '123.456.7890'
          timezone: 'America/Chicago'
          custom:
            NMLS: '1234567890'
          override: yes
          signature: '<p>An html signature.</p>'

        velocify:
          lead: """
  <root>
    <status>{Lead.Status}</status>
    <attributes>
      <propertytype>{Intended Property Use}</propertytype>
    </attributes>
    <agent>{User.Email}</agent>
    <recipient>
      <firstname>{First Name}</firstname>
      <lastname>{Last Name}</lastname>
      <email>{Email}</email>
      <address>
        <line1>{Street}</line1>
        <city>{City}</city>
        <state>{State}</state>
        <zip>{Zip Code}</zip>
      </address>
      <phones>
        <primary>{Day Phone}</primary>
        <cell>{Mobile Phone}</cell>
      </phones>
    </recipient>
    <external>
      <source>velocify</source>
      <id>{Lead.Id}</id>
    </external>
  </root>"""

      _.forOwn attributes, (attr) ->
        name = attr.name.toLowerCase().replace /\s/g, ''
        $scope.examples.lead.attributes[name] = '[' + _.pluck(attr.values, 'name').join(', ') + ']'

    $scope.newKey = ->
      $http.post('api/apikeys/create').success (data) ->
        if !$scope.apikeys? then $scope.apikeys = key: data.id, issued: 'Now'
        else $scope.apikeys.push key: data.id, issued: 'Now'

    $scope.updateWebhooks = ->
      $scope.forms.webhooks?.error = no
      $http.put('api/webhooks', $scope.webhooks).success (data) ->
        $scope.forms.webhooks?.$setPristine()
        $scope.forms.webhooks?.$setSubmitted()
      .error (data) ->
        $scope.forms.webhooks?.$setPristine()
        $scope.forms.webhooks?.error = yes
  ]
