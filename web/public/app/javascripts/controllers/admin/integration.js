(function() {
  angular.module('app.admin').controller('AdminIntegration', [
    '$scope', '$http', '$q', function($scope, $http, $q) {
      var apiurl;
      $scope.forms = {};
      $scope.showing = {};
      $scope.egutc = moment().utc().toISOString();
      $scope.egexternal = encodeURIComponent(JSON.stringify({
        source: "source",
        id: 12345
      }));
      apiurl = function(url) {
        return $http.get('api/' + url);
      };
      $q.all(['integration', 'attributes?q=active', 'statuses?q=active'].map(apiurl)).then(function(results) {
        var attributes, integration, ref, statuses, x;
        ref = (function() {
          var i, len, results1;
          results1 = [];
          for (i = 0, len = results.length; i < len; i++) {
            x = results[i];
            results1.push(x.data);
          }
          return results1;
        })(), integration = ref[0], attributes = ref[1], statuses = ref[2];
        _.assign($scope, integration);
        $scope.examples = {
          lead: {
            status: '[' + _.pluck(statuses, 'name').join(', ') + ']',
            attributes: {},
            agent: 'jane.agent@email.com',
            recipient: {
              firstname: 'John',
              lastname: 'Doe',
              email: 'john.doe@email.com',
              address: {
                line1: '12345 Easy St.',
                line2: 'address2',
                city: 'Nowhere',
                state: 'AK',
                zip: '12345'
              },
              phones: {
                'primary': '1234567890',
                'cell': '0987654321',
                'group': '1236547890'
              }
            },
            external: {
              source: 'source',
              id: 12345
            }
          },
          optout: {
            email: 'email@example.com',
            source: 'website, phone, etc. // optional field'
          },
          group: {
            name: 'Group Name',
            email: 'group@domain.com',
            address: {
              address1: '12345 Easy St.',
              address2: '',
              city: 'Somewhere',
              state: 'AK',
              zip: '12345'
            }
          },
          sender: {
            email: 'sender@domain.com',
            group: '<guid>',
            active: true,
            firstname: 'sender',
            lastname: 'lastname',
            phones: {
              main: '(123) 456-7890',
              cell: '123-456-7890',
              fax: '123.456.7890'
            },
            timezone: 'America/Chicago',
            custom: {
              NMLS: '1234567890'
            },
            override: true,
            signature: '<p>An html signature.</p>'
          },
          velocify: {
            lead: "<root>\n  <status>{Lead.Status}</status>\n  <attributes>\n    <propertytype>{Intended Property Use}</propertytype>\n  </attributes>\n  <agent>{User.Email}</agent>\n  <recipient>\n    <firstname>{First Name}</firstname>\n    <lastname>{Last Name}</lastname>\n    <email>{Email}</email>\n    <address>\n      <line1>{Street}</line1>\n      <city>{City}</city>\n      <state>{State}</state>\n      <zip>{Zip Code}</zip>\n    </address>\n    <phones>\n      <primary>{Day Phone}</primary>\n      <cell>{Mobile Phone}</cell>\n    </phones>\n  </recipient>\n  <external>\n    <source>velocify</source>\n    <id>{Lead.Id}</id>\n  </external>\n</root>"
          }
        };
        return _.forOwn(attributes, function(attr) {
          var name;
          name = attr.name.toLowerCase().replace(/\s/g, '');
          return $scope.examples.lead.attributes[name] = '[' + _.pluck(attr.values, 'name').join(', ') + ']';
        });
      });
      $scope.newKey = function() {
        return $http.post('api/apikeys/create').success(function(data) {
          if ($scope.apikeys == null) {
            return $scope.apikeys = {
              key: data.id,
              issued: 'Now'
            };
          } else {
            return $scope.apikeys.push({
              key: data.id,
              issued: 'Now'
            });
          }
        });
      };
      return $scope.updateWebhooks = function() {
        var ref;
        if ((ref = $scope.forms.webhooks) != null) {
          ref.error = false;
        }
        return $http.put('api/webhooks', $scope.webhooks).success(function(data) {
          var ref1, ref2;
          if ((ref1 = $scope.forms.webhooks) != null) {
            ref1.$setPristine();
          }
          return (ref2 = $scope.forms.webhooks) != null ? ref2.$setSubmitted() : void 0;
        }).error(function(data) {
          var ref1, ref2;
          if ((ref1 = $scope.forms.webhooks) != null) {
            ref1.$setPristine();
          }
          return (ref2 = $scope.forms.webhooks) != null ? ref2.error = true : void 0;
        });
      };
    }
  ]);

}).call(this);
