isValidEmailAddress = (email) ->
  atidx = email.indexOf '@'
  email.length >= 3 && ~atidx && atidx < email.length - 1

angular.module('app.admin')
  .controller 'AdminOptOuts',
    ['$scope', '$http', '$timeout', '$uibModal', '$interval', 'growl',
    ($scope, $http, $timeout, $uibModal, $interval, growl) ->
      $http.get('api/optouts/imports')
      .success (data) ->
        $scope.imports = data
      .error (err) ->
        growl.danger 'Failed to get optouts: ', err

      timed = (min, cb) ->
        [cb, min] = [min, 600] unless cb
        start = Date.now()
        ->
          args = arguments
          $timeout ->
            cb.apply null, args
          , Math.max 0, min - (Date.now() - start)

      $scope.form = {}
      $scope.search = ->
        $scope.details = undefined
        $scope.forms.search.$submitting = yes

        email = $.trim $scope.form.search

        unless isValidEmailAddress email
          start = Date.now()
          $timeout ->
            $scope.forms.search.$submitting = no
            $scope.details = notfound: yes
          , Math.max 0, 600 - (Date.now() - start)
          return
        $http.get('api/optouts/' + email)
          .success timed (data) ->
            if data is 'missing' or not data
              $scope.forms.search.$submitting = no
              $scope.details = notfound: yes
            else
              $scope.forms.search.$submitting = no
              $scope.details = data
              $scope.details.last = data.history[0]
              $scope.details.updated ?= data.created
          .error timed ->
            $scope.forms.search.$submitting = no
            $scope.details = notfound: yes

      $scope.$watch ('form.optout'), (optout) ->
        if optout is ''
          $scope.forms.optout.error = no
          $scope.forms.optout.$success = no

      $scope.optout = ->
        $scope.forms.optout.$success = no
        $scope.forms.optout.$submitting = yes
        $scope.forms.optout.error = no

        start = Date.now()
        emails = $scope.form.optout.split ','
        onsuccess = _.after emails.length, timed ->
          $scope.forms.optout.$submitting = no
          $scope.forms.optout.$success = yes
        onerror = timed ->
          $scope.forms.optout.$submitting = no
          $scope.forms.optout.error = yes

        for email in emails
          if isValidEmailAddress email
            $http.post 'api/optout/' + $.trim email
            .success onsuccess
            .error (err) ->
              onerror()
              growl.danger "Failed to opt-out #{email}", err
          else
            onerror()

      $scope.optin = ->
        $http.post 'api/optin/' + $.trim $scope.form.search
        .success (data) ->
          $scope.search()
        .error (err) ->
          growl.danger "Failed to opt-in #{$scope.form.search}", err

      $scope.reset = ->
        $scope.form.uploading = $scope.form.uploaded = $scope.typeerror = no

      # function that starts running interval. interval checks running / finished
      $scope.startRunning = ->
        $scope.running = yes
        $scope.stopped = no
        $scope.stopping = no
        $scope.runningImport = $scope.imports[0]
        $scope.checkRunning = $interval ->
          if $scope.running and not $scope.runningImport.finished?
            $http.get('api/optouts/imports')
            .error (err) ->
              growl.danger 'Error refreshing import data ' , err
            .success (imports) ->
              $scope.imports = imports
              $scope.runningImport = $scope.imports[0]
          else if $scope.runningImport.finished?
            $scope.stopRunning()
        , 1500

      $scope.stop = (_import) ->
        return unless _import.$job
        $interval.cancel $scope.checkRunning
        $scope.stopping = yes
        $http.post('api/import/cancel/' + _import.$job)
        .success ->
          $scope.running = no
          $scope.startStopping _import
        .error (err) ->
          growl.danger 'error: ', err

      # function that starts stopping interval. interval checks to see if job still exists
      $scope.startStopping = (_import) ->
        $scope.checkStopping = $interval ->
          if _import.$job
            $http.get('api/optouts/imports')
            .error (err) ->
              growl.danger 'Error refreshing import data ' , err
            .success (imports) ->
              $scope.imports = imports
              _import = _.find imports, id: _import.id
          else
            $scope.stopRunning()
        , 3000

      $scope.stopRunning = ->
        $scope.running = no
        $scope.runningImport = undefined
        $interval.cancel $scope.checkRunning

      $scope.stopStopping = ->
        $scope.stopping = no
        $scope.stopped = yes
        $interval.cancel $scope.checkStopping

      $scope.upload = ->
        inst = $uibModal.open
          templateUrl: 'admin/bulk-import-modal.html'
          backdrop: 'static'
          controller: ['$scope', '$uibModalInstance', ($scope, $uibModalInstance) ->
            $scope.forms.import = {}

            $scope.$watch 'forms.import.csv', (csv) ->
              return unless csv
              $scope.forms.import.$setDirty()
              $scope.typeerror = !~csv.name.indexOf '.csv'

            $scope.import = ->
              return $scope.typeerror = yes unless ~$scope.forms.import.csv.name.indexOf '.csv'

              fd = new FormData
              fd.append 'file', $scope.forms.import.csv

              $scope.$saving = yes
              $http.post('api/optout/upload', fd,
                transformRequest: angular.identity
                headers: 'Content-Type': undefined
              ).success ->
                $scope.$saving = no
                $uibModalInstance.close()
          ]

        inst.result.then ->
          start = Date.now()
          $timeout ->
            $http.get('api/optouts/imports').error (err) ->
              growl.danger 'Error loading import data ' , err
            .success (imports) ->
              $scope.imports = imports
              # start running interval
              $scope.startRunning()
          , Math.max 0, 600 - (start - Date.now())

    ]
