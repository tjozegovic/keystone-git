(function() {
  var isValidEmailAddress;

  isValidEmailAddress = function(email) {
    var atidx;
    atidx = email.indexOf('@');
    return email.length >= 3 && ~atidx && atidx < email.length - 1;
  };

  angular.module('app.admin').controller('AdminOptOuts', [
    '$scope', '$http', '$timeout', '$uibModal', '$interval', 'growl', function($scope, $http, $timeout, $uibModal, $interval, growl) {
      var timed;
      $http.get('api/optouts/imports').success(function(data) {
        return $scope.imports = data;
      }).error(function(err) {
        return growl.danger('Failed to get optouts: ', err);
      });
      timed = function(min, cb) {
        var ref, start;
        if (!cb) {
          ref = [min, 600], cb = ref[0], min = ref[1];
        }
        start = Date.now();
        return function() {
          var args;
          args = arguments;
          return $timeout(function() {
            return cb.apply(null, args);
          }, Math.max(0, min - (Date.now() - start)));
        };
      };
      $scope.form = {};
      $scope.search = function() {
        var email, start;
        $scope.details = void 0;
        $scope.forms.search.$submitting = true;
        email = $.trim($scope.form.search);
        if (!isValidEmailAddress(email)) {
          start = Date.now();
          $timeout(function() {
            $scope.forms.search.$submitting = false;
            return $scope.details = {
              notfound: true
            };
          }, Math.max(0, 600 - (Date.now() - start)));
          return;
        }
        return $http.get('api/optouts/' + email).success(timed(function(data) {
          var base;
          if (data === 'missing' || !data) {
            $scope.forms.search.$submitting = false;
            return $scope.details = {
              notfound: true
            };
          } else {
            $scope.forms.search.$submitting = false;
            $scope.details = data;
            $scope.details.last = data.history[0];
            return (base = $scope.details).updated != null ? base.updated : base.updated = data.created;
          }
        })).error(timed(function() {
          $scope.forms.search.$submitting = false;
          return $scope.details = {
            notfound: true
          };
        }));
      };
      $scope.$watch('form.optout', function(optout) {
        if (optout === '') {
          $scope.forms.optout.error = false;
          return $scope.forms.optout.$success = false;
        }
      });
      $scope.optout = function() {
        var email, emails, i, len, onerror, onsuccess, results, start;
        $scope.forms.optout.$success = false;
        $scope.forms.optout.$submitting = true;
        $scope.forms.optout.error = false;
        start = Date.now();
        emails = $scope.form.optout.split(',');
        onsuccess = _.after(emails.length, timed(function() {
          $scope.forms.optout.$submitting = false;
          return $scope.forms.optout.$success = true;
        }));
        onerror = timed(function() {
          $scope.forms.optout.$submitting = false;
          return $scope.forms.optout.error = true;
        });
        results = [];
        for (i = 0, len = emails.length; i < len; i++) {
          email = emails[i];
          if (isValidEmailAddress(email)) {
            results.push($http.post('api/optout/' + $.trim(email)).success(onsuccess).error(function(err) {
              onerror();
              return growl.danger("Failed to opt-out " + email, err);
            }));
          } else {
            results.push(onerror());
          }
        }
        return results;
      };
      $scope.optin = function() {
        return $http.post('api/optin/' + $.trim($scope.form.search)).success(function(data) {
          return $scope.search();
        }).error(function(err) {
          return growl.danger("Failed to opt-in " + $scope.form.search, err);
        });
      };
      $scope.reset = function() {
        return $scope.form.uploading = $scope.form.uploaded = $scope.typeerror = false;
      };
      $scope.startRunning = function() {
        $scope.running = true;
        $scope.stopped = false;
        $scope.stopping = false;
        $scope.runningImport = $scope.imports[0];
        return $scope.checkRunning = $interval(function() {
          if ($scope.running && ($scope.runningImport.finished == null)) {
            return $http.get('api/optouts/imports').error(function(err) {
              return growl.danger('Error refreshing import data ', err);
            }).success(function(imports) {
              $scope.imports = imports;
              return $scope.runningImport = $scope.imports[0];
            });
          } else if ($scope.runningImport.finished != null) {
            return $scope.stopRunning();
          }
        }, 1500);
      };
      $scope.stop = function(_import) {
        if (!_import.$job) {
          return;
        }
        $interval.cancel($scope.checkRunning);
        $scope.stopping = true;
        return $http.post('api/import/cancel/' + _import.$job).success(function() {
          $scope.running = false;
          return $scope.startStopping(_import);
        }).error(function(err) {
          return growl.danger('error: ', err);
        });
      };
      $scope.startStopping = function(_import) {
        return $scope.checkStopping = $interval(function() {
          if (_import.$job) {
            return $http.get('api/optouts/imports').error(function(err) {
              return growl.danger('Error refreshing import data ', err);
            }).success(function(imports) {
              $scope.imports = imports;
              return _import = _.find(imports, {
                id: _import.id
              });
            });
          } else {
            return $scope.stopRunning();
          }
        }, 3000);
      };
      $scope.stopRunning = function() {
        $scope.running = false;
        $scope.runningImport = void 0;
        return $interval.cancel($scope.checkRunning);
      };
      $scope.stopStopping = function() {
        $scope.stopping = false;
        $scope.stopped = true;
        return $interval.cancel($scope.checkStopping);
      };
      return $scope.upload = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'admin/bulk-import-modal.html',
          backdrop: 'static',
          controller: [
            '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
              $scope.forms["import"] = {};
              $scope.$watch('forms.import.csv', function(csv) {
                if (!csv) {
                  return;
                }
                $scope.forms["import"].$setDirty();
                return $scope.typeerror = !~csv.name.indexOf('.csv');
              });
              return $scope["import"] = function() {
                var fd;
                if (!~$scope.forms["import"].csv.name.indexOf('.csv')) {
                  return $scope.typeerror = true;
                }
                fd = new FormData;
                fd.append('file', $scope.forms["import"].csv);
                $scope.$saving = true;
                return $http.post('api/optout/upload', fd, {
                  transformRequest: angular.identity,
                  headers: {
                    'Content-Type': void 0
                  }
                }).success(function() {
                  $scope.$saving = false;
                  return $uibModalInstance.close();
                });
              };
            }
          ]
        });
        return inst.result.then(function() {
          var start;
          start = Date.now();
          return $timeout(function() {
            return $http.get('api/optouts/imports').error(function(err) {
              return growl.danger('Error loading import data ', err);
            }).success(function(imports) {
              $scope.imports = imports;
              return $scope.startRunning();
            });
          }, Math.max(0, 600 - (start - Date.now())));
        });
      };
    }
  ]);

}).call(this);
