angular.module('app.admin')
  .controller 'AdminPolicies',
    ['$scope', '$http', '$uibModal', 'growl',
    ($scope, $http, $uibModal, growl) ->
      $http.get('/api/policies?q=active').success (policies) ->
        $scope.policies = policies

      _open = (policy) ->
        inst = $uibModal.open
          templateUrl: 'policy-modal.html'
          controller: 'PolicyModal'
          size: 'lg'
          backdrop: false
          resolve: policy: -> angular.copy policy

        inst.result.then (policy) ->
          unless not policy.id?
            location = _.findIndex $scope.policies, id: policy.id
            if location is -1 and policy.id?
              $scope.policies.push policy
            else
              $scope.policies[location] = policy

      $scope.create = ->
        _open $scope.ref = {}

      $scope.edit = (policy) ->
        _open policy

      $scope.delete = (policy) ->
        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'

        inst.result.then ->
          $http.delete('/api/policies/' + policy.id)
          .success ->
            $scope.policies = _.reject $scope.policies, (s) -> s.id is policy.id
            growl.success 'Deleted!'
          .error (err) ->
            growl.danger err
    ]

  .controller 'PolicyModal',
    ['$scope', '$http', '$uibModalInstance', '$timeout', 'policy', 'modalCheck', 'dismissCheck',
    'errorTimeout', 'saveTimeout',
    ($scope, $http, $uibModalInstance, $timeout, policy, modalCheck, dismissCheck, errorTimeout, saveTimeout) ->
      $scope['editor.policies'] = extraPlugins: ''

      $scope.forms = {}
      policy_ref = {}
      $scope.policy = policy
      angular.copy $scope.policy, policy_ref
      if $scope.policy.use is 'html'
        $scope.policy.$html = policy.value

      ref = $scope.policy.use

      $scope.save = (cb) ->
        start = Date.now()
        $scope.errorMessage = undefined
        $scope.forms.policy.saved = no
        $scope.forms.policy.submitted = no
        $scope.forms.policy.saving = yes

        if $scope.forms.policy.$invalid
          errorTimeout.begin $scope.forms.policy, start
        else
          if $scope.policy.use is 'html'
            $scope.policy.value = $scope.policy.$html

          if $scope.policy.id
            $http.put('/api/policies/' + $scope.policy.id, $scope.policy)
            .success (data) ->
              $scope.policy.version = data.version
              $scope.policy.updated = new Date
              angular.copy $scope.policy, policy_ref
              saveTimeout.begin start, $scope.forms.policy
              if cb? then cb null, $scope.policy
            .error (err) ->
              $scope.errorMessage = err
              errorTimeout.begin $scope.forms.policy, start
              if cb? then cb err
          else
            $http.post('/api/policies', $scope.policy)
            .success (data) ->
              $scope.policy.id = data.id
              $scope.policy.version = data.version
              $scope.policy.updated = new Date
              angular.copy $scope.policy, policy_ref
              saveTimeout.begin start, $scope.forms.policy
              if cb? then cb null, $scope.policy
            .error (err) ->
              $scope.errorMessage = err
              errorTimeout.begin $scope.forms.policy, start
              if cb? then cb err

      $scope.close = ->
        if $scope.forms.policy.saved and $scope.forms.policy.$pristine
          $uibModalInstance.close $scope.policy
        else if $scope.forms.policy.$dirty or $scope.formErrors or $scope.policy.use isnt ref
          dismissCheck.dismiss $uibModalInstance, item: $scope.policy, ref: policy_ref, save: $scope.save
        else
          $uibModalInstance.dismiss()

    ]
