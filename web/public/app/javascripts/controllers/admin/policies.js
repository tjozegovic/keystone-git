(function() {
  angular.module('app.admin').controller('AdminPolicies', [
    '$scope', '$http', '$uibModal', 'growl', function($scope, $http, $uibModal, growl) {
      var _open;
      $http.get('/api/policies?q=active').success(function(policies) {
        return $scope.policies = policies;
      });
      _open = function(policy) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'policy-modal.html',
          controller: 'PolicyModal',
          size: 'lg',
          backdrop: false,
          resolve: {
            policy: function() {
              return angular.copy(policy);
            }
          }
        });
        return inst.result.then(function(policy) {
          var location;
          if (!(policy.id == null)) {
            location = _.findIndex($scope.policies, {
              id: policy.id
            });
            if (location === -1 && (policy.id != null)) {
              return $scope.policies.push(policy);
            } else {
              return $scope.policies[location] = policy;
            }
          }
        });
      };
      $scope.create = function() {
        return _open($scope.ref = {});
      };
      $scope.edit = function(policy) {
        return _open(policy);
      };
      return $scope["delete"] = function(policy) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal'
        });
        return inst.result.then(function() {
          return $http["delete"]('/api/policies/' + policy.id).success(function() {
            $scope.policies = _.reject($scope.policies, function(s) {
              return s.id === policy.id;
            });
            return growl.success('Deleted!');
          }).error(function(err) {
            return growl.danger(err);
          });
        });
      };
    }
  ]).controller('PolicyModal', [
    '$scope', '$http', '$uibModalInstance', '$timeout', 'policy', 'modalCheck', 'dismissCheck', 'errorTimeout', 'saveTimeout', function($scope, $http, $uibModalInstance, $timeout, policy, modalCheck, dismissCheck, errorTimeout, saveTimeout) {
      var policy_ref, ref;
      $scope['editor.policies'] = {
        extraPlugins: ''
      };
      $scope.forms = {};
      policy_ref = {};
      $scope.policy = policy;
      angular.copy($scope.policy, policy_ref);
      if ($scope.policy.use === 'html') {
        $scope.policy.$html = policy.value;
      }
      ref = $scope.policy.use;
      $scope.save = function(cb) {
        var start;
        start = Date.now();
        $scope.errorMessage = void 0;
        $scope.forms.policy.saved = false;
        $scope.forms.policy.submitted = false;
        $scope.forms.policy.saving = true;
        if ($scope.forms.policy.$invalid) {
          return errorTimeout.begin($scope.forms.policy, start);
        } else {
          if ($scope.policy.use === 'html') {
            $scope.policy.value = $scope.policy.$html;
          }
          if ($scope.policy.id) {
            return $http.put('/api/policies/' + $scope.policy.id, $scope.policy).success(function(data) {
              $scope.policy.version = data.version;
              $scope.policy.updated = new Date;
              angular.copy($scope.policy, policy_ref);
              saveTimeout.begin(start, $scope.forms.policy);
              if (cb != null) {
                return cb(null, $scope.policy);
              }
            }).error(function(err) {
              $scope.errorMessage = err;
              errorTimeout.begin($scope.forms.policy, start);
              if (cb != null) {
                return cb(err);
              }
            });
          } else {
            return $http.post('/api/policies', $scope.policy).success(function(data) {
              $scope.policy.id = data.id;
              $scope.policy.version = data.version;
              $scope.policy.updated = new Date;
              angular.copy($scope.policy, policy_ref);
              saveTimeout.begin(start, $scope.forms.policy);
              if (cb != null) {
                return cb(null, $scope.policy);
              }
            }).error(function(err) {
              $scope.errorMessage = err;
              errorTimeout.begin($scope.forms.policy, start);
              if (cb != null) {
                return cb(err);
              }
            });
          }
        }
      };
      return $scope.close = function() {
        if ($scope.forms.policy.saved && $scope.forms.policy.$pristine) {
          return $uibModalInstance.close($scope.policy);
        } else if ($scope.forms.policy.$dirty || $scope.formErrors || $scope.policy.use !== ref) {
          return dismissCheck.dismiss($uibModalInstance, {
            item: $scope.policy,
            ref: policy_ref,
            save: $scope.save
          });
        } else {
          return $uibModalInstance.dismiss();
        }
      };
    }
  ]);

}).call(this);
