angular.module('app.admin')
  .controller 'AdminSenders',
    ['$scope', '$http', '$uibModal', '$q', 'growl',
    ($scope, $http, $uibModal, $q, growl) ->
      $scope.search = {}

      $scope.sort =
        header: 'Email'
        column: 'email'
        desc: no

      $q.all [$http.get('api/groups?q=active'), $http.get('api/senders?q=active')]
        .then (results) ->
          $scope.groups = _.indexBy results[0].data, 'id'
          $scope.senders = results[1].data
          $scope.emails = _.groupBy _.pluck($scope.senders, 'email'), $scope.senders.email

      $http.get('api/account/settings')
      .success (settings) ->
        $scope.settings = settings
      .error (err) ->
        growl.danger 'Failed to load account settings. ' + err

      $scope.hasPhones = (sender) -> Object.keys(sender.phones).length > 0

      $scope.sortby = (value, header) ->
        $scope.sort.desc = if $scope.sort.header is header then !$scope.sort.desc else no
        $scope.sort.header = header

        $scope.sort.column = if value is 'group'
          (sender) -> $scope.groups[sender.group].name
        else if value is 'name'
          (sender) -> [sender.lastname, sender.firstname]
        else
          value

      $scope.filter = (item) ->
        new RegExp($scope.search.name, 'i').test [
          item.firstname
          item.lastname
          item.email
          $scope.groups[item.group]?.name or ''
        ].concat(_.values item.phones).join '|'

      $scope.create = ->
        inst = $uibModal.open
          templateUrl: 'sender-modal.html'
          controller: 'SenderModal'
          size: 'lg'
          backdrop: false
          resolve:
            groups: -> $scope.groups
            sender: -> phones: {}, group: _.keys($scope.groups)?[0]
            emails: -> $scope.emails

        inst.result.then (sender) ->
          # add new sender to list
          $scope.senders.push(sender) if sender.id
          $scope.system.senders = $scope.senders.length

          # pull updated settings
          $http.get('api/account/settings').success (settings) ->
            $scope.settings = settings

      $scope.edit = (sender) ->
        ref = sender
        inst = $uibModal.open
          templateUrl: 'sender-modal.html'
          controller: 'SenderModal'
          size: 'lg'
          backdrop: false
          resolve:
            groups: -> $scope.groups
            sender: -> angular.copy sender
            emails: -> $scope.emails

        inst.result.then (sender) ->
          # find location of sender in list, and update, otherwise give error
          senderLocation = _.findIndex $scope.senders, id: sender.id
          if senderLocation is -1
            growl.danger 'Senders list failed to update. Please refresh page.'
          else
            $scope.senders[senderLocation] = sender

          # pull updated settings
          $http.get('api/account/settings').success (settings) ->
            $scope.settings = settings

      $scope.editInactive = (sender) ->
        ref = sender
        inst = $uibModal.open
          templateUrl: 'sender-modal.html'
          controller: 'SenderModal'
          size: 'lg'
          backdrop: false
          resolve:
            groups: -> $scope.groups
            sender: -> angular.copy sender
            emails: -> $scope.emails

        inst.result.then (sender) ->
          # find location of sender in list, and update, otherwise give error
          senderLocation = _.findIndex $scope.inactiveSenders, id: sender.id
          if senderLocation is -1
            growl.danger 'Senders list failed to update. Please refresh page.'
          else
            $scope.inactiveSenders[senderLocation] = sender

          # pull updated settings
            $http.get('api/account/settings')
            .success (settings) ->
              $scope.settings = settings
            .error (err) ->
              growl.danger 'Failed to load account settings. ' + err

      $scope.showInactive = ->
        $http.get('api/senders').success (results) ->
          $scope.inactiveSenders = []
          for result in results
            if result.active is false
              $scope.inactiveSenders.push result
        $http.get('api/senders?q=active')
        .success (senders) ->
          $scope.senders = senders
        .error (err) ->
          growl.danger 'Failed to load senders. ' + err

      $scope.delete = (sender) ->
        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'
          scope: _.assign $scope.$new(), name: sender.email

        inst.result.then ->
          $http.delete('api/senders/' + sender.id)
          .success ->
            $scope.senders = _.reject $scope.senders, (s) -> s.id is sender.id
            $scope.system.senders = $scope.senders.length
            $scope.showInactive()
          .error (err) ->
            growl.danger err

      $scope.editProfiles = ->
        inst = $uibModal.open
          templateUrl: 'profile-modal.html'
          controller: 'ProfileModal'
          resolve: url: -> 'api/profiles/settings'

      $scope.makeActive = (sender) ->
        inst = $uibModal.open
          templateUrl: 'reactivate-modal.html'
          controller: 'ConfirmModal'
          backdrop: 'static'
          scope: _.assign $scope.$new(), name: sender.email

        inst.result.then ->
          sender.active = true
          $http.put('api/senders/' + sender.id, sender)
          .success ->
            angular.copy sender
            $scope.inactiveSenders = _.reject $scope.inactiveSenders, (s) -> s.id is sender.id
            $scope.showInactive()
          .error (err) ->
            growl.danger 'Failed to save sender. ' + err

      $scope.upload = ->
        inst = $uibModal.open
          templateUrl: 'upload-modal.html'
          controller: 'SenderUploadModal'

        inst.result.then ->
          $scope.showInactive()
          $http.get('api/account/settings')
          .success (settings) ->
            $scope.settings = settings
          .error (err) ->
            growl.danger 'Failed to load settings. ' + err
    ]

  .controller 'SenderModal',
    ['$scope', '$http', '$uibModal', '$uibModalInstance', '$timeout',
    'sender', 'emails', 'groups', 'dismissCheck', 'saveTimeout', 'errorTimeout',
    ($scope, $http, $uibModal, $uibModalInstance, $timeout,
    sender, emails, groups, dismissCheck, saveTimeout, errorTimeout) ->
      $scope.sender = sender
      $scope.sender.custom ?= {}
      $scope.sender.phones ?= {}
      $scope.sender.dailyvu ?= on: $scope.system.dailyvu.on, time: $scope.system.dailyvu.time or '0800'
      $scope.groups = groups
      # for the select input box for angular, all items need to be an object that has a 'text' property
      $scope.aliases = _.compact _.map ($scope.sender.aliases or []), (alias) ->
        if alias != (sender.firstname + " " + sender.lastname) and alias != (sender.lastname + ", " + sender.firstname)
          text: alias
      $scope.temp = {}
      $scope.forms = {}
      sender_ref = {}
      angular.copy $scope.sender, sender_ref
      $scope.emails = emails
      $scope.used = no

      $http.get('api/account/settings')
      .success (settings) ->
        $scope.settings = settings
        # if/else for if the selected sender is the default sender. set temp and default_sender (button clicked or not)
        if $scope.settings.default_sender is sender.email
          $scope.default_sender = true
          $scope.temp.default_sender = sender.email
        else
          $scope.default_sender = false
          $scope.temp.default_sender = $scope.settings.default_sender
      .error (err) ->
        growl.danger 'Failed to load account settings. ' + err

      $http.get('api/profiles/settings')
      .success (fields) ->
        $scope.fields = fields
      .error (err) ->
        growl.danger 'Failed to load profile settings. ' + err

      $scope.tagRemoved = ->
        $scope.forms.sender.$setDirty()

      $scope.worequired = (item) ->
        not _.any $scope.fields, (field) -> field.name is item.name

      $scope.delete = (arr, item) ->
        arr.splice arr.indexOf(item), 1
        $scope.forms.sender.$setDirty()

      $scope.override = ->
        delete $scope.sender.signature unless $scope.sender.override
        $scope.forms.sender.$setDirty()

      # modal to confirm default sender change.
      $scope.defaultCheck = (email) ->
        $scope.default_sender = false
        inst = $uibModal.open
          templateUrl: 'changeDefault-modal.html'
          controller: 'ConfirmModal'
          backdrop: 'static'
          scope: _.assign $scope.$new(), name: $scope.sender.email, default: $scope.settings.default_sender

        inst.result.then ->
          $scope.default_sender = true
          $scope.settings.default_sender = email

      $scope.defaultSend = (email) ->
        # if button has been selected then set default to senders email
        if $scope.default_sender
          # calls defaultCheck to see if default is already set
          if $scope.settings.default_sender?
            $scope.defaultCheck email
          else
            $scope.settings.default_sender = email
        # if button has been deselected, and the current default doesn't match the temp default
        # then set current default to temp default
        else if !$scope.default_sender and $scope.settings.default_sender isnt $scope.temp.default_sender
          $scope.settings.default_sender = $scope.temp.default_sender
        # else button has been deselected and current does match the temp so set current default to empty string
        else
          $scope.settings.default_sender = null

      $scope.pretty = (tz) ->
        switch tz
          when 'America/Los_Angeles' then '(GMT-08:00) Pacific Time (US & Canada)'
          when 'America/Phoenix' then '(GMT-07:00) Arizona'
          when 'America/Denver' then '(GMT-07:00) Mountain Time (US & Canada)'
          when 'America/Chicago' then '(GMT-06:00) Central Time (US & Canada)'
          when 'America/New_York' then '(GMT-05:00) Eastern Time (US & Canada)'

      # submit new sender to db
      httpPost = (sender, start, cb) ->
        $http.post('/api/senders', sender)
        .success (data) ->
          $scope.sender.id = data.id
          $scope.sender.version = data.version
          angular.copy $scope.sender, sender_ref
          cb null, sender_ref
        .error (err) ->
          $scope.errorMessage = err
          saveTimeout.begin start, $scope.forms.sender
          cb err

      # submit sender edits to db
      httpPut = (sender, start, cb) ->
        $http.put('/api/senders/' + sender.id, sender)
        .success (data) ->
          $scope.sender.version = data.version
          angular.copy $scope.sender, sender_ref
          cb null, sender_ref
        .error (err) ->
          $scope.errorMessage = err
          saveTimeout.begin start, $scope.forms.sender
          cb err

      # submit account settings edits (default sender) to db
      saveDefaultSender = (start) ->
        $http.put('api/account/settings', default_sender: $scope.settings.default_sender)
        .success ->
          saveTimeout.begin start, $scope.forms.sender
        .error (err) ->
          $scope.errorMessage = err
          saveTimeout.begin start, $scope.forms.sender

      # check to see if the user inputed email has already been used. only for creating new senders
      checkForUsed = (sender) ->
        if $scope.emails[sender.email]? and not sender.id?
          $scope.used = yes
        else
          $scope.used = no

      checkId = (sender, newSender, cb) ->
        if !sender.id
          newSender = yes
        cb sender, newSender

      $scope.save = (cb) ->
        start = Date.now()
        newSender = no
        $scope.errorMessage = undefined
        $scope.formErrors = undefined
        $scope.forms.sender.saved = no
        $scope.forms.sender.saving = yes
        $scope.forms.sender.submitted = no
        delete $scope.sender.timezone if $scope.sender.timezone is ''

        if $scope.forms.sender.$invalid
          if $scope.forms.sender.email?.$invalid
            $scope.formErrors = 'Please fill out all required fields and/or enter a valid email'
          errorTimeout.begin $scope.forms.sender, start
        else
          checkForUsed $scope.sender
          if $scope.used
            errorTimeout.begin $scope.forms.sender, start
          else
            checkId $scope.sender, newSender, (sender, isNew) ->
              $scope.sender = sender
              $scope.emails[sender.email] = yes
              # the angular select input box stores the items as objects with 'text' as the typed in alias
              $scope.sender.aliases = _.pluck $scope.aliases, 'text'
              if isNew
                httpPost $scope.sender, start, (err, ref) ->
                  sender_ref = ref
                  saveDefaultSender()
                  if cb? then cb err, sender_ref
              else
                httpPut $scope.sender, start, (err, ref) ->
                  sender_ref = ref
                  saveDefaultSender()
                  if cb? then cb err, sender_ref


      $scope.close = ->
        if $scope.forms.sender.saved and $scope.forms.sender.$pristine
          $uibModalInstance.close $scope.sender
        else if $scope.forms.sender.$dirty or $scope.formErrors or not _.isEqual $scope.sender.aliases, $scope.aliases
          dismissCheck.dismiss $uibModalInstance, item: $scope.sender, ref: sender_ref, save: $scope.save
        else
          $uibModalInstance.dismiss()
    ]

  .controller 'SenderUploadModal',
    ['$scope', '$http', '$uibModalInstance', '$uibModal',
    ($scope, $http, $uibModalInstance, $uibModal) ->
      $scope.forms = upload: null

      $scope.$watch 'forms.upload.csv', (csv) ->
        return unless csv
        $scope.forms.upload.$setDirty()
        $scope.typeerror = !~csv.name.indexOf '.csv'

      $scope.url = ->
        if $scope.senderOptions is 'includeSenders'
          'api/senders/download'
        else if $scope.senderOptions is 'includeActiveSenders'
          'api/senders/active/download'
        else
          'profiles/template/download'

      $scope.upload = ->
        return $scope.typeerror = yes unless ~$scope.forms.upload.csv.name.indexOf '.csv'

        fd = new FormData
        fd.append 'file', $scope.forms.upload.csv

        $scope.$saving = yes
        $http.post('api/senders/upload', fd,
          transformRequest: angular.identity
          headers: 'Content-Type': undefined
        ).success (stats) ->
          $scope.$saving = no
          $uibModalInstance.close()

          inst = $uibModal.open
            templateUrl: 'stats-modal.html'
            backdrop: 'static'
            controller: ['$scope', '$uibModalInstance', ($scope, $uibModalInstance) ->
              $scope.stats = stats
              $scope.showInactive = ->
                $http.get('api/senders').success (results) ->
                  $scope.inactiveSenders = []
                  for result in results
                    if result.active is false
                      $scope.inactiveSenders.push result
                $http.get('api/senders?q=active').success (senders) ->
                  $scope.senders = senders

              $scope.showInactive()
              $scope.close = -> $uibModalInstance.close()
            ]
    ]
