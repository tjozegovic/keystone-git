(function() {
  angular.module('app.admin').controller('AdminSenders', [
    '$scope', '$http', '$uibModal', '$q', 'growl', function($scope, $http, $uibModal, $q, growl) {
      $scope.search = {};
      $scope.sort = {
        header: 'Email',
        column: 'email',
        desc: false
      };
      $q.all([$http.get('api/groups?q=active'), $http.get('api/senders?q=active')]).then(function(results) {
        $scope.groups = _.indexBy(results[0].data, 'id');
        $scope.senders = results[1].data;
        return $scope.emails = _.groupBy(_.pluck($scope.senders, 'email'), $scope.senders.email);
      });
      $http.get('api/account/settings').success(function(settings) {
        return $scope.settings = settings;
      }).error(function(err) {
        return growl.danger('Failed to load account settings. ' + err);
      });
      $scope.hasPhones = function(sender) {
        return Object.keys(sender.phones).length > 0;
      };
      $scope.sortby = function(value, header) {
        $scope.sort.desc = $scope.sort.header === header ? !$scope.sort.desc : false;
        $scope.sort.header = header;
        return $scope.sort.column = value === 'group' ? function(sender) {
          return $scope.groups[sender.group].name;
        } : value === 'name' ? function(sender) {
          return [sender.lastname, sender.firstname];
        } : value;
      };
      $scope.filter = function(item) {
        var ref1;
        return new RegExp($scope.search.name, 'i').test([item.firstname, item.lastname, item.email, ((ref1 = $scope.groups[item.group]) != null ? ref1.name : void 0) || ''].concat(_.values(item.phones)).join('|'));
      };
      $scope.create = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'sender-modal.html',
          controller: 'SenderModal',
          size: 'lg',
          backdrop: false,
          resolve: {
            groups: function() {
              return $scope.groups;
            },
            sender: function() {
              var ref1;
              return {
                phones: {},
                group: (ref1 = _.keys($scope.groups)) != null ? ref1[0] : void 0
              };
            },
            emails: function() {
              return $scope.emails;
            }
          }
        });
        return inst.result.then(function(sender) {
          if (sender.id) {
            $scope.senders.push(sender);
          }
          $scope.system.senders = $scope.senders.length;
          return $http.get('api/account/settings').success(function(settings) {
            return $scope.settings = settings;
          });
        });
      };
      $scope.edit = function(sender) {
        var inst, ref;
        ref = sender;
        inst = $uibModal.open({
          templateUrl: 'sender-modal.html',
          controller: 'SenderModal',
          size: 'lg',
          backdrop: false,
          resolve: {
            groups: function() {
              return $scope.groups;
            },
            sender: function() {
              return angular.copy(sender);
            },
            emails: function() {
              return $scope.emails;
            }
          }
        });
        return inst.result.then(function(sender) {
          var senderLocation;
          senderLocation = _.findIndex($scope.senders, {
            id: sender.id
          });
          if (senderLocation === -1) {
            growl.danger('Senders list failed to update. Please refresh page.');
          } else {
            $scope.senders[senderLocation] = sender;
          }
          return $http.get('api/account/settings').success(function(settings) {
            return $scope.settings = settings;
          });
        });
      };
      $scope.editInactive = function(sender) {
        var inst, ref;
        ref = sender;
        inst = $uibModal.open({
          templateUrl: 'sender-modal.html',
          controller: 'SenderModal',
          size: 'lg',
          backdrop: false,
          resolve: {
            groups: function() {
              return $scope.groups;
            },
            sender: function() {
              return angular.copy(sender);
            },
            emails: function() {
              return $scope.emails;
            }
          }
        });
        return inst.result.then(function(sender) {
          var senderLocation;
          senderLocation = _.findIndex($scope.inactiveSenders, {
            id: sender.id
          });
          if (senderLocation === -1) {
            return growl.danger('Senders list failed to update. Please refresh page.');
          } else {
            $scope.inactiveSenders[senderLocation] = sender;
            return $http.get('api/account/settings').success(function(settings) {
              return $scope.settings = settings;
            }).error(function(err) {
              return growl.danger('Failed to load account settings. ' + err);
            });
          }
        });
      };
      $scope.showInactive = function() {
        $http.get('api/senders').success(function(results) {
          var i, len, result, results1;
          $scope.inactiveSenders = [];
          results1 = [];
          for (i = 0, len = results.length; i < len; i++) {
            result = results[i];
            if (result.active === false) {
              results1.push($scope.inactiveSenders.push(result));
            } else {
              results1.push(void 0);
            }
          }
          return results1;
        });
        return $http.get('api/senders?q=active').success(function(senders) {
          return $scope.senders = senders;
        }).error(function(err) {
          return growl.danger('Failed to load senders. ' + err);
        });
      };
      $scope["delete"] = function(sender) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal',
          scope: _.assign($scope.$new(), {
            name: sender.email
          })
        });
        return inst.result.then(function() {
          return $http["delete"]('api/senders/' + sender.id).success(function() {
            $scope.senders = _.reject($scope.senders, function(s) {
              return s.id === sender.id;
            });
            $scope.system.senders = $scope.senders.length;
            return $scope.showInactive();
          }).error(function(err) {
            return growl.danger(err);
          });
        });
      };
      $scope.editProfiles = function() {
        var inst;
        return inst = $uibModal.open({
          templateUrl: 'profile-modal.html',
          controller: 'ProfileModal',
          resolve: {
            url: function() {
              return 'api/profiles/settings';
            }
          }
        });
      };
      $scope.makeActive = function(sender) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'reactivate-modal.html',
          controller: 'ConfirmModal',
          backdrop: 'static',
          scope: _.assign($scope.$new(), {
            name: sender.email
          })
        });
        return inst.result.then(function() {
          sender.active = true;
          return $http.put('api/senders/' + sender.id, sender).success(function() {
            angular.copy(sender);
            $scope.inactiveSenders = _.reject($scope.inactiveSenders, function(s) {
              return s.id === sender.id;
            });
            return $scope.showInactive();
          }).error(function(err) {
            return growl.danger('Failed to save sender. ' + err);
          });
        });
      };
      return $scope.upload = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'upload-modal.html',
          controller: 'SenderUploadModal'
        });
        return inst.result.then(function() {
          $scope.showInactive();
          return $http.get('api/account/settings').success(function(settings) {
            return $scope.settings = settings;
          }).error(function(err) {
            return growl.danger('Failed to load settings. ' + err);
          });
        });
      };
    }
  ]).controller('SenderModal', [
    '$scope', '$http', '$uibModal', '$uibModalInstance', '$timeout', 'sender', 'emails', 'groups', 'dismissCheck', 'saveTimeout', 'errorTimeout', function($scope, $http, $uibModal, $uibModalInstance, $timeout, sender, emails, groups, dismissCheck, saveTimeout, errorTimeout) {
      var base, base1, base2, checkForUsed, checkId, httpPost, httpPut, saveDefaultSender, sender_ref;
      $scope.sender = sender;
      if ((base = $scope.sender).custom == null) {
        base.custom = {};
      }
      if ((base1 = $scope.sender).phones == null) {
        base1.phones = {};
      }
      if ((base2 = $scope.sender).dailyvu == null) {
        base2.dailyvu = {
          on: $scope.system.dailyvu.on,
          time: $scope.system.dailyvu.time || '0800'
        };
      }
      $scope.groups = groups;
      $scope.aliases = _.compact(_.map($scope.sender.aliases || [], function(alias) {
        if (alias !== (sender.firstname + " " + sender.lastname) && alias !== (sender.lastname + ", " + sender.firstname)) {
          return {
            text: alias
          };
        }
      }));
      $scope.temp = {};
      $scope.forms = {};
      sender_ref = {};
      angular.copy($scope.sender, sender_ref);
      $scope.emails = emails;
      $scope.used = false;
      $http.get('api/account/settings').success(function(settings) {
        $scope.settings = settings;
        if ($scope.settings.default_sender === sender.email) {
          $scope.default_sender = true;
          return $scope.temp.default_sender = sender.email;
        } else {
          $scope.default_sender = false;
          return $scope.temp.default_sender = $scope.settings.default_sender;
        }
      }).error(function(err) {
        return growl.danger('Failed to load account settings. ' + err);
      });
      $http.get('api/profiles/settings').success(function(fields) {
        return $scope.fields = fields;
      }).error(function(err) {
        return growl.danger('Failed to load profile settings. ' + err);
      });
      $scope.tagRemoved = function() {
        return $scope.forms.sender.$setDirty();
      };
      $scope.worequired = function(item) {
        return !_.any($scope.fields, function(field) {
          return field.name === item.name;
        });
      };
      $scope["delete"] = function(arr, item) {
        arr.splice(arr.indexOf(item), 1);
        return $scope.forms.sender.$setDirty();
      };
      $scope.override = function() {
        if (!$scope.sender.override) {
          delete $scope.sender.signature;
        }
        return $scope.forms.sender.$setDirty();
      };
      $scope.defaultCheck = function(email) {
        var inst;
        $scope.default_sender = false;
        inst = $uibModal.open({
          templateUrl: 'changeDefault-modal.html',
          controller: 'ConfirmModal',
          backdrop: 'static',
          scope: _.assign($scope.$new(), {
            name: $scope.sender.email,
            "default": $scope.settings.default_sender
          })
        });
        return inst.result.then(function() {
          $scope.default_sender = true;
          return $scope.settings.default_sender = email;
        });
      };
      $scope.defaultSend = function(email) {
        if ($scope.default_sender) {
          if ($scope.settings.default_sender != null) {
            return $scope.defaultCheck(email);
          } else {
            return $scope.settings.default_sender = email;
          }
        } else if (!$scope.default_sender && $scope.settings.default_sender !== $scope.temp.default_sender) {
          return $scope.settings.default_sender = $scope.temp.default_sender;
        } else {
          return $scope.settings.default_sender = null;
        }
      };
      $scope.pretty = function(tz) {
        switch (tz) {
          case 'America/Los_Angeles':
            return '(GMT-08:00) Pacific Time (US & Canada)';
          case 'America/Phoenix':
            return '(GMT-07:00) Arizona';
          case 'America/Denver':
            return '(GMT-07:00) Mountain Time (US & Canada)';
          case 'America/Chicago':
            return '(GMT-06:00) Central Time (US & Canada)';
          case 'America/New_York':
            return '(GMT-05:00) Eastern Time (US & Canada)';
        }
      };
      httpPost = function(sender, start, cb) {
        return $http.post('/api/senders', sender).success(function(data) {
          $scope.sender.id = data.id;
          $scope.sender.version = data.version;
          angular.copy($scope.sender, sender_ref);
          return cb(null, sender_ref);
        }).error(function(err) {
          $scope.errorMessage = err;
          saveTimeout.begin(start, $scope.forms.sender);
          return cb(err);
        });
      };
      httpPut = function(sender, start, cb) {
        return $http.put('/api/senders/' + sender.id, sender).success(function(data) {
          $scope.sender.version = data.version;
          angular.copy($scope.sender, sender_ref);
          return cb(null, sender_ref);
        }).error(function(err) {
          $scope.errorMessage = err;
          saveTimeout.begin(start, $scope.forms.sender);
          return cb(err);
        });
      };
      saveDefaultSender = function(start) {
        return $http.put('api/account/settings', {
          default_sender: $scope.settings.default_sender
        }).success(function() {
          return saveTimeout.begin(start, $scope.forms.sender);
        }).error(function(err) {
          $scope.errorMessage = err;
          return saveTimeout.begin(start, $scope.forms.sender);
        });
      };
      checkForUsed = function(sender) {
        if (($scope.emails[sender.email] != null) && (sender.id == null)) {
          return $scope.used = true;
        } else {
          return $scope.used = false;
        }
      };
      checkId = function(sender, newSender, cb) {
        if (!sender.id) {
          newSender = true;
        }
        return cb(sender, newSender);
      };
      $scope.save = function(cb) {
        var newSender, ref1, start;
        start = Date.now();
        newSender = false;
        $scope.errorMessage = void 0;
        $scope.formErrors = void 0;
        $scope.forms.sender.saved = false;
        $scope.forms.sender.saving = true;
        $scope.forms.sender.submitted = false;
        if ($scope.sender.timezone === '') {
          delete $scope.sender.timezone;
        }
        if ($scope.forms.sender.$invalid) {
          if ((ref1 = $scope.forms.sender.email) != null ? ref1.$invalid : void 0) {
            $scope.formErrors = 'Please fill out all required fields and/or enter a valid email';
          }
          return errorTimeout.begin($scope.forms.sender, start);
        } else {
          checkForUsed($scope.sender);
          if ($scope.used) {
            return errorTimeout.begin($scope.forms.sender, start);
          } else {
            return checkId($scope.sender, newSender, function(sender, isNew) {
              $scope.sender = sender;
              $scope.emails[sender.email] = true;
              $scope.sender.aliases = _.pluck($scope.aliases, 'text');
              if (isNew) {
                return httpPost($scope.sender, start, function(err, ref) {
                  sender_ref = ref;
                  saveDefaultSender();
                  if (cb != null) {
                    return cb(err, sender_ref);
                  }
                });
              } else {
                return httpPut($scope.sender, start, function(err, ref) {
                  sender_ref = ref;
                  saveDefaultSender();
                  if (cb != null) {
                    return cb(err, sender_ref);
                  }
                });
              }
            });
          }
        }
      };
      return $scope.close = function() {
        if ($scope.forms.sender.saved && $scope.forms.sender.$pristine) {
          return $uibModalInstance.close($scope.sender);
        } else if ($scope.forms.sender.$dirty || $scope.formErrors || !_.isEqual($scope.sender.aliases, $scope.aliases)) {
          return dismissCheck.dismiss($uibModalInstance, {
            item: $scope.sender,
            ref: sender_ref,
            save: $scope.save
          });
        } else {
          return $uibModalInstance.dismiss();
        }
      };
    }
  ]).controller('SenderUploadModal', [
    '$scope', '$http', '$uibModalInstance', '$uibModal', function($scope, $http, $uibModalInstance, $uibModal) {
      $scope.forms = {
        upload: null
      };
      $scope.$watch('forms.upload.csv', function(csv) {
        if (!csv) {
          return;
        }
        $scope.forms.upload.$setDirty();
        return $scope.typeerror = !~csv.name.indexOf('.csv');
      });
      $scope.url = function() {
        if ($scope.senderOptions === 'includeSenders') {
          return 'api/senders/download';
        } else if ($scope.senderOptions === 'includeActiveSenders') {
          return 'api/senders/active/download';
        } else {
          return 'profiles/template/download';
        }
      };
      return $scope.upload = function() {
        var fd;
        if (!~$scope.forms.upload.csv.name.indexOf('.csv')) {
          return $scope.typeerror = true;
        }
        fd = new FormData;
        fd.append('file', $scope.forms.upload.csv);
        $scope.$saving = true;
        return $http.post('api/senders/upload', fd, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': void 0
          }
        }).success(function(stats) {
          var inst;
          $scope.$saving = false;
          $uibModalInstance.close();
          return inst = $uibModal.open({
            templateUrl: 'stats-modal.html',
            backdrop: 'static',
            controller: [
              '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
                $scope.stats = stats;
                $scope.showInactive = function() {
                  $http.get('api/senders').success(function(results) {
                    var i, len, result, results1;
                    $scope.inactiveSenders = [];
                    results1 = [];
                    for (i = 0, len = results.length; i < len; i++) {
                      result = results[i];
                      if (result.active === false) {
                        results1.push($scope.inactiveSenders.push(result));
                      } else {
                        results1.push(void 0);
                      }
                    }
                    return results1;
                  });
                  return $http.get('api/senders?q=active').success(function(senders) {
                    return $scope.senders = senders;
                  });
                };
                $scope.showInactive();
                return $scope.close = function() {
                  return $uibModalInstance.close();
                };
              }
            ]
          });
        });
      };
    }
  ]);

}).call(this);
