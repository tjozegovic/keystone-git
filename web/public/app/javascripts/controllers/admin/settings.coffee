angular.module('app.admin')
  .controller 'AdminTemplates', [
    '$scope', '$http', '$interval', '$uibModal', '$q', '$timeout', 'growl',
    ($scope, $http, $interval, $uibModal, $q, $timeout, growl) ->
      $scope.forms = {}
      $scope.stopped = no
      $scope.stopping = no
      $scope.running = no
      $scope.runningImport  = undefined

      $scope.delayoptions = [
        { t: 'None', v: 0 }
        { t: '12', v: 12 }
        { t: '24', v: 24 }
        { t: '48', v: 48 }
      ]

      $http.get('api/account/settings').success (settings) ->
        $scope.settings = settings
        $scope.privacy_url = settings.privacy_url
        $scope.settings.dailyvu ?= on: no, time: '0800'
        $scope.settings.notifications.delay ?= 24
        $scope.settings.timezone = settings.timezone or jstz.determine().name()

      $http.get('api/account/template').success (template) ->
        $scope.template = template

      $http.get('api/leads/imports')
      .error (err) ->
        growl.danger 'Error loading import data ' , err
      .success (imports) ->
        $scope.imports = imports

      # function that starts running interval. interval checks running / finished
      $scope.startRunning = ->
        $scope.running = yes
        $scope.stopped = no
        $scope.stopping = no
        $scope.runningImport = $scope.imports[0]
        $scope.checkRunning = $interval ->
          if $scope.running and not $scope.runningImport.finished?
            $http.get('api/leads/imports')
            .error (err) ->
              growl.danger 'Error refreshing import data ' , err
            .success (imports) ->
              $scope.imports = imports
              $scope.runningImport = $scope.imports[0]
          else if $scope.runningImport.finished?
            $scope.stopRunning()
        , 1500

      $scope.stop = (_import) ->
        return unless _import.$job
        $interval.cancel $scope.checkRunning
        $scope.stopping = yes
        $http.post('api/import/cancel/' + _import.$job)
        .success ->
          $scope.running = no
          $scope.startStopping _import
        .error (err) ->
          growl.danger 'error: ', err

      # function that starts stopping interval. interval checks to see if job still exists
      $scope.startStopping = (_import) ->
        $scope.checkStopping = $interval ->
          if _import.$job
            $http.get('api/leads/imports')
            .error (err) ->
              growl.danger 'Error refreshing import data ' , err
            .success (imports) ->
              $scope.imports = imports
              _import = _.where imports, id: _import.id
          else
            $scope.stopRunning()
        , 3000

      $scope.stopRunning = ->
        $scope.running = no
        $scope.runningImport = undefined
        $interval.cancel $scope.checkRunning

      $scope.stopStopping = ->
        $scope.stopping = no
        $scope.stopped = yes
        $interval.cancel $scope.checkStopping

      $scope.save = ->
        start = Date.now()
        $scope.$saving = yes
        $http.put('api/account/settings', $scope.settings).success ->
          ##This is setting information on the $rootScope
          $scope.setSystem _.pick $scope.settings, 'dailyvu', 'notifications', 'timezone'
          $timeout ->
            $scope.forms.notifications.$setPristine()
            $scope.$submitted = yes # in later versions of angular, we have $setSubmitted() on form
            $scope.$saving = no
          , Math.max 0, 600 - (start - Date.now())

      $scope.saveTemplate = (template = $scope.template) ->
        $.post('api/account/template', template).success (data) ->
          $timeout (-> $scope.forms.template.$setPristine()), 1000

      $scope.import = ->
        inst = $uibModal.open
          templateUrl: 'admin/bulk-import-modal.html'
          backdrop: 'static'
          controller: ['$scope', '$uibModalInstance', ($scope, $uibModalInstance) ->
            $scope.forms.import = {}

            $scope.$watch 'forms.import.csv', (csv) ->
              return unless csv
              $scope.forms.import.$setDirty()
              $scope.typeerror = !~csv.name.indexOf '.csv'

            $scope.import = ->
              return $scope.typeerror = yes unless ~$scope.forms.import.csv.name.indexOf '.csv'

              fd = new FormData
              fd.append 'file', $scope.forms.import.csv

              $scope.$saving = yes
              $http.post('api/leads/import', fd,
                transformRequest: angular.identity
                headers: 'Content-Type': undefined
              ).success ->
                $scope.$saving = no
                $uibModalInstance.close()
          ]

        inst.result.then ->
          start = Date.now()
          $timeout ->
            $http.get('api/leads/imports').error (err) ->
              growl.danger 'Error loading import data ' , err
            .success (imports) ->
              $scope.imports = imports
              # start running interval
              $scope.startRunning()
          , Math.max 0, 600 - (start - Date.now())

  ]
