(function() {
  angular.module('app.admin').controller('AdminTemplates', [
    '$scope', '$http', '$interval', '$uibModal', '$q', '$timeout', 'growl', function($scope, $http, $interval, $uibModal, $q, $timeout, growl) {
      $scope.forms = {};
      $scope.stopped = false;
      $scope.stopping = false;
      $scope.running = false;
      $scope.runningImport = void 0;
      $scope.delayoptions = [
        {
          t: 'None',
          v: 0
        }, {
          t: '12',
          v: 12
        }, {
          t: '24',
          v: 24
        }, {
          t: '48',
          v: 48
        }
      ];
      $http.get('api/account/settings').success(function(settings) {
        var base, base1;
        $scope.settings = settings;
        $scope.privacy_url = settings.privacy_url;
        if ((base = $scope.settings).dailyvu == null) {
          base.dailyvu = {
            on: false,
            time: '0800'
          };
        }
        if ((base1 = $scope.settings.notifications).delay == null) {
          base1.delay = 24;
        }
        return $scope.settings.timezone = settings.timezone || jstz.determine().name();
      });
      $http.get('api/account/template').success(function(template) {
        return $scope.template = template;
      });
      $http.get('api/leads/imports').error(function(err) {
        return growl.danger('Error loading import data ', err);
      }).success(function(imports) {
        return $scope.imports = imports;
      });
      $scope.startRunning = function() {
        $scope.running = true;
        $scope.stopped = false;
        $scope.stopping = false;
        $scope.runningImport = $scope.imports[0];
        return $scope.checkRunning = $interval(function() {
          if ($scope.running && ($scope.runningImport.finished == null)) {
            return $http.get('api/leads/imports').error(function(err) {
              return growl.danger('Error refreshing import data ', err);
            }).success(function(imports) {
              $scope.imports = imports;
              return $scope.runningImport = $scope.imports[0];
            });
          } else if ($scope.runningImport.finished != null) {
            return $scope.stopRunning();
          }
        }, 1500);
      };
      $scope.stop = function(_import) {
        if (!_import.$job) {
          return;
        }
        $interval.cancel($scope.checkRunning);
        $scope.stopping = true;
        return $http.post('api/import/cancel/' + _import.$job).success(function() {
          $scope.running = false;
          return $scope.startStopping(_import);
        }).error(function(err) {
          return growl.danger('error: ', err);
        });
      };
      $scope.startStopping = function(_import) {
        return $scope.checkStopping = $interval(function() {
          if (_import.$job) {
            return $http.get('api/leads/imports').error(function(err) {
              return growl.danger('Error refreshing import data ', err);
            }).success(function(imports) {
              $scope.imports = imports;
              return _import = _.where(imports, {
                id: _import.id
              });
            });
          } else {
            return $scope.stopRunning();
          }
        }, 3000);
      };
      $scope.stopRunning = function() {
        $scope.running = false;
        $scope.runningImport = void 0;
        return $interval.cancel($scope.checkRunning);
      };
      $scope.stopStopping = function() {
        $scope.stopping = false;
        $scope.stopped = true;
        return $interval.cancel($scope.checkStopping);
      };
      $scope.save = function() {
        var start;
        start = Date.now();
        $scope.$saving = true;
        return $http.put('api/account/settings', $scope.settings).success(function() {
          $scope.setSystem(_.pick($scope.settings, 'dailyvu', 'notifications', 'timezone'));
          return $timeout(function() {
            $scope.forms.notifications.$setPristine();
            $scope.$submitted = true;
            return $scope.$saving = false;
          }, Math.max(0, 600 - (start - Date.now())));
        });
      };
      $scope.saveTemplate = function(template) {
        if (template == null) {
          template = $scope.template;
        }
        return $.post('api/account/template', template).success(function(data) {
          return $timeout((function() {
            return $scope.forms.template.$setPristine();
          }), 1000);
        });
      };
      return $scope["import"] = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'admin/bulk-import-modal.html',
          backdrop: 'static',
          controller: [
            '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
              $scope.forms["import"] = {};
              $scope.$watch('forms.import.csv', function(csv) {
                if (!csv) {
                  return;
                }
                $scope.forms["import"].$setDirty();
                return $scope.typeerror = !~csv.name.indexOf('.csv');
              });
              return $scope["import"] = function() {
                var fd;
                if (!~$scope.forms["import"].csv.name.indexOf('.csv')) {
                  return $scope.typeerror = true;
                }
                fd = new FormData;
                fd.append('file', $scope.forms["import"].csv);
                $scope.$saving = true;
                return $http.post('api/leads/import', fd, {
                  transformRequest: angular.identity,
                  headers: {
                    'Content-Type': void 0
                  }
                }).success(function() {
                  $scope.$saving = false;
                  return $uibModalInstance.close();
                });
              };
            }
          ]
        });
        return inst.result.then(function() {
          var start;
          start = Date.now();
          return $timeout(function() {
            return $http.get('api/leads/imports').error(function(err) {
              return growl.danger('Error loading import data ', err);
            }).success(function(imports) {
              $scope.imports = imports;
              return $scope.startRunning();
            });
          }, Math.max(0, 600 - (start - Date.now())));
        });
      };
    }
  ]);

}).call(this);
