angular.module('app.admin')
  .controller 'AdminShortform',
    ['$scope', '$http', '$timeout', 'growl', 'saveTimeout', 'errorTimeout',
    ($scope, $http, $timeout, growl, saveTimeout, errorTimeout) ->
      $scope.shortform = status: '', agent: '', attributes: {}
      $scope.forms = {}

      async.map ['api/attributes?q=active', 'api/statuses?q=active', 'api/senders?q=active'],
        (url, cb) ->
          $http.get(url)
            .success (data) -> cb null, data
            .error (err) -> cb err
        (err, data) ->
          $scope.statuses = data[1]
          $scope.attributes = data[0]
          $scope.senders = data[2]

          $scope.shortform.status = _.first(_.values($scope.statuses)).name
          $scope.shortform.agent = $scope.senders[0].email
          $scope.shortform.attributes = _.transform $scope.attributes, (memo, group) ->
            group.short = group.name.replace(/\s*/g, '').toLowerCase()
            memo[group.short] = _.sortBy(group.values, 'name')[0].name
          , {}

      $scope.send = ->
        start = Date.now()
        $scope.formErrors = undefined
        $scope.errorMessage = undefined
        $scope.forms.shortform.saving = yes
        $scope.forms.shortform.saved = no
        $scope.forms.shortform.submitted = no

        if $scope.forms.shortform.$invalid
          if $scope.forms.shortform.email?.$invalid
            $scope.formErrors = 'Please fill out all required fields and/or enter a valid email'
          errorTimeout.begin $scope.forms.shortform, start
          return
        else
          $http.post '/api/lead', $scope.shortform
          .success ->
            saveTimeout.begin start, $scope.forms.shortform
          .error (message) ->
            $scope.errorMessage = message
            errorTimeout.begin $scope.forms.shortform, start
    ]
