(function() {
  angular.module('app.admin').controller('AdminShortform', [
    '$scope', '$http', '$timeout', 'growl', 'saveTimeout', 'errorTimeout', function($scope, $http, $timeout, growl, saveTimeout, errorTimeout) {
      $scope.shortform = {
        status: '',
        agent: '',
        attributes: {}
      };
      $scope.forms = {};
      async.map(['api/attributes?q=active', 'api/statuses?q=active', 'api/senders?q=active'], function(url, cb) {
        return $http.get(url).success(function(data) {
          return cb(null, data);
        }).error(function(err) {
          return cb(err);
        });
      }, function(err, data) {
        $scope.statuses = data[1];
        $scope.attributes = data[0];
        $scope.senders = data[2];
        $scope.shortform.status = _.first(_.values($scope.statuses)).name;
        $scope.shortform.agent = $scope.senders[0].email;
        return $scope.shortform.attributes = _.transform($scope.attributes, function(memo, group) {
          group.short = group.name.replace(/\s*/g, '').toLowerCase();
          return memo[group.short] = _.sortBy(group.values, 'name')[0].name;
        }, {});
      });
      return $scope.send = function() {
        var ref, start;
        start = Date.now();
        $scope.formErrors = void 0;
        $scope.errorMessage = void 0;
        $scope.forms.shortform.saving = true;
        $scope.forms.shortform.saved = false;
        $scope.forms.shortform.submitted = false;
        if ($scope.forms.shortform.$invalid) {
          if ((ref = $scope.forms.shortform.email) != null ? ref.$invalid : void 0) {
            $scope.formErrors = 'Please fill out all required fields and/or enter a valid email';
          }
          errorTimeout.begin($scope.forms.shortform, start);
        } else {
          return $http.post('/api/lead', $scope.shortform).success(function() {
            return saveTimeout.begin(start, $scope.forms.shortform);
          }).error(function(message) {
            $scope.errorMessage = message;
            return errorTimeout.begin($scope.forms.shortform, start);
          });
        }
      };
    }
  ]);

}).call(this);
