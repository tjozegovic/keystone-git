angular.module('app')
  .controller 'LeadInfo', ['$scope', '$http', '$uibModal', '$uibModalInstance', '$timeout', 'id', ($scope, $http, $uibModal, $uibModalInstance, $timeout, id) ->
    $scope.id = id
    $scope.$loading = yes
    start = Date.now()

    $http.get('api/statuses')
      .success (statuses) ->
        return unless _.keys(statuses).length

        $scope.statuses = _.indexBy statuses, 'id'

        _.forEach $scope.statuses, (status) ->
          status.segments = _.indexBy status.segments, 'id'
          _.forEach status.segments, (segment) ->
            segment.steps = _.indexBy segment.steps, 'id'

    $timeout ->
      async.map ['lead/info/' + $scope.id, 'api/search/leadById/' + $scope.id, 'api/events/lead/' + $scope.id],
        (url, cb) ->
          $http.get(url)
            .success (data) -> cb null, data
            .error (err) -> cb err
        (err, data) ->
          $scope.lead = data[0]
          $scope.emails = data[1]
          $scope.events = data[2]
          $scope.email_events = []
          general = {}
          for email in $scope.emails
            general = id: email.id, sender: email.sender?.email || 'No sender', type: email.type
            if email.type is 'drip'
              _.assign general, {status: email.status, segment: email.segment, step: email.step}
            else if email.type is 'direct'
              general.message = email.message.name
            $scope.email_events.push _.assign({event: 'errored', at: email.errored.at}, general) if email.errored
            $scope.email_events.push _.assign({event: 'queued', at: email.queued}, general) if email.queued
            $scope.email_events.push  _.assign({event: 'send', at: email.delivered}, general) if email.delivered
            $scope.email_events.push _.assign({event: 'bounced', at: email.bounced}, general) if email.bounced
            if email.opened
              for open in email.opened
                $scope.email_events.push _.assign {event: 'open', at: open.at}, general
            if email.clicked
              for click in email.clicked
                $scope.email_events.push _.assign {event: 'click', at: click.at}, general
          $scope.emails = []
          $scope.$loading = no
    , Math.max 0, 800 - (Date.now() - start)

    # message preview
    $scope.show = (send) ->
      $scope.send = {}
      $http.get('api/emails/' + send.id).success (data) ->
        _.assign $scope.send, data

        $uibModal.open
          templateUrl: 'previewLead-modal.html'
          size: 'lg'
          scope: $scope

    $scope.dismiss = -> $uibModalInstance.dismiss()
  ]

  .controller 'ProfileModal',
    ['$scope', '$http', '$uibModalInstance', 'dismissCheck', 'url',
    ($scope, $http, $uibModalInstance, dismissCheck, url) ->
      $scope.fields = []
      $scope.forms = {}
      $scope.url = url

      $http.get $scope.url
      .success (fields) ->
        $scope.fields = fields
      .error (err) ->
        growl.danger 'Failed to load profile settings. ' + err

      $scope.add = (arr, item) ->
        arr.push item
        $scope.forms.profile.$setDirty()

      $scope.delete = (arr, item) ->
        arr.splice arr.indexOf(item), 1
        $scope.forms.profile.$setDirty()

      $scope.save = ->
        $http.put($scope.url, $scope.fields)
        .success ->
          $scope.$submitted = yes
          $scope.forms.profile.$setPristine()
        .error (err) ->
          growl.danger 'Failed to save profile settings. ' + err

      $scope.close = ->
        if $scope.forms.profile.$dirty
          dismissCheck.dismiss $uibModalInstance, save: $scope.save
        else
          $uibModalInstance.close()

    ]

  .controller 'ConfirmClose', ['$scope', '$uibModal', '$uibModalInstance', 'type', 'refItem', 'scopeItem', 'save', 'growl',
  ($scope, $uibModal, $uibModalInstance, type, refItem, scopeItem, save, growl) ->
    $scope.type = type

    scopeItem = refItem unless not refItem
    $scope.temp = save or undefined
    $scope.save = ->
      save (err, result) ->
        if err
          growl.danger 'Error saving: ', err
          $uibModalInstance.dismiss()
        growl.success 'Saved!'
        $uibModalInstance.close result

    $scope.confirm = -> $uibModalInstance.close scopeItem
    $scope.dismiss = -> $uibModalInstance.dismiss()
  ]

  .controller 'Header', ['$scope', '$location', ($scope, $location) ->
    $scope.$on '$stateChangeSuccess', (event, toState, toParams) ->
      $scope.category = toState.data?.section or toParams.section

    $scope.isActive = (url) -> ~$location.path().indexOf url
  ]

  .controller 'ClientSearch', ['$scope', ($scope) ->
    # needed a controller to encapsulate the search variable
  ]
