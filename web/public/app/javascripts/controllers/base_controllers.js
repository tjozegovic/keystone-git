(function() {
  angular.module('app').controller('LeadInfo', [
    '$scope', '$http', '$uibModal', '$uibModalInstance', '$timeout', 'id', function($scope, $http, $uibModal, $uibModalInstance, $timeout, id) {
      var start;
      $scope.id = id;
      $scope.$loading = true;
      start = Date.now();
      $http.get('api/statuses').success(function(statuses) {
        if (!_.keys(statuses).length) {
          return;
        }
        $scope.statuses = _.indexBy(statuses, 'id');
        return _.forEach($scope.statuses, function(status) {
          status.segments = _.indexBy(status.segments, 'id');
          return _.forEach(status.segments, function(segment) {
            return segment.steps = _.indexBy(segment.steps, 'id');
          });
        });
      });
      $timeout(function() {
        return async.map(['lead/info/' + $scope.id, 'api/search/leadById/' + $scope.id, 'api/events/lead/' + $scope.id], function(url, cb) {
          return $http.get(url).success(function(data) {
            return cb(null, data);
          }).error(function(err) {
            return cb(err);
          });
        }, function(err, data) {
          var click, email, general, i, j, k, len, len1, len2, open, ref, ref1, ref2, ref3;
          $scope.lead = data[0];
          $scope.emails = data[1];
          $scope.events = data[2];
          $scope.email_events = [];
          general = {};
          ref = $scope.emails;
          for (i = 0, len = ref.length; i < len; i++) {
            email = ref[i];
            general = {
              id: email.id,
              sender: ((ref1 = email.sender) != null ? ref1.email : void 0) || 'No sender',
              type: email.type
            };
            if (email.type === 'drip') {
              _.assign(general, {
                status: email.status,
                segment: email.segment,
                step: email.step
              });
            } else if (email.type === 'direct') {
              general.message = email.message.name;
            }
            if (email.errored) {
              $scope.email_events.push(_.assign({
                event: 'errored',
                at: email.errored.at
              }, general));
            }
            if (email.queued) {
              $scope.email_events.push(_.assign({
                event: 'queued',
                at: email.queued
              }, general));
            }
            if (email.delivered) {
              $scope.email_events.push(_.assign({
                event: 'send',
                at: email.delivered
              }, general));
            }
            if (email.bounced) {
              $scope.email_events.push(_.assign({
                event: 'bounced',
                at: email.bounced
              }, general));
            }
            if (email.opened) {
              ref2 = email.opened;
              for (j = 0, len1 = ref2.length; j < len1; j++) {
                open = ref2[j];
                $scope.email_events.push(_.assign({
                  event: 'open',
                  at: open.at
                }, general));
              }
            }
            if (email.clicked) {
              ref3 = email.clicked;
              for (k = 0, len2 = ref3.length; k < len2; k++) {
                click = ref3[k];
                $scope.email_events.push(_.assign({
                  event: 'click',
                  at: click.at
                }, general));
              }
            }
          }
          $scope.emails = [];
          return $scope.$loading = false;
        });
      }, Math.max(0, 800 - (Date.now() - start)));
      $scope.show = function(send) {
        $scope.send = {};
        return $http.get('api/emails/' + send.id).success(function(data) {
          _.assign($scope.send, data);
          return $uibModal.open({
            templateUrl: 'previewLead-modal.html',
            size: 'lg',
            scope: $scope
          });
        });
      };
      return $scope.dismiss = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]).controller('ProfileModal', [
    '$scope', '$http', '$uibModalInstance', 'dismissCheck', 'url', function($scope, $http, $uibModalInstance, dismissCheck, url) {
      $scope.fields = [];
      $scope.forms = {};
      $scope.url = url;
      $http.get($scope.url).success(function(fields) {
        return $scope.fields = fields;
      }).error(function(err) {
        return growl.danger('Failed to load profile settings. ' + err);
      });
      $scope.add = function(arr, item) {
        arr.push(item);
        return $scope.forms.profile.$setDirty();
      };
      $scope["delete"] = function(arr, item) {
        arr.splice(arr.indexOf(item), 1);
        return $scope.forms.profile.$setDirty();
      };
      $scope.save = function() {
        return $http.put($scope.url, $scope.fields).success(function() {
          $scope.$submitted = true;
          return $scope.forms.profile.$setPristine();
        }).error(function(err) {
          return growl.danger('Failed to save profile settings. ' + err);
        });
      };
      return $scope.close = function() {
        if ($scope.forms.profile.$dirty) {
          return dismissCheck.dismiss($uibModalInstance, {
            save: $scope.save
          });
        } else {
          return $uibModalInstance.close();
        }
      };
    }
  ]).controller('ConfirmClose', [
    '$scope', '$uibModal', '$uibModalInstance', 'type', 'refItem', 'scopeItem', 'save', 'growl', function($scope, $uibModal, $uibModalInstance, type, refItem, scopeItem, save, growl) {
      $scope.type = type;
      if (!!refItem) {
        scopeItem = refItem;
      }
      $scope.temp = save || void 0;
      $scope.save = function() {
        return save(function(err, result) {
          if (err) {
            growl.danger('Error saving: ', err);
            $uibModalInstance.dismiss();
          }
          growl.success('Saved!');
          return $uibModalInstance.close(result);
        });
      };
      $scope.confirm = function() {
        return $uibModalInstance.close(scopeItem);
      };
      return $scope.dismiss = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]).controller('Header', [
    '$scope', '$location', function($scope, $location) {
      $scope.$on('$stateChangeSuccess', function(event, toState, toParams) {
        var ref;
        return $scope.category = ((ref = toState.data) != null ? ref.section : void 0) || toParams.section;
      });
      return $scope.isActive = function(url) {
        return ~$location.path().indexOf(url);
      };
    }
  ]).controller('ClientSearch', ['$scope', function($scope) {}]);

}).call(this);
