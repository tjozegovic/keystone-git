angular.module('app.manage')
  .controller 'ManageAssets', [
    '$scope', '$http', '$uibModal', '$timeout', 'fileUpload', 'growl',
    ($scope, $http, $uibModal, $timeout, uploader, growl) ->
      start = Date.now()
      $scope.$loading = yes
      $scope.type = 'public'
      $scope.checking = yes

      $timeout ->
        $scope.error = no
        $http.get('/api/assets/public')
        .success (assets) ->
          $scope.$loading = no
          $scope.assets = assets
        .error (err) ->
          $scope.$loading = no
          $scope.error = yes
      , Math.max 0,800 - (Date.now() - start)

      $http.get 'api/check/assets'
      .success (results) ->
        $scope.checking = no
        $scope.private_folder_check = results.check

      $scope.$watch 'type', (type, old) ->
        $scope.get_type(type) if type isnt old

      $scope.get_type = (type) ->
        $scope.$loading = yes
        $http.get '/api/assets/' + type
        .success (assets) ->
          $scope.$loading = no
          $scope.assets = assets
        .error (err) ->
          $scope.assets = undefined
          $scope.$loading = no
          $scope.error = yes

      $scope.newupload = ->
        inst = $uibModal.open
          templateUrl: 'upload-modal.html'
          controller: 'UploadModal'

        inst.result.then (asset) ->
          $http.get("/api/aws/url?file=#{asset.name}&type=#{asset.file.type}")
          .success (data) ->
            fd = new FormData()
            fd.append 'file', asset.file

            $http.put data.url, asset.file,
              transformRequest: angular.identity
              headers: 'Content-Type': asset.file.type
          .error (err) ->
            growl.danger err
  ]

  .controller 'UploadModal', [
    '$scope', '$uibModalInstance',
    ($scope, $uibModalInstance) ->
      $scope.meta = name: null, file: null

      $scope.upload = ->
        $uibModalInstance.close $scope.meta
  ]
