(function() {
  angular.module('app.manage').controller('ManageAssets', [
    '$scope', '$http', '$uibModal', '$timeout', 'fileUpload', 'growl', function($scope, $http, $uibModal, $timeout, uploader, growl) {
      var start;
      start = Date.now();
      $scope.$loading = true;
      $scope.type = 'public';
      $scope.checking = true;
      $timeout(function() {
        $scope.error = false;
        return $http.get('/api/assets/public').success(function(assets) {
          $scope.$loading = false;
          return $scope.assets = assets;
        }).error(function(err) {
          $scope.$loading = false;
          return $scope.error = true;
        });
      }, Math.max(0, 800 - (Date.now() - start)));
      $http.get('api/check/assets').success(function(results) {
        $scope.checking = false;
        return $scope.private_folder_check = results.check;
      });
      $scope.$watch('type', function(type, old) {
        if (type !== old) {
          return $scope.get_type(type);
        }
      });
      $scope.get_type = function(type) {
        $scope.$loading = true;
        return $http.get('/api/assets/' + type).success(function(assets) {
          $scope.$loading = false;
          return $scope.assets = assets;
        }).error(function(err) {
          $scope.assets = void 0;
          $scope.$loading = false;
          return $scope.error = true;
        });
      };
      return $scope.newupload = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'upload-modal.html',
          controller: 'UploadModal'
        });
        return inst.result.then(function(asset) {
          return $http.get("/api/aws/url?file=" + asset.name + "&type=" + asset.file.type).success(function(data) {
            var fd;
            fd = new FormData();
            fd.append('file', asset.file);
            return $http.put(data.url, asset.file, {
              transformRequest: angular.identity,
              headers: {
                'Content-Type': asset.file.type
              }
            });
          }).error(function(err) {
            return growl.danger(err);
          });
        });
      };
    }
  ]).controller('UploadModal', [
    '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
      $scope.meta = {
        name: null,
        file: null
      };
      return $scope.upload = function() {
        return $uibModalInstance.close($scope.meta);
      };
    }
  ]);

}).call(this);
