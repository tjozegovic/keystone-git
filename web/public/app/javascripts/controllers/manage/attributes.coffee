angular.module('app.manage')
  .controller 'ManageAttributes',
    ['$scope', '$http', '$uibModal', 'growl',
    ($scope, $http, $uibModal, growl) ->
      $http.get('/api/attributes?q=active').success (data) ->
        $scope.attributes = data or []

      $http.get('api/account/settings').success (settings) ->
        $scope.settings = settings

      $scope.new = ->
        inst = $uibModal.open
          templateUrl: 'manage/attribute-modal.html'
          controller: 'AttributeModal'
          resolve:
            attribute: -> name: '', values: []
            attributes: -> angular.copy $scope.attributes

        inst.result.then (attributes) ->
          $scope.attributes = attributes

      $scope.edit = (attribute) ->
        inst = $uibModal.open
          templateUrl: 'manage/attribute-modal.html'
          controller: 'AttributeModal'
          resolve:
            attribute: -> angular.copy attribute
            attributes: -> angular.copy $scope.attributes

        inst.result.then (update) ->
          $scope.attributes = update

      $scope.delete = ($event, id) ->
        $event.stopPropagation()

        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'
          resolve:
            scopeItem: -> null
            refItem: -> null

        inst.result.then ->
          $http.delete '/api/attributes/' + id
          .success (data) ->
            _.remove $scope.attributes, (a) -> a.id is id
            growl.success 'Deleted!'
          .error (err) ->
            growl.danger err

      $scope.bulkUpload = ($event, id) ->
        $event.stopPropagation()

        inst = $uibModal.open
          templateUrl: 'bulkUpload-modal.html'
          controller: 'AttributeUploadModal'
          resolve: id: -> id

        inst.result.then ->
          $http.get('api/attributes?q=active').success (data) ->
            $scope.attributes = data or []
    ]

  .controller 'AttributeModal',
    ['$scope', '$http', '$uibModalInstance', '$uibModal', '$timeout', '$filter', 'attribute', 'attributes', 'dismissCheck',
    'saveTimeout', 'errorTimeout', 'checkLocation',
    ($scope, $http, $uibModalInstance, $uibModal, $timeout, $filter, attribute, attributes, dismissCheck,
    saveTimeout, errorTimeout, checkLocation) ->
      orderBy = $filter('orderBy')
      $scope.attribute = attribute
      $scope.attributes = attributes
      $scope.attribute.newvalues = []
      $scope.default_value = $scope.attribute.default
      attributes_ref = []
      $scope.forms = {}
      angular.copy $scope.attributes, attributes_ref

      do $scope.sortOnSave = ->
        $scope.attribute.values = orderBy($scope.attribute.values, 'name')

      $scope.addValue = ->
        $scope.attribute.newvalues.push id: uuid.v4()
        $scope.forms.attribute.$setDirty()
        $scope.lastValue = $scope.attribute.newvalues[$scope.attribute.newvalues.length-1].id

      $scope.deleteValue = (arr, id) ->
        _.remove arr, (v) -> v.id is id
        $scope.forms.attribute.$setDirty()

      checkNewValues = (cb) ->
        if $scope.attribute.newvalues?.length
          $scope.attribute.values = $scope.attribute.values.concat $scope.attribute.newvalues
          $scope.attribute.newvalues = []
        cb()

      httpPost = (start, cb) ->
        $http.post '/api/attributes', _.omit $scope.attribute, 'newvalues'
        .success (data) ->
          $scope.attribute.id = data.id
          $scope.attribute.version = data.version
          angular.copy $scope.attributes, attributes_ref
          saveTimeout.begin start, $scope.forms.attribute
          if cb? then cb null, $scope.attributes
        .error (err) ->
          $scope.errorMessage = err
          errorTimeout.begin $scope.forms.attribute, start
          if cb? then cb err

      httpPut = (start, attribute, cb) ->
        $http.put '/api/attributes/' + attribute.id, _.omit attribute, 'newvalues'
        .success (data) ->
          $scope.attribute.version = data.version
          angular.copy $scope.attributes, attributes_ref
          saveTimeout.begin start, $scope.forms.attribute
          $scope.attribute.newvalues = []
          if cb? then cb null, $scope.attributes
        .error (err) ->
          $scope.errorMessage = err
          errorTimeout.begin $scope.forms.attribute, start
          if cb? then cb err

      $scope.save = (cb) ->
        start = Date.now()
        $scope.errorMessage = undefined
        $scope.forms.attribute.saved = no
        $scope.forms.attribute.saving = yes
        $scope.forms.attribute.submitted = no

        if $scope.forms.attribute.$invalid
          errorTimeout.begin $scope.forms.attribute, start
        else
          $scope.attribute.default = $scope.default_value

          checkNewValues ->
            checkLocation.id $scope.attributes, $scope.attribute, ->
              $scope.sortOnSave()
              if $scope.attribute.id?
                httpPut start, attribute, cb
              else
                httpPost start, cb

      $scope.close = ->
        if $scope.forms.attribute.saved and $scope.forms.attribute.$pristine
          $uibModalInstance.close $scope.attributes
        else if $scope.forms.attribute.$dirty or $scope.forms.attribute.$invalid
          dismissCheck.dismiss $uibModalInstance, item: $scope.attributes, ref: attributes_ref, save: $scope.save
        else
          $uibModalInstance.dismiss()
    ]

  .directive 'ngEnter', ->
    (scope, elm, attrs) ->
      elm.bind 'keydown keypress', (event) ->
        if event.which is 13
          scope.$apply ->
            scope.$eval(attrs.ngEnter)
          $('input[name="' + scope.$parent.lastValue + '"]').focus()
          event.preventDefault()

  .controller 'AttributeUploadModal',
    ['$scope', '$http', '$uibModalInstance', '$uibModal', 'id',
    ($scope, $http, $uibModalInstance, $uibModal, id) ->
      $scope.forms = upload: null
      $scope.id = id

      $scope.$watch 'forms.upload.csv', (csv) ->
        return unless csv
        $scope.typeerror = !~csv.name.indexOf '.csv'

      $scope.url = ->
        if $scope.includeAttributes
          "api/attributes/#{$scope.id}/download"
        else
          'values/template/download'

      $scope.upload = ->
        return $scope.typeerror = yes unless ~$scope.forms.upload.csv.name.indexOf '.csv'

        fd = new FormData
        fd.append 'file', $scope.forms.upload.csv

        $http.post("api/attributes/#{$scope.id}/upload", fd,
          transformRequest: angular.identity
          headers: 'Content-Type': undefined
        ).success (stats) ->
          $uibModalInstance.close()

          inst = $uibModal.open
            templateUrl: 'attributeStats-modal.html'
            controller: ['$scope', '$uibModalInstance', ($scope, $uibModalInstance) ->
              $scope.stats = stats
              $scope.close = -> $uibModalInstance.close()
            ]
    ]
