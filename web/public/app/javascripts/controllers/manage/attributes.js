(function() {
  angular.module('app.manage').controller('ManageAttributes', [
    '$scope', '$http', '$uibModal', 'growl', function($scope, $http, $uibModal, growl) {
      $http.get('/api/attributes?q=active').success(function(data) {
        return $scope.attributes = data || [];
      });
      $http.get('api/account/settings').success(function(settings) {
        return $scope.settings = settings;
      });
      $scope["new"] = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'manage/attribute-modal.html',
          controller: 'AttributeModal',
          resolve: {
            attribute: function() {
              return {
                name: '',
                values: []
              };
            },
            attributes: function() {
              return angular.copy($scope.attributes);
            }
          }
        });
        return inst.result.then(function(attributes) {
          return $scope.attributes = attributes;
        });
      };
      $scope.edit = function(attribute) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'manage/attribute-modal.html',
          controller: 'AttributeModal',
          resolve: {
            attribute: function() {
              return angular.copy(attribute);
            },
            attributes: function() {
              return angular.copy($scope.attributes);
            }
          }
        });
        return inst.result.then(function(update) {
          return $scope.attributes = update;
        });
      };
      $scope["delete"] = function($event, id) {
        var inst;
        $event.stopPropagation();
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal',
          resolve: {
            scopeItem: function() {
              return null;
            },
            refItem: function() {
              return null;
            }
          }
        });
        return inst.result.then(function() {
          return $http["delete"]('/api/attributes/' + id).success(function(data) {
            _.remove($scope.attributes, function(a) {
              return a.id === id;
            });
            return growl.success('Deleted!');
          }).error(function(err) {
            return growl.danger(err);
          });
        });
      };
      return $scope.bulkUpload = function($event, id) {
        var inst;
        $event.stopPropagation();
        inst = $uibModal.open({
          templateUrl: 'bulkUpload-modal.html',
          controller: 'AttributeUploadModal',
          resolve: {
            id: function() {
              return id;
            }
          }
        });
        return inst.result.then(function() {
          return $http.get('api/attributes?q=active').success(function(data) {
            return $scope.attributes = data || [];
          });
        });
      };
    }
  ]).controller('AttributeModal', [
    '$scope', '$http', '$uibModalInstance', '$uibModal', '$timeout', '$filter', 'attribute', 'attributes', 'dismissCheck', 'saveTimeout', 'errorTimeout', 'checkLocation', function($scope, $http, $uibModalInstance, $uibModal, $timeout, $filter, attribute, attributes, dismissCheck, saveTimeout, errorTimeout, checkLocation) {
      var attributes_ref, checkNewValues, httpPost, httpPut, orderBy;
      orderBy = $filter('orderBy');
      $scope.attribute = attribute;
      $scope.attributes = attributes;
      $scope.attribute.newvalues = [];
      $scope.default_value = $scope.attribute["default"];
      attributes_ref = [];
      $scope.forms = {};
      angular.copy($scope.attributes, attributes_ref);
      ($scope.sortOnSave = function() {
        return $scope.attribute.values = orderBy($scope.attribute.values, 'name');
      })();
      $scope.addValue = function() {
        $scope.attribute.newvalues.push({
          id: uuid.v4()
        });
        $scope.forms.attribute.$setDirty();
        return $scope.lastValue = $scope.attribute.newvalues[$scope.attribute.newvalues.length - 1].id;
      };
      $scope.deleteValue = function(arr, id) {
        _.remove(arr, function(v) {
          return v.id === id;
        });
        return $scope.forms.attribute.$setDirty();
      };
      checkNewValues = function(cb) {
        var ref;
        if ((ref = $scope.attribute.newvalues) != null ? ref.length : void 0) {
          $scope.attribute.values = $scope.attribute.values.concat($scope.attribute.newvalues);
          $scope.attribute.newvalues = [];
        }
        return cb();
      };
      httpPost = function(start, cb) {
        return $http.post('/api/attributes', _.omit($scope.attribute, 'newvalues')).success(function(data) {
          $scope.attribute.id = data.id;
          $scope.attribute.version = data.version;
          angular.copy($scope.attributes, attributes_ref);
          saveTimeout.begin(start, $scope.forms.attribute);
          if (cb != null) {
            return cb(null, $scope.attributes);
          }
        }).error(function(err) {
          $scope.errorMessage = err;
          errorTimeout.begin($scope.forms.attribute, start);
          if (cb != null) {
            return cb(err);
          }
        });
      };
      httpPut = function(start, attribute, cb) {
        return $http.put('/api/attributes/' + attribute.id, _.omit(attribute, 'newvalues')).success(function(data) {
          $scope.attribute.version = data.version;
          angular.copy($scope.attributes, attributes_ref);
          saveTimeout.begin(start, $scope.forms.attribute);
          $scope.attribute.newvalues = [];
          if (cb != null) {
            return cb(null, $scope.attributes);
          }
        }).error(function(err) {
          $scope.errorMessage = err;
          errorTimeout.begin($scope.forms.attribute, start);
          if (cb != null) {
            return cb(err);
          }
        });
      };
      $scope.save = function(cb) {
        var start;
        start = Date.now();
        $scope.errorMessage = void 0;
        $scope.forms.attribute.saved = false;
        $scope.forms.attribute.saving = true;
        $scope.forms.attribute.submitted = false;
        if ($scope.forms.attribute.$invalid) {
          return errorTimeout.begin($scope.forms.attribute, start);
        } else {
          $scope.attribute["default"] = $scope.default_value;
          return checkNewValues(function() {
            return checkLocation.id($scope.attributes, $scope.attribute, function() {
              $scope.sortOnSave();
              if ($scope.attribute.id != null) {
                return httpPut(start, attribute, cb);
              } else {
                return httpPost(start, cb);
              }
            });
          });
        }
      };
      return $scope.close = function() {
        if ($scope.forms.attribute.saved && $scope.forms.attribute.$pristine) {
          return $uibModalInstance.close($scope.attributes);
        } else if ($scope.forms.attribute.$dirty || $scope.forms.attribute.$invalid) {
          return dismissCheck.dismiss($uibModalInstance, {
            item: $scope.attributes,
            ref: attributes_ref,
            save: $scope.save
          });
        } else {
          return $uibModalInstance.dismiss();
        }
      };
    }
  ]).directive('ngEnter', function() {
    return function(scope, elm, attrs) {
      return elm.bind('keydown keypress', function(event) {
        if (event.which === 13) {
          scope.$apply(function() {
            return scope.$eval(attrs.ngEnter);
          });
          $('input[name="' + scope.$parent.lastValue + '"]').focus();
          return event.preventDefault();
        }
      });
    };
  }).controller('AttributeUploadModal', [
    '$scope', '$http', '$uibModalInstance', '$uibModal', 'id', function($scope, $http, $uibModalInstance, $uibModal, id) {
      $scope.forms = {
        upload: null
      };
      $scope.id = id;
      $scope.$watch('forms.upload.csv', function(csv) {
        if (!csv) {
          return;
        }
        return $scope.typeerror = !~csv.name.indexOf('.csv');
      });
      $scope.url = function() {
        if ($scope.includeAttributes) {
          return "api/attributes/" + $scope.id + "/download";
        } else {
          return 'values/template/download';
        }
      };
      return $scope.upload = function() {
        var fd;
        if (!~$scope.forms.upload.csv.name.indexOf('.csv')) {
          return $scope.typeerror = true;
        }
        fd = new FormData;
        fd.append('file', $scope.forms.upload.csv);
        return $http.post("api/attributes/" + $scope.id + "/upload", fd, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': void 0
          }
        }).success(function(stats) {
          var inst;
          $uibModalInstance.close();
          return inst = $uibModal.open({
            templateUrl: 'attributeStats-modal.html',
            controller: [
              '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
                $scope.stats = stats;
                return $scope.close = function() {
                  return $uibModalInstance.close();
                };
              }
            ]
          });
        });
      };
    }
  ]);

}).call(this);
