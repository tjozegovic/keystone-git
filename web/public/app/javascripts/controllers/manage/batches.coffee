angular.module('app.manage')
  .filter 'trust', ['$sce', ($sce) ->
    (html) -> $sce.trustAsHtml html
  ]

  .config [
    '$stateProvider', '$urlRouterProvider',
    ($stateProvider, $urlRouterProvider) ->
      $stateProvider
        .state 'manage-batches',
          data: section: 'manage'
          url: '/manage/batches'
          templateUrl: '/html/manage/batches.html'

        .state 'manage-batches.edit',
          url: '/:batch'

        .state 'manage-batches-with-message',
          url: '/manage/batch/:message'
          templateUrl: '/html/manage/batches.html'
  ]

  .controller 'ManageBatches',
    ['$scope', '$http', '$timeout', '$q', '$sce', '$state', '$uibModal', 'growl',
    'saveTimeout', 'errorTimeout',
    ($scope, $http, $timeout, $q, $sce, $state, $uibModal, growl,
    saveTimeout, errorTimeout ) ->
      $scope['editor.batches'] = extraPlugins: 'strinsert', height: '245px'

      $scope.nav = html: {}, text: {}
      $scope.batches = $scope.recent = []
      $scope.fileError = no
      $scope.message_id = $state.params.message

      # TODO there are some very similar pieces in here to the StepModal in statuses
      start = Date.now()
      $scope.selected = batch: null
      $scope.sticky = []
      $scope.recent = []
      $scope.expanded = pending: yes, scheduled: yes, sent: yes

      $scope.$loading = yes
      urls = ['batches?q=pending', 'batches?q=recent', 'batches?q=scheduled', 'templates', 'pages', 'policies', 'messages?q=all']
      $q.all _.map(urls, (url) -> $http.get "/api/#{url}")
      .then ([batches, recent, deployed, templates, pages, policies, messages]) ->
        $scope.batches = batches.data
        for send in recent.data
          if send.sticky
            $scope.sticky.push send
          else
            $scope.recent.push send
        $scope.deployed = deployed.data
        $scope.templates = templates.data
        $scope.pages = _.zipObject _.pluck(pages.data, 'id'), _.pluck(pages.data, 'name')
        $scope.policies = _.zipObject _.pluck(policies.data, 'id'), _.pluck(policies.data, 'name')
        $scope.availableMessages = _.filter messages.data, {'progressStatus' : 'completed'}
        $scope.$thumbnailsLoaded = no
        $timeout ->
          $scope.$loading = no
        , Math.max 0, 800 - (Date.now() - start)

        if batch = $state.params.batch
          find = _([])
            .concat $scope.batches
            .concat $scope.recent
            .find id: batch
          $scope.set find if find?
        else if $scope.batches.length && !$state.params.message
          $scope.set $scope.batches[0]
        else
          $scope.new()

      .catch (err) ->
        growl.danger err

      $scope.reset = ->
        $scope.message_id = null
        $scope.batch.message = null
        $scope.batch.subject = null
        $scope.batch.html = null
        $scope.batch.text = null
        $scope.batch.template = null

      $scope.new = ->
        $scope.batches.push batch =
          name: "#{moment().format 'MMM Do @ h:mma'}"
          hasChanged: no
          new: yes
          schedule: now: yes
          message: $scope.message_id
        $scope.set batch

      $scope.set = (batch) ->
        $scope.batch = batch
        $scope.selected.batch = $scope.batch.id
        $scope.tempName = $scope.batch.name
        if($scope.batch.message)
          $scope.setMessageValues()
        unless $scope.batch.new
          $scope.batch.hasChanged = yes
        if(batch.id)
          $state.go 'manage-batches.edit', batch: batch.id
          $timeout (-> $scope.batchform?.$setPristine()), 1000 if $scope.batch.id

      $scope.onMessageChange = (messageId) ->
        if $scope.batch.message
          $scope.setMessageValues()
        else
          $scope.reset()

      $scope.setMessageValues = ->
        message = _.find $scope.availableMessages, {'id': $scope.batch.message}
        if message?
          $scope.batch.subject = message.subject
          $scope.batch.html = message.html
          $scope.batch.text = message.text
          $scope.batch.template = message.template

      $scope.$on 'ready.ckeditor', ->
        $scope.batchform.$setPristine()

      $scope.$watch 'batch.schedule.now', (now) ->
        return unless $scope.batch
        return if $scope.batch.hasChanged

        nowDate = "#{moment().format 'MMM Do @ h:mma'}"
        whenDate = "#{moment($scope.batch.schedule.when).format 'MMM Do @ h:mma'}"

        if now and $scope.tempName isnt nowDate
          $scope.batch.name = nowDate
          $scope.tempName = $scope.batch.name

        if not now and $scope.tempName isnt whenDate
          $scope.batch.name = whenDate
          $scope.tempName = $scope.batch.name

      $scope.$watch 'batch.schedule.when', (whenDate) ->
        return unless whenDate
        return if $scope.batch.hasChanged

        if not $scope.batch.schedule.now and $scope.tempName is $scope.batch.name
          $scope.batch.name = "#{moment(whenDate).format 'MMM Do @ h:mma'}"
          $scope.tempName = $scope.batch.name

      $scope.$watch 'batch.message', (message) ->
        return unless message

        $http.get 'messages/' + message
        .success (result) ->
          if result.template.value?
            $scope.template = _.find $scope.templates, id: result.template.value
          else
            $scope.template = _.find $scope.templates, id: result.template
        .error (err) ->
          growl.danger 'Error getting message: ', err

      $scope.$watch 'batch', (batch) ->
        return unless batch

        $scope.tempName = $scope.batch.name
        $scope.batch.html ?= {}
        $scope.batch.text ?= {}
        $scope.fileSuccess = no
        $scope.batchform?.saved = no
        $scope.errors = []

        if batch.csv?.name?
          batchName = batch.csv.name
        else
          batchName = null

        unless batch.status isnt 'finished'
          $scope.loadingSummary = yes
          $scope.loadingErrors = yes
          $scope.getSummary()
          $scope.getErrors()

        $('.fileinput-filename').text(batchName)

        if !$scope.$thumbnailsLoaded
          startTemplates = Date.now()
          _.forEach $scope.templates, (template) ->
            template.$thumbnailLoading = yes
            $http.get('api/templates/' + template.id)
              .then (res) ->
                template.thumbnail = res.data.thumbnail
                $timeout ->
                  template.$thumbnailLoading = no
                  $scope.$thumbnailsLoaded = yes
                , Math.max 0, 750 - (Date.now() - startTemplates)

        _setTemplateDefaults() if $scope.template = _.find $scope.templates, id: $scope.batch?.template

      $scope.getSummary = ->
        $http.get 'api/batches/report/' + $scope.batch.id
        .success (report) ->
          $scope.summary = report
          $scope.loadingSummary = no
        .error (err) ->
          growl.danger 'Failed to get report ', err
          $scope.loadingSummary = no

      $scope.getErrors = ->
        $http.get 'api/batches/errors/' + $scope.batch.id
        .success (errors) ->
          $scope.errors = errors
          $scope.loadingErrors = no
        .error (err) ->
          growl.danger 'Failed to get batch errors ', err
          $scope.loadingErrors = no

      $scope.fileTempCheck = (file, cb) ->
        cb null unless file and file.type is 'text/csv'

        $scope.$checking = yes

        fd = new FormData
        fd.append 'file', file

        reader = new FileReader()
        templateHeaders = ['agent', 'recipient.email']
        variables = $scope.template.meta.html.variables

        for item in variables
          unless ~item.name.indexOf('agent') or ~item.name.indexOf('sender') or ~item.name.indexOf('assets')
            templateHeaders.push item.name

        reader.readAsText(file)
        checkHeaders = (headers, templateHeaders) ->
          $scope.fileError = no
          $scope.headerErrors = []
          $scope.fileSuccess = no

          for value in templateHeaders when headers.indexOf(value) is -1
            #TODO: Didn't want to have to clear fileinput this way but had to
            $('.fileinput').fileinput('clear')
            $scope.fileError = yes
            $scope.headerErrors.push value
            $scope.$checking = no
            fd = undefined
            $scope.$apply()
          cb fd, reader

        reader.onload = ->
          readerSliced = reader.result.split /[\r\n]+/g
          headers = readerSliced[0].split /\s*,\s*/g

          checkHeaders headers, templateHeaders

      $scope.trust = (html) ->
        $sce.trustAsHtml html

      done = (start, cb) ->
        if $scope.batch.csv?.name
          $('.fileinput-filename').text($scope.batch.csv.name)
        $scope.$checking = no
        saveTimeout.begin start, $scope.batchform
        cb?()

      httpPut = (start, cb) ->
        $http.put '/api/batches/' + $scope.batch.id, $scope.batch
        .success (body) ->
          $scope.batch.version = body.version
          done start, cb
        .error (err) ->
          $scope.errorMessage = err
          errorTimeout.begin $scope.batchform, start

      httpPost = (start, cb) ->
        $http.post '/api/batches', $scope.batch
        .success (body) ->
          $scope.batch.id = body.id
          $scope.batch.version = body.version
          $scope.batch.status = 'pending'
          done start, cb
        .error (err) ->
          $scope.errorMessage = err
          errorTimeout.begin $scope.batchform, start

      $scope.save = (cb) ->
        fd = new FormData
        start = Date.now()
        $scope.batch.new = no
        $scope.batch.hasChanged = yes
        $scope.errorMessage = undefined
        $scope.batchform.submitted = no
        $scope.batchform.saved = no
        $scope.batchform.saving = yes
        $scope.message_id = null

        if $scope.batchform.$invalid or not $scope.batch.template
          errorTimeout.begin $scope.batchform, start
        else
          if $scope.batch.csv?.file?.type is 'text/csv'
            $scope.fileTempCheck $scope.batch.csv?.file, (fd, reader) ->
              if not $scope.fileError
                if fd
                  fd.append 'name', $scope.batch.csv.file.name
                  fd.append 'file', $scope.batch.csv.file if $scope.batch.csv?.file?
                  $scope.batch.csv.name = $scope.batch.csv.file.name
                  $scope.batch.file = reader?.result
                if $scope.batch.id
                  httpPut start, cb
                else
                  httpPost start, cb
              else
                errorTimeout.begin $scope.batchform, start
          else
            delete $scope.batch.file
            if $scope.batch.id
              httpPut start, cb
            else
              httpPost start, cb

      $scope.setSticky = (batch) ->
        if batch.sticky
          $scope.sticky.push batch
          _.pull $scope.recent, batch
        else
          $scope.recent.push batch
          _.pull $scope.sticky, batch
        $scope.save()

      $scope.expand = (type) ->
        if $scope.expanded[type] is yes
          $scope.expanded[type] = no
        else
          $scope.expanded[type] = yes

      $scope.change = ->
        delete $scope.batch.csv

      $scope.cancelable = ->
        return no unless batch = $scope.batch
        batch.status is 'deployed'

      $scope.archivable = ->
        return no unless batch = $scope.batch
        batch.status is 'finished'

      $scope.editable = ->
        return yes unless batch = $scope.batch
        batch.status not in ['archived', 'failed', 'finished']

      $scope.deployable = ->
        return no unless batch = $scope.batch
        batch.status is 'pending' and batch.csv?.name and $scope.previewable()

      $scope.previewable = ->
        return no unless batch = $scope.batch
        $scope.batchform?.$pristine and batch.subject and batch.template

      $scope.cancel = (batch) ->
        if batch?
          $scope.batch = batch

        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'
          resolve:
            type: -> "Cancel"

        inst.result.then ->
          $http.post('/api/batches/' + $scope.batch.id + '/cancel').success (data) ->
            $scope.batch.version = data.version
            $scope.batch.status = 'pending'
            $scope.batches.push $scope.batch
            $scope.deployed.splice $scope.deployed.indexOf($scope.batch), 1
            $scope.batchform.$setPristine()

      $scope.deploy = ->
        $http.post('/api/batches/' + $scope.batch.id + '/deploy').success (data) ->
          $scope.batch.version = data.version
          if $scope.batch.schedule.now
            $scope.batch.status = 'finished'
            $scope.recent.unshift $scope.batch
            $scope.batches.splice $scope.batches.indexOf($scope.batch), 1
          else
            $scope.batch.status = 'deployed'
            $scope.deployed.push $scope.batch
            $scope.batches.splice $scope.batches.indexOf($scope.batch), 1
          $scope.batchform.$setPristine()

      $scope.clone = ->
        $http.post("api/batches/#{$scope.batch.id}/clone").success (batch) ->
          batch.status = 'pending'
          $scope.batches.push batch
          $state.go '.', batch: batch.id
          $scope.batchform.$setPristine()
          growl.success 'Cloned!'

      $scope.archive = ->
        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'
          resolve:
            type: -> "Archive"

        inst.result.then ->
          $scope.batch.status = 'archived'
          $scope.save ->
            $scope.recent.splice $scope.recent.indexOf($scope.batch), 1
            $scope.batch = undefined

      $scope.delete = (batch) ->
        if batch?
          $scope.batch = batch

        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'
          resolve:
            type: -> "Delete"

        inst.result.then ->
          $http.delete('/api/batches/' + $scope.batch.id)
          .success ->
            $scope.batches.splice $scope.batches.indexOf($scope.batch), 1
            $scope.batch = undefined
            growl.success 'Deleted!'
          .error (err) ->
            growl.danger 'Delete failed ' + err

      $scope.select = (template) ->
        return unless $scope.editable()

        $scope.template = template
        $scope.batch.template = template.id
        _setTemplateDefaults()

      _setTemplateDefaults = ->
        for type in ['html', 'text']
          for part in ['blocks', 'links', 'policies']
            $scope.batch[type]?[part] ?= {}
            for piece in $scope.template.meta[type]?[part] or []
              $scope.batch[type][part][piece.name] ?= if part is 'links' then type: '' else ''
        $scope.nav.html.selectedBlock = $scope.template.meta.html.blocks[0]?.name

      $scope.preview = ->
        inst = $uibModal.open
          templateUrl: 'preview-modal.html'
          controller: 'BatchPreviewModal'
          resolve: template: -> $scope.template

        inst.result.then (preview) ->
          preview.type = 'batch'
          preview.batch = $scope.batch.id
          $http.post('api/send', preview)
  ]

  .controller 'BatchPreviewModal',
    ['$scope', '$http', '$uibModalInstance', 'template'
    ($scope, $http, $uibModalInstance, template) ->
      _setter = (obj, prop, val) ->
        props = prop.split '.'
        final = props.pop()

        while p = props.shift()
          obj[p] = {} if typeof obj[p] is 'undefined'
          obj = obj[p]

        if val then obj[final] = val else obj[final]

      $scope.preview = {}
      $scope.custom = {}

      $scope.template = template
      combo = [].concat(template.meta.html.variables, template.meta.text.variables)
      $scope.variables = _.uniq _.remove _.pluck(combo, 'name'), (v) ->
        !~v.indexOf('assets.') and !~v.indexOf('sender.') and !~v.indexOf('agent.') and !~v.indexOf('lead.attributes.') and v isnt 'unsub'

      $http.get('api/attributes?q=active').success (attributes) ->
        $scope.attributes = attributes
        _.forEach $scope.attributes, (attribute) ->
          attribute._name = 'lead.attributes.' + attribute.name.replace(/\s/g, '').toLowerCase()
          $scope.attribute_variables = _.uniq _.remove _.pluck(combo, 'name'), (a) ->
            !!~a.indexOf('lead.attributes.')
          $scope.filtered_attributes = []
          _.forEach $scope.attributes, (attribute) ->
            _.forEach $scope.attribute_variables, (av) ->
              $scope.filtered_attributes.push attribute if av is attribute._name

      $http.get('api/senders?q=active').success (senders) ->
        $scope.senders = _.pluck senders, 'email'

      $scope.trigger = ->
        _.forEach [$scope.variables, $scope.attribute_variables], (v) ->
          for path in v or []
            _setter $scope.preview, path, $scope.custom[path]
        $uibModalInstance.close $scope.preview
    ]
