(function() {
  angular.module('app.manage').filter('trust', [
    '$sce', function($sce) {
      return function(html) {
        return $sce.trustAsHtml(html);
      };
    }
  ]).config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      return $stateProvider.state('manage-batches', {
        data: {
          section: 'manage'
        },
        url: '/manage/batches',
        templateUrl: '/html/manage/batches.html'
      }).state('manage-batches.edit', {
        url: '/:batch'
      }).state('manage-batches-with-message', {
        url: '/manage/batch/:message',
        templateUrl: '/html/manage/batches.html'
      });
    }
  ]).controller('ManageBatches', [
    '$scope', '$http', '$timeout', '$q', '$sce', '$state', '$uibModal', 'growl', 'saveTimeout', 'errorTimeout', function($scope, $http, $timeout, $q, $sce, $state, $uibModal, growl, saveTimeout, errorTimeout) {
      var _setTemplateDefaults, done, httpPost, httpPut, start, urls;
      $scope['editor.batches'] = {
        extraPlugins: 'strinsert',
        height: '245px'
      };
      $scope.nav = {
        html: {},
        text: {}
      };
      $scope.batches = $scope.recent = [];
      $scope.fileError = false;
      $scope.message_id = $state.params.message;
      start = Date.now();
      $scope.selected = {
        batch: null
      };
      $scope.sticky = [];
      $scope.recent = [];
      $scope.expanded = {
        pending: true,
        scheduled: true,
        sent: true
      };
      $scope.$loading = true;
      urls = ['batches?q=pending', 'batches?q=recent', 'batches?q=scheduled', 'templates', 'pages', 'policies', 'messages?q=all'];
      $q.all(_.map(urls, function(url) {
        return $http.get("/api/" + url);
      })).then(function(arg) {
        var batch, batches, deployed, find, i, len, messages, pages, policies, recent, ref, send, templates;
        batches = arg[0], recent = arg[1], deployed = arg[2], templates = arg[3], pages = arg[4], policies = arg[5], messages = arg[6];
        $scope.batches = batches.data;
        ref = recent.data;
        for (i = 0, len = ref.length; i < len; i++) {
          send = ref[i];
          if (send.sticky) {
            $scope.sticky.push(send);
          } else {
            $scope.recent.push(send);
          }
        }
        $scope.deployed = deployed.data;
        $scope.templates = templates.data;
        $scope.pages = _.zipObject(_.pluck(pages.data, 'id'), _.pluck(pages.data, 'name'));
        $scope.policies = _.zipObject(_.pluck(policies.data, 'id'), _.pluck(policies.data, 'name'));
        $scope.availableMessages = _.filter(messages.data, {
          'progressStatus': 'completed'
        });
        $scope.$thumbnailsLoaded = false;
        $timeout(function() {
          return $scope.$loading = false;
        }, Math.max(0, 800 - (Date.now() - start)));
        if (batch = $state.params.batch) {
          find = _([]).concat($scope.batches).concat($scope.recent).find({
            id: batch
          });
          if (find != null) {
            return $scope.set(find);
          }
        } else if ($scope.batches.length && !$state.params.message) {
          return $scope.set($scope.batches[0]);
        } else {
          return $scope["new"]();
        }
      })["catch"](function(err) {
        return growl.danger(err);
      });
      $scope.reset = function() {
        $scope.message_id = null;
        $scope.batch.message = null;
        $scope.batch.subject = null;
        $scope.batch.html = null;
        $scope.batch.text = null;
        return $scope.batch.template = null;
      };
      $scope["new"] = function() {
        var batch;
        $scope.batches.push(batch = {
          name: "" + (moment().format('MMM Do @ h:mma')),
          hasChanged: false,
          "new": true,
          schedule: {
            now: true
          },
          message: $scope.message_id
        });
        return $scope.set(batch);
      };
      $scope.set = function(batch) {
        $scope.batch = batch;
        $scope.selected.batch = $scope.batch.id;
        $scope.tempName = $scope.batch.name;
        if ($scope.batch.message) {
          $scope.setMessageValues();
        }
        if (!$scope.batch["new"]) {
          $scope.batch.hasChanged = true;
        }
        if (batch.id) {
          $state.go('manage-batches.edit', {
            batch: batch.id
          });
          if ($scope.batch.id) {
            return $timeout((function() {
              var ref;
              return (ref = $scope.batchform) != null ? ref.$setPristine() : void 0;
            }), 1000);
          }
        }
      };
      $scope.onMessageChange = function(messageId) {
        if ($scope.batch.message) {
          return $scope.setMessageValues();
        } else {
          return $scope.reset();
        }
      };
      $scope.setMessageValues = function() {
        var message;
        message = _.find($scope.availableMessages, {
          'id': $scope.batch.message
        });
        if (message != null) {
          $scope.batch.subject = message.subject;
          $scope.batch.html = message.html;
          $scope.batch.text = message.text;
          return $scope.batch.template = message.template;
        }
      };
      $scope.$on('ready.ckeditor', function() {
        return $scope.batchform.$setPristine();
      });
      $scope.$watch('batch.schedule.now', function(now) {
        var nowDate, whenDate;
        if (!$scope.batch) {
          return;
        }
        if ($scope.batch.hasChanged) {
          return;
        }
        nowDate = "" + (moment().format('MMM Do @ h:mma'));
        whenDate = "" + (moment($scope.batch.schedule.when).format('MMM Do @ h:mma'));
        if (now && $scope.tempName !== nowDate) {
          $scope.batch.name = nowDate;
          $scope.tempName = $scope.batch.name;
        }
        if (!now && $scope.tempName !== whenDate) {
          $scope.batch.name = whenDate;
          return $scope.tempName = $scope.batch.name;
        }
      });
      $scope.$watch('batch.schedule.when', function(whenDate) {
        if (!whenDate) {
          return;
        }
        if ($scope.batch.hasChanged) {
          return;
        }
        if (!$scope.batch.schedule.now && $scope.tempName === $scope.batch.name) {
          $scope.batch.name = "" + (moment(whenDate).format('MMM Do @ h:mma'));
          return $scope.tempName = $scope.batch.name;
        }
      });
      $scope.$watch('batch.message', function(message) {
        if (!message) {
          return;
        }
        return $http.get('messages/' + message).success(function(result) {
          if (result.template.value != null) {
            return $scope.template = _.find($scope.templates, {
              id: result.template.value
            });
          } else {
            return $scope.template = _.find($scope.templates, {
              id: result.template
            });
          }
        }).error(function(err) {
          return growl.danger('Error getting message: ', err);
        });
      });
      $scope.$watch('batch', function(batch) {
        var base, base1, batchName, ref, ref1, ref2, startTemplates;
        if (!batch) {
          return;
        }
        $scope.tempName = $scope.batch.name;
        if ((base = $scope.batch).html == null) {
          base.html = {};
        }
        if ((base1 = $scope.batch).text == null) {
          base1.text = {};
        }
        $scope.fileSuccess = false;
        if ((ref = $scope.batchform) != null) {
          ref.saved = false;
        }
        $scope.errors = [];
        if (((ref1 = batch.csv) != null ? ref1.name : void 0) != null) {
          batchName = batch.csv.name;
        } else {
          batchName = null;
        }
        if (batch.status === 'finished') {
          $scope.loadingSummary = true;
          $scope.loadingErrors = true;
          $scope.getSummary();
          $scope.getErrors();
        }
        $('.fileinput-filename').text(batchName);
        if (!$scope.$thumbnailsLoaded) {
          startTemplates = Date.now();
          _.forEach($scope.templates, function(template) {
            template.$thumbnailLoading = true;
            return $http.get('api/templates/' + template.id).then(function(res) {
              template.thumbnail = res.data.thumbnail;
              return $timeout(function() {
                template.$thumbnailLoading = false;
                return $scope.$thumbnailsLoaded = true;
              }, Math.max(0, 750 - (Date.now() - startTemplates)));
            });
          });
        }
        if ($scope.template = _.find($scope.templates, {
          id: (ref2 = $scope.batch) != null ? ref2.template : void 0
        })) {
          return _setTemplateDefaults();
        }
      });
      $scope.getSummary = function() {
        return $http.get('api/batches/report/' + $scope.batch.id).success(function(report) {
          $scope.summary = report;
          return $scope.loadingSummary = false;
        }).error(function(err) {
          growl.danger('Failed to get report ', err);
          return $scope.loadingSummary = false;
        });
      };
      $scope.getErrors = function() {
        return $http.get('api/batches/errors/' + $scope.batch.id).success(function(errors) {
          $scope.errors = errors;
          return $scope.loadingErrors = false;
        }).error(function(err) {
          growl.danger('Failed to get batch errors ', err);
          return $scope.loadingErrors = false;
        });
      };
      $scope.fileTempCheck = function(file, cb) {
        var checkHeaders, fd, i, item, len, reader, templateHeaders, variables;
        if (!(file && file.type === 'text/csv')) {
          cb(null);
        }
        $scope.$checking = true;
        fd = new FormData;
        fd.append('file', file);
        reader = new FileReader();
        templateHeaders = ['agent', 'recipient.email'];
        variables = $scope.template.meta.html.variables;
        for (i = 0, len = variables.length; i < len; i++) {
          item = variables[i];
          if (!(~item.name.indexOf('agent') || ~item.name.indexOf('sender') || ~item.name.indexOf('assets'))) {
            templateHeaders.push(item.name);
          }
        }
        reader.readAsText(file);
        checkHeaders = function(headers, templateHeaders) {
          var j, len1, value;
          $scope.fileError = false;
          $scope.headerErrors = [];
          $scope.fileSuccess = false;
          for (j = 0, len1 = templateHeaders.length; j < len1; j++) {
            value = templateHeaders[j];
            if (!(headers.indexOf(value) === -1)) {
              continue;
            }
            $('.fileinput').fileinput('clear');
            $scope.fileError = true;
            $scope.headerErrors.push(value);
            $scope.$checking = false;
            fd = void 0;
            $scope.$apply();
          }
          return cb(fd, reader);
        };
        return reader.onload = function() {
          var headers, readerSliced;
          readerSliced = reader.result.split(/[\r\n]+/g);
          headers = readerSliced[0].split(/\s*,\s*/g);
          return checkHeaders(headers, templateHeaders);
        };
      };
      $scope.trust = function(html) {
        return $sce.trustAsHtml(html);
      };
      done = function(start, cb) {
        var ref;
        if ((ref = $scope.batch.csv) != null ? ref.name : void 0) {
          $('.fileinput-filename').text($scope.batch.csv.name);
        }
        $scope.$checking = false;
        saveTimeout.begin(start, $scope.batchform);
        return typeof cb === "function" ? cb() : void 0;
      };
      httpPut = function(start, cb) {
        return $http.put('/api/batches/' + $scope.batch.id, $scope.batch).success(function(body) {
          $scope.batch.version = body.version;
          return done(start, cb);
        }).error(function(err) {
          $scope.errorMessage = err;
          return errorTimeout.begin($scope.batchform, start);
        });
      };
      httpPost = function(start, cb) {
        return $http.post('/api/batches', $scope.batch).success(function(body) {
          $scope.batch.id = body.id;
          $scope.batch.version = body.version;
          $scope.batch.status = 'pending';
          return done(start, cb);
        }).error(function(err) {
          $scope.errorMessage = err;
          return errorTimeout.begin($scope.batchform, start);
        });
      };
      $scope.save = function(cb) {
        var fd, ref, ref1, ref2;
        fd = new FormData;
        start = Date.now();
        $scope.batch["new"] = false;
        $scope.batch.hasChanged = true;
        $scope.errorMessage = void 0;
        $scope.batchform.submitted = false;
        $scope.batchform.saved = false;
        $scope.batchform.saving = true;
        $scope.message_id = null;
        if ($scope.batchform.$invalid || !$scope.batch.template) {
          return errorTimeout.begin($scope.batchform, start);
        } else {
          if (((ref = $scope.batch.csv) != null ? (ref1 = ref.file) != null ? ref1.type : void 0 : void 0) === 'text/csv') {
            return $scope.fileTempCheck((ref2 = $scope.batch.csv) != null ? ref2.file : void 0, function(fd, reader) {
              var ref3;
              if (!$scope.fileError) {
                if (fd) {
                  fd.append('name', $scope.batch.csv.file.name);
                  if (((ref3 = $scope.batch.csv) != null ? ref3.file : void 0) != null) {
                    fd.append('file', $scope.batch.csv.file);
                  }
                  $scope.batch.csv.name = $scope.batch.csv.file.name;
                  $scope.batch.file = reader != null ? reader.result : void 0;
                }
                if ($scope.batch.id) {
                  return httpPut(start, cb);
                } else {
                  return httpPost(start, cb);
                }
              } else {
                return errorTimeout.begin($scope.batchform, start);
              }
            });
          } else {
            delete $scope.batch.file;
            if ($scope.batch.id) {
              return httpPut(start, cb);
            } else {
              return httpPost(start, cb);
            }
          }
        }
      };
      $scope.setSticky = function(batch) {
        if (batch.sticky) {
          $scope.sticky.push(batch);
          _.pull($scope.recent, batch);
        } else {
          $scope.recent.push(batch);
          _.pull($scope.sticky, batch);
        }
        return $scope.save();
      };
      $scope.expand = function(type) {
        if ($scope.expanded[type] === true) {
          return $scope.expanded[type] = false;
        } else {
          return $scope.expanded[type] = true;
        }
      };
      $scope.change = function() {
        return delete $scope.batch.csv;
      };
      $scope.cancelable = function() {
        var batch;
        if (!(batch = $scope.batch)) {
          return false;
        }
        return batch.status === 'deployed';
      };
      $scope.archivable = function() {
        var batch;
        if (!(batch = $scope.batch)) {
          return false;
        }
        return batch.status === 'finished';
      };
      $scope.editable = function() {
        var batch, ref;
        if (!(batch = $scope.batch)) {
          return true;
        }
        return (ref = batch.status) !== 'archived' && ref !== 'failed' && ref !== 'finished';
      };
      $scope.deployable = function() {
        var batch, ref;
        if (!(batch = $scope.batch)) {
          return false;
        }
        return batch.status === 'pending' && ((ref = batch.csv) != null ? ref.name : void 0) && $scope.previewable();
      };
      $scope.previewable = function() {
        var batch, ref;
        if (!(batch = $scope.batch)) {
          return false;
        }
        return ((ref = $scope.batchform) != null ? ref.$pristine : void 0) && batch.subject && batch.template;
      };
      $scope.cancel = function(batch) {
        var inst;
        if (batch != null) {
          $scope.batch = batch;
        }
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal',
          resolve: {
            type: function() {
              return "Cancel";
            }
          }
        });
        return inst.result.then(function() {
          return $http.post('/api/batches/' + $scope.batch.id + '/cancel').success(function(data) {
            $scope.batch.version = data.version;
            $scope.batch.status = 'pending';
            $scope.batches.push($scope.batch);
            $scope.deployed.splice($scope.deployed.indexOf($scope.batch), 1);
            return $scope.batchform.$setPristine();
          });
        });
      };
      $scope.deploy = function() {
        return $http.post('/api/batches/' + $scope.batch.id + '/deploy').success(function(data) {
          $scope.batch.version = data.version;
          if ($scope.batch.schedule.now) {
            $scope.batch.status = 'finished';
            $scope.recent.unshift($scope.batch);
            $scope.batches.splice($scope.batches.indexOf($scope.batch), 1);
          } else {
            $scope.batch.status = 'deployed';
            $scope.deployed.push($scope.batch);
            $scope.batches.splice($scope.batches.indexOf($scope.batch), 1);
          }
          return $scope.batchform.$setPristine();
        });
      };
      $scope.clone = function() {
        return $http.post("api/batches/" + $scope.batch.id + "/clone").success(function(batch) {
          batch.status = 'pending';
          $scope.batches.push(batch);
          $state.go('.', {
            batch: batch.id
          });
          $scope.batchform.$setPristine();
          return growl.success('Cloned!');
        });
      };
      $scope.archive = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal',
          resolve: {
            type: function() {
              return "Archive";
            }
          }
        });
        return inst.result.then(function() {
          $scope.batch.status = 'archived';
          return $scope.save(function() {
            $scope.recent.splice($scope.recent.indexOf($scope.batch), 1);
            return $scope.batch = void 0;
          });
        });
      };
      $scope["delete"] = function(batch) {
        var inst;
        if (batch != null) {
          $scope.batch = batch;
        }
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal',
          resolve: {
            type: function() {
              return "Delete";
            }
          }
        });
        return inst.result.then(function() {
          return $http["delete"]('/api/batches/' + $scope.batch.id).success(function() {
            $scope.batches.splice($scope.batches.indexOf($scope.batch), 1);
            $scope.batch = void 0;
            return growl.success('Deleted!');
          }).error(function(err) {
            return growl.danger('Delete failed ' + err);
          });
        });
      };
      $scope.select = function(template) {
        if (!$scope.editable()) {
          return;
        }
        $scope.template = template;
        $scope.batch.template = template.id;
        return _setTemplateDefaults();
      };
      _setTemplateDefaults = function() {
        var base, i, j, k, len, len1, len2, name, part, piece, ref, ref1, ref2, ref3, ref4, ref5, type;
        ref = ['html', 'text'];
        for (i = 0, len = ref.length; i < len; i++) {
          type = ref[i];
          ref1 = ['blocks', 'links', 'policies'];
          for (j = 0, len1 = ref1.length; j < len1; j++) {
            part = ref1[j];
            if ((ref2 = $scope.batch[type]) != null) {
              if (ref2[part] == null) {
                ref2[part] = {};
              }
            }
            ref4 = ((ref3 = $scope.template.meta[type]) != null ? ref3[part] : void 0) || [];
            for (k = 0, len2 = ref4.length; k < len2; k++) {
              piece = ref4[k];
              if ((base = $scope.batch[type][part])[name = piece.name] == null) {
                base[name] = part === 'links' ? {
                  type: ''
                } : '';
              }
            }
          }
        }
        return $scope.nav.html.selectedBlock = (ref5 = $scope.template.meta.html.blocks[0]) != null ? ref5.name : void 0;
      };
      return $scope.preview = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'preview-modal.html',
          controller: 'BatchPreviewModal',
          resolve: {
            template: function() {
              return $scope.template;
            }
          }
        });
        return inst.result.then(function(preview) {
          preview.type = 'batch';
          preview.batch = $scope.batch.id;
          return $http.post('api/send', preview);
        });
      };
    }
  ]).controller('BatchPreviewModal', [
    '$scope', '$http', '$uibModalInstance', 'template', function($scope, $http, $uibModalInstance, template) {
      var _setter, combo;
      _setter = function(obj, prop, val) {
        var final, p, props;
        props = prop.split('.');
        final = props.pop();
        while (p = props.shift()) {
          if (typeof obj[p] === 'undefined') {
            obj[p] = {};
          }
          obj = obj[p];
        }
        if (val) {
          return obj[final] = val;
        } else {
          return obj[final];
        }
      };
      $scope.preview = {};
      $scope.custom = {};
      $scope.template = template;
      combo = [].concat(template.meta.html.variables, template.meta.text.variables);
      $scope.variables = _.uniq(_.remove(_.pluck(combo, 'name'), function(v) {
        return !~v.indexOf('assets.') && !~v.indexOf('sender.') && !~v.indexOf('agent.') && !~v.indexOf('lead.attributes.') && v !== 'unsub';
      }));
      $http.get('api/attributes?q=active').success(function(attributes) {
        $scope.attributes = attributes;
        return _.forEach($scope.attributes, function(attribute) {
          attribute._name = 'lead.attributes.' + attribute.name.replace(/\s/g, '').toLowerCase();
          $scope.attribute_variables = _.uniq(_.remove(_.pluck(combo, 'name'), function(a) {
            return !!~a.indexOf('lead.attributes.');
          }));
          $scope.filtered_attributes = [];
          return _.forEach($scope.attributes, function(attribute) {
            return _.forEach($scope.attribute_variables, function(av) {
              if (av === attribute._name) {
                return $scope.filtered_attributes.push(attribute);
              }
            });
          });
        });
      });
      $http.get('api/senders?q=active').success(function(senders) {
        return $scope.senders = _.pluck(senders, 'email');
      });
      return $scope.trigger = function() {
        _.forEach([$scope.variables, $scope.attribute_variables], function(v) {
          var i, len, path, ref, results;
          ref = v || [];
          results = [];
          for (i = 0, len = ref.length; i < len; i++) {
            path = ref[i];
            results.push(_setter($scope.preview, path, $scope.custom[path]));
          }
          return results;
        });
        return $uibModalInstance.close($scope.preview);
      };
    }
  ]);

}).call(this);
