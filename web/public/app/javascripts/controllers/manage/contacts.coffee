isValidEmailAddress = (email) ->
  atidx = email.indexOf '@'
  email.length >= 3 && ~atidx && atidx < email.length - 1

angular.module('app.manage')
  .config [
    '$stateProvider', '$urlRouterProvider',
    ($stateProvider, $urlRouterProvider) ->
      $stateProvider

        .state 'manage-contacts',
          data: section: 'manage'
          templateUrl: '/html/manage/contacts.html'
          url: '/manage/contacts'
          controller: 'ContactInfo'

        .state 'manage-contacts.edit',
          url: '/:id'
        .state 'manage-contacts.new',
          url: '/:id'
  ]

  .factory 'Contacts', ['$resource', ($resource) ->
    $resource 'api/leads'
  ]

  .controller 'ContactInfo', [
    '$scope', '$state', '$http', '$uibModal', '$q', '$timeout', 'growl', 'showLead', 'showAttribute',
    ($scope, $state, $http, $uibModal, $q, $timeout, growl, showLead, showAttribute) ->
      $scope.form = {}
      $scope.contact = undefined
      $scope.senders = []
      $scope.activeStatuses = {}
      $scope.showAttribute = showAttribute
      $scope.showLead = showLead

      # if the search form is cleared out then reset details
      $scope.$watch 'form.contactSearch', (contactSearch) ->
        unless contactSearch
          $scope.details = {}
          $scope.searchedLeads = undefined
          $scope.searched = no

      urls = ['statuses', 'attributes?q=active', 'senders?q=active', 'contacts/settings']
      $q.all _.map(urls, (url) -> $http.get "/api/#{url}")
      .then ([statuses, attributes, senders, fields]) ->
        $scope.statuses = _.indexBy statuses.data, 'id'
        $scope.attributes = attributes.data
        $scope.fields = fields.data
        $scope.attrib_aliases = {}
        $scope.compare = {}
        attribs = {}

        _.map senders.data, (sender) ->
          $scope.senders.push sender.email

        _.transform $scope.attributes, (memo, group) ->
          group.short = group.name.replace(/\s*/g, '').toLowerCase()
          memo[group.short] = _.sortBy(group.values, 'name')[0].name
          attribs[group.short] = []
          for option in group.values
            attribs[group.short].push option.name
            $scope.compare[option.name] = option.name
            for alias in option.aliases
              attribs[group.short].push alias
              $scope.compare[alias] = option.name
            $scope.attrib_aliases[group.short] = _.uniq attribs[group.short]
        , {}

        for key, status of $scope.statuses
          unless not status.active
            $scope.activeStatuses[status.name] = status
            if status.aliases?
              for alias in status.aliases
                $scope.activeStatuses[alias] = status

        _($scope.statuses)
          .forEach (status) ->
            status.segments = _(status.segments)
              .indexBy 'id'
              .forEach (segment) ->
                segment.steps = _(segment.steps)
                  .sortBy 'delay.delayInHours'
                  .indexBy 'id'
                  .value()
              .value()
          .value()

      $scope.groupStatus = (item) ->
        item.value.name + ' options:'

      $scope.groupAttribute = (item) ->
        $scope.compare[item] + ' options:'

      $scope.hasPhones = (lead) ->
        unless not lead.recipient.phones
          Object.keys(lead.recipient.phones).length > 0

      # get leads based on email. pass data back in callback
      $scope.getLeads = (email, cb) ->
        $http.get('api/search/leads/' + email)
          .success (leads) ->
            if leads.length
              cb leads, yes
            else
              cb leads, notfound: yes

          .error ->
            $scope.forms.contactSearch.$submitting = no
            $scope.searched = notfound: yes

      # main search function. calls getLeads and sets data in scope variables
      $scope.search = ->
        $scope.searched = no
        $scope.forms.contactSearch.$submitting = yes
        $scope.loadContacts = yes
        start = Date.now()

        email = $.trim $scope.form.contactSearch
        if email is '' or not isValidEmailAddress email
          $scope.forms.contactSearch.$submitting = no
          $scope.loadContacts = no
          return

        $scope.getLeads email, (leads, details) ->
          $scope.searchedLeads = leads
          $scope.expand = []
          $timeout ->
            $scope.searched = details
            $scope.forms.contactSearch.$submitting = no
            $scope.loadContacts = no
          , Math.max 0, 800 - (Date.now() - start)

      $scope.$on '$stateChangeSuccess', ->
        $scope.contact = undefined

        {id} = $state.params

        return unless id?

        if id is 'new'
          $scope.contact = {}
        else
          $http.get 'api/leads/' + id
          .success (contact) ->
            $scope.contact = contact
          .error (err) ->
            growl.danger 'Error getting contact: ', err

      $scope.newContact = ->
        $state.go 'manage-contacts.new', id: 'new'

      $scope.deleteContact = (lead) ->
        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'
          resolve:
            type: -> lead.recipient.email
            scopeItem: -> null
            refItem: -> null

        inst.result.then ->
          lead.active = no
          $http.put '/api/leads/' + lead.id, lead
          .error (err) ->
            growl.danger "Error deleting contact #{lead.recipient.email} " + err
          .success (result) ->
            growl.success 'Deleted!'
            _.remove $scope.leads, lead
            $scope.inactiveContacts.push lead

      $scope.showInactive = ->
        $http.get('api/leads?q=all').success (results) ->
          $scope.inactiveContacts = []
          for result in results
            if result.active is false
              $scope.inactiveContacts.push result
        $http.get 'api/leads?q=active'
        .success (result) ->
          $scope.leads = result
        .error (err) ->
          growl.danger 'Failed to load contacts. ' + err

      $scope.makeActive = (lead) ->
        inst = $uibModal.open
          templateUrl: 'reactivate-modal.html'
          controller: 'ConfirmModal'
          backdrop: 'static'
          scope: _.assign $scope.$new(), name: lead.recipient.email

        inst.result.then ->
          lead.active = true
          $http.put 'api/leads/' + lead.id, lead
          .success ->
            angular.copy lead
            $scope.inactiveContacts = _.reject $scope.inactiveContacts, (s) -> s.id is lead.id
            $scope.showInactive()
          .error (err) ->
            growl.danger 'Failed to save contact. ' + err

      $scope.editProfiles = ->
        inst = $uibModal.open
          templateUrl: 'profile-modal.html'
          controller: 'ProfileModal'
          resolve: url: -> 'api/contacts/settings'

        inst.result.then ->
          $http.get 'api/contacts/settings'
          .success (fields) ->
            $scope.fields = fields
          .error (err) ->
            growl.danger 'Error saving custom variables: ', err
  ]

  .controller 'ContactPage',
  ['$scope', '$http', '$uibModal', '$q', '$rootScope', '$timeout',  'dismissCheck',
  'saveTimeout', 'errorTimeout', 'checkLocation', 'growl',
  ($scope, $http, $uibModal, $q, $rootScope, $timeout,  dismissCheck, saveTimeout,
  errorTimeout, checkLocation, growl) ->
    lead_ref = {}
    $scope.forms = {}
    $scope.forms.contact = saving: no
    angular.copy $scope.contact, lead_ref
    $scope.addit = no
    $scope.reqAlert = no
    $scope.$loading = yes
    start = Date.now()
    $scope.result = undefined
    $scope.running = no

    runCheck = ->
      total = 0
      for attribute of $scope.contact.attributes
        total += 1

      return unless total is $rootScope.system.attributes
      $scope.check_attributes()

    $scope.$watchCollection 'contact.attributes',  (attributes) ->
      return unless attributes? and $scope.contact.status?
      runCheck()

    $scope.$watchCollection 'contact.status', (status) ->
      return unless $scope.contact.attributes and status
      runCheck()

    $scope.check_attributes = ->
      $scope.running = yes
      $http.put '/api/check/attribute', status: $scope.contact.status, attributes: $scope.contact.attributes
      .error (err) ->
        $scope.result = 'error'
        $scope.running = no
      .success (result) ->
        if result.result is true
          $scope.result = 'success'
        else
          $scope.result = 'error'
        $scope.running = no

    $timeout ->
      # if required custom values, show alert.
      for field in $scope.fields
        if field.required
          $scope.reqAlert = yes

      $scope.$loading = no
    , Math.max 0, 800 - (Date.now() - start)

    $scope.saveCheck = ->
      if (lead_ref.status isnt $scope.contact.status) and $scope.contact.id?
        inst = $uibModal.open
          templateUrl: 'save-check.html'
          controller: 'SaveCheckModal'
          backdrop: 'static'

        inst.result.then ->
          $scope.save yes
      else
        $scope.save no

    $scope.additional = ->
      if not $scope.addit
        $scope.addit = yes
      else
        $scope.addit = no

    $scope.generateLeadData = ->
      $scope.contact =
        recipient:
          firstname: firstName = faker.name.firstName()
          lastname: lastName = faker.name.lastName()
          email: faker.internet.email firstName, lastName
          address:
            line1: faker.address.streetAddress()
            city: faker.address.city()
            state: faker.address.stateAbbr()
            zip: faker.address.zipCode()
          phones:
            home: faker.phone.phoneNumberFormat()
            mobile: faker.phone.phoneNumberFormat()
            work: faker.phone.phoneNumberFormat()

    errored = (err) ->
      $scope.errorMessage = err
      errorTimeout.begin $scope.forms.contact, start

    saved = (start, resegmented) ->
      if resegmented
        $timeout ->
          $http.get 'api/leads/' + $scope.contact.id
          .success (contact) ->
            $scope.contact = contact
            angular.copy $scope.contact, lead_ref
            saveTimeout.begin start, $scope.forms.contact
          .error (err) ->
            growl.danger 'Error getting updated contact: ', err
        , Math.max 0, 1500 - (Date.now() - start)
      else
        angular.copy $scope.contact, lead_ref
        saveTimeout.begin start, $scope.forms.contact

    $scope.save = (resegmented) ->
      start = Date.now()
      $scope.errorMessage = undefined
      $scope.forms.contact.saved = no
      $scope.forms.contact.saving = yes
      $scope.forms.contact.submitted = no

      if $scope.forms.contact.$invalid
        errorTimeout.begin $scope.forms.contact, start
      else if $scope.contact.id
        $http.put '/api/leads/' + $scope.contact.id, $scope.contact
        .error (err) ->
          errored err
        .success (data) ->
          $scope.contact.version = data.version
          saved start, resegmented
      else
        $http.post '/api/leads/', $scope.contact
        .error (err) ->
          errored err
        .success (data) ->
          _.assign $scope.contact, data
          saved start, resegmented
  ]

  .controller 'SaveCheckModal', ['$scope', '$uibModal', '$uibModalInstance', 'growl',
  ($scope, $uibModal, $uibModalInstance, growl) ->
    $scope.confirm = -> $uibModalInstance.close()
    $scope.dismiss = -> $uibModalInstance.dismiss()
  ]
