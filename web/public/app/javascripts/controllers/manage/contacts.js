(function() {
  var isValidEmailAddress;

  isValidEmailAddress = function(email) {
    var atidx;
    atidx = email.indexOf('@');
    return email.length >= 3 && ~atidx && atidx < email.length - 1;
  };

  angular.module('app.manage').config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      return $stateProvider.state('manage-contacts', {
        data: {
          section: 'manage'
        },
        templateUrl: '/html/manage/contacts.html',
        url: '/manage/contacts',
        controller: 'ContactInfo'
      }).state('manage-contacts.edit', {
        url: '/:id'
      }).state('manage-contacts.new', {
        url: '/:id'
      });
    }
  ]).factory('Contacts', [
    '$resource', function($resource) {
      return $resource('api/leads');
    }
  ]).controller('ContactInfo', [
    '$scope', '$state', '$http', '$uibModal', '$q', '$timeout', 'growl', 'showLead', 'showAttribute', function($scope, $state, $http, $uibModal, $q, $timeout, growl, showLead, showAttribute) {
      var urls;
      $scope.form = {};
      $scope.contact = void 0;
      $scope.senders = [];
      $scope.activeStatuses = {};
      $scope.showAttribute = showAttribute;
      $scope.showLead = showLead;
      $scope.$watch('form.contactSearch', function(contactSearch) {
        if (!contactSearch) {
          $scope.details = {};
          $scope.searchedLeads = void 0;
          return $scope.searched = false;
        }
      });
      urls = ['statuses', 'attributes?q=active', 'senders?q=active', 'contacts/settings'];
      $q.all(_.map(urls, function(url) {
        return $http.get("/api/" + url);
      })).then(function(arg) {
        var alias, attribs, attributes, fields, i, key, len, ref, ref1, senders, status, statuses;
        statuses = arg[0], attributes = arg[1], senders = arg[2], fields = arg[3];
        $scope.statuses = _.indexBy(statuses.data, 'id');
        $scope.attributes = attributes.data;
        $scope.fields = fields.data;
        $scope.attrib_aliases = {};
        $scope.compare = {};
        attribs = {};
        _.map(senders.data, function(sender) {
          return $scope.senders.push(sender.email);
        });
        _.transform($scope.attributes, function(memo, group) {
          var alias, i, j, len, len1, option, ref, ref1, results1;
          group.short = group.name.replace(/\s*/g, '').toLowerCase();
          memo[group.short] = _.sortBy(group.values, 'name')[0].name;
          attribs[group.short] = [];
          ref = group.values;
          results1 = [];
          for (i = 0, len = ref.length; i < len; i++) {
            option = ref[i];
            attribs[group.short].push(option.name);
            $scope.compare[option.name] = option.name;
            ref1 = option.aliases;
            for (j = 0, len1 = ref1.length; j < len1; j++) {
              alias = ref1[j];
              attribs[group.short].push(alias);
              $scope.compare[alias] = option.name;
            }
            results1.push($scope.attrib_aliases[group.short] = _.uniq(attribs[group.short]));
          }
          return results1;
        }, {});
        ref = $scope.statuses;
        for (key in ref) {
          status = ref[key];
          if (!!status.active) {
            $scope.activeStatuses[status.name] = status;
            if (status.aliases != null) {
              ref1 = status.aliases;
              for (i = 0, len = ref1.length; i < len; i++) {
                alias = ref1[i];
                $scope.activeStatuses[alias] = status;
              }
            }
          }
        }
        return _($scope.statuses).forEach(function(status) {
          return status.segments = _(status.segments).indexBy('id').forEach(function(segment) {
            return segment.steps = _(segment.steps).sortBy('delay.delayInHours').indexBy('id').value();
          }).value();
        }).value();
      });
      $scope.groupStatus = function(item) {
        return item.value.name + ' options:';
      };
      $scope.groupAttribute = function(item) {
        return $scope.compare[item] + ' options:';
      };
      $scope.hasPhones = function(lead) {
        if (!!lead.recipient.phones) {
          return Object.keys(lead.recipient.phones).length > 0;
        }
      };
      $scope.getLeads = function(email, cb) {
        return $http.get('api/search/leads/' + email).success(function(leads) {
          if (leads.length) {
            return cb(leads, true);
          } else {
            return cb(leads, {
              notfound: true
            });
          }
        }).error(function() {
          $scope.forms.contactSearch.$submitting = false;
          return $scope.searched = {
            notfound: true
          };
        });
      };
      $scope.search = function() {
        var email, start;
        $scope.searched = false;
        $scope.forms.contactSearch.$submitting = true;
        $scope.loadContacts = true;
        start = Date.now();
        email = $.trim($scope.form.contactSearch);
        if (email === '' || !isValidEmailAddress(email)) {
          $scope.forms.contactSearch.$submitting = false;
          $scope.loadContacts = false;
          return;
        }
        return $scope.getLeads(email, function(leads, details) {
          $scope.searchedLeads = leads;
          $scope.expand = [];
          return $timeout(function() {
            $scope.searched = details;
            $scope.forms.contactSearch.$submitting = false;
            return $scope.loadContacts = false;
          }, Math.max(0, 800 - (Date.now() - start)));
        });
      };
      $scope.$on('$stateChangeSuccess', function() {
        var id;
        $scope.contact = void 0;
        id = $state.params.id;
        if (id == null) {
          return;
        }
        if (id === 'new') {
          return $scope.contact = {};
        } else {
          return $http.get('api/leads/' + id).success(function(contact) {
            return $scope.contact = contact;
          }).error(function(err) {
            return growl.danger('Error getting contact: ', err);
          });
        }
      });
      $scope.newContact = function() {
        return $state.go('manage-contacts.new', {
          id: 'new'
        });
      };
      $scope.deleteContact = function(lead) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal',
          resolve: {
            type: function() {
              return lead.recipient.email;
            },
            scopeItem: function() {
              return null;
            },
            refItem: function() {
              return null;
            }
          }
        });
        return inst.result.then(function() {
          lead.active = false;
          return $http.put('/api/leads/' + lead.id, lead).error(function(err) {
            return growl.danger(("Error deleting contact " + lead.recipient.email + " ") + err);
          }).success(function(result) {
            growl.success('Deleted!');
            _.remove($scope.leads, lead);
            return $scope.inactiveContacts.push(lead);
          });
        });
      };
      $scope.showInactive = function() {
        $http.get('api/leads?q=all').success(function(results) {
          var i, len, result, results1;
          $scope.inactiveContacts = [];
          results1 = [];
          for (i = 0, len = results.length; i < len; i++) {
            result = results[i];
            if (result.active === false) {
              results1.push($scope.inactiveContacts.push(result));
            } else {
              results1.push(void 0);
            }
          }
          return results1;
        });
        return $http.get('api/leads?q=active').success(function(result) {
          return $scope.leads = result;
        }).error(function(err) {
          return growl.danger('Failed to load contacts. ' + err);
        });
      };
      $scope.makeActive = function(lead) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'reactivate-modal.html',
          controller: 'ConfirmModal',
          backdrop: 'static',
          scope: _.assign($scope.$new(), {
            name: lead.recipient.email
          })
        });
        return inst.result.then(function() {
          lead.active = true;
          return $http.put('api/leads/' + lead.id, lead).success(function() {
            angular.copy(lead);
            $scope.inactiveContacts = _.reject($scope.inactiveContacts, function(s) {
              return s.id === lead.id;
            });
            return $scope.showInactive();
          }).error(function(err) {
            return growl.danger('Failed to save contact. ' + err);
          });
        });
      };
      return $scope.editProfiles = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'profile-modal.html',
          controller: 'ProfileModal',
          resolve: {
            url: function() {
              return 'api/contacts/settings';
            }
          }
        });
        return inst.result.then(function() {
          return $http.get('api/contacts/settings').success(function(fields) {
            return $scope.fields = fields;
          }).error(function(err) {
            return growl.danger('Error saving custom variables: ', err);
          });
        });
      };
    }
  ]).controller('ContactPage', [
    '$scope', '$http', '$uibModal', '$q', '$rootScope', '$timeout', 'dismissCheck', 'saveTimeout', 'errorTimeout', 'checkLocation', 'growl', function($scope, $http, $uibModal, $q, $rootScope, $timeout, dismissCheck, saveTimeout, errorTimeout, checkLocation, growl) {
      var errored, lead_ref, runCheck, saved, start;
      lead_ref = {};
      $scope.forms = {};
      $scope.forms.contact = {
        saving: false
      };
      angular.copy($scope.contact, lead_ref);
      $scope.addit = false;
      $scope.reqAlert = false;
      $scope.$loading = true;
      start = Date.now();
      $scope.result = void 0;
      $scope.running = false;
      runCheck = function() {
        var attribute, total;
        total = 0;
        for (attribute in $scope.contact.attributes) {
          total += 1;
        }
        if (total !== $rootScope.system.attributes) {
          return;
        }
        return $scope.check_attributes();
      };
      $scope.$watchCollection('contact.attributes', function(attributes) {
        if (!((attributes != null) && ($scope.contact.status != null))) {
          return;
        }
        return runCheck();
      });
      $scope.$watchCollection('contact.status', function(status) {
        if (!($scope.contact.attributes && status)) {
          return;
        }
        return runCheck();
      });
      $scope.check_attributes = function() {
        $scope.running = true;
        return $http.put('/api/check/attribute', {
          status: $scope.contact.status,
          attributes: $scope.contact.attributes
        }).error(function(err) {
          $scope.result = 'error';
          return $scope.running = false;
        }).success(function(result) {
          if (result.result === true) {
            $scope.result = 'success';
          } else {
            $scope.result = 'error';
          }
          return $scope.running = false;
        });
      };
      $timeout(function() {
        var field, i, len, ref;
        ref = $scope.fields;
        for (i = 0, len = ref.length; i < len; i++) {
          field = ref[i];
          if (field.required) {
            $scope.reqAlert = true;
          }
        }
        return $scope.$loading = false;
      }, Math.max(0, 800 - (Date.now() - start)));
      $scope.saveCheck = function() {
        var inst;
        if ((lead_ref.status !== $scope.contact.status) && ($scope.contact.id != null)) {
          inst = $uibModal.open({
            templateUrl: 'save-check.html',
            controller: 'SaveCheckModal',
            backdrop: 'static'
          });
          return inst.result.then(function() {
            return $scope.save(true);
          });
        } else {
          return $scope.save(false);
        }
      };
      $scope.additional = function() {
        if (!$scope.addit) {
          return $scope.addit = true;
        } else {
          return $scope.addit = false;
        }
      };
      $scope.generateLeadData = function() {
        var firstName, lastName;
        return $scope.contact = {
          recipient: {
            firstname: firstName = faker.name.firstName(),
            lastname: lastName = faker.name.lastName(),
            email: faker.internet.email(firstName, lastName),
            address: {
              line1: faker.address.streetAddress(),
              city: faker.address.city(),
              state: faker.address.stateAbbr(),
              zip: faker.address.zipCode()
            },
            phones: {
              home: faker.phone.phoneNumberFormat(),
              mobile: faker.phone.phoneNumberFormat(),
              work: faker.phone.phoneNumberFormat()
            }
          }
        };
      };
      errored = function(err) {
        $scope.errorMessage = err;
        return errorTimeout.begin($scope.forms.contact, start);
      };
      saved = function(start, resegmented) {
        if (resegmented) {
          return $timeout(function() {
            return $http.get('api/leads/' + $scope.contact.id).success(function(contact) {
              $scope.contact = contact;
              angular.copy($scope.contact, lead_ref);
              return saveTimeout.begin(start, $scope.forms.contact);
            }).error(function(err) {
              return growl.danger('Error getting updated contact: ', err);
            });
          }, Math.max(0, 1500 - (Date.now() - start)));
        } else {
          angular.copy($scope.contact, lead_ref);
          return saveTimeout.begin(start, $scope.forms.contact);
        }
      };
      return $scope.save = function(resegmented) {
        start = Date.now();
        $scope.errorMessage = void 0;
        $scope.forms.contact.saved = false;
        $scope.forms.contact.saving = true;
        $scope.forms.contact.submitted = false;
        if ($scope.forms.contact.$invalid) {
          return errorTimeout.begin($scope.forms.contact, start);
        } else if ($scope.contact.id) {
          return $http.put('/api/leads/' + $scope.contact.id, $scope.contact).error(function(err) {
            return errored(err);
          }).success(function(data) {
            $scope.contact.version = data.version;
            return saved(start, resegmented);
          });
        } else {
          return $http.post('/api/leads/', $scope.contact).error(function(err) {
            return errored(err);
          }).success(function(data) {
            _.assign($scope.contact, data);
            return saved(start, resegmented);
          });
        }
      };
    }
  ]).controller('SaveCheckModal', [
    '$scope', '$uibModal', '$uibModalInstance', 'growl', function($scope, $uibModal, $uibModalInstance, growl) {
      $scope.confirm = function() {
        return $uibModalInstance.close();
      };
      return $scope.dismiss = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]);

}).call(this);
