angular.module('app.manage' )
  .directive 'messageLibraryPanel', [
    'ContextMenuService',
    (contextMenuService) ->
      scope:
        title   :  '@'
        copy    :   '@'
      templateUrl: 'message-panel.html'
  ]
  .directive 'messageSubjectEditor', [
    'ContextMenuService',
    (contextMenuService) ->
      controller: 'MessageLibrarySubjectsCtrl'
      templateUrl: 'message-library-subject-template.html'
  ]

  .directive 'messageLinksEditor', [
    'ContextMenuService',
    (contextMenuService) ->
      controller: 'MessageLibraryLinksCtrl'
      templateUrl: 'message-library-links-template.html'
  ]

  .directive 'messageHtmlBlocksEditor', [
    'ContextMenuService',
    (contextMenuService) ->
      controller: 'MessageLibraryHtmlBlocksCtrl'
      templateUrl: 'message-library-html-template.html'
  ]

  .directive 'messagePlainTextEditor', [
    'ContextMenuService',
    (contextMenuService) ->
      controller: 'MessageLibraryPlainTextCtrl'
      templateUrl: 'message-library-plaintext-template.html'
  ]

  .directive 'messagePoliciesEditor', [
    'ContextMenuService',
    (contextMenuService) ->
      controller: 'MessageLibraryPoliciesCtrl'
      templateUrl: 'message-library-policies-template.html'
  ]

  .directive 'messagePreviewEditor', [
    'ContextMenuService',
    (contextMenuService) ->
      controller: 'MessageLibraryPreviewCtrl'
      templateUrl: 'message-library-preview-template.html'
  ]
