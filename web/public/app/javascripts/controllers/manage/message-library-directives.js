(function() {
  angular.module('app.manage').directive('messageLibraryPanel', [
    'ContextMenuService', function(contextMenuService) {
      return {
        scope: {
          title: '@',
          copy: '@'
        },
        templateUrl: 'message-panel.html'
      };
    }
  ]).directive('messageSubjectEditor', [
    'ContextMenuService', function(contextMenuService) {
      return {
        controller: 'MessageLibrarySubjectsCtrl',
        templateUrl: 'message-library-subject-template.html'
      };
    }
  ]).directive('messageLinksEditor', [
    'ContextMenuService', function(contextMenuService) {
      return {
        controller: 'MessageLibraryLinksCtrl',
        templateUrl: 'message-library-links-template.html'
      };
    }
  ]).directive('messageHtmlBlocksEditor', [
    'ContextMenuService', function(contextMenuService) {
      return {
        controller: 'MessageLibraryHtmlBlocksCtrl',
        templateUrl: 'message-library-html-template.html'
      };
    }
  ]).directive('messagePlainTextEditor', [
    'ContextMenuService', function(contextMenuService) {
      return {
        controller: 'MessageLibraryPlainTextCtrl',
        templateUrl: 'message-library-plaintext-template.html'
      };
    }
  ]).directive('messagePoliciesEditor', [
    'ContextMenuService', function(contextMenuService) {
      return {
        controller: 'MessageLibraryPoliciesCtrl',
        templateUrl: 'message-library-policies-template.html'
      };
    }
  ]).directive('messagePreviewEditor', [
    'ContextMenuService', function(contextMenuService) {
      return {
        controller: 'MessageLibraryPreviewCtrl',
        templateUrl: 'message-library-preview-template.html'
      };
    }
  ]);

}).call(this);
