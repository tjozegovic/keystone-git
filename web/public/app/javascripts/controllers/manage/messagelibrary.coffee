angular.module('app.manage' )
  .config [
    '$stateProvider', '$urlRouterProvider',
    ($stateProvider, $urlRouterProvider) ->
      $stateProvider

        .state 'manage-messagelibrary',
          data: section: 'manage'
          templateUrl: '/html/manage/message-library.html'
          url: '/manage/message-library'
        .state 'manage-messagelibrary.message',
          url: '/:message'
        .state 'manage-messagelibrary.message.version',
          url: '/:version'
        .state 'manage-messagelibrary.message.version.state',
          url: '/:state'

        .state 'manage-messagedripmanager',
          data: section: 'manage'
          templateUrl: '/html/manage/message-drip-manager.html'
          url: '/manage/message-drip-manager'
        .state 'manage-messagedripmanager.status',
          url: '/:status'
        .state 'manage-messagedripmanager.status.segment',
          url: '/:segment'
  ]

  .service 'MessageLibrarySrv',['$http', '$stateParams', '$state', '$q',
    ($http, $stateParams, $state, $q, httpRequest) ->
      currentMessageTemplateBeingEdited =
        message : {}
      currentVersion = {}
      currentTemplate = undefined
      clientAttributes = []
      condensedAttributes = []
      clientLandingPages = []
      clientPolicies = []
      versionsWithGeneric = []
      completedMessagesInLibrary = []
      draftMessagesInLibrary = []
      messagesTemplatesTemp = []
      wizardPosition = null
      messageLibraryIsLoading = false
      messageProgress = 'draft'
      messageSlugName = ''
      breadcrumbStatus = undefined
      breadcrumbSegment = undefined
      messagesInCampaigns = {}
      currentStatuses = {}
      currentOrder = []

      that = this

      this.loadMessages = (override = false) ->
        if override
          messageLibraryIsLoading = false
        if messageLibraryIsLoading == false
          messageLibraryIsLoading = true

          urls = ['templates?q=active', 'messages?q=all', 'attributes?q=active', 'policies', 'pages', 'statuses?q=active']
          $q.all _.map(urls, (url) -> $http.get "/api/#{url}")
          .then ([templates, messages, attributes, policies, pages, statuses]) ->
            completedMessagesInLibrary = []
            draftMessagesInLibrary = []
            messagesTemplatesTemp = templates.data

            clientAttributes = attributes.data
            for a in attributes.data
              for b in a.values
                condensedAttributes.push  (attributeId: b.id, attributeName: b.name)

            currentStatuses = _.cloneDeep statuses.data
            currentOrderTemp = []
            _.forIn currentStatuses, (value) ->
              if _.indexOf(currentOrderTemp, value.id ) == -1
                currentOrderTemp.push value.id
            currentOrder = _.cloneDeep currentOrderTemp

            messagesInCampaignsTemp = {}
            _.forOwn currentStatuses, (status, key) ->
              for a in [0...status.segments.length]
                for b in [0...status.segments[a].steps.length]
                  if status.segments[a].steps[b].message?
                    if not messagesInCampaignsTemp[status.segments[a].steps[b].message]?
                      messagesInCampaignsTemp[status.segments[a].steps[b].message] = []
                    dripItem = {}
                    dripItem.status = status.name
                    dripItem.segment = status.segments[a].name
                    dripItem.drip = status.segments[a].steps[b]
                    messagesInCampaignsTemp[status.segments[a].steps[b].message].push dripItem

            messagesInCampaigns = _.cloneDeep messagesInCampaignsTemp

            clientLandingPages = pages.data
            clientPolicies = policies.data

            temp = messages.data
            for a in temp
              if not a.name?
                a.name = a.subject
              if not a.hidden? || a.hidden == false
                if a.progressStatus == 'draft'
                  draftMessagesInLibrary.push a
                else
                  _.forEach a.versions, (value) ->

                    if not value.id? then value.id = uuid.v4()
                    if not value.hidden? then value.hidden = false
                    if value.id == 'default' && value.name == ''
                      value.name = "Default Version"
                    if value.isDefault?
                      delete value["isDefault"]
                    true
                  completedMessagesInLibrary.push a
            messageToSet = _.find _.union(completedMessagesInLibrary, draftMessagesInLibrary), {'id' : $stateParams.message}
            if messageToSet?
              that.setMessageTemplateToBeEdited messageToSet
              if not currentMessageTemplateBeingEdited.message.versions? || currentMessageTemplateBeingEdited.message.versions.length == 0
                createDefaultVersion()

              preCleanUp()

              that.updateVersionWithGeneric()
              ##Check to see if we need to load version or set it to default to edit
              if $stateParams.version == 'default' || $stateParams.version ==  ''
                that.setCurrentVersionById 'default'
              else
                that.setCurrentVersionById $stateParams.version
              that.isMessageDraft()
            else if $state.current.name == 'manage-messagelibrary'
              ## message doesn't exist create new message
              $state.go 'manage-messagelibrary'


      this.loadMessages()

      this.messageTemplates = ->
        messagesTemplatesTemp

      this.completedMessages =  ->
        completedMessagesInLibrary

      this.messagesInDraft = ->
        draftMessagesInLibrary

      this.campaignMessages = ->
        messagesInCampaigns

      this.attributes = ->
        clientAttributes

      this.statuses = ->
        currentStatuses

      this.order = ->
        currentOrder

      this.policies = ->
        clientPolicies

      this.landingPages = ->
        clientLandingPages

      this.messageStatus = ->
        messageProgress

      this.currentTemplate = ->
        currentTemplate

      this.versionBeingEdited = ->
        currentVersion

      this.fetchMessageId = ->
        if currentMessageTemplateBeingEdited?.message?.id?
          currentMessageTemplateBeingEdited.message.id
        else
          ''

      this.getCurrentMessageId = ->
        if currentMessageTemplateBeingEdited?.message?
          if currentMessageTemplateBeingEdited.message.id?
            currentMessageTemplateBeingEdited.message.id
          else
            currentMessageTemplateBeingEdited.message.id = 'new'
            currentMessageTemplateBeingEdited.message.id
        else
          currentMessageTemplateBeingEdited =
            message :
              id : 'new'
          currentMessageTemplateBeingEdited.message.id

      this.getAttributesById = (attribute_ids) ->
        attribute_names = []
        _.forEach attribute_ids, (value, key) ->
          for a in [0...value.length]
            attribute_names.push _.result(_.find(condensedAttributes, 'attributeId' : value[a]), 'attributeName')
        attribute_names

      this.setWizardPosition = (position) ->
        wizardPosition = _.clone(position)

      this.getWizardPosition = ->
        wizardPosition

      this.getMessageRevision = ->
        currentMessageTemplateBeingEdited.message.version

      this.getTemplateById = (id)->
        _.find messagesTemplatesTemp, {'id' : id}

      this.whereIsTemplateBeingUsed = (templateId) ->
        messages = _.filter _.union(completedMessagesInLibrary, draftMessagesInLibrary), (message) ->
          if message.template?
            if message.template.value?
              message.template.value == templateId
            else
              message.template == templateId
          else
            false

      this.setMessageTemplateToBeEdited = (template) ->
        currentMessageTemplateBeingEdited = {}
        currentMessageTemplateBeingEdited.message = _.cloneDeep(template)

        if currentMessageTemplateBeingEdited.message.template?
          if not currentMessageTemplateBeingEdited.message.template.value?
            currentMessageTemplateBeingEdited.message.template =
              value   : currentMessageTemplateBeingEdited.message.template
              ref     : 'default'

        if currentMessageTemplateBeingEdited.message.templateFromDefault?
          delete currentMessageTemplateBeingEdited.message["templateFromDefault"]

        checkBlockTexts()

        if currentMessageTemplateBeingEdited.message?.slug?
          messageSlugName = currentMessageTemplateBeingEdited.message.slug
        else
          messageSlugName = ''
          currentMessageTemplateBeingEdited.message.slug = ''

      checkBlockTexts = ->
        _.forEach currentMessageTemplateBeingEdited.message.html.blocks, (value, key) ->
          currentMessageTemplateBeingEdited.message.html.blocks[key] = fixBlockText value

        _.forEach currentMessageTemplateBeingEdited.message.text.blocks, (value, key) ->
          currentMessageTemplateBeingEdited.message.text.blocks[key] = fixBlockText value

        for a in [0...currentMessageTemplateBeingEdited.message.versions]
          versionValue = currentMessageTemplateBeingEdited.message.versions[a]
          _.forEach versionValue.html.blocks, (value, key) ->
            currentMessageTemplateBeingEdited.message.versions[a].html.blocks[key] = fixBlockText value

          _.forEach versionValue.text.blocks, (value, key) ->
            currentMessageTemplateBeingEdited.message.versions[a].html.blocks[key] = fixBlockText value


      fixBlockText = (value) ->
        if value?.blockText?
          value.value = value.blockText
          delete value["blockText"]
        else
          value

      this.parseStateParams = ->
        ## Are we working on the current message loaded
        if not currentMessageTemplateBeingEdited.message.id? ||currentMessageTemplateBeingEdited.message.id != $stateParams.message || this.versionBeingEdited()?.id != $stateParams.version
        ## Is this an existing message?
          currentMessageTemplateBeingEdited = {}
          currentVersion = {}
          if $stateParams.message != 'new' && $stateParams.message != ''
            ## Has all of the messages loaded
            if (completedMessagesInLibrary.length > 0 || draftMessagesInLibrary.length > 0) && messagesTemplatesTemp.length > 0
              ## find message in library
              messageToSet = _.find _.union(completedMessagesInLibrary, draftMessagesInLibrary), {'id' : $stateParams.message}
              if messageToSet?
                this.setMessageTemplateToBeEdited messageToSet
                if not currentMessageTemplateBeingEdited.message.versions? || currentMessageTemplateBeingEdited.message.versions.length == 0
                  createDefaultVersion()

                preCleanUp()

                this.updateVersionWithGeneric()
                ##Check to see if we need to load version or set it to default to edit
                if $stateParams.version == 'default' || $stateParams.version ==  ''
                  this.setCurrentVersionById 'default'
                else
                  this.setCurrentVersionById $stateParams.version
                this.isMessageDraft()
              else
                ## message doesn't exist create new message
                $state.go 'manage-messagelibrary'
            else
              ##set up message to process once messages have loaded
              this.createNewMessageTemplate()
              this.loadMessages()
          else
            ##Create new message
            this.createNewMessageTemplate()
            this.updateVersionWithGeneric()
            this.setCurrentVersionById 'default'

        else
          this.updateVersionWithGeneric()
          this.isMessageDraft()

        if not currentMessageTemplateBeingEdited? && $state.current.name == 'manage-messagelibrary'
          $state.go 'manage-messagelibrary'
          wizardPosition = null
        else if $stateParams.state
          switch $stateParams.state.toLowerCase()
            when '' then that.setWizardPosition(null)
            when 'templates' then that.setWizardPosition(0)
            when 'subjects' then that.setWizardPosition(1)
            when 'links' then that.setWizardPosition(2)
            when 'html'then that.setWizardPosition(3)
            when 'plaintext' then that.setWizardPosition(4)
            when 'policies' then that.setWizardPosition(5)
            when 'confirmation' then that.setWizardPosition(6)

      this.createNewMessageTemplate = ->
        messageProgress = 'draft'
        currentMessageTemplateBeingEdited = {}
        currentMessageTemplateBeingEdited =
          message :
            id    : if $stateParams.message? then $stateParams.message else 'new'
            name : ''
            template :
              value : '-1'
              ref   : ''
            subject :
              value : ''
              ref   : 'default'
            html:
              blocks: {}
              links:  {}
              policies: {}
            text:
              blocks: {}
              links:  {}
              policies: {}
            progressStatus : 'draft'
            versions : []

        default_version = _.cloneDeep currentMessageTemplateBeingEdited.message
        default_version.id = 'default'
        default_version.hidden = false
        delete default_version["progressStatus"]
        delete default_version["versions"]
        currentMessageTemplateBeingEdited.message.versions.push default_version

        this.setMessageTemplateName ""
        this.updateVersionWithGeneric()
        this.setCurrentVersionById 'default'

      this.setMessageTemplateName = (name) ->
        currentMessageTemplateBeingEdited.message.name = name
        this.isMessageDraft()
        false

      this.isMessageDraft = ->
        isMessageComplete = true
        if currentMessageTemplateBeingEdited.message.versions.length == 0
          isMessageComplete = false
        for a in [0...currentMessageTemplateBeingEdited.message.versions.length]
          versionToLookAt = currentMessageTemplateBeingEdited.message.versions[a]
          if versionToLookAt.template.ref? &&versionToLookAt.template.ref == "default"
            versionTemplate = this.getTemplateById currentMessageTemplateBeingEdited.message.versions[0].template.value
          else
            versionTemplate = this.getTemplateById versionToLookAt.template.value

          if not versionToLookAt.hidden? || versionToLookAt.hidden == false
            if not versionTemplate?
              isMessageComplete = false

            if versionToLookAt.template.value == -1
              isMessageComplete = false

            if not versionTemplate?
              isMessageComplete = false
            if versionToLookAt.subject.ref != 'default' && not versionToLookAt.subject.value? || versionToLookAt.subject.value == ''
              isMessageComplete = false
            if isMessageComplete
              for a in [0...versionTemplate.meta.html.blocks.length]
                value = versionToLookAt.html.blocks[versionTemplate.meta.html.blocks[a].name]
                if value?
                  if value.ref !="default" && (not value.value? || value.value == '')
                    isMessageComplete = false
                else
                  isMessageComplete = false

            if isMessageComplete
              for a in [0...versionTemplate.meta.text.blocks.length]
                value = versionToLookAt.text.blocks[versionTemplate.meta.text.blocks[a].name]
                if value?
                  if value.ref !="default" && (not value.value? || value.value == '')
                    isMessageComplete = false
                else
                  isMessageComplete = false

            if isMessageComplete
              for a in [0...versionTemplate.meta.html.links.length]
                value = versionToLookAt.html.links[versionTemplate.meta.html.links[a].name]
                if value?
                  if value.ref !="default" && (not value.value? || value.value == '')
                    isMessageComplete = false
                  if versionTemplate.meta.html.links[a].needs? && versionTemplate.meta.html.links[a].needs[0].indexOf('text') != -1
                    if not value.text? || value.text == ''
                      isMessageComplete = false
                else
                  isMessageComplete = false

            if isMessageComplete
              for a in [0...versionTemplate.meta.text.links.length]
                value = versionToLookAt.text.links[versionTemplate.meta.text.links[a].name]
                if value?
                  if value.ref != "default" && (not value.value? || value.value == '')
                    isMessageComplete = false
                else
                  isMessageComplete = false

            if isMessageComplete
              for a in [0...versionTemplate.meta.html.policies.length]
                value = versionToLookAt.html.policies[versionTemplate.meta.html.policies[a].name]
                if value?
                  if value.ref != "default" && (not value.value? || value.value == '')
                    isMessageComplete = false
                else
                  isMessageComplete = false

            if isMessageComplete
              for a in [0...versionTemplate.meta.text.policies.length]
                value = versionToLookAt.text.policies[versionTemplate.meta.text.policies[a].name]
                if value?
                  if value.ref != "default" && (not value.value? || value.value == '')
                    isMessageComplete = false
                else
                  isMessageComplete = false
        if isMessageComplete
          messageProgress = 'completed'
          currentMessageTemplateBeingEdited.message.progressStatus = 'completed'
          return 'completed'
        else
          messageProgress = 'draft'
          currentMessageTemplateBeingEdited.message.progressStatus = 'draft'
          return 'draft'

      this.messageTemplateName = ->
        currentMessageTemplateBeingEdited?.message?.name or ''

      this.getMessageSlugName = ->
        if currentMessageTemplateBeingEdited?.message?.slug?
          return currentMessageTemplateBeingEdited.message.slug
        else
          return ''

      this.messageSlugNameSet = (slug) ->
        messageSlugName = slug
        currentMessageTemplateBeingEdited.message.slug = slug

      this.currentMessageTemplateId = ->
        if this.versionBeingEdited().template?value?
          return this.versionBeingEdited().template.value
        else
          return -1

      this.setMessageTemplateCreativeTemplate = (template) ->
        currentVersion.template =
          value : template.id
        if currentVersion.id == 'default'
          currentVersion.template.ref = 'default'
        else
          currentVersion.template.ref = 'self'

        this.updateVersionWithGeneric()
        this.isMessageDraft()

      this.addVersion = (message, version) ->
        currentMessageTemplateBeingEdited = {}
        currentMessageTemplateBeingEdited.message = _.cloneDeep message
        this.updateVersionWithGeneric()
        this.versionsWithGeneric = currentMessageTemplateBeingEdited.message.versions
        newVersion =
          id        : version.id
          template  :
            value : message.versions[0].template.value
            ref   : 'default'
          name      : version.name
          filters   : version.filters
          hidden    : false
          subject   :
            value   : ''
            ref     : 'default'
          html      :
            blocks  : _.forEach _.cloneDeep(currentMessageTemplateBeingEdited.message.html.blocks), (value) -> value.ref = "default"
            links   : _.forEach _.cloneDeep(currentMessageTemplateBeingEdited.message.html.links), (value) -> value.ref = "default"
            policies: _.forEach _.cloneDeep(currentMessageTemplateBeingEdited.message.html.policies), (value) -> value.ref = "default"
          text      :
            blocks  : _.forEach _.cloneDeep(currentMessageTemplateBeingEdited.message.text.blocks), (value) -> value.ref = "default"
            links   : _.forEach _.cloneDeep(currentMessageTemplateBeingEdited.message.text.links), (value) -> value.ref = "default"
            policies: _.forEach _.cloneDeep(currentMessageTemplateBeingEdited.message.text.policies), (value) -> value.ref = "default"

        if _.isObject(_.cloneDeep this.defaultVersion().subject)
          newVersion.subject.value = _.cloneDeep(this.defaultVersion().subject).value
        else
          newVersion.subject.value = _.cloneDeep this.defaultVersion().subject
        currentMessageTemplateBeingEdited.message.versions.push newVersion
        this.setCurrentVersionById version.id
        this.isMessageDraft()

      this.removeVersion = (message, version) ->
        currentMessageTemplateBeingEdited =
          message : message
        versionToArchive = _.find currentMessageTemplateBeingEdited.message.versions,  {'id': version.id}
        versionToArchive.hidden = true
        this.saveMessageToLibrary()

      this.saveVersionAttributes = (message, version) ->
        currentMessageTemplateBeingEdited =
          message : _.cloneDeep message
        _.forEach currentMessageTemplateBeingEdited.message.versions, (value) ->
          if value.id == version.id
            value.name = _.clone version.name
            value.filters = _.cloneDeep version.filters
        this.saveMessageToLibrary()

      this.updateVersionWithGeneric = ->
        this.versionsWithGeneric = []

        defaultVersion = currentMessageTemplateBeingEdited.message.versions[0]
        currentMessageTemplateBeingEdited.message.html = _.cloneDeep defaultVersion.html
        currentMessageTemplateBeingEdited.message.text = _.cloneDeep defaultVersion.text
        currentMessageTemplateBeingEdited.message.subject = _.cloneDeep defaultVersion.subject
        currentMessageTemplateBeingEdited.message.template = _.cloneDeep defaultVersion.template

        this.versionsWithGeneric = _.cloneDeep currentMessageTemplateBeingEdited.message.versions


      this.defaultVersion = ->
        _.find currentMessageTemplateBeingEdited.message.versions, (version) -> version.id == 'default'

      this.setCurrentVersionById = (id) ->
        this.updateVersionWithGeneric()
        currentVersion = _.find this.versionsWithGeneric, {'id' : id}

      this.updateMessage = (first_level, second_level, newValue) ->
        if currentVersion.id == 'default'
          if second_level?
            currentMessageTemplateBeingEdited.message[first_level][second_level] = _.cloneDeep newValue
          else
            currentMessageTemplateBeingEdited.message[first_level] = _.cloneDeep newValue

          for a in [0...currentMessageTemplateBeingEdited.message.versions.length]
            version = currentMessageTemplateBeingEdited.message.versions[a]
            if second_level?
              _.forEach version[first_level][second_level], (value, key) ->
                if version[first_level][second_level][key]?.ref == 'default' || version.id == 'default'
                  if newValue[key]?
                    version[first_level][second_level][key] = _.cloneDeep newValue[key]
              _.forEach newValue, (value, key) ->
                if not version[first_level][second_level][key]? && newValue[key]?
                  version[first_level][second_level][key] =
                    value : _.cloneDeep newValue[key].value
                    ref   : 'default'
            else
              if version.id =="default" || version[first_level]?.ref == 'default'
                if version[first_level]?
                  if _.isObject version[first_level]
                    version[first_level].value = _.cloneDeep newValue.value
                  else
                    version[first_level] =
                      value : version[first_level]
                      ref   : 'default'
              _.forEach newValue, (value, key) ->
                if not version[first_level][key]? && newValue[key]?
                  version[first_level][key] =
                    value : _.cloneDeep newValue[key].value
                    ref   : 'default'
        else
          version = _.find currentMessageTemplateBeingEdited.message.versions, {'id': currentVersion.id}
          if second_level?
            _.forEach version[first_level][second_level], (value, key) ->
              if newValue[key]?
                version[first_level][second_level][key] = _.cloneDeep newValue[key]
            _.forEach newValue, (value, key) ->
              if not version[first_level][second_level][key]?
                version[first_level][second_level][key] =
                  value : _.cloneDeep newValue[key].value
                  ref   : 'self'
          else
            if version[first_level]?
              version[first_level] = _.cloneDeep newValue
            _.forEach newValue, (value, key) ->
              if not version[first_level][key]?
                version[first_level][key] =
                  value : _.cloneDeep newValue[key].value
                  ref : 'self'

        this.updateVersionWithGeneric()
        this.isMessageDraft()

      this.saveMessageToLibrary = () ->
        templateToSend = _.cloneDeep currentMessageTemplateBeingEdited.message
        if templateToSend.versions?
          cleanUpMessageToSend templateToSend, true
          for a in [0...templateToSend.versions.length]
            if templateToSend.versions[a].hidden == false
              cleanUpMessageToSend templateToSend.versions[a], false
        if not templateToSend.id? || templateToSend.id == 'new'
          delete templateToSend["id"]
          $http.post('api/messages', templateToSend).success (data) ->
            currentMessageTemplateBeingEdited.message.id = data.id
            currentMessageTemplateBeingEdited.message.version = data.version
            messageLibraryIsLoading = false
            that.loadMessages()
        else
          $http.put('api/messages/' + templateToSend.id, templateToSend).success (data) ->
            currentMessageTemplateBeingEdited.message.version = data.version
            messageLibraryIsLoading = false
            that.loadMessages()

      preCleanUp = ->
        if currentMessageTemplateBeingEdited.message?.versions?.length > 0
          for a in [0...currentMessageTemplateBeingEdited.message.versions.length]
            currentMessageVersion = currentMessageTemplateBeingEdited.message.versions[a]
            if currentMessageVersion.links?
              currentMessageVersion.html.links = {}
              currentMessageVersion.text.links = {}
              for a in [0...currentMessageVersion.links.length]
                oldLink = currentMessageVersion.links[a]
                replacementLink =
                  needs   : _.cloneDeep oldLink.needs
                  text    : _.cloneDeep oldLink.text
                  type    : _.cloneDeep oldLink.type
                  value   : _.cloneDeep oldLink.value

                if not oldLink.active? ||oldLink.active == true
                  replacementLink.ref = 'default'
                else
                  replacementLink.ref = 'self'
                if currentMessageVersion.links[a].linkEncodeType == "HTML Link"
                  currentMessageVersion.html.links[currentMessageVersion.links[a].name] = replacementLink
                else
                  currentMessageVersion.text.links[currentMessageVersion.links[a].name] = replacementLink
              delete currentMessageVersion["links"]

            if currentMessageVersion.policies?
              currentMessageVersion.html.policies = {}
              currentMessageVersion.text.policies = {}
              for a in [0...currentMessageVersion.policies.length]
                oldPolicy = currentMessageVersion.policies[a]
                replacementPolicy =
                  value   : oldPolicy.value
                if not oldPolicy.active? || oldPolicy.active == true
                  replacementPolicy.ref = 'default'
                else
                  replacementPolicy.ref = 'self'
                if currentMessageVersion.policies[a].policyEncodeType == "HTML Policy"
                  currentMessageVersion.html.policies[currentMessageVersion.policies[a].policyName] = replacementPolicy
                else
                  currentMessageVersion.text.policies[currentMessageVersion.policies[a].policyName] = replacementPolicy
              delete currentMessageVersion["policies"]

            if _.isArray currentMessageVersion.html.blocks
              oldArray = _.cloneDeep currentMessageVersion.html.blocks
              currentMessageVersion.html.blocks = {}
              for a in [0...oldArray.length]
                value = oldArray[a]
                currentMessageVersion.html.blocks[value.name] =
                  value : value.blockText
                if value.active
                  currentMessageVersion.html.blocks[value.name].ref = 'default'

            if _.isArray currentMessageVersion.text.blocks
              oldArray = _.cloneDeep currentMessageVersion.text.blocks
              currentMessageVersion.text.blocks = {}
              for a in [0...oldArray.length]
                value = oldArray[a]
                currentMessageVersion.text.blocks[value.name] =
                  value : value.blockText
                if value.active
                  currentMessageVersion.text.blocks[value.name].ref = 'default'

            if currentMessageVersion.subject?
              if not currentMessageVersion.subject.value?
                subject =
                  value : currentMessageVersion.subject
                  ref   : 'default'
                currentMessageVersion.subject = subject

            if currentMessageVersion.template?
              if not currentMessageVersion.template.value?
                template =
                  value : currentMessageVersion.template
                  ref   : 'default'
                currentMessageVersion.template = template
            if currentMessageVersion.templateFromDefault?
              delete currentMessageVersion["templateFromDefault"]


            sections = ["html", "text"]
            properties = ["blocks", "links", "policies"]
            for a in [0...sections.length]
              currentSection = sections[a]
              for b in [0...properties.length]
                currentProperty = properties[b]

                _.forEach currentMessageVersion[currentSection][currentProperty], (value, key) ->
                  if currentMessageVersion[currentSection][currentProperty]?[key]?.active? == true
                    delete currentMessageVersion[currentSection][currentProperty][key]["active"]
                  if currentMessageVersion[currentSection][currentProperty][key]?.blockText?
                    block = currentMessageVersion[currentSection][currentProperty][key]
                    block.value = block.blockText
                    delete block["blockText"]


                  if currentMessageVersion.hidden == false
                    if currentMessageVersion[currentSection][currentProperty][key].ref == 'default' || currentMessageVersion.id == 'default'
                      currentDefaultVersion  = _.cloneDeep currentMessageTemplateBeingEdited.message.versions[0]
                      partToSet = _.cloneDeep currentDefaultVersion[currentSection][currentProperty][key]
                      partToSet["ref"] = "default"
                      currentMessageVersion[currentSection][currentProperty][key] = partToSet
                    else
                      currentMessageVersion[currentSection][currentProperty][key]["ref"] = "self"


            if not currentMessageVersion.subject? || currentMessageVersion.subject.ref == 'default' ||  currentMessageVersion.id == 'default'
              currentDefaultVersion  = _.cloneDeep currentMessageTemplateBeingEdited.message.versions[0]
              partToSet = _.cloneDeep currentDefaultVersion.subject
              if partToSet?
                partToSet["ref"] = "default"
              else
                partToSet =
                  ref : 'self'
                  value : ''
              currentMessageVersion.subject = partToSet
            else
              currentMessageVersion.subject["ref"] ="self"

            if not currentMessageVersion.template? ||currentMessageVersion.template.ref == "default" || currentMessageVersion.id == 'default'
              currentDefaultVersion  = _.cloneDeep currentMessageTemplateBeingEdited.message.versions[0]
              partToSet = _.cloneDeep currentDefaultVersion.template
              if partToSet?
                partToSet["ref"] = "default"
              else
                partToSet =
                  ref : 'self'
                  value : ''
              currentMessageVersion.template  = partToSet
            else
              currentMessageVersion.template["ref"] = "self"

        delete currentMessageTemplateBeingEdited.message["links"]
        delete currentMessageTemplateBeingEdited.message["policies"]

      createDefaultVersion = ->
        currentMessageTemplateBeingEdited.message.versions = []
        catchAll =
          id        : 'default'
          name      : 'Default Version'
          subject   : _.cloneDeep currentMessageTemplateBeingEdited.message.subject
          html      :
            blocks  : {}
            links   : _.cloneDeep currentMessageTemplateBeingEdited.message.html.links
            policies: _.cloneDeep currentMessageTemplateBeingEdited.message.html.policies
          text      :
            blocks  : {}
            links   : _.cloneDeep currentMessageTemplateBeingEdited.message.text.links
            policies: _.cloneDeep currentMessageTemplateBeingEdited.message.text.policies

        if currentMessageTemplateBeingEdited.message.template?
          if not currentMessageTemplateBeingEdited.message.template?
            catchAll.template =
              value : currentMessageTemplateBeingEdited.message.template
              ref   : 'default'
          else
            catchAll.template = currentMessageTemplateBeingEdited.message.template.value
        _.forEach currentMessageTemplateBeingEdited.message.html.blocks, (value, key) ->
          if currentMessageTemplateBeingEdited.message.html.blocks[key]? && not currentMessageTemplateBeingEdited.message.html.blocks[key].value?
            catchAll.html.blocks[key] =
              value:  currentMessageTemplateBeingEdited.message.html.blocks[key]

        _.forEach currentMessageTemplateBeingEdited.message.text.blocks, (value, key) ->
          if currentMessageTemplateBeingEdited.message.text.blocks[key]? && not currentMessageTemplateBeingEdited.message.text.blocks[key].value?
            catchAll.text.blocks[key] =
              value:  currentMessageTemplateBeingEdited.message.text.blocks[key]

        _.forEach currentMessageTemplateBeingEdited.message.html.policies, (value, key) ->
          if currentMessageTemplateBeingEdited.message.html.policies[key]? && not currentMessageTemplateBeingEdited.message.html.policies[key].value?
            catchAll.html.policies[key] =
              value: currentMessageTemplateBeingEdited.message.html.policies[key]

        _.forEach currentMessageTemplateBeingEdited.message.text.policies, (value, key) ->
          if currentMessageTemplateBeingEdited.message.text.policies[key]? &&not currentMessageTemplateBeingEdited.message.text.policies[key].value?
            catchAll.text.policies[key] =
              value : currentMessageTemplateBeingEdited.message.text.policies[key]

        currentMessageTemplateBeingEdited.message.versions.unshift catchAll

      cleanUpMessageToSend = (versionToCheck, isBase) ->
        if versionToCheck.id == 'default' && versionToCheck.name ==''
          versionToCheck.name = "Default Version"

        if isBase || versionToCheck.id == 'default' || versionToCheck.template.ref != 'default'
          if isBase
            versionToCheck.template = versionToCheck.versions[0].template.value
          else
            delete versionToCheck.template["ref"]
        else
          delete versionToCheck.template["value"]

        if isBase || versionToCheck.id == 'default' || versionToCheck.subject.ref != 'default'
          if isBase
            versionToCheck.subject = versionToCheck.versions[0].subject.value
          else
            delete versionToCheck.subject["ref"]
        else
          delete versionToCheck.subject["value"]

        first_pos = ['html', 'text']
        second_pos = ['blocks', 'policies', 'links']

        for a in [0...first_pos.length]
          a_first_pos = first_pos[a]
          for b in [0...second_pos.length]
            a_second_pos = second_pos[b]
            _.forEach versionToCheck[a_first_pos][a_second_pos], (value, key) ->
              if isBase || versionToCheck.id == 'default' || versionToCheck[a_first_pos][a_second_pos][key].ref != 'default'
                delete versionToCheck[a_first_pos][a_second_pos][key]["ref"]
              else
                if second_pos != "links"
                  delete versionToCheck[a_first_pos][a_second_pos][key]["value"]
                else
                  delete versionToCheck[a_first_pos][a_second_pos][key]["value"]
                  delete versionToCheck[a_first_pos][a_second_pos][key]["needs"]
                  delete versionToCheck[a_first_pos][a_second_pos][key]["text"]
                  delete versionToCheck[a_first_pos][a_second_pos][key]["type"]

        pieces = ["templates", "subject", "html", "links", "policies", "text"]
        for a in [0...pieces.length]
          if versionToCheck[pieces[a]+"FromDefault"]?
            delete versionToCheck[pieces[a]+"FromDefault"]

      this.archiveMessage = (message) ->
        templateToSend = _.find _.union(completedMessagesInLibrary, draftMessagesInLibrary), (value) -> value.id == message.id
        templateToSend.hidden = true
        $http.post('api/messages', templateToSend).success (data) ->
          messageLibraryIsLoading = false
          that.loadMessages()

      this.cloneMessageById = (id) ->
        message = _.find _.union(completedMessagesInLibrary, draftMessagesInLibrary), {'id' : id}

      this.resetMessageLibrary = ->
        this.setWizardPosition null
        this.loadMessages()
        false

      this.setStatusSegmentBreadcrumb = (status, segment) ->
        breadcrumbStatus = status
        breadcrumbSegment = segment

      this.getStatusSegmentBreadcrumb = ->
        if not breadcrumbStatus?
          return undefined
        else
          return [breadcrumbStatus, breadcrumbSegment]

      assign = (obj, prop, value) ->
        if typeof prop is "string"
          prop = prop.split(".")

        if prop.length > 1
          e = prop.shift()
          if Object.prototype.toString.call(obj[e]) isnt "[object Object]"
            obj[e] = {}
          assign obj[e], prop, value
        else
          obj[prop[0]] = value
      false
]


  .controller 'DripEditorModalCtrl',
    ['$scope', 'MessageLibrarySrv', '$state', 'drip', 'new_step', 'segment', '$uibModalInstance', '$http',
    ($scope, MessageLibrarySrv, $state, drip, new_step, segment, $uibModalInstance, $http) ->
      $scope.messageService = MessageLibrarySrv
      $scope.currentDrip = _.cloneDeep drip
      $scope.newStep = new_step
      $scope.senders = []
      if not $scope.currentDrip.notify?
        if not $scope.currentDrip.vuNotification?
          $scope.currentDrip.notify = $scope.currentDrip.vuNotification = false
        else
          $scope.currentDrip.notify = $scope.currentDrip.vuNotification

      if $scope.currentDrip.message?.id?
        $scope.currentDripMessageId = $scope.currentDrip.message.id
      else
        $scope.currentDripMessageId = $scope.currentDrip.message

      if not $scope.currentDripMessageId?
        $scope.dripError = "The message that is current associated with this drip no longer exists or is in DRAFT mode."
      else
        $scope.dripError = undefined

      timeToCheck = $scope.currentDrip.delay.value * if $scope.currentDrip.delay.units is 'day' then 24 else 1
      timeSlotTaken = _.find segment.steps, (step) ->
        if step.delay.units == 'day'
          dripDelay = step.delay.value * 24
        else
          dripDelay = step.delay.value
        dripDelay == timeToCheck && step.id != $scope.currentDrip.id

      if timeSlotTaken?
        $scope.dripTemplateWarning = 'You already have a message being sent during this time for this campaign.  Adding it could be detrimental to the performance of your campaign.'
      else
        $scope.dripTemplateWarning = undefined

      $scope.templatesInLibrary1 = MessageLibrarySrv.completedMessages()

      $http.get('api/senders?q=active').success (senders) ->
        $scope.senders = _.pluck senders, 'email'

      $scope.$watch ('messageService.completedMessages()'), ->
        $scope.templatesInLibrary1 = MessageLibrarySrv.completedMessages()

      $scope.onMessageChange = ->
        $scope.dripError = undefined
        $scope.dripTemplateWarning = undefined
        $scope.currentDrip.message = $scope.messageService.cloneMessageById($scope.currentDripMessageId)
        $scope.currentDrip.name = $scope.currentDrip.message.name
        $scope.isTimeSlotTaken()
        false

      $scope.onDelayChange = ->
        $scope.baseLineTimeToHours()
        $scope.isTimeSlotTaken()

      $scope.onNotificationChange = ->
        $scope.currentDrip.vuNotification = $scope.currentDrip.notify
        $scope.isTimeSlotTaken()

      $scope.isTimeSlotTaken = ->
        timeToCheck = $scope.currentDrip.delay.value * if $scope.currentDrip.delay.units is 'day' then 24 else 1
        timeSlotTaken = _.find (_.filter segment.steps, (f) ->
          return f if f.deleted isnt true
          ), (step) ->
          dripDelay = step.delay.value * if step.delay.units is 'day' then 24 else 1
          dripDelay == timeToCheck && step.id != $scope.currentDrip.id

        if timeSlotTaken?
          $scope.dripTemplateWarning = 'You already have a message being sent during this time for this campaign.  Adding it could be detrimental to the performance of your campaign.'
        else
          $scope.dripTemplateWarning = undefined


      $scope.baseLineTimeToHours = ->
          if $scope.currentDrip.delay.units == 'day'
            $scope.currentDrip.delay.delayInHours = parseInt($scope.currentDrip.delay.value, 10) * 24
          else
            $scope.currentDrip.delay.delayInHours = parseInt($scope.currentDrip.delay.value, 10)

      $scope.save = ->
        $uibModalInstance.close(_.cloneDeep($scope.currentDrip))

      $scope.close = ->
        $uibModalInstance.dismiss()

      $scope.baseLineTimeToHours()
      $scope.isTimeSlotTaken()
      false
]

  .controller 'MessageLibraryCtrl',
    ['$scope','MessageLibrarySrv', '$state', '$uibModal', 'growl',
    ($scope, MessageLibrarySrv, $state, $uibModal, growl) ->
      $scope.navigationItems = ['Template', 'Subject', 'Links', 'HTML', 'Plain Text', 'Policies', 'Preview']
      $scope.navURLItems = ['templates', 'subjects', 'links', 'html', 'plaintext', 'policies', 'confirmation']
      $scope.service = MessageLibrarySrv
      $scope.currentWizardPosition = 0
      $scope.selectedTemplate = -1
      $scope.messageName = ''
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
      $scope.hovering = false
      $scope.parentHeight = ($('#wizardContainer').height()/7)
      $scope.messageTemplateExists = false
      $scope.incomingWizardPosition = MessageLibrarySrv.getWizardPosition()
      $scope.messageProgress = MessageLibrarySrv.messageStatus()
      $scope.messagesInCampaigns = {}

      $scope.$watch ('service.getWizardPosition()'), ->
        $scope.incomingWizardPosition = MessageLibrarySrv.getWizardPosition()
        if $scope.incomingWizardPosition?
          $('#messageLibrary').animate({marginLeft:'-100%'}, 500)
          $('#wizard-footer').fadeIn()
          $scope.gotoMessageLibraryScreen($scope.incomingWizardPosition, true)
        else
          $('#messageLibrary').animate({marginLeft:'0%'}, 500)
          $('#wizard-footer').fadeOut()

      $scope.$watch ('service.campaignMessages()'), ->
        $scope.messagesInCampaigns = MessageLibrarySrv.campaignMessages()


      $scope.$watch ('service.versionBeingEdited()'), ->
        $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
        if $scope.currentVersion.template?
          $scope.messageTemplateExists = _.find($scope.messageTemplates, {'id' : $scope.currentVersion.template.value})?
          if $scope.messageTemplateExists == false
            $scope.messageTemplateExists = _.find($scope.messageTemplates, {'id' : $scope.currentVersion.template})?

      $scope.$watch ('service.messageStatus()'), ->
        $scope.messageProgress = MessageLibrarySrv.messageStatus()
        if MessageLibrarySrv.getCurrentMessageId() != 'new' && $scope.messagesInCampaigns[MessageLibrarySrv.getCurrentMessageId()]? && $scope.messageProgress == "draft"
          growl.danger "This message is currently being used in a drip campaign.  DRAFT mode for this message has been de-activated."

      $scope.isMessageInCampaign = (messageId) ->
        $scope.messagesInCampaigns[messageId]?

      $scope.$on '$stateChangeSuccess', ->
        if $state.current.url == '/manage/message-library'
          $scope.service.resetMessageLibrary()
        if $state.current.url == '/:state'
          $scope.service.parseStateParams()

      $scope.saveDripMessage = ->
        MessageLibrarySrv.saveMessageToLibrary()
        inst = $uibModal.open
          templateUrl: 'template_save_confirmation_modal.html'
          controller: 'SaveTemplateModalCtrl'
        false


      $scope.cancelDripMessageEdit = ->
        inst = $uibModal.open
          templateUrl: 'template_cancel_confirmation_modal.html'
          controller: 'CancelTemplateModalCtrl'
        false

      $scope.gotoMessageLibraryScreen = (newPosition, overrideCheck) ->
        if overrideCheck
          $scope.currentWizardPosition = newPosition
          tagName = 'section' + newPosition
          aTag = $("a[name='section"+newPosition+"']")
          parentContainer = $('#wizardContainer')
          if aTag.offset()?
            $('#wizardContainer').animate({marginTop: (parentContainer.offset().top - aTag.offset().top) + 'px'} ,250)
            $scope.updateNavigation()
          else

        else
          $scope.messageName = MessageLibrarySrv.messageTemplateName()
          $scope.selectedTemplate = MessageLibrarySrv.currentMessageTemplateId()
          if ($scope.selectedTemplate.value != -1)
            if !$scope.messageName
              $scope.templateNameError = 'Please give us a name for this template'
            else
              $scope.templateNameError = null
              if newPosition == null
                if $scope.currentWizardPosition < 6
                  $scope.currentWizardPosition += 1
              else
                $scope.currentWizardPosition = newPosition

            aTag = $("a[name='section"+newPosition+"']")
            parentContainer = $('#wizardContainer')
            $('#wizardContainer').animate({marginTop: (parentContainer.offset().top - aTag.offset().top) + 'px'} ,250)
            $scope.updateNavigation()
          else
            $scope.templateNameError = 'Please choose a template.'

      $scope.stepBackMessageWizard = ->
        $scope.gotoMessageLibraryScreen($scope.currentWizardPosition - 1, true)

      $scope.stepForwardMessageWizard = ->
        $scope.gotoMessageLibraryScreen($scope.currentWizardPosition + 1, true)


      $scope.onMouseover = (id) ->
        if id!=$scope.currentWizardPosition && $scope.currentVersion.template.value!=-1
          $('#nav_' + id).addClass('navigation-hover')
        else
          $('#nav_' + id).removeClass('navigation-hover')
        false

      $scope.onMouseout = (id) ->
        if id!=$scope.currentWizardPosition
          $('#nav_' + id).removeClass('navigation-hover')
        false

      $scope.updateNavigation = ->
        message = MessageLibrarySrv.getCurrentMessageId()
        version = MessageLibrarySrv.versionBeingEdited().id
        state = $scope.navURLItems[$scope.currentWizardPosition]
        $state.go 'manage-messagelibrary.message.version.state',
          message: message
          version: version
          state: state

      $scope.changeWizardPosition = (id) ->
        if $scope.currentVersion.template.value!=-1 then $scope.service.setWizardPosition id

      $scope.onBackToLibraryClicked = ->
        if $scope.service.getStatusSegmentBreadcrumb()?
          status = $scope.service.getStatusSegmentBreadcrumb()[0]
          segment = $scope.service.getStatusSegmentBreadcrumb()[1]
          $state.go 'manage-statuses.status.segment',
            status: status
            segment: segment
        else
          $state.go 'manage-messagelibrary'
          $scope.service.setWizardPosition null
  ]

  .controller 'DripTemplateChooserCtrl',
    ['$scope', 'MessageLibrarySrv', '$state', '$uibModal', 'growl', '$http',
    ($scope, MessageLibrarySrv, $state, $uibModal, growl, $http) ->
      $scope.service = MessageLibrarySrv
      $scope.templatesInLibrary = MessageLibrarySrv.completedMessages()
      $scope.messageName = MessageLibrarySrv.messageTemplateName()
      $scope.selectedTemplateId = MessageLibrarySrv.currentMessageTemplateId()
      $scope.searchText   = ''
      $scope.draftsInLibrary = MessageLibrarySrv.messagesInDraft()
      $scope.creativeTemplates = MessageLibrarySrv.messageTemplates()
      $scope.statuses = []
      $scope.order = []
      $scope.showMessageSearch = false
      $scope.showDraftSearch = false
      $scope.showVersions = {}
      $scope.messagesInCampaigns = {}

      $scope.$watch ('service.completedMessages()'), ->
        $scope.templatesInLibrary = MessageLibrarySrv.completedMessages()
        $scope.draftsInLibrary = MessageLibrarySrv.messagesInDraft()

        _.forEach $scope.templatesInLibrary, (value) ->
          $scope.showVersions[value.id] = true
          if value.versions?
            value.filter_names = []
            _.forEach value.versions, (versionValue, versionKey) ->
              versionValue.filter_names = MessageLibrarySrv.getAttributesById versionValue.filters

        _.forEach $scope.draftsInLibrary, (value) ->
          $scope.showVersions[value.id] = true
          if value.versions?
            value.filter_names = []
            _.forEach value.versions, (versionValue, versionKey) ->
              versionValue.filter_names = MessageLibrarySrv.getAttributesById versionValue.filters

      $scope.$watch ('service.messageTemplateName()'), ->
        $scope.messageName = MessageLibrarySrv.messageTemplateName()
      $scope.$watch ('service.currentMessageTemplateId()'), ->
        $scope.selectedTemplateId = MessageLibrarySrv.currentMessageTemplateId()
      $scope.$watch ('service.messagesInDraft()'), ->
        $scope.draftsInLibrary = MessageLibrarySrv.messagesInDraft()
      $scope.$watch ('service.statuses()'), ->
        $scope.statuses = MessageLibrarySrv.statuses()
      $scope.$watch ('service.order()'), ->
        $scope.order = MessageLibrarySrv.order()
      $scope.$watch ('service.messageTemplates()'), ->
        $scope.creativeTemplates = MessageLibrarySrv.messageTemplates()
      $scope.$watch ('service.campaignMessages()'), ->
        $scope.messagesInCampaigns = MessageLibrarySrv.campaignMessages()

      $scope.isDefault = (version) ->
        return version.id != 'default'

      $scope.isMessageInCampaign = (messageId) ->
        $scope.messagesInCampaigns[messageId]?

      $scope.numberOfAttributes = (filters) ->
        number = 0
        _.forEach filters, (value, key) ->
          number += value.length
        number

      $scope.templateExists = (template) ->
        !(_.some $scope.creativeTemplates, 'id': template.value)

      $scope.active_versions = (message) ->
        if message.versions?
          active_versions = _.filter _.cloneDeep(message.versions), {'hidden': false}
          return active_versions.length
        else
          return 0

      $scope.toggleMessageSearch = ($event) ->
        $event.stopPropagation()
        $scope.showMessageSearch = !$scope.showMessageSearch

      $scope.editVersion = (message, message_version, $event) ->
        message = message.id
        version = message_version.id
        $state.go 'manage-messagelibrary.message.version.state',
          message: message
          version: version
          state: 'templates'

      $scope.editAttributes = (message, message_version) ->
        inst = $uibModal.open
          templateUrl:  'editVersionAttributesModal.html'
          controller:   'EditVersionAttributesModalCtrl'
          resolve:
            message: -> _.cloneDeep message
            currentVersion: -> _.cloneDeep message_version

      $scope.cloneVersion = (template, templateVersion) ->
        inst = $uibModal.open
          templateUrl:  'cloneVersionModal.html'
          controller:   'CloneVersionModalCtrl'
          resolve:
            message:  ->  template
            version:  ->  _.cloneDeep templateVersion

      $scope.deleteVersion = (message, message_version) ->
        inst = $uibModal.open
          templateUrl:  'version_delete-confirm.html'
          controller:   'DeleteVersionModalCtrl'
          resolve:
            message:  -> message
            version:  -> message_version


      $scope.shouldShowVersions = (template_id) ->
        return $scope.showVersions[template_id]

      $scope.toggleDraftSearch = ($event) ->
        $event.stopPropagation()
        $scope.showDraftSearch = !$scope.showDraftSearch

      $scope.editMessageDripTemplate = (message) ->
        if message.versions?.length > 1
          toggleShowVersions(message.id)
        else
          MessageLibrarySrv.setStatusSegmentBreadcrumb undefined, undefined

          $state.go 'manage-messagelibrary.message.version.state',
            message: message.id
            version: 'default'
            state: 'templates'

      $scope.createNewTemplate = ->
        MessageLibrarySrv.createNewMessageTemplate()
        $state.go 'manage-messagelibrary.message.version.state',
          message: 'new'
          version: 'default'
          state: 'templates'

      $scope.messageIsReadyToSave = ->
        if not $scope.messageName? || $scope.messageName.length==0 || $scope.selectedTemplateId == -1
          return false

        return true

      $scope.showStatusesThatHaveMessage = ($event, message) ->
        $event.stopPropagation()
        inst = $uibModal.open
          templateUrl: 'statusesWhereMessageExistModal.html'
          controller: 'StatusesWhereMessageExistModalCtrl'
          resolve:
            message:  -> _.cloneDeep message

      $scope.saveDripMessage = ->
        if $scope.service.getCurrentMessageId() != 'new' && $scope.messagesInCampaigns[$scope.service.getCurrentMessageId()]?
          growl.warning "This message is currently being used in a drip campaign.  DRAFT mode for this message has been de-activated."
        else
          MessageLibrarySrv.saveMessageToLibrary()
          inst = $uibModal.open
            templateUrl: 'template_save_confirmation_modal.html'
            controller: 'SaveTemplateModalCtrl'
        false

      $scope.cancelDripMessageEdit = ->
        inst = $uibModal.open
          templateUrl: 'template_cancel_confirmation_modal.html'
          controller: 'CancelTemplateModalCtrl'
        false

      $scope.cloneMessage = (message) ->
        cloned = _.cloneDeep message
        cloned.name = cloned.name + " (Cloned)"
        inst = $uibModal.open
          templateUrl: 'message_clone-confirm.html'
          controller: 'MessageCloneConfirmCtrl'
          resolve:
            message: -> cloned
        inst.result.then (message)->
          $http.post('api/messages', message).success (data) ->
            message.version = data.version
            $scope.service.messageLibraryIsLoading = false
            $scope.service.loadMessages(true)
            growl.success message.name + ' has been added to your message library.'
        false

      $scope.addVersionToMessage = (message) ->
        cloned = _.cloneDeep message
        currentVersion = {}
        currentVersion.id = uuid.v4()
        inst = $uibModal.open
          templateUrl: 'add-version_to-message-modal.html'
          controller: 'AddVersionToMessageModalCtrl'
          resolve:
            message: -> cloned
            currentVersion: -> _.cloneDeep currentVersion

      $scope.addMessageToDrip = (message) ->

        cloned = _.cloneDeep message
        inst = $uibModal.open
          templateUrl: 'add-message-to-drip.html'
          controller: 'AddMessageToDripModal'
          size: 'lg'
          resolve:
            order: -> $scope.order
            statuses: -> $scope.statuses
            message: -> cloned
        inst.result.then (status) ->
          if status.id is $scope.selected_status.id
            $scope.selected_status = status
          $scope.statuses[status.id] = status

      $scope.sendMessageAsBatch = (message) ->
        $state.go 'manage-batches-with-message', message: message.id

      $scope.viewMessagePreview = (message, message_id) ->
        template_id = message.template
        if template_id.value?
          template_id = template_id.value
        template = template_id
        inst = $uibModal.open
          templateUrl: 'html-modal.html'
          size: 'lg'
          scope: _.assign $scope.$new(yes),
            id: template
            url: "/messages/#{template}/preview"
            post: id: message_id

      $scope.sendMessageSample = (message, version) ->
        step = {}
        step.message = message
        template_id = message.template
        if template_id.value?
          template_id = template_id.value
        inst = $uibModal.open
          templateUrl: 'sample-modal.html'
          controller: 'SampleModal'
          resolve:
            template: -> template_id
            status: -> null
            segment: -> null
            step: -> step

      $scope.archiveMessage = (message) ->
        cloned = _.cloneDeep message
        inst = $uibModal.open
          templateUrl: 'message_delete-confirm.html'
          controller: 'MessageDeleteConfirmCtrl'
          resolve:
            message: -> cloned
        inst.result.then (message)->
          $scope.service.archiveMessage(message)
          growl.success 'Message deleted'
        false

      toggleShowVersions = (template_id) ->
        _.forOwn $scope.showVersions, (value, key) ->
          if key == template_id
            value = !value
            $scope.showVersions[key] = value
            return value
  ]

  .controller 'SaveTemplateModalCtrl',
    ['$scope', '$state', '$uibModalInstance', 'MessageLibrarySrv',
    ($scope, $state, $uibModalInstance, MessageLibrarySrv) ->

      $scope.service = MessageLibrarySrv

      $scope.continueEditing = ->
        $uibModalInstance.dismiss()

      $scope.exitToLibrary = ->
        $uibModalInstance.dismiss()
        if not $scope.service.getStatusSegmentBreadcrumb()?
          $state.go 'manage-messagelibrary'
          $scope.service.setWizardPosition null
        else
          status = $scope.service.getStatusSegmentBreadcrumb()[0]
          segment = $scope.service.getStatusSegmentBreadcrumb()[1]
          $state.go 'manage-statuses.status.segment',
            status: status
            segment: segment

      false
  ]

  .controller 'CancelTemplateModalCtrl',
    ['$scope', '$state', '$uibModalInstance', 'MessageLibrarySrv',
    ($scope, $state, $uibModalInstance, MessageLibrarySrv) ->

      $scope.service = MessageLibrarySrv

      $scope.saveChangesFirst = ->
        MessageLibrarySrv.saveMessageToLibrary()
        $uibModalInstance.dismiss()
        if not $scope.service.getStatusSegmentBreadcrumb()?
          $state.go 'manage-messagelibrary'
          $scope.service.setWizardPosition null
        else
          status = $scope.service.getStatusSegmentBreadcrumb()[0]
          segment = $scope.service.getStatusSegmentBreadcrumb()[1]
          $state.go 'manage-statuses.status.segment',
            status: status
            segment: segment

      $scope.continueEditing = ->
        $uibModalInstance.dismiss()

      $scope.exitToLibrary = ->
        $uibModalInstance.dismiss()
        if not $scope.service.getStatusSegmentBreadcrumb()?
          $state.go 'manage-messagelibrary'
          $scope.service.setWizardPosition null
        else
          status = $scope.service.getStatusSegmentBreadcrumb()[0]
          segment = $scope.service.getStatusSegmentBreadcrumb()[1]
          $state.go 'manage-statuses.status.segment',
            status: status
            segment: segment
  ]

  .controller 'MessageDeleteConfirmCtrl',
    ['$scope', '$uibModalInstance', 'message',
    ($scope, $uibModalInstance, message) ->

      $scope.messageDeleteConfirmed = ->
        $uibModalInstance.close message

      $scope.cancelMessageDelete = ->
        $uibModalInstance.dismiss()
  ]

  .controller 'MessageCloneConfirmCtrl',
    ['$scope', '$uibModalInstance', 'message',
    ($scope, $uibModalInstance, message) ->

      $scope.messageToClone = message
      delete $scope.messageToClone['id']
      delete $scope.messageToClone['version']

      $scope.messageCloneConfirmed = ->
        $uibModalInstance.close $scope.messageToClone

      $scope.cancelMessageClone = ->
        $uibModalInstance.dismiss()
  ]

  .controller 'StatusesWhereMessageExistModalCtrl',
    ['$scope', '$uibModalInstance', '$state', 'message', 'MessageLibrarySrv',
    ($scope, $uibModalInstance, $state, message, MessageLibrarySrv) ->
      $scope.campaigns = MessageLibrarySrv.campaignMessages()[message.id]
      $scope.message = message

      $scope.goToCampaign = (campaign) ->
        $uibModalInstance.dismiss()
        $state.go 'manage-statuses.status.segment',
          status: campaign.status
          segment: campaign.segment

      $scope.delayInHours = (step) ->
        if step.drip.delay.units == 'day'
          step.drip.delay.value * 24
        else
          step.drip.delay.value

      $scope.dismiss = ->
        $uibModalInstance.dismiss()
  ]

  .controller 'AddMessageToDripModal',
    ['$scope', '$uibModalInstance', 'order', 'statuses', 'message',
    'httpRequest', 'saveTimeout', 'errorTimeout', 'growl',
    ($scope, $uibModalInstance, order, statuses, message,
    httpRequest, saveTimeout, errorTimeout, growl) ->
      $scope.order = order
      $scope.statuses = statuses
      $scope.forms = {}
      $scope.selected = status: null, segment: null

      $scope.currentDrip =
        delay :
          value : ''
          units : 'day'
        vuNotification  : false
        notify  : false
        message : message
        name    : message.name

      $scope.onDelayChange = ->
        $scope.baseLineTimeToHours()
        $scope.isTimeSlotTaken()

      $scope.onNotificationChange = ->
        $scope.currentDrip.vuNotification = $scope.currentDrip.notify
        $scope.isTimeSlotTaken()

      $scope.isTimeSlotTaken = ->
        timeToCheck = $scope.currentDrip.delay.value * if $scope.currentDrip.delay.units is 'day' then 24 else 1
        timeSlotTaken = _.find $scope.selected.segment.steps, (step) ->
          if step.delay.units == 'day'
            dripDelay = step.delay.value * 24
          else
            dripDelay = step.delay.value
          dripDelay == timeToCheck && step.id != $scope.currentDrip.id

        if timeSlotTaken?
          $scope.dripTemplateWarning = 'You already have a message being sent during this time for this campaign.  Adding it could be detrimental to the performance of your campaign.'
        else
          $scope.dripTemplateWarning = undefined

      $scope.baseLineTimeToHours = ->
        if $scope.currentDrip.delay.units is 'day'
          $scope.currentDrip.delay.delayInHours = parseInt($scope.currentDrip.delay.value, 10) * 24
        else
          $scope.currentDrip.delay.delayInHours = parseInt($scope.currentDrip.delay.value, 10)

      $scope.createDripStep = ->
        $scope.errorMessage = undefined
        $scope.formErrors = undefined
        $scope.forms.clone.submitted = no
        $scope.forms.clone.saved = no
        $scope.forms.clone.saving = yes

        $scope.currentDrip.vuNotification = $scope.currentDrip.notify
        setDelayInHours $scope.currentDrip
        $scope.selected.segment.steps.push $scope.currentDrip

        saveStatus "Drip Step #{$scope.currentDrip.name} Saved", "Error creating #{$scope.currentDrip.name} "

      $scope.close = ->
        $uibModalInstance.dismiss()

      setDelayInHours = (step) ->
        if step.delay.units is 'day'
          delay = parseInt(step.delay.value, 10) * 24
          step.delay.delayInHours = delay
        else
          delay = parseInt(step.delay.value, 10)
          step.delay.delayInHours = delay

      saveStatus = (success_message, error_message) ->
        if $scope.selected_segment?.steps?
          $scope.selected.segment.steps = _.sortBy $scope.selected.segment.steps, (step) ->
            if step.delay.units == 'day'
              step.delay.value * 24
            else
              step.delay.value

        httpRequest.save $scope.selected.status, (err, data) ->
          if not err?
            $scope.selected.status.version = data.version
            $uibModalInstance.dismiss()
        , success_message, error_message
    ]

  .controller 'TemplateChooserCtrl',
    ['$scope','MessageLibrarySrv',
    ($scope, MessageLibrarySrv) ->
      $scope.service = MessageLibrarySrv
      $scope.messageTemplates = MessageLibrarySrv.messageTemplates()
      $scope.message = {}
      $scope.message.messageName = MessageLibrarySrv.messageTemplateName()
      $scope.message.slugName = MessageLibrarySrv.getMessageSlugName()
      $scope.selectedTemplate = MessageLibrarySrv.currentTemplate()
      $scope.messageTemplateExists = false
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
      $scope.messageId = MessageLibrarySrv.fetchMessageId()
      $scope.currentTemplate = MessageLibrarySrv.versionBeingEdited().template

      $scope.$watch ('service.messageTemplates()'), ->
        $scope.messageTemplates = MessageLibrarySrv.messageTemplates()
        for a in [0...$scope.messageTemplates.length]
          $scope.messageTemplates[a].templateName = ''
          if $scope.messageTemplates[a].tags? > 0
            $scope.messageTemplates[a].templateName = $scope.messageTemplates[a].tags[0]

      $scope.$watch ('service.getMessageSlugName()'), ->
        $scope.message.slugName = MessageLibrarySrv.getMessageSlugName()

      $scope.$watch ('service.messageTemplateName()'), ->
        $scope.message.messageName = MessageLibrarySrv.messageTemplateName()
        $scope.message.slugName = MessageLibrarySrv.getMessageSlugName()

      $scope.$watch ('service.currentTemplate()'), ->
        $scope.selectedTemplate = MessageLibrarySrv.currentTemplate()

      $scope.$watch ('service.versionBeingEdited()'), ->
        $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
        $scope.currentTemplate = $scope.currentVersion.template
        $scope.messageId = MessageLibrarySrv.fetchMessageId()
        $scope.messageTemplateExists = showTemplateWarning()

      $scope.onUseDefaultChange = ->
        if $scope.currentTemplate.ref == 'default'
          defaultTemplateId = MessageLibrarySrv.defaultVersion().template.value
          defaultTemplate = MessageLibrarySrv.getTemplateById(defaultTemplateId)
          MessageLibrarySrv.setMessageTemplateCreativeTemplate defaultTemplate
          MessageLibrarySrv.updateMessage 'template',null,$scope.currentTemplate

      $scope.selectTemplate = (template) ->
        $scope.currentTemplate.value = template.id
        if ($scope.currentTemplate.ref != 'default' && $scope.currentVersion.id != 'default') || $scope.currentVersion.id == 'default'
          MessageLibrarySrv.setMessageTemplateCreativeTemplate template
          MessageLibrarySrv.updateMessage 'template',null,$scope.currentTemplate

      $scope.onMessageNameChange = ->
        MessageLibrarySrv.setMessageTemplateName _.clone $scope.message.messageName

      $scope.onSlugNameChange = ->
        MessageLibrarySrv.messageSlugNameSet _.clone $scope.message.slugName

      showTemplateWarning = ->
        if not $scope.currentVersion.template?
          return false
        if $scope.messageId == "new"  || $scope.currentVersion.template == -1 || $scope.currentVersion.template.value? ==-1
          return true
        else
          if $scope.currentVersion.template.value?
             return _.find($scope.messageTemplates, {'id' : $scope.currentVersion.template.value})?
          else
            return _.find($scope.messageTemplates, {'id' : $scope.currentVersion.template})?
        false
  ]

  .controller 'MessageLibrarySubjectsCtrl',
    ['$scope','MessageLibrarySrv',
    ($scope, MessageLibrarySrv) ->
      $scope.service = MessageLibrarySrv
      $scope.currentSubject = {}
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()

      $scope.$watch ('service.versionBeingEdited()'),  ->
        if MessageLibrarySrv.versionBeingEdited().subject?
          $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
          $scope.currentSubject = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().subject)
      , true

      $scope.onSubjectChange = ->
        MessageLibrarySrv.updateMessage 'subject',null,$scope.currentSubject

      $scope.onUseDefaultChange = ->
        if $scope.currentSubject.ref == 'default'
          $scope.currentSubject.value = MessageLibrarySrv.defaultVersion().subject.value
        MessageLibrarySrv.updateMessage 'subject',null,$scope.currentSubject

  ]

  .controller 'MessageLibraryLinksCtrl',
  ['$scope','MessageLibrarySrv',
  ($scope, MessageLibrarySrv) ->
    $scope.service = MessageLibrarySrv
    $scope.htmlLinks = {}
    $scope.textLinks = {}
    $scope.availableLandingPages = MessageLibrarySrv.landingPages()
    $scope.hasActiveLinks = false
    $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()

    $scope.$watch ('service.versionBeingEdited()'), ->
      if MessageLibrarySrv.versionBeingEdited().html? && MessageLibrarySrv.versionBeingEdited().html.links?
        $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
        $scope.htmlLinks = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().html.links)
        $scope.textLinks = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().text.links)
        $scope.hasActiveLinks = false
        $scope.activeLinkTab = null
        if $scope.currentVersion.template.value != "-1"
          $scope.selectedTemplate = MessageLibrarySrv.getTemplateById $scope.currentVersion.template.value

          if $scope.selectedTemplate?.meta.html.links.length > 0
            for a in [0...$scope.selectedTemplate.meta.html.links.length]
              if not $scope.selectedTemplate.meta.html.links[a].needs? || $scope.selectedTemplate.meta.html.links[a].needs[0].indexOf('text') == -1
                if $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name]? && $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name].text?
                  delete $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name]["text"]
              if $scope.selectedTemplate.meta.html.links[a].needs? && $scope.selectedTemplate.meta.html.links[a].needs[0].indexOf('text') != -1
                if $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name]? &&  not $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name].text?
                  linkToLookAt = $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name]
                  linkToLookAt["text"] = ''

              for a in [0...$scope.selectedTemplate.meta.html.links.length]
                if not $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name]?
                  $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name] =
                    type  : 'url'

              for a in [0...$scope.selectedTemplate.meta.text.links.length]
                if not $scope.textLinks[$scope.selectedTemplate.meta.text.links[a].name]?
                  $scope.textLinks[$scope.selectedTemplate.meta.text.links[a].name] =
                    type  : 'url'

            $scope.hasActiveLinks = true
            $scope.activeLinkTab = 'html' + $scope.selectedTemplate.meta.html.links[0].name
          else if $scope.selectedTemplate?.meta.text.links.length > 0
            $scope.hasActiveLinks = true
            $scope.activeLinkTab = 'text' + $scope.selectedTemplate.meta.text.links[0].name
          if MessageLibrarySrv.defaultVersion()?
            $scope.defaultTemplate = MessageLibrarySrv.getTemplateById MessageLibrarySrv.defaultVersion().template.value
    , true

    $scope.$watch ('service.landingPages()'), -> $scope.availableLandingPages = MessageLibrarySrv.landingPages()

    $scope.doesLinkHaveNeeds = (link) ->
      if link.needs?
        link.needs[0].indexOf('text') != -1
      else
        false

    $scope.onHTMLLinkChange = ->
      MessageLibrarySrv.updateMessage 'html','links',$scope.htmlLinks

    $scope.onTextLinkChange = ->
      MessageLibrarySrv.updateMessage 'text','links',$scope.textLinks

    $scope.onUseDefaultChange = ->
      _.forEach $scope.htmlLinks, (value, key) ->
        if $scope.htmlLinks[key].ref == 'default'
          $scope.htmlLinks[key] = MessageLibrarySrv.defaultVersion().html.links[key]

      _.forEach $scope.textLinks, (value, key) ->
        if $scope.textLinks[key].ref == 'default'
          $scope.textLinks[key] = MessageLibrarySrv.defaultVersion().text.links[key]

        MessageLibrarySrv.updateMessage 'html','links', $scope.htmlLinks
        MessageLibrarySrv.updateMessage 'text','links', $scope.textLinks

    $scope.linkTabClick = (id) ->
      $scope.activeLinkTab = id
  ]

  .controller 'MessageLibraryHtmlBlocksCtrl',
  ['$scope','MessageLibrarySrv',
  ($scope, MessageLibrarySrv) ->
    $scope.activeHtmlTab = ''
    $scope.service = MessageLibrarySrv
    $scope.hasActiveHTMLBlocks = false
    $scope.currentHTMLBlocks = {}
    $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()

    $scope['editor.htmlblock'] = extraPlugins: 'strinsert'

    $scope.$watch ('service.versionBeingEdited()'), ->
      if MessageLibrarySrv.versionBeingEdited().html?.blocks?
        $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
        $scope.currentHTMLBlocks = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().html.blocks)
        $scope.hasActiveHTMLBlocks = false
        $scope.activeHtmlTab = ''
        if $scope.currentVersion.template.value != "-1"
          $scope.selectedTemplate = MessageLibrarySrv.getTemplateById $scope.currentVersion.template.value
          if MessageLibrarySrv.defaultVersion()?
            $scope.defaultTemplate = MessageLibrarySrv.getTemplateById MessageLibrarySrv.defaultVersion().template.value
          if $scope.selectedTemplate?.meta.html.blocks.length > 0
            $scope.hasActiveHTMLBlocks = true
            $scope.activeHtmlTab = $scope.selectedTemplate.meta.html.blocks[0].name
    , true

    $scope.onBlockChange = ->
      MessageLibrarySrv.updateMessage 'html','blocks', $scope.currentHTMLBlocks

    $scope.onUseDefaultChange = ->
      _.forEach $scope.currentHTMLBlocks, (value, key) ->
        if $scope.currentHTMLBlocks[key].ref == 'default'
          $scope.currentHTMLBlocks[key].value = MessageLibrarySrv.defaultVersion().html.blocks[key].value
      MessageLibrarySrv.updateMessage 'html','blocks', $scope.currentHTMLBlocks

    $scope.htmlTabClick = (key) ->
      $scope.activeHtmlTab = key
  ]

  .controller 'MessageLibraryPlainTextCtrl',
  ['$scope','MessageLibrarySrv',
  ($scope, MessageLibrarySrv) ->

    $scope.activePlainTextTab = ''
    $scope.currentPlainText = []
    $scope.hasActivePlainTexts = false
    $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()

    $scope.$watch ('service.versionBeingEdited()'), ->
      if MessageLibrarySrv.versionBeingEdited().text?
        $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
        $scope.currentPlainText = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().text.blocks)
        $scope.hasActivePlainTexts = false
        $scope.activePlainTextTab = ''

        if $scope.currentVersion.template.value != "-1"
          $scope.selectedTemplate = MessageLibrarySrv.getTemplateById $scope.currentVersion.template.value
          if MessageLibrarySrv.defaultVersion()?
            $scope.defaultTemplate = MessageLibrarySrv.getTemplateById MessageLibrarySrv.defaultVersion().template.value

          if $scope.selectedTemplate?.meta.text.blocks.length > 0
            $scope.hasActivePlainTexts = true
            $scope.activePlainTextTab = $scope.selectedTemplate.meta.text.blocks[0].name
    , true

    $scope.onPlainTextChange = ->
      MessageLibrarySrv.updateMessage 'text','blocks',$scope.currentPlainText

    $scope.onUseDefaultChange = ->
      _.forEach $scope.currentPlainText, (value, key) ->
        if $scope.currentPlainText[key].ref == 'default'
          $scope.currentPlainText[key].value = MessageLibrarySrv.defaultVersion().text.blocks[key].value
      MessageLibrarySrv.updateMessage 'text','blocks',$scope.currentPlainText

    $scope.plainTextTabClick = (key) ->
      $scope.activePlainTextTab = key
  ]

  .controller 'MessageLibraryPoliciesCtrl',
  ['$scope','MessageLibrarySrv',
  ($scope, MessageLibrarySrv) ->
    $scope.service = MessageLibrarySrv
    $scope.availablePolicies = MessageLibrarySrv.policies()
    $scope.htmlPolicies = {}
    $scope.textPolicies = {}
    $scope.hasActivePolicies = false
    $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()

    $scope.$watch ('service.versionBeingEdited()'), ->
      if MessageLibrarySrv.versionBeingEdited().html?.policies?
        $scope.currentVersion = MessageLibrarySrv.versionBeingEdited()
        $scope.textPolicies = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().text.policies)
        $scope.htmlPolicies = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().html.policies)
        if $scope.currentVersion.template.value != "-1"
          $scope.selectedTemplate = MessageLibrarySrv.getTemplateById $scope.currentVersion.template.value
          $scope.hasActivePolicies = $scope.selectedTemplate?.meta.html.policies.length > 0 || $scope.selectedTemplate?.meta.text.policies.length > 0
          if MessageLibrarySrv.defaultVersion()?
            $scope.defaultTemplate = MessageLibrarySrv.getTemplateById MessageLibrarySrv.defaultVersion().template.value
    , true

    $scope.$watch ('service.policies()'), ->
      $scope.availablePolicies = MessageLibrarySrv.policies()

    $scope.onHTMLPolicyChange = ->
      $scope.htmlPolicies
      MessageLibrarySrv.updateMessage 'html','policies', $scope.htmlPolicies

    $scope.onTextPolicyChange = ->
      MessageLibrarySrv.updateMessage 'text','policies', $scope.textPolicies

    $scope.onUseDefaultChange = ->
      _.forEach $scope.htmlPolicies, (value, key) ->
        if $scope.htmlPolicies[key].ref == 'default'
          $scope.htmlPolicies[key].value = MessageLibrarySrv.defaultVersion().html.policies[key].value

      _.forEach $scope.textPolicies, (value, key) ->
        if $scope.textPolicies[key].ref == 'default'
          $scope.textPolicies[key].value = MessageLibrarySrv.defaultVersion().text.policies[key].value

      MessageLibrarySrv.updateMessage 'html','policies', $scope.htmlPolicies
      MessageLibrarySrv.updateMessage 'text','policies', $scope.textPolicies


  ]

  .controller 'MessageLibraryPreviewCtrl',
  ['$scope', 'MessageLibrarySrv',
  ($scope, MessageLibrarySrv) ->

    $scope.service = MessageLibrarySrv
    $scope.htmlPreview = ''
    $scope.preview = reload: 0

    $scope.$watch ('service.versionBeingEdited()'), ->
      $scope.selectedTemplateId = MessageLibrarySrv.versionBeingEdited().template
      if $scope.selectedTemplateId?.value?
        $scope.selectedTemplateId = $scope.selectedTemplateId.value
    , true

    $scope.$watch ('service.getMessageRevision()'), ->
      reloadPreview()
    , true

    reloadPreview = ->
      $scope.messageId = MessageLibrarySrv.fetchMessageId()
      $scope.messageProgress = MessageLibrarySrv.messageStatus()
      if $scope.messageProgress == 'completed' && $scope.selectedTemplateId != "-1" && $scope.messageId != "new"
        $scope.preview.reload = Math.random()
  ]

  .controller 'AddVersionToMessageModalCtrl',
    ['$scope', '$uibModalInstance', 'dismissCheck',
    'saveTimeout', 'errorTimeout', 'checkLocation',
    'message', 'currentVersion', 'MessageLibrarySrv', '$state',
    ($scope, $uibModalInstance, dismissCheck, saveTimeout,
    errorTimeout, checkLocation, message, currentVersion,
    MessageLibrarySrv, $state) ->
      $scope.currentVersion = currentVersion
      $scope.forms = {}
      ref = {}
      status_ref = {}
      $scope.message = message
      active_attributes = _.pluck MessageLibrarySrv.attributes(), 'id'
      $scope.attributes = MessageLibrarySrv.attributes()
      $scope.currentVersion.filters = {}
      $scope.currentVersion.filters[k] = [] for k in active_attributes when k not of $scope.currentVersion.filters
      delete $scope.currentVersion.filters[key] for key of $scope.currentVersion.filters when key not in active_attributes

      $scope.$watch 'currentVersion.filters', (newValue, oldValue) ->
        return unless newValue

      $scope.selectall = ({id, values}) ->
        $scope.currentVersion.filters[id] = _.uniq $scope.currentVersion.filters[id].concat _.pluck values, 'id'
        false
      $scope.deselectall = ({id, values}) ->
        $scope.currentVersion.filters[id] = []
        false

      $scope.createMessageVersion = ->
        MessageLibrarySrv.addVersion $scope.message, $scope.currentVersion
        $uibModalInstance.dismiss()
        message = MessageLibrarySrv.getCurrentMessageId()
        version  = $scope.currentVersion.id
        $state.go 'manage-messagelibrary.message.version.state',
          message: message
          version: version
          state: 'templates'

      $scope.close = ->
        $uibModalInstance.dismiss()
    ]

  .controller 'EditVersionAttributesModalCtrl',
    ['$scope', '$uibModalInstance', 'dismissCheck',
    'saveTimeout', 'errorTimeout', 'checkLocation',
    'message', 'currentVersion', 'MessageLibrarySrv', '$state',
    ($scope, $uibModalInstance, dismissCheck, saveTimeout,
    errorTimeout, checkLocation, message, currentVersion,
    MessageLibrarySrv, $state) ->
      $scope.currentVersion = currentVersion
      $scope.forms = {}
      ref = {}
      status_ref = {}
      $scope.message = message
      $scope.attributes = MessageLibrarySrv.attributes()

      if _.isArray($scope.currentVersion.filters)
        active_attributes = _.pluck MessageLibrarySrv.attributes(), 'id'
        $scope.currentVersion.filters = {}
        $scope.currentVersion.filters[k] = [] for k in active_attributes when k not of $scope.currentVersion.filters
        delete $scope.currentVersion.filters[key] for key of $scope.currentVersion.filters when key not in active_attributes

      $scope.$watch 'currentVersion.filters', (newValue, oldValue) ->
        return unless newValue
        if not _.isEqual newValue, oldValue
          $scope.forms.currentVersion.$setDirty()
      , true

      $scope.selectall = ({id, values}) ->
        $scope.currentVersion.filters[id] = _.uniq $scope.currentVersion.filters[id].concat _.pluck values, 'id'
      $scope.deselectall = ({id, values}) ->
        $scope.currentVersion.filters[id] = []

      $scope.saveVersionAttributes = ->
        MessageLibrarySrv.saveVersionAttributes $scope.message, $scope.currentVersion
        $uibModalInstance.dismiss()

      $scope.close = ->
        if $scope.forms.currentVersion.saved and $scope.forms.currentVersion.$pristine
          $uibModalInstance.close $scope.status
        else if $scope.forms.currentVersion.$invalid or $scope.forms.currentVersion.$dirty or not _.isEqual($scope.currentVersion.filters, ref)
          dismissCheck.dismiss $uibModalInstance, item: $scope.status, ref: status_ref, save: $scope.save
        else
          $uibModalInstance.dismiss()
    ]

  .controller 'CloneVersionModalCtrl',
    ['$scope', '$uibModalInstance', 'dismissCheck',
    'saveTimeout', 'errorTimeout', 'checkLocation',
    'message', 'version', 'MessageLibrarySrv', '$state',
    ($scope, $uibModalInstance, dismissCheck, saveTimeout,
    errorTimeout, checkLocation, message, version,
    MessageLibrarySrv, $state) ->
      $scope.currentVersion = _.cloneDeep version
      $scope.currentVersion.name = $scope.currentVersion.name + " (Cloned)"
      $scope.cloneTitle = _.cloneDeep version
      $scope.forms = {}
      ref = {}
      status_ref = {}
      $scope.message = message
      $scope.attributes = MessageLibrarySrv.attributes()

      if _.isArray($scope.currentVersion.filters)
        active_attributes = _.pluck MessageLibrarySrv.attributes(), 'id'
        $scope.currentVersion.filters = {}
        $scope.currentVersion.filters[k] = [] for k in active_attributes when k not of $scope.currentVersion.filters
        delete $scope.currentVersion.filters[key] for key of $scope.currentVersion.filters when key not in active_attributes

      $scope.selectall = ({id, values}) ->
        if not $scope.currentVersion.filters[id]?
          $scope.currentVersion.filters[id] = []
        $scope.currentVersion.filters[id] = _.uniq $scope.currentVersion.filters[id].concat _.pluck values, 'id'
        false
      $scope.deselectall = ({id, values}) ->
        $scope.currentVersion.filters[id] = []
        false

      $scope.createMessageVersion = ->
        $scope.currentVersion.id = uuid.v4()
        MessageLibrarySrv.addVersion $scope.message, $scope.currentVersion
        $uibModalInstance.dismiss()
        message = MessageLibrarySrv.getCurrentMessageId()
        version  = $scope.currentVersion.id
        $state.go 'manage-messagelibrary.message.version.state',
          message: message
          version: version
          state: 'templates'


      $scope.close = ->
        $uibModalInstance.dismiss()
    ]

  .controller 'DeleteVersionModalCtrl',
    ['$scope', '$uibModalInstance', 'message', 'version', 'MessageLibrarySrv',
    ($scope, $uibModalInstance, message, version, MessageLibrarySrv) ->
      $scope.version = version
      $scope.message = message

      $scope.versionDeleteConfirmed = ->
        MessageLibrarySrv.removeVersion(message, version)
        $uibModalInstance.dismiss()

      $scope.cancelVersionDelete = ->
        $uibModalInstance.dismiss()
  ]
