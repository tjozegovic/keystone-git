(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('app.manage').config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      return $stateProvider.state('manage-messagelibrary', {
        data: {
          section: 'manage'
        },
        templateUrl: '/html/manage/message-library.html',
        url: '/manage/message-library'
      }).state('manage-messagelibrary.message', {
        url: '/:message'
      }).state('manage-messagelibrary.message.version', {
        url: '/:version'
      }).state('manage-messagelibrary.message.version.state', {
        url: '/:state'
      }).state('manage-messagedripmanager', {
        data: {
          section: 'manage'
        },
        templateUrl: '/html/manage/message-drip-manager.html',
        url: '/manage/message-drip-manager'
      }).state('manage-messagedripmanager.status', {
        url: '/:status'
      }).state('manage-messagedripmanager.status.segment', {
        url: '/:segment'
      });
    }
  ]).service('MessageLibrarySrv', [
    '$http', '$stateParams', '$state', '$q', function($http, $stateParams, $state, $q, httpRequest) {
      var assign, breadcrumbSegment, breadcrumbStatus, checkBlockTexts, cleanUpMessageToSend, clientAttributes, clientLandingPages, clientPolicies, completedMessagesInLibrary, condensedAttributes, createDefaultVersion, currentMessageTemplateBeingEdited, currentOrder, currentStatuses, currentTemplate, currentVersion, draftMessagesInLibrary, fixBlockText, messageLibraryIsLoading, messageProgress, messageSlugName, messagesInCampaigns, messagesTemplatesTemp, preCleanUp, that, versionsWithGeneric, wizardPosition;
      currentMessageTemplateBeingEdited = {
        message: {}
      };
      currentVersion = {};
      currentTemplate = void 0;
      clientAttributes = [];
      condensedAttributes = [];
      clientLandingPages = [];
      clientPolicies = [];
      versionsWithGeneric = [];
      completedMessagesInLibrary = [];
      draftMessagesInLibrary = [];
      messagesTemplatesTemp = [];
      wizardPosition = null;
      messageLibraryIsLoading = false;
      messageProgress = 'draft';
      messageSlugName = '';
      breadcrumbStatus = void 0;
      breadcrumbSegment = void 0;
      messagesInCampaigns = {};
      currentStatuses = {};
      currentOrder = [];
      that = this;
      this.loadMessages = function(override) {
        var urls;
        if (override == null) {
          override = false;
        }
        if (override) {
          messageLibraryIsLoading = false;
        }
        if (messageLibraryIsLoading === false) {
          messageLibraryIsLoading = true;
          urls = ['templates?q=active', 'messages?q=all', 'attributes?q=active', 'policies', 'pages', 'statuses?q=active'];
          return $q.all(_.map(urls, function(url) {
            return $http.get("/api/" + url);
          })).then(function(arg) {
            var a, attributes, b, currentOrderTemp, i, j, l, len, len1, len2, messageToSet, messages, messagesInCampaignsTemp, pages, policies, ref1, ref2, statuses, temp, templates;
            templates = arg[0], messages = arg[1], attributes = arg[2], policies = arg[3], pages = arg[4], statuses = arg[5];
            completedMessagesInLibrary = [];
            draftMessagesInLibrary = [];
            messagesTemplatesTemp = templates.data;
            clientAttributes = attributes.data;
            ref1 = attributes.data;
            for (i = 0, len = ref1.length; i < len; i++) {
              a = ref1[i];
              ref2 = a.values;
              for (j = 0, len1 = ref2.length; j < len1; j++) {
                b = ref2[j];
                condensedAttributes.push({
                  attributeId: b.id,
                  attributeName: b.name
                });
              }
            }
            currentStatuses = _.cloneDeep(statuses.data);
            currentOrderTemp = [];
            _.forIn(currentStatuses, function(value) {
              if (_.indexOf(currentOrderTemp, value.id) === -1) {
                return currentOrderTemp.push(value.id);
              }
            });
            currentOrder = _.cloneDeep(currentOrderTemp);
            messagesInCampaignsTemp = {};
            _.forOwn(currentStatuses, function(status, key) {
              var dripItem, l, ref3, results;
              results = [];
              for (a = l = 0, ref3 = status.segments.length; 0 <= ref3 ? l < ref3 : l > ref3; a = 0 <= ref3 ? ++l : --l) {
                results.push((function() {
                  var m, ref4, results1;
                  results1 = [];
                  for (b = m = 0, ref4 = status.segments[a].steps.length; 0 <= ref4 ? m < ref4 : m > ref4; b = 0 <= ref4 ? ++m : --m) {
                    if (status.segments[a].steps[b].message != null) {
                      if (messagesInCampaignsTemp[status.segments[a].steps[b].message] == null) {
                        messagesInCampaignsTemp[status.segments[a].steps[b].message] = [];
                      }
                      dripItem = {};
                      dripItem.status = status.name;
                      dripItem.segment = status.segments[a].name;
                      dripItem.drip = status.segments[a].steps[b];
                      results1.push(messagesInCampaignsTemp[status.segments[a].steps[b].message].push(dripItem));
                    } else {
                      results1.push(void 0);
                    }
                  }
                  return results1;
                })());
              }
              return results;
            });
            messagesInCampaigns = _.cloneDeep(messagesInCampaignsTemp);
            clientLandingPages = pages.data;
            clientPolicies = policies.data;
            temp = messages.data;
            for (l = 0, len2 = temp.length; l < len2; l++) {
              a = temp[l];
              if (a.name == null) {
                a.name = a.subject;
              }
              if ((a.hidden == null) || a.hidden === false) {
                if (a.progressStatus === 'draft') {
                  draftMessagesInLibrary.push(a);
                } else {
                  _.forEach(a.versions, function(value) {
                    if (value.id == null) {
                      value.id = uuid.v4();
                    }
                    if (value.hidden == null) {
                      value.hidden = false;
                    }
                    if (value.id === 'default' && value.name === '') {
                      value.name = "Default Version";
                    }
                    if (value.isDefault != null) {
                      delete value["isDefault"];
                    }
                    return true;
                  });
                  completedMessagesInLibrary.push(a);
                }
              }
            }
            messageToSet = _.find(_.union(completedMessagesInLibrary, draftMessagesInLibrary), {
              'id': $stateParams.message
            });
            if (messageToSet != null) {
              that.setMessageTemplateToBeEdited(messageToSet);
              if ((currentMessageTemplateBeingEdited.message.versions == null) || currentMessageTemplateBeingEdited.message.versions.length === 0) {
                createDefaultVersion();
              }
              preCleanUp();
              that.updateVersionWithGeneric();
              if ($stateParams.version === 'default' || $stateParams.version === '') {
                that.setCurrentVersionById('default');
              } else {
                that.setCurrentVersionById($stateParams.version);
              }
              return that.isMessageDraft();
            } else if ($state.current.name === 'manage-messagelibrary') {
              return $state.go('manage-messagelibrary');
            }
          });
        }
      };
      this.loadMessages();
      this.messageTemplates = function() {
        return messagesTemplatesTemp;
      };
      this.completedMessages = function() {
        return completedMessagesInLibrary;
      };
      this.messagesInDraft = function() {
        return draftMessagesInLibrary;
      };
      this.campaignMessages = function() {
        return messagesInCampaigns;
      };
      this.attributes = function() {
        return clientAttributes;
      };
      this.statuses = function() {
        return currentStatuses;
      };
      this.order = function() {
        return currentOrder;
      };
      this.policies = function() {
        return clientPolicies;
      };
      this.landingPages = function() {
        return clientLandingPages;
      };
      this.messageStatus = function() {
        return messageProgress;
      };
      this.currentTemplate = function() {
        return currentTemplate;
      };
      this.versionBeingEdited = function() {
        return currentVersion;
      };
      this.fetchMessageId = function() {
        var ref1;
        if ((currentMessageTemplateBeingEdited != null ? (ref1 = currentMessageTemplateBeingEdited.message) != null ? ref1.id : void 0 : void 0) != null) {
          return currentMessageTemplateBeingEdited.message.id;
        } else {
          return '';
        }
      };
      this.getCurrentMessageId = function() {
        if ((currentMessageTemplateBeingEdited != null ? currentMessageTemplateBeingEdited.message : void 0) != null) {
          if (currentMessageTemplateBeingEdited.message.id != null) {
            return currentMessageTemplateBeingEdited.message.id;
          } else {
            currentMessageTemplateBeingEdited.message.id = 'new';
            return currentMessageTemplateBeingEdited.message.id;
          }
        } else {
          currentMessageTemplateBeingEdited = {
            message: {
              id: 'new'
            }
          };
          return currentMessageTemplateBeingEdited.message.id;
        }
      };
      this.getAttributesById = function(attribute_ids) {
        var attribute_names;
        attribute_names = [];
        _.forEach(attribute_ids, function(value, key) {
          var a, i, ref1, results;
          results = [];
          for (a = i = 0, ref1 = value.length; 0 <= ref1 ? i < ref1 : i > ref1; a = 0 <= ref1 ? ++i : --i) {
            results.push(attribute_names.push(_.result(_.find(condensedAttributes, {
              'attributeId': value[a]
            }), 'attributeName')));
          }
          return results;
        });
        return attribute_names;
      };
      this.setWizardPosition = function(position) {
        return wizardPosition = _.clone(position);
      };
      this.getWizardPosition = function() {
        return wizardPosition;
      };
      this.getMessageRevision = function() {
        return currentMessageTemplateBeingEdited.message.version;
      };
      this.getTemplateById = function(id) {
        return _.find(messagesTemplatesTemp, {
          'id': id
        });
      };
      this.whereIsTemplateBeingUsed = function(templateId) {
        var messages;
        return messages = _.filter(_.union(completedMessagesInLibrary, draftMessagesInLibrary), function(message) {
          if (message.template != null) {
            if (message.template.value != null) {
              return message.template.value === templateId;
            } else {
              return message.template === templateId;
            }
          } else {
            return false;
          }
        });
      };
      this.setMessageTemplateToBeEdited = function(template) {
        var ref1;
        currentMessageTemplateBeingEdited = {};
        currentMessageTemplateBeingEdited.message = _.cloneDeep(template);
        if (currentMessageTemplateBeingEdited.message.template != null) {
          if (currentMessageTemplateBeingEdited.message.template.value == null) {
            currentMessageTemplateBeingEdited.message.template = {
              value: currentMessageTemplateBeingEdited.message.template,
              ref: 'default'
            };
          }
        }
        if (currentMessageTemplateBeingEdited.message.templateFromDefault != null) {
          delete currentMessageTemplateBeingEdited.message["templateFromDefault"];
        }
        checkBlockTexts();
        if (((ref1 = currentMessageTemplateBeingEdited.message) != null ? ref1.slug : void 0) != null) {
          return messageSlugName = currentMessageTemplateBeingEdited.message.slug;
        } else {
          messageSlugName = '';
          return currentMessageTemplateBeingEdited.message.slug = '';
        }
      };
      checkBlockTexts = function() {
        var a, i, ref1, results, versionValue;
        _.forEach(currentMessageTemplateBeingEdited.message.html.blocks, function(value, key) {
          return currentMessageTemplateBeingEdited.message.html.blocks[key] = fixBlockText(value);
        });
        _.forEach(currentMessageTemplateBeingEdited.message.text.blocks, function(value, key) {
          return currentMessageTemplateBeingEdited.message.text.blocks[key] = fixBlockText(value);
        });
        results = [];
        for (a = i = 0, ref1 = currentMessageTemplateBeingEdited.message.versions; 0 <= ref1 ? i < ref1 : i > ref1; a = 0 <= ref1 ? ++i : --i) {
          versionValue = currentMessageTemplateBeingEdited.message.versions[a];
          _.forEach(versionValue.html.blocks, function(value, key) {
            return currentMessageTemplateBeingEdited.message.versions[a].html.blocks[key] = fixBlockText(value);
          });
          results.push(_.forEach(versionValue.text.blocks, function(value, key) {
            return currentMessageTemplateBeingEdited.message.versions[a].html.blocks[key] = fixBlockText(value);
          }));
        }
        return results;
      };
      fixBlockText = function(value) {
        if ((value != null ? value.blockText : void 0) != null) {
          value.value = value.blockText;
          return delete value["blockText"];
        } else {
          return value;
        }
      };
      this.parseStateParams = function() {
        var messageToSet, ref1;
        if ((currentMessageTemplateBeingEdited.message.id == null) || currentMessageTemplateBeingEdited.message.id !== $stateParams.message || ((ref1 = this.versionBeingEdited()) != null ? ref1.id : void 0) !== $stateParams.version) {
          currentMessageTemplateBeingEdited = {};
          currentVersion = {};
          if ($stateParams.message !== 'new' && $stateParams.message !== '') {
            if ((completedMessagesInLibrary.length > 0 || draftMessagesInLibrary.length > 0) && messagesTemplatesTemp.length > 0) {
              messageToSet = _.find(_.union(completedMessagesInLibrary, draftMessagesInLibrary), {
                'id': $stateParams.message
              });
              if (messageToSet != null) {
                this.setMessageTemplateToBeEdited(messageToSet);
                if ((currentMessageTemplateBeingEdited.message.versions == null) || currentMessageTemplateBeingEdited.message.versions.length === 0) {
                  createDefaultVersion();
                }
                preCleanUp();
                this.updateVersionWithGeneric();
                if ($stateParams.version === 'default' || $stateParams.version === '') {
                  this.setCurrentVersionById('default');
                } else {
                  this.setCurrentVersionById($stateParams.version);
                }
                this.isMessageDraft();
              } else {
                $state.go('manage-messagelibrary');
              }
            } else {
              this.createNewMessageTemplate();
              this.loadMessages();
            }
          } else {
            this.createNewMessageTemplate();
            this.updateVersionWithGeneric();
            this.setCurrentVersionById('default');
          }
        } else {
          this.updateVersionWithGeneric();
          this.isMessageDraft();
        }
        if ((currentMessageTemplateBeingEdited == null) && $state.current.name === 'manage-messagelibrary') {
          $state.go('manage-messagelibrary');
          return wizardPosition = null;
        } else if ($stateParams.state) {
          switch ($stateParams.state.toLowerCase()) {
            case '':
              return that.setWizardPosition(null);
            case 'templates':
              return that.setWizardPosition(0);
            case 'subjects':
              return that.setWizardPosition(1);
            case 'links':
              return that.setWizardPosition(2);
            case 'html':
              return that.setWizardPosition(3);
            case 'plaintext':
              return that.setWizardPosition(4);
            case 'policies':
              return that.setWizardPosition(5);
            case 'confirmation':
              return that.setWizardPosition(6);
          }
        }
      };
      this.createNewMessageTemplate = function() {
        var default_version;
        messageProgress = 'draft';
        currentMessageTemplateBeingEdited = {};
        currentMessageTemplateBeingEdited = {
          message: {
            id: $stateParams.message != null ? $stateParams.message : 'new',
            name: '',
            template: {
              value: '-1',
              ref: ''
            },
            subject: {
              value: '',
              ref: 'default'
            },
            html: {
              blocks: {},
              links: {},
              policies: {}
            },
            text: {
              blocks: {},
              links: {},
              policies: {}
            },
            progressStatus: 'draft',
            versions: []
          }
        };
        default_version = _.cloneDeep(currentMessageTemplateBeingEdited.message);
        default_version.id = 'default';
        default_version.hidden = false;
        delete default_version["progressStatus"];
        delete default_version["versions"];
        currentMessageTemplateBeingEdited.message.versions.push(default_version);
        this.setMessageTemplateName("");
        this.updateVersionWithGeneric();
        return this.setCurrentVersionById('default');
      };
      this.setMessageTemplateName = function(name) {
        currentMessageTemplateBeingEdited.message.name = name;
        this.isMessageDraft();
        return false;
      };
      this.isMessageDraft = function() {
        var a, i, isMessageComplete, j, l, m, n, o, p, ref1, ref2, ref3, ref4, ref5, ref6, ref7, value, versionTemplate, versionToLookAt;
        isMessageComplete = true;
        if (currentMessageTemplateBeingEdited.message.versions.length === 0) {
          isMessageComplete = false;
        }
        for (a = i = 0, ref1 = currentMessageTemplateBeingEdited.message.versions.length; 0 <= ref1 ? i < ref1 : i > ref1; a = 0 <= ref1 ? ++i : --i) {
          versionToLookAt = currentMessageTemplateBeingEdited.message.versions[a];
          if ((versionToLookAt.template.ref != null) && versionToLookAt.template.ref === "default") {
            versionTemplate = this.getTemplateById(currentMessageTemplateBeingEdited.message.versions[0].template.value);
          } else {
            versionTemplate = this.getTemplateById(versionToLookAt.template.value);
          }
          if ((versionToLookAt.hidden == null) || versionToLookAt.hidden === false) {
            if (versionTemplate == null) {
              isMessageComplete = false;
            }
            if (versionToLookAt.template.value === -1) {
              isMessageComplete = false;
            }
            if (versionTemplate == null) {
              isMessageComplete = false;
            }
            if (versionToLookAt.subject.ref !== 'default' && (versionToLookAt.subject.value == null) || versionToLookAt.subject.value === '') {
              isMessageComplete = false;
            }
            if (isMessageComplete) {
              for (a = j = 0, ref2 = versionTemplate.meta.html.blocks.length; 0 <= ref2 ? j < ref2 : j > ref2; a = 0 <= ref2 ? ++j : --j) {
                value = versionToLookAt.html.blocks[versionTemplate.meta.html.blocks[a].name];
                if (value != null) {
                  if (value.ref !== "default" && ((value.value == null) || value.value === '')) {
                    isMessageComplete = false;
                  }
                } else {
                  isMessageComplete = false;
                }
              }
            }
            if (isMessageComplete) {
              for (a = l = 0, ref3 = versionTemplate.meta.text.blocks.length; 0 <= ref3 ? l < ref3 : l > ref3; a = 0 <= ref3 ? ++l : --l) {
                value = versionToLookAt.text.blocks[versionTemplate.meta.text.blocks[a].name];
                if (value != null) {
                  if (value.ref !== "default" && ((value.value == null) || value.value === '')) {
                    isMessageComplete = false;
                  }
                } else {
                  isMessageComplete = false;
                }
              }
            }
            if (isMessageComplete) {
              for (a = m = 0, ref4 = versionTemplate.meta.html.links.length; 0 <= ref4 ? m < ref4 : m > ref4; a = 0 <= ref4 ? ++m : --m) {
                value = versionToLookAt.html.links[versionTemplate.meta.html.links[a].name];
                if (value != null) {
                  if (value.ref !== "default" && ((value.value == null) || value.value === '')) {
                    isMessageComplete = false;
                  }
                  if ((versionTemplate.meta.html.links[a].needs != null) && versionTemplate.meta.html.links[a].needs[0].indexOf('text') !== -1) {
                    if ((value.text == null) || value.text === '') {
                      isMessageComplete = false;
                    }
                  }
                } else {
                  isMessageComplete = false;
                }
              }
            }
            if (isMessageComplete) {
              for (a = n = 0, ref5 = versionTemplate.meta.text.links.length; 0 <= ref5 ? n < ref5 : n > ref5; a = 0 <= ref5 ? ++n : --n) {
                value = versionToLookAt.text.links[versionTemplate.meta.text.links[a].name];
                if (value != null) {
                  if (value.ref !== "default" && ((value.value == null) || value.value === '')) {
                    isMessageComplete = false;
                  }
                } else {
                  isMessageComplete = false;
                }
              }
            }
            if (isMessageComplete) {
              for (a = o = 0, ref6 = versionTemplate.meta.html.policies.length; 0 <= ref6 ? o < ref6 : o > ref6; a = 0 <= ref6 ? ++o : --o) {
                value = versionToLookAt.html.policies[versionTemplate.meta.html.policies[a].name];
                if (value != null) {
                  if (value.ref !== "default" && ((value.value == null) || value.value === '')) {
                    isMessageComplete = false;
                  }
                } else {
                  isMessageComplete = false;
                }
              }
            }
            if (isMessageComplete) {
              for (a = p = 0, ref7 = versionTemplate.meta.text.policies.length; 0 <= ref7 ? p < ref7 : p > ref7; a = 0 <= ref7 ? ++p : --p) {
                value = versionToLookAt.text.policies[versionTemplate.meta.text.policies[a].name];
                if (value != null) {
                  if (value.ref !== "default" && ((value.value == null) || value.value === '')) {
                    isMessageComplete = false;
                  }
                } else {
                  isMessageComplete = false;
                }
              }
            }
          }
        }
        if (isMessageComplete) {
          messageProgress = 'completed';
          currentMessageTemplateBeingEdited.message.progressStatus = 'completed';
          return 'completed';
        } else {
          messageProgress = 'draft';
          currentMessageTemplateBeingEdited.message.progressStatus = 'draft';
          return 'draft';
        }
      };
      this.messageTemplateName = function() {
        var ref1;
        return (currentMessageTemplateBeingEdited != null ? (ref1 = currentMessageTemplateBeingEdited.message) != null ? ref1.name : void 0 : void 0) || '';
      };
      this.getMessageSlugName = function() {
        var ref1;
        if ((currentMessageTemplateBeingEdited != null ? (ref1 = currentMessageTemplateBeingEdited.message) != null ? ref1.slug : void 0 : void 0) != null) {
          return currentMessageTemplateBeingEdited.message.slug;
        } else {
          return '';
        }
      };
      this.messageSlugNameSet = function(slug) {
        messageSlugName = slug;
        return currentMessageTemplateBeingEdited.message.slug = slug;
      };
      this.currentMessageTemplateId = function() {
        var base;
        if (typeof (base = this.versionBeingEdited()).template === "function" ? base.template(typeof value !== "undefined" && value !== null) : void 0) {
          return this.versionBeingEdited().template.value;
        } else {
          return -1;
        }
      };
      this.setMessageTemplateCreativeTemplate = function(template) {
        currentVersion.template = {
          value: template.id
        };
        if (currentVersion.id === 'default') {
          currentVersion.template.ref = 'default';
        } else {
          currentVersion.template.ref = 'self';
        }
        this.updateVersionWithGeneric();
        return this.isMessageDraft();
      };
      this.addVersion = function(message, version) {
        var newVersion;
        currentMessageTemplateBeingEdited = {};
        currentMessageTemplateBeingEdited.message = _.cloneDeep(message);
        this.updateVersionWithGeneric();
        this.versionsWithGeneric = currentMessageTemplateBeingEdited.message.versions;
        newVersion = {
          id: version.id,
          template: {
            value: message.versions[0].template.value,
            ref: 'default'
          },
          name: version.name,
          filters: version.filters,
          hidden: false,
          subject: {
            value: '',
            ref: 'default'
          },
          html: {
            blocks: _.forEach(_.cloneDeep(currentMessageTemplateBeingEdited.message.html.blocks), function(value) {
              return value.ref = "default";
            }),
            links: _.forEach(_.cloneDeep(currentMessageTemplateBeingEdited.message.html.links), function(value) {
              return value.ref = "default";
            }),
            policies: _.forEach(_.cloneDeep(currentMessageTemplateBeingEdited.message.html.policies), function(value) {
              return value.ref = "default";
            })
          },
          text: {
            blocks: _.forEach(_.cloneDeep(currentMessageTemplateBeingEdited.message.text.blocks), function(value) {
              return value.ref = "default";
            }),
            links: _.forEach(_.cloneDeep(currentMessageTemplateBeingEdited.message.text.links), function(value) {
              return value.ref = "default";
            }),
            policies: _.forEach(_.cloneDeep(currentMessageTemplateBeingEdited.message.text.policies), function(value) {
              return value.ref = "default";
            })
          }
        };
        if (_.isObject(_.cloneDeep(this.defaultVersion().subject))) {
          newVersion.subject.value = _.cloneDeep(this.defaultVersion().subject).value;
        } else {
          newVersion.subject.value = _.cloneDeep(this.defaultVersion().subject);
        }
        currentMessageTemplateBeingEdited.message.versions.push(newVersion);
        this.setCurrentVersionById(version.id);
        return this.isMessageDraft();
      };
      this.removeVersion = function(message, version) {
        var versionToArchive;
        currentMessageTemplateBeingEdited = {
          message: message
        };
        versionToArchive = _.find(currentMessageTemplateBeingEdited.message.versions, {
          'id': version.id
        });
        versionToArchive.hidden = true;
        return this.saveMessageToLibrary();
      };
      this.saveVersionAttributes = function(message, version) {
        currentMessageTemplateBeingEdited = {
          message: _.cloneDeep(message)
        };
        _.forEach(currentMessageTemplateBeingEdited.message.versions, function(value) {
          if (value.id === version.id) {
            value.name = _.clone(version.name);
            return value.filters = _.cloneDeep(version.filters);
          }
        });
        return this.saveMessageToLibrary();
      };
      this.updateVersionWithGeneric = function() {
        var defaultVersion;
        this.versionsWithGeneric = [];
        defaultVersion = currentMessageTemplateBeingEdited.message.versions[0];
        currentMessageTemplateBeingEdited.message.html = _.cloneDeep(defaultVersion.html);
        currentMessageTemplateBeingEdited.message.text = _.cloneDeep(defaultVersion.text);
        currentMessageTemplateBeingEdited.message.subject = _.cloneDeep(defaultVersion.subject);
        currentMessageTemplateBeingEdited.message.template = _.cloneDeep(defaultVersion.template);
        return this.versionsWithGeneric = _.cloneDeep(currentMessageTemplateBeingEdited.message.versions);
      };
      this.defaultVersion = function() {
        return _.find(currentMessageTemplateBeingEdited.message.versions, function(version) {
          return version.id === 'default';
        });
      };
      this.setCurrentVersionById = function(id) {
        this.updateVersionWithGeneric();
        return currentVersion = _.find(this.versionsWithGeneric, {
          'id': id
        });
      };
      this.updateMessage = function(first_level, second_level, newValue) {
        var a, i, ref1, ref2, version;
        if (currentVersion.id === 'default') {
          if (second_level != null) {
            currentMessageTemplateBeingEdited.message[first_level][second_level] = _.cloneDeep(newValue);
          } else {
            currentMessageTemplateBeingEdited.message[first_level] = _.cloneDeep(newValue);
          }
          for (a = i = 0, ref1 = currentMessageTemplateBeingEdited.message.versions.length; 0 <= ref1 ? i < ref1 : i > ref1; a = 0 <= ref1 ? ++i : --i) {
            version = currentMessageTemplateBeingEdited.message.versions[a];
            if (second_level != null) {
              _.forEach(version[first_level][second_level], function(value, key) {
                var ref2;
                if (((ref2 = version[first_level][second_level][key]) != null ? ref2.ref : void 0) === 'default' || version.id === 'default') {
                  if (newValue[key] != null) {
                    return version[first_level][second_level][key] = _.cloneDeep(newValue[key]);
                  }
                }
              });
              _.forEach(newValue, function(value, key) {
                if ((version[first_level][second_level][key] == null) && (newValue[key] != null)) {
                  return version[first_level][second_level][key] = {
                    value: _.cloneDeep(newValue[key].value),
                    ref: 'default'
                  };
                }
              });
            } else {
              if (version.id === "default" || ((ref2 = version[first_level]) != null ? ref2.ref : void 0) === 'default') {
                if (version[first_level] != null) {
                  if (_.isObject(version[first_level])) {
                    version[first_level].value = _.cloneDeep(newValue.value);
                  } else {
                    version[first_level] = {
                      value: version[first_level],
                      ref: 'default'
                    };
                  }
                }
              }
              _.forEach(newValue, function(value, key) {
                if ((version[first_level][key] == null) && (newValue[key] != null)) {
                  return version[first_level][key] = {
                    value: _.cloneDeep(newValue[key].value),
                    ref: 'default'
                  };
                }
              });
            }
          }
        } else {
          version = _.find(currentMessageTemplateBeingEdited.message.versions, {
            'id': currentVersion.id
          });
          if (second_level != null) {
            _.forEach(version[first_level][second_level], function(value, key) {
              if (newValue[key] != null) {
                return version[first_level][second_level][key] = _.cloneDeep(newValue[key]);
              }
            });
            _.forEach(newValue, function(value, key) {
              if (version[first_level][second_level][key] == null) {
                return version[first_level][second_level][key] = {
                  value: _.cloneDeep(newValue[key].value),
                  ref: 'self'
                };
              }
            });
          } else {
            if (version[first_level] != null) {
              version[first_level] = _.cloneDeep(newValue);
            }
            _.forEach(newValue, function(value, key) {
              if (version[first_level][key] == null) {
                return version[first_level][key] = {
                  value: _.cloneDeep(newValue[key].value),
                  ref: 'self'
                };
              }
            });
          }
        }
        this.updateVersionWithGeneric();
        return this.isMessageDraft();
      };
      this.saveMessageToLibrary = function() {
        var a, i, ref1, templateToSend;
        templateToSend = _.cloneDeep(currentMessageTemplateBeingEdited.message);
        if (templateToSend.versions != null) {
          cleanUpMessageToSend(templateToSend, true);
          for (a = i = 0, ref1 = templateToSend.versions.length; 0 <= ref1 ? i < ref1 : i > ref1; a = 0 <= ref1 ? ++i : --i) {
            if (templateToSend.versions[a].hidden === false) {
              cleanUpMessageToSend(templateToSend.versions[a], false);
            }
          }
        }
        if ((templateToSend.id == null) || templateToSend.id === 'new') {
          delete templateToSend["id"];
          return $http.post('api/messages', templateToSend).success(function(data) {
            currentMessageTemplateBeingEdited.message.id = data.id;
            currentMessageTemplateBeingEdited.message.version = data.version;
            messageLibraryIsLoading = false;
            return that.loadMessages();
          });
        } else {
          return $http.put('api/messages/' + templateToSend.id, templateToSend).success(function(data) {
            currentMessageTemplateBeingEdited.message.version = data.version;
            messageLibraryIsLoading = false;
            return that.loadMessages();
          });
        }
      };
      preCleanUp = function() {
        var a, b, currentDefaultVersion, currentMessageVersion, currentProperty, currentSection, i, j, l, m, n, o, oldArray, oldLink, oldPolicy, p, partToSet, properties, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, replacementLink, replacementPolicy, sections, subject, template, value;
        if (((ref1 = currentMessageTemplateBeingEdited.message) != null ? (ref2 = ref1.versions) != null ? ref2.length : void 0 : void 0) > 0) {
          for (a = i = 0, ref3 = currentMessageTemplateBeingEdited.message.versions.length; 0 <= ref3 ? i < ref3 : i > ref3; a = 0 <= ref3 ? ++i : --i) {
            currentMessageVersion = currentMessageTemplateBeingEdited.message.versions[a];
            if (currentMessageVersion.links != null) {
              currentMessageVersion.html.links = {};
              currentMessageVersion.text.links = {};
              for (a = j = 0, ref4 = currentMessageVersion.links.length; 0 <= ref4 ? j < ref4 : j > ref4; a = 0 <= ref4 ? ++j : --j) {
                oldLink = currentMessageVersion.links[a];
                replacementLink = {
                  needs: _.cloneDeep(oldLink.needs),
                  text: _.cloneDeep(oldLink.text),
                  type: _.cloneDeep(oldLink.type),
                  value: _.cloneDeep(oldLink.value)
                };
                if ((oldLink.active == null) || oldLink.active === true) {
                  replacementLink.ref = 'default';
                } else {
                  replacementLink.ref = 'self';
                }
                if (currentMessageVersion.links[a].linkEncodeType === "HTML Link") {
                  currentMessageVersion.html.links[currentMessageVersion.links[a].name] = replacementLink;
                } else {
                  currentMessageVersion.text.links[currentMessageVersion.links[a].name] = replacementLink;
                }
              }
              delete currentMessageVersion["links"];
            }
            if (currentMessageVersion.policies != null) {
              currentMessageVersion.html.policies = {};
              currentMessageVersion.text.policies = {};
              for (a = l = 0, ref5 = currentMessageVersion.policies.length; 0 <= ref5 ? l < ref5 : l > ref5; a = 0 <= ref5 ? ++l : --l) {
                oldPolicy = currentMessageVersion.policies[a];
                replacementPolicy = {
                  value: oldPolicy.value
                };
                if ((oldPolicy.active == null) || oldPolicy.active === true) {
                  replacementPolicy.ref = 'default';
                } else {
                  replacementPolicy.ref = 'self';
                }
                if (currentMessageVersion.policies[a].policyEncodeType === "HTML Policy") {
                  currentMessageVersion.html.policies[currentMessageVersion.policies[a].policyName] = replacementPolicy;
                } else {
                  currentMessageVersion.text.policies[currentMessageVersion.policies[a].policyName] = replacementPolicy;
                }
              }
              delete currentMessageVersion["policies"];
            }
            if (_.isArray(currentMessageVersion.html.blocks)) {
              oldArray = _.cloneDeep(currentMessageVersion.html.blocks);
              currentMessageVersion.html.blocks = {};
              for (a = m = 0, ref6 = oldArray.length; 0 <= ref6 ? m < ref6 : m > ref6; a = 0 <= ref6 ? ++m : --m) {
                value = oldArray[a];
                currentMessageVersion.html.blocks[value.name] = {
                  value: value.blockText
                };
                if (value.active) {
                  currentMessageVersion.html.blocks[value.name].ref = 'default';
                }
              }
            }
            if (_.isArray(currentMessageVersion.text.blocks)) {
              oldArray = _.cloneDeep(currentMessageVersion.text.blocks);
              currentMessageVersion.text.blocks = {};
              for (a = n = 0, ref7 = oldArray.length; 0 <= ref7 ? n < ref7 : n > ref7; a = 0 <= ref7 ? ++n : --n) {
                value = oldArray[a];
                currentMessageVersion.text.blocks[value.name] = {
                  value: value.blockText
                };
                if (value.active) {
                  currentMessageVersion.text.blocks[value.name].ref = 'default';
                }
              }
            }
            if (currentMessageVersion.subject != null) {
              if (currentMessageVersion.subject.value == null) {
                subject = {
                  value: currentMessageVersion.subject,
                  ref: 'default'
                };
                currentMessageVersion.subject = subject;
              }
            }
            if (currentMessageVersion.template != null) {
              if (currentMessageVersion.template.value == null) {
                template = {
                  value: currentMessageVersion.template,
                  ref: 'default'
                };
                currentMessageVersion.template = template;
              }
            }
            if (currentMessageVersion.templateFromDefault != null) {
              delete currentMessageVersion["templateFromDefault"];
            }
            sections = ["html", "text"];
            properties = ["blocks", "links", "policies"];
            for (a = o = 0, ref8 = sections.length; 0 <= ref8 ? o < ref8 : o > ref8; a = 0 <= ref8 ? ++o : --o) {
              currentSection = sections[a];
              for (b = p = 0, ref9 = properties.length; 0 <= ref9 ? p < ref9 : p > ref9; b = 0 <= ref9 ? ++p : --p) {
                currentProperty = properties[b];
                _.forEach(currentMessageVersion[currentSection][currentProperty], function(value, key) {
                  var block, currentDefaultVersion, partToSet, ref10, ref11, ref12;
                  if ((((ref10 = currentMessageVersion[currentSection][currentProperty]) != null ? (ref11 = ref10[key]) != null ? ref11.active : void 0 : void 0) != null) === true) {
                    delete currentMessageVersion[currentSection][currentProperty][key]["active"];
                  }
                  if (((ref12 = currentMessageVersion[currentSection][currentProperty][key]) != null ? ref12.blockText : void 0) != null) {
                    block = currentMessageVersion[currentSection][currentProperty][key];
                    block.value = block.blockText;
                    delete block["blockText"];
                  }
                  if (currentMessageVersion.hidden === false) {
                    if (currentMessageVersion[currentSection][currentProperty][key].ref === 'default' || currentMessageVersion.id === 'default') {
                      currentDefaultVersion = _.cloneDeep(currentMessageTemplateBeingEdited.message.versions[0]);
                      partToSet = _.cloneDeep(currentDefaultVersion[currentSection][currentProperty][key]);
                      partToSet["ref"] = "default";
                      return currentMessageVersion[currentSection][currentProperty][key] = partToSet;
                    } else {
                      return currentMessageVersion[currentSection][currentProperty][key]["ref"] = "self";
                    }
                  }
                });
              }
            }
            if ((currentMessageVersion.subject == null) || currentMessageVersion.subject.ref === 'default' || currentMessageVersion.id === 'default') {
              currentDefaultVersion = _.cloneDeep(currentMessageTemplateBeingEdited.message.versions[0]);
              partToSet = _.cloneDeep(currentDefaultVersion.subject);
              if (partToSet != null) {
                partToSet["ref"] = "default";
              } else {
                partToSet = {
                  ref: 'self',
                  value: ''
                };
              }
              currentMessageVersion.subject = partToSet;
            } else {
              currentMessageVersion.subject["ref"] = "self";
            }
            if ((currentMessageVersion.template == null) || currentMessageVersion.template.ref === "default" || currentMessageVersion.id === 'default') {
              currentDefaultVersion = _.cloneDeep(currentMessageTemplateBeingEdited.message.versions[0]);
              partToSet = _.cloneDeep(currentDefaultVersion.template);
              if (partToSet != null) {
                partToSet["ref"] = "default";
              } else {
                partToSet = {
                  ref: 'self',
                  value: ''
                };
              }
              currentMessageVersion.template = partToSet;
            } else {
              currentMessageVersion.template["ref"] = "self";
            }
          }
        }
        delete currentMessageTemplateBeingEdited.message["links"];
        return delete currentMessageTemplateBeingEdited.message["policies"];
      };
      createDefaultVersion = function() {
        var catchAll;
        currentMessageTemplateBeingEdited.message.versions = [];
        catchAll = {
          id: 'default',
          name: 'Default Version',
          subject: _.cloneDeep(currentMessageTemplateBeingEdited.message.subject),
          html: {
            blocks: {},
            links: _.cloneDeep(currentMessageTemplateBeingEdited.message.html.links),
            policies: _.cloneDeep(currentMessageTemplateBeingEdited.message.html.policies)
          },
          text: {
            blocks: {},
            links: _.cloneDeep(currentMessageTemplateBeingEdited.message.text.links),
            policies: _.cloneDeep(currentMessageTemplateBeingEdited.message.text.policies)
          }
        };
        if (currentMessageTemplateBeingEdited.message.template != null) {
          if (currentMessageTemplateBeingEdited.message.template == null) {
            catchAll.template = {
              value: currentMessageTemplateBeingEdited.message.template,
              ref: 'default'
            };
          } else {
            catchAll.template = currentMessageTemplateBeingEdited.message.template.value;
          }
        }
        _.forEach(currentMessageTemplateBeingEdited.message.html.blocks, function(value, key) {
          if ((currentMessageTemplateBeingEdited.message.html.blocks[key] != null) && (currentMessageTemplateBeingEdited.message.html.blocks[key].value == null)) {
            return catchAll.html.blocks[key] = {
              value: currentMessageTemplateBeingEdited.message.html.blocks[key]
            };
          }
        });
        _.forEach(currentMessageTemplateBeingEdited.message.text.blocks, function(value, key) {
          if ((currentMessageTemplateBeingEdited.message.text.blocks[key] != null) && (currentMessageTemplateBeingEdited.message.text.blocks[key].value == null)) {
            return catchAll.text.blocks[key] = {
              value: currentMessageTemplateBeingEdited.message.text.blocks[key]
            };
          }
        });
        _.forEach(currentMessageTemplateBeingEdited.message.html.policies, function(value, key) {
          if ((currentMessageTemplateBeingEdited.message.html.policies[key] != null) && (currentMessageTemplateBeingEdited.message.html.policies[key].value == null)) {
            return catchAll.html.policies[key] = {
              value: currentMessageTemplateBeingEdited.message.html.policies[key]
            };
          }
        });
        _.forEach(currentMessageTemplateBeingEdited.message.text.policies, function(value, key) {
          if ((currentMessageTemplateBeingEdited.message.text.policies[key] != null) && (currentMessageTemplateBeingEdited.message.text.policies[key].value == null)) {
            return catchAll.text.policies[key] = {
              value: currentMessageTemplateBeingEdited.message.text.policies[key]
            };
          }
        });
        return currentMessageTemplateBeingEdited.message.versions.unshift(catchAll);
      };
      cleanUpMessageToSend = function(versionToCheck, isBase) {
        var a, a_first_pos, a_second_pos, b, first_pos, i, j, l, pieces, ref1, ref2, ref3, results, second_pos;
        if (versionToCheck.id === 'default' && versionToCheck.name === '') {
          versionToCheck.name = "Default Version";
        }
        if (isBase || versionToCheck.id === 'default' || versionToCheck.template.ref !== 'default') {
          if (isBase) {
            versionToCheck.template = versionToCheck.versions[0].template.value;
          } else {
            delete versionToCheck.template["ref"];
          }
        } else {
          delete versionToCheck.template["value"];
        }
        if (isBase || versionToCheck.id === 'default' || versionToCheck.subject.ref !== 'default') {
          if (isBase) {
            versionToCheck.subject = versionToCheck.versions[0].subject.value;
          } else {
            delete versionToCheck.subject["ref"];
          }
        } else {
          delete versionToCheck.subject["value"];
        }
        first_pos = ['html', 'text'];
        second_pos = ['blocks', 'policies', 'links'];
        for (a = i = 0, ref1 = first_pos.length; 0 <= ref1 ? i < ref1 : i > ref1; a = 0 <= ref1 ? ++i : --i) {
          a_first_pos = first_pos[a];
          for (b = j = 0, ref2 = second_pos.length; 0 <= ref2 ? j < ref2 : j > ref2; b = 0 <= ref2 ? ++j : --j) {
            a_second_pos = second_pos[b];
            _.forEach(versionToCheck[a_first_pos][a_second_pos], function(value, key) {
              if (isBase || versionToCheck.id === 'default' || versionToCheck[a_first_pos][a_second_pos][key].ref !== 'default') {
                return delete versionToCheck[a_first_pos][a_second_pos][key]["ref"];
              } else {
                if (second_pos !== "links") {
                  return delete versionToCheck[a_first_pos][a_second_pos][key]["value"];
                } else {
                  delete versionToCheck[a_first_pos][a_second_pos][key]["value"];
                  delete versionToCheck[a_first_pos][a_second_pos][key]["needs"];
                  delete versionToCheck[a_first_pos][a_second_pos][key]["text"];
                  return delete versionToCheck[a_first_pos][a_second_pos][key]["type"];
                }
              }
            });
          }
        }
        pieces = ["templates", "subject", "html", "links", "policies", "text"];
        results = [];
        for (a = l = 0, ref3 = pieces.length; 0 <= ref3 ? l < ref3 : l > ref3; a = 0 <= ref3 ? ++l : --l) {
          if (versionToCheck[pieces[a] + "FromDefault"] != null) {
            results.push(delete versionToCheck[pieces[a] + "FromDefault"]);
          } else {
            results.push(void 0);
          }
        }
        return results;
      };
      this.archiveMessage = function(message) {
        var templateToSend;
        templateToSend = _.find(_.union(completedMessagesInLibrary, draftMessagesInLibrary), function(value) {
          return value.id === message.id;
        });
        templateToSend.hidden = true;
        return $http.post('api/messages', templateToSend).success(function(data) {
          messageLibraryIsLoading = false;
          return that.loadMessages();
        });
      };
      this.cloneMessageById = function(id) {
        var message;
        return message = _.find(_.union(completedMessagesInLibrary, draftMessagesInLibrary), {
          'id': id
        });
      };
      this.resetMessageLibrary = function() {
        this.setWizardPosition(null);
        this.loadMessages();
        return false;
      };
      this.setStatusSegmentBreadcrumb = function(status, segment) {
        breadcrumbStatus = status;
        return breadcrumbSegment = segment;
      };
      this.getStatusSegmentBreadcrumb = function() {
        if (breadcrumbStatus == null) {
          return void 0;
        } else {
          return [breadcrumbStatus, breadcrumbSegment];
        }
      };
      assign = function(obj, prop, value) {
        var e;
        if (typeof prop === "string") {
          prop = prop.split(".");
        }
        if (prop.length > 1) {
          e = prop.shift();
          if (Object.prototype.toString.call(obj[e]) !== "[object Object]") {
            obj[e] = {};
          }
          return assign(obj[e], prop, value);
        } else {
          return obj[prop[0]] = value;
        }
      };
      return false;
    }
  ]).controller('DripEditorModalCtrl', [
    '$scope', 'MessageLibrarySrv', '$state', 'drip', 'new_step', 'segment', '$uibModalInstance', '$http', function($scope, MessageLibrarySrv, $state, drip, new_step, segment, $uibModalInstance, $http) {
      var ref1, timeSlotTaken, timeToCheck;
      $scope.messageService = MessageLibrarySrv;
      $scope.currentDrip = _.cloneDeep(drip);
      $scope.newStep = new_step;
      $scope.senders = [];
      if ($scope.currentDrip.notify == null) {
        if ($scope.currentDrip.vuNotification == null) {
          $scope.currentDrip.notify = $scope.currentDrip.vuNotification = false;
        } else {
          $scope.currentDrip.notify = $scope.currentDrip.vuNotification;
        }
      }
      if (((ref1 = $scope.currentDrip.message) != null ? ref1.id : void 0) != null) {
        $scope.currentDripMessageId = $scope.currentDrip.message.id;
      } else {
        $scope.currentDripMessageId = $scope.currentDrip.message;
      }
      if ($scope.currentDripMessageId == null) {
        $scope.dripError = "The message that is current associated with this drip no longer exists or is in DRAFT mode.";
      } else {
        $scope.dripError = void 0;
      }
      timeToCheck = $scope.currentDrip.delay.value * ($scope.currentDrip.delay.units === 'day' ? 24 : 1);
      timeSlotTaken = _.find(segment.steps, function(step) {
        var dripDelay;
        if (step.delay.units === 'day') {
          dripDelay = step.delay.value * 24;
        } else {
          dripDelay = step.delay.value;
        }
        return dripDelay === timeToCheck && step.id !== $scope.currentDrip.id;
      });
      if (timeSlotTaken != null) {
        $scope.dripTemplateWarning = 'You already have a message being sent during this time for this campaign.  Adding it could be detrimental to the performance of your campaign.';
      } else {
        $scope.dripTemplateWarning = void 0;
      }
      $scope.templatesInLibrary1 = MessageLibrarySrv.completedMessages();
      $http.get('api/senders?q=active').success(function(senders) {
        return $scope.senders = _.pluck(senders, 'email');
      });
      $scope.$watch('messageService.completedMessages()', function() {
        return $scope.templatesInLibrary1 = MessageLibrarySrv.completedMessages();
      });
      $scope.onMessageChange = function() {
        $scope.dripError = void 0;
        $scope.dripTemplateWarning = void 0;
        $scope.currentDrip.message = $scope.messageService.cloneMessageById($scope.currentDripMessageId);
        $scope.currentDrip.name = $scope.currentDrip.message.name;
        $scope.isTimeSlotTaken();
        return false;
      };
      $scope.onDelayChange = function() {
        $scope.baseLineTimeToHours();
        return $scope.isTimeSlotTaken();
      };
      $scope.onNotificationChange = function() {
        $scope.currentDrip.vuNotification = $scope.currentDrip.notify;
        return $scope.isTimeSlotTaken();
      };
      $scope.isTimeSlotTaken = function() {
        timeToCheck = $scope.currentDrip.delay.value * ($scope.currentDrip.delay.units === 'day' ? 24 : 1);
        timeSlotTaken = _.find(_.filter(segment.steps, function(f) {
          if (f.deleted !== true) {
            return f;
          }
        }), function(step) {
          var dripDelay;
          dripDelay = step.delay.value * (step.delay.units === 'day' ? 24 : 1);
          return dripDelay === timeToCheck && step.id !== $scope.currentDrip.id;
        });
        if (timeSlotTaken != null) {
          return $scope.dripTemplateWarning = 'You already have a message being sent during this time for this campaign.  Adding it could be detrimental to the performance of your campaign.';
        } else {
          return $scope.dripTemplateWarning = void 0;
        }
      };
      $scope.baseLineTimeToHours = function() {
        if ($scope.currentDrip.delay.units === 'day') {
          return $scope.currentDrip.delay.delayInHours = parseInt($scope.currentDrip.delay.value, 10) * 24;
        } else {
          return $scope.currentDrip.delay.delayInHours = parseInt($scope.currentDrip.delay.value, 10);
        }
      };
      $scope.save = function() {
        return $uibModalInstance.close(_.cloneDeep($scope.currentDrip));
      };
      $scope.close = function() {
        return $uibModalInstance.dismiss();
      };
      $scope.baseLineTimeToHours();
      $scope.isTimeSlotTaken();
      return false;
    }
  ]).controller('MessageLibraryCtrl', [
    '$scope', 'MessageLibrarySrv', '$state', '$uibModal', 'growl', function($scope, MessageLibrarySrv, $state, $uibModal, growl) {
      $scope.navigationItems = ['Template', 'Subject', 'Links', 'HTML', 'Plain Text', 'Policies', 'Preview'];
      $scope.navURLItems = ['templates', 'subjects', 'links', 'html', 'plaintext', 'policies', 'confirmation'];
      $scope.service = MessageLibrarySrv;
      $scope.currentWizardPosition = 0;
      $scope.selectedTemplate = -1;
      $scope.messageName = '';
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
      $scope.hovering = false;
      $scope.parentHeight = $('#wizardContainer').height() / 7;
      $scope.messageTemplateExists = false;
      $scope.incomingWizardPosition = MessageLibrarySrv.getWizardPosition();
      $scope.messageProgress = MessageLibrarySrv.messageStatus();
      $scope.messagesInCampaigns = {};
      $scope.$watch('service.getWizardPosition()', function() {
        $scope.incomingWizardPosition = MessageLibrarySrv.getWizardPosition();
        if ($scope.incomingWizardPosition != null) {
          $('#messageLibrary').animate({
            marginLeft: '-100%'
          }, 500);
          $('#wizard-footer').fadeIn();
          return $scope.gotoMessageLibraryScreen($scope.incomingWizardPosition, true);
        } else {
          $('#messageLibrary').animate({
            marginLeft: '0%'
          }, 500);
          return $('#wizard-footer').fadeOut();
        }
      });
      $scope.$watch('service.campaignMessages()', function() {
        return $scope.messagesInCampaigns = MessageLibrarySrv.campaignMessages();
      });
      $scope.$watch('service.versionBeingEdited()', function() {
        $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
        if ($scope.currentVersion.template != null) {
          $scope.messageTemplateExists = _.find($scope.messageTemplates, {
            'id': $scope.currentVersion.template.value
          }) != null;
          if ($scope.messageTemplateExists === false) {
            return $scope.messageTemplateExists = _.find($scope.messageTemplates, {
              'id': $scope.currentVersion.template
            }) != null;
          }
        }
      });
      $scope.$watch('service.messageStatus()', function() {
        $scope.messageProgress = MessageLibrarySrv.messageStatus();
        if (MessageLibrarySrv.getCurrentMessageId() !== 'new' && ($scope.messagesInCampaigns[MessageLibrarySrv.getCurrentMessageId()] != null) && $scope.messageProgress === "draft") {
          return growl.danger("This message is currently being used in a drip campaign.  DRAFT mode for this message has been de-activated.");
        }
      });
      $scope.isMessageInCampaign = function(messageId) {
        return $scope.messagesInCampaigns[messageId] != null;
      };
      $scope.$on('$stateChangeSuccess', function() {
        if ($state.current.url === '/manage/message-library') {
          $scope.service.resetMessageLibrary();
        }
        if ($state.current.url === '/:state') {
          return $scope.service.parseStateParams();
        }
      });
      $scope.saveDripMessage = function() {
        var inst;
        MessageLibrarySrv.saveMessageToLibrary();
        inst = $uibModal.open({
          templateUrl: 'template_save_confirmation_modal.html',
          controller: 'SaveTemplateModalCtrl'
        });
        return false;
      };
      $scope.cancelDripMessageEdit = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'template_cancel_confirmation_modal.html',
          controller: 'CancelTemplateModalCtrl'
        });
        return false;
      };
      $scope.gotoMessageLibraryScreen = function(newPosition, overrideCheck) {
        var aTag, parentContainer, tagName;
        if (overrideCheck) {
          $scope.currentWizardPosition = newPosition;
          tagName = 'section' + newPosition;
          aTag = $("a[name='section" + newPosition + "']");
          parentContainer = $('#wizardContainer');
          if (aTag.offset() != null) {
            $('#wizardContainer').animate({
              marginTop: (parentContainer.offset().top - aTag.offset().top) + 'px'
            }, 250);
            return $scope.updateNavigation();
          } else {

          }
        } else {
          $scope.messageName = MessageLibrarySrv.messageTemplateName();
          $scope.selectedTemplate = MessageLibrarySrv.currentMessageTemplateId();
          if ($scope.selectedTemplate.value !== -1) {
            if (!$scope.messageName) {
              $scope.templateNameError = 'Please give us a name for this template';
            } else {
              $scope.templateNameError = null;
              if (newPosition === null) {
                if ($scope.currentWizardPosition < 6) {
                  $scope.currentWizardPosition += 1;
                }
              } else {
                $scope.currentWizardPosition = newPosition;
              }
            }
            aTag = $("a[name='section" + newPosition + "']");
            parentContainer = $('#wizardContainer');
            $('#wizardContainer').animate({
              marginTop: (parentContainer.offset().top - aTag.offset().top) + 'px'
            }, 250);
            return $scope.updateNavigation();
          } else {
            return $scope.templateNameError = 'Please choose a template.';
          }
        }
      };
      $scope.stepBackMessageWizard = function() {
        return $scope.gotoMessageLibraryScreen($scope.currentWizardPosition - 1, true);
      };
      $scope.stepForwardMessageWizard = function() {
        return $scope.gotoMessageLibraryScreen($scope.currentWizardPosition + 1, true);
      };
      $scope.onMouseover = function(id) {
        if (id !== $scope.currentWizardPosition && $scope.currentVersion.template.value !== -1) {
          $('#nav_' + id).addClass('navigation-hover');
        } else {
          $('#nav_' + id).removeClass('navigation-hover');
        }
        return false;
      };
      $scope.onMouseout = function(id) {
        if (id !== $scope.currentWizardPosition) {
          $('#nav_' + id).removeClass('navigation-hover');
        }
        return false;
      };
      $scope.updateNavigation = function() {
        var message, state, version;
        message = MessageLibrarySrv.getCurrentMessageId();
        version = MessageLibrarySrv.versionBeingEdited().id;
        state = $scope.navURLItems[$scope.currentWizardPosition];
        return $state.go('manage-messagelibrary.message.version.state', {
          message: message,
          version: version,
          state: state
        });
      };
      $scope.changeWizardPosition = function(id) {
        if ($scope.currentVersion.template.value !== -1) {
          return $scope.service.setWizardPosition(id);
        }
      };
      return $scope.onBackToLibraryClicked = function() {
        var segment, status;
        if ($scope.service.getStatusSegmentBreadcrumb() != null) {
          status = $scope.service.getStatusSegmentBreadcrumb()[0];
          segment = $scope.service.getStatusSegmentBreadcrumb()[1];
          return $state.go('manage-statuses.status.segment', {
            status: status,
            segment: segment
          });
        } else {
          $state.go('manage-messagelibrary');
          return $scope.service.setWizardPosition(null);
        }
      };
    }
  ]).controller('DripTemplateChooserCtrl', [
    '$scope', 'MessageLibrarySrv', '$state', '$uibModal', 'growl', '$http', function($scope, MessageLibrarySrv, $state, $uibModal, growl, $http) {
      var toggleShowVersions;
      $scope.service = MessageLibrarySrv;
      $scope.templatesInLibrary = MessageLibrarySrv.completedMessages();
      $scope.messageName = MessageLibrarySrv.messageTemplateName();
      $scope.selectedTemplateId = MessageLibrarySrv.currentMessageTemplateId();
      $scope.searchText = '';
      $scope.draftsInLibrary = MessageLibrarySrv.messagesInDraft();
      $scope.creativeTemplates = MessageLibrarySrv.messageTemplates();
      $scope.statuses = [];
      $scope.order = [];
      $scope.showMessageSearch = false;
      $scope.showDraftSearch = false;
      $scope.showVersions = {};
      $scope.messagesInCampaigns = {};
      $scope.$watch('service.completedMessages()', function() {
        $scope.templatesInLibrary = MessageLibrarySrv.completedMessages();
        $scope.draftsInLibrary = MessageLibrarySrv.messagesInDraft();
        _.forEach($scope.templatesInLibrary, function(value) {
          $scope.showVersions[value.id] = true;
          if (value.versions != null) {
            value.filter_names = [];
            return _.forEach(value.versions, function(versionValue, versionKey) {
              return versionValue.filter_names = MessageLibrarySrv.getAttributesById(versionValue.filters);
            });
          }
        });
        return _.forEach($scope.draftsInLibrary, function(value) {
          $scope.showVersions[value.id] = true;
          if (value.versions != null) {
            value.filter_names = [];
            return _.forEach(value.versions, function(versionValue, versionKey) {
              return versionValue.filter_names = MessageLibrarySrv.getAttributesById(versionValue.filters);
            });
          }
        });
      });
      $scope.$watch('service.messageTemplateName()', function() {
        return $scope.messageName = MessageLibrarySrv.messageTemplateName();
      });
      $scope.$watch('service.currentMessageTemplateId()', function() {
        return $scope.selectedTemplateId = MessageLibrarySrv.currentMessageTemplateId();
      });
      $scope.$watch('service.messagesInDraft()', function() {
        return $scope.draftsInLibrary = MessageLibrarySrv.messagesInDraft();
      });
      $scope.$watch('service.statuses()', function() {
        return $scope.statuses = MessageLibrarySrv.statuses();
      });
      $scope.$watch('service.order()', function() {
        return $scope.order = MessageLibrarySrv.order();
      });
      $scope.$watch('service.messageTemplates()', function() {
        return $scope.creativeTemplates = MessageLibrarySrv.messageTemplates();
      });
      $scope.$watch('service.campaignMessages()', function() {
        return $scope.messagesInCampaigns = MessageLibrarySrv.campaignMessages();
      });
      $scope.isDefault = function(version) {
        return version.id !== 'default';
      };
      $scope.isMessageInCampaign = function(messageId) {
        return $scope.messagesInCampaigns[messageId] != null;
      };
      $scope.numberOfAttributes = function(filters) {
        var number;
        number = 0;
        _.forEach(filters, function(value, key) {
          return number += value.length;
        });
        return number;
      };
      $scope.templateExists = function(template) {
        return !(_.some($scope.creativeTemplates, {
          'id': template.value
        }));
      };
      $scope.active_versions = function(message) {
        var active_versions;
        if (message.versions != null) {
          active_versions = _.filter(_.cloneDeep(message.versions), {
            'hidden': false
          });
          return active_versions.length;
        } else {
          return 0;
        }
      };
      $scope.toggleMessageSearch = function($event) {
        $event.stopPropagation();
        return $scope.showMessageSearch = !$scope.showMessageSearch;
      };
      $scope.editVersion = function(message, message_version, $event) {
        var version;
        message = message.id;
        version = message_version.id;
        return $state.go('manage-messagelibrary.message.version.state', {
          message: message,
          version: version,
          state: 'templates'
        });
      };
      $scope.editAttributes = function(message, message_version) {
        var inst;
        return inst = $uibModal.open({
          templateUrl: 'editVersionAttributesModal.html',
          controller: 'EditVersionAttributesModalCtrl',
          resolve: {
            message: function() {
              return _.cloneDeep(message);
            },
            currentVersion: function() {
              return _.cloneDeep(message_version);
            }
          }
        });
      };
      $scope.cloneVersion = function(template, templateVersion) {
        var inst;
        return inst = $uibModal.open({
          templateUrl: 'cloneVersionModal.html',
          controller: 'CloneVersionModalCtrl',
          resolve: {
            message: function() {
              return template;
            },
            version: function() {
              return _.cloneDeep(templateVersion);
            }
          }
        });
      };
      $scope.deleteVersion = function(message, message_version) {
        var inst;
        return inst = $uibModal.open({
          templateUrl: 'version_delete-confirm.html',
          controller: 'DeleteVersionModalCtrl',
          resolve: {
            message: function() {
              return message;
            },
            version: function() {
              return message_version;
            }
          }
        });
      };
      $scope.shouldShowVersions = function(template_id) {
        return $scope.showVersions[template_id];
      };
      $scope.toggleDraftSearch = function($event) {
        $event.stopPropagation();
        return $scope.showDraftSearch = !$scope.showDraftSearch;
      };
      $scope.editMessageDripTemplate = function(message) {
        var ref1;
        if (((ref1 = message.versions) != null ? ref1.length : void 0) > 1) {
          return toggleShowVersions(message.id);
        } else {
          MessageLibrarySrv.setStatusSegmentBreadcrumb(void 0, void 0);
          return $state.go('manage-messagelibrary.message.version.state', {
            message: message.id,
            version: 'default',
            state: 'templates'
          });
        }
      };
      $scope.createNewTemplate = function() {
        MessageLibrarySrv.createNewMessageTemplate();
        return $state.go('manage-messagelibrary.message.version.state', {
          message: 'new',
          version: 'default',
          state: 'templates'
        });
      };
      $scope.messageIsReadyToSave = function() {
        if (($scope.messageName == null) || $scope.messageName.length === 0 || $scope.selectedTemplateId === -1) {
          return false;
        }
        return true;
      };
      $scope.showStatusesThatHaveMessage = function($event, message) {
        var inst;
        $event.stopPropagation();
        return inst = $uibModal.open({
          templateUrl: 'statusesWhereMessageExistModal.html',
          controller: 'StatusesWhereMessageExistModalCtrl',
          resolve: {
            message: function() {
              return _.cloneDeep(message);
            }
          }
        });
      };
      $scope.saveDripMessage = function() {
        var inst;
        if ($scope.service.getCurrentMessageId() !== 'new' && ($scope.messagesInCampaigns[$scope.service.getCurrentMessageId()] != null)) {
          growl.warning("This message is currently being used in a drip campaign.  DRAFT mode for this message has been de-activated.");
        } else {
          MessageLibrarySrv.saveMessageToLibrary();
          inst = $uibModal.open({
            templateUrl: 'template_save_confirmation_modal.html',
            controller: 'SaveTemplateModalCtrl'
          });
        }
        return false;
      };
      $scope.cancelDripMessageEdit = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'template_cancel_confirmation_modal.html',
          controller: 'CancelTemplateModalCtrl'
        });
        return false;
      };
      $scope.cloneMessage = function(message) {
        var cloned, inst;
        cloned = _.cloneDeep(message);
        cloned.name = cloned.name + " (Cloned)";
        inst = $uibModal.open({
          templateUrl: 'message_clone-confirm.html',
          controller: 'MessageCloneConfirmCtrl',
          resolve: {
            message: function() {
              return cloned;
            }
          }
        });
        inst.result.then(function(message) {
          return $http.post('api/messages', message).success(function(data) {
            message.version = data.version;
            $scope.service.messageLibraryIsLoading = false;
            $scope.service.loadMessages(true);
            return growl.success(message.name + ' has been added to your message library.');
          });
        });
        return false;
      };
      $scope.addVersionToMessage = function(message) {
        var cloned, currentVersion, inst;
        cloned = _.cloneDeep(message);
        currentVersion = {};
        currentVersion.id = uuid.v4();
        return inst = $uibModal.open({
          templateUrl: 'add-version_to-message-modal.html',
          controller: 'AddVersionToMessageModalCtrl',
          resolve: {
            message: function() {
              return cloned;
            },
            currentVersion: function() {
              return _.cloneDeep(currentVersion);
            }
          }
        });
      };
      $scope.addMessageToDrip = function(message) {
        var cloned, inst;
        cloned = _.cloneDeep(message);
        inst = $uibModal.open({
          templateUrl: 'add-message-to-drip.html',
          controller: 'AddMessageToDripModal',
          size: 'lg',
          resolve: {
            order: function() {
              return $scope.order;
            },
            statuses: function() {
              return $scope.statuses;
            },
            message: function() {
              return cloned;
            }
          }
        });
        return inst.result.then(function(status) {
          if (status.id === $scope.selected_status.id) {
            $scope.selected_status = status;
          }
          return $scope.statuses[status.id] = status;
        });
      };
      $scope.sendMessageAsBatch = function(message) {
        return $state.go('manage-batches-with-message', {
          message: message.id
        });
      };
      $scope.viewMessagePreview = function(message, message_id) {
        var inst, template, template_id;
        template_id = message.template;
        if (template_id.value != null) {
          template_id = template_id.value;
        }
        template = template_id;
        return inst = $uibModal.open({
          templateUrl: 'html-modal.html',
          size: 'lg',
          scope: _.assign($scope.$new(true), {
            id: template,
            url: "/messages/" + template + "/preview",
            post: {
              id: message_id
            }
          })
        });
      };
      $scope.sendMessageSample = function(message, version) {
        var inst, step, template_id;
        step = {};
        step.message = message;
        template_id = message.template;
        if (template_id.value != null) {
          template_id = template_id.value;
        }
        return inst = $uibModal.open({
          templateUrl: 'sample-modal.html',
          controller: 'SampleModal',
          resolve: {
            template: function() {
              return template_id;
            },
            status: function() {
              return null;
            },
            segment: function() {
              return null;
            },
            step: function() {
              return step;
            }
          }
        });
      };
      $scope.archiveMessage = function(message) {
        var cloned, inst;
        cloned = _.cloneDeep(message);
        inst = $uibModal.open({
          templateUrl: 'message_delete-confirm.html',
          controller: 'MessageDeleteConfirmCtrl',
          resolve: {
            message: function() {
              return cloned;
            }
          }
        });
        inst.result.then(function(message) {
          $scope.service.archiveMessage(message);
          return growl.success('Message deleted');
        });
        return false;
      };
      return toggleShowVersions = function(template_id) {
        return _.forOwn($scope.showVersions, function(value, key) {
          if (key === template_id) {
            value = !value;
            $scope.showVersions[key] = value;
            return value;
          }
        });
      };
    }
  ]).controller('SaveTemplateModalCtrl', [
    '$scope', '$state', '$uibModalInstance', 'MessageLibrarySrv', function($scope, $state, $uibModalInstance, MessageLibrarySrv) {
      $scope.service = MessageLibrarySrv;
      $scope.continueEditing = function() {
        return $uibModalInstance.dismiss();
      };
      $scope.exitToLibrary = function() {
        var segment, status;
        $uibModalInstance.dismiss();
        if ($scope.service.getStatusSegmentBreadcrumb() == null) {
          $state.go('manage-messagelibrary');
          return $scope.service.setWizardPosition(null);
        } else {
          status = $scope.service.getStatusSegmentBreadcrumb()[0];
          segment = $scope.service.getStatusSegmentBreadcrumb()[1];
          return $state.go('manage-statuses.status.segment', {
            status: status,
            segment: segment
          });
        }
      };
      return false;
    }
  ]).controller('CancelTemplateModalCtrl', [
    '$scope', '$state', '$uibModalInstance', 'MessageLibrarySrv', function($scope, $state, $uibModalInstance, MessageLibrarySrv) {
      $scope.service = MessageLibrarySrv;
      $scope.saveChangesFirst = function() {
        var segment, status;
        MessageLibrarySrv.saveMessageToLibrary();
        $uibModalInstance.dismiss();
        if ($scope.service.getStatusSegmentBreadcrumb() == null) {
          $state.go('manage-messagelibrary');
          return $scope.service.setWizardPosition(null);
        } else {
          status = $scope.service.getStatusSegmentBreadcrumb()[0];
          segment = $scope.service.getStatusSegmentBreadcrumb()[1];
          return $state.go('manage-statuses.status.segment', {
            status: status,
            segment: segment
          });
        }
      };
      $scope.continueEditing = function() {
        return $uibModalInstance.dismiss();
      };
      return $scope.exitToLibrary = function() {
        var segment, status;
        $uibModalInstance.dismiss();
        if ($scope.service.getStatusSegmentBreadcrumb() == null) {
          $state.go('manage-messagelibrary');
          return $scope.service.setWizardPosition(null);
        } else {
          status = $scope.service.getStatusSegmentBreadcrumb()[0];
          segment = $scope.service.getStatusSegmentBreadcrumb()[1];
          return $state.go('manage-statuses.status.segment', {
            status: status,
            segment: segment
          });
        }
      };
    }
  ]).controller('MessageDeleteConfirmCtrl', [
    '$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message) {
      $scope.messageDeleteConfirmed = function() {
        return $uibModalInstance.close(message);
      };
      return $scope.cancelMessageDelete = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]).controller('MessageCloneConfirmCtrl', [
    '$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message) {
      $scope.messageToClone = message;
      delete $scope.messageToClone['id'];
      delete $scope.messageToClone['version'];
      $scope.messageCloneConfirmed = function() {
        return $uibModalInstance.close($scope.messageToClone);
      };
      return $scope.cancelMessageClone = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]).controller('StatusesWhereMessageExistModalCtrl', [
    '$scope', '$uibModalInstance', '$state', 'message', 'MessageLibrarySrv', function($scope, $uibModalInstance, $state, message, MessageLibrarySrv) {
      $scope.campaigns = MessageLibrarySrv.campaignMessages()[message.id];
      $scope.message = message;
      $scope.goToCampaign = function(campaign) {
        $uibModalInstance.dismiss();
        return $state.go('manage-statuses.status.segment', {
          status: campaign.status,
          segment: campaign.segment
        });
      };
      $scope.delayInHours = function(step) {
        if (step.drip.delay.units === 'day') {
          return step.drip.delay.value * 24;
        } else {
          return step.drip.delay.value;
        }
      };
      return $scope.dismiss = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]).controller('AddMessageToDripModal', [
    '$scope', '$uibModalInstance', 'order', 'statuses', 'message', 'httpRequest', 'saveTimeout', 'errorTimeout', 'growl', function($scope, $uibModalInstance, order, statuses, message, httpRequest, saveTimeout, errorTimeout, growl) {
      var saveStatus, setDelayInHours;
      $scope.order = order;
      $scope.statuses = statuses;
      $scope.forms = {};
      $scope.selected = {
        status: null,
        segment: null
      };
      $scope.currentDrip = {
        delay: {
          value: '',
          units: 'day'
        },
        vuNotification: false,
        notify: false,
        message: message,
        name: message.name
      };
      $scope.onDelayChange = function() {
        $scope.baseLineTimeToHours();
        return $scope.isTimeSlotTaken();
      };
      $scope.onNotificationChange = function() {
        $scope.currentDrip.vuNotification = $scope.currentDrip.notify;
        return $scope.isTimeSlotTaken();
      };
      $scope.isTimeSlotTaken = function() {
        var timeSlotTaken, timeToCheck;
        timeToCheck = $scope.currentDrip.delay.value * ($scope.currentDrip.delay.units === 'day' ? 24 : 1);
        timeSlotTaken = _.find($scope.selected.segment.steps, function(step) {
          var dripDelay;
          if (step.delay.units === 'day') {
            dripDelay = step.delay.value * 24;
          } else {
            dripDelay = step.delay.value;
          }
          return dripDelay === timeToCheck && step.id !== $scope.currentDrip.id;
        });
        if (timeSlotTaken != null) {
          return $scope.dripTemplateWarning = 'You already have a message being sent during this time for this campaign.  Adding it could be detrimental to the performance of your campaign.';
        } else {
          return $scope.dripTemplateWarning = void 0;
        }
      };
      $scope.baseLineTimeToHours = function() {
        if ($scope.currentDrip.delay.units === 'day') {
          return $scope.currentDrip.delay.delayInHours = parseInt($scope.currentDrip.delay.value, 10) * 24;
        } else {
          return $scope.currentDrip.delay.delayInHours = parseInt($scope.currentDrip.delay.value, 10);
        }
      };
      $scope.createDripStep = function() {
        $scope.errorMessage = void 0;
        $scope.formErrors = void 0;
        $scope.forms.clone.submitted = false;
        $scope.forms.clone.saved = false;
        $scope.forms.clone.saving = true;
        $scope.currentDrip.vuNotification = $scope.currentDrip.notify;
        setDelayInHours($scope.currentDrip);
        $scope.selected.segment.steps.push($scope.currentDrip);
        return saveStatus("Drip Step " + $scope.currentDrip.name + " Saved", "Error creating " + $scope.currentDrip.name + " ");
      };
      $scope.close = function() {
        return $uibModalInstance.dismiss();
      };
      setDelayInHours = function(step) {
        var delay;
        if (step.delay.units === 'day') {
          delay = parseInt(step.delay.value, 10) * 24;
          return step.delay.delayInHours = delay;
        } else {
          delay = parseInt(step.delay.value, 10);
          return step.delay.delayInHours = delay;
        }
      };
      return saveStatus = function(success_message, error_message) {
        var ref1;
        if (((ref1 = $scope.selected_segment) != null ? ref1.steps : void 0) != null) {
          $scope.selected.segment.steps = _.sortBy($scope.selected.segment.steps, function(step) {
            if (step.delay.units === 'day') {
              return step.delay.value * 24;
            } else {
              return step.delay.value;
            }
          });
        }
        return httpRequest.save($scope.selected.status, function(err, data) {
          if (err == null) {
            $scope.selected.status.version = data.version;
            return $uibModalInstance.dismiss();
          }
        }, success_message, error_message);
      };
    }
  ]).controller('TemplateChooserCtrl', [
    '$scope', 'MessageLibrarySrv', function($scope, MessageLibrarySrv) {
      var showTemplateWarning;
      $scope.service = MessageLibrarySrv;
      $scope.messageTemplates = MessageLibrarySrv.messageTemplates();
      $scope.message = {};
      $scope.message.messageName = MessageLibrarySrv.messageTemplateName();
      $scope.message.slugName = MessageLibrarySrv.getMessageSlugName();
      $scope.selectedTemplate = MessageLibrarySrv.currentTemplate();
      $scope.messageTemplateExists = false;
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
      $scope.messageId = MessageLibrarySrv.fetchMessageId();
      $scope.currentTemplate = MessageLibrarySrv.versionBeingEdited().template;
      $scope.$watch('service.messageTemplates()', function() {
        var a, i, ref1, results;
        $scope.messageTemplates = MessageLibrarySrv.messageTemplates();
        results = [];
        for (a = i = 0, ref1 = $scope.messageTemplates.length; 0 <= ref1 ? i < ref1 : i > ref1; a = 0 <= ref1 ? ++i : --i) {
          $scope.messageTemplates[a].templateName = '';
          if (($scope.messageTemplates[a].tags != null) > 0) {
            results.push($scope.messageTemplates[a].templateName = $scope.messageTemplates[a].tags[0]);
          } else {
            results.push(void 0);
          }
        }
        return results;
      });
      $scope.$watch('service.getMessageSlugName()', function() {
        return $scope.message.slugName = MessageLibrarySrv.getMessageSlugName();
      });
      $scope.$watch('service.messageTemplateName()', function() {
        $scope.message.messageName = MessageLibrarySrv.messageTemplateName();
        return $scope.message.slugName = MessageLibrarySrv.getMessageSlugName();
      });
      $scope.$watch('service.currentTemplate()', function() {
        return $scope.selectedTemplate = MessageLibrarySrv.currentTemplate();
      });
      $scope.$watch('service.versionBeingEdited()', function() {
        $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
        $scope.currentTemplate = $scope.currentVersion.template;
        $scope.messageId = MessageLibrarySrv.fetchMessageId();
        return $scope.messageTemplateExists = showTemplateWarning();
      });
      $scope.onUseDefaultChange = function() {
        var defaultTemplate, defaultTemplateId;
        if ($scope.currentTemplate.ref === 'default') {
          defaultTemplateId = MessageLibrarySrv.defaultVersion().template.value;
          defaultTemplate = MessageLibrarySrv.getTemplateById(defaultTemplateId);
          MessageLibrarySrv.setMessageTemplateCreativeTemplate(defaultTemplate);
          return MessageLibrarySrv.updateMessage('template', null, $scope.currentTemplate);
        }
      };
      $scope.selectTemplate = function(template) {
        $scope.currentTemplate.value = template.id;
        if (($scope.currentTemplate.ref !== 'default' && $scope.currentVersion.id !== 'default') || $scope.currentVersion.id === 'default') {
          MessageLibrarySrv.setMessageTemplateCreativeTemplate(template);
          return MessageLibrarySrv.updateMessage('template', null, $scope.currentTemplate);
        }
      };
      $scope.onMessageNameChange = function() {
        return MessageLibrarySrv.setMessageTemplateName(_.clone($scope.message.messageName));
      };
      $scope.onSlugNameChange = function() {
        return MessageLibrarySrv.messageSlugNameSet(_.clone($scope.message.slugName));
      };
      return showTemplateWarning = function() {
        if ($scope.currentVersion.template == null) {
          return false;
        }
        if ($scope.messageId === "new" || $scope.currentVersion.template === -1 || ($scope.currentVersion.template.value != null) === -1) {
          return true;
        } else {
          if ($scope.currentVersion.template.value != null) {
            return _.find($scope.messageTemplates, {
              'id': $scope.currentVersion.template.value
            }) != null;
          } else {
            return _.find($scope.messageTemplates, {
              'id': $scope.currentVersion.template
            }) != null;
          }
        }
        return false;
      };
    }
  ]).controller('MessageLibrarySubjectsCtrl', [
    '$scope', 'MessageLibrarySrv', function($scope, MessageLibrarySrv) {
      $scope.service = MessageLibrarySrv;
      $scope.currentSubject = {};
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
      $scope.$watch('service.versionBeingEdited()', function() {
        if (MessageLibrarySrv.versionBeingEdited().subject != null) {
          $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
          return $scope.currentSubject = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().subject);
        }
      }, true);
      $scope.onSubjectChange = function() {
        return MessageLibrarySrv.updateMessage('subject', null, $scope.currentSubject);
      };
      return $scope.onUseDefaultChange = function() {
        if ($scope.currentSubject.ref === 'default') {
          $scope.currentSubject.value = MessageLibrarySrv.defaultVersion().subject.value;
        }
        return MessageLibrarySrv.updateMessage('subject', null, $scope.currentSubject);
      };
    }
  ]).controller('MessageLibraryLinksCtrl', [
    '$scope', 'MessageLibrarySrv', function($scope, MessageLibrarySrv) {
      $scope.service = MessageLibrarySrv;
      $scope.htmlLinks = {};
      $scope.textLinks = {};
      $scope.availableLandingPages = MessageLibrarySrv.landingPages();
      $scope.hasActiveLinks = false;
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
      $scope.$watch('service.versionBeingEdited()', function() {
        var a, i, j, l, linkToLookAt, ref1, ref2, ref3, ref4, ref5;
        if ((MessageLibrarySrv.versionBeingEdited().html != null) && (MessageLibrarySrv.versionBeingEdited().html.links != null)) {
          $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
          $scope.htmlLinks = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().html.links);
          $scope.textLinks = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().text.links);
          $scope.hasActiveLinks = false;
          $scope.activeLinkTab = null;
          if ($scope.currentVersion.template.value !== "-1") {
            $scope.selectedTemplate = MessageLibrarySrv.getTemplateById($scope.currentVersion.template.value);
            if (((ref1 = $scope.selectedTemplate) != null ? ref1.meta.html.links.length : void 0) > 0) {
              for (a = i = 0, ref2 = $scope.selectedTemplate.meta.html.links.length; 0 <= ref2 ? i < ref2 : i > ref2; a = 0 <= ref2 ? ++i : --i) {
                if (($scope.selectedTemplate.meta.html.links[a].needs == null) || $scope.selectedTemplate.meta.html.links[a].needs[0].indexOf('text') === -1) {
                  if (($scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name] != null) && ($scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name].text != null)) {
                    delete $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name]["text"];
                  }
                }
                if (($scope.selectedTemplate.meta.html.links[a].needs != null) && $scope.selectedTemplate.meta.html.links[a].needs[0].indexOf('text') !== -1) {
                  if (($scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name] != null) && ($scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name].text == null)) {
                    linkToLookAt = $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name];
                    linkToLookAt["text"] = '';
                  }
                }
                for (a = j = 0, ref3 = $scope.selectedTemplate.meta.html.links.length; 0 <= ref3 ? j < ref3 : j > ref3; a = 0 <= ref3 ? ++j : --j) {
                  if ($scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name] == null) {
                    $scope.htmlLinks[$scope.selectedTemplate.meta.html.links[a].name] = {
                      type: 'url'
                    };
                  }
                }
                for (a = l = 0, ref4 = $scope.selectedTemplate.meta.text.links.length; 0 <= ref4 ? l < ref4 : l > ref4; a = 0 <= ref4 ? ++l : --l) {
                  if ($scope.textLinks[$scope.selectedTemplate.meta.text.links[a].name] == null) {
                    $scope.textLinks[$scope.selectedTemplate.meta.text.links[a].name] = {
                      type: 'url'
                    };
                  }
                }
              }
              $scope.hasActiveLinks = true;
              $scope.activeLinkTab = 'html' + $scope.selectedTemplate.meta.html.links[0].name;
            } else if (((ref5 = $scope.selectedTemplate) != null ? ref5.meta.text.links.length : void 0) > 0) {
              $scope.hasActiveLinks = true;
              $scope.activeLinkTab = 'text' + $scope.selectedTemplate.meta.text.links[0].name;
            }
            if (MessageLibrarySrv.defaultVersion() != null) {
              return $scope.defaultTemplate = MessageLibrarySrv.getTemplateById(MessageLibrarySrv.defaultVersion().template.value);
            }
          }
        }
      }, true);
      $scope.$watch('service.landingPages()', function() {
        return $scope.availableLandingPages = MessageLibrarySrv.landingPages();
      });
      $scope.doesLinkHaveNeeds = function(link) {
        if (link.needs != null) {
          return link.needs[0].indexOf('text') !== -1;
        } else {
          return false;
        }
      };
      $scope.onHTMLLinkChange = function() {
        return MessageLibrarySrv.updateMessage('html', 'links', $scope.htmlLinks);
      };
      $scope.onTextLinkChange = function() {
        return MessageLibrarySrv.updateMessage('text', 'links', $scope.textLinks);
      };
      $scope.onUseDefaultChange = function() {
        _.forEach($scope.htmlLinks, function(value, key) {
          if ($scope.htmlLinks[key].ref === 'default') {
            return $scope.htmlLinks[key] = MessageLibrarySrv.defaultVersion().html.links[key];
          }
        });
        return _.forEach($scope.textLinks, function(value, key) {
          if ($scope.textLinks[key].ref === 'default') {
            $scope.textLinks[key] = MessageLibrarySrv.defaultVersion().text.links[key];
          }
          MessageLibrarySrv.updateMessage('html', 'links', $scope.htmlLinks);
          return MessageLibrarySrv.updateMessage('text', 'links', $scope.textLinks);
        });
      };
      return $scope.linkTabClick = function(id) {
        return $scope.activeLinkTab = id;
      };
    }
  ]).controller('MessageLibraryHtmlBlocksCtrl', [
    '$scope', 'MessageLibrarySrv', function($scope, MessageLibrarySrv) {
      $scope.activeHtmlTab = '';
      $scope.service = MessageLibrarySrv;
      $scope.hasActiveHTMLBlocks = false;
      $scope.currentHTMLBlocks = {};
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
      $scope['editor.htmlblock'] = {
        extraPlugins: 'strinsert'
      };
      $scope.$watch('service.versionBeingEdited()', function() {
        var ref1, ref2;
        if (((ref1 = MessageLibrarySrv.versionBeingEdited().html) != null ? ref1.blocks : void 0) != null) {
          $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
          $scope.currentHTMLBlocks = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().html.blocks);
          $scope.hasActiveHTMLBlocks = false;
          $scope.activeHtmlTab = '';
          if ($scope.currentVersion.template.value !== "-1") {
            $scope.selectedTemplate = MessageLibrarySrv.getTemplateById($scope.currentVersion.template.value);
            if (MessageLibrarySrv.defaultVersion() != null) {
              $scope.defaultTemplate = MessageLibrarySrv.getTemplateById(MessageLibrarySrv.defaultVersion().template.value);
            }
            if (((ref2 = $scope.selectedTemplate) != null ? ref2.meta.html.blocks.length : void 0) > 0) {
              $scope.hasActiveHTMLBlocks = true;
              return $scope.activeHtmlTab = $scope.selectedTemplate.meta.html.blocks[0].name;
            }
          }
        }
      }, true);
      $scope.onBlockChange = function() {
        return MessageLibrarySrv.updateMessage('html', 'blocks', $scope.currentHTMLBlocks);
      };
      $scope.onUseDefaultChange = function() {
        _.forEach($scope.currentHTMLBlocks, function(value, key) {
          if ($scope.currentHTMLBlocks[key].ref === 'default') {
            return $scope.currentHTMLBlocks[key].value = MessageLibrarySrv.defaultVersion().html.blocks[key].value;
          }
        });
        return MessageLibrarySrv.updateMessage('html', 'blocks', $scope.currentHTMLBlocks);
      };
      return $scope.htmlTabClick = function(key) {
        return $scope.activeHtmlTab = key;
      };
    }
  ]).controller('MessageLibraryPlainTextCtrl', [
    '$scope', 'MessageLibrarySrv', function($scope, MessageLibrarySrv) {
      $scope.activePlainTextTab = '';
      $scope.currentPlainText = [];
      $scope.hasActivePlainTexts = false;
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
      $scope.$watch('service.versionBeingEdited()', function() {
        var ref1;
        if (MessageLibrarySrv.versionBeingEdited().text != null) {
          $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
          $scope.currentPlainText = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().text.blocks);
          $scope.hasActivePlainTexts = false;
          $scope.activePlainTextTab = '';
          if ($scope.currentVersion.template.value !== "-1") {
            $scope.selectedTemplate = MessageLibrarySrv.getTemplateById($scope.currentVersion.template.value);
            if (MessageLibrarySrv.defaultVersion() != null) {
              $scope.defaultTemplate = MessageLibrarySrv.getTemplateById(MessageLibrarySrv.defaultVersion().template.value);
            }
            if (((ref1 = $scope.selectedTemplate) != null ? ref1.meta.text.blocks.length : void 0) > 0) {
              $scope.hasActivePlainTexts = true;
              return $scope.activePlainTextTab = $scope.selectedTemplate.meta.text.blocks[0].name;
            }
          }
        }
      }, true);
      $scope.onPlainTextChange = function() {
        return MessageLibrarySrv.updateMessage('text', 'blocks', $scope.currentPlainText);
      };
      $scope.onUseDefaultChange = function() {
        _.forEach($scope.currentPlainText, function(value, key) {
          if ($scope.currentPlainText[key].ref === 'default') {
            return $scope.currentPlainText[key].value = MessageLibrarySrv.defaultVersion().text.blocks[key].value;
          }
        });
        return MessageLibrarySrv.updateMessage('text', 'blocks', $scope.currentPlainText);
      };
      return $scope.plainTextTabClick = function(key) {
        return $scope.activePlainTextTab = key;
      };
    }
  ]).controller('MessageLibraryPoliciesCtrl', [
    '$scope', 'MessageLibrarySrv', function($scope, MessageLibrarySrv) {
      $scope.service = MessageLibrarySrv;
      $scope.availablePolicies = MessageLibrarySrv.policies();
      $scope.htmlPolicies = {};
      $scope.textPolicies = {};
      $scope.hasActivePolicies = false;
      $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
      $scope.$watch('service.versionBeingEdited()', function() {
        var ref1, ref2, ref3;
        if (((ref1 = MessageLibrarySrv.versionBeingEdited().html) != null ? ref1.policies : void 0) != null) {
          $scope.currentVersion = MessageLibrarySrv.versionBeingEdited();
          $scope.textPolicies = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().text.policies);
          $scope.htmlPolicies = _.cloneDeep(MessageLibrarySrv.versionBeingEdited().html.policies);
          if ($scope.currentVersion.template.value !== "-1") {
            $scope.selectedTemplate = MessageLibrarySrv.getTemplateById($scope.currentVersion.template.value);
            $scope.hasActivePolicies = ((ref2 = $scope.selectedTemplate) != null ? ref2.meta.html.policies.length : void 0) > 0 || ((ref3 = $scope.selectedTemplate) != null ? ref3.meta.text.policies.length : void 0) > 0;
            if (MessageLibrarySrv.defaultVersion() != null) {
              return $scope.defaultTemplate = MessageLibrarySrv.getTemplateById(MessageLibrarySrv.defaultVersion().template.value);
            }
          }
        }
      }, true);
      $scope.$watch('service.policies()', function() {
        return $scope.availablePolicies = MessageLibrarySrv.policies();
      });
      $scope.onHTMLPolicyChange = function() {
        $scope.htmlPolicies;
        return MessageLibrarySrv.updateMessage('html', 'policies', $scope.htmlPolicies);
      };
      $scope.onTextPolicyChange = function() {
        return MessageLibrarySrv.updateMessage('text', 'policies', $scope.textPolicies);
      };
      return $scope.onUseDefaultChange = function() {
        _.forEach($scope.htmlPolicies, function(value, key) {
          if ($scope.htmlPolicies[key].ref === 'default') {
            return $scope.htmlPolicies[key].value = MessageLibrarySrv.defaultVersion().html.policies[key].value;
          }
        });
        _.forEach($scope.textPolicies, function(value, key) {
          if ($scope.textPolicies[key].ref === 'default') {
            return $scope.textPolicies[key].value = MessageLibrarySrv.defaultVersion().text.policies[key].value;
          }
        });
        MessageLibrarySrv.updateMessage('html', 'policies', $scope.htmlPolicies);
        return MessageLibrarySrv.updateMessage('text', 'policies', $scope.textPolicies);
      };
    }
  ]).controller('MessageLibraryPreviewCtrl', [
    '$scope', 'MessageLibrarySrv', function($scope, MessageLibrarySrv) {
      var reloadPreview;
      $scope.service = MessageLibrarySrv;
      $scope.htmlPreview = '';
      $scope.preview = {
        reload: 0
      };
      $scope.$watch('service.versionBeingEdited()', function() {
        var ref1;
        $scope.selectedTemplateId = MessageLibrarySrv.versionBeingEdited().template;
        if (((ref1 = $scope.selectedTemplateId) != null ? ref1.value : void 0) != null) {
          return $scope.selectedTemplateId = $scope.selectedTemplateId.value;
        }
      }, true);
      $scope.$watch('service.getMessageRevision()', function() {
        return reloadPreview();
      }, true);
      return reloadPreview = function() {
        $scope.messageId = MessageLibrarySrv.fetchMessageId();
        $scope.messageProgress = MessageLibrarySrv.messageStatus();
        if ($scope.messageProgress === 'completed' && $scope.selectedTemplateId !== "-1" && $scope.messageId !== "new") {
          return $scope.preview.reload = Math.random();
        }
      };
    }
  ]).controller('AddVersionToMessageModalCtrl', [
    '$scope', '$uibModalInstance', 'dismissCheck', 'saveTimeout', 'errorTimeout', 'checkLocation', 'message', 'currentVersion', 'MessageLibrarySrv', '$state', function($scope, $uibModalInstance, dismissCheck, saveTimeout, errorTimeout, checkLocation, message, currentVersion, MessageLibrarySrv, $state) {
      var active_attributes, i, k, key, len, ref, status_ref;
      $scope.currentVersion = currentVersion;
      $scope.forms = {};
      ref = {};
      status_ref = {};
      $scope.message = message;
      active_attributes = _.pluck(MessageLibrarySrv.attributes(), 'id');
      $scope.attributes = MessageLibrarySrv.attributes();
      $scope.currentVersion.filters = {};
      for (i = 0, len = active_attributes.length; i < len; i++) {
        k = active_attributes[i];
        if (!(k in $scope.currentVersion.filters)) {
          $scope.currentVersion.filters[k] = [];
        }
      }
      for (key in $scope.currentVersion.filters) {
        if (indexOf.call(active_attributes, key) < 0) {
          delete $scope.currentVersion.filters[key];
        }
      }
      $scope.$watch('currentVersion.filters', function(newValue, oldValue) {
        if (!newValue) {

        }
      });
      $scope.selectall = function(arg) {
        var id, values;
        id = arg.id, values = arg.values;
        $scope.currentVersion.filters[id] = _.uniq($scope.currentVersion.filters[id].concat(_.pluck(values, 'id')));
        return false;
      };
      $scope.deselectall = function(arg) {
        var id, values;
        id = arg.id, values = arg.values;
        $scope.currentVersion.filters[id] = [];
        return false;
      };
      $scope.createMessageVersion = function() {
        var version;
        MessageLibrarySrv.addVersion($scope.message, $scope.currentVersion);
        $uibModalInstance.dismiss();
        message = MessageLibrarySrv.getCurrentMessageId();
        version = $scope.currentVersion.id;
        return $state.go('manage-messagelibrary.message.version.state', {
          message: message,
          version: version,
          state: 'templates'
        });
      };
      return $scope.close = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]).controller('EditVersionAttributesModalCtrl', [
    '$scope', '$uibModalInstance', 'dismissCheck', 'saveTimeout', 'errorTimeout', 'checkLocation', 'message', 'currentVersion', 'MessageLibrarySrv', '$state', function($scope, $uibModalInstance, dismissCheck, saveTimeout, errorTimeout, checkLocation, message, currentVersion, MessageLibrarySrv, $state) {
      var active_attributes, i, k, key, len, ref, status_ref;
      $scope.currentVersion = currentVersion;
      $scope.forms = {};
      ref = {};
      status_ref = {};
      $scope.message = message;
      $scope.attributes = MessageLibrarySrv.attributes();
      if (_.isArray($scope.currentVersion.filters)) {
        active_attributes = _.pluck(MessageLibrarySrv.attributes(), 'id');
        $scope.currentVersion.filters = {};
        for (i = 0, len = active_attributes.length; i < len; i++) {
          k = active_attributes[i];
          if (!(k in $scope.currentVersion.filters)) {
            $scope.currentVersion.filters[k] = [];
          }
        }
        for (key in $scope.currentVersion.filters) {
          if (indexOf.call(active_attributes, key) < 0) {
            delete $scope.currentVersion.filters[key];
          }
        }
      }
      $scope.$watch('currentVersion.filters', function(newValue, oldValue) {
        if (!newValue) {
          return;
        }
        if (!_.isEqual(newValue, oldValue)) {
          return $scope.forms.currentVersion.$setDirty();
        }
      }, true);
      $scope.selectall = function(arg) {
        var id, values;
        id = arg.id, values = arg.values;
        return $scope.currentVersion.filters[id] = _.uniq($scope.currentVersion.filters[id].concat(_.pluck(values, 'id')));
      };
      $scope.deselectall = function(arg) {
        var id, values;
        id = arg.id, values = arg.values;
        return $scope.currentVersion.filters[id] = [];
      };
      $scope.saveVersionAttributes = function() {
        MessageLibrarySrv.saveVersionAttributes($scope.message, $scope.currentVersion);
        return $uibModalInstance.dismiss();
      };
      return $scope.close = function() {
        if ($scope.forms.currentVersion.saved && $scope.forms.currentVersion.$pristine) {
          return $uibModalInstance.close($scope.status);
        } else if ($scope.forms.currentVersion.$invalid || $scope.forms.currentVersion.$dirty || !_.isEqual($scope.currentVersion.filters, ref)) {
          return dismissCheck.dismiss($uibModalInstance, {
            item: $scope.status,
            ref: status_ref,
            save: $scope.save
          });
        } else {
          return $uibModalInstance.dismiss();
        }
      };
    }
  ]).controller('CloneVersionModalCtrl', [
    '$scope', '$uibModalInstance', 'dismissCheck', 'saveTimeout', 'errorTimeout', 'checkLocation', 'message', 'version', 'MessageLibrarySrv', '$state', function($scope, $uibModalInstance, dismissCheck, saveTimeout, errorTimeout, checkLocation, message, version, MessageLibrarySrv, $state) {
      var active_attributes, i, k, key, len, ref, status_ref;
      $scope.currentVersion = _.cloneDeep(version);
      $scope.currentVersion.name = $scope.currentVersion.name + " (Cloned)";
      $scope.cloneTitle = _.cloneDeep(version);
      $scope.forms = {};
      ref = {};
      status_ref = {};
      $scope.message = message;
      $scope.attributes = MessageLibrarySrv.attributes();
      if (_.isArray($scope.currentVersion.filters)) {
        active_attributes = _.pluck(MessageLibrarySrv.attributes(), 'id');
        $scope.currentVersion.filters = {};
        for (i = 0, len = active_attributes.length; i < len; i++) {
          k = active_attributes[i];
          if (!(k in $scope.currentVersion.filters)) {
            $scope.currentVersion.filters[k] = [];
          }
        }
        for (key in $scope.currentVersion.filters) {
          if (indexOf.call(active_attributes, key) < 0) {
            delete $scope.currentVersion.filters[key];
          }
        }
      }
      $scope.selectall = function(arg) {
        var id, values;
        id = arg.id, values = arg.values;
        if ($scope.currentVersion.filters[id] == null) {
          $scope.currentVersion.filters[id] = [];
        }
        $scope.currentVersion.filters[id] = _.uniq($scope.currentVersion.filters[id].concat(_.pluck(values, 'id')));
        return false;
      };
      $scope.deselectall = function(arg) {
        var id, values;
        id = arg.id, values = arg.values;
        $scope.currentVersion.filters[id] = [];
        return false;
      };
      $scope.createMessageVersion = function() {
        $scope.currentVersion.id = uuid.v4();
        MessageLibrarySrv.addVersion($scope.message, $scope.currentVersion);
        $uibModalInstance.dismiss();
        message = MessageLibrarySrv.getCurrentMessageId();
        version = $scope.currentVersion.id;
        return $state.go('manage-messagelibrary.message.version.state', {
          message: message,
          version: version,
          state: 'templates'
        });
      };
      return $scope.close = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]).controller('DeleteVersionModalCtrl', [
    '$scope', '$uibModalInstance', 'message', 'version', 'MessageLibrarySrv', function($scope, $uibModalInstance, message, version, MessageLibrarySrv) {
      $scope.version = version;
      $scope.message = message;
      $scope.versionDeleteConfirmed = function() {
        MessageLibrarySrv.removeVersion(message, version);
        return $uibModalInstance.dismiss();
      };
      return $scope.cancelVersionDelete = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]);

}).call(this);
