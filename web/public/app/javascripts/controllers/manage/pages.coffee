isValidEmailAddress = (email) ->
  atidx = email.indexOf '@'
  email.length >= 3 && ~atidx && atidx < email.length - 1

angular.module('app.manage')
  .config [
    '$stateProvider', '$urlRouterProvider',
    ($stateProvider, $urlRouterProvider) ->
      $stateProvider
        .state 'manage-pages',
          data: section: 'manage'
          url: '/manage/pages'
          templateUrl: '/html/manage/pages.html'
          controller: 'ManagePages'
          resolve:
            pages: ['$http', ($http) ->
              $http.get 'api/pages?q=active'
            ]
            layouts: ['$http', ($http) ->
              $http.get 'api/layouts?q=active'
            ]
        .state 'manage-pages.layout',
          url: '/layout/:id'
          data: type: 'layout'
        .state 'manage-pages.page',
          url: '/page/:id'
          data: type: 'page'
  ]

  .controller 'ManagePages', [
    '$scope', '$http', '$timeout', '$state', '$q', '$uibModal', 'pages', 'layouts', 'errorTimeout', 'saveTimeout', 'growl',
    ($scope, $http, $timeout, $state, $q, $uibModal, pages, layouts, errorTimeout, saveTimeout, growl) ->
      $scope.pages = pages.data
      $scope.layouts = layouts.data
      $scope.search = {}

      $scope.$on '$stateChangeSuccess', setview = ->
        $scope.template = undefined
        $scope.type = $state.$current.data?.type

        return unless id = $state.params.id

        if id is 'new'
          $scope.template = hbs: '', pagetype: $scope.type
        else
          $scope.template = _.find([].concat($scope.layouts, $scope.pages), id: id)
        $scope.template.variables ?= []

      $scope.$watch 'search.pages', (search) ->
        re = new RegExp search, 'i'
        ptags = []
        for page in $scope.pages
          ptags = if page.tags? then page.tags.split ', '
          page.$hidden = unless search then no else not re.test(page.name) and not re.test(ptags)

      $scope.$watch 'search.layouts', (search) ->
        re = new RegExp search, 'i'
        ltags = []
        for layout in $scope.layouts
          ltags = if layout.tags? then layout.tags.split ', '
          layout.$hidden = unless search then no else not re.test(layout.name) and not re.test(ltags)

      $scope.anyVisible = (what) ->
        _.any what, (i) -> return not i.$hidden

      $scope.save = (type = $scope.type, template = $scope.template) ->
        unless $scope.form.page
          $scope.form.page = {}
          $scope.form.page.$setPristine = ->
            $scope.form.page.$pristine = yes
            $scope.form.page.$dirty = no

        $scope.form.page.submitted = no
        $scope.form.page.formErrors = undefined
        $scope.form.page.saving = yes
        $scope.form.page.saved = no
        start = Date.now()

        done = ->
          saveTimeout.begin start, $scope.form.page
          template.updated = (new Date).toJSON()
        if $scope.form.page.$invalid
          errorTimeout.begin $scope.form.page, start
        else
          if $scope.template?.response?.recipients?.length
            for email in $scope.template.response.recipients
              email = email.trim()
              if not isValidEmailAddress email
                $scope.form.page.$invalid = yes
                $scope.form.page.formErrors = 'Please make sure all email addresses are valid'
                errorTimeout.begin $scope.form.page, start
                return

        if $scope.form.page?.$invalid
          errorTimeout.begin $scope.form.page, start
        else
          if template.response?.recipients?.length
            for email in template.response.recipients
              email = email.trim()
              if not isValidEmailAddress email
                $scope.form.page.$invalid = yes
                $scope.form.page.formErrors = 'Please make sure all email addresses are valid'
                errorTimeout.begin $scope.form.page, start
                return
          url = "api/#{type}s"
          if template.id?
            $http.put("#{url}/#{template.id}", template)
            .success (result) ->
              template.version = result.version
              done()
            .error (err) ->
              $scope.form.page.$invalid = yes
              $scope.form.page.formErrors = err
              $scope.form.page.$setPristine()
          else
            $http.post(url, template)
            .success (result) ->
              template.id = result.id
              template.version = result.version
              $scope[type + 's'].push template
              done()
            .error (err) ->
              $scope.form.page.$invalid = yes
              $scope.form.page.formErrors = err
              $scope.form.page.$setPristine()

      $scope.delete = (type, template) ->
        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'
          resolve:
            type: -> type

        inst.result.then ->
          $http.delete "api/#{type}s/#{template.id}"
          .success (body) ->
            _.remove $scope[type + 's'], id: template.id
            growl.success 'Deleted!'
          .error (err) ->
            growl.danger "Failed to delete #{type}. ", err

      $scope.remove = (arr, item) ->
        arr.splice arr.indexOf(item), 1
  ]
