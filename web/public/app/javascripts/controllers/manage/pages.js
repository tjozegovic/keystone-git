(function() {
  var isValidEmailAddress;

  isValidEmailAddress = function(email) {
    var atidx;
    atidx = email.indexOf('@');
    return email.length >= 3 && ~atidx && atidx < email.length - 1;
  };

  angular.module('app.manage').config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      return $stateProvider.state('manage-pages', {
        data: {
          section: 'manage'
        },
        url: '/manage/pages',
        templateUrl: '/html/manage/pages.html',
        controller: 'ManagePages',
        resolve: {
          pages: [
            '$http', function($http) {
              return $http.get('api/pages?q=active');
            }
          ],
          layouts: [
            '$http', function($http) {
              return $http.get('api/layouts?q=active');
            }
          ]
        }
      }).state('manage-pages.layout', {
        url: '/layout/:id',
        data: {
          type: 'layout'
        }
      }).state('manage-pages.page', {
        url: '/page/:id',
        data: {
          type: 'page'
        }
      });
    }
  ]).controller('ManagePages', [
    '$scope', '$http', '$timeout', '$state', '$q', '$uibModal', 'pages', 'layouts', 'errorTimeout', 'saveTimeout', 'growl', function($scope, $http, $timeout, $state, $q, $uibModal, pages, layouts, errorTimeout, saveTimeout, growl) {
      var setview;
      $scope.pages = pages.data;
      $scope.layouts = layouts.data;
      $scope.search = {};
      $scope.$on('$stateChangeSuccess', setview = function() {
        var base, id, ref;
        $scope.template = void 0;
        $scope.type = (ref = $state.$current.data) != null ? ref.type : void 0;
        if (!(id = $state.params.id)) {
          return;
        }
        if (id === 'new') {
          $scope.template = {
            hbs: '',
            pagetype: $scope.type
          };
        } else {
          $scope.template = _.find([].concat($scope.layouts, $scope.pages), {
            id: id
          });
        }
        return (base = $scope.template).variables != null ? base.variables : base.variables = [];
      });
      $scope.$watch('search.pages', function(search) {
        var j, len, page, ptags, re, ref, results;
        re = new RegExp(search, 'i');
        ptags = [];
        ref = $scope.pages;
        results = [];
        for (j = 0, len = ref.length; j < len; j++) {
          page = ref[j];
          ptags = page.tags != null ? page.tags.split(', ') : void 0;
          results.push(page.$hidden = !search ? false : !re.test(page.name) && !re.test(ptags));
        }
        return results;
      });
      $scope.$watch('search.layouts', function(search) {
        var j, layout, len, ltags, re, ref, results;
        re = new RegExp(search, 'i');
        ltags = [];
        ref = $scope.layouts;
        results = [];
        for (j = 0, len = ref.length; j < len; j++) {
          layout = ref[j];
          ltags = layout.tags != null ? layout.tags.split(', ') : void 0;
          results.push(layout.$hidden = !search ? false : !re.test(layout.name) && !re.test(ltags));
        }
        return results;
      });
      $scope.anyVisible = function(what) {
        return _.any(what, function(i) {
          return !i.$hidden;
        });
      };
      $scope.save = function(type, template) {
        var done, email, j, k, len, len1, ref, ref1, ref2, ref3, ref4, ref5, ref6, ref7, start, url;
        if (type == null) {
          type = $scope.type;
        }
        if (template == null) {
          template = $scope.template;
        }
        if (!$scope.form.page) {
          $scope.form.page = {};
          $scope.form.page.$setPristine = function() {
            $scope.form.page.$pristine = true;
            return $scope.form.page.$dirty = false;
          };
        }
        $scope.form.page.submitted = false;
        $scope.form.page.formErrors = void 0;
        $scope.form.page.saving = true;
        $scope.form.page.saved = false;
        start = Date.now();
        done = function() {
          saveTimeout.begin(start, $scope.form.page);
          return template.updated = (new Date).toJSON();
        };
        if ($scope.form.page.$invalid) {
          errorTimeout.begin($scope.form.page, start);
        } else {
          if ((ref = $scope.template) != null ? (ref1 = ref.response) != null ? (ref2 = ref1.recipients) != null ? ref2.length : void 0 : void 0 : void 0) {
            ref3 = $scope.template.response.recipients;
            for (j = 0, len = ref3.length; j < len; j++) {
              email = ref3[j];
              email = email.trim();
              if (!isValidEmailAddress(email)) {
                $scope.form.page.$invalid = true;
                $scope.form.page.formErrors = 'Please make sure all email addresses are valid';
                errorTimeout.begin($scope.form.page, start);
                return;
              }
            }
          }
        }
        if ((ref4 = $scope.form.page) != null ? ref4.$invalid : void 0) {
          return errorTimeout.begin($scope.form.page, start);
        } else {
          if ((ref5 = template.response) != null ? (ref6 = ref5.recipients) != null ? ref6.length : void 0 : void 0) {
            ref7 = template.response.recipients;
            for (k = 0, len1 = ref7.length; k < len1; k++) {
              email = ref7[k];
              email = email.trim();
              if (!isValidEmailAddress(email)) {
                $scope.form.page.$invalid = true;
                $scope.form.page.formErrors = 'Please make sure all email addresses are valid';
                errorTimeout.begin($scope.form.page, start);
                return;
              }
            }
          }
          url = "api/" + type + "s";
          if (template.id != null) {
            return $http.put(url + "/" + template.id, template).success(function(result) {
              template.version = result.version;
              return done();
            }).error(function(err) {
              $scope.form.page.$invalid = true;
              $scope.form.page.formErrors = err;
              return $scope.form.page.$setPristine();
            });
          } else {
            return $http.post(url, template).success(function(result) {
              template.id = result.id;
              template.version = result.version;
              $scope[type + 's'].push(template);
              return done();
            }).error(function(err) {
              $scope.form.page.$invalid = true;
              $scope.form.page.formErrors = err;
              return $scope.form.page.$setPristine();
            });
          }
        }
      };
      $scope["delete"] = function(type, template) {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal',
          resolve: {
            type: function() {
              return type;
            }
          }
        });
        return inst.result.then(function() {
          return $http["delete"]("api/" + type + "s/" + template.id).success(function(body) {
            _.remove($scope[type + 's'], {
              id: template.id
            });
            return growl.success('Deleted!');
          }).error(function(err) {
            return growl.danger("Failed to delete " + type + ". ", err);
          });
        });
      };
      return $scope.remove = function(arr, item) {
        return arr.splice(arr.indexOf(item), 1);
      };
    }
  ]);

}).call(this);
