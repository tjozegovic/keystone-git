angular.module('app.manage' )
  .config [
    '$stateProvider', '$urlRouterProvider',
    ($stateProvider, $urlRouterProvider) ->
      $stateProvider
        .state 'manage-statuses',
          data: section: 'manage'
          url: '/manage/statuses'
          templateUrl: '/html/manage/statuses.html'

        .state 'manage-statuses.status',
          url: '/:status'
        .state 'manage-statuses.status.segment',
          url: '/:segment'
  ]

  .factory 'ContextMenuService', ->
    menu: null # track open context menus so we can close them, leaving only one open

  .directive 'contextMenu', [
    '$compile', '$parse', '$templateCache', 'ContextMenuService',
    ($compile, $parse, $templateCache, contextMenuService) ->
      scope: no
      link: (scope, elem, attrs) ->
        html = $templateCache.get attrs.contextMenu
        el = $compile(html)(scope)

        elem.on 'click', '.context', (e) ->
          e.stopPropagation()
          scope.menu.contextmenu 'show', e

        $('body').append el
        scope.menu = $(elem[0]).contextmenu
          target: el
          before: (e) ->
            contextMenuService.menu?.contextmenu 'closemenu', e
            contextMenuService.menu = scope.menu
            scope.$apply -> scope.item = $parse(attrs.contextMenuItem)(scope)
            return yes # tell context menu to open

        # if the parent element is removed from the dom, i.e. we switch segments or statuses
        # we should also remove the created menus from the dom, as they are no longer needed
        elem.on 'remove', ->
          el.remove()
  ]

  .factory 'httpRequest', ['$http', ($http) ->
    save: (status, cb, successMessage = null, errorMessage = null) ->
      status.successMessage = successMessage if successMessage
      status.errorMessage = errorMessage if errorMessage
      $http.put('/api/statuses/' + status.id, status)
      .success (data) ->
        cb null, data
      .error (err) ->
        cb err
  ]

  .factory 'httpPost', ['$http', ($http) ->
    save: (status, cb) ->
      $http.post('/api/statuses', status)
      .success (data) ->
        cb null, data
      .error (err) ->
        cb err
  ]

  .filter 'orderStatuses',[ ->
    return (input, sortBy) ->
      ordered = []
      for key in sortBy
        ordered.push input[key]
      return ordered
  ]

  .controller 'ManageStatuses',
    ['$scope', '$http', '$timeout', '$state', '$uibModal', '$q', 'growl', 'httpRequest',
    'MessageLibrarySrv',
    ($scope, $http, $timeout, $state, $uibModal, $q, growl, httpRequest,
    MessageLibrarySrv) ->
      $scope.$search = {}
      $scope.messageService = MessageLibrarySrv

      $scope.$watch 'order', (neworder, oldorder) ->
        return unless oldorder and not neworder.compare oldorder
        $http.put('/api/settings/statuses', neworder)
      , yes

      # create a scope.load function and call it right away
      do $scope.load = (cb = null) ->
        $scope.$loading = yes
        start = Date.now()

        urls = ['attributes?q=active', 'settings/statuses', 'statuses?q=active', 'pages', 'messages?q=all']
        $q.all _.map(urls, (url) -> $http.get "/api/#{url}")
        .then ([attributes, order, statuses, pages, messages]) ->
          $scope.attributes = attributes.data
          $scope.availableLandingPages = pages.data
          $scope.templatesInLibrary = messages.data

          for id, {segments} of statuses.data
            segments ?= []
            fixSegment segment for segment in segments

          $scope.statuses = statuses.data
          _.forIn $scope.statuses, (value, key) ->
            _.forEach value.segments, (segment) ->
              _.forEach segment.steps, (step) ->
                if step.message?
                  if step.message.id?
                    step.message = step.message.id
                  step.message = _.find $scope.templatesInLibrary, {'id' : step.message}

                  if step.delay.units == "day"
                    step.name = "Day"
                  else
                    step.name = "Hour"
                  step.name += " " + step.delay.value + " - " + step.message?.name
          $scope.order = if _.isArray(order.data) and order.data?.length is _.keys($scope.statuses).length
            order.data
          else
            (id for id, status of $scope.statuses when status.active)

          _.remove $scope.order,(n) ->
            return $scope.statuses[n] == undefined

          _.forIn $scope.statuses, (value, key) ->
            if(_.indexOf($scope.order, value.id ) == -1)
              $scope.order.push value.id

          {status, segment} = $state.params
          if status?
            $scope.setStatus _.find($scope.statuses, (m) -> m.name is status), segment?

            if segment?
              $scope.setSegment _.find $scope.selected_status.segments, (s) -> s.name is segment
          $timeout ->
            $scope.$loading = no
          , Math.max 0, 800 - (Date.now() - start)

      # watches dragging variable and saves when dragging is no (done)
      $scope.$watch 'dragging', (dragging) ->
        return unless $scope.selected_status and $scope.segmentOrder and dragging is no
        unless $scope.segmentOrder.compare $scope.selected_status.segments
          if $scope.selected_segment?.steps?
            $scope.selected_segment.steps = _.sortBy $scope.selected_segment.steps, (step) ->
              if step.delay.units == 'day'
                step.delay.value * 24
              else
                step.delay.value
          httpRequest.save $scope.selected_status, (err, data) ->
            if data?
              $scope.segmentOrder = _.clone $scope.selected_status.segments
              $scope.selected_status.version = data.version
          , "Saved!", "Error saving segment order for status #{$scope.selected_status.name} "

      $scope.status_stats = (status) ->
        segments: status.segments.length
        messages: _.reduce status.segments, ((acc, seg) -> acc + seg.steps?.length or 0), 0

      fixSegment = (segment = {}) ->
        segment.steps ?= []
        segment.filters = {} unless _.isPlainObject segment.filters

        active_attributes = _.pluck $scope.attributes, 'id'
        segment.filters[k] = [] for k in active_attributes when k not of segment.filters
        delete segment.filters[key] for key of segment.filters when key not in active_attributes
        segment

      $scope.draggable =
        start: -> $scope.dragging = yes
        stop: -> $timeout (-> $scope.dragging = no), 10

      $scope.setStatus = do (unwatch = undefined) -> (status, notransition) ->
        return if $scope.dragging or not status

        unwatch?()
        $scope.selected_status = status
        $scope.segmentOrder = _.clone status.segments
        $state.go 'manage-statuses.status',
          status: status.name unless notransition

        $scope.selected_segment = null

      $scope.setSegment = (segment) ->
        return if $scope.dragging or not segment

        $scope.selected_segment = segment
        for a in $scope.selected_segment.steps
          if(a.id == undefined)
            a.id =uuid.v4()
          _.forEach a.message?.html?.links, (link) ->
            if link.type is 'page'
              link.name = _.result _.find($scope.availableLandingPages, id: link.value), 'name'
              link.pagetype = _.result _.find($scope.availableLandingPages, id: link.value), 'pagetype'
        $state.go 'manage-statuses.status.segment',
          status: $scope.selected_status.name
          segment: segment.name

      $scope.showContext = ($event) ->
        $event.stopPropagation()
        return false

      $scope.anyVisible = (what) ->
        _.any what, (i) -> return not i.$hidden

      $scope.newStatus = ->
        inst = $uibModal.open
          templateUrl: 'status-modal.html'
          controller: 'StatusModal'
          resolve: status: -> {}

        inst.result.then (status) ->
          unless _.isEmpty status
            $scope.order.push status.id
            $scope.statuses[status.id] = status
            $scope.setStatus status
            $scope.system.statuses += 1

      $scope.editStatus = (status) ->
        ref = status
        inst = $uibModal.open
          templateUrl: 'status-modal.html'
          controller: 'StatusModal'
          resolve: status: -> angular.copy status

        inst.result.then (status) ->
          angular.copy status, ref

      $scope.deleteStatus = (status) ->
        inst = _confirm 'status'
        inst.result.then ->
          status.active = no
          httpRequest.save status, (err, data) ->
            if data?
              $scope.order = _.without $scope.order, status.id
              $scope.selected_status = null if $scope.selected_status?.id is status.id
              $scope.status = null if $scope.status?.id is status.id
              $scope.system.statuses -= 1
          ,'Deleted!', "Error deleting status #{status.name} "

      $scope.newSegment = ->
        inst = $uibModal.open
          templateUrl: 'segment-modal.html'
          controller: 'SegmentModal'
          resolve:
            segment: -> fixSegment()
            attributes: -> $scope.attributes
            status: -> $scope.selected_status

        inst.result.then (status) ->
          $scope.selected_status = status

      $scope.editSegment = (segment) ->
        ref = segment
        inst = $uibModal.open
          templateUrl: 'segment-modal.html'
          controller: 'SegmentModal'
          resolve:
            segment: -> angular.copy segment
            attributes: -> $scope.attributes
            status: -> $scope.selected_status

        inst.result.then (status) ->
          $scope.selected_status = status

      $scope.deleteSegment = (segment) ->
        inst = _confirm 'segment'
        inst.result.then ->
          _.remove $scope.selected_status.segments, id: segment.id

          httpRequest.save $scope.selected_status, (err, data) ->
            if data?
              $scope.selected_status.version = data.version
              $scope.selected_segment = null if $scope.selected_segment?.id is segment.id
          , 'Deleted!', "Error deleting segment #{segment.name} in status #{$scope.selected_status.name} "

      $scope.editMessage = (drip) ->
        MessageLibrarySrv.setStatusSegmentBreadcrumb($scope.selected_status.name, $scope.selected_segment.name)
        $state.go 'manage-messagelibrary.message.version.state',
          message: drip.message.id
          version: 'default'
          state: 'templates'

      $scope.editDrip = (drip) ->
        cloned = _.cloneDeep drip
        cloned.id = uuid.v4()
        inst = $uibModal.open
          templateUrl: 'drip-edit.html'
          controller: 'DripEditorModalCtrl'
          resolve:
            drip: -> cloned
            segment: -> $scope.selected_segment
            new_step: -> false
        inst.result.then (incomingDrip) ->
          drip.active = no

          dripToAdd = _.cloneDeep(incomingDrip)
          dripToAdd.vuNotification = dripToAdd.notify
          setDripName dripToAdd
          $scope.selected_segment.steps.push dripToAdd

          cleanUpOldSteps()
          saveStatus 'Drip Step Saved', "Error saving status #{$scope.selected_status.name} "

      $scope.newDrip = ->
        newDrip = {}
        newDrip.id = uuid.v4()
        newDrip.delay = {}
        newDrip.delay.units = 'day'
        newDrip.delay.value = '0'
        newDrip.notify = false
        newDrip.vuNotification = false
        newDrip.message = {}
        inst = $uibModal.open
          templateUrl: 'drip-edit.html'
          controller: 'DripEditorModalCtrl'
          resolve:
            drip: -> newDrip
            segment: -> $scope.selected_segment
            new_step: -> true
        inst.result.then (incomingDrip) ->
          dripToAdd = _.cloneDeep(incomingDrip)
          dripToAdd.vuNotification = dripToAdd.notify
          setDripName dripToAdd
          $scope.selected_segment.steps.push dripToAdd

          cleanUpOldSteps()
          saveStatus "Drip Step #{dripToAdd.name} Saved", "Error creating #{dripToAdd.name} "


      $scope.deleteStep = (step) ->
        inst = _confirm 'step'
        inst.result.then ->
          step.active = no
          $scope.selected_segment.steps = _.sortBy $scope.selected_segment.steps, (step) ->
            if step.delay.units == 'day'
              step.delay.value * 24
            else
              step.delay.value

          msg = "Error deleting step #{step.name} in segment " +
            "#{$scope.selected_segment.name} in status #{$scope.selected_status.name}"
          saveStatus "Deleted #{step.name}.", msg

      $scope.cloneStep = (step) ->
        cloned = angular.copy step
        cloned.id = uuid.v4()
        cloned.name = step.name + ' (clone)'
        cloned.message = _.cloneDeep step.message
        $scope.selected_segment.steps.push cloned

        segLocation = _.findIndex $scope.selected_status.segments, id: $scope.selected_segment.id
        if segLocation is -1
          $scope.selected_status.segments.push $scope.selected_segment
        else
          $scope.selected_status.segments[segLocation] = $scope.selected_segment

        msg = "Error cloning step #{cloned.name} in segment " +
          "#{$scope.selected_segment} in status #{$scope.selected_status.name} "
        saveStatus "Cloned #{cloned.name}", msg

      $scope.cloneStepTo = (step) ->
        cloned = angular.copy step
        inst = $uibModal.open
          templateUrl: 'clone-modal.html'
          controller: 'CloneStepModal'
          size: 'sm'
          resolve:
            order: -> $scope.order
            statuses: -> $scope.statuses
            cloned: -> cloned

        inst.result.then (status) ->
          if status.id is $scope.selected_status.id
            $scope.selected_status = status
          $scope.statuses[status.id] = status

      $scope.viewStep = (step) ->
        if not step.message.id?
          step.message = $scope.messageService.cloneMessageById step.message
        template = step.message.template

        inst = $uibModal.open
          templateUrl: 'html-modal.html'
          size: 'lg'
          scope: _.assign $scope.$new(yes),
            id: template
            url: "/messages/#{template}/preview"
            post: id: step.message.id

      $scope.previewStep = (step) ->
        if not step.message.id?
          step.message = $scope.messageService.cloneMessageById step.message

        template = step.message.template
        if template.value?
          template = template.value
        inst = $uibModal.open
          templateUrl: 'sample-modal.html'
          controller: 'SampleModal'
          resolve:
            template: -> step.message.template
            status: -> $scope.selected_status
            segment: -> $scope.selected_segment
            step: -> step

      $scope.openCampaignManager = ->
        status = $scope.selected_status.name
        segment = $scope.selected_segment.name
        $state.go 'manage-messagedripmanager.status.segment',
          status: status
          segment: segment

      _confirm = (type) ->
        $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'
          resolve:
            type: -> type
            scopeItem: -> null
            refItem: -> null

      $scope.statusesImport = ($event) ->
        $event.stopPropagation()
        inst = $uibModal.open
          templateUrl: 'bulkUpload-modal.html'
          controller: 'StatusesUploadModal'

        inst.result.then ->
          $http.get '/api/statuses?q=active'
          .error (err) ->
            growl.danger 'Failed to update statuses'
          .success (result) ->
            for key, value of result
              if _.indexOf($scope.order, key) is -1
                $scope.order.push key
                $scope.system.statuses += 1
            $scope.statuses = result

      $scope.delayInHours = (step) ->
        if step.delay.units == 'day'
          step.delay.value * 24
        else
          step.delay.value

      setDelayInHours = (step) ->
        if step.delay.units is 'day'
          delay = parseInt(step.delay.value, 10) * 24
          step.delay.delayInHours = delay
        else
          delay = parseInt(step.delay.value, 10)
          step.delay.delayInHours = delay

      setDripName = (step) ->
        step_unit = step.delay.units
        step_unit_title = step_unit.charAt(0).toUpperCase() + step_unit.slice(1)
        step_delay = step.delay.value
        step_message_name = step.message.name
        step.name = step_unit_title + " " + step_delay + " - " + step_message_name

      cleanUpOldSteps = ->
        for a in [0...$scope.selected_segment.steps.length]
          if(not $scope.selected_segment.steps[a].message?.id?)
            message_id = $scope.selected_segment.steps[a].message
            $scope.selected_segment.steps[a].message = $scope.messageService.cloneMessageById message_id
          if($scope.selected_segment.steps[a].message?.version?)
            delete $scope.selected_segment.steps[a].message['version']
          setDelayInHours $scope.selected_segment.steps[a]

      saveStatus = (success_message, error_message) ->
        if $scope.selected_segment?.steps?
          $scope.selected_segment.steps = _.sortBy $scope.selected_segment.steps, (step) ->
            if step.delay.units == 'day'
              step.delay.value * 24
            else
              step.delay.value

        httpRequest.save $scope.selected_status, (err, data) ->
          if data?
            $scope.selected_status.version = data.version
        , success_message, error_message
    ]

  .controller 'StatusesUploadModal',
    ['$scope', '$http', '$uibModalInstance', '$uibModal',
    ($scope, $http, $uibModalInstance, $uibModal) ->
      $scope.forms = upload: null

      $scope.$watch 'forms.upload.csv', (csv) ->
        return unless csv
        $scope.typeerror = !~csv.name.indexOf '.csv'

      $scope.url = ->
        if $scope.includeStatuses
          "/api/bulk/statuses/download"
        else
          'bulk/statuses/template/download'

      $scope.upload = ->
        return $scope.typeerror = yes unless ~$scope.forms.upload.csv.name.indexOf '.csv'

        fd = new FormData
        fd.append 'file', $scope.forms.upload.csv

        $http.post("api/bulk/statuses/upload", fd,
          transformRequest: angular.identity
          headers: 'Content-Type': undefined
        ).success (stats) ->
          $uibModalInstance.close()

          inst = $uibModal.open
            templateUrl: 'statusesStats-modal.html'
            controller: ['$scope', '$uibModalInstance', ($scope, $uibModalInstance) ->
              $scope.stats = stats
              $scope.close = -> $uibModalInstance.close()
            ]
  ]

  .directive 'previewIframe', ['$http', ($http) ->
    link: (scope, elm, attr) ->
      iframe = elm[0]
      scope.iframe = iframe.contentWindow or iframe.contentDocument.document or iframe.contentDocument
      scope.error = no
      $http.post(scope.url, scope.post)
        .success (data) ->
          scope.iframe.document.open()
          scope.iframe.document.write data
          scope.iframe.document.close()
        .error (data) ->
          scope.error = data
  ]

  .directive 'messagePreviewIframe', ['$http', ($http) ->
    return (scope, elm, attrs) ->
      iframe = elm[0]
      scope.iframe = iframe.contentWindow or iframe.contentDocument.document or iframe.contentDocument

      refreshPreview = ->
        return unless scope.selectedTemplateId? and scope.messageId?.length and scope.messageId isnt 'new'
        $http.post('/messages/' + scope.selectedTemplateId + '/preview', id: scope.messageId)
          .success (data) ->
            scope.iframe.document.open()
            scope.iframe.document.write data
            scope.iframe.document.close()
          .error (data) ->

      scope.$watch attrs.refreshtrigger, refreshPreview
    ]

  .controller 'StatusModal',
    ['$scope', '$http', '$uibModalInstance', '$timeout', 'status', 'httpRequest',
    'httpPost', 'dismissCheck',
    'saveTimeout', 'errorTimeout',
    ($scope, $http, $uibModalInstance, $timeout, status, httpRequest, httpPost,
    dismissCheck, saveTimeout, errorTimeout) ->
      $scope.status = status or {}
      $scope.forms = {}
      $scope.forms.status = saving: no
      ref = []
      status_ref = {}
      $scope.aliases = _.map $scope.status.aliases or [], (alias) -> text: alias
      angular.copy $scope.aliases, ref
      angular.copy $scope.status, status_ref

      checkId = (status) ->
        if !status.id
          return no
        else
          return yes

      $scope.save = (cb) ->
        start = Date.now()
        $scope.errorMessage = undefined
        $scope.forms.status.saved = no
        $scope.forms.status.saving = yes
        $scope.forms.status.submitted = no
        $scope.status.aliases = _.pluck $scope.aliases, 'text'

        if $scope.forms.status.$invalid
          errorTimeout.begin $scope.forms.status, start
        else
          checkId $scope.status, (status) ->
            $scope.status = status
          if checkId $scope.status
            if $scope.selected_segment?.steps?
              $scope.selected_segment.steps = _.sortBy $scope.selected_segment.steps, (step) ->
                if step.delay.units == 'day'
                  step.delay.value * 24
                else
                  step.delay.value
            httpRequest.save $scope.status, (err, data) ->
              if err
                $scope.errorMessage = err
                errorTimeout.begin $scope.forms.status, start
                if cb? then cb err
              else
                $scope.status.version = data.version
                $scope.errorMessage = undefined
                $scope.forms.status.$setPristine()
                $scope.forms.status.saved = yes
                angular.copy $scope.status, status_ref
                if cb? then cb null, $scope.status
                saveTimeout.begin start, $scope.forms.status
          else
            tempStatus =
              name: $scope.status.name
              active: yes
              aliases: $scope.status.aliases
              segments: []
            $scope.status = tempStatus
            httpPost.save $scope.status, (err, data) ->
              if err
                $scope.errorMessage = err
                errorTimeout.begin $scope.forms.status, start
                if cb? then cb err
              else
                _.assign $scope.status, data
                angular.copy $scope.status, status_ref
                if cb? then cb null, $scope.status
                saveTimeout.begin start, $scope.forms.status

      $scope.close = ->
        if $scope.forms.status.saved and $scope.forms.status.$pristine
          $uibModalInstance.close $scope.status
        else if $scope.forms.status.$invalid or $scope.forms.status.$dirty or not _.isEqual($scope.aliases, ref)
          dismissCheck.dismiss $uibModalInstance, item: $scope.status, ref: status_ref, save: $scope.save
        else
          $uibModalInstance.dismiss()

    ]

  .controller 'SegmentModal',
    ['$scope', '$uibModalInstance', '$timeout', 'segment', 'attributes', 'status',
    'httpRequest', 'dismissCheck',
    'saveTimeout', 'errorTimeout', 'checkLocation',
    ($scope, $uibModalInstance, $timeout, segment, attributes, status, httpRequest,
    dismissCheck, saveTimeout,
    errorTimeout, checkLocation) ->
      $scope.segment = segment
      $scope.attributes = attributes
      $scope.status = status
      $scope.forms = {}
      ref = {}
      status_ref = {}
      angular.copy $scope.segment.filters, ref
      angular.copy $scope.status, status_ref

      $scope.$watch 'segment.filters', (newValue, oldValue) ->
        return unless newValue
        if not _.isEqual newValue, oldValue
          $scope.forms.segment.$setDirty()
      , true

      checkId = (segment, cb) ->
        if !segment.id
          segment.id = uuid.v4()
        cb segment

      $scope.save = (cb) ->
        start = Date.now()
        $scope.errorMessage = undefined
        $scope.forms.segment.saved = no
        $scope.forms.segment.saving = yes
        $scope.forms.segment.submitted = no

        if $scope.forms.segment.$invalid
          errorTimeout.begin $scope.forms.segment, start
        else
          checkId $scope.segment, (segment) ->
            $scope.segment = segment

            checkLocation.id $scope.status.segments, $scope.segment, (list, segment) ->
              $scope.status.segments = list
              $scope.segment = segment
              if $scope.selected_segment?.steps?
                $scope.selected_segment.steps = _.sortBy $scope.selected_segment.steps, (step) ->
                  if step.delay.units == 'day'
                    step.delay.value * 24
                  else
                    step.delay.value
              httpRequest.save $scope.status, (err, data) ->
                if err
                  $scope.errorMessage = err
                  errorTimeout.begin $scope.forms.segment, start
                  if cb? then cb err
                else
                  $scope.status.version = data.version
                  $scope.errorMessage = undefined
                  $scope.forms.segment.$setPristine()
                  $scope.forms.segment.saved = yes
                  angular.copy $scope.status, status_ref
                  angular.copy $scope.segment.filters, ref
                  saveTimeout.begin start, $scope.forms.segment
                  if cb? then cb null, $scope.status

      $scope.selectall = ({id, values}) ->
        $scope.segment.filters[id] = _.uniq $scope.segment.filters[id].concat _.pluck values, 'id'
      $scope.deselectall = ({id, values}) ->
        $scope.segment.filters[id] = []

      $scope.close = ->
        if $scope.forms.segment.saved and $scope.forms.segment.$pristine
          $uibModalInstance.close $scope.status
        else if $scope.forms.segment.$invalid or $scope.forms.segment.$dirty or not _.isEqual($scope.segment.filters, ref)
          dismissCheck.dismiss $uibModalInstance, item: $scope.status, ref: status_ref, save: $scope.save
        else
          $uibModalInstance.dismiss()
    ]

  .controller 'StepModal',
    ['$scope', '$http', '$q', '$timeout', '$uibModalInstance', 'status', 'segment',
    'step', 'dismissCheck','httpRequest', 'saveTimeout', 'errorTimeout', 'checkLocation',
    ($scope, $http, $q, $timeout, $uibModalInstance, status, segment, step,
    dismissCheck, httpRequest, saveTimeout, errorTimeout, checkLocation) ->
      $scope.$loading = yes
      $scope.status = status
      $scope.segment = segment
      status_ref = {}
      $scope.forms = {}
      $scope.formErrors = {}
      angular.copy $scope.status, status_ref
      $scope['editor.htmlblock'] = extraPlugins: 'strinsert', height: '200px'

      $scope.templateChange = no

      # TODO there are some very similar parts to the batch message controller
      $scope.step = step
      $scope.step.delay = {value: "0", units: "hour"} unless _.isPlainObject $scope.step.delay

      $scope.step.message ?= {}
      _.defaults $scope.step.message, html: {}, text: {}
      _.defaults $scope.step.message.html, blocks: {}, links: {}, policies: {}
      _.defaults $scope.step.message.text, blocks: {}, links: {}, policies: {}

      $scope.nav = html: {}, text: {}

      do (start = Date.now()) ->
        $q.all ['api/pages', 'api/policies', 'api/templates'].map (url) -> $http.get url
        .then (results) ->
          [pages, policies, templates] = (x.data for x in results)
          $scope.pages = _.zipObject _.pluck(pages, 'id'), _.pluck(pages, 'name')
          $scope.policies = _.zipObject _.pluck(policies, 'id'), _.pluck(policies, 'name')
          $scope.templates = templates

          _setTemplateDefaults() if $scope.template = _.find $scope.templates, id: $scope.step.message.template

          $timeout ->
            $scope.$loading = no
          , Math.max 0, 750 - (Date.now() - start)

      checkId = (step, cb) ->
        if !step.id
          step.id = uuid.v4()
          # step.message.id = uuid.v4()
        cb step

      $scope.save = (cb) ->
        start = Date.now()
        $scope.errorMessage = undefined
        $scope.forms.step.saved = no
        $scope.forms.step.saving = yes

        if $scope.forms.step.$invalid
          errorTimeout.begin $scope.forms.step, start
        else
          checkId $scope.step, (step) ->
            $scope.step = step

            checkLocation.id $scope.segment.steps, $scope.step, (list, step) ->
              $scope.segment.steps = list
              savestep = ->
                # save step
                if $scope.selected_segment?.steps?
                  $scope.selected_segment.steps = _.sortBy $scope.selected_segment.steps, (step) ->
                    if step.delay.units == 'day'
                      step.delay.value * 24
                    else
                      step.delay.value
                httpRequest.save $scope.status, (err, data) ->
                  if err
                    $scope.errorMessage = err
                    errorTimeout.begin $scope.forms.step, start
                  else
                    $scope.status.version = data.version
                    angular.copy $scope.status, status_ref
                  if cb? then cb null, $scope.status
                  saveTimeout.begin start, $scope.forms.step

              promise = if $scope.step.message.id?
                $http.put 'api/messages/' + $scope.step.message.id, $scope.step.message
              else
                $http.post 'api/messages', $scope.step.message

              promise
                .success (data) ->
                  _.assign $scope.step.message, data

                  $scope.errorMessage = undefined
                  $scope.forms.step.$setPristine()
                  $scope.forms.step.saved = yes
                  $scope.$broadcast 'cleanup'
                  savestep()
                .error (err) ->
                  $scope.errorMessage = err

      $scope.select = (template) ->
        $scope.templateChange = yes
        $scope.template = template

        $scope.step.message.template = template.id
        _setTemplateDefaults()

      _setTemplateDefaults = ->
        _.forEach ['html', 'text'], (type) ->
          for piece in pieces = $scope.template.meta[type]['links'] or []
            $scope.step.message[type]['links'][piece.name] ?= type: ''
        $scope.nav.html.selectedBlock = $scope.template.meta.html.blocks[0]?.name

      $scope.close = ->
        if $scope.forms.step.saved and $scope.forms.step.$pristine
          $uibModalInstance.close $scope.status
        else if $scope.forms.step.$invalid and $scope.forms.step.$dirty or $scope.templateChange
          dismissCheck.dismiss $uibModalInstance, item: $scope.status, ref: status_ref, save: $scope.save
        else
          $uibModalInstance.dismiss()
    ]

  .controller 'CloneStepModal',
    ['$scope', '$uibModalInstance', '$timeout', 'order', 'statuses', 'cloned',
    'httpRequest', 'saveTimeout', 'errorTimeout',
    ($scope, $uibModalInstance, $timeout, order, statuses, cloned,
    httpRequest, saveTimeout, errorTimeout) ->
      $scope.order = order
      $scope.statuses = statuses
      $scope.forms = {}
      $scope.selected = status: null, segment: null

      $scope.clone = ->
        start = Date.now()
        $scope.errorMessage = undefined
        $scope.formErrors = undefined
        $scope.forms.clone.submitted = no
        $scope.forms.clone.saved = no
        $scope.forms.clone.saving = yes

        if $scope.forms.clone.$invalid
          $scope.formErrors = 'Please fill out all variables'
          $scope.forms.clone.submitted = yes
          errorTimeout.begin $scope.forms.clone, start
        else
          cloned.id = uuid.v4()
          cloned.name = cloned.name
          $scope.status = $scope.selected.status
          segment = $scope.selected.segment
          segLocation = _.findIndex $scope.status.segments, id: segment.id
          if segLocation is -1
            $scope.formErrors = 'Error placing clone'
            $scope.forms.clone.submitted = yes
            errorTimeout.begin $scope.forms.clone, start
          else
            $scope.status.segments[segLocation].steps.push cloned
            if $scope.selected_segment?.steps?
              $scope.selected_segment.steps = _.sortBy $scope.selected_segment.steps, (step) ->
                if step.delay.units == 'day'
                  step.delay.value * 24
                else
                  step.delay.value
            httpRequest.save $scope.status, (err, data) ->
              if err
                $scope.errorMessage = err
                errorTimeout.begin $scope.forms.clone, start
              else
                $scope.status.version = data.version
                $scope.forms.clone.saved = yes
                saveTimeout.begin start, $scope.forms.clone

      $scope.close = ->
        $uibModalInstance.close $scope.status
    ]

  .controller 'SampleModal',
    ['$scope', '$http', '$uibModalInstance', '$timeout', 'status', 'segment',
    'step', 'template', 'errorTimeout', 'saveTimeout', 'growl',
    ($scope, $http, $uibModalInstance, $timeout, status, segment, step,
    template, errorTimeout, saveTimeout, growl) ->
      _setter = (obj, prop, val) ->
        props = prop.split '.'
        final = props.pop()

        while p = props.shift()
          obj[p] = {} if typeof obj[p] is 'undefined'
          obj = obj[p]

        if val then obj[final] = val else obj[final]

      $scope.sample = {}
      $scope.custom = {}
      $http.get('api/senders?q=active').success (senders) ->
        $scope.senders = _.pluck senders, 'email'

      $http.get("api/templates/#{template}").success (template) ->
        $scope.template = template

        combo = [].concat(template.meta.html.variables, template.meta.text.variables)
        $scope.variables = _.uniq _.remove _.pluck(combo, 'name'), (v) ->
          !~v.indexOf('assets.') and !~v.indexOf('sender.') and !~v.indexOf('agent.') and !~v.indexOf('lead.attributes.') and v isnt 'unsub'

        $http.get('api/attributes?q=active').success (attributes) ->
          $scope.attributes = attributes
          _.forEach $scope.attributes, (attribute) ->
            attribute._name = 'lead.attributes.' + attribute.name.replace(/\s/g, '').toLowerCase()
            $scope.attribute_variables = _.uniq _.remove _.pluck(combo, 'name'), (a) ->
              !!~a.indexOf('lead.attributes.')
            $scope.filtered_attributes = []
            _.forEach $scope.attributes, (attribute) ->
              _.forEach $scope.attribute_variables, (av) ->
                $scope.filtered_attributes.push attribute if av is attribute._name

      $scope.trigger = ->
        start = Date.now()
        $scope.forms.sample.saved = no
        $scope.forms.sample.saving = yes
        $scope.forms.sample.submitted = no

        if $scope.forms.sample.$invalid
          errorTimeout.begin $scope.forms.sample, start
        else
          _.forEach [$scope.variables, $scope.attribute_variables], (v) ->
            for path in v or []
              _setter $scope.sample, path, $scope.custom[path]

          statusId = null
          if(status?.id?)
            statusId = status.id
          segmentId = null
          if(segment?.id?)
            segmentId = segment.id
          stepId = null
          if(step?.id?)
            stepId = step.id
          post = _.assign $scope.sample,
            type: 'drip'
            status: statusId
            segment: segmentId
            step: stepId
            message: step.message.id
          $http.post '/api/send', post
          .success (body) ->
            $scope.errorMessage = undefined
            $scope.forms.sample.saved = yes
            growl.success 'Sample queued!'
            saveTimeout.begin start, $scope.forms.sample
          .error (err) ->
            $scope.errorMessage = err
            growl.error 'Message Failed'
            errorTimeout.begin $scope.forms.sample, start

      $scope.close = ->
        $uibModalInstance.dismiss()
    ]
