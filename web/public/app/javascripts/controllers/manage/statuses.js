(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('app.manage').config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      return $stateProvider.state('manage-statuses', {
        data: {
          section: 'manage'
        },
        url: '/manage/statuses',
        templateUrl: '/html/manage/statuses.html'
      }).state('manage-statuses.status', {
        url: '/:status'
      }).state('manage-statuses.status.segment', {
        url: '/:segment'
      });
    }
  ]).factory('ContextMenuService', function() {
    return {
      menu: null
    };
  }).directive('contextMenu', [
    '$compile', '$parse', '$templateCache', 'ContextMenuService', function($compile, $parse, $templateCache, contextMenuService) {
      return {
        scope: false,
        link: function(scope, elem, attrs) {
          var el, html;
          html = $templateCache.get(attrs.contextMenu);
          el = $compile(html)(scope);
          elem.on('click', '.context', function(e) {
            e.stopPropagation();
            return scope.menu.contextmenu('show', e);
          });
          $('body').append(el);
          scope.menu = $(elem[0]).contextmenu({
            target: el,
            before: function(e) {
              var ref1;
              if ((ref1 = contextMenuService.menu) != null) {
                ref1.contextmenu('closemenu', e);
              }
              contextMenuService.menu = scope.menu;
              scope.$apply(function() {
                return scope.item = $parse(attrs.contextMenuItem)(scope);
              });
              return true;
            }
          });
          return elem.on('remove', function() {
            return el.remove();
          });
        }
      };
    }
  ]).factory('httpRequest', [
    '$http', function($http) {
      return {
        save: function(status, cb, successMessage, errorMessage) {
          if (successMessage == null) {
            successMessage = null;
          }
          if (errorMessage == null) {
            errorMessage = null;
          }
          if (successMessage) {
            status.successMessage = successMessage;
          }
          if (errorMessage) {
            status.errorMessage = errorMessage;
          }
          return $http.put('/api/statuses/' + status.id, status).success(function(data) {
            return cb(null, data);
          }).error(function(err) {
            return cb(err);
          });
        }
      };
    }
  ]).factory('httpPost', [
    '$http', function($http) {
      return {
        save: function(status, cb) {
          return $http.post('/api/statuses', status).success(function(data) {
            return cb(null, data);
          }).error(function(err) {
            return cb(err);
          });
        }
      };
    }
  ]).filter('orderStatuses', [
    function() {
      return function(input, sortBy) {
        var j, key, len, ordered;
        ordered = [];
        for (j = 0, len = sortBy.length; j < len; j++) {
          key = sortBy[j];
          ordered.push(input[key]);
        }
        return ordered;
      };
    }
  ]).controller('ManageStatuses', [
    '$scope', '$http', '$timeout', '$state', '$uibModal', '$q', 'growl', 'httpRequest', 'MessageLibrarySrv', function($scope, $http, $timeout, $state, $uibModal, $q, growl, httpRequest, MessageLibrarySrv) {
      var _confirm, cleanUpOldSteps, fixSegment, saveStatus, setDelayInHours, setDripName;
      $scope.$search = {};
      $scope.messageService = MessageLibrarySrv;
      $scope.$watch('order', function(neworder, oldorder) {
        if (!(oldorder && !neworder.compare(oldorder))) {
          return;
        }
        return $http.put('/api/settings/statuses', neworder);
      }, true);
      ($scope.load = function(cb) {
        var start, urls;
        $scope.$loading = true;
        start = Date.now();
        urls = ['attributes?q=active', 'settings/statuses', 'statuses?q=active', 'pages', 'messages?q=all'];
        return $q.all(_.map(urls, function(url) {
          return $http.get("/api/" + url);
        })).then(function(arg) {
          var attributes, id, j, len, messages, order, pages, ref1, ref2, ref3, segment, segments, status, statuses;
          attributes = arg[0], order = arg[1], statuses = arg[2], pages = arg[3], messages = arg[4];
          $scope.attributes = attributes.data;
          $scope.availableLandingPages = pages.data;
          $scope.templatesInLibrary = messages.data;
          ref1 = statuses.data;
          for (id in ref1) {
            segments = ref1[id].segments;
            if (segments == null) {
              segments = [];
            }
            for (j = 0, len = segments.length; j < len; j++) {
              segment = segments[j];
              fixSegment(segment);
            }
          }
          $scope.statuses = statuses.data;
          _.forIn($scope.statuses, function(value, key) {
            return _.forEach(value.segments, function(segment) {
              return _.forEach(segment.steps, function(step) {
                var ref2;
                if (step.message != null) {
                  if (step.message.id != null) {
                    step.message = step.message.id;
                  }
                  step.message = _.find($scope.templatesInLibrary, {
                    'id': step.message
                  });
                  if (step.delay.units === "day") {
                    step.name = "Day";
                  } else {
                    step.name = "Hour";
                  }
                  return step.name += " " + step.delay.value + " - " + ((ref2 = step.message) != null ? ref2.name : void 0);
                }
              });
            });
          });
          $scope.order = _.isArray(order.data) && ((ref2 = order.data) != null ? ref2.length : void 0) === _.keys($scope.statuses).length ? order.data : (function() {
            var ref3, results1;
            ref3 = $scope.statuses;
            results1 = [];
            for (id in ref3) {
              status = ref3[id];
              if (status.active) {
                results1.push(id);
              }
            }
            return results1;
          })();
          _.remove($scope.order, function(n) {
            return $scope.statuses[n] === void 0;
          });
          _.forIn($scope.statuses, function(value, key) {
            if (_.indexOf($scope.order, value.id) === -1) {
              return $scope.order.push(value.id);
            }
          });
          ref3 = $state.params, status = ref3.status, segment = ref3.segment;
          if (status != null) {
            $scope.setStatus(_.find($scope.statuses, function(m) {
              return m.name === status;
            }), segment != null);
            if (segment != null) {
              $scope.setSegment(_.find($scope.selected_status.segments, function(s) {
                return s.name === segment;
              }));
            }
          }
          return $timeout(function() {
            return $scope.$loading = false;
          }, Math.max(0, 800 - (Date.now() - start)));
        });
      })(null);
      $scope.$watch('dragging', function(dragging) {
        var ref1;
        if (!($scope.selected_status && $scope.segmentOrder && dragging === false)) {
          return;
        }
        if (!$scope.segmentOrder.compare($scope.selected_status.segments)) {
          if (((ref1 = $scope.selected_segment) != null ? ref1.steps : void 0) != null) {
            $scope.selected_segment.steps = _.sortBy($scope.selected_segment.steps, function(step) {
              if (step.delay.units === 'day') {
                return step.delay.value * 24;
              } else {
                return step.delay.value;
              }
            });
          }
          return httpRequest.save($scope.selected_status, function(err, data) {
            if (data != null) {
              $scope.segmentOrder = _.clone($scope.selected_status.segments);
              return $scope.selected_status.version = data.version;
            }
          }, "Saved!", "Error saving segment order for status " + $scope.selected_status.name + " ");
        }
      });
      $scope.status_stats = function(status) {
        return {
          segments: status.segments.length,
          messages: _.reduce(status.segments, (function(acc, seg) {
            var ref1;
            return acc + ((ref1 = seg.steps) != null ? ref1.length : void 0) || 0;
          }), 0)
        };
      };
      fixSegment = function(segment) {
        var active_attributes, j, k, key, len;
        if (segment == null) {
          segment = {};
        }
        if (segment.steps == null) {
          segment.steps = [];
        }
        if (!_.isPlainObject(segment.filters)) {
          segment.filters = {};
        }
        active_attributes = _.pluck($scope.attributes, 'id');
        for (j = 0, len = active_attributes.length; j < len; j++) {
          k = active_attributes[j];
          if (!(k in segment.filters)) {
            segment.filters[k] = [];
          }
        }
        for (key in segment.filters) {
          if (indexOf.call(active_attributes, key) < 0) {
            delete segment.filters[key];
          }
        }
        return segment;
      };
      $scope.draggable = {
        start: function() {
          return $scope.dragging = true;
        },
        stop: function() {
          return $timeout((function() {
            return $scope.dragging = false;
          }), 10);
        }
      };
      $scope.setStatus = (function(unwatch) {
        return function(status, notransition) {
          if ($scope.dragging || !status) {
            return;
          }
          if (typeof unwatch === "function") {
            unwatch();
          }
          $scope.selected_status = status;
          $scope.segmentOrder = _.clone(status.segments);
          $state.go('manage-statuses.status', !notransition ? {
            status: status.name
          } : void 0);
          return $scope.selected_segment = null;
        };
      })(void 0);
      $scope.setSegment = function(segment) {
        var a, j, len, ref1, ref2, ref3;
        if ($scope.dragging || !segment) {
          return;
        }
        $scope.selected_segment = segment;
        ref1 = $scope.selected_segment.steps;
        for (j = 0, len = ref1.length; j < len; j++) {
          a = ref1[j];
          if (a.id === void 0) {
            a.id = uuid.v4();
          }
          _.forEach((ref2 = a.message) != null ? (ref3 = ref2.html) != null ? ref3.links : void 0 : void 0, function(link) {
            if (link.type === 'page') {
              link.name = _.result(_.find($scope.availableLandingPages, {
                id: link.value
              }), 'name');
              return link.pagetype = _.result(_.find($scope.availableLandingPages, {
                id: link.value
              }), 'pagetype');
            }
          });
        }
        return $state.go('manage-statuses.status.segment', {
          status: $scope.selected_status.name,
          segment: segment.name
        });
      };
      $scope.showContext = function($event) {
        $event.stopPropagation();
        return false;
      };
      $scope.anyVisible = function(what) {
        return _.any(what, function(i) {
          return !i.$hidden;
        });
      };
      $scope.newStatus = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'status-modal.html',
          controller: 'StatusModal',
          resolve: {
            status: function() {
              return {};
            }
          }
        });
        return inst.result.then(function(status) {
          if (!_.isEmpty(status)) {
            $scope.order.push(status.id);
            $scope.statuses[status.id] = status;
            $scope.setStatus(status);
            return $scope.system.statuses += 1;
          }
        });
      };
      $scope.editStatus = function(status) {
        var inst, ref;
        ref = status;
        inst = $uibModal.open({
          templateUrl: 'status-modal.html',
          controller: 'StatusModal',
          resolve: {
            status: function() {
              return angular.copy(status);
            }
          }
        });
        return inst.result.then(function(status) {
          return angular.copy(status, ref);
        });
      };
      $scope.deleteStatus = function(status) {
        var inst;
        inst = _confirm('status');
        return inst.result.then(function() {
          status.active = false;
          return httpRequest.save(status, function(err, data) {
            var ref1, ref2;
            if (data != null) {
              $scope.order = _.without($scope.order, status.id);
              if (((ref1 = $scope.selected_status) != null ? ref1.id : void 0) === status.id) {
                $scope.selected_status = null;
              }
              if (((ref2 = $scope.status) != null ? ref2.id : void 0) === status.id) {
                $scope.status = null;
              }
              return $scope.system.statuses -= 1;
            }
          }, 'Deleted!', "Error deleting status " + status.name + " ");
        });
      };
      $scope.newSegment = function() {
        var inst;
        inst = $uibModal.open({
          templateUrl: 'segment-modal.html',
          controller: 'SegmentModal',
          resolve: {
            segment: function() {
              return fixSegment();
            },
            attributes: function() {
              return $scope.attributes;
            },
            status: function() {
              return $scope.selected_status;
            }
          }
        });
        return inst.result.then(function(status) {
          return $scope.selected_status = status;
        });
      };
      $scope.editSegment = function(segment) {
        var inst, ref;
        ref = segment;
        inst = $uibModal.open({
          templateUrl: 'segment-modal.html',
          controller: 'SegmentModal',
          resolve: {
            segment: function() {
              return angular.copy(segment);
            },
            attributes: function() {
              return $scope.attributes;
            },
            status: function() {
              return $scope.selected_status;
            }
          }
        });
        return inst.result.then(function(status) {
          return $scope.selected_status = status;
        });
      };
      $scope.deleteSegment = function(segment) {
        var inst;
        inst = _confirm('segment');
        return inst.result.then(function() {
          _.remove($scope.selected_status.segments, {
            id: segment.id
          });
          return httpRequest.save($scope.selected_status, function(err, data) {
            var ref1;
            if (data != null) {
              $scope.selected_status.version = data.version;
              if (((ref1 = $scope.selected_segment) != null ? ref1.id : void 0) === segment.id) {
                return $scope.selected_segment = null;
              }
            }
          }, 'Deleted!', "Error deleting segment " + segment.name + " in status " + $scope.selected_status.name + " ");
        });
      };
      $scope.editMessage = function(drip) {
        MessageLibrarySrv.setStatusSegmentBreadcrumb($scope.selected_status.name, $scope.selected_segment.name);
        return $state.go('manage-messagelibrary.message.version.state', {
          message: drip.message.id,
          version: 'default',
          state: 'templates'
        });
      };
      $scope.editDrip = function(drip) {
        var cloned, inst;
        cloned = _.cloneDeep(drip);
        cloned.id = uuid.v4();
        inst = $uibModal.open({
          templateUrl: 'drip-edit.html',
          controller: 'DripEditorModalCtrl',
          resolve: {
            drip: function() {
              return cloned;
            },
            segment: function() {
              return $scope.selected_segment;
            },
            new_step: function() {
              return false;
            }
          }
        });
        return inst.result.then(function(incomingDrip) {
          var dripToAdd;
          drip.active = false;
          dripToAdd = _.cloneDeep(incomingDrip);
          dripToAdd.vuNotification = dripToAdd.notify;
          setDripName(dripToAdd);
          $scope.selected_segment.steps.push(dripToAdd);
          cleanUpOldSteps();
          return saveStatus('Drip Step Saved', "Error saving status " + $scope.selected_status.name + " ");
        });
      };
      $scope.newDrip = function() {
        var inst, newDrip;
        newDrip = {};
        newDrip.id = uuid.v4();
        newDrip.delay = {};
        newDrip.delay.units = 'day';
        newDrip.delay.value = '0';
        newDrip.notify = false;
        newDrip.vuNotification = false;
        newDrip.message = {};
        inst = $uibModal.open({
          templateUrl: 'drip-edit.html',
          controller: 'DripEditorModalCtrl',
          resolve: {
            drip: function() {
              return newDrip;
            },
            segment: function() {
              return $scope.selected_segment;
            },
            new_step: function() {
              return true;
            }
          }
        });
        return inst.result.then(function(incomingDrip) {
          var dripToAdd;
          dripToAdd = _.cloneDeep(incomingDrip);
          dripToAdd.vuNotification = dripToAdd.notify;
          setDripName(dripToAdd);
          $scope.selected_segment.steps.push(dripToAdd);
          cleanUpOldSteps();
          return saveStatus("Drip Step " + dripToAdd.name + " Saved", "Error creating " + dripToAdd.name + " ");
        });
      };
      $scope.deleteStep = function(step) {
        var inst;
        inst = _confirm('step');
        return inst.result.then(function() {
          var msg;
          step.active = false;
          $scope.selected_segment.steps = _.sortBy($scope.selected_segment.steps, function(step) {
            if (step.delay.units === 'day') {
              return step.delay.value * 24;
            } else {
              return step.delay.value;
            }
          });
          msg = ("Error deleting step " + step.name + " in segment ") + ($scope.selected_segment.name + " in status " + $scope.selected_status.name);
          return saveStatus("Deleted " + step.name + ".", msg);
        });
      };
      $scope.cloneStep = function(step) {
        var cloned, msg, segLocation;
        cloned = angular.copy(step);
        cloned.id = uuid.v4();
        cloned.name = step.name + ' (clone)';
        cloned.message = _.cloneDeep(step.message);
        $scope.selected_segment.steps.push(cloned);
        segLocation = _.findIndex($scope.selected_status.segments, {
          id: $scope.selected_segment.id
        });
        if (segLocation === -1) {
          $scope.selected_status.segments.push($scope.selected_segment);
        } else {
          $scope.selected_status.segments[segLocation] = $scope.selected_segment;
        }
        msg = ("Error cloning step " + cloned.name + " in segment ") + ($scope.selected_segment + " in status " + $scope.selected_status.name + " ");
        return saveStatus("Cloned " + cloned.name, msg);
      };
      $scope.cloneStepTo = function(step) {
        var cloned, inst;
        cloned = angular.copy(step);
        inst = $uibModal.open({
          templateUrl: 'clone-modal.html',
          controller: 'CloneStepModal',
          size: 'sm',
          resolve: {
            order: function() {
              return $scope.order;
            },
            statuses: function() {
              return $scope.statuses;
            },
            cloned: function() {
              return cloned;
            }
          }
        });
        return inst.result.then(function(status) {
          if (status.id === $scope.selected_status.id) {
            $scope.selected_status = status;
          }
          return $scope.statuses[status.id] = status;
        });
      };
      $scope.viewStep = function(step) {
        var inst, template;
        if (step.message.id == null) {
          step.message = $scope.messageService.cloneMessageById(step.message);
        }
        template = step.message.template;
        return inst = $uibModal.open({
          templateUrl: 'html-modal.html',
          size: 'lg',
          scope: _.assign($scope.$new(true), {
            id: template,
            url: "/messages/" + template + "/preview",
            post: {
              id: step.message.id
            }
          })
        });
      };
      $scope.previewStep = function(step) {
        var inst, template;
        if (step.message.id == null) {
          step.message = $scope.messageService.cloneMessageById(step.message);
        }
        template = step.message.template;
        if (template.value != null) {
          template = template.value;
        }
        return inst = $uibModal.open({
          templateUrl: 'sample-modal.html',
          controller: 'SampleModal',
          resolve: {
            template: function() {
              return step.message.template;
            },
            status: function() {
              return $scope.selected_status;
            },
            segment: function() {
              return $scope.selected_segment;
            },
            step: function() {
              return step;
            }
          }
        });
      };
      $scope.openCampaignManager = function() {
        var segment, status;
        status = $scope.selected_status.name;
        segment = $scope.selected_segment.name;
        return $state.go('manage-messagedripmanager.status.segment', {
          status: status,
          segment: segment
        });
      };
      _confirm = function(type) {
        return $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal',
          resolve: {
            type: function() {
              return type;
            },
            scopeItem: function() {
              return null;
            },
            refItem: function() {
              return null;
            }
          }
        });
      };
      $scope.statusesImport = function($event) {
        var inst;
        $event.stopPropagation();
        inst = $uibModal.open({
          templateUrl: 'bulkUpload-modal.html',
          controller: 'StatusesUploadModal'
        });
        return inst.result.then(function() {
          return $http.get('/api/statuses?q=active').error(function(err) {
            return growl.danger('Failed to update statuses');
          }).success(function(result) {
            var key, value;
            for (key in result) {
              value = result[key];
              if (_.indexOf($scope.order, key) === -1) {
                $scope.order.push(key);
                $scope.system.statuses += 1;
              }
            }
            return $scope.statuses = result;
          });
        });
      };
      $scope.delayInHours = function(step) {
        if (step.delay.units === 'day') {
          return step.delay.value * 24;
        } else {
          return step.delay.value;
        }
      };
      setDelayInHours = function(step) {
        var delay;
        if (step.delay.units === 'day') {
          delay = parseInt(step.delay.value, 10) * 24;
          return step.delay.delayInHours = delay;
        } else {
          delay = parseInt(step.delay.value, 10);
          return step.delay.delayInHours = delay;
        }
      };
      setDripName = function(step) {
        var step_delay, step_message_name, step_unit, step_unit_title;
        step_unit = step.delay.units;
        step_unit_title = step_unit.charAt(0).toUpperCase() + step_unit.slice(1);
        step_delay = step.delay.value;
        step_message_name = step.message.name;
        return step.name = step_unit_title + " " + step_delay + " - " + step_message_name;
      };
      cleanUpOldSteps = function() {
        var a, j, message_id, ref1, ref2, ref3, results1;
        results1 = [];
        for (a = j = 0, ref1 = $scope.selected_segment.steps.length; 0 <= ref1 ? j < ref1 : j > ref1; a = 0 <= ref1 ? ++j : --j) {
          if (((ref2 = $scope.selected_segment.steps[a].message) != null ? ref2.id : void 0) == null) {
            message_id = $scope.selected_segment.steps[a].message;
            $scope.selected_segment.steps[a].message = $scope.messageService.cloneMessageById(message_id);
          }
          if ((((ref3 = $scope.selected_segment.steps[a].message) != null ? ref3.version : void 0) != null)) {
            delete $scope.selected_segment.steps[a].message['version'];
          }
          results1.push(setDelayInHours($scope.selected_segment.steps[a]));
        }
        return results1;
      };
      return saveStatus = function(success_message, error_message) {
        var ref1;
        if (((ref1 = $scope.selected_segment) != null ? ref1.steps : void 0) != null) {
          $scope.selected_segment.steps = _.sortBy($scope.selected_segment.steps, function(step) {
            if (step.delay.units === 'day') {
              return step.delay.value * 24;
            } else {
              return step.delay.value;
            }
          });
        }
        return httpRequest.save($scope.selected_status, function(err, data) {
          if (data != null) {
            return $scope.selected_status.version = data.version;
          }
        }, success_message, error_message);
      };
    }
  ]).controller('StatusesUploadModal', [
    '$scope', '$http', '$uibModalInstance', '$uibModal', function($scope, $http, $uibModalInstance, $uibModal) {
      $scope.forms = {
        upload: null
      };
      $scope.$watch('forms.upload.csv', function(csv) {
        if (!csv) {
          return;
        }
        return $scope.typeerror = !~csv.name.indexOf('.csv');
      });
      $scope.url = function() {
        if ($scope.includeStatuses) {
          return "/api/bulk/statuses/download";
        } else {
          return 'bulk/statuses/template/download';
        }
      };
      return $scope.upload = function() {
        var fd;
        if (!~$scope.forms.upload.csv.name.indexOf('.csv')) {
          return $scope.typeerror = true;
        }
        fd = new FormData;
        fd.append('file', $scope.forms.upload.csv);
        return $http.post("api/bulk/statuses/upload", fd, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': void 0
          }
        }).success(function(stats) {
          var inst;
          $uibModalInstance.close();
          return inst = $uibModal.open({
            templateUrl: 'statusesStats-modal.html',
            controller: [
              '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
                $scope.stats = stats;
                return $scope.close = function() {
                  return $uibModalInstance.close();
                };
              }
            ]
          });
        });
      };
    }
  ]).directive('previewIframe', [
    '$http', function($http) {
      return {
        link: function(scope, elm, attr) {
          var iframe;
          iframe = elm[0];
          scope.iframe = iframe.contentWindow || iframe.contentDocument.document || iframe.contentDocument;
          scope.error = false;
          return $http.post(scope.url, scope.post).success(function(data) {
            scope.iframe.document.open();
            scope.iframe.document.write(data);
            return scope.iframe.document.close();
          }).error(function(data) {
            return scope.error = data;
          });
        }
      };
    }
  ]).directive('messagePreviewIframe', [
    '$http', function($http) {
      return function(scope, elm, attrs) {
        var iframe, refreshPreview;
        iframe = elm[0];
        scope.iframe = iframe.contentWindow || iframe.contentDocument.document || iframe.contentDocument;
        refreshPreview = function() {
          var ref1;
          if (!((scope.selectedTemplateId != null) && ((ref1 = scope.messageId) != null ? ref1.length : void 0) && scope.messageId !== 'new')) {
            return;
          }
          return $http.post('/messages/' + scope.selectedTemplateId + '/preview', {
            id: scope.messageId
          }).success(function(data) {
            scope.iframe.document.open();
            scope.iframe.document.write(data);
            return scope.iframe.document.close();
          }).error(function(data) {});
        };
        return scope.$watch(attrs.refreshtrigger, refreshPreview);
      };
    }
  ]).controller('StatusModal', [
    '$scope', '$http', '$uibModalInstance', '$timeout', 'status', 'httpRequest', 'httpPost', 'dismissCheck', 'saveTimeout', 'errorTimeout', function($scope, $http, $uibModalInstance, $timeout, status, httpRequest, httpPost, dismissCheck, saveTimeout, errorTimeout) {
      var checkId, ref, status_ref;
      $scope.status = status || {};
      $scope.forms = {};
      $scope.forms.status = {
        saving: false
      };
      ref = [];
      status_ref = {};
      $scope.aliases = _.map($scope.status.aliases || [], function(alias) {
        return {
          text: alias
        };
      });
      angular.copy($scope.aliases, ref);
      angular.copy($scope.status, status_ref);
      checkId = function(status) {
        if (!status.id) {
          return false;
        } else {
          return true;
        }
      };
      $scope.save = function(cb) {
        var ref1, start, tempStatus;
        start = Date.now();
        $scope.errorMessage = void 0;
        $scope.forms.status.saved = false;
        $scope.forms.status.saving = true;
        $scope.forms.status.submitted = false;
        $scope.status.aliases = _.pluck($scope.aliases, 'text');
        if ($scope.forms.status.$invalid) {
          return errorTimeout.begin($scope.forms.status, start);
        } else {
          checkId($scope.status, function(status) {
            return $scope.status = status;
          });
          if (checkId($scope.status)) {
            if (((ref1 = $scope.selected_segment) != null ? ref1.steps : void 0) != null) {
              $scope.selected_segment.steps = _.sortBy($scope.selected_segment.steps, function(step) {
                if (step.delay.units === 'day') {
                  return step.delay.value * 24;
                } else {
                  return step.delay.value;
                }
              });
            }
            return httpRequest.save($scope.status, function(err, data) {
              if (err) {
                $scope.errorMessage = err;
                errorTimeout.begin($scope.forms.status, start);
                if (cb != null) {
                  return cb(err);
                }
              } else {
                $scope.status.version = data.version;
                $scope.errorMessage = void 0;
                $scope.forms.status.$setPristine();
                $scope.forms.status.saved = true;
                angular.copy($scope.status, status_ref);
                if (cb != null) {
                  cb(null, $scope.status);
                }
                return saveTimeout.begin(start, $scope.forms.status);
              }
            });
          } else {
            tempStatus = {
              name: $scope.status.name,
              active: true,
              aliases: $scope.status.aliases,
              segments: []
            };
            $scope.status = tempStatus;
            return httpPost.save($scope.status, function(err, data) {
              if (err) {
                $scope.errorMessage = err;
                errorTimeout.begin($scope.forms.status, start);
                if (cb != null) {
                  return cb(err);
                }
              } else {
                _.assign($scope.status, data);
                angular.copy($scope.status, status_ref);
                if (cb != null) {
                  cb(null, $scope.status);
                }
                return saveTimeout.begin(start, $scope.forms.status);
              }
            });
          }
        }
      };
      return $scope.close = function() {
        if ($scope.forms.status.saved && $scope.forms.status.$pristine) {
          return $uibModalInstance.close($scope.status);
        } else if ($scope.forms.status.$invalid || $scope.forms.status.$dirty || !_.isEqual($scope.aliases, ref)) {
          return dismissCheck.dismiss($uibModalInstance, {
            item: $scope.status,
            ref: status_ref,
            save: $scope.save
          });
        } else {
          return $uibModalInstance.dismiss();
        }
      };
    }
  ]).controller('SegmentModal', [
    '$scope', '$uibModalInstance', '$timeout', 'segment', 'attributes', 'status', 'httpRequest', 'dismissCheck', 'saveTimeout', 'errorTimeout', 'checkLocation', function($scope, $uibModalInstance, $timeout, segment, attributes, status, httpRequest, dismissCheck, saveTimeout, errorTimeout, checkLocation) {
      var checkId, ref, status_ref;
      $scope.segment = segment;
      $scope.attributes = attributes;
      $scope.status = status;
      $scope.forms = {};
      ref = {};
      status_ref = {};
      angular.copy($scope.segment.filters, ref);
      angular.copy($scope.status, status_ref);
      $scope.$watch('segment.filters', function(newValue, oldValue) {
        if (!newValue) {
          return;
        }
        if (!_.isEqual(newValue, oldValue)) {
          return $scope.forms.segment.$setDirty();
        }
      }, true);
      checkId = function(segment, cb) {
        if (!segment.id) {
          segment.id = uuid.v4();
        }
        return cb(segment);
      };
      $scope.save = function(cb) {
        var start;
        start = Date.now();
        $scope.errorMessage = void 0;
        $scope.forms.segment.saved = false;
        $scope.forms.segment.saving = true;
        $scope.forms.segment.submitted = false;
        if ($scope.forms.segment.$invalid) {
          return errorTimeout.begin($scope.forms.segment, start);
        } else {
          return checkId($scope.segment, function(segment) {
            $scope.segment = segment;
            return checkLocation.id($scope.status.segments, $scope.segment, function(list, segment) {
              var ref1;
              $scope.status.segments = list;
              $scope.segment = segment;
              if (((ref1 = $scope.selected_segment) != null ? ref1.steps : void 0) != null) {
                $scope.selected_segment.steps = _.sortBy($scope.selected_segment.steps, function(step) {
                  if (step.delay.units === 'day') {
                    return step.delay.value * 24;
                  } else {
                    return step.delay.value;
                  }
                });
              }
              return httpRequest.save($scope.status, function(err, data) {
                if (err) {
                  $scope.errorMessage = err;
                  errorTimeout.begin($scope.forms.segment, start);
                  if (cb != null) {
                    return cb(err);
                  }
                } else {
                  $scope.status.version = data.version;
                  $scope.errorMessage = void 0;
                  $scope.forms.segment.$setPristine();
                  $scope.forms.segment.saved = true;
                  angular.copy($scope.status, status_ref);
                  angular.copy($scope.segment.filters, ref);
                  saveTimeout.begin(start, $scope.forms.segment);
                  if (cb != null) {
                    return cb(null, $scope.status);
                  }
                }
              });
            });
          });
        }
      };
      $scope.selectall = function(arg) {
        var id, values;
        id = arg.id, values = arg.values;
        return $scope.segment.filters[id] = _.uniq($scope.segment.filters[id].concat(_.pluck(values, 'id')));
      };
      $scope.deselectall = function(arg) {
        var id, values;
        id = arg.id, values = arg.values;
        return $scope.segment.filters[id] = [];
      };
      return $scope.close = function() {
        if ($scope.forms.segment.saved && $scope.forms.segment.$pristine) {
          return $uibModalInstance.close($scope.status);
        } else if ($scope.forms.segment.$invalid || $scope.forms.segment.$dirty || !_.isEqual($scope.segment.filters, ref)) {
          return dismissCheck.dismiss($uibModalInstance, {
            item: $scope.status,
            ref: status_ref,
            save: $scope.save
          });
        } else {
          return $uibModalInstance.dismiss();
        }
      };
    }
  ]).controller('StepModal', [
    '$scope', '$http', '$q', '$timeout', '$uibModalInstance', 'status', 'segment', 'step', 'dismissCheck', 'httpRequest', 'saveTimeout', 'errorTimeout', 'checkLocation', function($scope, $http, $q, $timeout, $uibModalInstance, status, segment, step, dismissCheck, httpRequest, saveTimeout, errorTimeout, checkLocation) {
      var _setTemplateDefaults, base, checkId, status_ref;
      $scope.$loading = true;
      $scope.status = status;
      $scope.segment = segment;
      status_ref = {};
      $scope.forms = {};
      $scope.formErrors = {};
      angular.copy($scope.status, status_ref);
      $scope['editor.htmlblock'] = {
        extraPlugins: 'strinsert',
        height: '200px'
      };
      $scope.templateChange = false;
      $scope.step = step;
      if (!_.isPlainObject($scope.step.delay)) {
        $scope.step.delay = {
          value: "0",
          units: "hour"
        };
      }
      if ((base = $scope.step).message == null) {
        base.message = {};
      }
      _.defaults($scope.step.message, {
        html: {},
        text: {}
      });
      _.defaults($scope.step.message.html, {
        blocks: {},
        links: {},
        policies: {}
      });
      _.defaults($scope.step.message.text, {
        blocks: {},
        links: {},
        policies: {}
      });
      $scope.nav = {
        html: {},
        text: {}
      };
      (function(start) {
        return $q.all(['api/pages', 'api/policies', 'api/templates'].map(function(url) {
          return $http.get(url);
        })).then(function(results) {
          var pages, policies, ref1, templates, x;
          ref1 = (function() {
            var j, len, results1;
            results1 = [];
            for (j = 0, len = results.length; j < len; j++) {
              x = results[j];
              results1.push(x.data);
            }
            return results1;
          })(), pages = ref1[0], policies = ref1[1], templates = ref1[2];
          $scope.pages = _.zipObject(_.pluck(pages, 'id'), _.pluck(pages, 'name'));
          $scope.policies = _.zipObject(_.pluck(policies, 'id'), _.pluck(policies, 'name'));
          $scope.templates = templates;
          if ($scope.template = _.find($scope.templates, {
            id: $scope.step.message.template
          })) {
            _setTemplateDefaults();
          }
          return $timeout(function() {
            return $scope.$loading = false;
          }, Math.max(0, 750 - (Date.now() - start)));
        });
      })(Date.now());
      checkId = function(step, cb) {
        if (!step.id) {
          step.id = uuid.v4();
        }
        return cb(step);
      };
      $scope.save = function(cb) {
        var start;
        start = Date.now();
        $scope.errorMessage = void 0;
        $scope.forms.step.saved = false;
        $scope.forms.step.saving = true;
        if ($scope.forms.step.$invalid) {
          return errorTimeout.begin($scope.forms.step, start);
        } else {
          return checkId($scope.step, function(step) {
            $scope.step = step;
            return checkLocation.id($scope.segment.steps, $scope.step, function(list, step) {
              var promise, savestep;
              $scope.segment.steps = list;
              savestep = function() {
                var ref1;
                if (((ref1 = $scope.selected_segment) != null ? ref1.steps : void 0) != null) {
                  $scope.selected_segment.steps = _.sortBy($scope.selected_segment.steps, function(step) {
                    if (step.delay.units === 'day') {
                      return step.delay.value * 24;
                    } else {
                      return step.delay.value;
                    }
                  });
                }
                return httpRequest.save($scope.status, function(err, data) {
                  if (err) {
                    $scope.errorMessage = err;
                    errorTimeout.begin($scope.forms.step, start);
                  } else {
                    $scope.status.version = data.version;
                    angular.copy($scope.status, status_ref);
                  }
                  if (cb != null) {
                    cb(null, $scope.status);
                  }
                  return saveTimeout.begin(start, $scope.forms.step);
                });
              };
              promise = $scope.step.message.id != null ? $http.put('api/messages/' + $scope.step.message.id, $scope.step.message) : $http.post('api/messages', $scope.step.message);
              return promise.success(function(data) {
                _.assign($scope.step.message, data);
                $scope.errorMessage = void 0;
                $scope.forms.step.$setPristine();
                $scope.forms.step.saved = true;
                $scope.$broadcast('cleanup');
                return savestep();
              }).error(function(err) {
                return $scope.errorMessage = err;
              });
            });
          });
        }
      };
      $scope.select = function(template) {
        $scope.templateChange = true;
        $scope.template = template;
        $scope.step.message.template = template.id;
        return _setTemplateDefaults();
      };
      _setTemplateDefaults = function() {
        var ref1;
        _.forEach(['html', 'text'], function(type) {
          var base1, j, len, name, piece, pieces, ref1, results1;
          ref1 = pieces = $scope.template.meta[type]['links'] || [];
          results1 = [];
          for (j = 0, len = ref1.length; j < len; j++) {
            piece = ref1[j];
            results1.push((base1 = $scope.step.message[type]['links'])[name = piece.name] != null ? base1[name] : base1[name] = {
              type: ''
            });
          }
          return results1;
        });
        return $scope.nav.html.selectedBlock = (ref1 = $scope.template.meta.html.blocks[0]) != null ? ref1.name : void 0;
      };
      return $scope.close = function() {
        if ($scope.forms.step.saved && $scope.forms.step.$pristine) {
          return $uibModalInstance.close($scope.status);
        } else if ($scope.forms.step.$invalid && $scope.forms.step.$dirty || $scope.templateChange) {
          return dismissCheck.dismiss($uibModalInstance, {
            item: $scope.status,
            ref: status_ref,
            save: $scope.save
          });
        } else {
          return $uibModalInstance.dismiss();
        }
      };
    }
  ]).controller('CloneStepModal', [
    '$scope', '$uibModalInstance', '$timeout', 'order', 'statuses', 'cloned', 'httpRequest', 'saveTimeout', 'errorTimeout', function($scope, $uibModalInstance, $timeout, order, statuses, cloned, httpRequest, saveTimeout, errorTimeout) {
      $scope.order = order;
      $scope.statuses = statuses;
      $scope.forms = {};
      $scope.selected = {
        status: null,
        segment: null
      };
      $scope.clone = function() {
        var ref1, segLocation, segment, start;
        start = Date.now();
        $scope.errorMessage = void 0;
        $scope.formErrors = void 0;
        $scope.forms.clone.submitted = false;
        $scope.forms.clone.saved = false;
        $scope.forms.clone.saving = true;
        if ($scope.forms.clone.$invalid) {
          $scope.formErrors = 'Please fill out all variables';
          $scope.forms.clone.submitted = true;
          return errorTimeout.begin($scope.forms.clone, start);
        } else {
          cloned.id = uuid.v4();
          cloned.name = cloned.name;
          $scope.status = $scope.selected.status;
          segment = $scope.selected.segment;
          segLocation = _.findIndex($scope.status.segments, {
            id: segment.id
          });
          if (segLocation === -1) {
            $scope.formErrors = 'Error placing clone';
            $scope.forms.clone.submitted = true;
            return errorTimeout.begin($scope.forms.clone, start);
          } else {
            $scope.status.segments[segLocation].steps.push(cloned);
            if (((ref1 = $scope.selected_segment) != null ? ref1.steps : void 0) != null) {
              $scope.selected_segment.steps = _.sortBy($scope.selected_segment.steps, function(step) {
                if (step.delay.units === 'day') {
                  return step.delay.value * 24;
                } else {
                  return step.delay.value;
                }
              });
            }
            return httpRequest.save($scope.status, function(err, data) {
              if (err) {
                $scope.errorMessage = err;
                return errorTimeout.begin($scope.forms.clone, start);
              } else {
                $scope.status.version = data.version;
                $scope.forms.clone.saved = true;
                return saveTimeout.begin(start, $scope.forms.clone);
              }
            });
          }
        }
      };
      return $scope.close = function() {
        return $uibModalInstance.close($scope.status);
      };
    }
  ]).controller('SampleModal', [
    '$scope', '$http', '$uibModalInstance', '$timeout', 'status', 'segment', 'step', 'template', 'errorTimeout', 'saveTimeout', 'growl', function($scope, $http, $uibModalInstance, $timeout, status, segment, step, template, errorTimeout, saveTimeout, growl) {
      var _setter;
      _setter = function(obj, prop, val) {
        var final, p, props;
        props = prop.split('.');
        final = props.pop();
        while (p = props.shift()) {
          if (typeof obj[p] === 'undefined') {
            obj[p] = {};
          }
          obj = obj[p];
        }
        if (val) {
          return obj[final] = val;
        } else {
          return obj[final];
        }
      };
      $scope.sample = {};
      $scope.custom = {};
      $http.get('api/senders?q=active').success(function(senders) {
        return $scope.senders = _.pluck(senders, 'email');
      });
      $http.get("api/templates/" + template).success(function(template) {
        var combo;
        $scope.template = template;
        combo = [].concat(template.meta.html.variables, template.meta.text.variables);
        $scope.variables = _.uniq(_.remove(_.pluck(combo, 'name'), function(v) {
          return !~v.indexOf('assets.') && !~v.indexOf('sender.') && !~v.indexOf('agent.') && !~v.indexOf('lead.attributes.') && v !== 'unsub';
        }));
        return $http.get('api/attributes?q=active').success(function(attributes) {
          $scope.attributes = attributes;
          return _.forEach($scope.attributes, function(attribute) {
            attribute._name = 'lead.attributes.' + attribute.name.replace(/\s/g, '').toLowerCase();
            $scope.attribute_variables = _.uniq(_.remove(_.pluck(combo, 'name'), function(a) {
              return !!~a.indexOf('lead.attributes.');
            }));
            $scope.filtered_attributes = [];
            return _.forEach($scope.attributes, function(attribute) {
              return _.forEach($scope.attribute_variables, function(av) {
                if (av === attribute._name) {
                  return $scope.filtered_attributes.push(attribute);
                }
              });
            });
          });
        });
      });
      $scope.trigger = function() {
        var post, segmentId, start, statusId, stepId;
        start = Date.now();
        $scope.forms.sample.saved = false;
        $scope.forms.sample.saving = true;
        $scope.forms.sample.submitted = false;
        if ($scope.forms.sample.$invalid) {
          return errorTimeout.begin($scope.forms.sample, start);
        } else {
          _.forEach([$scope.variables, $scope.attribute_variables], function(v) {
            var j, len, path, ref1, results1;
            ref1 = v || [];
            results1 = [];
            for (j = 0, len = ref1.length; j < len; j++) {
              path = ref1[j];
              results1.push(_setter($scope.sample, path, $scope.custom[path]));
            }
            return results1;
          });
          statusId = null;
          if (((status != null ? status.id : void 0) != null)) {
            statusId = status.id;
          }
          segmentId = null;
          if (((segment != null ? segment.id : void 0) != null)) {
            segmentId = segment.id;
          }
          stepId = null;
          if (((step != null ? step.id : void 0) != null)) {
            stepId = step.id;
          }
          post = _.assign($scope.sample, {
            type: 'drip',
            status: statusId,
            segment: segmentId,
            step: stepId,
            message: step.message.id
          });
          return $http.post('/api/send', post).success(function(body) {
            $scope.errorMessage = void 0;
            $scope.forms.sample.saved = true;
            growl.success('Sample queued!');
            return saveTimeout.begin(start, $scope.forms.sample);
          }).error(function(err) {
            $scope.errorMessage = err;
            growl.error('Message Failed');
            return errorTimeout.begin($scope.forms.sample, start);
          });
        }
      };
      return $scope.close = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]);

}).call(this);
