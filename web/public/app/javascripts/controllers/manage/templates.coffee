angular.module('app.manage')
  .config [
    '$stateProvider', '$urlRouterProvider',
    ($stateProvider, $urlRouterProvider) ->
      $stateProvider
        .state 'manage-templates',
          data: section: 'manage'
          url: '/manage/templates'
          templateUrl: '/html/manage/templates.html'
          controller: 'ManageTemplates'
          resolve:
            templates: ['$http', ($http) ->
              $http.get 'api/templates?q=active'
            ]
        .state 'manage-templates.edit',
          url: '/:id'
        .state 'manage-templates.clone',
          url: '/clone/:id'
  ]

  #TODO move into base.coffee?
  .service 'popupService', ['$window', ($window) ->
    class Popup
      constructor: ->
        @win = $window.open 'about:blank', '_new'
        @win.document.body.innerHTML = '<i>Loading...</i>'

      write: (html) ->
        @win.document.body.innerHTML = ''
        @win.document.write html
        @win.document.close()

    this.create = ->
      return new Popup()

    return this
  ]

  .controller 'ManageTemplates',
    ['$scope', '$http', '$timeout', '$location', '$uibModal', '$state',
    'popupService', 'templates', 'MessageLibrarySrv',
    ($scope, $http, $timeout, $location, $uibModal, $state, popupService,
    templates, MessageLibrarySrv) ->
      start = Date.now()

      $scope.templates = templates.data
      $scope.messageService = MessageLibrarySrv

      $scope.display = 'html'
      $scope.templateError = ''

      $scope.template_can_be_changed = true

      template_in_use_error_string = "This template is being used by message that is in a current campaign. " +
        "You cannot change the naming of the block, polices or blocks."
      template_format_error_string = "We have found a problem with your template and are unable to save it at this time. " +
        "Please correct the error and try again."

      $scope.preview = ->
        popup = popupService.create()
        $http.post('templates/preview', html: $scope.template.html).success (html) ->
          popup.write html

      $scope.whereIsTemplateBeingUsed = (templateId) ->
        $scope.messageService.whereIsTemplateBeingUsed templateId

      $scope.showMessagesWhereTemplateIsUsed = ($event, template) ->
        $event.stopPropagation()
        cloned = _.cloneDeep $scope.messageService.whereIsTemplateBeingUsed template.id
        inst = $uibModal.open
          templateUrl: 'messagesWithTemplateModal.html'
          controller: 'MessageWithTemplateModalCtrl'
          resolve:
            messages: -> cloned
            templateName: -> template.tags[0]

        inst.result.then ->
          $http.delete('api/templates/' + id).success (data) ->
            _.remove $scope.templates, id: id


      $scope.templateChange = ->
        $scope.template_can_be_changed = true
        is_template_in_a_drip = false
        where_template_is_used = $scope.messageService.whereIsTemplateBeingUsed $scope.template.id
        _.forEach where_template_is_used, (value, key) ->
          if is_template_in_a_drip == false
            is_template_in_a_drip = $scope.messageService.campaignMessages()[value.id]?

        if is_template_in_a_drip then check_html_blocks() else $scope.template_can_be_changed = true


      check_html_blocks = ->
        d = new DOMParser()
        p = d.parseFromString($scope.template.html, "text/html")
        html_blocks = []
        $(p).find('*[data-block]').each (index) ->
          found_block_name = $(this).data('block')
          if (_.find html_blocks, (block_name) -> block_name == found_block_name) == undefined
            html_blocks.push found_block_name


        _.forEach html_blocks, (new_block_name) ->
          does_name_exist = _.find $scope.template.meta.html.blocks, (existing_block_name) ->
            existing_block_name.name == new_block_name
          if $scope.template_can_be_changed == true
            if does_name_exist == undefined then $scope.template_can_be_changed = false

        if $scope.template_can_be_changed
          check_html_links(p)
        else
          $scope.templateError = template_in_use_error_string

      check_html_links = (p) ->
        html_links = []
        $(p).find('*[data-link]').each (index) ->
          found_link_name = $(this).data('link')
          if (_.find html_links, (link_name) -> link_name == found_link_name) == undefined
            html_links.push found_link_name

        _.forEach html_links, (new_link_name) ->
          does_name_exist = _.find $scope.template.meta.html.links, (existing_link_name) ->
            existing_link_name.name == new_link_name
          if $scope.template_can_be_changed == true
            if does_name_exist == undefined then $scope.template_can_be_changed = false

        if $scope.template_can_be_changed
          check_html_policies(p)
        else
          scope.templateError = template_in_use_error_string

      check_html_policies = (p) ->
        html_policies = []
        $(p).find('*[data-policy]').each (index) ->
          found_policy_name = $(this).data('policy')
          if (_.find html_policies, (policy_name) -> policy_name == found_policy_name) == undefined
            html_policies.push found_policy_name

        _.forEach html_policies, (new_policy_name) ->
          does_name_exist = _.find $scope.template.meta.html.policies, (existing_policy_name) ->
            existing_policy_name.name == new_policy_name
          if $scope.template_can_be_changed == true
            if does_name_exist == undefined then $scope.template_can_be_changed = false

        if $scope.template_can_be_changed
          check_text_blocks()
        else
          $scope.form.template.$setDirty()
          $scope.templateError = template_in_use_error_string

      check_text_blocks = ->
        voodoo_result_holder = blocks: {}, links: {}, policies: {}, variables: {}
        re = /\{\{\{?>?\s*(?:(?:(block|link|policy)\.([\w]+).*)?|([\w\.]+?))\s*\}\}\}?/g

        while match = re.exec $scope.template.text
          [whole, type, name, fullname] = match
          voodoo_result_holder[switch type
            when 'block' then 'blocks'
            when 'link' then 'links'
            when 'policy' then 'policies'
            else 'variables'
          ][if type then name else fullname] = yes

        _.forIn voodoo_result_holder.blocks, (value, key) ->
          does_block_exist = _.find $scope.template.meta.text.blocks, (existing_block_name) ->
              existing_block_name == existing_block_name
          if $scope.template_can_be_changed == true
            if does_block_exist == undefined
              $scope.template_can_be_changed = false

        if $scope.template_can_be_changed
          check_text_links(voodoo_result_holder)
        else
          $scope.templateError = template_in_use_error_string


      check_text_links = (voodoo_result_holder) ->
        _.forIn voodoo_result_holder.links, (value, key) ->
          does_link_exist = _.find $scope.template.meta.text.links, (existing_link_name) ->
              existing_link_name == existing_link_name
          if $scope.template_can_be_changed == true
            if does_link_exist == undefined
              $scope.template_can_be_changed = false

        if $scope.template_can_be_changed
          check_text_policies(voodoo_result_holder)
        else
          $scope.templateError = template_in_use_error_string

      check_text_policies = (voodoo_result_holder) ->
        _.forIn voodoo_result_holder.policies, (value, key) ->
          does_policy_exist = _.find $scope.template.meta.text.policies, (existing_policy_name) ->
              existing_policy_name == existing_policy_name
          if $scope.template_can_be_changed == true
            if does_policy_exist == undefined
              $scope.template_can_be_changed = false
        if $scope.template_can_be_changed == true
          $scope.templateError = ""
        else
          $scope.templateError = template_in_use_error_string

      $scope.$on '$stateChangeSuccess', ->
        $scope.template = undefined

        {id} = $state.params
        return unless id?

        if id is 'new'
          $scope.template = html: '', text: ''
        else
          $http.get('api/templates/' + id).success (template) ->
            if $state.current.name is 'manage-templates.clone'
              $scope.template = _.omit template, 'id'
            else
              $scope.template = template

      render = (html) ->
        iframe = $('#thumbnail').removeClass('hide')[0]
        iframe = iframe.contentWindow or
          iframe.contentDocument.document or
          iframe.contentDocument

        iframe.document.open()
        iframe.document.write html
        iframe.document.close()

        try
          iw = $('#preview').innerWidth()
          html2canvas iframe.document.body,
            proxy: '/proxy'
            onrendered: (canvas) ->
              resize = document.createElement 'canvas'
              resize.width = iw
              resize.height = iw * (canvas.height / canvas.width)
              $('#preview').html resize

              ctx = resize.getContext '2d'
              ctx.drawImage canvas, 0, 0, resize.width, resize.height

              $scope.template.thumbnail = resize.toDataURL 'image/png'
        catch e
          console.log e

      $scope.$watch 'template.html', _.debounce (now = '', old = null) ->
        render now if now.length > 0 and old
      , 2000, maxWait: 5000

      $scope.delete = ($event, id) ->
        $event.stopPropagation()

        inst = $uibModal.open
          templateUrl: 'confirm-modal.html'
          controller: 'ConfirmModal'

        inst.result.then ->
          $http.delete('api/templates/' + id).success (data) ->
            _.remove $scope.templates, id: id
            $scope.messageService.loadTemplates()

      $scope.save = (template = $scope.template) ->
        $scope.templateErrorExists = false
        if template.id?
          $.put('api/templates/' + template.id, template)
          .done (data) ->
            if data.err
              $scope.templateError = data.err
              $scope.form.template.$setPristine()
              $scope.messageService.loadTemplates()
            else
              $scope.templateError = ''
              template.version = data.version
              $scope.templates[template.id] = template
              $scope.form.template.$setPristine()
            $scope.template.meta = data.meta
          .fail ->
              $scope.templateError = template_format_error_string
        else
          $.post('api/templates', template)
          .done (data) ->
            if data.err?
              $scope.templateError = data.err
              $scope.form.template.$setPristine()
              $scope.messageService.loadTemplates()
            else
              $scope.templates[template.id = data.id] = template
              template.version = data.version
              $scope.form.template.$setPristine()
              $state.go '^.edit', id: template.id
              $http.get('api/templates/' + template.id).success (temp) ->
                $scope.template = temp
            $scope.template.meta = data.meta
          .fail ->
             $scope.templateError = template_format_error_string
    ]

  .controller 'MessageWithTemplateModalCtrl',
    ['$scope', '$state', '$uibModalInstance', 'MessageLibrarySrv', 'messages', 'templateName',
    ($scope, $state, $uibModalInstance, MessageLibrarySrv, messages, templateName) ->
      $scope.service = MessageLibrarySrv
      $scope.messages = messages
      $scope.templateName = templateName

      $scope.dismiss = ->
        $uibModalInstance.dismiss()

      $scope.editMessageDripTemplate = (message) ->
        MessageLibrarySrv.setStatusSegmentBreadcrumb undefined, undefined
        MessageLibrarySrv.setMessageTemplateToBeEdited(message)
        $scope.service.setWizardPosition 0
        $state.go 'manage-messagelibrary.message.state', message: message.id, state: 'templates'
        $uibModalInstance.dismiss()

      false
]
