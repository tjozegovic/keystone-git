(function() {
  angular.module('app.manage').config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      return $stateProvider.state('manage-templates', {
        data: {
          section: 'manage'
        },
        url: '/manage/templates',
        templateUrl: '/html/manage/templates.html',
        controller: 'ManageTemplates',
        resolve: {
          templates: [
            '$http', function($http) {
              return $http.get('api/templates?q=active');
            }
          ]
        }
      }).state('manage-templates.edit', {
        url: '/:id'
      }).state('manage-templates.clone', {
        url: '/clone/:id'
      });
    }
  ]).service('popupService', [
    '$window', function($window) {
      var Popup;
      Popup = (function() {
        function Popup() {
          this.win = $window.open('about:blank', '_new');
          this.win.document.body.innerHTML = '<i>Loading...</i>';
        }

        Popup.prototype.write = function(html) {
          this.win.document.body.innerHTML = '';
          this.win.document.write(html);
          return this.win.document.close();
        };

        return Popup;

      })();
      this.create = function() {
        return new Popup();
      };
      return this;
    }
  ]).controller('ManageTemplates', [
    '$scope', '$http', '$timeout', '$location', '$uibModal', '$state', 'popupService', 'templates', 'MessageLibrarySrv', function($scope, $http, $timeout, $location, $uibModal, $state, popupService, templates, MessageLibrarySrv) {
      var check_html_blocks, check_html_links, check_html_policies, check_text_blocks, check_text_links, check_text_policies, render, start, template_format_error_string, template_in_use_error_string;
      start = Date.now();
      $scope.templates = templates.data;
      $scope.messageService = MessageLibrarySrv;
      $scope.display = 'html';
      $scope.templateError = '';
      $scope.template_can_be_changed = true;
      template_in_use_error_string = "This template is being used by message that is in a current campaign. " + "You cannot change the naming of the block, polices or blocks.";
      template_format_error_string = "We have found a problem with your template and are unable to save it at this time. " + "Please correct the error and try again.";
      $scope.preview = function() {
        var popup;
        popup = popupService.create();
        return $http.post('templates/preview', {
          html: $scope.template.html
        }).success(function(html) {
          return popup.write(html);
        });
      };
      $scope.whereIsTemplateBeingUsed = function(templateId) {
        return $scope.messageService.whereIsTemplateBeingUsed(templateId);
      };
      $scope.showMessagesWhereTemplateIsUsed = function($event, template) {
        var cloned, inst;
        $event.stopPropagation();
        cloned = _.cloneDeep($scope.messageService.whereIsTemplateBeingUsed(template.id));
        inst = $uibModal.open({
          templateUrl: 'messagesWithTemplateModal.html',
          controller: 'MessageWithTemplateModalCtrl',
          resolve: {
            messages: function() {
              return cloned;
            },
            templateName: function() {
              return template.tags[0];
            }
          }
        });
        return inst.result.then(function() {
          return $http["delete"]('api/templates/' + id).success(function(data) {
            return _.remove($scope.templates, {
              id: id
            });
          });
        });
      };
      $scope.templateChange = function() {
        var is_template_in_a_drip, where_template_is_used;
        $scope.template_can_be_changed = true;
        is_template_in_a_drip = false;
        where_template_is_used = $scope.messageService.whereIsTemplateBeingUsed($scope.template.id);
        _.forEach(where_template_is_used, function(value, key) {
          if (is_template_in_a_drip === false) {
            return is_template_in_a_drip = $scope.messageService.campaignMessages()[value.id] != null;
          }
        });
        if (is_template_in_a_drip) {
          return check_html_blocks();
        } else {
          return $scope.template_can_be_changed = true;
        }
      };
      check_html_blocks = function() {
        var d, html_blocks, p;
        d = new DOMParser();
        p = d.parseFromString($scope.template.html, "text/html");
        html_blocks = [];
        $(p).find('*[data-block]').each(function(index) {
          var found_block_name;
          found_block_name = $(this).data('block');
          if ((_.find(html_blocks, function(block_name) {
            return block_name === found_block_name;
          })) === void 0) {
            return html_blocks.push(found_block_name);
          }
        });
        _.forEach(html_blocks, function(new_block_name) {
          var does_name_exist;
          does_name_exist = _.find($scope.template.meta.html.blocks, function(existing_block_name) {
            return existing_block_name.name === new_block_name;
          });
          if ($scope.template_can_be_changed === true) {
            if (does_name_exist === void 0) {
              return $scope.template_can_be_changed = false;
            }
          }
        });
        if ($scope.template_can_be_changed) {
          return check_html_links(p);
        } else {
          return $scope.templateError = template_in_use_error_string;
        }
      };
      check_html_links = function(p) {
        var html_links;
        html_links = [];
        $(p).find('*[data-link]').each(function(index) {
          var found_link_name;
          found_link_name = $(this).data('link');
          if ((_.find(html_links, function(link_name) {
            return link_name === found_link_name;
          })) === void 0) {
            return html_links.push(found_link_name);
          }
        });
        _.forEach(html_links, function(new_link_name) {
          var does_name_exist;
          does_name_exist = _.find($scope.template.meta.html.links, function(existing_link_name) {
            return existing_link_name.name === new_link_name;
          });
          if ($scope.template_can_be_changed === true) {
            if (does_name_exist === void 0) {
              return $scope.template_can_be_changed = false;
            }
          }
        });
        if ($scope.template_can_be_changed) {
          return check_html_policies(p);
        } else {
          return scope.templateError = template_in_use_error_string;
        }
      };
      check_html_policies = function(p) {
        var html_policies;
        html_policies = [];
        $(p).find('*[data-policy]').each(function(index) {
          var found_policy_name;
          found_policy_name = $(this).data('policy');
          if ((_.find(html_policies, function(policy_name) {
            return policy_name === found_policy_name;
          })) === void 0) {
            return html_policies.push(found_policy_name);
          }
        });
        _.forEach(html_policies, function(new_policy_name) {
          var does_name_exist;
          does_name_exist = _.find($scope.template.meta.html.policies, function(existing_policy_name) {
            return existing_policy_name.name === new_policy_name;
          });
          if ($scope.template_can_be_changed === true) {
            if (does_name_exist === void 0) {
              return $scope.template_can_be_changed = false;
            }
          }
        });
        if ($scope.template_can_be_changed) {
          return check_text_blocks();
        } else {
          $scope.form.template.$setDirty();
          return $scope.templateError = template_in_use_error_string;
        }
      };
      check_text_blocks = function() {
        var fullname, match, name, re, type, voodoo_result_holder, whole;
        voodoo_result_holder = {
          blocks: {},
          links: {},
          policies: {},
          variables: {}
        };
        re = /\{\{\{?>?\s*(?:(?:(block|link|policy)\.([\w]+).*)?|([\w\.]+?))\s*\}\}\}?/g;
        while (match = re.exec($scope.template.text)) {
          whole = match[0], type = match[1], name = match[2], fullname = match[3];
          voodoo_result_holder[(function() {
            switch (type) {
              case 'block':
                return 'blocks';
              case 'link':
                return 'links';
              case 'policy':
                return 'policies';
              default:
                return 'variables';
            }
          })()][type ? name : fullname] = true;
        }
        _.forIn(voodoo_result_holder.blocks, function(value, key) {
          var does_block_exist;
          does_block_exist = _.find($scope.template.meta.text.blocks, function(existing_block_name) {
            return existing_block_name === existing_block_name;
          });
          if ($scope.template_can_be_changed === true) {
            if (does_block_exist === void 0) {
              return $scope.template_can_be_changed = false;
            }
          }
        });
        if ($scope.template_can_be_changed) {
          return check_text_links(voodoo_result_holder);
        } else {
          return $scope.templateError = template_in_use_error_string;
        }
      };
      check_text_links = function(voodoo_result_holder) {
        _.forIn(voodoo_result_holder.links, function(value, key) {
          var does_link_exist;
          does_link_exist = _.find($scope.template.meta.text.links, function(existing_link_name) {
            return existing_link_name === existing_link_name;
          });
          if ($scope.template_can_be_changed === true) {
            if (does_link_exist === void 0) {
              return $scope.template_can_be_changed = false;
            }
          }
        });
        if ($scope.template_can_be_changed) {
          return check_text_policies(voodoo_result_holder);
        } else {
          return $scope.templateError = template_in_use_error_string;
        }
      };
      check_text_policies = function(voodoo_result_holder) {
        _.forIn(voodoo_result_holder.policies, function(value, key) {
          var does_policy_exist;
          does_policy_exist = _.find($scope.template.meta.text.policies, function(existing_policy_name) {
            return existing_policy_name === existing_policy_name;
          });
          if ($scope.template_can_be_changed === true) {
            if (does_policy_exist === void 0) {
              return $scope.template_can_be_changed = false;
            }
          }
        });
        if ($scope.template_can_be_changed === true) {
          return $scope.templateError = "";
        } else {
          return $scope.templateError = template_in_use_error_string;
        }
      };
      $scope.$on('$stateChangeSuccess', function() {
        var id;
        $scope.template = void 0;
        id = $state.params.id;
        if (id == null) {
          return;
        }
        if (id === 'new') {
          return $scope.template = {
            html: '',
            text: ''
          };
        } else {
          return $http.get('api/templates/' + id).success(function(template) {
            if ($state.current.name === 'manage-templates.clone') {
              return $scope.template = _.omit(template, 'id');
            } else {
              return $scope.template = template;
            }
          });
        }
      });
      render = function(html) {
        var e, iframe, iw;
        iframe = $('#thumbnail').removeClass('hide')[0];
        iframe = iframe.contentWindow || iframe.contentDocument.document || iframe.contentDocument;
        iframe.document.open();
        iframe.document.write(html);
        iframe.document.close();
        try {
          iw = $('#preview').innerWidth();
          return html2canvas(iframe.document.body, {
            proxy: '/proxy',
            onrendered: function(canvas) {
              var ctx, resize;
              resize = document.createElement('canvas');
              resize.width = iw;
              resize.height = iw * (canvas.height / canvas.width);
              $('#preview').html(resize);
              ctx = resize.getContext('2d');
              ctx.drawImage(canvas, 0, 0, resize.width, resize.height);
              return $scope.template.thumbnail = resize.toDataURL('image/png');
            }
          });
        } catch (_error) {
          e = _error;
          return console.log(e);
        }
      };
      $scope.$watch('template.html', _.debounce(function(now, old) {
        if (now == null) {
          now = '';
        }
        if (old == null) {
          old = null;
        }
        if (now.length > 0 && old) {
          return render(now);
        }
      }, 2000, {
        maxWait: 5000
      }));
      $scope["delete"] = function($event, id) {
        var inst;
        $event.stopPropagation();
        inst = $uibModal.open({
          templateUrl: 'confirm-modal.html',
          controller: 'ConfirmModal'
        });
        return inst.result.then(function() {
          return $http["delete"]('api/templates/' + id).success(function(data) {
            _.remove($scope.templates, {
              id: id
            });
            return $scope.messageService.loadTemplates();
          });
        });
      };
      return $scope.save = function(template) {
        if (template == null) {
          template = $scope.template;
        }
        $scope.templateErrorExists = false;
        if (template.id != null) {
          return $.put('api/templates/' + template.id, template).done(function(data) {
            if (data.err) {
              $scope.templateError = data.err;
              $scope.form.template.$setPristine();
              $scope.messageService.loadTemplates();
            } else {
              $scope.templateError = '';
              template.version = data.version;
              $scope.templates[template.id] = template;
              $scope.form.template.$setPristine();
            }
            return $scope.template.meta = data.meta;
          }).fail(function() {
            return $scope.templateError = template_format_error_string;
          });
        } else {
          return $.post('api/templates', template).done(function(data) {
            if (data.err != null) {
              $scope.templateError = data.err;
              $scope.form.template.$setPristine();
              $scope.messageService.loadTemplates();
            } else {
              $scope.templates[template.id = data.id] = template;
              template.version = data.version;
              $scope.form.template.$setPristine();
              $state.go('^.edit', {
                id: template.id
              });
              $http.get('api/templates/' + template.id).success(function(temp) {
                return $scope.template = temp;
              });
            }
            return $scope.template.meta = data.meta;
          }).fail(function() {
            return $scope.templateError = template_format_error_string;
          });
        }
      };
    }
  ]).controller('MessageWithTemplateModalCtrl', [
    '$scope', '$state', '$uibModalInstance', 'MessageLibrarySrv', 'messages', 'templateName', function($scope, $state, $uibModalInstance, MessageLibrarySrv, messages, templateName) {
      $scope.service = MessageLibrarySrv;
      $scope.messages = messages;
      $scope.templateName = templateName;
      $scope.dismiss = function() {
        return $uibModalInstance.dismiss();
      };
      $scope.editMessageDripTemplate = function(message) {
        MessageLibrarySrv.setStatusSegmentBreadcrumb(void 0, void 0);
        MessageLibrarySrv.setMessageTemplateToBeEdited(message);
        $scope.service.setWizardPosition(0);
        $state.go('manage-messagelibrary.message.state', {
          message: message.id,
          state: 'templates'
        });
        return $uibModalInstance.dismiss();
      };
      return false;
    }
  ]);

}).call(this);
