angular.module('app.manage')
  .directive 'awsFileSelect', [
    '$http', ($http) ->
      scope: uploader: '='
      link: (scope, elm, attrs) ->
        unless scope.uploader.isHTML5
          elm.removeAttr 'multiple'

        elm.bind 'change', ->
          data = if scope.uploader.isHTML5 then @files else [@]
          options = scope.$eval(attrs.awsFileSelect) or {}

          _.forEach data, (file) ->
            $http.get("/api/aws/policy?type=#{encodeURIComponent file.type}").success (policy) ->
              opts = _.extend {}, options
              opts.formData ?= []
              opts.formData.push _.omit policy, 'bucket', 'private'
              opts.formData.push
                acl: 'public-read', 'Content-Type': file.type, success_action_status: '201'
              opts.key = "#{policy.private.folder}/" + file.name
              opts.url = "https://#{policy.private.bucket}.s3.amazonaws.com"
              scope.uploader.addToQueue file, opts

          if scope.uploader.isHTML5 and elm.attr('multiple')
            elm.prop 'value', null

        elm.prop 'value', null
  ]

  .controller 'ManageUploads', [
    '$scope', '$http', '$uibModal', 'FileUploader',
    ($scope, $http, $uibModal, FileUploader) ->
      $scope.uploader = new FileUploader

      $scope.uploader.onBeforeUploadItem = (item) ->
        item.formData.push key: item.key
  ]
