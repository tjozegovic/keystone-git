(function() {
  angular.module('app.manage').directive('awsFileSelect', [
    '$http', function($http) {
      return {
        scope: {
          uploader: '='
        },
        link: function(scope, elm, attrs) {
          if (!scope.uploader.isHTML5) {
            elm.removeAttr('multiple');
          }
          elm.bind('change', function() {
            var data, options;
            data = scope.uploader.isHTML5 ? this.files : [this];
            options = scope.$eval(attrs.awsFileSelect) || {};
            _.forEach(data, function(file) {
              return $http.get("/api/aws/policy?type=" + (encodeURIComponent(file.type))).success(function(policy) {
                var opts;
                opts = _.extend({}, options);
                if (opts.formData == null) {
                  opts.formData = [];
                }
                opts.formData.push(_.omit(policy, 'bucket', 'private'));
                opts.formData.push({
                  acl: 'public-read',
                  'Content-Type': file.type,
                  success_action_status: '201'
                });
                opts.key = (policy["private"].folder + "/") + file.name;
                opts.url = "https://" + policy["private"].bucket + ".s3.amazonaws.com";
                return scope.uploader.addToQueue(file, opts);
              });
            });
            if (scope.uploader.isHTML5 && elm.attr('multiple')) {
              return elm.prop('value', null);
            }
          });
          return elm.prop('value', null);
        }
      };
    }
  ]).controller('ManageUploads', [
    '$scope', '$http', '$uibModal', 'FileUploader', function($scope, $http, $uibModal, FileUploader) {
      $scope.uploader = new FileUploader;
      return $scope.uploader.onBeforeUploadItem = function(item) {
        return item.formData.push({
          key: item.key
        });
      };
    }
  ]);

}).call(this);
