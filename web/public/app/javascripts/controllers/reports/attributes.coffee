angular.module('app.reports')
  .controller 'AttributesSummary', [
    '$scope', '$http', '$controller', '$uibModal',
    ($scope, $http, $controller, $uibModal) ->
      $scope.expanded = []
      $scope.range =
        from: moment().startOf('month').format 'l'
        to: moment().endOf('month').format 'l'

      $http.get('/api/attributes?q=active').success (attributes) ->
        $scope.groups = _(attributes)
          .indexBy 'id'
          .forEach (group) ->
            group.attributes = _.indexBy group.values, 'id'
          .value()

      _rollup = (rows) ->
        rollup = _.reduce rows, (summary, row) ->
          summary[id = row.key.pop()] = _.mapValues row.value, (val, key) -> val + (summary[id]?[key] or 0)
          summary
        , {}

        _(rollup)
          .map (item, id) ->
            key: id, stats: item
          .sortBy (kvp) ->
            kvp.stats.sends
          .reverse()
          .value()

      reload = ->
        $scope.$loading = yes
        qry = root: 'groups', from: $scope.range.from, to: $scope.range.to
        $http.get('/api/emails/summary', params: qry).success (rows) ->
          $scope.summary = _rollup rows
          if $scope.expanded.length
            async.forEachSeries $scope.expanded, (group, cb) ->
              $scope.expand group, cb
            , (err) -> $scope.$loading = no
          else if $scope.summary.length is 1
            $scope.expand $scope.summary[0].key, null
          else
            $scope.$loading = no

      $scope.$watch 'range', _.debounce(reload, 100), yes

      $scope.expand = (group, cb) ->
        get = (cb) ->
          qry = root: 'groups', from: $scope.range.from, to: $scope.range.to, group: group
          $http.get('/api/emails/summary', params: qry).success cb

        done = ->
          return cb() if cb?
          $scope.$loading = no
          $scope.expanded.push group

        if group
          _.find($scope.summary, key: group).attributes = $loading: yes
          get (rows) ->
            _.find($scope.summary, key: group).attributes = _rollup rows
            done()

      $scope.close = (group) ->
        delete _.find($scope.summary, key: group).attributes
        $scope.expanded = _.reject $scope.expanded, (g) -> g is group
  ]
