(function() {
  angular.module('app.reports').controller('AttributesSummary', [
    '$scope', '$http', '$controller', '$uibModal', function($scope, $http, $controller, $uibModal) {
      var _rollup, reload;
      $scope.expanded = [];
      $scope.range = {
        from: moment().startOf('month').format('l'),
        to: moment().endOf('month').format('l')
      };
      $http.get('/api/attributes?q=active').success(function(attributes) {
        return $scope.groups = _(attributes).indexBy('id').forEach(function(group) {
          return group.attributes = _.indexBy(group.values, 'id');
        }).value();
      });
      _rollup = function(rows) {
        var rollup;
        rollup = _.reduce(rows, function(summary, row) {
          var id;
          summary[id = row.key.pop()] = _.mapValues(row.value, function(val, key) {
            var ref;
            return val + (((ref = summary[id]) != null ? ref[key] : void 0) || 0);
          });
          return summary;
        }, {});
        return _(rollup).map(function(item, id) {
          return {
            key: id,
            stats: item
          };
        }).sortBy(function(kvp) {
          return kvp.stats.sends;
        }).reverse().value();
      };
      reload = function() {
        var qry;
        $scope.$loading = true;
        qry = {
          root: 'groups',
          from: $scope.range.from,
          to: $scope.range.to
        };
        return $http.get('/api/emails/summary', {
          params: qry
        }).success(function(rows) {
          $scope.summary = _rollup(rows);
          if ($scope.expanded.length) {
            return async.forEachSeries($scope.expanded, function(group, cb) {
              return $scope.expand(group, cb);
            }, function(err) {
              return $scope.$loading = false;
            });
          } else if ($scope.summary.length === 1) {
            return $scope.expand($scope.summary[0].key, null);
          } else {
            return $scope.$loading = false;
          }
        });
      };
      $scope.$watch('range', _.debounce(reload, 100), true);
      $scope.expand = function(group, cb) {
        var done, get;
        get = function(cb) {
          var qry;
          qry = {
            root: 'groups',
            from: $scope.range.from,
            to: $scope.range.to,
            group: group
          };
          return $http.get('/api/emails/summary', {
            params: qry
          }).success(cb);
        };
        done = function() {
          if (cb != null) {
            return cb();
          }
          $scope.$loading = false;
          return $scope.expanded.push(group);
        };
        if (group) {
          _.find($scope.summary, {
            key: group
          }).attributes = {
            $loading: true
          };
          return get(function(rows) {
            _.find($scope.summary, {
              key: group
            }).attributes = _rollup(rows);
            return done();
          });
        }
      };
      return $scope.close = function(group) {
        delete _.find($scope.summary, {
          key: group
        }).attributes;
        return $scope.expanded = _.reject($scope.expanded, function(g) {
          return g === group;
        });
      };
    }
  ]);

}).call(this);
