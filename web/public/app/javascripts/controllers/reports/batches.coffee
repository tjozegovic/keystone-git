angular.module('app.reports')
  .controller 'ReportsBatches', [
    '$scope', '$http', '$uibModal', '$timeout',
    ($scope, $http, $uibModal, $timeout) ->
      $scope.$watchCollection 'range', reload = ->
        [start, $scope.$loading] = [Date.now(), yes]
        $http.get 'api/batches?q=finished'
        .success (batches) ->
          adjustedTo = $scope.range.to.clone()
          adjustedTo.add(1, 'd')
          $scope.batches = batches
          $http.get 'api/summaries?q=batches',
            params:
              from: $scope.range.from.format 'l'
              to: adjustedTo.format 'l'
          .success (summary) ->
            $scope.summary = _.indexBy summary, 'batch'
            $timeout ->
              $scope.$loading = no
            , Math.max 0, 800 - (Date.now() - start)

      $scope.show = (send) ->
        $scope.send = send
        $uibModal.open
          templateUrl: 'reports/batches/preview.html'
          size: 'lg'
          scope: $scope
  ]
