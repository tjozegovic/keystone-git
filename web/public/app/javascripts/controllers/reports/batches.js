(function() {
  angular.module('app.reports').controller('ReportsBatches', [
    '$scope', '$http', '$uibModal', '$timeout', function($scope, $http, $uibModal, $timeout) {
      var reload;
      $scope.$watchCollection('range', reload = function() {
        var ref, start;
        ref = [Date.now(), true], start = ref[0], $scope.$loading = ref[1];
        return $http.get('api/batches?q=finished').success(function(batches) {
          var adjustedTo;
          adjustedTo = $scope.range.to.clone();
          adjustedTo.add(1, 'd');
          $scope.batches = batches;
          return $http.get('api/summaries?q=batches', {
            params: {
              from: $scope.range.from.format('l'),
              to: adjustedTo.format('l')
            }
          }).success(function(summary) {
            $scope.summary = _.indexBy(summary, 'batch');
            return $timeout(function() {
              return $scope.$loading = false;
            }, Math.max(0, 800 - (Date.now() - start)));
          });
        });
      });
      return $scope.show = function(send) {
        $scope.send = send;
        return $uibModal.open({
          templateUrl: 'reports/batches/preview.html',
          size: 'lg',
          scope: $scope
        });
      };
    }
  ]);

}).call(this);
