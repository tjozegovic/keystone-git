angular.module('app.reports')
  .factory 'MessageClicks', ['$resource', ($resource) ->
    $resource 'api/emails?q=clicks'
  ]
