(function() {
  angular.module('app.reports').factory('MessageClicks', [
    '$resource', function($resource) {
      return $resource('api/emails?q=clicks');
    }
  ]);

}).call(this);
