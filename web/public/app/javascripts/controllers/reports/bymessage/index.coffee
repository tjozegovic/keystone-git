angular.module('app.reports')
  .config ['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
    $urlRouterProvider.when '/reports/bymessage', '/reports/summary'

    $stateProvider
      .state 'bymessage',
        abstract: yes
        data: section: 'reports'
        url: '/reports/bymessage'
        templateUrl: '/html/reports/bymessage/index.html'
        controller: 'ReportsByMessage'

      .state 'bymessage.report',
        url: '/:report'
        templateUrl: (params) -> "/html/reports/bymessage/#{params.report}.html"
  ]

  # placeholder
  .controller 'ReportsByMessage', [
    '$scope', '$http', '$location', 'showLead', ($scope, $http, $location, showLead) ->
      $http.get('/api/statuses?q=active').success (data) ->
        $scope.statuses = data
        _.forEach $scope.statuses, (status) ->
          status.segments = _.indexBy status.segments, 'id'
          _.forEach status.segments, (segment) ->
            segment.steps = _.indexBy segment.steps, 'id'

      $scope.showLead = (id) ->
        showLead.open id

      if batch = $location.search().batch
        $http.get("api/batches/#{batch}").success (data) ->
          $scope.batch = data
  ]
