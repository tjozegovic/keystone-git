(function() {
  angular.module('app.reports').config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/reports/bymessage', '/reports/summary');
      return $stateProvider.state('bymessage', {
        abstract: true,
        data: {
          section: 'reports'
        },
        url: '/reports/bymessage',
        templateUrl: '/html/reports/bymessage/index.html',
        controller: 'ReportsByMessage'
      }).state('bymessage.report', {
        url: '/:report',
        templateUrl: function(params) {
          return "/html/reports/bymessage/" + params.report + ".html";
        }
      });
    }
  ]).controller('ReportsByMessage', [
    '$scope', '$http', '$location', 'showLead', function($scope, $http, $location, showLead) {
      var batch;
      $http.get('/api/statuses?q=active').success(function(data) {
        $scope.statuses = data;
        return _.forEach($scope.statuses, function(status) {
          status.segments = _.indexBy(status.segments, 'id');
          return _.forEach(status.segments, function(segment) {
            return segment.steps = _.indexBy(segment.steps, 'id');
          });
        });
      });
      $scope.showLead = function(id) {
        return showLead.open(id);
      };
      if (batch = $location.search().batch) {
        return $http.get("api/batches/" + batch).success(function(data) {
          return $scope.batch = data;
        });
      }
    }
  ]);

}).call(this);
