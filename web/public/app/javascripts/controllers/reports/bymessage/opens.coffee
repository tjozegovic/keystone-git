angular.module('app.reports')
  .factory 'MessageOpens', ['$resource', ($resource) ->
    $resource 'api/emails?q=opens'
  ]
