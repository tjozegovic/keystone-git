(function() {
  angular.module('app.reports').factory('MessageOpens', [
    '$resource', function($resource) {
      return $resource('api/emails?q=opens');
    }
  ]);

}).call(this);
