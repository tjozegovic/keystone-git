angular.module('app.reports')
  .factory 'MessageOptouts', ['$resource', ($resource) ->
    $resource 'api/emails?q=optouts'
  ]
