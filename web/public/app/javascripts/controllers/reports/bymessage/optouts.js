(function() {
  angular.module('app.reports').factory('MessageOptouts', [
    '$resource', function($resource) {
      return $resource('api/emails?q=optouts');
    }
  ]);

}).call(this);
