(function() {
  angular.module('app.reports').factory('MessageSends', [
    '$resource', function($resource) {
      return $resource('api/emails', {
        sampleless: true
      });
    }
  ]);

}).call(this);
