angular.module('app.reports')
  .directive 'plotGraph', ['$http', '$timeout', ($http, $timeout) ->
    formatters =
      human: (n) -> n.toString().replace /\B(?=(\d{3})+(?!\d))/g, ','
      percentage: (n) -> (n * 100).toFixed(2) + '%'

    link: ($scope, $elm, $attrs) ->
      pre = $elm[0].innerHTML
      $attrs.$observe 'plotGraph', (status) ->
        return unless status

        $elm
          .css height: '260px'
          .html pre

        $http.get "api/trend/#{status}"
          .success (trends) ->
            dt = moment.utc().startOf('day').subtract 30, 'days'
            data = delivers: [], unqopens: [], unqclicks: [], openrate: [], clickrate: []

            # trends is in chrono order, track the date of the head
            head = trends[0]?.day

            end = moment.utc().subtract 1, 'day'
            while end.isAfter dt
              [stamp, trend] = [dt.valueOf(), null]

              if dt.isSame head, 'day'
                trend = trends.shift()
                head = trends[0]?.day

              for key, values of data
                values.push [stamp, trend?[key] or 0]

              dt.add 1, 'day'

            addLine = (data, label, lines, axis = 1) ->
              lines = switch lines
                when 'volume' then lines: fill: yes
                when 'rate' then lines: { show: yes, fill: no }, points: show: yes
              _.assign { data: data, label: '&nbsp;' + label, yaxis: axis }, lines

            $scope.plot = $.plot $elm, [
              addLine data.delivers, 'Sends', 'volume'
              addLine data.unqopens, 'Opens', 'volume'
              addLine data.unqclicks, 'Clicks', 'volume'
              addLine data.openrate, 'Open %', 'rate', 2
              addLine data.clickrate, 'Click %', 'rate', 2
            ],
              legend: position: 'nw'
              xaxes: [ mode: 'time' ]
              yaxes: [
                {
                  min: 0,
                  minTickSize: 10,
                  tickFormatter: formatters.human
                }
                {
                  min: 0,
                  alignTicksWithAxis: 1,
                  position: 'right',
                  autoscaleMargin: .02,
                  tickFormatter: formatters.percentage
                }
              ]
          .error ->
            $elm.html 'No trending data.'
  ]

  .controller 'ReportsDashboard', ['$scope', '$http', ($scope, $http) ->
    $http.get('api/events/email.clicked?limit=12')
      .success (clicks) ->
        $scope.clicks = clicks

    $scope.perf = {}
    $http.get('api/reports/performance?type=best&limit=3')
      .success (best) ->
        $scope.perf.best = best
      .error ->
        $scope.perf.best = []

    $http.get('api/reports/performance?type=worst&limit=3')
      .success (worst) ->
        $scope.perf.worst = worst.reverse()
      .error ->
        $scope.perf.worst = []

    $http.get('api/settings/dashboard')
      .success (settings) ->
        $scope.graphs = _.compact settings

        $scope.$watch 'graphs', (curr, prev) ->
          return unless curr.length isnt prev.length or _.difference(curr, prev).length
          $http.put('api/settings/dashboard', $scope.graphs)
        , yes

    $http.get('api/statuses?q=active')
      .success (statuses) ->
        return unless _.keys(statuses).length

        $scope.statuses = statuses

        _.forEach $scope.statuses, (status) ->
          status.segments = _.indexBy status.segments, 'id'
          _.forEach status.segments, (segment) ->
            segment.steps = _.indexBy segment.steps, 'id'

        $scope.status_array = _.values statuses
        $scope.graphs = [$scope.status_array[0].id] unless _.isArray($scope.graphs) and $scope.graphs.length

    $http.get('api/reports/dashboard')
      .success (summary) ->
        $scope.summary = summary
        $scope.summary.errored = summary['email.errored'] + summary['api.errored']
  ]
