(function() {
  angular.module('app.reports').directive('plotGraph', [
    '$http', '$timeout', function($http, $timeout) {
      var formatters;
      formatters = {
        human: function(n) {
          return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        },
        percentage: function(n) {
          return (n * 100).toFixed(2) + '%';
        }
      };
      return {
        link: function($scope, $elm, $attrs) {
          var pre;
          pre = $elm[0].innerHTML;
          return $attrs.$observe('plotGraph', function(status) {
            if (!status) {
              return;
            }
            $elm.css({
              height: '260px'
            }).html(pre);
            return $http.get("api/trend/" + status).success(function(trends) {
              var addLine, data, dt, end, head, key, ref, ref1, ref2, stamp, trend, values;
              dt = moment.utc().startOf('day').subtract(30, 'days');
              data = {
                delivers: [],
                unqopens: [],
                unqclicks: [],
                openrate: [],
                clickrate: []
              };
              head = (ref = trends[0]) != null ? ref.day : void 0;
              end = moment.utc().subtract(1, 'day');
              while (end.isAfter(dt)) {
                ref1 = [dt.valueOf(), null], stamp = ref1[0], trend = ref1[1];
                if (dt.isSame(head, 'day')) {
                  trend = trends.shift();
                  head = (ref2 = trends[0]) != null ? ref2.day : void 0;
                }
                for (key in data) {
                  values = data[key];
                  values.push([stamp, (trend != null ? trend[key] : void 0) || 0]);
                }
                dt.add(1, 'day');
              }
              addLine = function(data, label, lines, axis) {
                if (axis == null) {
                  axis = 1;
                }
                lines = (function() {
                  switch (lines) {
                    case 'volume':
                      return {
                        lines: {
                          fill: true
                        }
                      };
                    case 'rate':
                      return {
                        lines: {
                          show: true,
                          fill: false
                        },
                        points: {
                          show: true
                        }
                      };
                  }
                })();
                return _.assign({
                  data: data,
                  label: '&nbsp;' + label,
                  yaxis: axis
                }, lines);
              };
              return $scope.plot = $.plot($elm, [addLine(data.delivers, 'Sends', 'volume'), addLine(data.unqopens, 'Opens', 'volume'), addLine(data.unqclicks, 'Clicks', 'volume'), addLine(data.openrate, 'Open %', 'rate', 2), addLine(data.clickrate, 'Click %', 'rate', 2)], {
                legend: {
                  position: 'nw'
                },
                xaxes: [
                  {
                    mode: 'time'
                  }
                ],
                yaxes: [
                  {
                    min: 0,
                    minTickSize: 10,
                    tickFormatter: formatters.human
                  }, {
                    min: 0,
                    alignTicksWithAxis: 1,
                    position: 'right',
                    autoscaleMargin: .02,
                    tickFormatter: formatters.percentage
                  }
                ]
              });
            }).error(function() {
              return $elm.html('No trending data.');
            });
          });
        }
      };
    }
  ]).controller('ReportsDashboard', [
    '$scope', '$http', function($scope, $http) {
      $http.get('api/events/email.clicked?limit=12').success(function(clicks) {
        return $scope.clicks = clicks;
      });
      $scope.perf = {};
      $http.get('api/reports/performance?type=best&limit=3').success(function(best) {
        return $scope.perf.best = best;
      }).error(function() {
        return $scope.perf.best = [];
      });
      $http.get('api/reports/performance?type=worst&limit=3').success(function(worst) {
        return $scope.perf.worst = worst.reverse();
      }).error(function() {
        return $scope.perf.worst = [];
      });
      $http.get('api/settings/dashboard').success(function(settings) {
        $scope.graphs = _.compact(settings);
        return $scope.$watch('graphs', function(curr, prev) {
          if (!(curr.length !== prev.length || _.difference(curr, prev).length)) {
            return;
          }
          return $http.put('api/settings/dashboard', $scope.graphs);
        }, true);
      });
      $http.get('api/statuses?q=active').success(function(statuses) {
        if (!_.keys(statuses).length) {
          return;
        }
        $scope.statuses = statuses;
        _.forEach($scope.statuses, function(status) {
          status.segments = _.indexBy(status.segments, 'id');
          return _.forEach(status.segments, function(segment) {
            return segment.steps = _.indexBy(segment.steps, 'id');
          });
        });
        $scope.status_array = _.values(statuses);
        if (!(_.isArray($scope.graphs) && $scope.graphs.length)) {
          return $scope.graphs = [$scope.status_array[0].id];
        }
      });
      return $http.get('api/reports/dashboard').success(function(summary) {
        $scope.summary = summary;
        return $scope.summary.errored = summary['email.errored'] + summary['api.errored'];
      });
    }
  ]);

}).call(this);
