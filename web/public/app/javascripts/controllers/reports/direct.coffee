angular.module('app.reports')
  .factory 'DirectSends', ['$resource', ($resource) ->
    $resource 'api/emails?type=direct'
  ]

  .controller 'ReportsDirect', [
    '$scope', '$filter', '$http', '$location', '$uibModal', '$q', '$timeout', '$window', 'growl',
    ($scope, $filter, $http, $location, $uibModal, $q, $timeout, $window, growl) ->
      $scope.filter = []
      $scope.paging_filter = []

      $http.get '/api/messages?q=active'
      .success (results) ->
        $scope.messages = _.uniq results, 'name'

        $scope.messagefilter = _.reduce _.sortBy($scope.messages, 'name'), (filter, group) ->
          filter.push group: group.id, name: group.name if group.name?.length
          filter
        , []

      .error (err) ->
        growl.danger 'Failed to get messages ', err

      $scope.selectedValues = (filter) ->
        $scope.filter = _.map $filter('filter')(filter, checked: true), (val) ->
          _.pick val, 'name'
          $scope.paging_filter.push val.name

      $scope.resetFilter = (filter) ->
        _.map filter, (val) -> val.checked = false
        $scope.filter = []
        $scope.paging_filter = []
        $location.search 'messages', _.uniq($scope.paging_filter) or []
        $scope.$emit 'objects:reload'

      $scope.applyFilter = ->
        $location.search 'messages', _.uniq($scope.paging_filter) or []
        $scope.$emit 'objects:reload'
  ]
