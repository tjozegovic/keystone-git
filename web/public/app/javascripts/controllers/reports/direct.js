(function() {
  angular.module('app.reports').factory('DirectSends', [
    '$resource', function($resource) {
      return $resource('api/emails?type=direct');
    }
  ]).controller('ReportsDirect', [
    '$scope', '$filter', '$http', '$location', '$uibModal', '$q', '$timeout', '$window', 'growl', function($scope, $filter, $http, $location, $uibModal, $q, $timeout, $window, growl) {
      $scope.filter = [];
      $scope.paging_filter = [];
      $http.get('/api/messages?q=active').success(function(results) {
        $scope.messages = _.uniq(results, 'name');
        return $scope.messagefilter = _.reduce(_.sortBy($scope.messages, 'name'), function(filter, group) {
          var ref;
          if ((ref = group.name) != null ? ref.length : void 0) {
            filter.push({
              group: group.id,
              name: group.name
            });
          }
          return filter;
        }, []);
      }).error(function(err) {
        return growl.danger('Failed to get messages ', err);
      });
      $scope.selectedValues = function(filter) {
        return $scope.filter = _.map($filter('filter')(filter, {
          checked: true
        }), function(val) {
          _.pick(val, 'name');
          return $scope.paging_filter.push(val.name);
        });
      };
      $scope.resetFilter = function(filter) {
        _.map(filter, function(val) {
          return val.checked = false;
        });
        $scope.filter = [];
        $scope.paging_filter = [];
        $location.search('messages', _.uniq($scope.paging_filter) || []);
        return $scope.$emit('objects:reload');
      };
      return $scope.applyFilter = function() {
        $location.search('messages', _.uniq($scope.paging_filter) || []);
        return $scope.$emit('objects:reload');
      };
    }
  ]);

}).call(this);
