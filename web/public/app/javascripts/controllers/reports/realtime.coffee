angular.module('app.reports')
  .config ['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
    $urlRouterProvider.when '/reports/realtime', '/reports/realtime/sends'

    $stateProvider
      .state 'realtime',
        abstract: yes
        data: section: 'reports'
        url: '/reports/realtime'
        templateUrl: (params) -> "html/reports/realtime.html"
        controller: 'ReportsRealtime'

      .state 'realtime.report',
        url: '/:report'
        templateUrl: (params) -> "html/reports/realtime/#{params.report}.html"
  ]

  .controller 'ReportsRealtime', [
    '$scope', '$state', '$location', '$http', '$rootScope', 'showLead',
    ($scope, $state, $location, $http, $rootScope, showLead) ->
      $scope.state = $state
      $scope.showLead = showLead

      do set_query = -> $scope.query = $.param _.omit $location.search(), 'page'
      $rootScope.$on '$locationChangeSuccess', set_query

      $http.get('api/statuses?q=active').success (data) ->
        $scope.statuses = data
        _.forEach $scope.statuses, (status) ->
          status.segments = _.indexBy status.segments, 'id'
          _.forEach status.segments, (segment) ->
            segment.steps = _.indexBy segment.steps, 'id'
  ]
