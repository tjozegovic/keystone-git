(function() {
  angular.module('app.reports').config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/reports/realtime', '/reports/realtime/sends');
      return $stateProvider.state('realtime', {
        abstract: true,
        data: {
          section: 'reports'
        },
        url: '/reports/realtime',
        templateUrl: function(params) {
          return "html/reports/realtime.html";
        },
        controller: 'ReportsRealtime'
      }).state('realtime.report', {
        url: '/:report',
        templateUrl: function(params) {
          return "html/reports/realtime/" + params.report + ".html";
        }
      });
    }
  ]).controller('ReportsRealtime', [
    '$scope', '$state', '$location', '$http', '$rootScope', 'showLead', function($scope, $state, $location, $http, $rootScope, showLead) {
      var set_query;
      $scope.state = $state;
      $scope.showLead = showLead;
      (set_query = function() {
        return $scope.query = $.param(_.omit($location.search(), 'page'));
      })();
      $rootScope.$on('$locationChangeSuccess', set_query);
      return $http.get('api/statuses?q=active').success(function(data) {
        $scope.statuses = data;
        return _.forEach($scope.statuses, function(status) {
          status.segments = _.indexBy(status.segments, 'id');
          return _.forEach(status.segments, function(segment) {
            return segment.steps = _.indexBy(segment.steps, 'id');
          });
        });
      });
    }
  ]);

}).call(this);
