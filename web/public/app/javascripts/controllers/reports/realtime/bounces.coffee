angular.module('app.reports')
  .factory 'RealtimeBounces', ['$resource', ($resource) ->
    $resource 'api/events/email.bounced'
  ]

  .controller 'ReportBounces', ['$scope', ($scope) ->
    $scope.get_message_name = ({status, segment, step}) ->
      return null unless (st = $scope.statuses[status])? and (sg = st.segments[segment])? and (sp = sg.steps[step])?
      "#{st.name} - #{sg.name} - #{sp.name}"

    $scope.exportRow = (row) ->
      [
        moment(row.at).format 'MMM D, YYYY h:mm a'
        row.email.recipient.email
        if (msg = $scope.get_message_name row)?
          msg
        else if row.batch
          row.batch.name
        else
          row.email.message.name
        row.error or 'n/a'
      ]
  ]
