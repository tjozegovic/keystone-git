(function() {
  angular.module('app.reports').factory('RealtimeBounces', [
    '$resource', function($resource) {
      return $resource('api/events/email.bounced');
    }
  ]).controller('ReportBounces', [
    '$scope', function($scope) {
      $scope.get_message_name = function(arg) {
        var segment, sg, sp, st, status, step;
        status = arg.status, segment = arg.segment, step = arg.step;
        if (!(((st = $scope.statuses[status]) != null) && ((sg = st.segments[segment]) != null) && ((sp = sg.steps[step]) != null))) {
          return null;
        }
        return st.name + " - " + sg.name + " - " + sp.name;
      };
      return $scope.exportRow = function(row) {
        var msg;
        return [moment(row.at).format('MMM D, YYYY h:mm a'), row.email.recipient.email, (msg = $scope.get_message_name(row)) != null ? msg : row.batch ? row.batch.name : row.email.message.name, row.error || 'n/a'];
      };
    }
  ]);

}).call(this);
