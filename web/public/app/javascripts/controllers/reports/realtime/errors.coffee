angular.module('app.reports')
  .factory 'Errors', ['$resource', ($resource) ->
    $resource 'api/events/errors?descending=true'
  ]

  .controller 'ReportsErrors', [
    '$scope', '$http', '$uibModal', 'growl',
    ($scope, $http, $uibModal, growl) ->
      $http.get('api/statuses').success (body) ->
        $scope.statuses = body

      $scope.findStep = (email, cb) ->
        status = _.find $scope.statuses, _id: email.status if email.status
        segment = _.find status.segments, id: email.segment if email.segment and status
        step = _.find segment.steps, id: email.step if email.step and segment
        cb status: status, segment: segment, step: step

      $scope.getErrorMessage = (error) ->
        [
          moment(error.at).format 'MMM D, YYYY h:mm a'
           error.error.message || error.message || error
          error.type
          error.stack
        ]

      $scope.show = (error) ->
        $scope.error = error

        if error.email?
          $http.get('api/emails/' + error.email).success (body) ->
            email = body
            $scope.findStep email, (info) ->
              $scope.error._status = info.status?.name
              $scope.error._segment = info.segment?.name
              $scope.error._step = info.step?.name
              $uibModal.open templateUrl: 'errors/raw.html', size: 'lg', scope: $scope
        else
          $scope.findStep error, (step) ->
            $scope.error._step = step.name
            $uibModal.open templateUrl: 'errors/raw.html', size: 'lg', scope: $scope
  ]
