(function() {
  angular.module('app.reports').factory('Errors', [
    '$resource', function($resource) {
      return $resource('api/events/errors?descending=true');
    }
  ]).controller('ReportsErrors', [
    '$scope', '$http', '$uibModal', 'growl', function($scope, $http, $uibModal, growl) {
      $http.get('api/statuses').success(function(body) {
        return $scope.statuses = body;
      });
      $scope.findStep = function(email, cb) {
        var segment, status, step;
        if (email.status) {
          status = _.find($scope.statuses, {
            _id: email.status
          });
        }
        if (email.segment && status) {
          segment = _.find(status.segments, {
            id: email.segment
          });
        }
        if (email.step && segment) {
          step = _.find(segment.steps, {
            id: email.step
          });
        }
        return cb({
          status: status,
          segment: segment,
          step: step
        });
      };
      $scope.getErrorMessage = function(error) {
        return [moment(error.at).format('MMM D, YYYY h:mm a'), error.error.message || error.message || error, error.type, error.stack];
      };
      return $scope.show = function(error) {
        $scope.error = error;
        if (error.email != null) {
          return $http.get('api/emails/' + error.email).success(function(body) {
            var email;
            email = body;
            return $scope.findStep(email, function(info) {
              var ref, ref1, ref2;
              $scope.error._status = (ref = info.status) != null ? ref.name : void 0;
              $scope.error._segment = (ref1 = info.segment) != null ? ref1.name : void 0;
              $scope.error._step = (ref2 = info.step) != null ? ref2.name : void 0;
              return $uibModal.open({
                templateUrl: 'errors/raw.html',
                size: 'lg',
                scope: $scope
              });
            });
          });
        } else {
          return $scope.findStep(error, function(step) {
            $scope.error._step = step.name;
            return $uibModal.open({
              templateUrl: 'errors/raw.html',
              size: 'lg',
              scope: $scope
            });
          });
        }
      };
    }
  ]);

}).call(this);
