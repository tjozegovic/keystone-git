isValidEmailAddress = (email) ->
  atidx = email.indexOf '@'
  email.length >= 3 && ~atidx && atidx < email.length - 1

angular.module('app.reports')
  .factory 'Leads', ['$resource', ($resource) ->
    $resource 'api/leads'
  ]

  .controller 'ReportsLeads',
    ['$scope', '$http', '$uibModal', '$timeout', 'showAttribute',
    ($scope, $http, $uibModal, $timeout, showAttribute) ->
      # if the search form is cleared out then reset details
      $scope.$watch 'form.leadSearch', (leadSearch) ->
        unless leadSearch
          $scope.details = {}
          $scope.leads = undefined
          $scope.searched = no

      $scope.form = {}
      $scope.loadLeads = no
      $scope.details = {}
      $scope.searched = no
      $scope.$loadingMessages = {}
      $scope.expand = {}
      $scope.showAttribute = showAttribute

      # get messages based on the lead id. pass data back in callback
      $scope.getMessages = (lead, cb) ->
        $http.get('api/search/leadById/' + lead.id)
          .success (data) ->
            $scope.data = []
            if data.length
              $scope.data = data
            else
              $scope.data = notfound: yes

            cb $scope.data
          .error ->
            $scope.details[lead.id] = noMessage: yes

      # function used to make toggles. calls getMessages as needed
      $scope.leadInfo = (lead) ->
        $scope.$loadingMessages = {}
        start = Date.now()
        $scope.$loadingMessages[lead.id] = yes

        if $scope.expand[lead.id] isnt yes
          $scope.expand[lead.id] = yes
          $scope.getMessages lead, (data) ->
            if data.notfound?
              $scope.details[lead.id] = noMessage: yes
            else
              $scope.details[lead.id] = data
              $scope.details[lead.id].updated ?= data.created
            $timeout ->
              $scope.$loadingMessages[lead.id] = no
            , Math.max 0, 800 - (Date.now() - start)

        else
          $scope.expand[lead.id] = no
          $timeout ->
            $scope.$loadingMessages[lead.id] = no
          , Math.max 0, 800 - (Date.now() - start)

      # get leads based on email. pass data back in callback
      $scope.getLeads = (email, cb) ->
        $http.get('api/search/leads/' + email)
          .success (leads) ->
            if leads.length
              cb leads, yes
            else
              cb leads, notfound: yes

          .error ->
            $scope.forms.leadSearch.$submitting = no
            $scope.searched = notfound: yes

      # main search function. calls getLeads and sets data in scope variables
      $scope.search = ->
        $scope.searched = no
        $scope.forms.leadSearch.$submitting = yes
        $scope.loadLeads = yes
        start = Date.now()

        email = $.trim $scope.form.leadSearch
        if email is '' or not isValidEmailAddress email
          $scope.forms.leadSearch.$submitting = no
          $scope.loadLeads = no
          return

        $scope.getLeads email, (leads, details) ->
          $scope.leads = leads
          $scope.expand = []
          $timeout ->
            $scope.searched = details
            $scope.forms.leadSearch.$submitting = no
            $scope.loadLeads = no
          , Math.max 0, 800 - (Date.now() - start)

      # show lead information
      $scope.show = (lead) ->
        $scope.lead = lead
        $uibModal.open
          templateUrl: 'lead-modal.html',
          scope: $scope

      $scope.showMessage = (send) ->
        $scope.send = send
        $uibModal.open
          templateUrl: 'preview-modal.html'
          size: 'lg'
          scope: $scope

        unless send.mail.html
          $http.get('api/emails/' + send.id).success (data) ->
            _.assign send.mail, _.pick data.mail, ['html', 'text']
    ]
