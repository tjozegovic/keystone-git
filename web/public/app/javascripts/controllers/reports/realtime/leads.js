(function() {
  var isValidEmailAddress;

  isValidEmailAddress = function(email) {
    var atidx;
    atidx = email.indexOf('@');
    return email.length >= 3 && ~atidx && atidx < email.length - 1;
  };

  angular.module('app.reports').factory('Leads', [
    '$resource', function($resource) {
      return $resource('api/leads');
    }
  ]).controller('ReportsLeads', [
    '$scope', '$http', '$uibModal', '$timeout', 'showAttribute', function($scope, $http, $uibModal, $timeout, showAttribute) {
      $scope.$watch('form.leadSearch', function(leadSearch) {
        if (!leadSearch) {
          $scope.details = {};
          $scope.leads = void 0;
          return $scope.searched = false;
        }
      });
      $scope.form = {};
      $scope.loadLeads = false;
      $scope.details = {};
      $scope.searched = false;
      $scope.$loadingMessages = {};
      $scope.expand = {};
      $scope.showAttribute = showAttribute;
      $scope.getMessages = function(lead, cb) {
        return $http.get('api/search/leadById/' + lead.id).success(function(data) {
          $scope.data = [];
          if (data.length) {
            $scope.data = data;
          } else {
            $scope.data = {
              notfound: true
            };
          }
          return cb($scope.data);
        }).error(function() {
          return $scope.details[lead.id] = {
            noMessage: true
          };
        });
      };
      $scope.leadInfo = function(lead) {
        var start;
        $scope.$loadingMessages = {};
        start = Date.now();
        $scope.$loadingMessages[lead.id] = true;
        if ($scope.expand[lead.id] !== true) {
          $scope.expand[lead.id] = true;
          return $scope.getMessages(lead, function(data) {
            var base;
            if (data.notfound != null) {
              $scope.details[lead.id] = {
                noMessage: true
              };
            } else {
              $scope.details[lead.id] = data;
              if ((base = $scope.details[lead.id]).updated == null) {
                base.updated = data.created;
              }
            }
            return $timeout(function() {
              return $scope.$loadingMessages[lead.id] = false;
            }, Math.max(0, 800 - (Date.now() - start)));
          });
        } else {
          $scope.expand[lead.id] = false;
          return $timeout(function() {
            return $scope.$loadingMessages[lead.id] = false;
          }, Math.max(0, 800 - (Date.now() - start)));
        }
      };
      $scope.getLeads = function(email, cb) {
        return $http.get('api/search/leads/' + email).success(function(leads) {
          if (leads.length) {
            return cb(leads, true);
          } else {
            return cb(leads, {
              notfound: true
            });
          }
        }).error(function() {
          $scope.forms.leadSearch.$submitting = false;
          return $scope.searched = {
            notfound: true
          };
        });
      };
      $scope.search = function() {
        var email, start;
        $scope.searched = false;
        $scope.forms.leadSearch.$submitting = true;
        $scope.loadLeads = true;
        start = Date.now();
        email = $.trim($scope.form.leadSearch);
        if (email === '' || !isValidEmailAddress(email)) {
          $scope.forms.leadSearch.$submitting = false;
          $scope.loadLeads = false;
          return;
        }
        return $scope.getLeads(email, function(leads, details) {
          $scope.leads = leads;
          $scope.expand = [];
          return $timeout(function() {
            $scope.searched = details;
            $scope.forms.leadSearch.$submitting = false;
            return $scope.loadLeads = false;
          }, Math.max(0, 800 - (Date.now() - start)));
        });
      };
      $scope.show = function(lead) {
        $scope.lead = lead;
        return $uibModal.open({
          templateUrl: 'lead-modal.html',
          scope: $scope
        });
      };
      return $scope.showMessage = function(send) {
        $scope.send = send;
        $uibModal.open({
          templateUrl: 'preview-modal.html',
          size: 'lg',
          scope: $scope
        });
        if (!send.mail.html) {
          return $http.get('api/emails/' + send.id).success(function(data) {
            return _.assign(send.mail, _.pick(data.mail, ['html', 'text']));
          });
        }
      };
    }
  ]);

}).call(this);
