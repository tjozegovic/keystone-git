angular.module('app.reports')
  .factory 'RealtimeOpens', ['$resource', ($resource) ->
    $resource 'api/events/email.opened'
  ]

  .controller 'ReportOpens', ['$scope', '$uibModal', ($scope, $uibModal) ->
    $scope.exportRow = (row) ->
      [
        moment(row.email.queued).format 'MMM D, YYYY h:mm a'
        moment(row.at).format 'MMM D, YYYY h:mm a'
        row.email.sender.email
        row.email.recipient.email
        if row.status and row.segment and row.step
          "#{$scope.statuses[row.status].name} - " +
          "#{$scope.statuses[row.status].segments[row.segment].name} - " +
          "#{$scope.statuses[row.status].segments[row.segment].steps[row.step].name}"
        else if row.batch
          row.batch.name
        else
          row.email.message.name
      ]
  ]
