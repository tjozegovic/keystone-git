(function() {
  angular.module('app.reports').factory('RealtimeOpens', [
    '$resource', function($resource) {
      return $resource('api/events/email.opened');
    }
  ]).controller('ReportOpens', [
    '$scope', '$uibModal', function($scope, $uibModal) {
      return $scope.exportRow = function(row) {
        return [moment(row.email.queued).format('MMM D, YYYY h:mm a'), moment(row.at).format('MMM D, YYYY h:mm a'), row.email.sender.email, row.email.recipient.email, row.status && row.segment && row.step ? ($scope.statuses[row.status].name + " - ") + ($scope.statuses[row.status].segments[row.segment].name + " - ") + ("" + $scope.statuses[row.status].segments[row.segment].steps[row.step].name) : row.batch ? row.batch.name : row.email.message.name];
      };
    }
  ]);

}).call(this);
