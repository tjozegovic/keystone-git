angular.module('app.reports')
  .factory 'RealtimeOptOuts', ['$resource', ($resource) ->
    $resource 'api/events/optout'
  ]
