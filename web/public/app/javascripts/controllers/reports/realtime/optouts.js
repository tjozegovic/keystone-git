(function() {
  angular.module('app.reports').factory('RealtimeOptOuts', [
    '$resource', function($resource) {
      return $resource('api/events/optout');
    }
  ]);

}).call(this);
