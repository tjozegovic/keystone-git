isValidEmailAddress = (email) ->
  atidx = email.indexOf '@'
  email.length >= 3 && ~atidx && atidx < email.length - 1

angular.module('app.reports')
  .factory 'RealtimeSends', ['$resource', ($resource) ->
    $resource 'api/emails/:id', null,
      'count': method: 'HEAD'
  ]

  .controller 'ReportsSends', [
    '$scope', '$http', '$uibModal', '$timeout',
    ($scope, $http, $uibModal, $timeout) ->

      $scope.form = {}
      $scope.loadSends = no
      $scope.form.searchType = 'recipient'

      # if the search form is cleared out then reset details
      $scope.$watch 'form.sendSearch', (sendSearch) ->
        unless sendSearch
          $scope.sends = undefined
          $scope.sendData = undefined

      $scope.$watch 'form.searchType', ->
        return unless $scope.form.searchType and $scope.form.sendSearch
        $scope.search()

      # get messages based on the given email. pass data back in callback
      $scope.getMessages = (email, cb) ->
        if $scope.form.searchType is 'sender'
          query = 'api/search/sends/'
        else
          query = 'api/search/messages/'
        $http.get(query + email)
          .success (data) ->
            $scope.data = []
            if data.length
              $scope.data = data
            else
              details = notfound: yes
            cb $scope.data, details
          .error ->
            $scope.sendData = notfound: yes

      # main search function. calls getMessages and sets data in scope variables
      $scope.search = ->
        $scope.sendData = undefined
        $scope.forms.sendSearch.$submitting = yes
        $scope.loadSends = yes
        start = Date.now()

        email = $.trim $scope.form.sendSearch
        if email is '' or not isValidEmailAddress email
          $scope.forms.sendSearch.$submitting = no
          $scope.loadSends = no
          return

        $scope.getMessages email, (data, details) ->
          $scope.sends = data
          $scope.sendData = yes
          $timeout ->
            $scope.sendData = details
            $scope.forms.sendSearch.$submitting = no
            $scope.loadSends = no
          , Math.max 0, 300 - (Date.now() - start)

      # message preview
      $scope.show = (send) ->
        $scope.send = send
        $uibModal.open
          templateUrl: 'preview-modal.html'
          size: 'lg'
          scope: $scope

        unless send.mail.html
          $http.get('api/emails/' + send.id).success (data) ->
            _.assign send.mail, _.pick data.mail, ['html', 'text']

      $scope.state = (send) ->
        if 'sent' of send then 'Sent' else 'Pending'

      $scope.get_message_name = (send) ->
        return send.message.name if send.sample

        switch send.type
          when 'drip' then "#{send.status.name} - #{send.segment.name} - #{send.step.name}"
          when 'batch' then send.batch.name
          else send.message.name

      $scope.whitelist = [
        'id'
        'type'
        'queued'
        'delivered'
        'sent'
        'bounced'
        'errored'
        'opened'
        'clicked'
        'sample'
        'sender.email'
        'recipient.email'
        'recipient.firstname'
        'recipient.lastname'
        'mail.subject'
        'status.name'
        'segment.name'
        'step.name'
        'batch.name'
        'message.name'
        'lead.id'
      ]

      $scope.getExportedRow = (row) -> [
        moment(row.sent or row.queued).format 'MMM D, YYYY h:mm a'
        row.sender.email
        row.recipient.email
        row.recipient.firstname
        row.recipient.lastname
        row.mail.subject
        $scope.get_message_name row
      ]
  ]
