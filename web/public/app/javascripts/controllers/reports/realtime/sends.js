(function() {
  var isValidEmailAddress;

  isValidEmailAddress = function(email) {
    var atidx;
    atidx = email.indexOf('@');
    return email.length >= 3 && ~atidx && atidx < email.length - 1;
  };

  angular.module('app.reports').factory('RealtimeSends', [
    '$resource', function($resource) {
      return $resource('api/emails/:id', null, {
        'count': {
          method: 'HEAD'
        }
      });
    }
  ]).controller('ReportsSends', [
    '$scope', '$http', '$uibModal', '$timeout', function($scope, $http, $uibModal, $timeout) {
      $scope.form = {};
      $scope.loadSends = false;
      $scope.form.searchType = 'recipient';
      $scope.$watch('form.sendSearch', function(sendSearch) {
        if (!sendSearch) {
          $scope.sends = void 0;
          return $scope.sendData = void 0;
        }
      });
      $scope.$watch('form.searchType', function() {
        if (!($scope.form.searchType && $scope.form.sendSearch)) {
          return;
        }
        return $scope.search();
      });
      $scope.getMessages = function(email, cb) {
        var query;
        if ($scope.form.searchType === 'sender') {
          query = 'api/search/sends/';
        } else {
          query = 'api/search/messages/';
        }
        return $http.get(query + email).success(function(data) {
          var details;
          $scope.data = [];
          if (data.length) {
            $scope.data = data;
          } else {
            details = {
              notfound: true
            };
          }
          return cb($scope.data, details);
        }).error(function() {
          return $scope.sendData = {
            notfound: true
          };
        });
      };
      $scope.search = function() {
        var email, start;
        $scope.sendData = void 0;
        $scope.forms.sendSearch.$submitting = true;
        $scope.loadSends = true;
        start = Date.now();
        email = $.trim($scope.form.sendSearch);
        if (email === '' || !isValidEmailAddress(email)) {
          $scope.forms.sendSearch.$submitting = false;
          $scope.loadSends = false;
          return;
        }
        return $scope.getMessages(email, function(data, details) {
          $scope.sends = data;
          $scope.sendData = true;
          return $timeout(function() {
            $scope.sendData = details;
            $scope.forms.sendSearch.$submitting = false;
            return $scope.loadSends = false;
          }, Math.max(0, 300 - (Date.now() - start)));
        });
      };
      $scope.show = function(send) {
        $scope.send = send;
        $uibModal.open({
          templateUrl: 'preview-modal.html',
          size: 'lg',
          scope: $scope
        });
        if (!send.mail.html) {
          return $http.get('api/emails/' + send.id).success(function(data) {
            return _.assign(send.mail, _.pick(data.mail, ['html', 'text']));
          });
        }
      };
      $scope.state = function(send) {
        if ('sent' in send) {
          return 'Sent';
        } else {
          return 'Pending';
        }
      };
      $scope.get_message_name = function(send) {
        if (send.sample) {
          return send.message.name;
        }
        switch (send.type) {
          case 'drip':
            return send.status.name + " - " + send.segment.name + " - " + send.step.name;
          case 'batch':
            return send.batch.name;
          default:
            return send.message.name;
        }
      };
      $scope.whitelist = ['id', 'type', 'queued', 'delivered', 'sent', 'bounced', 'errored', 'opened', 'clicked', 'sample', 'sender.email', 'recipient.email', 'recipient.firstname', 'recipient.lastname', 'mail.subject', 'status.name', 'segment.name', 'step.name', 'batch.name', 'message.name', 'lead.id'];
      return $scope.getExportedRow = function(row) {
        return [moment(row.sent || row.queued).format('MMM D, YYYY h:mm a'), row.sender.email, row.recipient.email, row.recipient.firstname, row.recipient.lastname, row.mail.subject, $scope.get_message_name(row)];
      };
    }
  ]);

}).call(this);
