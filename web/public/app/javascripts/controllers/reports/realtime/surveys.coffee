angular.module('app.reports')
  .factory 'Survey', ['$resource', ($resource) ->
    $resource 'api/surveys/:id'
  ]

  .controller 'ReportsSurveys', [
    '$scope', '$uibModal',
    ($scope, $uibModal) ->
      $scope.view = (survey) ->
        $uibModal.open
          templateUrl: 'reports/surveys/view.html'
          windowClass: 'modal-dialog-center'
          scope: _.assign $scope.$new(), survey: survey
  ]
