(function() {
  angular.module('app.reports').factory('Survey', [
    '$resource', function($resource) {
      return $resource('api/surveys/:id');
    }
  ]).controller('ReportsSurveys', [
    '$scope', '$uibModal', function($scope, $uibModal) {
      return $scope.view = function(survey) {
        return $uibModal.open({
          templateUrl: 'reports/surveys/view.html',
          windowClass: 'modal-dialog-center',
          scope: _.assign($scope.$new(), {
            survey: survey
          })
        });
      };
    }
  ]);

}).call(this);
