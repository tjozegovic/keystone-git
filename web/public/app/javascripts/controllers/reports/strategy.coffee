angular.module('app.reports')
  .controller 'ReportsStrategy', ['$scope', '$http', '$q', '$uibModal', 'MessageLibrarySrv', ($scope, $http, $q, $uibModal, MessageLibrarySrv) ->
    $scope.steps = []
    $scope.selected = status: null, attributes: {}

    $scope.messageService = MessageLibrarySrv

    $q.all [
      $http.get('/api/attributes?q=active')
      $http.get('/api/settings/statuses')
      $http.get('/api/statuses?q=active')
    ]
    .then ([attributes, order, statuses]) ->
      ids = if order.data.length is statuses.data.length then order.data else
        _.map (_.sortBy statuses.data, 'name'), (status) -> status.id
      $scope.selected.status = ids[0]
      $scope.statuses = statuses.data
      $scope.total_segments = 0
      _.forEach $scope.statuses, (status) ->
        $scope.total_segments += status.segments.length
      $scope.multiplier = if $scope.total_segments < 12 then 12 else $scope.total_segments
      $scope.attributes = attributes.data

      $scope.root = name: 'Strategy'
      unsorted = _.map $scope.statuses, (status, id) ->
        id: id
        name: status.name
        children: _.sortBy (_.map status.segments, (segment) ->
          _.pick segment, ['id', 'name', 'steps', 'products']), (innerChild) ->
            innerChild.name

      sortedWithChildren = _.sortBy (_.filter unsorted, (child) ->
        child.children.length > 0), (child) ->
          child.name

      sortedNoChildren = _.sortBy (_.filter unsorted, (child) ->
        child.children.length is 0), (child) ->
          child.name

      $scope.root.children = sortedWithChildren.concat sortedNoChildren

      $scope.w = 800
      $scope.h = 50 * $scope.multiplier
      $scope.i = 0

      $scope.root.x0 = $scope.h / 2
      $scope.root.y0 = 0

      $scope.tree = d3.layout.tree().size [$scope.h - 100, $scope.w]
      $scope.diagonal = d3.svg.diagonal().projection (d) ->
        if d.depth is 1 and !d.children then [d.y, d.x + 25] else [d.y, d.x]

      $scope.vis = d3.select('#graph').append('svg:svg')
        .attr('width', $scope.w)
        .attr('height', $scope.h)
        .append('svg:g')
        .attr 'transform', (d) -> "translate(75, 25)"

      $scope.highlight()

    $scope.highlight = (node) ->
      return unless $scope.selected.status

      unless node
        path = _.map $scope.selected.attributes, (value, id) -> "#{id}/#{value}" if value?
        status = _.find $scope.root.children, id: $scope.selected.status
        node = if _.isEmpty(_.omit $scope.selected.attributes, _.isEmpty) then status else
          _.find status.children, ({products}) ->
            _.find products, (product) -> _.isEqual product.sort(), _.compact path.sort()

      $scope.selected.segment = node
      if !node
        $scope.$no_match = yes
      else if !$scope.selected.segment?.products
        $scope.selected.segment.$status = yes
        $scope.$no_match = no
      else if $scope.selected.segment?.products?[0].length is 0
        $scope.selected.segment.$no_attributes = yes
        $scope.$no_match = no
      else if $scope.selected.segment?.products?.length is 1
        $scope.selected.segment.$multiple_attributes = no
        $scope.$no_match = no
        $scope.selected.attributes = _.reduce $scope.selected.segment.products[0], (memo, value) ->
            [key, value] = value.split '/'
            memo[key] = value
            memo
          , {}
      else if !$scope.selected.segment.products?
        $scope.selected.segment.$multiple_attributes = no
        $scope.$no_match = no
      else
        $scope.selected.segment.$multiple_attributes = yes
        $scope.$no_match = no

      $scope.steps = (_.filter node?.steps, (step) -> not step.active? or step.active) or []
      update()

    $scope.viewStep = (step) ->
      return unless step.message
      if(step.message.id == undefined)
        step.message = $scope.messageService.cloneMessageById step.message
      template = step.message.template
      inst = $uibModal.open
        templateUrl: 'html-modal.html'
        size: 'lg'
        scope: _.assign $scope.$new(yes),
          id: template
          url: "/messages/#{template}/preview"
          post: id: step.message.id

    update = (source = $scope.root) ->
      # Compute the new tree layout.
      nodes = $scope.tree.nodes($scope.root).reverse()

      # Normalize for fixed-depth.
      nodes.forEach (d) -> d.y = d.depth * 225

      # Update the nodes...
      node = $scope.vis.selectAll('g.node')
        .data nodes, (d) -> d.id || (d.id = ++$scope.i)

      # Enter any new nodes at the parent's previous position.
      nodeEnter = node.enter().append('svg:g')
        .attr('class', 'node')
        .attr('transform', (d) -> "translate(#{source.y0}, #{source.x0})")
        .on 'click', (d) ->
          $scope.$apply ->
            _.forEach $scope.selected.attributes, (value, id) ->
              $scope.selected.attributes[id] = null
            $scope.selected.status = if d.depth < 2 then d.id else d.parent.id
            $scope.highlight d

      nodeEnter.append('svg:circle')
        .attr('r', 1e-6)
        .style 'fill', '#fff'

      nodeEnter.append('svg:text')
        .attr('dy', '.35em')
        .attr('text-anchor', (d) -> if d.children then 'middle' else 'start')
        .text((d) -> d.name)
        .style 'fill-opacity', 1e-6
        .call wrap, 200

      # Transition nodes to their new position.
      nodeUpdate = node.transition()
        .attr 'transform', (d) ->
          if d.depth is 1 and !d.children then "translate(#{d.y}, #{d.x + 25})" else "translate(#{d.y}, #{d.x})"
      nodeUpdate.select('circle')
        .attr('r', 12)
        .style('fill', (d) -> if d.id is $scope.selected.segment?.id then 'lightsteelblue' else '#fff')

      nodeUpdate.select('text').style 'fill-opacity', 1

      # Transition exiting nodes to the parent's new position.
      nodeExit = node.exit().transition()
        .attr('transform', (d) -> "translate(#{source.x}, #{source.y})")
        .remove()

      nodeExit.select('circle').attr 'r', 1e-6
      nodeExit.select('text').style 'fill-opacity', 1e-6

      # Update the links…
      links = $scope.vis.selectAll('path.link')
        .data($scope.tree.links(nodes), (d) -> d.target.id)
        .classed('active', false)

      # Enter any new links at the parent's previous position.
      links.enter().insert('svg:path', 'g')
        .attr('class', 'link')
        .attr('d', (d) ->
          o = x: source.x0, y: source.y0
          $scope.diagonal source: o, target: o
        ).transition().attr 'd', $scope.diagonal

      # Transition links to their new position.
      links.transition().attr 'd', $scope.diagonal

      # Transition exiting nodes to the parent's new position.
      links.exit().transition().attr('d', (d) ->
        o = x: source.x, y: source.y
        $scope.diagonal source: o, target: o
      ).remove()

      # Walk parent chain
      if $scope.selected.segment
        ancestors = []
        parent = $scope.selected.segment
        while not _.isUndefined parent
          ancestors.push parent
          parent = parent.parent

        # Get the matched links
        matchedLinks = []
        links
          .filter (d, i) ->
            _.any ancestors, (p) -> p is d.target
          .classed 'active', true

      # Stash the old positions for transition.
      nodes.forEach (d) ->
        d.x0 = d.x
        d.y0 = d.y

    wrap = (text, width) ->
      text.each ->
        t = d3.select(this)
        words = t.text().split(/\s+/).reverse()
        word = undefined
        line = []
        lineNumber = 0
        lineHeight = 1.1
        y = t.attr('y')
        dy = parseFloat(t.attr('dy'))
        tspan = t.text(null).append('tspan')
          .attr('x', (d) -> if d.children then 0 else 15)
          .attr('y', (d) -> if d.children then 19 else 0)
          .attr('dy', dy + 'em')
        while word = words.pop()
          line.push word
          tspan.text line.join(' ')
          if tspan.node().getComputedTextLength() > width
            line.pop()
            tspan.text line.join(' ')
            line = [ word ]
            tspan = t.append('tspan')
              .attr('x', (d) -> if d.children then 0 else 15)
              .attr('y', (d) -> if d.children then 19 else 0)
              .attr('dy', ++lineNumber * lineHeight + dy + 'em').text(word)
  ]
