(function() {
  angular.module('app.reports').controller('ReportsStrategy', [
    '$scope', '$http', '$q', '$uibModal', 'MessageLibrarySrv', function($scope, $http, $q, $uibModal, MessageLibrarySrv) {
      var update, wrap;
      $scope.steps = [];
      $scope.selected = {
        status: null,
        attributes: {}
      };
      $scope.messageService = MessageLibrarySrv;
      $q.all([$http.get('/api/attributes?q=active'), $http.get('/api/settings/statuses'), $http.get('/api/statuses?q=active')]).then(function(arg) {
        var attributes, ids, order, sortedNoChildren, sortedWithChildren, statuses, unsorted;
        attributes = arg[0], order = arg[1], statuses = arg[2];
        ids = order.data.length === statuses.data.length ? order.data : _.map(_.sortBy(statuses.data, 'name'), function(status) {
          return status.id;
        });
        $scope.selected.status = ids[0];
        $scope.statuses = statuses.data;
        $scope.total_segments = 0;
        _.forEach($scope.statuses, function(status) {
          return $scope.total_segments += status.segments.length;
        });
        $scope.multiplier = $scope.total_segments < 12 ? 12 : $scope.total_segments;
        $scope.attributes = attributes.data;
        $scope.root = {
          name: 'Strategy'
        };
        unsorted = _.map($scope.statuses, function(status, id) {
          return {
            id: id,
            name: status.name,
            children: _.sortBy(_.map(status.segments, function(segment) {
              return _.pick(segment, ['id', 'name', 'steps', 'products']);
            }), function(innerChild) {
              return innerChild.name;
            })
          };
        });
        sortedWithChildren = _.sortBy(_.filter(unsorted, function(child) {
          return child.children.length > 0;
        }), function(child) {
          return child.name;
        });
        sortedNoChildren = _.sortBy(_.filter(unsorted, function(child) {
          return child.children.length === 0;
        }), function(child) {
          return child.name;
        });
        $scope.root.children = sortedWithChildren.concat(sortedNoChildren);
        $scope.w = 800;
        $scope.h = 50 * $scope.multiplier;
        $scope.i = 0;
        $scope.root.x0 = $scope.h / 2;
        $scope.root.y0 = 0;
        $scope.tree = d3.layout.tree().size([$scope.h - 100, $scope.w]);
        $scope.diagonal = d3.svg.diagonal().projection(function(d) {
          if (d.depth === 1 && !d.children) {
            return [d.y, d.x + 25];
          } else {
            return [d.y, d.x];
          }
        });
        $scope.vis = d3.select('#graph').append('svg:svg').attr('width', $scope.w).attr('height', $scope.h).append('svg:g').attr('transform', function(d) {
          return "translate(75, 25)";
        });
        return $scope.highlight();
      });
      $scope.highlight = function(node) {
        var path, ref, ref1, ref2, ref3, ref4, status;
        if (!$scope.selected.status) {
          return;
        }
        if (!node) {
          path = _.map($scope.selected.attributes, function(value, id) {
            if (value != null) {
              return id + "/" + value;
            }
          });
          status = _.find($scope.root.children, {
            id: $scope.selected.status
          });
          node = _.isEmpty(_.omit($scope.selected.attributes, _.isEmpty)) ? status : _.find(status.children, function(arg) {
            var products;
            products = arg.products;
            return _.find(products, function(product) {
              return _.isEqual(product.sort(), _.compact(path.sort()));
            });
          });
        }
        $scope.selected.segment = node;
        if (!node) {
          $scope.$no_match = true;
        } else if (!((ref = $scope.selected.segment) != null ? ref.products : void 0)) {
          $scope.selected.segment.$status = true;
          $scope.$no_match = false;
        } else if (((ref1 = $scope.selected.segment) != null ? (ref2 = ref1.products) != null ? ref2[0].length : void 0 : void 0) === 0) {
          $scope.selected.segment.$no_attributes = true;
          $scope.$no_match = false;
        } else if (((ref3 = $scope.selected.segment) != null ? (ref4 = ref3.products) != null ? ref4.length : void 0 : void 0) === 1) {
          $scope.selected.segment.$multiple_attributes = false;
          $scope.$no_match = false;
          $scope.selected.attributes = _.reduce($scope.selected.segment.products[0], function(memo, value) {
            var key, ref5;
            ref5 = value.split('/'), key = ref5[0], value = ref5[1];
            memo[key] = value;
            return memo;
          }, {});
        } else if ($scope.selected.segment.products == null) {
          $scope.selected.segment.$multiple_attributes = false;
          $scope.$no_match = false;
        } else {
          $scope.selected.segment.$multiple_attributes = true;
          $scope.$no_match = false;
        }
        $scope.steps = (_.filter(node != null ? node.steps : void 0, function(step) {
          return (step.active == null) || step.active;
        })) || [];
        return update();
      };
      $scope.viewStep = function(step) {
        var inst, template;
        if (!step.message) {
          return;
        }
        if (step.message.id === void 0) {
          step.message = $scope.messageService.cloneMessageById(step.message);
        }
        template = step.message.template;
        return inst = $uibModal.open({
          templateUrl: 'html-modal.html',
          size: 'lg',
          scope: _.assign($scope.$new(true), {
            id: template,
            url: "/messages/" + template + "/preview",
            post: {
              id: step.message.id
            }
          })
        });
      };
      update = function(source) {
        var ancestors, links, matchedLinks, node, nodeEnter, nodeExit, nodeUpdate, nodes, parent;
        if (source == null) {
          source = $scope.root;
        }
        nodes = $scope.tree.nodes($scope.root).reverse();
        nodes.forEach(function(d) {
          return d.y = d.depth * 225;
        });
        node = $scope.vis.selectAll('g.node').data(nodes, function(d) {
          return d.id || (d.id = ++$scope.i);
        });
        nodeEnter = node.enter().append('svg:g').attr('class', 'node').attr('transform', function(d) {
          return "translate(" + source.y0 + ", " + source.x0 + ")";
        }).on('click', function(d) {
          return $scope.$apply(function() {
            _.forEach($scope.selected.attributes, function(value, id) {
              return $scope.selected.attributes[id] = null;
            });
            $scope.selected.status = d.depth < 2 ? d.id : d.parent.id;
            return $scope.highlight(d);
          });
        });
        nodeEnter.append('svg:circle').attr('r', 1e-6).style('fill', '#fff');
        nodeEnter.append('svg:text').attr('dy', '.35em').attr('text-anchor', function(d) {
          if (d.children) {
            return 'middle';
          } else {
            return 'start';
          }
        }).text(function(d) {
          return d.name;
        }).style('fill-opacity', 1e-6).call(wrap, 200);
        nodeUpdate = node.transition().attr('transform', function(d) {
          if (d.depth === 1 && !d.children) {
            return "translate(" + d.y + ", " + (d.x + 25) + ")";
          } else {
            return "translate(" + d.y + ", " + d.x + ")";
          }
        });
        nodeUpdate.select('circle').attr('r', 12).style('fill', function(d) {
          var ref;
          if (d.id === ((ref = $scope.selected.segment) != null ? ref.id : void 0)) {
            return 'lightsteelblue';
          } else {
            return '#fff';
          }
        });
        nodeUpdate.select('text').style('fill-opacity', 1);
        nodeExit = node.exit().transition().attr('transform', function(d) {
          return "translate(" + source.x + ", " + source.y + ")";
        }).remove();
        nodeExit.select('circle').attr('r', 1e-6);
        nodeExit.select('text').style('fill-opacity', 1e-6);
        links = $scope.vis.selectAll('path.link').data($scope.tree.links(nodes), function(d) {
          return d.target.id;
        }).classed('active', false);
        links.enter().insert('svg:path', 'g').attr('class', 'link').attr('d', function(d) {
          var o;
          o = {
            x: source.x0,
            y: source.y0
          };
          return $scope.diagonal({
            source: o,
            target: o
          });
        }).transition().attr('d', $scope.diagonal);
        links.transition().attr('d', $scope.diagonal);
        links.exit().transition().attr('d', function(d) {
          var o;
          o = {
            x: source.x,
            y: source.y
          };
          return $scope.diagonal({
            source: o,
            target: o
          });
        }).remove();
        if ($scope.selected.segment) {
          ancestors = [];
          parent = $scope.selected.segment;
          while (!_.isUndefined(parent)) {
            ancestors.push(parent);
            parent = parent.parent;
          }
          matchedLinks = [];
          links.filter(function(d, i) {
            return _.any(ancestors, function(p) {
              return p === d.target;
            });
          }).classed('active', true);
        }
        return nodes.forEach(function(d) {
          d.x0 = d.x;
          return d.y0 = d.y;
        });
      };
      return wrap = function(text, width) {
        return text.each(function() {
          var dy, line, lineHeight, lineNumber, results, t, tspan, word, words, y;
          t = d3.select(this);
          words = t.text().split(/\s+/).reverse();
          word = void 0;
          line = [];
          lineNumber = 0;
          lineHeight = 1.1;
          y = t.attr('y');
          dy = parseFloat(t.attr('dy'));
          tspan = t.text(null).append('tspan').attr('x', function(d) {
            if (d.children) {
              return 0;
            } else {
              return 15;
            }
          }).attr('y', function(d) {
            if (d.children) {
              return 19;
            } else {
              return 0;
            }
          }).attr('dy', dy + 'em');
          results = [];
          while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(' '));
            if (tspan.node().getComputedTextLength() > width) {
              line.pop();
              tspan.text(line.join(' '));
              line = [word];
              results.push(tspan = t.append('tspan').attr('x', function(d) {
                if (d.children) {
                  return 0;
                } else {
                  return 15;
                }
              }).attr('y', function(d) {
                if (d.children) {
                  return 19;
                } else {
                  return 0;
                }
              }).attr('dy', ++lineNumber * lineHeight + dy + 'em').text(word));
            } else {
              results.push(void 0);
            }
          }
          return results;
        });
      };
    }
  ]);

}).call(this);
