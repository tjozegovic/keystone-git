angular.module('app.reports')
  .controller 'ReportsSummary', [
    '$scope', '$http', '$timeout', '$location', '$q', 'growl', '$filter'
    ($scope, $http, $timeout, $location, $q, growl, $filter) ->
      addSpaces = (num) -> new Array(num + 1).join String.fromCharCode(160)

      $scope.sortDelay = (segment, status) ->
        steps = $scope.statuses[status]?.segments[segment]?.steps

        (step) ->
          return unless (step = steps?[step.key])?
          step.delay.value * if step.delay.units is 'hour' then 1 else 24

      $scope.filter = []
      $scope.expanded =
        groups: []
        statuses: []

      $scope.display = type: $location.search().type or 'message'
      $scope.$watch 'display.type', (type, old) ->
        $location.search 'type', type
        return unless type isnt old
        reload()

      $q.all ['/api/statuses?q=active', '/api/attributes?q=active'].map (url) -> $http.get url
      .then (results) ->
        [statuses, attributes] = (x.data for x in results)

        $scope.statuses = _(statuses)
          .forEach (status) ->
            status.segments = _(status.segments)
              .indexBy 'id'
              .forEach (segment) ->
                segment.status = status.id
                segment.steps = _(segment.steps)
                  .sortBy 'delay.delayInHours'
                  .indexBy 'id'
                  .forEach (step) ->
                    step.status = status.id
                    step.segment = segment.id
                  .value()
              .value()
          .value()

        $scope.stepfilter = _.reduce _.sortBy($scope.statuses, 'name'), (filter, status) ->
          filter.push status: status.id, name: status.name + ' (Status)'
          _.forEach _.sortBy(status.segments, 'name'), (segment) ->
            filter.push status: status.id, segment: segment.id, name: addSpaces(2) + segment.name + ' (Segment)'
            _.forEach segment.steps, (step) ->
              filter.push status: status.id, segment: segment.id, step: step.id, name: addSpaces(4) + step.name

          filter
        , []

        $scope.groups = _(attributes)
          .indexBy 'id'
          .forEach (group) ->
            group.attributes = _(group.values)
              .indexBy 'id'
              .forEach (attr) ->
                attr.group = group.id
              .value()
          .value()

        $scope.attributefilter = _.reduce _.sortBy($scope.groups, 'name'), (filter, group) ->
          filter.push group: group.id, name: group.name + ' (Group)'
          _.forEach _.sortBy(group.attributes, 'name'), (attribute) ->
            filter.push group: group.id, attribute: attribute.id, name: addSpaces(2) + attribute.name

          filter
        , []

      .catch (err) ->
        growl.danger err

      $scope.selectedValues = (filter) ->
        $scope.filter = _.map $filter('filter')(filter, checked: true), (val) ->
          _.pick val, 'status', 'segment', 'step', 'group', 'attribute'

      $scope.resetFilter = (filter) ->
        _.map filter, (val) -> val.checked = false
        $scope.filter = []
        reload()

      $scope.applyFilter = ->
        reload()

      $scope._rollup = (rows) ->
        rollup = _.reduce rows, (summary, row) ->
          summary[id = row.key.pop()] = _.mapValues row.value, (val, key) -> val + (summary[id]?[key] or 0)
          summary
        , {}

        _(rollup)
          .map (item, id) ->
            key: id, stats: item
          .sortBy (kvp) ->
            kvp.stats.sends
          .reverse()
          .value()

      start = null
      $scope.reset = ->
        $timeout ->
          $scope.$loading = no
        , Math.max 0, 680 - (Date.now() - start)

      reload = ->
        start = Date.now()
        $scope.$loading = yes
        $http.get '/api/summaries?q=drips',
          params: _.assign
            root: if $scope.display.type is 'message' then 'statuses' else 'groups'
            from: $scope.range.from.toJSON()
            to: $scope.range.to.toJSON()
            filter: JSON.stringify($scope.filter)
        .success (rows) ->
          $scope.summary = _(rows)
            .map (row) ->
              key: row.status or row.group
              stats: row
            .sortBy ({stats}) ->
              stats.sends
            .reverse()
            .value()

          $timeout ->
            $scope.$loading = no
          , Math.max 0, 680 - (Date.now() - start)
        .error (err) ->
          growl.danger err

      reload = _.debounce reload, 1200, leading: yes, trailing: no
      $scope.$watchCollection 'range', reload
  ]

  .controller 'StatusesSummary', [
    '$scope', '$http', '$timeout',
    ($scope, $http, $timeout) ->
      $scope.$watch 'summary', (summary) ->
        return unless summary and $scope.display.type is 'message'

        if $scope.expanded.statuses.length
          async.forEachSeries $scope.expanded.statuses, ([status, segment], cb) ->
            $scope.expand status, segment, cb
          , (err) -> $scope.reset()
        else if $scope.summary.length is 1
          $scope.expand $scope.summary[0].key, null
        else
          $scope.reset()

      $scope.expand = (status, segment, cb) ->
        do (start = Date.now()) ->
          get = (cb) ->
            $http.get '/api/summaries?q=drips',
              params: _.assign
                root: 'statuses'
                from: $scope.range.from.toJSON()
                to: $scope.range.to.toJSON()
                status: status
                segment: segment
                filter: JSON.stringify($scope.filter)
            .success cb

          done = ->
            return cb() if cb?
            $scope.reset()
            $scope.expanded.statuses.push [status, segment]

          if segment
            return unless _status = _.find($scope.summary, key: status)
            return unless _segment = _.find(_status.segments, key: segment)

            _segment.steps = $loading: yes
            get (rows) ->
              {steps} = $scope.statuses[status].segments[segment]
              _segment.steps = _(rows)
                .map (row) ->
                  key: row.step
                  stats: row
                .sortBy ({key}) ->
                  return -1 unless steps[key]?

                  {value, type} = steps[key].delay
                  # type is either days or hours, changes days to hours for sorting
                  value * if type is 'days' then 24 else 1
                .value()

              $timeout done, Math.max 0, 350 - (Date.now() - start)
          else
            return unless _status = _.find($scope.summary, key: status)

            _status.segments = $loading: yes
            get (rows) ->
              _status.segments = _(rows)
                .map (row) ->
                  key: row.segment
                  stats: row
                  sortBy: -row.sends
                .sortBy 'sortBy'
                .value()

              $timeout done, Math.max 0, 350 - (Date.now() - start)

      $scope.close = (status, segment) ->
        if segment
          delete _.find(_.find($scope.summary, key: status).segments, key: segment).steps
        else
          delete _.find($scope.summary, key: status).segments

        $scope.expanded.statuses = _.reject $scope.expanded.statuses, ([i, j]) -> i is status and j is segment
  ]

  .controller 'AttributesSummary', [
    '$scope', '$http', '$timeout',
    ($scope, $http, $timeout) ->
      $scope.$watch 'summary', (summary) ->
        return unless summary and $scope.display.type is 'attribute'
        if $scope.expanded.groups.length
          async.forEachSeries $scope.expanded.groups, (group, cb) ->
            $scope.expand group, cb
          , (err) -> $scope.reset()
        else if $scope.summary.length is 1
          $scope.expand $scope.summary[0].key, null
        else
          $scope.reset()

      $scope.expand = (group, cb) ->
        do (start = Date.now()) ->
          get = (cb) ->
            $http.get 'api/summaries?q=drips',
              params: _.assign
                root: 'groups'
                group: group
                from: $scope.range.from.toJSON()
                to: $scope.range.to.toJSON()
                filter: JSON.stringify($scope.filter)
            .success cb

          done = ->
            return cb() if cb?
            $scope.reset()
            $scope.expanded.groups.push group

          if group
            return unless _group = _.find($scope.summary, key: group)

            _group.attributes = $loading: yes
            get (rows) ->
              $timeout ->
                _group.attributes = _(rows)
                  .map (row) ->
                    key: row.attribute
                    stats: row
                  .sortBy ({key}) ->
                    $scope.groups[group].attributes[key]?.name
                  .value()
                done()
              , Math.max 0, 1000 - (Date.now() - start)

      $scope.close = (group) ->
        delete _.find($scope.summary, key: group).attributes
        $scope.expanded.groups = _.reject $scope.expanded.groups, (g) -> g is group
  ]
