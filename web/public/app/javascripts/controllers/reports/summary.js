(function() {
  angular.module('app.reports').controller('ReportsSummary', [
    '$scope', '$http', '$timeout', '$location', '$q', 'growl', '$filter', function($scope, $http, $timeout, $location, $q, growl, $filter) {
      var addSpaces, reload, start;
      addSpaces = function(num) {
        return new Array(num + 1).join(String.fromCharCode(160));
      };
      $scope.sortDelay = function(segment, status) {
        var ref, ref1, steps;
        steps = (ref = $scope.statuses[status]) != null ? (ref1 = ref.segments[segment]) != null ? ref1.steps : void 0 : void 0;
        return function(step) {
          if ((step = steps != null ? steps[step.key] : void 0) == null) {
            return;
          }
          return step.delay.value * (step.delay.units === 'hour' ? 1 : 24);
        };
      };
      $scope.filter = [];
      $scope.expanded = {
        groups: [],
        statuses: []
      };
      $scope.display = {
        type: $location.search().type || 'message'
      };
      $scope.$watch('display.type', function(type, old) {
        $location.search('type', type);
        if (type === old) {
          return;
        }
        return reload();
      });
      $q.all(['/api/statuses?q=active', '/api/attributes?q=active'].map(function(url) {
        return $http.get(url);
      })).then(function(results) {
        var attributes, ref, statuses, x;
        ref = (function() {
          var k, len, results1;
          results1 = [];
          for (k = 0, len = results.length; k < len; k++) {
            x = results[k];
            results1.push(x.data);
          }
          return results1;
        })(), statuses = ref[0], attributes = ref[1];
        $scope.statuses = _(statuses).forEach(function(status) {
          return status.segments = _(status.segments).indexBy('id').forEach(function(segment) {
            segment.status = status.id;
            return segment.steps = _(segment.steps).sortBy('delay.delayInHours').indexBy('id').forEach(function(step) {
              step.status = status.id;
              return step.segment = segment.id;
            }).value();
          }).value();
        }).value();
        $scope.stepfilter = _.reduce(_.sortBy($scope.statuses, 'name'), function(filter, status) {
          filter.push({
            status: status.id,
            name: status.name + ' (Status)'
          });
          _.forEach(_.sortBy(status.segments, 'name'), function(segment) {
            filter.push({
              status: status.id,
              segment: segment.id,
              name: addSpaces(2) + segment.name + ' (Segment)'
            });
            return _.forEach(segment.steps, function(step) {
              return filter.push({
                status: status.id,
                segment: segment.id,
                step: step.id,
                name: addSpaces(4) + step.name
              });
            });
          });
          return filter;
        }, []);
        $scope.groups = _(attributes).indexBy('id').forEach(function(group) {
          return group.attributes = _(group.values).indexBy('id').forEach(function(attr) {
            return attr.group = group.id;
          }).value();
        }).value();
        return $scope.attributefilter = _.reduce(_.sortBy($scope.groups, 'name'), function(filter, group) {
          filter.push({
            group: group.id,
            name: group.name + ' (Group)'
          });
          _.forEach(_.sortBy(group.attributes, 'name'), function(attribute) {
            return filter.push({
              group: group.id,
              attribute: attribute.id,
              name: addSpaces(2) + attribute.name
            });
          });
          return filter;
        }, []);
      })["catch"](function(err) {
        return growl.danger(err);
      });
      $scope.selectedValues = function(filter) {
        return $scope.filter = _.map($filter('filter')(filter, {
          checked: true
        }), function(val) {
          return _.pick(val, 'status', 'segment', 'step', 'group', 'attribute');
        });
      };
      $scope.resetFilter = function(filter) {
        _.map(filter, function(val) {
          return val.checked = false;
        });
        $scope.filter = [];
        return reload();
      };
      $scope.applyFilter = function() {
        return reload();
      };
      $scope._rollup = function(rows) {
        var rollup;
        rollup = _.reduce(rows, function(summary, row) {
          var id;
          summary[id = row.key.pop()] = _.mapValues(row.value, function(val, key) {
            var ref;
            return val + (((ref = summary[id]) != null ? ref[key] : void 0) || 0);
          });
          return summary;
        }, {});
        return _(rollup).map(function(item, id) {
          return {
            key: id,
            stats: item
          };
        }).sortBy(function(kvp) {
          return kvp.stats.sends;
        }).reverse().value();
      };
      start = null;
      $scope.reset = function() {
        return $timeout(function() {
          return $scope.$loading = false;
        }, Math.max(0, 680 - (Date.now() - start)));
      };
      reload = function() {
        start = Date.now();
        $scope.$loading = true;
        return $http.get('/api/summaries?q=drips', {
          params: _.assign({
            root: $scope.display.type === 'message' ? 'statuses' : 'groups',
            from: $scope.range.from.toJSON(),
            to: $scope.range.to.toJSON(),
            filter: JSON.stringify($scope.filter)
          })
        }).success(function(rows) {
          $scope.summary = _(rows).map(function(row) {
            return {
              key: row.status || row.group,
              stats: row
            };
          }).sortBy(function(arg) {
            var stats;
            stats = arg.stats;
            return stats.sends;
          }).reverse().value();
          return $timeout(function() {
            return $scope.$loading = false;
          }, Math.max(0, 680 - (Date.now() - start)));
        }).error(function(err) {
          return growl.danger(err);
        });
      };
      reload = _.debounce(reload, 1200, {
        leading: true,
        trailing: false
      });
      return $scope.$watchCollection('range', reload);
    }
  ]).controller('StatusesSummary', [
    '$scope', '$http', '$timeout', function($scope, $http, $timeout) {
      $scope.$watch('summary', function(summary) {
        if (!(summary && $scope.display.type === 'message')) {
          return;
        }
        if ($scope.expanded.statuses.length) {
          return async.forEachSeries($scope.expanded.statuses, function(arg, cb) {
            var segment, status;
            status = arg[0], segment = arg[1];
            return $scope.expand(status, segment, cb);
          }, function(err) {
            return $scope.reset();
          });
        } else if ($scope.summary.length === 1) {
          return $scope.expand($scope.summary[0].key, null);
        } else {
          return $scope.reset();
        }
      });
      $scope.expand = function(status, segment, cb) {
        return (function(start) {
          var _segment, _status, done, get;
          get = function(cb) {
            return $http.get('/api/summaries?q=drips', {
              params: _.assign({
                root: 'statuses',
                from: $scope.range.from.toJSON(),
                to: $scope.range.to.toJSON(),
                status: status,
                segment: segment,
                filter: JSON.stringify($scope.filter)
              })
            }).success(cb);
          };
          done = function() {
            if (cb != null) {
              return cb();
            }
            $scope.reset();
            return $scope.expanded.statuses.push([status, segment]);
          };
          if (segment) {
            if (!(_status = _.find($scope.summary, {
              key: status
            }))) {
              return;
            }
            if (!(_segment = _.find(_status.segments, {
              key: segment
            }))) {
              return;
            }
            _segment.steps = {
              $loading: true
            };
            return get(function(rows) {
              var steps;
              steps = $scope.statuses[status].segments[segment].steps;
              _segment.steps = _(rows).map(function(row) {
                return {
                  key: row.step,
                  stats: row
                };
              }).sortBy(function(arg) {
                var key, ref, type, value;
                key = arg.key;
                if (steps[key] == null) {
                  return -1;
                }
                ref = steps[key].delay, value = ref.value, type = ref.type;
                return value * (type === 'days' ? 24 : 1);
              }).value();
              return $timeout(done, Math.max(0, 350 - (Date.now() - start)));
            });
          } else {
            if (!(_status = _.find($scope.summary, {
              key: status
            }))) {
              return;
            }
            _status.segments = {
              $loading: true
            };
            return get(function(rows) {
              _status.segments = _(rows).map(function(row) {
                return {
                  key: row.segment,
                  stats: row,
                  sortBy: -row.sends
                };
              }).sortBy('sortBy').value();
              return $timeout(done, Math.max(0, 350 - (Date.now() - start)));
            });
          }
        })(Date.now());
      };
      return $scope.close = function(status, segment) {
        if (segment) {
          delete _.find(_.find($scope.summary, {
            key: status
          }).segments, {
            key: segment
          }).steps;
        } else {
          delete _.find($scope.summary, {
            key: status
          }).segments;
        }
        return $scope.expanded.statuses = _.reject($scope.expanded.statuses, function(arg) {
          var i, j;
          i = arg[0], j = arg[1];
          return i === status && j === segment;
        });
      };
    }
  ]).controller('AttributesSummary', [
    '$scope', '$http', '$timeout', function($scope, $http, $timeout) {
      $scope.$watch('summary', function(summary) {
        if (!(summary && $scope.display.type === 'attribute')) {
          return;
        }
        if ($scope.expanded.groups.length) {
          return async.forEachSeries($scope.expanded.groups, function(group, cb) {
            return $scope.expand(group, cb);
          }, function(err) {
            return $scope.reset();
          });
        } else if ($scope.summary.length === 1) {
          return $scope.expand($scope.summary[0].key, null);
        } else {
          return $scope.reset();
        }
      });
      $scope.expand = function(group, cb) {
        return (function(start) {
          var _group, done, get;
          get = function(cb) {
            return $http.get('api/summaries?q=drips', {
              params: _.assign({
                root: 'groups',
                group: group,
                from: $scope.range.from.toJSON(),
                to: $scope.range.to.toJSON(),
                filter: JSON.stringify($scope.filter)
              })
            }).success(cb);
          };
          done = function() {
            if (cb != null) {
              return cb();
            }
            $scope.reset();
            return $scope.expanded.groups.push(group);
          };
          if (group) {
            if (!(_group = _.find($scope.summary, {
              key: group
            }))) {
              return;
            }
            _group.attributes = {
              $loading: true
            };
            return get(function(rows) {
              return $timeout(function() {
                _group.attributes = _(rows).map(function(row) {
                  return {
                    key: row.attribute,
                    stats: row
                  };
                }).sortBy(function(arg) {
                  var key, ref;
                  key = arg.key;
                  return (ref = $scope.groups[group].attributes[key]) != null ? ref.name : void 0;
                }).value();
                return done();
              }, Math.max(0, 1000 - (Date.now() - start)));
            });
          }
        })(Date.now());
      };
      return $scope.close = function(group) {
        delete _.find($scope.summary, {
          key: group
        }).attributes;
        return $scope.expanded.groups = _.reject($scope.expanded.groups, function(g) {
          return g === group;
        });
      };
    }
  ]);

}).call(this);
