angular.module('app')
  .directive 'bsCheck', ['$parse', ($parse) ->
    require: ['^?bsRadios', '?ngModel']
    templateUrl: 'bsRadios/radio.html'
    transclude: yes
    replace: yes
    scope: {}
    link: (scope, elem, attrs, [radios, ngModel]) ->
      scope.klass = ->
        klasses = []
        if attrs.size then klasses.push "btn-#{attrs.size}"

        if scope.isSelected
          klasses.push 'checked', 'btn-success'
        else
          klasses.push 'btn-default'
        klasses

      unless 'readOnly' of attrs
        elem.on 'click', (e) ->
          e.preventDefault()
          last = scope.isSelected

          unless radios
            scope.$apply -> scope.isSelected = !last

          radios?.change attrs.value
          scope.$apply ->
            ngModel?.$setViewValue? !last

      if ngModel
        ngModel.$render = ->
          scope.isSelected = ngModel.$viewValue

      return unless radios
      scope.$watch ->
        radios.type()
      , (nv, ov) ->
        scope.isSelected = nv is attrs.value
  ]
