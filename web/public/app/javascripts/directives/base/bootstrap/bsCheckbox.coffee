angular.module('app')
  .directive 'bsCheckbox', ->
    require: '?ngModel'
    link: (scope, elm, attrs, ngModel) ->
      elm.addClass 'checks'

      unless 'readOnly' of attrs
        add = ->
          ngModel.$modelValue.push attrs.value
          ngModel.$setViewValue ngModel.$modelValue

        remove = ->
          ngModel.$setViewValue _.without ngModel.$modelValue, attrs.value

        elm.on 'click', (e) ->
          if attrs.value in ngModel.$modelValue then remove() else add()
          ngModel.$render()
          e.preventDefault()
          scope.$apply()

      ngModel.$render = ->
        scope.isSelected = ngModel.$modelValue and attrs.value in ngModel.$modelValue
        elm.find('input').prop 'checked', scope.isSelected
        elm.toggleClass 'checked', scope.isSelected
