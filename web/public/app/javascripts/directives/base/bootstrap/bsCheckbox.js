(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('app').directive('bsCheckbox', function() {
    return {
      require: '?ngModel',
      link: function(scope, elm, attrs, ngModel) {
        var add, remove;
        elm.addClass('checks');
        if (!('readOnly' in attrs)) {
          add = function() {
            ngModel.$modelValue.push(attrs.value);
            return ngModel.$setViewValue(ngModel.$modelValue);
          };
          remove = function() {
            return ngModel.$setViewValue(_.without(ngModel.$modelValue, attrs.value));
          };
          elm.on('click', function(e) {
            var ref;
            if (ref = attrs.value, indexOf.call(ngModel.$modelValue, ref) >= 0) {
              remove();
            } else {
              add();
            }
            ngModel.$render();
            e.preventDefault();
            return scope.$apply();
          });
        }
        return ngModel.$render = function() {
          var ref;
          scope.isSelected = ngModel.$modelValue && (ref = attrs.value, indexOf.call(ngModel.$modelValue, ref) >= 0);
          elm.find('input').prop('checked', scope.isSelected);
          return elm.toggleClass('checked', scope.isSelected);
        };
      }
    };
  });

}).call(this);
