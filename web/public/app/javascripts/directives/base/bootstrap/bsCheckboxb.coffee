angular.module('app')
  .directive 'bsCheckboxb', ->
    require: '?ngModel'
    template: '''
      <label class="btn" ng-class='{"checked": isSelected, "btn-default": !isSelected, "btn-success": isSelected}'>
        <i class="fa fa-check-square-o truthy" />
        <i class="fa fa-square-o falsy" />
        <input type='checkbox' />
        &nbsp;{{title}}
      </label>
    '''
    link: (scope, elm, attrs, ngModel) ->
      elm.addClass 'checks'
      scope.title = attrs.title

      unless 'readOnly' of attrs
        elm.on 'click', (e) ->
          fs = elm.find(':checkbox').parents('fieldset')
          return if fs?.is(':disabled')

          e.preventDefault()
          scope.$apply ->
            ngModel.$setViewValue scope.isSelected = !scope.isSelected
            elm.toggleClass 'checked', scope.isSelected

      ngModel.$render = ->
        scope.isSelected = !!ngModel.$modelValue
        elm.find('input').prop 'checked', scope.isSelected
        elm.toggleClass 'checked', scope.isSelected
