(function() {
  angular.module('app').directive('bsCheckboxb', function() {
    return {
      require: '?ngModel',
      template: '<label class="btn" ng-class=\'{"checked": isSelected, "btn-default": !isSelected, "btn-success": isSelected}\'>\n  <i class="fa fa-check-square-o truthy" />\n  <i class="fa fa-square-o falsy" />\n  <input type=\'checkbox\' />\n  &nbsp;{{title}}\n</label>',
      link: function(scope, elm, attrs, ngModel) {
        elm.addClass('checks');
        scope.title = attrs.title;
        if (!('readOnly' in attrs)) {
          elm.on('click', function(e) {
            var fs;
            fs = elm.find(':checkbox').parents('fieldset');
            if (fs != null ? fs.is(':disabled') : void 0) {
              return;
            }
            e.preventDefault();
            return scope.$apply(function() {
              ngModel.$setViewValue(scope.isSelected = !scope.isSelected);
              return elm.toggleClass('checked', scope.isSelected);
            });
          });
        }
        return ngModel.$render = function() {
          scope.isSelected = !!ngModel.$modelValue;
          elm.find('input').prop('checked', scope.isSelected);
          return elm.toggleClass('checked', scope.isSelected);
        };
      }
    };
  });

}).call(this);
