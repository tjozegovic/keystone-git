angular.module('app')
  .directive 'bsDatetimePicker', ->
    require: '?ngModel'
    link: (scope, elm, attrs, ngModel) ->
      elm.datetimepicker defaultDate: new Date

      attrs.$observe 'disabled', (val) ->
        elm.data('DateTimePicker')[if val then 'disable' else 'enable']()

      elm.on 'change', (e) ->
        # TODO this is a bit hacky...
        # https://github.com/angular/angular.js/wiki/Anti-Patterns
        # my problem is that you can't get much higher than an event in the "call stack"
        # this is really related to how quickly in combo with a $http request something happens
        return if scope.$$phase

        scope.$apply ->
          if attrs.dateFormat
            ngModel.$setViewValue elm.data('DateTimePicker').getDate().format attrs.dateFormat
          else
            # valueOf returns the actual datetime instead of the moment object...
            ngModel.$setViewValue elm.data('DateTimePicker').getDate()

      ngModel.$render = ->
        elm.data('DateTimePicker').setDate new Date ngModel.$viewValue
