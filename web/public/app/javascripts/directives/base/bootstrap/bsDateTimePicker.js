(function() {
  angular.module('app').directive('bsDatetimePicker', function() {
    return {
      require: '?ngModel',
      link: function(scope, elm, attrs, ngModel) {
        elm.datetimepicker({
          defaultDate: new Date
        });
        attrs.$observe('disabled', function(val) {
          return elm.data('DateTimePicker')[val ? 'disable' : 'enable']();
        });
        elm.on('change', function(e) {
          if (scope.$$phase) {
            return;
          }
          return scope.$apply(function() {
            if (attrs.dateFormat) {
              return ngModel.$setViewValue(elm.data('DateTimePicker').getDate().format(attrs.dateFormat));
            } else {
              return ngModel.$setViewValue(elm.data('DateTimePicker').getDate());
            }
          });
        });
        return ngModel.$render = function() {
          return elm.data('DateTimePicker').setDate(new Date(ngModel.$viewValue));
        };
      }
    };
  });

}).call(this);
