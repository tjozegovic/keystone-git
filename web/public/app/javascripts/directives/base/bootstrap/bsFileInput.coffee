angular.module('app')
  .directive 'bsFileInput', ->
    scope: 'onReset': '&'
    link: (scope, elm, attrs) ->
      elm.fileinput()
      elm.on 'clear.bs.fileinput', ->
        scope.$apply -> scope.onReset()
      elm.on 'reset.bs.fileinput', ->
        scope.$apply -> scope.onReset()
      scope.$on 'clear.fileinput', -> elm.fileinput 'clear'
