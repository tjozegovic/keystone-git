(function() {
  angular.module('app').directive('bsFileInput', function() {
    return {
      scope: {
        'onReset': '&'
      },
      link: function(scope, elm, attrs) {
        elm.fileinput();
        elm.on('clear.bs.fileinput', function() {
          return scope.$apply(function() {
            return scope.onReset();
          });
        });
        elm.on('reset.bs.fileinput', function() {
          return scope.$apply(function() {
            return scope.onReset();
          });
        });
        return scope.$on('clear.fileinput', function() {
          return elm.fileinput('clear');
        });
      }
    };
  });

}).call(this);
