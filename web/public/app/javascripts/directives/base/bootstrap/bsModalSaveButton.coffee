angular.module('app')
    .directive 'bsModalSaveButton', ['$timeout', ($timeout) ->
      scope: onSave: '&', errorexists : '@', errorAlert: '@'
      restrict: 'E'
      require: '^form'
      template: '''
      <span>
        <span class="{{errorClass}}" ng-if="errorMessage && !form.saving">
          <i class="fa fa-exclamation-triangle" />
          <span>{{errorMessage}}</span>
        </span>
        <span class="{{successClass}}" ng-if="form.saved && !form.saving">
          <i class="fa fa-check" />
          <span>&nbsp;{{finishedMessage}}&nbsp;</span>
        </span>
        <span class="{{errorClass}}" ng-if="form.submitted && form.$invalid && !form.$dirty">
          <i class="fa fa-exclamation-triangle" />
          <span>&nbsp;{{errorAlert || 'Please fill out required fields'}}&nbsp;</span>
        </span>
        <span ng-if="form.saving">
          <i class="fa fa-spinner fa-spin" />
          <span>&nbsp;{{runningMessage}}&nbsp;</span>
        </span>
        <button class="btn {{btnClass}}", ng-click='save()' ng-disabled="form.$invalid || form.saving">{{btnTitle}}</button>
      </span>
      '''
      replace: yes
      link: (scope, element, attributes, form) ->
        scope.form = form
        scope.form.saved = no
        scope.form.submitted = no
        _.assign scope, _.omit attributes, ['onSave']

        scope.btnClass ?= 'btn-primary'
        scope.btnTitle ?= 'Save'
        scope.successClass ?= 'text-success'
        scope.errorClass ?= 'text-danger'
        scope.runningMessage ?= 'Saving'
        scope.finishedMessage ?= 'Saved!'

        element.addClass 'bs-save-button'

        scope.save = ->
          scope.onSave()
    ]
