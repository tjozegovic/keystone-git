(function() {
  angular.module('app').directive('bsModalSaveButton', [
    '$timeout', function($timeout) {
      return {
        scope: {
          onSave: '&',
          errorexists: '@',
          errorAlert: '@'
        },
        restrict: 'E',
        require: '^form',
        template: '<span>\n  <span class="{{errorClass}}" ng-if="errorMessage && !form.saving">\n    <i class="fa fa-exclamation-triangle" />\n    <span>{{errorMessage}}</span>\n  </span>\n  <span class="{{successClass}}" ng-if="form.saved && !form.saving">\n    <i class="fa fa-check" />\n    <span>&nbsp;{{finishedMessage}}&nbsp;</span>\n  </span>\n  <span class="{{errorClass}}" ng-if="form.submitted && form.$invalid && !form.$dirty">\n    <i class="fa fa-exclamation-triangle" />\n    <span>&nbsp;{{errorAlert || \'Please fill out required fields\'}}&nbsp;</span>\n  </span>\n  <span ng-if="form.saving">\n    <i class="fa fa-spinner fa-spin" />\n    <span>&nbsp;{{runningMessage}}&nbsp;</span>\n  </span>\n  <button class="btn {{btnClass}}", ng-click=\'save()\' ng-disabled="form.$invalid || form.saving">{{btnTitle}}</button>\n</span>',
        replace: true,
        link: function(scope, element, attributes, form) {
          scope.form = form;
          scope.form.saved = false;
          scope.form.submitted = false;
          _.assign(scope, _.omit(attributes, ['onSave']));
          if (scope.btnClass == null) {
            scope.btnClass = 'btn-primary';
          }
          if (scope.btnTitle == null) {
            scope.btnTitle = 'Save';
          }
          if (scope.successClass == null) {
            scope.successClass = 'text-success';
          }
          if (scope.errorClass == null) {
            scope.errorClass = 'text-danger';
          }
          if (scope.runningMessage == null) {
            scope.runningMessage = 'Saving';
          }
          if (scope.finishedMessage == null) {
            scope.finishedMessage = 'Saved!';
          }
          element.addClass('bs-save-button');
          return scope.save = function() {
            return scope.onSave();
          };
        }
      };
    }
  ]);

}).call(this);
