(function() {
  angular.module('app').directive('bsRadio', function() {
    return {
      scope: {
        type: '=ngModel'
      },
      template: '<label class="btn" ng-class=\'{"btn-default": !isSelected, "btn-success": isSelected}\'>\n  <i class="fa fa-fw fa-check-square-o truthy" />\n  <i class="fa fa-fw fa-square-o falsy" />\n  <input type=\'checkbox\' />\n  &nbsp;{{title}}\n</label>',
      link: function($scope, $element, $attributes) {
        $element.addClass('checks');
        $scope.title = $attributes.title;
        if (!('readOnly' in $attributes)) {
          $element.on('click', function(e) {
            var fs;
            fs = $($element).find(':checkbox').parents('fieldset');
            if (fs != null ? fs.is(':disabled') : void 0) {
              return;
            }
            e.preventDefault();
            return $scope.$apply(function() {
              return $scope.type = $attributes.value;
            });
          });
        }
        return $scope.$watch('type', function(nv, ov) {
          $scope.isSelected = nv === $attributes.value;
          $element.find('input').prop('checked', $scope.isSelected);
          return $element.toggleClass('checked', $scope.isSelected);
        });
      }
    };
  });

}).call(this);
