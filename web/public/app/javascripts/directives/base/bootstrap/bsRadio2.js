(function() {
  angular.module('app').directive('bsRadio2', [
    '$parse', function($parse) {
      return {
        require: ['^?bsRadios', '?ngModel'],
        templateUrl: 'bsRadios/radio.html',
        transclude: true,
        replace: true,
        scope: {},
        link: function(scope, elem, attrs, arg) {
          var ngModel, radios;
          radios = arg[0], ngModel = arg[1];
          scope.klass = function() {
            var klasses;
            klasses = [];
            if (attrs.size) {
              klasses.push("btn-" + attrs.size);
            }
            if (scope.isSelected) {
              klasses.push('checked', 'btn-' + ((radios != null ? radios.selectedClass : void 0) || 'success'));
            } else {
              klasses.push('btn-default');
            }
            return klasses;
          };
          if (!('readOnly' in attrs)) {
            elem.on('click', function(e) {
              var last;
              e.preventDefault();
              last = scope.isSelected;
              if (!radios) {
                scope.$apply(function() {
                  return scope.isSelected = !last;
                });
              }
              if (radios != null) {
                radios.change(attrs.value);
              }
              return scope.$apply(function() {
                return ngModel != null ? typeof ngModel.$setViewValue === "function" ? ngModel.$setViewValue(!last) : void 0 : void 0;
              });
            });
          }
          if (ngModel) {
            ngModel.$render = function() {
              return scope.isSelected = ngModel.$viewValue;
            };
          }
          if (!radios) {
            return;
          }
          return scope.$watch(function() {
            return radios.type();
          }, function(nv, ov) {
            return scope.isSelected = nv === attrs.value;
          });
        }
      };
    }
  ]);

}).call(this);
