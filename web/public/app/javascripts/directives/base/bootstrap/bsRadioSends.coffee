angular.module('app')
  .directive 'bsRadioSends', ->
    scope: type: '=ngModel'
    template: '''
      <label class="btn sendSearch" ng-class='{"btn-default": !isSelected, "btn-default active": isSelected}'>
        <input type='checkbox' />
        {{title}}
      </label>
    '''
    link: ($scope, $element, $attributes) ->
      $element.addClass 'checks'
      $scope.title = $attributes.title

      unless 'readOnly' of $attributes
        $element.on 'click', (e) ->
          fs = $($element).find(':checkbox').parents('fieldset')
          return if fs?.is(':disabled')

          e.preventDefault()
          $scope.$apply -> $scope.type = $attributes.value

      $scope.$watch 'type', (nv, ov) ->
        $scope.isSelected = nv is $attributes.value
        $element.find('input').prop 'checked', $scope.isSelected
        $element.toggleClass 'checked', $scope.isSelected
