angular.module('app')
  .directive 'bsRadios', ->
    require: '^?form'
    scope: type: '=ngModel'
    controller: ['$scope', '$attrs', ($scope, $attrs) ->
      @change = (type) ->
        $scope.$apply ->
          $scope.type = type
          $scope.form?.$setDirty()

      @type = -> $scope.type
      @selectedClass = $attrs.selectedClass
    ]
    link: (scope, elem, attrs, form) ->
      scope.form = form
