(function() {
  angular.module('app').directive('bsRadios', function() {
    return {
      require: '^?form',
      scope: {
        type: '=ngModel'
      },
      controller: [
        '$scope', '$attrs', function($scope, $attrs) {
          this.change = function(type) {
            return $scope.$apply(function() {
              var ref;
              $scope.type = type;
              return (ref = $scope.form) != null ? ref.$setDirty() : void 0;
            });
          };
          this.type = function() {
            return $scope.type;
          };
          return this.selectedClass = $attrs.selectedClass;
        }
      ],
      link: function(scope, elem, attrs, form) {
        return scope.form = form;
      }
    };
  });

}).call(this);
