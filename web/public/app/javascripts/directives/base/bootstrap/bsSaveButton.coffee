angular.module('app')
  .directive 'bsSaveButton', ['$timeout', ($timeout) ->
    scope: onSave: '&', errorexists : '@'
    restrict: 'E'
    require: '^form'
    templateUrl: 'bsSaveButton.html'
    replace: yes
    link: (scope, element, attributes, form) ->
      scope.form = form
      scope.form.$saved = no
      _.assign scope, _.omit attributes, ['onSave']

      scope.spinnerPosition ?= 'right'
      scope.btnClass ?= 'btn-primary'

      element.addClass 'bs-save-button'

      scope.save = ->
        scope.start = (new Date).getTime()
        scope.form.$submitting = yes
        scope.form.$saved = no
        scope.onSave()

      do (old = scope.form.$setPristine) ->
        scope.form.$setPristine = ->
          $timeout ->
            scope.form.$submitting = no
            scope.form.$saved = yes
          , Math.max 0, 600 - ((new Date).getTime() - scope.start) # force at least 600ms
          old()
  ]
