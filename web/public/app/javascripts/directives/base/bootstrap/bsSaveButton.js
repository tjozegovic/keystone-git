(function() {
  angular.module('app').directive('bsSaveButton', [
    '$timeout', function($timeout) {
      return {
        scope: {
          onSave: '&',
          errorexists: '@'
        },
        restrict: 'E',
        require: '^form',
        templateUrl: 'bsSaveButton.html',
        replace: true,
        link: function(scope, element, attributes, form) {
          scope.form = form;
          scope.form.$saved = false;
          _.assign(scope, _.omit(attributes, ['onSave']));
          if (scope.spinnerPosition == null) {
            scope.spinnerPosition = 'right';
          }
          if (scope.btnClass == null) {
            scope.btnClass = 'btn-primary';
          }
          element.addClass('bs-save-button');
          scope.save = function() {
            scope.start = (new Date).getTime();
            scope.form.$submitting = true;
            scope.form.$saved = false;
            return scope.onSave();
          };
          return (function(old) {
            return scope.form.$setPristine = function() {
              $timeout(function() {
                scope.form.$submitting = false;
                return scope.form.$saved = true;
              }, Math.max(0, 600 - ((new Date).getTime() - scope.start)));
              return old();
            };
          })(scope.form.$setPristine);
        }
      };
    }
  ]);

}).call(this);
