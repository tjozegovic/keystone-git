angular.module('app')
  .directive 'goBack', ['$window', ($window) ->
    transclude: yes
    template: '<a style="cursor: pointer" ng-transclude></a>'
    link: (scope, element, attrs) ->
      element.on 'click', -> $window.history.back()
  ]
