(function() {
  angular.module('app').directive('goBack', [
    '$window', function($window) {
      return {
        transclude: true,
        template: '<a style="cursor: pointer" ng-transclude></a>',
        link: function(scope, element, attrs) {
          return element.on('click', function() {
            return $window.history.back();
          });
        }
      };
    }
  ]);

}).call(this);
