angular.module('app')
  .directive 'scrollTo', ['$timeout', ($timeout) ->
    restrict: 'A'
    link: ($scope, $element, $attributes) ->
      $scope.$watch ->
        $element[0].childNodes.length
      , _.debounce ->
        [list, selected] = [$element, $element.find($attributes.scrollTo)]
        return unless selected.length

        pright = list.outerWidth() + list.offset().left
        # scroll the window so that there are two templates in view if possible
        cright = 2 * selected.outerWidth() + selected.offset().left
        scrollto = cright - pright
        list.animate scrollLeft: scrollto, 'slow'
      , 200
  ]
