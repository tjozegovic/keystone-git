(function() {
  angular.module('app').directive('scrollTo', [
    '$timeout', function($timeout) {
      return {
        restrict: 'A',
        link: function($scope, $element, $attributes) {
          return $scope.$watch(function() {
            return $element[0].childNodes.length;
          }, _.debounce(function() {
            var cright, list, pright, ref, scrollto, selected;
            ref = [$element, $element.find($attributes.scrollTo)], list = ref[0], selected = ref[1];
            if (!selected.length) {
              return;
            }
            pright = list.outerWidth() + list.offset().left;
            cright = 2 * selected.outerWidth() + selected.offset().left;
            scrollto = cright - pright;
            return list.animate({
              scrollLeft: scrollto
            }, 'slow');
          }, 200));
        }
      };
    }
  ]);

}).call(this);
