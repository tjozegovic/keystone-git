angular.module('app')
  .directive 'wheelScroll', ->
    link: ($scope, $element, $attributes) ->
      $element.on 'mousewheel', (e, delta) ->
        el = $ @
        if el[0].scrollWidth > el.width()
          el.scrollLeft el.scrollLeft() - delta * 30
          e.preventDefault()
