(function() {
  angular.module('app').directive('wheelScroll', function() {
    return {
      link: function($scope, $element, $attributes) {
        return $element.on('mousewheel', function(e, delta) {
          var el;
          el = $(this);
          if (el[0].scrollWidth > el.width()) {
            el.scrollLeft(el.scrollLeft() - delta * 30);
            return e.preventDefault();
          }
        });
      }
    };
  });

}).call(this);
