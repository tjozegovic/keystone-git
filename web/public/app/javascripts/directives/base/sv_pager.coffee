angular.module('app')
  .directive 'svPaging', [
    '$injector', '$location', '$timeout', '$cookieStore',
    ($injector, $location, $timeout, $cookieStore) ->
      controller: ['$scope', '$attrs', '$parse', ($scope, $attrs, $parse) ->
        @search = keys: ['from', 'last', 'page', 'skip', 'size', 'to', 'whitelist', 'after']

        $scope.size = $attrs.initialSize if $attrs.initialSize?
        $scope.filter = undefined
        $scope.objects = []

        @whitelist = $parse $attrs.whitelist if $attrs.whitelist?

        @resource = -> $scope.resource
        @objects = -> $scope.objects
        $scope.$resource = $injector.get $attrs.svPaging

        $scope.$loading = yes
        @_fetch = (all, cb) =>
          [cb, all] = [all, no] unless cb

          query = {}
          if range = $scope.range
            query[key] = moment(range[key]).toISOString() for key in ['from', 'to']

          _.defaults query, _.omit $location.search(), 'page' # don't send page to the server
          $scope.filtered = 0 < _.difference(Object.keys(query), @search.keys).length

          get = =>
            query.size = $scope.size

            if $scope.page?
              query.skip = $scope.size * ($scope.page - 1)
            else
              query.after = $scope.after

            query.whitelist = @whitelist($scope).join ',' if @whitelist?

            query = _.omit query, 'page', 'size', 'skip', 'after' if all
            return result = $scope.$resource.query query, (_, headers) ->
              cb result, headers

          # we don't need to query the HEAD count unless date and/or other filters change
          return get() if (count_query = JSON.stringify query) is $scope.$filter

          # if the filter has changed, reset the after and the objects
          $scope.$filter = count_query
          $scope.after = null
          $scope.count = null
          $scope.resource = $scope.objects = null

          return get() unless _.isFunction $scope.$resource.count
          $scope.$resource.count query
            , (_, headers) ->
              $scope.count = headers 'X-Total-Count'
              get()
            , (error) ->
              # 404 means we have no count, that's ok
              get()

        @_fetchAll = (cb) => @_fetch yes, cb

        $scope.fetch = @fetch = =>
          [start, $scope.$loading] = [Date.now(), yes]
          @_fetch no, (results, headers) ->
            $timeout ->
              if $scope.page? or not $scope.resource?
                $scope.resource = $scope.objects = results
              else
                $scope.resource.push r for r in results

              $scope.last = results.length < $scope.size
              $scope.after = $scope.objects[$scope.objects.length - 1]?.id
              $scope.count = headers 'X-Total-Count'

              $scope.$loading = no
              $scope.$broadcast 'objects:fetched'
            , Math.max 0, 800 - (Date.now() - start)

        $scope.reset = =>
          removekeys = _.difference(Object.keys($location.search()), @search.keys)
          $location.search key, null for key in removekeys
          @fetch()

        $scope.prev = =>
          $scope.page = Math.max 1, --$scope.page if $scope.page?
          @fetch()

        $scope.next = =>
          return if $scope.$loading or $scope.last
          ++$scope.page if $scope.page?
          @fetch()

        $scope.$on 'objects:reload', =>
          console.log 'please reload the objects...'
          @fetch()

        $timeout @fetch
        return @
      ]
  ]

  .directive 'svPagingPager', ['$location', ($location) ->
    require: '^svPaging'
    templateUrl: 'svPaging/pager.html'
    link: (scope, elm, attrs, ctrl) ->
      _.assign scope,
        size: '20'
        page: '1'
        last: no
      , _.pick $location.search(), ['size', 'page', 'last']

      scope.$on 'objects:fetched', ->
        $location.search 'page', scope.page
        $location.search 'size', scope.size

        if scope.count? and scope.size?
          scope.pages = Math.ceil scope.count / scope.size
  ]

  .directive 'svPagingFilter', ->
    require: '^svPaging'
    templateUrl: 'svPaging/filter.html'
    replace: yes

  .directive 'svPagingRange', ['$location', ($location) ->
    require: '^?svPaging'
    templateUrl: 'svPaging/range.html'
    replace: yes
    link: (scope, elm, attrs, ctrl) ->
      tmp = _.mapValues _.pick($location.search(), ['from', 'to']), (dt) -> moment dt
      scope.range =
        from: tmp.from or moment().startOf 'month'
        to: (tmp.to or moment()).endOf 'day'
      scope.readonly = 'readonly' of attrs

      unless scope.readonly
        elm.daterangepicker
          format: 'YYYY-MM-DD'
          readonly: attrs.readonly
          opens: 'left'
          startDate: moment(scope.range.from)
          endDate: moment(scope.range.to)
          ranges:
            'Today': [moment(), moment()]
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')]
            'Last 7 Days': [moment().subtract(6, 'days'), moment()]
            'Last 30 Days': [moment().subtract(29, 'days'), moment()]
            'This Month': [moment().startOf('month'), moment().endOf('month')]
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        , (start, end) ->
          scope.$apply ->
            scope.range.from = start
            scope.range.to = end

          for own k, dt of scope.range
            $location.search k, moment(dt).format 'YYYY-MM-DD'
          ctrl?.fetch()

      scope.rangeqs = ->
        "from=#{scope.range.from.format 'YYYY-MM-DD'}&to=#{scope.range.to.format 'YYYY-MM-DD'}"

      elm.on '$destroy', -> elm.data('daterangepicker')?.remove()
  ]

  .directive 'svPagingExporter', ['$timeout', '$parse', ($timeout, $parse) ->
    require: '^svPaging'
    templateUrl: 'svPaging/exporter.html'
    link: (scope, elm, attrs, ctrl) ->
      state = undefined
      scope.isGenerating = -> state is 'generating'

      keys = (attrs.exportKeys or attrs.keys).split(/\s*,\s*/).map (key) ->
        if ~key.indexOf '='
          [key, val] = key.split /\s*=\s*/
          if ~val.indexOf '.'
            getter = (obj) ->
              path = val.split '.'
              return do inner = (nest = obj) ->
                return "" unless nest
                return inner nest[part] if part = path.shift()
                return nest
          else
            getter = (obj) -> obj[val]
        else
          getter = (obj) -> obj[key]

        header: key
        getter: getter

      link = elm.find 'a'
      elm.on 'click', (e) ->
        if state is 'generating'
          e.preventDefault()
          e.stopPropagation()
          return
        else if state is 'created'
          $timeout ->
            link.removeAttr 'href'
            state = null
          return

        $timeout ->
          state = 'generating'

          csv = 'data:application/csv;charset=utf-8,'
          ctrl._fetchAll (objects) ->
            if attrs.data
              objects = objects[attrs.data]

            row = $parse attrs.rows if attrs.rows?

            content = objects.map (obj) ->
              vals = if row?
                row scope, $event: e, row: obj
              else
                vals = []
                for val in keys.map((key) -> key.getter obj)
                  if _.isObject val
                    for own k, v of val
                      vals.push k
                      if _.isArray v
                        vals.push v.join ', '
                      else
                        vals.push v
                  else if /[\d-]*T[\d:\.]*Z/.test val
                    vals.push moment(val).format 'MMM D, YYYY h:mm a'
                  else
                    vals.push val
                vals

              vals = vals.map (v) ->
                unless v?
                  ""
                else if v.replace
                  "\"#{v.replace /"/g, '""'}\""
                else
                  "\"#{v}\""

              return vals.join(',') + '\n'

            content.unshift "\"#{_.pluck(keys, 'header').join '","'}\"\n"
            blob = new Blob content, type: 'application/csv'
            link.attr 'href', URL.createObjectURL blob

            $timeout ->
              state = 'created'
              link[0].click()
  ]
