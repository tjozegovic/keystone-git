(function() {
  var hasProp = {}.hasOwnProperty;

  angular.module('app').directive('svPaging', [
    '$injector', '$location', '$timeout', '$cookieStore', function($injector, $location, $timeout, $cookieStore) {
      return {
        controller: [
          '$scope', '$attrs', '$parse', function($scope, $attrs, $parse) {
            this.search = {
              keys: ['from', 'last', 'page', 'skip', 'size', 'to', 'whitelist', 'after']
            };
            if ($attrs.initialSize != null) {
              $scope.size = $attrs.initialSize;
            }
            $scope.filter = void 0;
            $scope.objects = [];
            if ($attrs.whitelist != null) {
              this.whitelist = $parse($attrs.whitelist);
            }
            this.resource = function() {
              return $scope.resource;
            };
            this.objects = function() {
              return $scope.objects;
            };
            $scope.$resource = $injector.get($attrs.svPaging);
            $scope.$loading = true;
            this._fetch = (function(_this) {
              return function(all, cb) {
                var count_query, get, i, key, len, query, range, ref, ref1;
                if (!cb) {
                  ref = [all, false], cb = ref[0], all = ref[1];
                }
                query = {};
                if (range = $scope.range) {
                  ref1 = ['from', 'to'];
                  for (i = 0, len = ref1.length; i < len; i++) {
                    key = ref1[i];
                    query[key] = moment(range[key]).toISOString();
                  }
                }
                _.defaults(query, _.omit($location.search(), 'page'));
                $scope.filtered = 0 < _.difference(Object.keys(query), _this.search.keys).length;
                get = function() {
                  var result;
                  query.size = $scope.size;
                  if ($scope.page != null) {
                    query.skip = $scope.size * ($scope.page - 1);
                  } else {
                    query.after = $scope.after;
                  }
                  if (_this.whitelist != null) {
                    query.whitelist = _this.whitelist($scope).join(',');
                  }
                  if (all) {
                    query = _.omit(query, 'page', 'size', 'skip', 'after');
                  }
                  return result = $scope.$resource.query(query, function(_, headers) {
                    return cb(result, headers);
                  });
                };
                if ((count_query = JSON.stringify(query)) === $scope.$filter) {
                  return get();
                }
                $scope.$filter = count_query;
                $scope.after = null;
                $scope.count = null;
                $scope.resource = $scope.objects = null;
                if (!_.isFunction($scope.$resource.count)) {
                  return get();
                }
                return $scope.$resource.count(query, function(_, headers) {
                  $scope.count = headers('X-Total-Count');
                  return get();
                }, function(error) {
                  return get();
                });
              };
            })(this);
            this._fetchAll = (function(_this) {
              return function(cb) {
                return _this._fetch(true, cb);
              };
            })(this);
            $scope.fetch = this.fetch = (function(_this) {
              return function() {
                var ref, start;
                ref = [Date.now(), true], start = ref[0], $scope.$loading = ref[1];
                return _this._fetch(false, function(results, headers) {
                  return $timeout(function() {
                    var i, len, r, ref1;
                    if (($scope.page != null) || ($scope.resource == null)) {
                      $scope.resource = $scope.objects = results;
                    } else {
                      for (i = 0, len = results.length; i < len; i++) {
                        r = results[i];
                        $scope.resource.push(r);
                      }
                    }
                    $scope.last = results.length < $scope.size;
                    $scope.after = (ref1 = $scope.objects[$scope.objects.length - 1]) != null ? ref1.id : void 0;
                    $scope.count = headers('X-Total-Count');
                    $scope.$loading = false;
                    return $scope.$broadcast('objects:fetched');
                  }, Math.max(0, 800 - (Date.now() - start)));
                });
              };
            })(this);
            $scope.reset = (function(_this) {
              return function() {
                var i, key, len, removekeys;
                removekeys = _.difference(Object.keys($location.search()), _this.search.keys);
                for (i = 0, len = removekeys.length; i < len; i++) {
                  key = removekeys[i];
                  $location.search(key, null);
                }
                return _this.fetch();
              };
            })(this);
            $scope.prev = (function(_this) {
              return function() {
                if ($scope.page != null) {
                  $scope.page = Math.max(1, --$scope.page);
                }
                return _this.fetch();
              };
            })(this);
            $scope.next = (function(_this) {
              return function() {
                if ($scope.$loading || $scope.last) {
                  return;
                }
                if ($scope.page != null) {
                  ++$scope.page;
                }
                return _this.fetch();
              };
            })(this);
            $scope.$on('objects:reload', (function(_this) {
              return function() {
                console.log('please reload the objects...');
                return _this.fetch();
              };
            })(this));
            $timeout(this.fetch);
            return this;
          }
        ]
      };
    }
  ]).directive('svPagingPager', [
    '$location', function($location) {
      return {
        require: '^svPaging',
        templateUrl: 'svPaging/pager.html',
        link: function(scope, elm, attrs, ctrl) {
          _.assign(scope, {
            size: '20',
            page: '1',
            last: false
          }, _.pick($location.search(), ['size', 'page', 'last']));
          return scope.$on('objects:fetched', function() {
            $location.search('page', scope.page);
            $location.search('size', scope.size);
            if ((scope.count != null) && (scope.size != null)) {
              return scope.pages = Math.ceil(scope.count / scope.size);
            }
          });
        }
      };
    }
  ]).directive('svPagingFilter', function() {
    return {
      require: '^svPaging',
      templateUrl: 'svPaging/filter.html',
      replace: true
    };
  }).directive('svPagingRange', [
    '$location', function($location) {
      return {
        require: '^?svPaging',
        templateUrl: 'svPaging/range.html',
        replace: true,
        link: function(scope, elm, attrs, ctrl) {
          var tmp;
          tmp = _.mapValues(_.pick($location.search(), ['from', 'to']), function(dt) {
            return moment(dt);
          });
          scope.range = {
            from: tmp.from || moment().startOf('month'),
            to: (tmp.to || moment()).endOf('day')
          };
          scope.readonly = 'readonly' in attrs;
          if (!scope.readonly) {
            elm.daterangepicker({
              format: 'YYYY-MM-DD',
              readonly: attrs.readonly,
              opens: 'left',
              startDate: moment(scope.range.from),
              endDate: moment(scope.range.to),
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              }
            }, function(start, end) {
              var dt, k, ref;
              scope.$apply(function() {
                scope.range.from = start;
                return scope.range.to = end;
              });
              ref = scope.range;
              for (k in ref) {
                if (!hasProp.call(ref, k)) continue;
                dt = ref[k];
                $location.search(k, moment(dt).format('YYYY-MM-DD'));
              }
              return ctrl != null ? ctrl.fetch() : void 0;
            });
          }
          scope.rangeqs = function() {
            return "from=" + (scope.range.from.format('YYYY-MM-DD')) + "&to=" + (scope.range.to.format('YYYY-MM-DD'));
          };
          return elm.on('$destroy', function() {
            var ref;
            return (ref = elm.data('daterangepicker')) != null ? ref.remove() : void 0;
          });
        }
      };
    }
  ]).directive('svPagingExporter', [
    '$timeout', '$parse', function($timeout, $parse) {
      return {
        require: '^svPaging',
        templateUrl: 'svPaging/exporter.html',
        link: function(scope, elm, attrs, ctrl) {
          var keys, link, state;
          state = void 0;
          scope.isGenerating = function() {
            return state === 'generating';
          };
          keys = (attrs.exportKeys || attrs.keys).split(/\s*,\s*/).map(function(key) {
            var getter, ref, val;
            if (~key.indexOf('=')) {
              ref = key.split(/\s*=\s*/), key = ref[0], val = ref[1];
              if (~val.indexOf('.')) {
                getter = function(obj) {
                  var inner, path;
                  path = val.split('.');
                  return (inner = function(nest) {
                    var part;
                    if (!nest) {
                      return "";
                    }
                    if (part = path.shift()) {
                      return inner(nest[part]);
                    }
                    return nest;
                  })(obj);
                };
              } else {
                getter = function(obj) {
                  return obj[val];
                };
              }
            } else {
              getter = function(obj) {
                return obj[key];
              };
            }
            return {
              header: key,
              getter: getter
            };
          });
          link = elm.find('a');
          return elm.on('click', function(e) {
            if (state === 'generating') {
              e.preventDefault();
              e.stopPropagation();
              return;
            } else if (state === 'created') {
              $timeout(function() {
                link.removeAttr('href');
                return state = null;
              });
              return;
            }
            return $timeout(function() {
              var csv;
              state = 'generating';
              csv = 'data:application/csv;charset=utf-8,';
              return ctrl._fetchAll(function(objects) {
                var blob, content, row;
                if (attrs.data) {
                  objects = objects[attrs.data];
                }
                if (attrs.rows != null) {
                  row = $parse(attrs.rows);
                }
                content = objects.map(function(obj) {
                  var k, v, val, vals;
                  vals = (function() {
                    var i, len, ref;
                    if (row != null) {
                      return row(scope, {
                        $event: e,
                        row: obj
                      });
                    } else {
                      vals = [];
                      ref = keys.map(function(key) {
                        return key.getter(obj);
                      });
                      for (i = 0, len = ref.length; i < len; i++) {
                        val = ref[i];
                        if (_.isObject(val)) {
                          for (k in val) {
                            if (!hasProp.call(val, k)) continue;
                            v = val[k];
                            vals.push(k);
                            if (_.isArray(v)) {
                              vals.push(v.join(', '));
                            } else {
                              vals.push(v);
                            }
                          }
                        } else if (/[\d-]*T[\d:\.]*Z/.test(val)) {
                          vals.push(moment(val).format('MMM D, YYYY h:mm a'));
                        } else {
                          vals.push(val);
                        }
                      }
                      return vals;
                    }
                  })();
                  vals = vals.map(function(v) {
                    if (v == null) {
                      return "";
                    } else if (v.replace) {
                      return "\"" + (v.replace(/"/g, '""')) + "\"";
                    } else {
                      return "\"" + v + "\"";
                    }
                  });
                  return vals.join(',') + '\n';
                });
                content.unshift("\"" + (_.pluck(keys, 'header').join('","')) + "\"\n");
                blob = new Blob(content, {
                  type: 'application/csv'
                });
                link.attr('href', URL.createObjectURL(blob));
                return $timeout(function() {
                  state = 'created';
                  return link[0].click();
                });
              });
            });
          });
        }
      };
    }
  ]);

}).call(this);
