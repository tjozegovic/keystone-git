angular.module('app')
  .directive 'fileModel', ->
    require: '?ngModel'
    link: (scope, elm, attrs, ngModel) ->
      elm.on 'change.bs.fileinput', ->
        scope.$apply ->
          ngModel.$setViewValue elm[0].files[0]

  .directive 'readfile', ->
    scope: readfile: '='
    link: (scope, elm, attrs) ->
      elm.on 'change.bs.fileinput', ->
        scope.$apply -> scope.readfile = elm[0].files[0]

  .directive 'autofocus', ['$timeout', ($timeout) ->
    restrict: 'A',
    link : ($scope, $element) ->
      $timeout -> $element[0].focus()
  ]

  .directive 'setClient', ->
    link: ($scope, $element, $attributes) ->
      $element.on 'click', ->
        form = $ '<form action="../setclient" method="post"/>'
        form.append $('<input type="hidden"/>').attr name: 'id', value: $attributes.setClient
        form.appendTo($element).submit()

  .directive 'previewHtml', ->
    require: 'ngModel'
    priority: 1
    link: (scope, elm, attr, ngModel) ->
      iframe = elm[0]
      scope.iframe = iframe.contentWindow or
        iframe.contentDocument.document or
        iframe.contentDocument

      ngModel.$render = ->
        if (ngModel.$modelValue or '').startsWith 'http'
          elm.attr 'src', ngModel.$modelValue
        else
          scope.iframe.document.open()
          scope.iframe.document.write ngModel.$modelValue
          scope.iframe.document.close()
