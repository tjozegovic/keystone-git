(function() {
  angular.module('app').directive('fileModel', function() {
    return {
      require: '?ngModel',
      link: function(scope, elm, attrs, ngModel) {
        return elm.on('change.bs.fileinput', function() {
          return scope.$apply(function() {
            return ngModel.$setViewValue(elm[0].files[0]);
          });
        });
      }
    };
  }).directive('readfile', function() {
    return {
      scope: {
        readfile: '='
      },
      link: function(scope, elm, attrs) {
        return elm.on('change.bs.fileinput', function() {
          return scope.$apply(function() {
            return scope.readfile = elm[0].files[0];
          });
        });
      }
    };
  }).directive('autofocus', [
    '$timeout', function($timeout) {
      return {
        restrict: 'A',
        link: function($scope, $element) {
          return $timeout(function() {
            return $element[0].focus();
          });
        }
      };
    }
  ]).directive('setClient', function() {
    return {
      link: function($scope, $element, $attributes) {
        return $element.on('click', function() {
          var form;
          form = $('<form action="../setclient" method="post"/>');
          form.append($('<input type="hidden"/>').attr({
            name: 'id',
            value: $attributes.setClient
          }));
          return form.appendTo($element).submit();
        });
      }
    };
  }).directive('previewHtml', function() {
    return {
      require: 'ngModel',
      priority: 1,
      link: function(scope, elm, attr, ngModel) {
        var iframe;
        iframe = elm[0];
        scope.iframe = iframe.contentWindow || iframe.contentDocument.document || iframe.contentDocument;
        return ngModel.$render = function() {
          if ((ngModel.$modelValue || '').startsWith('http')) {
            return elm.attr('src', ngModel.$modelValue);
          } else {
            scope.iframe.document.open();
            scope.iframe.document.write(ngModel.$modelValue);
            return scope.iframe.document.close();
          }
        };
      }
    };
  });

}).call(this);
