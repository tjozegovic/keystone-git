angular.module('app')
    .filter 'percentage', ['$filter', ($filter) ->
      (input, decimals = 2) ->
        if isNaN input
          "0.00%"
        else
          $filter('number')(input * 100, decimals) + '%'
    ]

  .filter 'humanizeComma', ->
    (num) ->
      Humanize.intComma(num)

  .filter 'alphanum', ->
    (str) ->
      str.replace /\W+/g, ''
