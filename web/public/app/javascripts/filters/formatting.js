(function() {
  angular.module('app').filter('percentage', [
    '$filter', function($filter) {
      return function(input, decimals) {
        if (decimals == null) {
          decimals = 2;
        }
        if (isNaN(input)) {
          return "0.00%";
        } else {
          return $filter('number')(input * 100, decimals) + '%';
        }
      };
    }
  ]).filter('humanizeComma', function() {
    return function(num) {
      return Humanize.intComma(num);
    };
  }).filter('alphanum', function() {
    return function(str) {
      return str.replace(/\W+/g, '');
    };
  });

}).call(this);
