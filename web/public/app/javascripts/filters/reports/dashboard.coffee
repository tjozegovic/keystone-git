angular.module('app')
  .filter 'humanizeDashboard', ->
    (num) ->
      if num > 99999
        Humanize.compactInteger(num, 0)
      else if num > 9999
        Humanize.compactInteger(num, 1)
      else if num > 999
        Humanize.intComma(num)
      else
        num

  .filter 'asCampaign', ->
    (statuses, obj) ->
      return '' unless statuses?
      status = statuses[obj.status.id || obj.status] or 'N/A'
      segment = status.segments[obj.segment.id || obj.segment] or 'N/A'
      step = segment.steps[obj.step.id || obj.step] or 'N/A'
      "#{status.name} / #{segment.name} / #{step.name}"
