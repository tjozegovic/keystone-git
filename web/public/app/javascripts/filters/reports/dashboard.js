(function() {
  angular.module('app').filter('humanizeDashboard', function() {
    return function(num) {
      if (num > 99999) {
        return Humanize.compactInteger(num, 0);
      } else if (num > 9999) {
        return Humanize.compactInteger(num, 1);
      } else if (num > 999) {
        return Humanize.intComma(num);
      } else {
        return num;
      }
    };
  }).filter('asCampaign', function() {
    return function(statuses, obj) {
      var segment, status, step;
      if (statuses == null) {
        return '';
      }
      status = statuses[obj.status.id || obj.status] || 'N/A';
      segment = status.segments[obj.segment.id || obj.segment] || 'N/A';
      step = segment.steps[obj.step.id || obj.step] || 'N/A';
      return status.name + " / " + segment.name + " / " + step.name;
    };
  });

}).call(this);
