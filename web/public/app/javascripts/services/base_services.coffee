angular.module('app')
  .factory 'httpRecoverInterceptor', [
    '$q', '$rootScope', 'growl',
    ($q, $rootScope, growl) ->
      response: (response) ->
        if ~response.config.url.indexOf '.html'
          $rootScope.route.notfound = no
        if response.config.data?.successMessage?
          growl.success response.config.data.successMessage
        response or $q.when response
      responseError: (rejection) ->
        if rejection.config.data.errorMessage?
          growl.danger rejection.config.data.errorMessage
          console.warn "#{config.url}\n#{status} #{statusText}#{message or ''}"
        else if rejection.status is 404 and ~rejection.config.url.indexOf '.html'
          rejection.status = 200
          $rootScope.route.notfound = yes
          return $q.when rejection
        else if rejection.status in [404, 500] and ~rejection.config.url.indexOf 'api/optouts/'
          rejection.status = 200
          return $q.when rejection
        else if rejection.status is 401
          location.reload()
        else if (_.some [/5\d{2}/g, /4\d{2}/g], (re) -> re.test rejection.status)
          if $rootScope.system.env in ['development', 'staging', 'dev'] && status != 500
            {data, config, status, statusText} = rejection
            message = ' - ' + (data.err?.description or data.err?.reason or data.err or JSON.stringify data) if data?
            console.warn "#{config.url}\n#{status} #{statusText}#{message or ''}"
            growl.danger width: 450, delay: 6000, "#{config.url}<br><b>#{status} #{statusText}</b>#{message or ''}"
          else
            growl.danger "Oops! Something has gone wrong. Click <a href='#' onclick='location.reload(true)'>here</a> to refresh the page."
            console.warn "#{config.url}\n#{status} #{statusText}#{message or ''}"

        $q.reject rejection
  ]

  .factory 'errorTimeout', ['$timeout', ($timeout) ->
    begin: (forms, start) ->
      $timeout ->
        forms.saving = no
        forms.submitted = yes
        forms.$setPristine()
      , Math.max 0,800 - (Date.now() - start)
  ]

  .factory 'checkLocation', [ ->
    id: (list, item, cb) ->
      location = _.findIndex list, id: item.id
      if location is -1
        list.push item
      else
        list[location] = item
      cb list, item
  ]


  .factory 'growl', ->
    defaults =
      align: 'center'
      ele: 'body'

    success: (opts, message) ->
      [message, opts] = [opts, message] unless message?
      $.bootstrapGrowl message, _.assign defaults, opts, type: 'success'
    danger: (opts, message) ->
      [message, opts] = [opts, message] unless message?
      $.bootstrapGrowl message, _.assign defaults, opts, type: 'danger'

  .service 'modalCheck', ['$uibModal', ($uibModal) ->
    dismiss: (modalInstance) ->
      inst = $uibModal.open
        templateUrl: 'dismissCheck-modal.html'
        backdrop: 'static'
        controller: 'ConfirmModal'

      inst.result.then ->
        modalInstance.dismiss()
  ]

  .service 'showLead', ['$uibModal', ($uibModal) ->
    open: (id) ->
      inst = $uibModal.open
        templateUrl: 'leadInfo-modal.html'
        controller: 'LeadInfo'
        resolve: id: -> id
        size: 'lg'
  ]

  # get related status and segment based off passed lead. place inside 'temp' scope, then open modal
  .service 'showAttribute', ['$http', '$rootScope', '$uibModal', ($http, $rootScope, $uibModal) ->
    open: (lead, statuses) ->
      openModal = (opts) ->
        $uibModal.open
          templateUrl: 'base/attribute-modal.html',
          scope: _.assign $rootScope.$new(), opts

      # TODO fix $status or status when most? leads are converted
      status = statuses[lead.$status or lead.status]
      segment = status.segments[lead.segment]
      activeSteps = _.filter status.segments[lead.segment].steps, (step) -> not step.active? or step.active
      steps = _.toArray activeSteps
      if steps.length
        for step in steps
          step.delay.display = moment(lead.segmented).add step.delay.value, step.delay.units
      else
        steps = [{name: 'No drips'}]

      if lead.$job
        $http.get '/api/lead/job/' + lead.$job
        .success (job) ->
          openModal job: job, lead: lead, status: status, segment: segment.name, steps: steps
        .error (err) ->
          growl.danger 'Failed to load job: ', err
      else
        openModal lead: lead, status: status, segment: segment.name, steps: steps
  ]

  .service 'dismissCheck', ['$uibModal', ($uibModal) ->
    dismiss: (modalInstance, options) ->
      inst = $uibModal.open
        templateUrl: 'dismissCheck-modal.html'
        backdrop: 'static'
        controller: 'ConfirmClose'
        resolve:
          refItem: -> options.ref
          scopeItem: -> options.item
          save: -> options.save

      inst.result.then (closeItem) ->
        modalInstance.close closeItem
  ]

  .service 'saveTimeout', ['$timeout', ($timeout) ->
    begin: (start, form) ->
      $timeout ->
        form.saving = no
        form.saved = yes
        form.$setPristine()
        form.submitted = yes
      , Math.max 0,800 - (Date.now() - start)
  ]



  .service 'fileUpload', ['$http', ($http) ->
    @upload = (file, url) ->
      fd = new FormData()
      fd.append 'file', file
      $http.post url, fd,
        transformRequest: angular.identity
        headers: 'Content-Type': undefined
    return
  ]
