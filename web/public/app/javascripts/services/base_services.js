(function() {
  angular.module('app').factory('httpRecoverInterceptor', [
    '$q', '$rootScope', 'growl', function($q, $rootScope, growl) {
      return {
        response: function(response) {
          var ref;
          if (~response.config.url.indexOf('.html')) {
            $rootScope.route.notfound = false;
          }
          if (((ref = response.config.data) != null ? ref.successMessage : void 0) != null) {
            growl.success(response.config.data.successMessage);
          }
          return response || $q.when(response);
        },
        responseError: function(rejection) {
          var config, data, message, ref, ref1, ref2, ref3, status, statusText;
          if (rejection.config.data.errorMessage != null) {
            growl.danger(rejection.config.data.errorMessage);
            console.warn(config.url + "\n" + status + " " + statusText + (message || ''));
          } else if (rejection.status === 404 && ~rejection.config.url.indexOf('.html')) {
            rejection.status = 200;
            $rootScope.route.notfound = true;
            return $q.when(rejection);
          } else if (((ref = rejection.status) === 404 || ref === 500) && ~rejection.config.url.indexOf('api/optouts/')) {
            rejection.status = 200;
            return $q.when(rejection);
          } else if (rejection.status === 401) {
            location.reload();
          } else if (_.some([/5\d{2}/g, /4\d{2}/g], function(re) {
            return re.test(rejection.status);
          })) {
            if (((ref1 = $rootScope.system.env) === 'development' || ref1 === 'staging' || ref1 === 'dev') && status !== 500) {
              data = rejection.data, config = rejection.config, status = rejection.status, statusText = rejection.statusText;
              if (data != null) {
                message = ' - ' + (((ref2 = data.err) != null ? ref2.description : void 0) || ((ref3 = data.err) != null ? ref3.reason : void 0) || data.err || JSON.stringify(data));
              }
              console.warn(config.url + "\n" + status + " " + statusText + (message || ''));
              growl.danger({
                width: 450,
                delay: 6000
              }, config.url + "<br><b>" + status + " " + statusText + "</b>" + (message || ''));
            } else {
              growl.danger("Oops! Something has gone wrong. Click <a href='#' onclick='location.reload(true)'>here</a> to refresh the page.");
              console.warn(config.url + "\n" + status + " " + statusText + (message || ''));
            }
          }
          return $q.reject(rejection);
        }
      };
    }
  ]).factory('errorTimeout', [
    '$timeout', function($timeout) {
      return {
        begin: function(forms, start) {
          return $timeout(function() {
            forms.saving = false;
            forms.submitted = true;
            return forms.$setPristine();
          }, Math.max(0, 800 - (Date.now() - start)));
        }
      };
    }
  ]).factory('checkLocation', [
    function() {
      return {
        id: function(list, item, cb) {
          var location;
          location = _.findIndex(list, {
            id: item.id
          });
          if (location === -1) {
            list.push(item);
          } else {
            list[location] = item;
          }
          return cb(list, item);
        }
      };
    }
  ]).factory('growl', function() {
    var defaults;
    defaults = {
      align: 'center',
      ele: 'body'
    };
    return {
      success: function(opts, message) {
        var ref;
        if (message == null) {
          ref = [opts, message], message = ref[0], opts = ref[1];
        }
        return $.bootstrapGrowl(message, _.assign(defaults, opts, {
          type: 'success'
        }));
      },
      danger: function(opts, message) {
        var ref;
        if (message == null) {
          ref = [opts, message], message = ref[0], opts = ref[1];
        }
        return $.bootstrapGrowl(message, _.assign(defaults, opts, {
          type: 'danger'
        }));
      }
    };
  }).service('modalCheck', [
    '$uibModal', function($uibModal) {
      return {
        dismiss: function(modalInstance) {
          var inst;
          inst = $uibModal.open({
            templateUrl: 'dismissCheck-modal.html',
            backdrop: 'static',
            controller: 'ConfirmModal'
          });
          return inst.result.then(function() {
            return modalInstance.dismiss();
          });
        }
      };
    }
  ]).service('showLead', [
    '$uibModal', function($uibModal) {
      return {
        open: function(id) {
          var inst;
          return inst = $uibModal.open({
            templateUrl: 'leadInfo-modal.html',
            controller: 'LeadInfo',
            resolve: {
              id: function() {
                return id;
              }
            },
            size: 'lg'
          });
        }
      };
    }
  ]).service('showAttribute', [
    '$http', '$rootScope', '$uibModal', function($http, $rootScope, $uibModal) {
      return {
        open: function(lead, statuses) {
          var activeSteps, i, len, openModal, segment, status, step, steps;
          openModal = function(opts) {
            return $uibModal.open({
              templateUrl: 'base/attribute-modal.html',
              scope: _.assign($rootScope.$new(), opts)
            });
          };
          status = statuses[lead.$status || lead.status];
          segment = status.segments[lead.segment];
          activeSteps = _.filter(status.segments[lead.segment].steps, function(step) {
            return (step.active == null) || step.active;
          });
          steps = _.toArray(activeSteps);
          if (steps.length) {
            for (i = 0, len = steps.length; i < len; i++) {
              step = steps[i];
              step.delay.display = moment(lead.segmented).add(step.delay.value, step.delay.units);
            }
          } else {
            steps = [
              {
                name: 'No drips'
              }
            ];
          }
          if (lead.$job) {
            return $http.get('/api/lead/job/' + lead.$job).success(function(job) {
              return openModal({
                job: job,
                lead: lead,
                status: status,
                segment: segment.name,
                steps: steps
              });
            }).error(function(err) {
              return growl.danger('Failed to load job: ', err);
            });
          } else {
            return openModal({
              lead: lead,
              status: status,
              segment: segment.name,
              steps: steps
            });
          }
        }
      };
    }
  ]).service('dismissCheck', [
    '$uibModal', function($uibModal) {
      return {
        dismiss: function(modalInstance, options) {
          var inst;
          inst = $uibModal.open({
            templateUrl: 'dismissCheck-modal.html',
            backdrop: 'static',
            controller: 'ConfirmClose',
            resolve: {
              refItem: function() {
                return options.ref;
              },
              scopeItem: function() {
                return options.item;
              },
              save: function() {
                return options.save;
              }
            }
          });
          return inst.result.then(function(closeItem) {
            return modalInstance.close(closeItem);
          });
        }
      };
    }
  ]).service('saveTimeout', [
    '$timeout', function($timeout) {
      return {
        begin: function(start, form) {
          return $timeout(function() {
            form.saving = false;
            form.saved = true;
            form.$setPristine();
            return form.submitted = true;
          }, Math.max(0, 800 - (Date.now() - start)));
        }
      };
    }
  ]).service('fileUpload', [
    '$http', function($http) {
      this.upload = function(file, url) {
        var fd;
        fd = new FormData();
        fd.append('file', file);
        return $http.post(url, fd, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': void 0
          }
        });
      };
    }
  ]);

}).call(this);
