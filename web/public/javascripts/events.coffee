$ ->
  add = (evt, args...) ->
    args = [moment(evt.dt).calendar()].concat args
    a = $('<a/>', href: "/events/#{evt.id}").text args.join ' '
    $('#events').prepend $('<li/>').append a

  socket = io.connect()
  socket.on 'event', (event) ->
    switch event.type
      when 'actionqueue.poll'
        $('#heartbeat').html moment(event.at).calendar()
      when 'email.queued', 'email.delivered'
        mail = event.data.mail
        add event, event.type
          , 'from:', mail.from
          , 'to:', mail.to
          , 'message:', event.data.message
      when 'opporunity.segmented'
        opporunity = event.data.opporunity
        filter = event.data.filter
        add event, event.type
          , 'opporunity:', opporunity.recipient.email
          , 'segment:', filter.segment
