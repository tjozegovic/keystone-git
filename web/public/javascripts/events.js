(function() {
  var slice = [].slice;

  $(function() {
    var add, socket;
    add = function() {
      var a, args, evt;
      evt = arguments[0], args = 2 <= arguments.length ? slice.call(arguments, 1) : [];
      args = [moment(evt.dt).calendar()].concat(args);
      a = $('<a/>', {
        href: "/events/" + evt.id
      }).text(args.join(' '));
      return $('#events').prepend($('<li/>').append(a));
    };
    socket = io.connect();
    return socket.on('event', function(event) {
      var filter, mail, opporunity;
      switch (event.type) {
        case 'actionqueue.poll':
          return $('#heartbeat').html(moment(event.at).calendar());
        case 'email.queued':
        case 'email.delivered':
          mail = event.data.mail;
          return add(event, event.type, 'from:', mail.from, 'to:', mail.to, 'message:', event.data.message);
        case 'opporunity.segmented':
          opporunity = event.data.opporunity;
          filter = event.data.filter;
          return add(event, event.type, 'opporunity:', opporunity.recipient.email, 'segment:', filter.segment);
      }
    });
  });

}).call(this);
