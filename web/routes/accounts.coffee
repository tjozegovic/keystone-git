_ = require 'lodash'
express = require 'express'

module.exports = router = express.Router()

ALLOWED_ACCOUNT_VARS =
  dailyvu: {}
  notifications: {}
  timezone: null
  default_sender: null
  privacy_url: null

router.get '/api/integration', (req, res, err) ->
  apikeys = req.db.use 'core/apikeys'
  apikeys.byclient req.session.client_id, (err, keys) ->
    return next err if err
    res.json _.assign
      apikeys: _.reject keys, 'revoked'
      integration: req._client.integrations?._current
      webhooks: req._client.webhooks

router.get '/api/account/settings', (req, res) ->
  res.json _.defaults _.pick(req._client, _.keys ALLOWED_ACCOUNT_VARS), ALLOWED_ACCOUNT_VARS

router.put '/api/account/settings', (req, res, next) ->
  clients = req.db.use 'core/clients'
  clients.get req.session.client_id, (err, client) ->
    return next err if err

    _.assign client, _.pick(req.body, _.keys ALLOWED_ACCOUNT_VARS)
    clients.save client, (err) ->
      return next err if err
      res.json ok: yes

router.get '/api/account/template', (req, res) ->
  templates = req.db.use 'core/templates'
  templates.get_by_client req.session.client_id, (err, body) ->
    res.json body

router.post '/api/account/template', (req, res) ->
  templates = req.db.use 'core/templates'
  templates.get_by_client req.session.client_id, (err, result) ->
    tmpl = req.body
    tmpl.id ?= result?.id
    tmpl.version ?= result?.version
    tmpl.created ?= result?.created
    templates.save_client_template tmpl, req.session.client_id, (err, result) ->
      if err then res.status(500).json ok: no, err: err else res.json ok: yes

router.put '/api/webhooks', (req, res, next) ->
  clients = req.db.use 'core/clients'
  clients.get req.session.client_id, (err, client) ->
    return next err if err

    _.assign client, webhooks: req.body
    clients.save client, (err) ->
      return next err if err
      res.json ok: yes

router.post '/api/apikeys/create', (req, res) ->
  keys = req.db.use 'core/apikeys'
  keys.create req.session.client_id, (err, body) ->
    return next err if err
    res.json body
