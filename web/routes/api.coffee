config = require 'config'

_ = require 'lodash'
crypto = require 'crypto'
express = require 'express'
rjs = require 'rabbit.js'
uuid = require 'uuid'

events = require 'shared/eventlib'
attributemapper = require 'shared/attributemapper'
segmenter = require 'shared/segmenter'

module.exports = router = express.Router()

ctx = rjs.createContext config.queues.url
ctx.once 'ready', ->

router.post '/api/send', (req, res, next) ->
  if message = req.body
    mail = _.defaults message,
      client: message.client || req.session.client_id
      sample: yes
      recipient:
        email: message.to
      mail: message.data

    pub = ctx.socket 'PUSH'
    pub.connect 'email', ->
      pub.write JSON.stringify mail
      res.json mail: mail
  else
    next new Error 'No message found.'

router.post '/api/lead', (req, res, next) ->
  [statuses, attributes, leads] = req.db.use 'client/statuses', 'client/attributes', 'client/leads'

  statuses.getByName req.body.status, (err, body) ->
    #req.db.data.view 'statuses', 'aliases', key: req.body.status, include_docs: yes, (err, body) ->
    if err
      return next err
    else unless status = body[0]
      return next new Error "Status '#{req.body.status}' is not a valid status name."

    _upsert = (type, lead, cb) ->
      lead.testing = yes

      leads.save lead, (err, body) ->
        return next err if err

        lead.id = body.id
        attributes.all (err, attribList) ->
          return next err if err

          try
            path = attributemapper.mapPath lead.attributes, attribList
            segment = segmenter.segment path, status.segments

            events.emit type, lead.client, lead: lead.id
            res.json id: lead.id
            cb?()
          catch e
            next e

    {external} = req.body
    leads.getByExternalId external.source, external.id, (err, body) ->
      return next err if err
      lead = body

      if lead?.length
        oldstatus = lead.$status or lead.status
        _upsert 'lead.updated', _.assign(lead, req.body, $status: status.id, updated: new Date), ->
          unless oldstatus is status.id
            events.emit 'lead.status.changed', req.session.client_id, lead: lead.id, status: status.id
      else
        _upsert 'lead.created', _.assign req.body,
          type: 'lead'
          client: req.session.client_id
          $status: status.id
          created: new Date
