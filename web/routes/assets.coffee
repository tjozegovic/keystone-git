express = require 'express'
_ = require 'lodash'
knox = require 'knox'

module.exports = router = express.Router()

# TODO this is pretty ugly, not sure i'm a fan, but also not sure how to handle it otherwise
# the coded templates show a preview through an iframe with the HTML, should the assets variable
# be replaced there? maybe the templates should actually send the html to the server to get a
# thumbnail back, allowing it to go through the same code as the template save stuff, using phantomjs?
module.exports.get_asset = get_asset = (req, res) ->
  unless req._client.credentials
    console.error 'could not find AWS credentials for asset request',
      client: req._client?.id
      asset: req.params.asset
    return res.status(404).end 'Not found'

  request = require 'request'
  request("https://#{req._client.credentials.aws.bucket}.s3.amazonaws.com/#{req.params.asset}").pipe res

# TODO idk why IIS forwards {{ and }}, where the express server uses the entities...
router.get '/manage/templates/%7B%7Bassets.baseurl%7D%7D/:asset*', get_asset
router.get '/manage/templates/{{assets.baseurl}}/:asset*', get_asset
router.get '/%7B%7Bassets.baseurl%7D%7D/:asset*', get_asset
router.get '/{{assets.baseurl}}/:asset*', get_asset

router.get '/api/assets/:type', (req, res) ->
  unless req._client.credentials?.aws
    return res.status(500).json 'AWS bucket not found. Please contact your administrator.'
  creds = req._client.credentials.aws
  if req.params.type is 'private'
    unless req._client.credentials?.aws.private?
      return res.status(500).json 'AWS private folder not found. Please contact your administrator.'
    creds.bucket = req._client.credentials.aws.private.bucket
    prefix = req._client.credentials.aws.private.folder + '/'
  client = knox.createClient creds
  client.list prefix: prefix, (err, body) ->
    return res.status(500).json err: err if err?
    if body?.Code is 'AccessDenied'
      return res.status(500).json err: body.Code
    res.json bucket: creds.bucket, contents: body.Contents

router.get '/api/check/assets', (req, res) ->
  if req._client.credentials?.aws?.private?
    return res.json check: yes
  else
    return res.json check: no
