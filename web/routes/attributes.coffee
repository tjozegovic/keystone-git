_ = require 'lodash'
async = require 'async'
csv = require 'csv'
busboy = require 'connect-busboy'
express = require 'express'
util = require 'util'
uuid = require 'uuid'

module.exports = router = express.Router()

# create template for csv files
setfields = (req, res, next) ->
  req.csvtemplate =
    fields: [
      'value'
      'aliases'
    ]
  next()

# blank csv file download
router.get '/values/template/download', setfields, (req, res) ->
  res.attachment 'SoftVu Bulk Values Upload.csv'
  res.send new Buffer req.csvtemplate.fields.join(',') + '\n'

# poplulated csv file download
router.get '/api/attributes/:id/download', setfields, (req, res) ->
  res.attachment 'SoftVu Bulk Values Upload.csv'
  attributes = req.db.use 'client/attributes'

  attributes.active (err, body) ->
    if err
     console.log 'Error getting attributes.active: ', err
    else
      attrib = _.find body, id: req.params.id

      csv()
      .from.array attrib.values
      .to.stream res, header: yes, columns: req.csvtemplate.fields
      .transform (row, index) ->
        row.value = row.name or ''

        row.aliases = _(row.aliases or '')
          .join ' | '
        return row

# upload data, from file, to database.
router.post '/api/attributes/:id/upload', busboy(immediate: yes), (req, res) ->
  return next() unless req.busboy

  req.busboy.on 'file', (name, file, filename, encoding, mimetype) ->
    return unless name is 'file'

    reader = csv().from file, columns: yes, relax: yes

    records = []
    reader
      .on 'record', (record) ->
        records.push record
      .on 'error', (err) ->
        res.status(500).json err: err
      .on 'end', (rows) ->

        attributes = req.db.use 'client/attributes'

        attributes.active (err, body) ->
          if err
           console.log 'Error getting attributes.active: ', err
          else
            attrib = _.find body, id: req.params.id

            stats = created: 0, updated: 0

            # for each record, check if there is already an existing value. if so, put new data there. if not, create new value.
            for record in records

              aliases = if record.aliases.trim() is ''
                []
              else
                record.aliases.split(/\s*\|\s*/g)

              if old = _.find(attrib.values, name: record.value)
                stats.updated++

                location = attrib.values.indexOf old
                if attribAliases = attrib.values[location].aliases
                  for alias in aliases
                    attribAliases.push alias
                  aliases = _.uniq attribAliases
                attrib.values[location] = { id: attrib.values[location].id, name: attrib.values[location].name, aliases: aliases}
              else
                stats.created++

                valueId = uuid.v4().replace /-/g, ''
                attrib.values.push {id: valueId, name: record.value, aliases: aliases}

            attributes.save attrib, ->

              return res.status(500).json err: err if err
              res.json stats
