express = require 'express'

module.exports = router = express.Router()

router.get '/url', (req, res) ->
  knox = require 'knox'
  s3client = knox.createClient req._client.credentials.aws

  in30min = new Date()
  in30min.setMinutes in30min.getMinutes() + 30

  res.json url: s3client.signedUrl req.query.file, in30min,
    verb: 'PUT', contentType: req.query.type, qs: 'x-amz-acl': 'public-read'

router.get '/policy', (req, res) ->
  crypto = require 'crypto'
  creds = req._client.credentials.aws

  in30min = new Date()
  in30min.setMinutes in30min.getMinutes() + 30

  s3Policy =
    expiration: in30min
    conditions: [
      ['starts-with', '$key', '']
      {'bucket': creds.private.bucket}
      {'acl': 'public-read'}
      ['starts-with', '$Content-Type', req.query.type]
      {'success_action_status': '201'}
    ]

  # stringify and encode the policy
  stringPolicy = JSON.stringify s3Policy
  base64Policy = new Buffer(stringPolicy, 'utf-8').toString 'base64'

  # sign the base64 encoded policy
  signature = crypto.createHmac('sha1', creds.secret)
    .update(new Buffer base64Policy, 'utf-8')
    .digest 'base64'

  # build the results object
  res.json
    private: creds.private
    bucket: creds.private.bucket
    Policy: base64Policy
    Signature: signature
    AWSAccessKeyId: creds.key
