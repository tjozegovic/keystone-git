_ = require 'lodash'
async = require 'async'
csv = require 'csv'
busboy = require 'connect-busboy'
express = require 'express'

module.exports = router = express.Router()

# create template for csv files
setfields = (req, res, next) ->
  req.csvtemplate =
    fields: [
      'agent'
      'recipient.email'
    ]
  next()

router.all '/api/batches*', (req, res, next) ->
  res.cbjson = (err, body) ->
    if err then res.status(500).json err else res.json body

  res.eventjson = (event, extra = {}) -> (err, body) ->
    if err
      res.status(500).json err
    else
      res.locals.events.publish event, _.defaults extra, batch: req.params.id if event
      res.json body

  req.batches = req.db.use 'client/batches'
  next()

router.all '/api/batches*', busboy(immediate: yes), (req, res, next) ->
  return next() unless req.busboy

  req.busboy.on 'field', (name, val) ->
    return unless name is 'data'
    req.batch = JSON.parse val

  req.busboy.on 'finish', ->
    next()

router.get '/batches/:id/download', (req, res, next) ->
  {id} = req.params
  batches = req.db.use 'client/batches'
  batches.get id, with_csv: yes, (err, att) ->
    return next err if err
    res.attachment 'batch.csv'
    res.send att.file

router.get '/batches/:tempId', setfields, (req, res) ->
  {tempId} = req.params
  templates = req.db.use 'client/templates'
  templates.get tempId, (err, template) ->
    return res.status(404).send 'Not Found' if err

    variables = template.meta.html.variables
    # TODO might be able to remove this when the variables array gets set up properly
    for item in variables
      unless ~item.name.indexOf('agent') or ~item.name.indexOf('sender') or ~item.name.indexOf('assets')
        req.csvtemplate.fields.push item.name
    req.csvtemplate.fields = _.uniq req.csvtemplate.fields

    res.attachment 'SoftVu Batch File Upload.csv'
    res.send new Buffer req.csvtemplate.fields.join(',') + '\n'

router.get '/api/batches/report/:id', (req, res) ->
  {id} = req.params
  summaries = req.db.use 'client/summaries'
  summaries.batches_by_id id, res.cbjson

router.get '/api/batches/errors/:id', (req, res) ->
  {id} = req.params
  emails = req.db.use 'client/emails'
  emails.by_batch_errors id, res.cbjson

router.get '/api/batches/:id/:template/errors', setfields, (req, res) ->
  {id: email_id, template: temp_id} = req.params
  [emails, templates] = req.db.use 'client/emails', 'client/templates'
  res.attachment 'SoftVu Batch Errors.csv'

  async.parallel
    errors: (cb) -> emails.by_batch_errors email_id, cb
    template: (cb) -> templates.get temp_id, cb
  , (err, {errors, template}) ->
    custom_columns = []

    for item in template.meta.html.variables
      unless ~item.name.indexOf('agent') or ~item.name.indexOf('sender') or ~item.name.indexOf('assets')
        req.csvtemplate.fields.push item.name
        custom_columns.push item.name

    req.csvtemplate.fields = _.uniq req.csvtemplate.fields
    req.csvtemplate.fields.push 'error', 'email_id'

    csv()
    .from.array errors
    .to.stream res, header: yes, columns: req.csvtemplate.fields
    .transform (row, index) ->
      row.agent = if row.agent then row.agent else ''
      for item in custom_columns
        row[item] = if x = _.result row, item then x else ''
      row['recipient.email'] = if row.recipient.email then row.recipient.email else ''
      if row.errored?[0].error
        row.error = row.errored[0].error.message || row.errored[0].stack[0..50].trim()
      else {}
      row['email_id'] = row.id
      row

router.post '/api/batches/:id/deploy', (req, res, next) ->
  {id} = req.params
  req.batches.get id, (err, batch) ->
    return next err if err
    batch.status = 'deploying'
    req.batches.update id, batch, res.eventjson 'batch.deployed', schedule: batch.schedule

router.post '/api/batches/:id/clone', (req, res, next) ->
  {id} = req.params
  req.batches.clone id, (err, body) ->
    return next err if err
    res.eventjson('batch.cloned', batch: body.id, parent: id) err, body

router.post '/api/batches/:id/cancel', (req, res, next) ->
  {id} = req.params
  req.batches.get id, (err, batch) ->
    return next err if err
    batch.status = 'canceling'
    req.batches.update id, batch, res.eventjson 'batch.canceled'

router.post '/api/batches', (req, res) ->
  req.batches.save req.body, res.cbjson

router.put '/api/batches/:id/upload', (req, res) ->
  req.batches.upload req.params.id, req.csv, res.cbjson

router.put '/api/batches/:id', (req, res) ->
  req.batches.update req.params.id, req.body, res.cbjson
