_ = require 'lodash'
async = require 'async'
express = require 'express'
pluralize = require 'pluralize'
{stringify} = require 'JSONStream'

module.exports = router = express.Router()

#  the routes below access the data/* modules which look and act like repositories
#  they are setup for very common collections that are used in many different places
#  and they are rooted to the account that is defined in the session
router.param 'collection', (req, res, next, collection) ->
  unless req.collection = req.db.use 'client/' + collection
    return next new Error "\"client/#{collection}\" is not a valid collection."

  next()

router.get '/api/:collection', (req, res) ->
  method = if req.query['q'] of req.collection
      req.query['q']
    else if 'query' of req.collection
      'query'
    else
      'all'
  fn = req.collection[method].bind req.collection

  send = (err, body, count) ->
    if err?
      console.error 'error in collection method', err
      return res.status(500).json err: err.message or err

    res.setHeader 'X-Total-Count', count if count?

    if _.isFunction body?.pipe
      body.pipe(stringify()).pipe(res)
    else if _.isArray(body) and body.length
      first = yes
      async.eachSeries body, (i, done) ->
        res.write if first then first = no; '[' else ','
        res.write JSON.stringify i
        done()
      , -> res.end ']'
    else
      res.json body

  if req.method is 'HEAD'
    req.query.count_only = yes

  try
    if fn.length is 2
      fn req.query, send
    else
      fn send
  catch e
    console.error e.message or e
    res.status(500).json err: reason: e.message or e

# TODO when error handling in the data repositories is fixed
# these routes need to handle the errors better
router.get '/api/:collection/:id', (req, res, next) ->
  if req.params.id of req.collection
    return next new Error 'deprecated: using /:method on a collection is not allowed. try ?q=method'

  try
    req.collection.get req.params.id, (err, body) ->
      if err?
        console.error 'collection get', err
        return res.status(500).json err: reason: err?.message or err

      return res.status(404).send 'Not Found' unless body?
      res.json body
  catch e
    res.status(500).json err: reason: e.message or e

_publish_route = (type, fn) ->
  (req, res) ->
    fn req, (getid) ->
      (err, body) ->
        if err
          message = err.message or err
          return res.status(500).end() if ~message.indexOf 'duplicate key'
          return res.status(500).send message

        name = req.collection.type or pluralize.singular req.params.collection

        evt = {}
        evt[name] = getid body
        evt.request = _.pick req, ['headers', 'query', 'originalUrl', 'ip', 'session']

        res.json body
        res.locals.events.publish "#{name}.#{type}", evt

router.post '/api/:collection', _publish_route 'created', (req, next) ->
  req.collection.save req.body, next (body) -> body.id

router.put '/api/:collection/:id', _publish_route 'updated', (req, next) ->
  req.collection.update req.params.id, req.body, next -> req.params.id

router.delete '/api/:collection/:id', _publish_route 'deleted', (req, next) ->
  req.collection.delete req.params.id, next -> req.params.id
