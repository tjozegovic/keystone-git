_ = require 'lodash'
attributemapper = require 'shared/attributemapper'
events = require 'shared/eventlib'
express = require 'express'
segmenter = require 'shared/segmenter'

module.exports = router = express.Router()

router.get '/lead/info/:id', (req, res) ->
  leads = req.db.use 'client/leads'
  leads.get req.params.id, (err, lead) ->
    return res.status(500).json err: err if err?
    res.json lead

router.get '/api/contacts/settings', (req, res) ->
  res.json req._client.contacts or []

router.put '/api/contacts/settings', (req, res) ->
  client = req.db.use 'core/clients'
  client.save _.assign(req._client, contacts: req.body), -> res.json ok: yes

router.put '/api/check/attribute', (req, res) ->
  [statuses, attributes] = req.db.use 'client/statuses', 'client/attributes'
  {attributes: attribs, status: status} = req.body
  statuses.by_alias status, (err, body) ->
    return res.status(500).json err: err if err?
    unless status = body
      return res.status(500).json err: 'No status found'
    attributes.all (err, attribList) ->
      return res.status(500).json err: err if err?

      try
        path = attributemapper.mapPath attribs, attribList
        segment = segmenter.segment path, status.segments

        res.json result: yes
      catch e
        res.json result: e.message

router.post '/api/leads/', (req, res, next) ->
  [statuses, attributes, leads] = req.db.use 'client/statuses', 'client/attributes', 'client/leads'

  statuses.by_alias req.body.status, (err, body) ->
    #req.db.data.view 'statuses', 'aliases', key: req.body.status, include_docs: yes, (err, body) ->
    if err
      return next err
    else unless status = body
      return next new Error "Status '#{req.body.status}' is not a valid status name."

    _upsert = (type, lead, cb) ->
      leads.save lead, (err, body) ->
        return next err if err

        _.assign lead, body
        attributes.all (err, attribList) ->
          return next err if err

          try
            path = attributemapper.mapPath lead.attributes, attribList
            segment = segmenter.segment path, status.segments

            events.emit type, lead.client, lead: lead.id
            res.json lead
            cb?()
          catch e
            next e

    _upsert 'lead.created', _.assign req.body,
      type: 'lead'
      client: req.session.client_id
      $status: status.id
      created: new Date

router.put '/api/leads/:id', (req, res, next) ->
  [statuses, attributes, leads] = req.db.use 'client/statuses', 'client/attributes', 'client/leads'

  statuses.by_alias req.body.status, (err, body) ->
    #req.db.data.view 'statuses', 'aliases', key: req.body.status, include_docs: yes, (err, body) ->
    if err
      return next err
    else unless status = body
      return next new Error "Status '#{req.body.status}' is not a valid status name."

    _upsert = (type, lead, cb) ->
      leads.update req.params.id, lead, (err, body) ->
        return next err if err

        _.assign lead, body
        attributes.all (err, attribList) ->
          return next err if err

          try
            path = attributemapper.mapPath lead.attributes, attribList
            segment = segmenter.segment path, status.segments

            events.emit type, lead.client, lead: lead.id
            res.json lead
            cb?()
          catch e
            next e

    lead = req.body
    oldstatus = lead.$status or lead.status
    _upsert 'lead.updated', _.assign(lead, req.body, $status: status.id, updated: new Date), ->
      unless oldstatus is status.id
        events.emit 'lead.status.changed', req.session.client_id, lead: lead.id, status: status.id
