path = require 'path'

_ = require 'lodash'
express = require 'express'
request = require 'request'

module.exports = router = express.Router()

router.get '/demo/form', (req, res) ->
  res.sendFile path.resolve __dirname + '/../views/demo/form.html'

router.get '/demo/thankyou', (req, res) ->
  res.sendFile path.resolve __dirname + '/../views/demo/thankyou.html'

router.post '/demo/form', (req, res) ->
  console.log 'xml sent to velocify', body = """
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <AddLeads xmlns="https://service.leads360.com">
      <username>jeff.waymire@softvu.com</username>
      <password>111111</password>
      <leads>
        <Leads>
          <Lead>
            <Status StatusId="1"/>
            <Campaign CampaignId="3"/>
            <Agent AgentName="dani.quick@softvu.com"/>
            <Fields>
              <Field FieldId="2" Value="#{req.body.firstname}"/>
              <Field FieldId="3" Value="#{req.body.lastname}"/>
              <Field FieldId="4" Value="#{req.body.email}"/>
              <Field FieldId="9" Value="#{req.body.address}"/>
              <Field FieldId="10" Value="#{req.body.city}"/>
              <Field FieldId="11" Value="#{req.body.state}"/>
              <Field FieldId="12" Value="#{req.body.zip}"/>
              <Field FieldId="27" Value="#{req.body.phone}"/>
              <Field FieldId="30" Value="#{req.body.bill}"/>
              <Field FieldId="35" Value="#{req.body.property}"/>
            </Fields>
          </Lead>
        </Leads>
      </leads>
    </AddLeads>
  </soap:Body>
</soap:Envelope>
"""

  request
    method: 'POST'
    url: 'https://service.leads360.com/ClientService.asmx'
    headers:
      'Content-Type': 'text/xml; charset=utf-8'
      'SOAPAction': '"https://service.leads360.com/AddLeads"'
    body: body
  , (err, response, body) ->
    console.error 'error returned from velocify', err if err # fuck... hope this isn't an issue during the demo
    console.log 'velocify response from addlead', body if body
    res.redirect '/demo/thankyou'
