uuid = require 'uuid'

module.exports =
  index: (req, res) ->
    res.render 'email'

  post: (req, res) ->
    if mail = req.body?.mail
      db = (require 'nano')('http://172.16.1.54:5984/jobs')

      at = new Date
      at = at.toJSON()

      id = uuid.v4()
      db.insert
        id: id
        at: at
        status: 'pending'
        action: 'email'
        data:
          client: 'a'
          mail: mail
      , "#{id}", (err, result) ->
        console.log at, result
        res.redirect 'back'
    else
      res.render 'email', error: true
