_ = require 'lodash'
express = require 'express'
S = require 'shared/util/string'
{correctids, nocache} = require '../middleware'

module.exports = router = express.Router()

# all events that save any fields from the req object should use this _.pick list
REQUEST_FIELDS = ['headers', 'query', 'originalUrl', 'ip']

router.all '/:type(click|pb|view|unsub)/:one([a-zA-Z0-9]{6,32})/:two([a-zA-Z0-9]{6,32})?/:three([a-zA-Z0-9]{6,32})?', (req, res, next) ->
  if req._iscname
    _.assign req.locals,
      client: req._client.id
      email: req.params.one
      link: req.params.two
  else
    _.assign req.locals,
      client: req.params.one
      email: req.params.two
      link: req.params.three

  req.db.client = req.locals.client
  next()

router.get '/click/*', (req, res) ->
  {client, email, link} = req.locals
  emails = req.db.use 'client/emails'

  emails.get email, (err, body) ->
    return res.status(404).end 'Not Found' if err? or not body?

    link = _.find body.links, id: link if body?
    if link.original_url?.length
      # ensure that redirects are absolute
      link.original_url = 'http://' + link.original_url unless /^https?:\/\/.*$/.test link.original_url

      res.redirect link.original_url
    else
      # TODO should raise/emit an error
      res.status(404).end 'Not Found'

    res.locals.events.emit 'email.clicked', client,
      email: email
      link: link
      redirect: link.original_url
      request: _.pick req, REQUEST_FIELDS

# web pixel/bug
router.get '/pb/*', nocache, (req, res) ->
  res.set 'Content-Type': 'image/gif'
  res.status(200).send new Buffer 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7=', 'base64'

  {client, email} = req.locals
  if client and email and not req.user
    res.locals.events.emit 'email.opened', client,
      email: email
      request: _.pick req, REQUEST_FIELDS

router.get '/view/*', nocache, (req, res) ->
  {client, email} = req.locals

  req.db.client = client
  emails = req.db.use 'client/emails'

  emails.get email, (err, email) ->
    return res.status(404).end 'Not found' unless email

    # res.locals.events.emit 'email.viewed', client,
    #   email: email
    #   request: _.pick req, REQUEST_FIELDS
    if S(email.mail.html or '').startsWith 'http'
      res.redirect email.mail.html
    else
      res.send email.mail.html

router.get '/unsub/*', (req, res) ->
  {client, email} = req.locals

  req.db.client = client
  emails = req.db.use 'client/emails'

  emails.get email, (err, email) ->
    unless email
      req.session.client = client
      return res.render 'unsubscribe'

    optouts = req.db.use 'core/optouts'
    req.session.email = email_address = email.recipient.email
    optouts.optout email_address, client, (err) ->
      return res.render 'unsubscribe', email: email_address if err

      res.locals.events.emit 'optout', client,
        from: 'unsub'
        email: email.id
        address: email_address
        request: _.pick req, REQUEST_FIELDS

      req.session.email = email_address
      res.redirect '/unsubscribed'

router.get '/unsubscribe', (req, res) ->
  res.render 'unsubscribe'

router.post '/unsubscribe', (req, res) ->
  {client} = req.session
  {email} = req.body

  # TODO do we need to make sure the client exists?
  optouts = req.db.use 'core/optouts'
  optouts.optout email, client, (err) ->
    res.locals.events.emit 'optout', client,
      from: 'unsub - manual'
      address: email
      request: _.pick req, REQUEST_FIELDS

    req.session.email = email
    res.redirect '/unsubscribed'

router.get '/unsubscribed', (req, res) ->
  res.render 'unsubscribed', email: req.session.email
