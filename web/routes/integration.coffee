module.exports =
  index: (req, res) ->
    integration = req.db.use 'integration'
    integration.keys (err, keys) ->
      res.render 'integration/index', keys: keys

  newapikey: (req, res) ->
    integration = req.db.use 'integration'
    integration.newkey (err, body) ->
      res.redirect 'back'
