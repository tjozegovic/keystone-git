_ = require 'lodash'
busboy = require 'connect-busboy'
express = require 'express'
config = require 'config'
{Queues} = require '../../actionqueue/src/queues'

module.exports = router = express.Router()

router.get '/leads/template/download', (req, res) ->
  res.attachment 'SoftVu Bulk Lead Import.csv'

  attributes = req.db.use 'client/attributes'
  attributes.active (err, attributes) ->
    return next err if err
    attrs = _.map attributes, (attr) ->
      attr.name.toLowerCase().replace /\s/g, ''

    req.csvtemplate =
      fields: [
        'external.source'
        'external.id'
        'agent'
        'status'
      ].concat(attrs).concat [
        'recipient.email'
        'recipient.lastname'
        'recipient.firstname'
        'created'
        'segmented'
        'recipient.override.segmented'
      ]

    res.send new Buffer req.csvtemplate.fields.join(',') + '\n'

router.get '/api/leads/imports', (req, res) ->
  uploads = req.db.use 'client/uploads'
  uploads.all type: 'lead.import', limit: 5, (err, uploads) ->
    return res.status(500).json err: err if err?
    res.json uploads

router.get '/api/search/messages/:email', (req, res) ->
  emails = req.db.use 'client/emails'
  emails.by_recipient_email req.params.email, (err, emails) ->
    return res.status(500).json err: err if err?
    res.json emails

router.get '/api/search/sends/:email', (req, res) ->
  emails = req.db.use 'client/emails'
  emails.by_sender_email req.params.email, (err, results) ->
    return res.status(500).json err: err if err?
    res.json results

router.get '/api/search/leadById/:id', (req, res) ->
  emails = req.db.use 'client/emails'
  emails.by_lead req.params.id, (err, ids) ->
    return res.status(500).json err: err if err?
    res.json ids

router.get '/api/events/lead/:id', (req, res) ->
  events = req.db.use 'client/events'
  events.leadinfo req.params.id, (err, result) ->
    return res.status(500).json err: err if err?
    res.json result

router.get '/api/search/leads/:email', (req, res) ->
  leads = req.db.use 'client/leads'
  leads.by_recipient_email req.params.email, (err, leads) ->
    return res.status(500).json err: err if err?
    res.json leads

router.get '/leads/import/:id/errors', (req, res) ->
  uploads = req.db.use 'client/uploads'
  uploads.get req.params.id, (err, csv) ->
    return res.status(500).json err: err if err?
    file = csv.file.toString()
    res.attachment('SoftVu Bulk Lead Import Errors.csv').send file

router.get '/api/lead/job/:job', (req, res) ->
  jobs = req.db.use 'core/jobs'
  jobs.get req.params.job, (err, job) ->
    return res.status(500).json err: err if err?
    res.json job

router.post '/api/import/cancel/:id', (req, res) ->
  queues = new Queues config.queues.url
  queues.push 'actionqueue', job: req.params.id, action: 'cancel', (err) ->
      return res.status(500).json err: err if err?
      res.status(202).end()

router.post '/api/leads/import', busboy(immediate: yes), (req, res) ->
  return next() unless req.busboy

  req.busboy.on 'file', (name, file, filename, encoding, mimetype) ->
    return unless name is 'file'

    uploads = req.db.use 'client/uploads'
    uploads.save type: 'lead.import', status: 'pending', file, (err, body) ->
      return res.status(500).json err: err if err?
      res.locals.events.publish 'leads.import.uploaded', import: body.id, import_type: 'lead'
      res.status(202).end()
