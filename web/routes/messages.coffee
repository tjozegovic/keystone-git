_ = require 'lodash'
cheerio = require 'cheerio'
express = require 'express'
hb = require 'handlebars'

module.exports = router = express.Router()

router.get '/messages/:id/preview', (req, res) ->
  templates = req.db.use 'client/templates'
  templates.get req.params.id, (err, template) ->
    return res.status(404).send 'Not Found' if err
    res.send template.html

router.get '/messages/:id', (req, res) ->
  {id} = req.params
  messages = req.db.use 'client/messages'
  messages.get id, (err, message) ->
    return res.status(404).send 'Not Found: ', err if err
    res.send message

router.post '/messages/:id/preview', (req, res) ->
  {id} = req.params
  return res.status(404).send 'Not Found' unless id and req.body.id

  [messages, templates, policies] = req.db.use 'client/messages', 'client/templates', 'client/policies'

  messages.get req.body.id, (err, message) ->
    return res.status(404).send 'Not Found' if err
    templates.get id, (err, template) ->
      return res.status(404).send 'Not Found' if err
      tpl = hb.compile template.html
      values = _.values(message.html.policies)
      if _.isObject values[0]
        policy_ids = _.pluck values, 'value'
      else
        policy_ids = values
      policies.get policy_ids, (err, policies) ->
        policies = _.zipObject _.pluck(policies, 'id'), policies

        # TODO: see if this can be combined with pipeline/src/filters/compile.coffee
        $ = cheerio.load template.html
        _.forIn message.html.blocks or {}, (html, name) ->
          el = $ "[data-block=\"#{name}\"]"
          if html?.value?
            el.html html.value
          else
            el.html html or ''
          el.removeAttr 'data-block'

        _.forIn message.html.policies or {}, (id, name) ->
          el = $ "[data-policy=\"#{name}\"]"
          el.html policies[id]?.value or ''
          el.removeAttr 'data-policy'

        _.forIn message.html.links or {}, (link, name) ->
          el = $ "[data-link=\"#{name}\"]"

          href = if link.type is 'page'
            "/page/#{req._client.id}/#{link.value}"
          else
            link.value or ''

          if 'text' of link
            el.text link.text

          el.attr 'href', href
          el.removeAttr 'data-link'

        tpl = hb.compile $.html decodeEntities: no
        tpldata = link: {}
        _.forIn message.html.links or {}, (link, name) ->
          tpldata.link[name] =
            type: link.type
            url: if link.type is 'page'
                "/page/#{req._client.id}/#{link.value}"
              else
                link.value

        if aws = req._client?.credentials?.aws
          # TODO cdn, cloudfront link
          tpldata = _.defaults tpldata,
            assets:
              baseurl: "https://#{aws.bucket}.s3.amazonaws.com"
              public: "https://#{aws.bucket}.s3.amazonaws.com"
              private: "https://#{aws.private.bucket}.s3.amazonaws.com/#{aws.private.folder}" if req._client.credentials.aws.private 

        $ = cheerio.load tpl tpldata,
          partials: 'sender.signature': ''
        links = $ 'a'
        links.attr 'target', '_blank'

        res.send $.html()
