_ = require 'lodash'
busboy = require 'connect-busboy'
es = require 'event-stream'
express = require 'express'

module.exports = router = express.Router()

router.use (req, res, next) ->
  req.isValidEmailAddress = (email) ->
    atidx = email.indexOf '@'
    email.length >= 3 && ~atidx && atidx < email.length - 1
  next()

router.param 'email', (req, res, next, email) ->
  return next 'route' unless req.isValidEmailAddress email
  next()

router.get '/api/optouts/:email', (req, res, next) ->
  optouts = req.db.use 'core/optouts'
  optouts.get req.params.email, (err, body) ->
    return next err if err
    return res.status(404).end() unless req.session.client_id in (body?.clients or [])
    body = _.omit body, '_rev', 'clients'
    body.history = _.filter body.history, (hist) ->
      hist.client is req.session.client_id
    res.json body

router.get '/api/optouts/:id/:type(error|download)', (req, res) ->
  uploads = req.db.use 'client/uploads'
  uploads.get req.params.id, (err, csv) ->
    return res.status(500).json err: err if err?
    file = csv.file.toString()
    if req.params.type is 'error'
      res.attachment('SoftVu Bulk Optout Errors.csv').send file
    else
      res.attachment('SoftVu Bulk Optout.csv').send file

router.get '/api/optouts/imports', (req, res) ->
  uploads = req.db.use 'client/uploads'
  uploads.all type: 'optout.import', limit: 5, (err, uploads) ->
    return res.status(500).json err: err if err?
    res.json uploads

router.post '/api/optout/stopped/:id', (req, res) ->
  uploads = req.db.use 'client/uploads'
  uploads.save req.body[0], (err, body) ->
    return res.status(500).json err: err if err?

    res.json body

router.post '/api/:opt(optin|optout)/:email', (req, res, next) ->
  optouts = req.db.use 'core/optouts'
  {email, opt} = req.params
  optouts[opt] email, req.session.client_id, (err, body) ->
    return next err if err

    res.locals.events.emit opt, req.session.client_id,
      from: 'website - manual'
      address: email
      user: req.session.username
      request: _.pick req, ['headers', 'query', 'originalUrl', 'ip']
    res.json body

router.post '/api/optout/upload', busboy(immediate: yes), (req, res, next) ->
  return next() unless req.busboy

  req.busboy.on 'file', (name, file, filename, encoding, mimetype) ->
    return unless name is 'file'

    uploads = req.db.use 'client/uploads'
    uploads.save type: 'optout.import', status: 'pending', file, (err, body) ->
      return res.status(500).json err: err if err?
      res.locals.events.publish 'optout.import.uploaded', import: body.id, import_type: 'optout', user: req.session.username
      res.status(202).end()
