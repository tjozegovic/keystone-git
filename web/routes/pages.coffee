_ = require 'lodash'
express = require 'express'

{get_asset} = require './assets'
{correctids, loadclient} = require '../middleware'

module.exports = router = express.Router()

router.get '/page/:client/%7B%7Bassets.baseurl%7D%7D/:asset*', correctids, get_asset
router.get '/page/:client/{{assets.baseurl}}/:asset*', correctids, get_asset

router.all '/page/:one([a-zA-Z0-9]{32})/:two([a-zA-Z0-9]{32})?',
  (req, res, next) ->
    if req._iscname
      _.assign req.locals,
        client: req._client.id
        page: req.params.one
    else
      _.assign req.locals,
        client: req.params.one
        page: req.params.two

    delete req._client if req.locals.client isnt req._client?.id
    next()
  loadclient
  (req, res, next) ->
    {client, page} = req.locals
    req.db.client = client

    [layouts, pages] = req.db.use 'client/layouts', 'client/pages'
    pages.get page, (err, page) ->
      return next err if err

      req.page = page
      # TODO errors with newrelic? or emits
      unless req.page?.hbs? and req.page.hbs.length
        console.warn "could not load page client: #{client} page: #{page.id}, no hbs template found"
        return res.render '404.jade', title: '404 File not found'

      return next() unless req.page.layout
      layouts.get req.page.layout, (err, {hbs}) ->
        return next err if err

        req.page.hbs = hbs.replace '{{{body}}}', req.page.hbs
        next()

router.get '/page/*', (req, res, next) ->
  unless req.page?
    # TODO errors with newrelic? or emits
    console.warn "could not load page #{req.locals.page}, not found in database"
    return res.render '404.jade', title:'404 File not found'
    ##return res.status(404).end 'Not Found' unless req.page?

  handlebars = require 'handlebars'
  for x in req.page.variables or []
    req.page.hbs = req.page.hbs.replace "{{#{x.name}}}", x.value

  if aws = req._client?.credentials?.aws
    baseurl = "https://#{aws.bucket}.s3.amazonaws.com"
    publicurl = "https://#{aws.bucket}.s3.amazonaws.com"
    if aws.private
      privateurl = "https://#{aws.private.bucket}.s3.amazonaws.com/#{aws.private.folder}"
  else
    baseurl = '{{assets.baseurl}}'
    publicurl = '{{assets.public}}'
    privateurl = '{{assets.private}}'

  emails = req.db.use 'client/emails'
  unless email = req.query.e
    hbs = req.page.hbs
    hbs = hbs.replace /\{\{assets.baseurl\}\}/g, baseurl
    hbs = hbs.replace /\{\{assets.public\}\}/g, publicurl
    hbs = hbs.replace /\{\{assets.private\}\}/g, privateurl if privateurl
    res.send hbs
  else
    emails.get email, (err, body) ->
      return next err if err
      return res.render '404.jade', title:'404 File not found' unless body

      tmpl = handlebars.compile req.page.hbs
      res.send tmpl _.assign(body, assets: baseurl: baseurl, public: publicurl, private: privateurl),
        partials: 'sender.signature': body.sender.signature or ''

router.post '/page/*', (req, res) ->
  res.locals.events.emit 'form.completed', req.locals.client,
    client: req.locals.client
    email: req.query.e
    page: req.page.id
    body: req.body
    redirect: req.body.redirect or req.page.redirect
    request: _.pick req, ['headers', 'query', 'originalUrl', 'ip']

  url = req.body.redirect or req.page.redirect
  url += "#{if ~url.indexOf '?' then '&' else '?'}e=#{req.query.e}" if req.query.e?

  res.redirect url

router.use (error, req, res, next) ->
  res.status 500
  res.render '500.jade', title:'500 Internal Server Error', error:error
