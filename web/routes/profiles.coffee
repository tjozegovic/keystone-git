_ = require 'lodash'
S = require 'shared/util/string'
async = require 'async'
csv = require 'csv'
busboy = require 'connect-busboy'
express = require 'express'

module.exports = router = express.Router()

setfields = (req, res, next) ->
  req.csvtemplate =
    fields: [
      'email'
      'aliases'
      'group'
      'lastname'
      'firstname'
      'override signature?'
      'active?'
      'phones.main'
      'phones.cell'
      'phones.fax'
      'default sender?'
    ].concat _.map _.pluck(req._client.profiles or [], 'name'), (custom) -> "custom.#{custom}"
  next()

router.get '/profiles/template/download', setfields, (req, res) ->
  res.attachment 'SoftVu Bulk User Upload.csv'
  res.send new Buffer req.csvtemplate.fields.join(',') + '\n'

router.get '/api/profiles/settings', (req, res) ->
  res.json req._client.profiles or []

router.put '/api/profiles/settings', (req, res) ->
  client = req.db.use 'core/clients'
  client.save _.assign(req._client, profiles: req.body), -> res.json ok: yes

router.get '/api/senders/download', setfields, (req, res) ->
  res.attachment 'SoftVu Bulk User Upload.csv'
  [senders, groups] = req.db.use 'client/senders', 'client/groups'
  default_sender = req._client.default_sender or null

  async.parallel
    senders: (cb) -> senders.all cb
    groups: (cb) -> groups.all cb
  , (err, {senders, groups}) ->
    csv()
    .from.array senders
    .to.stream res, header: yes, columns: req.csvtemplate.fields
    .transform (row, index) ->
      _groups = _.indexBy groups, 'id'
      row.group = _groups[row.group]?.name or ''
      row.aliases = _(row.aliases or [])
        .without "#{row.firstname} #{row.lastname}", "#{row.lastname}, #{row.firstname}"
        .join ', '

      row['active?'] = if row.active then 'yes' else 'no'
      row['override signature?'] = if row.override then 'yes' else 'no'
      row["phones.#{type}"] = val for own type, val of row.phones or {}
      row['default sender?'] = if row.email is default_sender then 'yes' else 'no'
      row["custom.#{key}"] = value for own key, value of row.custom or {}
      row

router.get '/api/senders/active/download', setfields, (req, res) ->
  res.attachment 'SoftVu Bulk User Upload.csv'
  [senders, groups] = req.db.use 'client/senders', 'client/groups'
  default_sender = req._client.default_sender or null

  async.parallel
    senders: (cb) -> senders.active cb
    groups: (cb) -> groups.all cb
  , (err, {senders, groups}) ->
    csv()
    .from.array senders
    .to.stream res, header: yes, columns: req.csvtemplate.fields
    .transform (row, index) ->
      _groups = _.indexBy groups, 'id'
      row.group = _groups[row.group]?.name or ''
      row.aliases = _(row.aliases or [])
        .without "#{row.firstname} #{row.lastname}", "#{row.lastname}, #{row.firstname}"
        .join ', '

      row['active?'] = if row.active then 'yes' else 'no'
      row['override signature?'] = if row.override then 'yes' else 'no'
      row["phones.#{type}"] = val for own type, val of row.phones or {}
      row['default sender?'] = if row.email is default_sender then 'yes' else 'no'
      row["custom.#{key}"] = value for own key, value of row.custom or {}
      row

router.post '/api/senders/upload', busboy(immediate: yes), (req, res) ->
  return next() unless req.busboy

  req.busboy.on 'file', (name, file, filename, encoding, mimetype) ->
    return unless name is 'file'

    reader = csv().from file, columns: yes, relax: yes
      .transform (row) ->
        _nullguard = (obj, key) -> obj[key] ?= {}
        _.transform row, (row, val, key) ->
          keys = key.split '.'
          [final, keys] = [keys.pop(), keys]

          obj = keys.reduce _nullguard, row
          obj[final] = val

    records = []
    reader
      .on 'record', (record) ->
        records.push record
      .on 'error', (err) ->
        res.status(500).json err: err
      .on 'end', (rows) ->
        [senders, groups, client] = req.db.use 'client/senders', 'client/groups', 'core/clients'

        senderlookup = (email, cb) ->
          senders.getEmail email, (err, lookup) ->
            cb err if err
            cb null, lookup

        stats = created: 0, updated: 0, invalid_groups: 0
        defaultChanged = false
        async.auto
          groups: (cb) ->
            groups.all cb
          stats: [
            'groups', (cb, results) ->
              groups = _.groupBy results.groups, 'name'
              async.each records, (record, cb) ->
                # TODO group doesn't exist
                if lookup = groups[record.group]
                  record.group = lookup[0]?.id
                else
                  stats.invalid_groups++

                implied_aliases = ["#{record.firstname} #{record.lastname}", "#{record.lastname}, #{record.firstname}"]
                record.aliases = _.compact implied_aliases.concat (record.aliases or '').split /\s*,\s*/

                if record['active?']
                  record.active = S(record['active?']).toBoolean()
                  delete record['active?']

                if record['override signature?']
                  record.override = S(record['override signature?']).toBoolean()
                  delete record['override signature?']

                # check for default sender field, if that field is true, and that default hasn't already been changed/set
                if record['default sender?'] and S(record['default sender?']).toBoolean() and !defaultChanged
                  client.save _.assign(req._client, default_sender: record.email ), (err, body) ->
                    cb err if err?
                    # variable to allow only one client save. multiple attempts causes error
                    defaultChanged = true
                    delete record['default sender?']

                # TODO sender doesn't exist
                senderlookup record.email, (err, lookup) ->
                  cb err if err?
                  if lookup
                    stats.updated++
                    record = _.assign lookup or {}, record
                  else
                    stats.created++
                  senders.upsert record, (err, body) ->
                    cb err if err?
                  cb null
              , (err) ->
                cb err if err?
                cb null, stats
          ]
        , (err, results) ->
          return res.status(500).json err: err if err
          res.json results.stats
