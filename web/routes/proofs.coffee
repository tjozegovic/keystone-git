_ = require 'lodash'
archiver = require 'archiver'
async = require 'async'
cheerio = require 'cheerio'
express = require 'express'
hb = require 'handlebars'
wkhtmltopdf = require 'wkhtmltopdf'

module.exports = app = express()

app.on 'mount', (parent) ->
  _.assign app.settings, parent.settings

app.get '/proofs/:status/:segment?/:step?', (req, res) ->

  [templates, policies, messages, statuses] = req.db.use ['client/templates', 'client/policies', 'client/messages', 'client/statuses']

  options = { dpi: '96', viewportSize: '1200 x 720', pageSize: 'letter', enableExternalLinks: true }

  statuses.get req.params.status, (err, body) ->

    # if there is a segment parameter set to segmentID
    if req.params.segment?
      segmentID = req.params.segment
      # find id in status.segments, set to seg
      seg = _.find(body.segments, id: segmentID)

      # if there is a step parameter set to stepID
      if req.params.step?
        stepID = req.params.step

        # find message id and step name in steps using stepID
        {message: msgID, name} = _.find(seg.steps, id: stepID)

        res.attachment name + '.pdf'

        makeHtml msgID, (content) ->

          # create the pdf stream using wkhtmltopdf. when the stream is finished, pipe the stream to the attachment file
          stream = wkhtmltopdf content, options

          stream.pipe res

      else
        archive = archiver 'zip'

        # get the name of the segment and use to set the name of the zip file
        {name: segName} = _.find(body.segments, id: segmentID)
        res.attachment segName + '.zip'
        archive.pipe res

        # get message id by getting each step based on the id of each seg.step
        async.eachSeries _.filter(seg.steps, 'id'), (step, cb) ->

          makeHtml step.message, (html) ->

            # create the pdf stream using wkhtmltopdf. when the streams are finished, call the callback
            stream = wkhtmltopdf html, options, ->
              cb()

            archive.append stream, { name: segName + '/' + step.name + '.pdf' }

        , -> # callback to finalize the archive after all pdfs have been created

          archive.finalize()

    else
      # get the name of the segment and use to set the name of the zip file
      res.attachment body.name + '.zip'
      archive = archiver 'zip'
      archive.pipe res

      async.eachSeries body.segments, (segment, _cb) ->

        async.eachSeries _.filter(segment.steps, 'id'), (step, cb) ->

          makeHtml step.message, (_html) ->

            # create the pdf stream using wkhtmltopdf. when the streams are finished, call the callback
            stream = wkhtmltopdf _html, options, ->
              cb()

            # add the stream to the zip archive
            archive.append stream, { name: body.name + '/' + segment.name + '/' + step.name + '.pdf' }

        , -> # callback function that calls another callback when all segments have been ran through
          if !segment.steps.length
            archive.append null, {name: body.name + '/' + segment.name + '/'}
          _cb()

      , -> # callback to finalize the archive after all pdfs have been created
        archive.finalize()


  makeHtml = (msgID, cb) ->

    messages.get msgID, (err, message) ->
      # get template id inside message body
      messageTmplt = message.template

      templates.get messageTmplt, (err, template) ->
        # handlebars compiles html inside template body
        tpl = hb.compile template.html

        # using policies db, get all of the policies in message.html
        policies.get _.values(message.html.policies), (err, policies) ->
          policies = _.zipObject _.pluck(policies, '_id'), policies

          data = {template: template, message: message, policies: policies }

          cb loadCheerio data

  makeUrl = (data) ->

    if data.link.type is 'page'
      return "#{data.base}/page/#{data.client_id}/#{data.link.value}"
    else
      return data.link.value or ''

  loadCheerio = (data) ->

    # call cheerio loading up the template html
    $ = cheerio.load data.template.html

    client_doc = req._client
    client_id = req._client._id

    base = if client_doc.cnames?.length
      'https://' + client_doc.cnames[0]
    else
      config.links?.baseurl or 'https://app.softvu.com'

    # fill in data block variables
    _.forIn data.message.html.blocks or {}, (html, name) ->
      el = $ "[data-block=\"#{name}\"]"
      el.html html
      el.removeAttr 'data-block'

    # fill in data policy variables
    _.forIn data.message.html.policies or {}, (id, name) ->
      el = $ "[data-policy=\"#{name}\"]"
      el.html data.policies[id]?.value or ''
      el.removeAttr 'data-policy'

    # fill in data link variables
    _.forIn data.message.html.links or {}, (link, name) ->
      el = $ "[data-link=\"#{name}\"]"
      href = makeUrl {base: base, client_id: client_id, link: link }
      if 'text' of link
        el.text link.text
      el.attr 'href', href
      el.removeAttr 'data-link'

    # compile template html with handlebars
    tpl = hb.compile $.html decodeEntities: no
    tpldata = link: {}
    _.forIn data.message.html.links or {}, (link, name) ->
      tpldata.link[name] =
        type: link.type
        url: makeUrl {base: base, client_id: client_id, link: link }

    # get items (images) from amazon s3 bucket
    if aws = req._client?.credentials?.aws
      tpldata = _.defaults tpldata,
        assets:
          baseurl: "https://#{aws.bucket}.s3.amazonaws.com"
          public: "https://#{aws.bucket}.s3.amazonaws.com"
          private: "https://#{aws.private.bucket}.s3.amazonaws.com/#{aws.private.folder}" if req._client.credentials.aws.private
    $ = cheerio.load tpl tpldata,
      partials: 'sender.signature': ''
    links = $ 'a'
    links.attr 'target', '_blank'

    # create html and send to callback function
    return result = $.html()
