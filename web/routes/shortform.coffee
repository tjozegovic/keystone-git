async = require 'async'

module.exports =
  index: (req, res) ->
    async.parallel [
      (cb) ->
        repo = req.db.use 'client/attributes'
        repo.all (attributes) -> cb null, attributes
      (cb) ->
        repo = req.db.use 'client/statuses'
        repo.active (statuses) -> cb null, statuses
      (cb) ->
        repo = req.db.use 'client/senders'
        repo.active (senders) -> cb null, senders
    ], (err, [attributes, statuses, senders]) ->
      res.render 'shortform', attributes: attributes, statuses: statuses, senders: senders

  post: (req, res) ->
    res.redirect 'back'
