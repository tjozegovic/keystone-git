_ = require 'lodash'
S = require 'shared/util/string'
async = require 'async'
csv = require 'csv'
busboy = require 'connect-busboy'
express = require 'express'

module.exports = router = express.Router()


setfields = (req, res, next) ->
  req.csvtemplate =
    fields: [
      'name'
      'aliases'
      'active'
      '$hidden'
    ]
  next()

router.get '/bulk/statuses/template/download', setfields, (req, res) ->
  res.attachment 'SoftVu Bulk Statuses Upload.csv'
  res.send new Buffer req.csvtemplate.fields.join(',') + '\n'

router.get '/api/bulk/statuses/download', setfields, (req, res) ->
  res.attachment 'SoftVu Bulk Status Upload.csv'
  statuses = req.db.use 'client/statuses'

  async.parallel
    statuses: (cb) -> statuses.all cb
  , (err, {statuses}) ->
    csv()
    .from.array statuses
    .to.stream res, header: yes, columns: req.csvtemplate.fields
    .transform (row, index) ->
      row.name = row.name or ''
      row.aliases = _(row.aliases or [])
        .join ', '

      row['active'] = if row.active then 'yes' else 'no'
      row['$hidden'] = if row.$hidden then 'yes' else 'no'
      row

router.post '/api/bulk/statuses/upload', busboy(immediate: yes), (req, res) ->
  return next() unless req.busboy

  req.busboy.on 'file', (name, file, filename, encoding, mimetype) ->
    return unless name is 'file'

    reader = csv().from file, columns: yes, relax: yes
      .transform (row) ->
        _nullguard = (obj, key) -> obj[key] ?= {}
        _.transform row, (row, val, key) ->
          keys = key.split '.'
          [final, keys] = [keys.pop(), keys]

          obj = keys.reduce _nullguard, row
          obj[final] = val

    records = []
    reader
      .on 'record', (record) ->
        records.push record
      .on 'error', (err) ->
        res.status(500).json err: err
      .on 'end', (rows) ->
        statuses = req.db.use 'client/statuses'

        statuslookup = (name, cb) ->
          statuses.getByName name, (err, lookup) ->
            cb err if err
            cb null, lookup

        stats = created: 0, updated: 0
        async.each records, (record, cb) ->
          if record.aliases.length
            record.aliases = record.aliases.split(/\s*,\s*/)
          else
            record.aliases = []

          if record.active
            record.active = S(record.active).toBoolean()
          else
            record.active = yes
          if record.$hidden
            record.$hidden = S(record.$hidden).toBoolean()
          else
            record.$hidden = no

          statuslookup record.name, (err, lookup) ->
            return res.status(500).json err: err if err
            if lookup.length
              stats.updated++
              record = _.assign lookup[0] or {}, record
            else
              stats.created++
            statuses.save record, (err, body) ->
              return cb res.status(500).json err: err if err
            cb null
        , (err) ->
          return res.status(500).json err: err if err
          res.json stats
