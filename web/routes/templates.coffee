express = require 'express'

module.exports = router = express.Router()

parse_html = (client, html) ->
  if aws = client?.credentials?.aws
    html = html.replace /\{\{assets\.baseurl\}\}/g, "https://#{aws.bucket}.s3.amazonaws.com"
    html = html.replace /\{\{assets\.public\}\}/g, "https://#{aws.bucket}.s3.amazonaws.com"
    if aws.private
      html = html.replace /\{\{assets\.private\}\}/g, "https://#{aws.private.bucket}.s3.amazonaws.com/#{aws.private.folder}"
  return html

router.get '/templates/:id/preview', (req, res, next) ->
  {id} = req.params
  templates = req.db.use 'client/templates'
  templates.get id, (err, template) ->
    return next err if err
    res.send parse_html req._client, template.html

router.post '/templates/preview', (req, res) ->
  {html} = req.body
  res.send parse_html req._client, html
