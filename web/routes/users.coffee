credential = require 'credential'
express = require 'express'
uuid = require 'uuid'
_ = require 'lodash'

module.exports = router = express.Router()

router.get '/login', (req, res) ->
  if req.user
    res.redirect '/'
  else
    res.render 'login'

router.post '/login', (req, res, next) ->
  return res.send 'Invalid credentials' unless body = req.body

  [clients, users] = req.db.use 'core/clients', 'core/users'
  users.get body.username, (err, user) ->
    forward = ->
      req.session.username = user.username
      if !user.client? or user.client is ''
        clients.active (err, body) ->
          clients = res.locals.clients = _.sortBy body, 'name'
          user.client = clients[Math.floor Math.random() * clients.length]?.id
          users.save user, (err) ->
            return next err if err
            login()
      else
        login()

    login = ->
      res.locals.events.emit 'user.loggedin', user.client, user: user.id

      return res.redirect if body.return?
        body.return
      else if user.isAdmin
        '/superadmin'
      else
        '/'

    return res.render 'login', error: 'Invalid username or password.' if err or not user?
    return res.render 'login', error: 'User is disabled.' if user?.disabled

    credential.verify user.password, body.password, (err, valid) ->
      if err
        console.error 'credential#verify error', err
        return res.render 'login', error: 'Invalid username or password.'

      return res.render 'login', error: 'Invalid username or password.' unless valid
      return forward() unless req.body.remember

      token = uuid.v4()
      user.token = token
      users.save user, (err) ->
        return next err if err

        expires = new Date
        expires.setDate expires.getDate() + 21
        res.cookie 'token', token, httpOnly: yes, expires: expires
        return forward()

router.get '/logout', (req, res) ->
  res.clearCookie 'token', httpOnly: yes, path: '/'
  res.locals.events.publish 'user.loggedout', user: req.session.username
  delete req.session.username
  delete req.session.client_id
  res.redirect '/login'
