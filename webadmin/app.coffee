config = require 'config'

_ = require 'lodash'
async = require 'async'
credential = require 'credential'
forge = require 'node-forge'
moment = require 'moment'
uuid = require 'uuid'
AWS = require 'aws-sdk'
S = require 'string'
pg = require 'pg'
Dns = require 'shared/dns'

# middlewares
bodyParser = require 'body-parser'
compression = require 'compression'
cookieParser = require 'cookie-parser'
favicon = require 'serve-favicon'
methodOverride = require 'method-override'
# session = require 'express-session'
session = require 'cookie-session'

express = require 'express'
fs = require 'fs'
busboy = require 'connect-busboy'
path = require 'path'
module.exports = app = express()

{SingleQueryDatabase, MultiQueryDatabase, RepositoryFactory} = require 'shared/database'
schema = require 'shared/database/schema'

AWS.config.update config.aws

app.set 'port', process.env.PORT or 3002
app.set 'views', path.join __dirname, 'views'
app.set 'view engine', 'jade'
app.locals.env = config.env

do (fs = require 'fs') ->
  file = path.join __dirname, '../.version'
  return unless fs.existsSync file

  version = fs.readFileSync(file).toString()
  app.locals.version =
    long: version
    short: version.split(' ')[0]

app.set 'db url', config.db.url
app.set 'queue url', config.queues.url

app.use bodyParser.urlencoded extended: yes
app.use bodyParser.json()
app.use cookieParser()

app.use methodOverride()
# app.use favicon()
app.use compression()

# ConnectCouchDB = require('connect-couchdb') session
# app.use session
#   secret: '$oftVu7381'
#   store: new ConnectCouchDB
#     name: 'sessions'
#     host: config.sessions.host
#   resave: yes
#   saveUninitialized: yes

app.use session secret: '$oftVu7381'

app.use (req, res, next) ->
  # res.locals.session will allow views access to the session
  req.db = new RepositoryFactory app.get 'db url'
  res.locals.req = req
  next()

app.use (req, res, next) ->
  return next() unless username = req.session.username

  users = req.db.use 'core/users'
  users.get username, (err, user) ->
    req.user = _.omit user, ['password']
    next()

app.use express.static path.join __dirname, 'public'

app.use (req, res, next) ->
  return next() if req.user?.isAdmin

  if req.user
    res.status(401).send 'Unauthorized'
  else
    res.redirect "/login?return=#{req.originalUrl}"

app.get '/api/clients', (req, res) ->
  clients = req.db.use 'core/clients'
  clients.all (err, clients) ->
    res.json _.indexBy clients, 'id'

if config.admin['allow delete']
  app.delete '/api/clients/:id', (req, res, next) ->
    {id} = req.params
    {core} = res.locals
    base = "client-#{id}-"
    async.auto do ->
      auto = destroy: (cb) ->
        core.head id, (err, _, headers) ->
          return cb err if err
          core.destroy id, headers.etag[1...-1], (err) ->
            return cb err if err
            cb null

      ['data', 'events', 'messages', 'testing'].forEach (name) ->
        auto[name] = ['destroy', (cb) -> core.server.db.destroy "#{base}#{name}", cb]

      return auto
    , (err) ->
      return next err if err
      res.json ok: yes

  app.delete '/api/clients/:id/events', (req, res, next) ->
    {id} = req.params
    {core} = res.locals
    db = "client-#{id}-events"
    async.series [
      (cb) -> core.server.db.destroy db, (err) -> cb null
      (cb) -> core.server.db.create db, cb
      (cb) -> couchdb.update "#{app.get 'db url'}/#{db}", 'clients/events', cb
    ], (err) ->
      return next err if err
      res.json ok: yes

  app.delete '/api/clients/:id/messages', (req, res, next) ->
    {id} = req.params
    {core} = res.locals
    db = "client-#{id}-messages"
    async.series [
      (cb) -> core.server.db.destroy db, (err) -> cb null
      (cb) -> core.server.db.create db, cb
      (cb) -> couchdb.update "#{app.get 'db url'}/#{db}", 'clients/messages', cb
    ], (err) ->
      return next err if err
      res.json ok: yes

app.post '/api/clients', (req, res) ->
  return unless client = req.body?.client

  clients = req.db.use 'core/clients'
  clients.save client, (err, body) ->
    return res.status(500).json err: err if err
    return res.json id: body.id, version: body.version

app.put '/api/clients/:id', (req, res) ->
  client = req.body
  client.id = req.params.id

  clients = req.db.use 'core/clients'
  clients.save client, (err, body) ->
    return res.status(500).json err: err if err
    res.status(204).end()

app.get '/api/public/aws/:bucket', (req, res) ->
  crypto = require 'crypto'
  creds = config.aws

  in30min = new Date()
  in30min.setMinutes in30min.getMinutes() + 30

  s3Policy =
    expiration: in30min
    conditions: [
      ['starts-with', '$key', '']
      {'bucket': req.params.bucket}
      {'acl': 'public-read'}
      ['starts-with', '$Content-Type', req.query.type]
      {'success_action_status': '201'}
    ]

  # stringify and encode the policy
  stringPolicy = JSON.stringify s3Policy
  base64Policy = new Buffer(stringPolicy, 'utf-8').toString 'base64'

  # sign the base64 encoded policy
  signature = crypto.createHmac('sha1', creds.secretAccessKey)
    .update(new Buffer base64Policy, 'utf-8')
    .digest 'base64'

  # build the results object
  res.json
    bucket: req.params.bucket
    Policy: base64Policy
    Signature: signature
    AWSAccessKeyId: creds.accessKeyId

app.get '/api/clients/getBuckets', (req, res) ->
  get_s3_buckets res

get_s3_buckets = (res) ->
  unless config.env is 'production'
    return res.json _.assign buckets: {Buckets: [{Name: 'softvu-staging'}, {Name: 'softvu-staging-private'}]}, config: config.aws
  s3 = new AWS.S3()

  s3.listBuckets (err, data) ->
    return res.status(500).json err: err if err
    res.json _.assign buckets: data, config: config.aws

app.post '/api/clients/createFolder/:id', (req, res) ->
  folder_name = 'client_' + req.params.id
  if config.env is 'production'
    bucket_name = 'softvu-private'
  else
    bucket_name = 'softvu-staging-private'

  create_private_folder folder: folder_name, bucket: bucket_name, res

create_private_folder = (private_folder, res) ->
  s3 = new AWS.S3()

  createParams =
    Bucket: private_folder.bucket
    Key: private_folder.folder + '/'
    ACL: 'public-read-write'

  s3.putObject createParams, (err, data)  ->
    return res.status(500).json err: err if err?

    res.json _.assign folder_name: private_folder.folder, bucket_name: private_folder.bucket, data: data

app.post '/api/clients/createBucket', (req, res) ->
  createS3Bucket "sv-" + S(req.body.bucketName).indexable().s, res

createS3Bucket = (originalBucketName, res, count = 0) ->
  s3 = new AWS.S3()

  newBucketName = originalBucketName
  newBucketName += count if count != 0

  #See if bucket already exists
  #Will receive an error if the bucket exists or a blank data object
  #if the bucket does not exist.
  headBucketParams = Bucket: newBucketName
  s3.headBucket headBucketParams, (err, data) ->
    if err?.statusCode is 404
      createParams =
         Bucket: newBucketName
         ACL: 'public-read-write'

      s3.createBucket createParams, (err, data)  ->
        if data?
          addBucketCORSParams =
            Bucket: newBucketName
            CORSConfiguration:
              CORSRules: [
                AllowedHeaders:['*']
                AllowedOrigins: ['*']
                MaxAgeSeconds: 3000
                AllowedMethods: ['GET', 'POST', 'PUT', 'DELETE']
              ]

           s3.putBucketCors addBucketCORSParams, (err, data) ->
             res.json _.assign bucketName: newBucketName, config.aws
    else
      #Bucket exists so we add anincremental number to the end
      #and then try to creating the bucket with the new name
      createS3Bucket originalBucketName, res, ++count

app.post '/api/clients/copyTemplateBucket', (req, res) ->
  copyTemplateBucket req.body.templateBucketName, req.body.newBucketName, res

copyTemplateBucket = (templateBucket, newBucket, res) ->
  s3 = new AWS.S3()

  listparams =
    Bucket: templateBucket
    EncodingType: 'url'
    Delimiter: '::'

  s3.listObjects listparams, (err, info)->
    return res.status(500).end err if err?

    # TODO if we're cloning a large client, this might stall the ui, we should create a job for this
    async.each info.Contents, (item, cb) ->
      params =
        Bucket: newBucket
        CopySource: templateBucket + "/" + item.Key
        Key: item.Key
        ACL: 'public-read-write'

      s3.copyObject params, cb
    , (err) ->
      return res.status(500).end err if err?
      res.json success: true

app.get '/api/users/:username?', (req, res) ->
  users = req.db.use 'core/users'
  if username = req.params.username
    users.get username, (err, body) ->
      if body
        body.username = body.id
        delete body.id
        body.hash = body.password
        delete body.password
        res.json body
      else
        res.status(404).end()
  else
    users.all (err, users) ->
      res.json _.map users, (user) ->
        user.hash = user.password
        delete user.password
        user

app.post '/api/users', (req, res) ->
  if user = req.body?.user
    unless user.id
      user.type = 'user'

    upsert = ->
      users = req.db.use 'core/users'
      users.save user, (err, body) ->
        res.json success: !err?, body: body

    if user?.password
      credential.hash user.password, (err, hash) ->
        user.password = hash
        upsert()
    else
      user.password = user.hash
      delete user.hash
      upsert()

app.put '/api/users/:username', (req, res) ->
  if user = req.body?.user
    upsert = ->
      users = req.db.use 'core/users'
      users.save user, (err, body) ->
        res.json success: !err?, body: body

    if user?.password
      credential.hash user.password, (err, hash) ->
        user.password = hash
        delete user.hash
        upsert()
    else
      user.password = user.hash
      delete user.hash
      upsert()

app.get '/api/apikeys', (req, res) ->
  keys = req.db.use 'core/apikeys'
  keys.all (err, body) ->
    res.json body

app.get '/api/billing', (req, res) ->
  [from, to] = ['from', 'to'].map (s) ->
    moment req.query[s]

  req.db.client = req.query.client
  summaries = req.db.use 'client/summaries'
  summaries.billing req.query, (err, totals) ->
    res.json totals

app.get '/api/templates', (req, res) ->
  templates = req.db.use 'core/templates'
  templates.fetch ['dailyvu', 'vunotification', 'form.response'], (err, body) ->
    res.json _.indexBy body, 'name'

app.post '/api/templates', (req, res) ->
  templates = req.db.use 'core/templates'
  async.each Object.keys(req.body), (template, cb) ->
    templates.get template, (err, result) ->
      tmpl = req.body[template]
      tmpl.id ?= result?.id
      tmpl.version ?= result?.version
      tmpl.created ?= result?.created
      name = template
      templates.save tmpl, name, cb
  , (err) ->
    if err then res.status(500).json ok: no, err: err else res.json ok: yes

app.get '/api/attributes/:client', (req, res) ->
  data = req.db.use "client-#{req.params.client}-data"
  data.get 'attributes', (err, doc) ->
    return res.status(500).json err: err if err
    res.json doc.attributes or {}

app.get '/api/apikeys/:client', (req, res, next) ->
  keys = req.db.use 'core/apikeys'
  keys.byclient req.params.client, (err, keys) ->
    return next err if err
    res.json keys

app.delete '/api/apikeys/:id', (req, res, next) ->
  keys = req.db.use 'core/apikeys'
  keys.get req.params.id, (err, body) ->
    return next err if err

    body.revoked = new Date
    keys.insert body, (err) ->
      return next err if err
      res.json ok: yes, key: body

app.get '/api/jobs', (req, res) ->
  jobs = req.db.use 'core/jobs'
  jobs.pending {at: moment().add(20, 'minute'), limit: 50}, (err, jobs) ->
    res.json jobs

app.get '/api/jobs/:client', (req, res) ->
  client = req.params.client
  jobs = req.db.use 'core/jobs'
  jobs.get_by_client client, (err, jobs) ->
    res.json jobs

app.put '/api/jobs', (req, res) ->
  jobs = req.db.use 'core/jobs'
  jobs.save req.body, (err, body) ->
    res.status(if err then 500 else 200).json ok: !!err, err: err, body: body

valid_email = (req, res, next) ->
  is_valid = (email) ->
    atidx = email.indexOf '@'
    email.length >= 3 && ~atidx && atidx < email.length - 1

  return next 'route' unless req.params.email? and is_valid req.params.email
  next()

app.get '/api/optouts/:email', valid_email, (req, res) ->
  optouts = req.db.use 'core/optouts'
  optouts.get req.params.email, (err, optout) ->
    return next err if err
    return res.status(404).end() unless optout?

    res.json _.omit optout, '_rev'

app.post '/api/:opt(optin|optout)/:email', valid_email, (req, res) ->
  optouts = req.db.use 'core/optouts'
  {email, opt} = req.params
  optouts[opt] email, req.body.client, from: 'webadmin', user: req.user.username, (err, body) ->
    return next err if err
    res.json body

app.get '/api/events', (req, res) ->
  jobs = req.db.use 'core/events'
  res.json []
  # jobs.view 'events', 'by_date', include_docs: yes, limit: 50, descending: yes, (err, body) ->
  #   if err then res.status(500).json err: err else res.json _.pluck body.rows, 'doc'

do ({exec} = require 'child_process') ->
  # http://stackoverflow.com/questions/4436558/start-stop-a-windows-service-from-a-non-administrator-user-account
  start = (name, cb) ->
    exec "net start keystone.#{name}", (err, stdout, stderr) ->
      cb err

  stop = (name, cb) ->
    exec "net stop keystone.#{name}", (err, stdout, stderr) ->
      cb if ~err?.message.indexOf 'not started' then null else err

  app.use '/api/services', router = new express.Router()
  router.post '/start', (req, res) ->
    start req.body.name, (err) ->
      return res.status(500).json err: err if err
      res.json ok: yes
  router.post '/stop', (req, res) ->
    stop req.body.name, (err) ->
      return res.status(500).json err: err if err
      res.json ok: yes
  router.post '/restart', (req, res) ->
    name = req.body.name
    stop name, (err) ->
      return res.status(500).json err: err if err
      start name, (err) ->
        return res.status(500).json err: err if err
        res.json ok: yes

if config.admin['allow cloning']
  app.enable 'allow cloning'

  clone = undefined
  replaced = undefined
  short = undefined

  app.get '/api/clients/:id/export', (req, res) ->
    if clone?
      res.attachment short + '_clone.json'
      res.send clone
      clone = undefined
      short = undefined
    else
      res.redirect '/superadmin'

  app.get '/api/clients/:id/import', (req, res) ->
    if replaced?
      res.attachment short + '_replaced.json'
      res.send replaced
      replaced = undefined
      short = undefined
    else
      res.redirect '/superadmin'

  app.post '/api/clients/:id/export', (req, res) ->
    return unless client = req.params.id

    req.db.client = client
    [attributes, templates, policies, pages, messages, statuses] =
    req.db.use 'client/attributes', 'client/templates', 'client/policies', 'client/pages', 'client/messages', 'client/statuses'
    short = req.body.short

    exportClient = ->
      clonedAttributes = {}
      clonedTemplates = {}
      clonedPolicies = {}
      clonedPages = {}
      clonedMessages = {}
      clonedStatuses = {}
      clonedClient = {}
      async.auto
        attributes: (cb) ->
          attributes.active_reg (err, body) ->
            async.each body, (attribute, bodycb) ->
              clonedAttributes[attribute.name] = attribute
              bodycb null
            cb null
        templates: (cb) ->
          templates.all (err, body) ->
            async.each body, (template, bodycb) ->
              clonedTemplates[template.id] = template
              bodycb null
            cb null
        policies: (cb) ->
          policies.all (err, body) ->
            async.each body, (policy, bodycb) ->
              clonedPolicies[policy.id] = policy
              bodycb null
            cb null
        pages: (cb) ->
          pages.all (err, body) ->
            async.each body, (page, bodycb) ->
              clonedPages[page.id] = page
              bodycb null
            cb null
        messages: ['attributes', 'templates', 'policies', 'pages', (cb) ->
          messages.active (err, body) ->
            async.each body, (message, bodycb) ->
              clonedMessages[message.name] = message
              bodycb null
            cb null
        ]
        statuses: ['messages', (cb) ->
          statuses.active (err, body) ->
            async.each body, (status, bodycb) ->
              clonedStatuses[status.name] = status
              bodycb null
            cb null
        ]
        compile: ['statuses', (cb) ->
          clonedClient['clientId'] = req.params.id
          clonedClient['environment'] = config.env
          clonedClient['clonedDate'] = moment().toString()
          clonedClient['attributes'] = clonedAttributes
          clonedClient['templates'] = clonedTemplates
          clonedClient['policies'] = clonedPolicies
          clonedClient['pages'] = clonedPages
          clonedClient['messages'] = clonedMessages
          clonedClient['statuses'] = clonedStatuses
          clone = JSON.stringify clonedClient
          res.end()
          cb null
        ]

    exportClient()

app.post '/api/clients/:id/import/:short', busboy(immediate:yes), (req, res) ->
  req.busboy.on 'file', (name, file, filename, encoding, mimetype) ->
    return unless name is 'file'
    req.db.client = req.params.id
    [attributes, templates, policies, pages, messages, statuses] =
    req.db.use 'client/attributes', 'client/templates', 'client/policies', 'client/pages', 'client/messages', 'client/statuses'

    buf = []
    file.on 'data', (data) ->
      buf.push data
    file.on 'end', ->
      json = Buffer.concat(buf).toString()
      clonedClient = JSON.parse json

      replacedClient = {}

      replaceIds = ->
        attributesMap = {}
        templatesMap = {}
        policiesMap = {}
        pagesMap = {}
        messagesMap = {}
        statusesMap = {}
        replacedAttributes = {}
        replacedTemplates = {}
        replacedPolicies = {}
        replacedPages = {}
        replacedMessages = {}
        replacedStatuses = {}

        async.auto
          attributes: (cb) ->
            async.each clonedClient.attributes, (attribute, attrcb) ->
              attributesMap[attribute.id] = newId = uuid.v4().replace /-/g, ''
              replacedAttributes[attribute.name] = _.omit attribute, 'created', 'updated', 'published', 'version'
              replacedAttributes[attribute.name].id = newId
              replacedAttributes[attribute.name].values = {}
              replacedAttributes[attribute.name].aliases = []
              attrcb null

              async.forEachOf attribute.values, (value, key, valcb) ->
                oldId = key
                attributesMap[oldId] = newId = uuid.v4().replace /-/g, ''
                replacedAttributes[attribute.name].values[newId] = value
                replacedAttributes[attribute.name].aliases ?= []
                if replacedAttributes[attribute.name].default
                  if oldId is replacedAttributes[attribute.name].default
                    replacedAttributes[attribute.name].default = newId
                valcb null

                async.each (_.where attribute.aliases, 'value': oldId), (alias, aliascb) ->
                  replacedAttributes[attribute.name].aliases.push name: alias.name, value: newId
                  aliascb null
            cb null
          templates: (cb) ->
            async.each clonedClient.templates, (template, tmplcb) ->
              template = _.omit template, 'created', 'updated', 'published', 'version'
              oldId = template.id
              template.id = newId = uuid.v4().replace /-/g, ''
              templatesMap[oldId] = newId
              replacedTemplates[oldId] = template
              tmplcb null
            cb null
          policies: (cb) ->
            async.each clonedClient.policies, (policy, plcycb) ->
              policy = _.omit policy, 'created', 'updated', 'published', 'version'
              oldId = policy.id
              policy.id = newId = uuid.v4().replace /-/g, ''
              policiesMap[oldId] = newId
              replacedPolicies[oldId] = policy
              plcycb null
            cb null
          pages: (cb) ->
            async.each clonedClient.pages, (page, pagecb) ->
              page = _.omit page, 'created', 'updated', 'published', 'version'
              oldId = page.id
              page.id = newId = uuid.v4().replace /-/g, ''
              pagesMap[oldId] = newId
              replacedPages[oldId] = page
              pagecb null
            cb null
          messages: ['attributes', 'templates', 'policies', 'pages', (cb) ->
            allIds = {}
            _.assign allIds, attributesMap, templatesMap, policiesMap, pagesMap, messagesMap, statusesMap
            async.each clonedClient.messages, (message, msgcb) ->
              message = _.omit message, 'created', 'updated', 'published', 'version'
              oldMessageId = message.id
              message.id = newMessageId = uuid.v4().replace /-/g, ''
              messagesMap[oldMessageId] = newMessageId
              replacedMessages[message.name] = message
              async.forEachOf allIds, (newId, oldId, idcb) ->
                oldIdRegEx = new RegExp oldId, 'g'
                newMessage = JSON.stringify(replacedMessages[message.name]).replace oldIdRegEx, newId
                newMessage = JSON.parse newMessage
                replacedMessages[message.name] = newMessage
                idcb null
              msgcb null
            cb null
          ]
          statuses: ['messages', (cb) ->
            allIds = {}
            _.assign allIds, attributesMap, templatesMap, policiesMap, pagesMap, messagesMap, statusesMap
            async.each clonedClient.statuses, (status, stscb) ->
              status = _.omit status, 'created', 'updated', 'published', 'version'
              oldStatusId = status.id
              status.id = newStatusId = uuid.v4().replace /-/g, ''
              statusesMap[oldStatusId] = newStatusId
              replacedStatuses[status.name] = status
              async.forEachOf allIds, (newId, oldId, idcb) ->
                oldIdRegEx = new RegExp oldId, 'g'
                newStatus = JSON.stringify(replacedStatuses[status.name]).replace oldIdRegEx, newId
                newStatus = JSON.parse newStatus
                replacedStatuses[status.name] = newStatus
                idcb null
              stscb null
            cb null
          ]
          compile: ['statuses', (cb) ->
            allIds = {}
            _.assign allIds, attributesMap, templatesMap, policiesMap, pagesMap, messagesMap, statusesMap
            replacedClient['clientId'] = req.params.id
            replacedClient['environment'] = config.env
            replacedClient['importedDate'] = moment().toString()
            replacedClient['uuidmap'] = allIds
            replacedClient['attributes'] = replacedAttributes
            replacedClient['templates'] = replacedTemplates
            replacedClient['policies'] = replacedPolicies
            replacedClient['pages'] = replacedPages
            replacedClient['messages'] = replacedMessages
            replacedClient['statuses'] = replacedStatuses
            dupeCount = 0
            clientString = JSON.stringify(_.omit replacedClient, 'uuidmap')
            async.forEachOf allIds, (newId, oldId, idcb) ->
              # checking to make sure old ids do not appear more than once
              # old ids are used as a key for some of the resultant json objects
              dupeCount += clientString.split(oldId).length - 1
              idcb
            if dupeCount isnt Object.keys(replacedTemplates).length + Object.keys(replacedPolicies).length + Object.keys(replacedPages).length
              console.log 'Something did not get replaced'
              cb null
            else
              console.log 'all good!'
              importClient()
              replaced = JSON.stringify(replacedClient)
              short = req.params.short
              res.end()
              cb null
          ]

      importClient = ->
        async.auto
          attributes: (cb) ->
            async.each replacedClient.attributes, (attribute, attrcb) ->
              attributes.import attribute, (err) ->
                return err if err
                attrcb null
              cb null
          templates: (cb) ->
            async.each replacedClient.templates, (template, tmplcb) ->
              templates.import template, (err) ->
                return err if err
                tmplcb null
              cb null
          policies: (cb) ->
            async.each replacedClient.policies, (policy, plcycb) ->
              policies.import policy, (err) ->
                return err if err
                plcycb null
              cb null
          pages: (cb) ->
            async.each replacedClient.pages, (page, pagecb) ->
              pages.import page, (err) ->
                return err if err
                pagecb null
              cb null
          messages: ['attributes', 'templates', 'policies', 'pages', (cb) ->
            async.each replacedClient.messages, (message, msgcb) ->
              messages.import message, (err) ->
                return err if err
                msgcb null
              cb null
          ]
          statuses: ['messages', (cb) ->
            async.each replacedClient.statuses, (status, stscb) ->
              statuses.import status, (err) ->
                return err if err
                stscb null
              cb null
          ]

      replaceIds()

app.post '/api/keypair', (req, res, next) ->
  pair = forge.pki.rsa.generateKeyPair bits: 1024
  res.json
    privateKey: forge.pki.privateKeyToPem(pair.privateKey).replace /\r/g, ''

    publicKey: forge.pki.publicKeyToPem(pair.publicKey).replace /\r/g, ''

app.post '/api/dns/check', (req, res) ->
  {Dns} = require 'shared/dns'
  dns = new Dns
  dns.clientTest req.body.id, req.body.prefix, (err) ->
    return res.json err: err if err?
    res.json success: yes

app.use '/api', (err, req, res, next) ->
  return next err unless req.xhr
  res.status(500).json ok: no, err: err

# ________    _______    _________   _________ __          _____  _____
# \______ \   \      \  /   _____/  /   _____//  |_ __ ___/ ____\/ ____\
#  |    |  \  /   |   \ \_____  \   \_____  \\   __\  |  \   __\\   __\
#  |    `   \/    |    \/        \  /        \|  | |  |  /|  |   |  |
# /_______  /\____|__  /_______  / /_______  /|__| |____/ |__|   |__|
#         \/         \/        \/          \/
#
# DNS Files:                                  Q1 2016 - njw
#
# webadmin\app.coffee                       ~ Line 500 (this file)
# webadmin\public\stylesheets\sb-admin.css  ~ Line 250
# shared\dns.coffee                         ~ Line 1
# webadmin\views\index.jade                 ~ Line 775
# webadmin\public\javascripts\index.coffee  ~ Line 950

conn = config.db.url

dns = new Dns

# dns page load

app.get '/dns/clients', (req, res) ->
  dns.get_clients (err, ret) ->
    res.json err: err, ret: ret

app.get '/dns/records', (req, res) ->
  dns.get_records (err, ret) ->
    res.json err: err, ret: ret

# dns actionqueue

app.get '/dns/actionqueue', (req, res) ->
  dns.get_actionqueue (err, ret) ->
    res.json err: err, ret: ret

app.post '/dns/actionqueue', (req, res) ->
  dns.post_actionqueue req.body.status, req.body.cron, (err) ->
    res.json err: err

# dns settings

app.get '/dns/settings', (req, res) ->
  dns.get_settings (err, ret) ->
    res.json err: err, ret: ret

app.post '/dns/settings', (req, res) ->
  dns.post_settings req.body.email, req.body.zone, (err) ->
    res.json err: err

# dns records

app.post '/dns/delete_record', (req, res) ->
  dns.delete_record req.body.id, (err) ->
    res.json err: err

app.post '/dns/verify_records', (req, res) ->
  dns.verify_records req.body.records, (err, ret) ->
    res.json err: err, ret: ret

app.post '/dns/record', (req, res) ->
  dns.post_record req.body.id, req.body.client_id, req.body.isglobal, req.body.name, req.body.type, req.body.domain, req.body.value, (err) ->
    res.json err: err




app.post '/lib/load_dns_tests', (req, res) ->
  return res.json err: err if err?
  pg.connect conn, (err, client, done) ->
    return res.json err: err if err?
    client.query "select * from keystone.dns_records where isglobal = true or client_id = '00000000-0000-0000-0000-000000000000' order by isglobal desc, id", (err, result) ->
      done()
      return res.json err: err if err?
      return res.json result.rows

app.post '/lib/update_test', (req, res) ->
  return res.json err: err if err?
  pg.connect conn, (err, client, done) ->
    return res.json err: err if err?
    client_id = if req.body.client_id is 'default' then '00000000-0000-0000-0000-000000000000' else req.body.client_id
    if req.body.id == '0'
      created = moment().format()
      client.query 'insert into keystone.dns_records (client_id, isglobal, name, type, domain, value, created) values ($1, $2, $3, $4, $5, $6, $7)',
      [client_id, req.body.isglobal, req.body.name, req.body.type, req.body.domain, req.body.value, new Date()],
      (err, result) ->
        done()
        return res.json err: err if err?
        res.json success: yes
    else
      client.query 'update keystone.dns_records set client_id = $1, isglobal = $2, name = $3, type = $4, domain = $5, value = $6 where id = $7',
      [client_id, req.body.isglobal, req.body.name, req.body.type, req.body.domain, req.body.value, req.body.id],
      (err, result) ->
        done()
        return res.json err: err if err?
        res.json success: yes

app.post '/lib/delete_test', (req, res) ->
  return res.json err: err if err?
  pg.connect conn, (err, client, done) ->
    return res.json err: err if err?
    client.query 'delete from keystone.dns_records where id = $1', [req.body.id], (err, result) ->
      done()
      return res.json err: err if err?
      res.json success: yes

app.post '/lib/new_client_job', (req, res) ->
  return res.json err: err if err?
  pg.connect conn, (err, client, done) ->
    return res.json err: err if err?
    client.query 'insert into keystone.dns_job (type, client_id) values ($1, $2) returning id',
    ['client', req.body.id], (err, result) ->
      done()
      return res.json err: err if err?
      res.json id: result.rows[0].id

app.post '/lib/get_client_doc', (req, res) ->
  return res.json err: err if err?
  pg.connect conn, (err, client, done) ->
    return res.json err: err if err?
    client.query "select doc from keystone.clients where id = $1 and upper(valid) = 'infinity'",
    [req.body.client_id], (err, result) ->
      done()
      return res.json err: err if err?
      res.json result.rows[0].doc

app.post '/lib/run_test', (req, res) ->
  return res.json err: err if err?
  res.json test_id: req.body.test_id

app.post '/lib/start_tests', (req, res) ->
  {Dns} = require 'shared/dns'
  dns = new Dns
  args =
    to: 'nathan.weisser@softvu.com'
    from: 'dns-tool@softvu.com'
    subject: 'Test Subject'
    html: 'Test Body'
  dns.email args
  return res.json err: err if err?
  # res.json client_id: req.body.client_id, mode: req.body.mode
  res.json client_id: 'hello', mode: 'goodbye'

# END DNS STUFF

app.all '*', (req, res) ->
  res.render 'index'

if require.main is module
  server = app.listen app.get('port'), ->
    console.log "Express server listening on port #{app.get('port')}"
