isValidEmailAddress = (email) ->
  atidx = email.indexOf '@'
  email.length >= 3 && ~atidx && atidx < email.length - 1

Admin = angular.module 'Admin', [
  'ngResource'
  'ngRoute'
  'angularMoment'
  'angularFileUpload'
  'angular.filter'
  'ui.bootstrap'
  'ui.ace'
  'frapontillo.bootstrap-switch'
]

Admin.service 'socket', ['$rootScope', class SocketService
  STATES = ['connected', 'connecting', 'disconnected']

  MAX_RETRIES = 20

  # create a simpler object that has the keys set
  # to the string value of the state
  STATE = _.object STATES, STATES

  constructor: (@rootScope) ->
    @handles = []
    @_state = STATE.disconnected
    @_retry = timeout: 1000, attempts: 0

    onbefore = window.onbeforeunload
    window.onbeforeunload = =>
      delete @socket.onclose
      onbefore?()

  connect: (cb) ->
    return cb?() unless @_state is STATE.disconnected

    @_state = STATE.connecting
    timer = setTimeout retry = =>
      @socket?.close()
      @_retry.attempts += 1

      # TODO many a suggestion talked about a window.reload() here...
      # i think maybe it's a modal question?
      warn = 'websocket timeout retries exhausted...'
      return console.warn warn if @_retry.attempts > MAX_RETRIES

      @socket = new SockJS '/stream/events'
      @socket.onopen = =>
        clearTimeout timer

        @socket.onmessage = @_onmessage
        @socket.onclose = @_onclose

        @_retry = timeout: 1000, attempts: 0
        @_state = STATE.connected
        cb?()

      timer = setTimeout retry, @_retry.timeout = 2 * @_retry.timeout
    , 1

  @state: -> @_state

  _onmessage: (e) =>
    obj = JSON.parse e.data
    @handles.forEach (fn) -> fn obj

  _onclose: =>
    @socket.close()
    @socket = null

    @_state = STATE.disconnected
    @connect()

  on: (eventName, cb) ->
    @connect()
    @handles.push @_wrap cb

  _wrap: (cb) =>
    (message) =>
      @rootScope.$apply =>
        cb.apply? @socket, [message]

  emit: (eventName, data, cb) ->
    console.error 'deprectated: anyone using this?'
]

Admin.factory 'Client', ($resource) ->
  $resource 'api/clients/:id', null,
    query: isArray: no

Admin.factory 'User', ($resource) ->
  $resource 'api/users/:id'

Admin.value 'type', ''
Admin.controller 'ConfirmClose', [
  '$scope', '$uibModal', '$uibModalInstance', 'type', 'refItem', 'scopeItem',
  ($scope, $uibModal, $uibModalInstance, type, refItem, scopeItem) ->
    $scope.type = type

    scopeItem = refItem unless !refItem
    $scope.confirm = -> $uibModalInstance.close scopeItem
    $scope.dismiss = -> $uibModalInstance.dismiss()
]

Admin.service 'dismissCheck', ['$uibModal', ($uibModal) ->
  dismiss: (modalInstance, item, ref) ->
    inst = $uibModal.open
      templateUrl: 'dismissCheck-modal.html'
      backdrop: 'static'
      controller: 'ConfirmClose'
      resolve:
        refItem: -> ref
        scopeItem: -> item

    inst.result.then (closeItem)->
      modalInstance.close closeItem
]

# TODO this also exists in the regular app...
Admin.directive 'bsDatetimePicker', ->
  require: '?ngModel'
  link: (scope, elm, attrs, ngModel) ->
    elm.datetimepicker defaultDate: new Date

    attrs.$observe 'disabled', (val) ->
      elm.data('DateTimePicker')[if val then 'disable' else 'enable']()

    elm.on 'change', (e) ->
      # TODO this is a bit hacky...
      # https://github.com/angular/angular.js/wiki/Anti-Patterns
      # my problem is that you can't get much higher than an event in the "call stack"
      # this is really related to how quickly in combo with a $http request something happens
      return if scope.$$phase

      scope.$apply ->
        if attrs.dateFormat
          ngModel.$setViewValue elm.data('DateTimePicker').getDate().format attrs.dateFormat
        else
          # valueOf returns the actual datetime instead of the moment object...
          ngModel.$setViewValue elm.data('DateTimePicker').getDate().valueOf()

    ngModel.$render = ->
      elm.data('DateTimePicker').setDate ngModel.$viewValue

Admin.directive 'setClient', ->
  link: ($scope, $element, $attributes) ->
    $element.on 'click', ->
      form = $ '<form action="../setclient" method="post" target="_blank"/>'
      form.append $('<input type="hidden"/>').attr name: 'id', value: $attributes.setClient
      form.appendTo($element).submit().remove()
      $scope.isOpen = no
      $('.dropdown.open .dropdown-toggle').dropdown 'toggle'

Admin.directive 'bsRadio', ->
  scope: type: '=ngModel'
  transclude: yes
  template: '''
  <i class="fa fa-check-square-o truthy" />
  <i class="fa fa-square-o falsy" />
  <input type='checkbox' />
  <span ng-transclude />
  '''
  link: ($scope, $element, $attributes) ->
    $element.addClass 'checks'

    unless 'readOnly' of $attributes
      $element.on 'click', (e) ->
        fs = $($element).find(':checkbox').parents('fieldset')
        return if fs?.is(':disabled')

        e.preventDefault()
        $scope.$apply -> $scope.type = $attributes.value

    $scope.$watch 'type', (nv, ov) ->
      $scope.selected = nv is $attributes.value
      $element.find('input').prop 'checked', $scope.selected
      $element.toggleClass 'checked', $scope.selected
      $element.toggleClass $attributes.bsRadioSelected, $scope.selected

Admin.directive 'bsConfirmClick', ($uibModal) ->
  priority: -1
  link: (scope, element, attributes) ->
    element.bind 'click', (e) ->
      e.preventDefault()
      e.stopImmediatePropagation()

      inst = $uibModal.open
        templateUrl: 'dialogs/confirm.html'
        keyboard: no
        backdrop: 'static'
        scope: _.assign scope.$new(),
          message: attributes.bsConfirmClick
          permanent: !!attributes.bsConfirmClickPermanent

      inst.result.then -> scope.$eval attributes.ngClick

Admin.directive 'svPagingRange', ['$location', ($location) ->
  templateUrl: 'paging/range.html'
  replace: yes
  link: (scope, elm, attrs) ->
    tmp = _.mapValues _.pick($location.search(), ['from', 'to']), (dt) -> moment dt
    scope.range =
      from: tmp.from or moment().startOf 'month'
      to: (tmp.to or moment()).endOf 'day'

    elm.daterangepicker
      format: 'YYYY-MM-DD'
      opens: 'left'
      startDate: moment(scope.range.from)
      endDate: moment(scope.range.to)
      ranges:
        'Today': [moment(), moment()]
        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)]
        'Last 7 Days': [moment().subtract('days', 6), moment()]
        'Last 30 Days': [moment().subtract('days', 29), moment()]
        'This Month': [moment().startOf('month'), moment().endOf('month')]
        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
    , (start, end) ->
      scope.$apply ->
        scope.range.from = start
        scope.range.to = end

      for own k, dt of scope.range
        $location.search k, moment(dt).format 'YYYY-MM-DD'
]

Admin.directive 'bsSaveButton', ->
  scope: onSave: '&'
  restrict: 'E'
  require: '^form'
  template: '''
  <button class="btn btn-primary" name="save" ng-click="save()" ng-disabled="form.$pristine">Save</button>
  <span class="spinner spinner-small no-animate" ng-if="form.$submitting"></span>
  <span class="fa fa-fw fa-check text-success" ng-if="form.$saved && form.$pristine"></span>

  '''
  link: (scope, element, attributes, form) ->
    scope.form = form
    element.addClass 'bs-save-button'

    scope.save = ->
      scope.start = (new Date).getTime()
      scope.form.$submitting = yes
      scope.form.$saved = no
      scope.onSave()

    do (old = scope.form.$setPristine) ->
      scope.form.$setPristine = ->
        setTimeout ->
          scope.form.$submitting = no
          scope.form.$saved = yes
        , Math.max 0, 600 - ((new Date).getTime() - scope.start) # force at least 600ms
        old()

Admin.directive 'goBack', ($window) ->
  transclude: yes
  template: '<a style="cursor: pointer" ng-transclude></a>'
  link: (scope, element, attrs) ->
    element.on 'click', -> $window.history.back()

Admin.directive 'ngListNewline', ->
  restrict: 'A',
  require: 'ngModel',
  link: (scope, element, attr, ngModel) ->
    ngModel.$parsers.push (text) ->
      text.split('\n').filter (x) -> x.trim().length > 0

    ngModel.$formatters.push (array) ->
      if array?.length then array.join '\n' else ''

Admin.config ($routeProvider, $locationProvider) ->
  $routeProvider
    .when '/dashboard',
      templateUrl: 'dashboard.html', controller: 'Dashboard'
    .when '/jobs',
      templateUrl: 'jobs.html', controller: 'Jobs'
    .when '/jobs/:client',
      templateUrl: 'jobs.html', controller: 'Jobs'
    .when '/clients/:id',
      templateUrl: 'client.html', controller: 'Client'
    .when '/clients',
      templateUrl: 'clients.html', controller: 'Clients', reloadOnSearch: no
    .when '/apikeys',
      templateUrl: 'apikeys.html', controller: 'APIKeys', reloadOnSearch: no
    .when '/users',
      templateUrl: 'users.html', controller: 'Users', reloadOnSearch: no
    .when '/billing',
      templateUrl: 'billing.html', controller: 'Billing'
    .when '/templates',
      templateUrl: 'templates.html', controller: 'Templates'
    .when '/dns',
      templateUrl: 'dns.html', controller: 'DNS'
    .when '/optouts',
      templateUrl: 'optouts.html', controller: 'Optouts', reloadOnSearch: no
    .when '/aws',
      templateUrl: 'aws.html', controller: 'AWS'
    .otherwise redirectTo: '/dashboard'

  $locationProvider.html5Mode true

Admin.run ($rootScope, $http, $location, $timeout, $q, $uibModal, socket, Client) ->
  $rootScope.isActive = (path) -> ~$location.path().indexOf path

  $rootScope.clients = Client.query()
  $rootScope.clients.$promise.then (clients) ->
    # TODO i haven't found a good resource for why this is necessary or how to get around it using iterators
    $rootScope.clients_array = (client for own id, client of $rootScope.clients when id[0] isnt '$')

  $http.get('api/events').success (events) ->
    $rootScope.events = events

  $rootScope.polls = {}
  socket.on 'event', (event) ->
    return unless event.type

    if ~event.type.indexOf 'heartbeat'
      event.at = moment(event.at).subtract 1, 'second'
      $rootScope.polls[event.type[...event.type.indexOf '.heartbeat']] = event
    else
      $rootScope.events.unshift event

      $timeout ->
        $rootScope.events = _.take $rootScope.events, 50
      , 1000

  $rootScope.show = (obj) ->
    $uibModal.open
      templateUrl: 'json/raw.html'
      size: 'lg'
      scope: _.assign $rootScope.$new(), obj: obj

  do (ace) ->
    dom = ace.require('ace/lib/dom')
    commands = ace.require('ace/commands/default_commands').commands

    commands.push
      name: 'Toggle Fullscreen'
      bindKey: 'F11'
      exec: (editor) ->
        dom.toggleCssClass document.body, 'fullScreen'
        dom.toggleCssClass editor.container, 'fullScreen-editor'
        editor.resize()

    $rootScope.aceLoaded = (editor) ->
      ace.require('ace/config')
        .set 'workerPath', '../bower_components/ace-builds/src-min-noconflict'
      ace.require('ace/ext/language_tools')

      editor.setTheme 'ace/theme/monokai'
      editor.getSession().setMode 'ace/mode/html'
      editor.setShowPrintMargin no
      editor.getSession().setUseWrapMode yes

Admin.controller 'Services', ($scope, $http, $interval) ->
  $interval $scope.apply, 2000

  $scope.services = _.map ['actionqueue', 'eventhose', 'pipeline', 'webhooks'], (name) ->
    name: name
    running: yes
    restarting: no

  $scope.outdated = (service) ->
    at = $scope.polls[service.name]?.at
    not at or 1 <= moment().diff at, 'minutes', yes

  $scope.stop = (service) ->
    service.restarting = yes
    $http.post('api/services/stop', name: service.name).then ->
      service.running = no
      service.restarting = no

  $scope.start = (service) ->
    service.restarting = yes
    $http.post('api/services/start', name: service.name).then ->
      service.restarting = no
      service.running = yes

  $scope.restart = (service) ->
    service.restarting = yes
    $http.post('api/services/restart', name: service.name).then ->
      service.restarting = no
      service.running = yes

Admin.controller 'Dashboard', ->

Admin.controller 'Jobs', ($scope, $http, $routeParams, $interval, $timeout) ->
  id = $routeParams.client
  clientJobs = "api/jobs/#{id}"
  allClientsJobs = 'api/jobs'
  currentJobs = null
  # $interval must be saved to a promise object in order to be stopped
  # when switching scopes
  jobsPromise = $interval ->
    $scope.$reloading = yes
    jobsGet ->
      $timeout ->
        $scope.$reloading = no
      , 2000
  , 60000

  jobsGet = (cb) ->
    $http.get(currentJobs).success (data) ->
      $scope.jobs = data
      cb?()

  jobsReload = ->
    jobsPromise

  if id
    $scope.client = id
    currentJobs = clientJobs
    jobsGet jobsReload
  else
    currentJobs = allClientsJobs
    jobsGet jobsReload

  $scope.$on '$destroy', ->
    $interval.cancel jobsPromise

  $scope.trigger = (job) ->
    job.data.original_at = job.at
    job.at = new Date()
    job.data.triggered = yes
    $http.put('api/jobs', job).success (data) ->

Admin.factory 'ContextMenuService', ->
  menu: null # track open context menus so we can close them, leaving only one open

Admin.directive 'contextMenu', [
  '$compile', '$parse', '$templateCache', 'ContextMenuService',
  ($compile, $parse, $templateCache, contextMenuService) ->
    scope: no
    link: (scope, elem, attrs) ->
      html = $templateCache.get attrs.contextMenu
      el = $compile(html)(scope)

      elem.on 'click', '.context', (e) ->
        e.stopPropagation()
        scope.menu.contextmenu 'show', e

      $('body').append el
      scope.menu = $(elem[0]).contextmenu
        target: el
        before: ->
          contextMenuService.menu?.contextmenu 'closemenu'
          contextMenuService.menu = scope.menu
          scope.$apply -> scope.item = $parse(attrs.contextMenuItem)(scope)
          return yes # tell context menu to open
]

Admin.directive 'bs`FileInput`', ->
  scope: 'onReset': '&'
  link: (scope, elm, attrs) ->
    elm.fileinput()
    elm.on 'clear.bs.fileinput', ->
      scope.$apply -> scope.onReset()
    elm.on 'reset.bs.fileinput', ->
      scope.$apply -> scope.onReset()
    scope.$on 'clear.fileinput', -> elm.fileinput 'clear'

Admin.directive 'fileModel', ->
  require: '?ngModel'
  link: (scope, elm, attrs, ngModel) ->
    elm.on 'change.bs.fileinput', ->
      scope.$apply ->
        ngModel.$setViewValue elm[0].files[0]

Admin.directive 'readfile', ->
  scope: readfile: '='
  link: (scope, elm, attrs) ->
    elm.on 'change.bs.fileinput', ->
      scope.$apply -> scope.readfile = elm[0].files[0]

Admin.controller 'Clients', ($scope, $http, $location, $routeParams, $uibModal, $window) ->
  $scope.show = archived: if $routeParams.archived is 'yes' then yes else no
  $scope.search = {}

  $scope.$watch 'clients', (clients) ->
    return unless clients
    $scope.ordered = for own id, client of clients when id[0] isnt '$'
      id: id
      name: client.name
      short: client.short
      archive: client.archive or no
      integration: client.integrations?._current or ''
  , yes

  $scope.archiveClient = (id) ->
    $scope.clients[id].archive = yes
    $http.post 'api/clients', client: $scope.clients[id]

  $scope.deleteClient = (id) ->
    return unless confirm 'This operation is irreversible. Continue?'
    $http.delete("api/clients/#{id}").success ->
      delete $scope.clients[id]

  $scope.deleteEvents = (id) ->
    return unless confirm 'This operation is irreversible. Continue?'
    $http.delete("api/clients/#{id}/events").success ->

  $scope.deleteMessages = (id) ->
    return unless confirm 'This operation is irreversible. Continue?'
    $http.delete("api/clients/#{id}/messages").success ->

  $scope.toggleArchived = ->
    $location.search archived: if $scope.show.archived then 'yes' else undefined

  $scope.showFilter = (item) ->
    return yes if $scope.show.archived or not item.archive

  $scope.create = ->
    inst = $uibModal.open
      templateUrl: 'client-create.html'
      controller: ($scope, $http, $uibModalInstance, $timeout) ->
        $scope.client = {}
        $scope.forms = {}

        setErrors = (start) ->
          $timeout ->
            $scope.forms.client.$invalid = yes
            $scope.$creating = no
            $scope.forms.client.$setPristine()
          , Math.max 0,800 - (Date.now() - start)

        requiredVariables = (client, cb) ->
          formErrors = {}
          if not client.name
            formErrors.name = yes
          cb formErrors

        $scope.create = ->
          $scope.formErrors = {}
          $scope.$creating = yes
          $scope.errorMessage = undefined
          $scope.$created = no
          start = Date.now()

          requiredVariables $scope.client, (formErrors) ->
            if not _.isEmpty formErrors
              $scope.formErrors = formErrors
              setErrors start
            else
              unless not $scope.client.manager?.length
                if not isValidEmailAddress $scope.client.manager
                  $scope.formErrors.manager = yes
                  setErrors start
                  return
              $http.post('api/clients', client: $scope.client)
              .success (data) ->
                #TODO Error handling needs to be addressed
                $scope.client.id = data.id
                $scope.client.version = data.version
                $timeout ->
                  $scope.$creating = no
                  $scope.$created = yes
                  $scope.forms.client.$setPristine()
                , Math.max 0,800 - (Date.now() - start)
              .error (err) ->
                $scope.errorMessage = err
                setErrors start

        $scope.close = ->
          if $scope.forms.client.$dirty or not _.isEmpty $scope.formErrors
            $uibModalInstance.dismiss()
          else
            $uibModalInstance.close $scope.client

    inst.result.then (client) ->
      unless not client.id
        $scope.clients[client.id] = client
        $location.path "/clients/#{client.id}"

  $scope.export = (client, short) ->
    $http.post("api/clients/#{client}/export", short: short)
      .success (data) ->
        $window.open("api/clients/#{client}/export")
      .error (err) ->
        $scope.errorMessage = err
        setErrors start

  $scope.showAttributes = (id) ->
    inst = $uibModal.open
      templateUrl: 'client-attributes.html'
      controller: ($scope, $http, $uibModalInstance) ->
        $scope.path = {}

        $http.get("api/attributes/#{id}").success (attributes) ->
          $scope.attributes =
            _(attributes)
              .map (attr, id) ->
                _.assign attr,
                  id: id
                  values: _(attr.values)
                    .map (value, id) -> _.assign value, id: id
                    .sortBy 'name'
                    .value()
              .sortBy 'name'
              .value()

          $scope.attributes.forEach (attr) ->
            $scope.path[attr.id] = attr.values[0].id

        $scope.make = ->
          JSON.stringify _.map($scope.path, (val, key) -> "#{key}/#{val}"), null, 2

Admin.controller 'Client', ($scope, $http, $route, $timeout, $uibModal) ->
  $scope.clients.$promise.then ->
    $scope.client = $scope.clients[$route.current.params.id]
    $scope.client.dkim ?= {}
    if $scope.client.debug then $scope.client.email_mode = 'Debug'
    $scope.client.email_mode ?= 'Live'
    $scope.creating_aws = no

    return if $scope.client.timezone
    $scope.client.timezone = jstz.determine().name()
    $scope.form.$setDirty()

  $http.get("api/apikeys/#{$route.current.params.id}").success (data) ->
    $scope.apikeys = data

  get_buckets = ->
    $http.get 'api/clients/getBuckets'
    .success (results) ->
      $scope.aws_buckets = _.pluck results.buckets.Buckets, 'Name'
      $scope.aws_config = results.config
      if $scope.client.credentials?.aws?.private?
        $scope.private_folder = $scope.client.credentials.aws.private.bucket + '/' + $scope.client.credentials.aws.private.folder

  get_buckets()

  $scope.clear_selection = ->
    $scope.client?.credentials?["aws"] = {}
    $scope.form.$setDirty()

  $scope.save = ->
    # TODO move this webhook option setting to the server side
    $scope.client.webhooks?.enabled_webhooks = []
    if $scope.client.webhooks?.use_integration is true and $scope.client.integrations?._current isnt ''
      if $scope.client.integrations._current is 'velocify'
        $scope.client.webhooks.url =
          'http://localhost:9998/' + $scope.client.integrations._current + '?client=' + $scope.client.id
      else if $scope.client.integrations._current is 'salesforce'
        $scope.client.webhooks.url =
          'http://localhost:9997/' + $scope.client.integrations._current + '?client=' + $scope.client.id
    if $scope.client.webhooks
      _.forEach $scope.client.webhooks.options, (v, k) ->
        $scope.client.webhooks.enabled_webhooks.push k if v.enabled is true
    if $scope.client.email_mode in ['Live', 'Internal', 'Off']
      $scope.client.debug = false
    else if $scope.client.email_mode is 'Debug'
      $scope.client.debug = true

    if $scope.client.credentials?["aws"]?.bucket? || $scope.client.credentials?["aws"]?.private?
      $scope.client.credentials["aws"].key = $scope.aws_config.accessKeyId
      $scope.client.credentials["aws"].secret = $scope.aws_config.secretAccessKey

    $http.post('api/clients', client: $scope.client).success (data) ->
      $scope.clients[data.id] = _.assign $scope.client, _.pick data, 'version'
      $scope.form.$setPristine()
      get_buckets()

  $scope.create_private_folder = ->
    $scope.creating_aws = yes
    start = Date.now()
    $http.post "api/clients/createFolder/#{$scope.client.id}"
    .success (results) ->
      unless $scope.client.credentials?
        $scope.client.credentials = aws: {}
      $scope.client.credentials["aws"].private = {}
      $scope.client.credentials["aws"].private.bucket = results.bucket_name
      $scope.client.credentials["aws"].private.folder = results.folder_name
      $timeout ->
        $scope.creating_aws = no
        $scope.save()
      , Math.max 0,800 - (Date.now() - start)


  $scope.createS3Bucket =  ->
    $scope.creatingBucket = true
    $scope.client.credentials = _.assign {}, $scope.client.credentials,
     aws:
       bucket: ' '
       key: ''
       secret: ''

    $http.post("api/clients/createBucket/", bucketName:$scope.client.name).success (data) ->
      $scope.client.credentials["aws"].bucket = data.bucketName
      $scope.client.credentials["aws"].key = data.accessKeyId
      $scope.client.credentials["aws"].secret = data.secretAccessKey

      if $scope.client.template == "6e2a0c50257c11e59604e772aaac09c0"
        $http.post "api/clients/copyTemplateBucket/",
          templateBucketName:"sv-solartemplate"
          newBucketName: data.bucketName
        $scope.creatingBucket = false
        $scope.save()
      else
        $scope.creatingBucket = false
        $scope.save()

  $scope.revoke = (key) ->
    $http.delete("api/apikeys/#{key.id}").success (data) ->
      key.revoked = data.key.revoked

  $scope.addDKIM = ->
    inst = $uibModal.open
      templateUrl: 'client/addDKIM.html'
      controller: ($scope, $uibModalInstance) ->
        $scope.key = {}
        $scope.add = -> $uibModalInstance.close $scope.key

    inst.result.then (key) ->
      $scope.client.dkim.keys ?= []
      $scope.client.dkim.keys.push key
      $scope.form.$setDirty()

  $scope.generateDKIM = ->
    inst = $uibModal.open
      templateUrl: 'client/generateDKIM.html'
      controller: ($scope, $uibModalInstance) ->
        $scope.key = {}
        $scope.generate = -> $uibModalInstance.close $scope.key

    inst.result.then (key) ->
      $http.post('api/keypair').success (keypair) ->
        $scope.client.dkim.keys ?= []
        $scope.client.dkim.keys.push _.assign key, keypair
        $scope.form.$setDirty()

  $scope.viewDKIM = (key) ->
    inst = $uibModal.open
      templateUrl: 'client/addDKIM.html'
      scope: _.assign $scope.$new(), ro: yes, key: key

  $scope.removeDKIM = (key) ->
    _.remove $scope.client.dkim.keys, key
    $scope.form.$setDirty()

  $scope.setupDNS = ->
    $scope.checking_dns = yes
    $http.post('api/dns/check', id: $scope.client.id, prefix: $scope.client.svemails.prefix).success (result) ->
      $scope.checking_dns = no
      if result.success
        $scope.client.svemails.dnscheck = prefix: $scope.client.svemails.prefix, valid: yes, checked: new Date()

  $scope.import = (id, short) ->
    inst = $uibModal.open
      templateUrl: 'client-import.html'
      controller: 'ClientImportModal'
      resolve:
        id: -> id
        short: -> short

Admin.controller 'Users', ($scope, $http, $q, $location, $uibModal) ->
  $scope.search = $location.search()
  $scope.search.disabled = if $scope.search.disabled is 'yes' then yes else no

  $http.get('api/users?limit=100')
    .success (users) ->
      $scope.users = users

  $scope.$watch 'users', ->
    _.forEach _.values($scope.users), (user) ->
      user.fullname = user.firstname + ' ' + user.lastname
  , yes

  $scope.$watch 'search', ->
    $location.search disabled: if $scope.search.disabled then 'yes' else undefined
  , yes

  $scope.searching = (item) ->
    return yes if $scope.search.disabled or not item.disabled

  $scope.create = ->
    inst = $uibModal.open
      templateUrl: 'user-edit.html'
      controller: ($scope, $http, $uibModalInstance, $timeout, dismissCheck) ->
        $scope.$isNewUser = yes
        $scope.user = client: ''
        user_ref = {}
        angular.copy $scope.user, user_ref

        setErrors = (start) ->
          $timeout ->
            $scope.forms.user.$invalid = yes
            $scope.$editing = no
            $scope.forms.user.$setPristine()
          , Math.max 0,800 - (Date.now() - start)

        requiredVariables = (user, cb) ->
          formErrors = {}
          if not user.username?.length
            formErrors.username = yes
          if $scope.$isNewUser and not user.password
            formErrors.password = yes
          cb formErrors

        $scope.save = ->
          $scope.formErrors = {}
          $scope.$editing = yes
          $scope.errorMessage = undefined
          $scope.$saved = no
          start = Date.now()

          requiredVariables $scope.user, (formErrors) ->
            if not _.isEmpty formErrors
              $scope.formErrors = formErrors
              setErrors start
            else
              delete $scope.user.fullname
              $http.post('api/users', user: $scope.user)
                .success (data) ->
                  #TODO Error handling needs to be addressed
                  $scope.user.id = data.id
                  $scope.user.version = data.version
                  $timeout ->
                    $scope.$editing = no
                    $scope.$saved = yes
                    angular.copy $scope.user, user_ref
                    $scope.forms.user.$setPristine()
                  , Math.max 0,800 - (Date.now() - start)
                .error (err) ->
                  $scope.errorMessage = err
                  setErrors start

        $scope.close = ->
          console.log 'forms.user:', $scope.forms.user
          if $scope.$saved and $scope.forms.user.$pristine
            $uibModalInstance.close $scope.user
          else if $scope.forms.user.$dirty or not _.isEmpty $scope.formErrors
            dismissCheck.dismiss $uibModalInstance, $scope.user, user_ref
          else
            $uibModalInstance.dismiss()

    inst.result.then (user) ->
      console.log 'users: ', $scope.users
      unless not user
        $scope.users.push user
      console.log 'post users: ', $scope.users

  $scope.edit = (user) ->
    inst = $uibModal.open
      templateUrl: 'user-edit.html'
      controller: ($scope, $http, $uibModalInstance, $timeout, dismissCheck) ->
        $scope.$isNewUser = no
        $scope.user = angular.copy user
        user_ref = {}
        angular.copy $scope.user, user_ref

        setErrors = (start) ->
          $timeout ->
            $scope.forms.user.$invalid = yes
            $scope.$editing = no
            $scope.forms.user.$setPristine()
          , Math.max 0,800 - (Date.now() - start)

        requiredVariables = (user, cb) ->
          formErrors = {}
          if not user.username?.length
            formErrors.username = yes
          cb formErrors

        $scope.save = ->
          $scope.formErrors = {}
          $scope.$editing = yes
          $scope.errorMessage = undefined
          $scope.$saved = no
          start = Date.now()

          requiredVariables $scope.user, (formErrors) ->
            if not _.isEmpty formErrors
              $scope.formErrors = formErrors
              setErrors start
            else
              delete $scope.user.fullname
              $http.put('api/users/' + $scope.user.username, user: $scope.user)
                .success (data) ->
                  console.log 'data: ', data
                  $scope.user.version = data.version
                  $timeout ->
                    $scope.$editing = no
                    $scope.$saved = yes
                    angular.copy $scope.user, user_ref
                    angular.copy $scope.user, user
                    $scope.forms.user.$setPristine()
                  , Math.max 0,800 - (Date.now() - start)
                .error (err) ->
                  $scope.errorMessage = err.message.message
                  setErrors start

        $scope.close = ->
          if $scope.$saved and $scope.forms.user.$pristine
            $uibModalInstance.close $scope.user
          else if $scope.forms.user.$dirty or $scope.formErrors
            dismissCheck.dismiss $uibModalInstance, $scope.user, user_ref
          else
            $uibModalInstance.dismiss()

    inst.result.then (user) ->
      $http.get 'api/users?limit=100'
      .success (users) ->
        $scope.users = users
      .error (err) ->
        console.error 'Failed to load users. ', err

  $scope.disable = (user) ->
    user.disabled = yes
    $http.put 'api/users/' + user.username, user: user

Admin.controller 'APIKeys', ($scope, $location, $http) ->
  $scope.search = $location.search()
  $http.get("api/apikeys").success (data) ->
    $scope.keys = data

Admin.controller 'Billing', ($scope, $http) ->
  $scope.$watch 'clients', (clients) ->
    return unless clients
    $scope.activeClients = _(clients)
      .filter (v, k) ->
        k[0] isnt '$'
      .filter (client) ->
        not client.archive
      .map (client, id) ->
        id: client.id
        name: client.name
        short: client.short
      .value()

    reload = ->
      _.forEach $scope.activeClients, (client) ->
        params = from: $scope.range.from.toISOString(), to: $scope.range.to.toISOString(), client: client.id
        $http.get('api/billing', params: params)
          .then (res) ->
            client.billing = res.data

    $scope.$watch 'range', _.debounce(reload, 100), yes
  , yes

Admin.controller 'ClientImportModal',
  ['$scope', '$http', '$uibModalInstance', '$uibModal', '$window', 'id', 'short',
  ($scope, $http, $uibModalInstance, $uibModal, $window, id, short) ->
    $scope.forms = upload: null

    $scope.$watch 'forms.upload.json', (json) ->
      return unless json
      $scope.typeerror = !~json.name.indexOf '.json'

    $scope.upload = ->
      return $scope.typeerror = yes unless ~$scope.forms.upload.json.name.indexOf '.json'

      fd = new FormData
      fd.append 'file', $scope.forms.upload.json

      $http.post("api/clients/#{id}/import/#{short}", fd,
        transformRequest: angular.identity
        headers: 'Content-Type': undefined
      ).success ->
        $window.open("api/clients/#{id}/import")
        $uibModalInstance.close()
  ]

Admin.controller 'Templates', ($scope, $http, $timeout) ->
  $http.get('api/templates').success (data) ->
    $scope.templates = data

  $scope.save = ->
    $http.post('api/templates', $scope.templates).success (data) ->
      $timeout (-> $scope.forms.templates.$setPristine()), 1000

Admin.service 'timer', ['$timeout', ($timeout) ->
  # use this service to wrap a callback function that should execute after a set amount of time
  # and ensuer that a digest loop is run after the callback completes, uses $timeout
  # this is very useful for delaying spinners and timers from not blinking too fast
  #
  # $http.post()
  #   .success timer 425, ->
  #     console.log 'something'
  #     $scope.other = 'foobar'
  (min, cb) ->
    [cb, min] = [min, 600] unless cb
    start = Date.now()

    # return a function wrapping the callback
    (args...) ->
      $timeout ->
        cb args...
      , Math.max 0, min - (Date.now() - start)
]

Admin.controller 'Optouts', [
  '$scope', '$http', '$location', '$timeout', 'timer',
  ($scope, $http, $location, $timeout, timer) ->
    $scope.search = $location.search()
    $scope.forms = {} # track form reference

    $scope.load = (email) ->
      $scope.details = undefined
      $scope.forms.search.$submitting = yes

      email = $.trim email
      unless isValidEmailAddress email
        return setTimeout ->
          $scope.forms.search.$submitting = no
          $scope.details = notfound: yes
        , 600

      $location.search email: email
      $http.get('api/optouts/' + email)
        .success timer (data) ->
          $scope.forms.search.$submitting = no
          $scope.details = data
        .error timer ->
          $scope.forms.search.$submitting = no
          $scope.details =
            id: email
            clients: []
            history: []
            global: no

    if email = $scope.search.email
      $timeout -> $scope.load $scope.search.email = email

    $scope.search = ->
      $scope.load $scope.search.email

    $scope.set_global = ->
      method = if $scope.details.global then 'optout' else 'optin'
      $http.post("api/#{method}/#{$scope.details.id}")
        .success ->
          $scope.details.history.unshift
            at: new Date()
            type: "global #{method}"
            from: 'webadmin'
        .error console.error.bind console

    $scope.optout = (client) ->
      $http.post("api/optout/#{$scope.details.id}", client: client)
        .success ->
          $scope.details.clients.push client
          $scope.details.history.unshift
            at: new Date()
            type: 'optout'
            from: 'webadmin'
            client: client
        .error console.error.bind console

    $scope.optin = (client) ->
      $http.post("api/optin/#{$scope.details.id}", client: client)
        .success ->
          _.pull $scope.details.clients, client
          $scope.details.history.unshift
            at: new Date()
            type: 'optin'
            from: 'webadmin'
            client: client
        .error console.error.bind console
]

Admin.controller 'AWS', [
  '$scope', '$rootScope', '$http', '$location', '$timeout', '$uibModal', 'FileUploader',
  ($scope, $rootScope, $http, $location, $timeout, $uibModal, FileUploader) ->
    $http.get 'api/clients/getBuckets'
    .success (results) ->
      $scope.aws_buckets = _.pluck results.buckets.Buckets, 'Name'
      $scope.aws_config = results.config
      if $scope.aws_config isnt 'production'
        $scope.aws_bucket = 'softvu-staging'

    $scope.uploader = new FileUploader

    $scope.uploader.onBeforeUploadItem = (item) ->
      item.formData.push key: item.key
]

.directive 'awsFileSelect', [
  '$http', ($http) ->
    scope: uploader: '=', bucket: '='
    link: (scope, elm, attrs) ->
      unless scope.uploader.isHTML5
        elm.removeAttr 'multiple'

      elm.bind 'change', ->
        if scope.bucket?
          data = if scope.uploader.isHTML5 then @files else [@]
          options = scope.$eval(attrs.awsFileSelect) or {}

          _.forEach data, (file) ->
            $http.get("api/public/aws/#{scope.bucket}?type=#{encodeURIComponent file.type}")
            .success (policy) ->
              opts = _.extend {}, options
              opts.formData ?= []
              opts.formData.push _.omit policy, 'bucket'
              opts.formData.push
                acl: 'public-read', 'Content-Type': file.type, success_action_status: '201'
              opts.key = file.name
              opts.url = "https://#{scope.bucket}.s3.amazonaws.com"

              scope.uploader.addToQueue file, opts
        else
          return

        if scope.uploader.isHTML5 and elm.attr('multiple')
          elm.prop 'value', null

      elm.prop 'value', null
]

$ -> angular.bootstrap document, ['Admin']

do ($ = jQuery) ->
  $(document).on 'mouseenter mouseleave', 'tbody.events tr', (e) ->
    el = $ @
    return unless /\w{32}/.test el.data 'ref'

    tbody = el.parent()
    if e.type is 'mouseenter'
      tbody.find(".evt-ref-#{el.data('ref')}").addClass 'hover'
    else if e.type is 'mouseleave'
      tbody.find('.hover').removeClass 'hover'

# ________    _______    _________   _________ __          _____  _____
# \______ \   \      \  /   _____/  /   _____//  |_ __ ___/ ____\/ ____\
#  |    |  \  /   |   \ \_____  \   \_____  \\   __\  |  \   __\\   __\
#  |    `   \/    |    \/        \  /        \|  | |  |  /|  |   |  |
# /_______  /\____|__  /_______  / /_______  /|__| |____/ |__|   |__|
#         \/         \/        \/          \/
#
# dns files:                                  Q1 2016 - njw
#
# webadmin\app.coffee                       ~ Line 500
# webadmin\public\stylesheets\sb-admin.css  ~ Line 250
# shared\dns.coffee                         ~ Line 1
# webadmin\views\index.jade                 ~ Line 775
# webadmin\public\javascripts\index.coffee  ~ Line 950 (this file)

# dns sections:     (they've got flower boxes)

# initialization
#   controller
#   start_dns_stuff()

# page load
#   get_clients(cb)
#   set_clients(cb)
#   get_records(cb)
#   write_records(cb)
#   new_record(cb)

# actionqueue
#   get_actionqueue(cb)
#   post_actionqueue()
#   refresh_actionqueu()

# settings
#   get_settings(cb)
#   post_settings()
#   refresh_settings()

# dns records
#   delete_record(e)
#   verify_record(e)
#   upsert_record(e)

# ui functions
#   window_scroll()
#   sticky_right_col()
#   checkbox_click(e)

# helper functions
#   dns_message(args)
#   String::map

# old shit
#   delete whenever

############################
####   initialization   ####
############################

Admin.controller 'DNS', ($scope, $http, $timeout) ->
  window.record_html = $('#dns-record-template').html()
  window.message_html =
    '<div class={color_class}>&gt;&nbsp;{message}</div>'
  m = 'message'
  g = 'green-text'
  y = 'yellow-text'
  window.messages = {
    page_done: [m, g, 'ready']
    # actionqueue
    get_actionqueue_fail: [m, y, 'get_actionqueue() function failed']
    post_actionqueue_success: [m, g, 'updated actionqueue successfully']
    post_actionqueue_fail: [m, y, 'update_actionqueue() function failed']
    refresh_actionqueue_success: [m, g, 'refreshed actionqueue successfully']
    # settings
    get_settings_fail: [m, y, 'get_settings() function failed']
    post_settings_success: [m, g, 'updated settings successfully']
    post_settings_fail: [m, y, 'post_settings() function failed']
    refresh_settings_success: [m, g, 'refreshed settings successfully']
    # records
    get_records_fail: [m, y, 'get_records() function failed']
    upsert_record_fail: [m, y, 'upsert_record() function failed']
    upsert_record_success: [m, g, 'upserted record successfully']
    delete_record_fail : [m, y, 'delete_record() function failed']
    delete_record_success: [m, g, 'deleted record successfully']
    # verify
    verify_record_fail: [m, y, 'verify_record() function failed']
    verify_record_success: [m, g, 'verify_record function ran successfully']
  }
  $(window).scroll window_scroll
  start_dns_stuff()

start_dns_stuff = ->
  $('#dns-update-aq').click post_actionqueue
  $('#dns-refresh-aq').click refresh_actionqueue
  $('#dns-update-settings').click post_settings
  $('#dns-refresh-settings').click refresh_settings
  get_clients -> # from the left
    set_clients -> # from the left
    get_records -> # everything you coded
      write_records -> # indented from the left
        new_record -> # from the left
          get_actionqueue -> # from the left
            get_settings -> # fucking coffeescript from the left
              dns_message window.messages.page_done

#######################
####   page load   ####
#######################

get_clients = (cb) ->
  $.get 'dns/clients', {}, ((data) ->
    window.clients = data
    cb()
  ), 'json'

set_clients = (cb) ->
  i = 0
  while i < window.clients.length
    o = '<option value={value}>{name}</option>'
    c = [{k: 'value', v: window.clients[i].id},
         {k: 'name',  v: window.clients[i].name }]
    $('#dns-client-select').append o.map c
    i++
  $('#dns-client-select').change ->
    client_select_change()
  cb()

get_records = (cb) ->
  $.get 'dns/records', {}, ((data) ->
    if data.err?
      dns_message window.messages.get_records_fail
      dns_message ['message', 'red-text', data.err.message]
    else
      window.dns_records = data.ret
    cb()
  ), 'json'

write_records = (cb) ->
  $('#dns-records-div').html ''
  r = window.dns_records
  i = 0
  color_class = ''
  window.global_count = 0
  window.client_count = 0
  while i < r.length
    if r[i].isglobal == true
      color_class = 'dns-global-color'
      window.global_count++
    else
      color_class = 'dns-client-color'
      window.client_count++
    a = [
      {k:'id',      v:r[i].id},
      {k:'name',    v:r[i].name},
      {k:'domain',  v:r[i].domain},
      {k:'value',   v:r[i].value}
    ]
    $('#dns-records-div').append(window.record_html.map a)
    $('#dns-container-' + r[i].id).addClass(color_class)
    $('#dns-type-' + r[i].id).val(r[i].type)
    $('#isglobal-' + r[i].id).prop('checked', r[i].isglobal)
    i++
  cb()

new_record = (cb) ->
  html = '<br/><h4 class="dns-left-column-title">New DNS Record</h4>'
  $('#dns-records-div').append html
  a = [
    {k:'id',      v:'0'},
    {k:'name',    v:''},
    {k:'domain',  v:''},
    {k:'value',   v:''}
  ]
  $('#dns-records-div').append(window.record_html.map a)
  $('#dns-update-0').html 'Add'
  $('#dns-delete-0').remove()
  $('#dns-verify-0').remove()
  # here's where the event handlers are defined
  $('.dns-delete').click (e) -> delete_record e
  $('.dns-verify').click (e) -> verify_record e
  $('.dns-update').click (e) -> upsert_record e
  $('.dns-record-checkbox').click (e) -> checkbox_click e
  $('#dns-container-0').addClass('dns-red')
  $('#dns-edit-globals').html(window.global_count)
  $('#dns-edit-clients').html(window.client_count)
  cb()

#########################
####   actionqueue   ####
#########################

get_actionqueue = (cb) ->
  $.get 'dns/actionqueue', {}, ((data) ->
    if data.err?
      dns_message window.messages.get_actionqueue_fail
      dns_message ['message', 'red-text', data.err.message]
    else
      window.aq = data.ret[0]
      $('#dns-aq-status-input').val window.aq.status
      $('#dns-aq-cron-input').val window.aq.repeat
    cb()
  ), 'json'

post_actionqueue = ->
  status = $('#dns-aq-status-input').val()
  cron = $('#dns-aq-cron-input').val()
  $.post 'dns/actionqueue', { status: status, cron: cron }, ((data) ->
    if data.err?
      dns_message window.messages.post_actionqueue_fail
      dns_message ['message', 'red-text', data.err.message]
    else
      dns_message window.messages.post_actionqueue_success
  ), 'json'

refresh_actionqueue = ->
  get_actionqueue ->
    dns_message window.messages.refresh_actionqueue_success

######################
####   settings   ####
######################

get_settings = (cb) ->
  $.get 'dns/settings', {}, ((data) ->
    if data.err?
      dns_message window.messages.get_settings_fail
      dns_message ['message', 'red-text', data.err.message]
    else
      window.dns_settings = data.ret[0]
      $('#dns-settings-email-input').val window.dns_settings.email_to
      $('#dns-settings-zone-input').val window.dns_settings.zone_id
    cb()
  ), 'json'

post_settings = ->
  email = $('#dns-settings-email-input').val()
  zone = $('#dns-settings-zone-input').val()
  $.post 'dns/settings', { email: email, zone: zone }, ((data) ->
    if data.err?
      dns_message window.messages.post_settings_fail
      dns_message ['message', 'red-text', data.err.message]
    else
      dns_message window.messages.post_settings_success
  ), 'json'

refresh_settings = ->
  get_settings ->
    dns_message window.messages.refresh_settings_success

#########################
####   dns records   ####
#########################

delete_record = (e) ->
  id = e.currentTarget.id.split('-')[2]
  $.post 'dns/delete_record', { id: id }, ((data) ->
    if data.err?
      dns_message window.messages.delete_record_fail
      dns_message ['message', 'red-text', data.err.message]
    else
      dns_message window.messages.delete_record_success
      get_records ->
        write_records ->
          new_record()
  ), 'json'

verify_record = (e) ->
  id = e.currentTarget.id.split('-')[2]
  records = [ id, 10 ] # adding 10 to test multi
  # records.push(id)
  # records.push(10)
  verify_records records

verify_all = ->
  i = 0
  records = []
  while i < window.dns_records.length
    records.push window.dns_records[i].id
    i++
  verify_records records

verify_records = (records) ->
  $.post 'dns/verify_records', {records: records}, ((data) ->
    if data.err?
      dns_message window.messages.verify_record_fail
      dns_message ['message', 'red-text', data.err.message]
    else
      window.verify = data.ret
      dns_message window.messages.verify_record_success
  ), 'json'

verify_records_old_old = (records) ->
  i = 0
  while i < records.length
    $.post 'dns/verify_record', { records: records }, ((data) ->
      if data.err?
        dns_message window.messages.verify_record_fail
        dns_message ['message', 'red-text', data.err.message]
      else
        window.verify = data.ret
        dns_message window.messages.verify_record_success
    ), 'json'
    i++

upsert_record = (e) ->
  id = e.currentTarget.id.split('-')[2]
  $.post 'dns/record', {
    id: id,
    client_id: $('#dns-client-select').val(),
    isglobal: $('#isglobal-' + id).prop('checked'),
    name: $('#dns-name-' + id).val(),
    type: $('#dns-type-' + id).val(),
    domain: $('#dns-domain-' + id).val(),
    value: $('#dns-value-' + id).val()
  }, ((data) ->
    if data.err?
      dns_message window.messages.upsert_record_fail
      dns_message ['message', 'red-text', data.err.message]
    else
      dns_message window.messages.upsert_record_success
      get_records ->
        write_records ->
          new_record()
  ), 'json'

##########################
####   ui functions   ####
##########################

window_scroll = ->
  sticky_right_col()

sticky_right_col = ->
  w = $(window).scrollTop()
  e = $('.dns-right-column')
  if ($(window).width() >= 1858)
    e.css 'margin-top', w + 'px'
  else
    e.css 'margin-top', 0

checkbox_click = (e) ->
  id = e.currentTarget.id.split('-')[1]
  $('#dns-container-' + id).removeClass('dns-global-color').removeClass('dns-client-color')
  new_class = if $('#isglobal-' + id).prop('checked') then 'dns-global-color' else 'dns-client-color'
  $('#dns-container-' + id).addClass(new_class)

##############################
####   helper functions   ####
##############################

dns_message = (args) ->
  obj = [ {k: 'color_class', v: args[1] },
          {k: 'message', v: args[2] }]
  html = window.message_html.map obj
  # TODO don't use silly old-school javascript
  e = document.getElementsByClassName('dns-' + args[0] + '-scrollable')[0]
  $(e).append html
  e.scrollTop = e.scrollHeight

String::map = ->
  a = arguments
  t = this
  i = 0
  while i < a[0].length
    f = '{' + a[0][i].k + '}'
    r = new RegExp(f, 'g')
    t = t.replace(r, a[0][i].v)
    i++
  t

######################
####   old s#!t   ####
######################

client_select_change = ->
  client_id = $('#dns-client-select').val()
  if client_id is 'default'
    console.log client_id
  else
    console.log 'it ain\'t the default...'

start_dns_stuff_old = ->
  client_id = window.location.pathname.split('/')[3]
  window.client_id = client_id
  window.test = ->
    window_test()
  window.test_html = $('#dns-test-template').html()
  # get_doc ->
  load_dns_tests ->
    print_tests ->
      new_test()
  $('#dns-client-start-button').click ->
    client_start_click()
  $('#dns-global-start-button').click ->
    global_start_click()

update_test = (e) -> # also handles inserts
  id = e.currentTarget.id.split('-')[2]
  $.post 'lib/update_test', {
    id: id,
    client_id: $('#dns-client-select').val(),
    isglobal: $('#isglobal-' + id).prop('checked'),
    name: $('#dns-name-' + id).val(),
    type: $('#dns-type-' + id).val(),
    domain: $('#dns-domain-' + id).val(),
    value: $('#dns-value-' + id).val()
  }, ((data) ->
    if data.success == true
      dns_message 'dns-main', 'Successfully Wrote DNS Record'
      load_dns_tests ->
        print_tests ->
          new_test()
    else
      dns_message 'dns-main', data.err.message
  ), 'json'

client_start_click = ->
  client_id = window.location.pathname.split('/')[3]
  # window.client_id = client_id
  # window.dns_test_mode = 'client'
  $.post 'lib/start_tests', { client_id: client_id, mode: 'client' }, ((data) ->
    console.log data
    # window.dns_job_id = data.id
    # get_doc ->
    #  start_tests()
  ), 'json'

global_start_click = ->
  console.log 'start global tests'

get_doc = (cb) ->
  $.post 'lib/get_client_doc', { client_id: window.client_id }, ((data) ->
    window.client_doc = data
    cb()
  ), 'json'

start_tests_old = ->
  i = 0
  t = window.dns_tests
  g = if window.dns_test_mode is 'client' then false else true
  while i < t.length
    if t[i].isglobal is g
      run_test i
    i++

run_test = (test_i) ->
  $.post 'lib/run_test', { test_id: window.dns_tests[test_i].id }, ((data) ->
    console.log 'done running test ' + window.dns_tests[test_i].id
    console.log data.test_id
  ), 'json'

String::f = -> # using map instead
  # - nope, now using jsviews.js template function
  args = arguments
  @replace /\{(\S+)\}/g, (m, n) ->
    args[n]

do ($ = jQuery) ->
  clips = new Clipboard '.copyable'
  clips.on 'success', (e) ->
    tip = $ e.trigger
    tip.tooltip title: 'Copied!'
    tip.tooltip 'show'
    setTimeout (-> tip.tooltip 'destroy'), 2000
