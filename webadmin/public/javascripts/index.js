(function() {
  var Admin, SocketService, checkbox_click, client_select_change, client_start_click, delete_record, dns_message, get_actionqueue, get_clients, get_doc, get_records, get_settings, global_start_click, isValidEmailAddress, new_record, post_actionqueue, post_settings, refresh_actionqueue, refresh_settings, run_test, set_clients, start_dns_stuff, start_dns_stuff_old, start_tests_old, sticky_right_col, update_test, upsert_record, verify_all, verify_record, verify_records, verify_records_old_old, window_scroll, write_records,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    hasProp = {}.hasOwnProperty,
    slice = [].slice;

  isValidEmailAddress = function(email) {
    var atidx;
    atidx = email.indexOf('@');
    return email.length >= 3 && ~atidx && atidx < email.length - 1;
  };

  Admin = angular.module('Admin', ['ngResource', 'ngRoute', 'angularMoment', 'angularFileUpload', 'angular.filter', 'ui.bootstrap', 'ui.ace', 'frapontillo.bootstrap-switch']);

  Admin.service('socket', [
    '$rootScope', SocketService = (function() {
      var MAX_RETRIES, STATE, STATES;

      STATES = ['connected', 'connecting', 'disconnected'];

      MAX_RETRIES = 20;

      STATE = _.object(STATES, STATES);

      function SocketService(rootScope) {
        var onbefore;
        this.rootScope = rootScope;
        this._wrap = bind(this._wrap, this);
        this._onclose = bind(this._onclose, this);
        this._onmessage = bind(this._onmessage, this);
        this.handles = [];
        this._state = STATE.disconnected;
        this._retry = {
          timeout: 1000,
          attempts: 0
        };
        onbefore = window.onbeforeunload;
        window.onbeforeunload = (function(_this) {
          return function() {
            delete _this.socket.onclose;
            return typeof onbefore === "function" ? onbefore() : void 0;
          };
        })(this);
      }

      SocketService.prototype.connect = function(cb) {
        var retry, timer;
        if (this._state !== STATE.disconnected) {
          return typeof cb === "function" ? cb() : void 0;
        }
        this._state = STATE.connecting;
        return timer = setTimeout(retry = (function(_this) {
          return function() {
            var ref1, warn;
            if ((ref1 = _this.socket) != null) {
              ref1.close();
            }
            _this._retry.attempts += 1;
            warn = 'websocket timeout retries exhausted...';
            if (_this._retry.attempts > MAX_RETRIES) {
              return console.warn(warn);
            }
            _this.socket = new SockJS('/stream/events');
            _this.socket.onopen = function() {
              clearTimeout(timer);
              _this.socket.onmessage = _this._onmessage;
              _this.socket.onclose = _this._onclose;
              _this._retry = {
                timeout: 1000,
                attempts: 0
              };
              _this._state = STATE.connected;
              return typeof cb === "function" ? cb() : void 0;
            };
            return timer = setTimeout(retry, _this._retry.timeout = 2 * _this._retry.timeout);
          };
        })(this), 1);
      };

      SocketService.state = function() {
        return this._state;
      };

      SocketService.prototype._onmessage = function(e) {
        var obj;
        obj = JSON.parse(e.data);
        return this.handles.forEach(function(fn) {
          return fn(obj);
        });
      };

      SocketService.prototype._onclose = function() {
        this.socket.close();
        this.socket = null;
        this._state = STATE.disconnected;
        return this.connect();
      };

      SocketService.prototype.on = function(eventName, cb) {
        this.connect();
        return this.handles.push(this._wrap(cb));
      };

      SocketService.prototype._wrap = function(cb) {
        return (function(_this) {
          return function(message) {
            return _this.rootScope.$apply(function() {
              return typeof cb.apply === "function" ? cb.apply(_this.socket, [message]) : void 0;
            });
          };
        })(this);
      };

      SocketService.prototype.emit = function(eventName, data, cb) {
        return console.error('deprectated: anyone using this?');
      };

      return SocketService;

    })()
  ]);

  Admin.factory('Client', function($resource) {
    return $resource('api/clients/:id', null, {
      query: {
        isArray: false
      }
    });
  });

  Admin.factory('User', function($resource) {
    return $resource('api/users/:id');
  });

  Admin.value('type', '');

  Admin.controller('ConfirmClose', [
    '$scope', '$uibModal', '$uibModalInstance', 'type', 'refItem', 'scopeItem', function($scope, $uibModal, $uibModalInstance, type, refItem, scopeItem) {
      $scope.type = type;
      if (!!refItem) {
        scopeItem = refItem;
      }
      $scope.confirm = function() {
        return $uibModalInstance.close(scopeItem);
      };
      return $scope.dismiss = function() {
        return $uibModalInstance.dismiss();
      };
    }
  ]);

  Admin.service('dismissCheck', [
    '$uibModal', function($uibModal) {
      return {
        dismiss: function(modalInstance, item, ref) {
          var inst;
          inst = $uibModal.open({
            templateUrl: 'dismissCheck-modal.html',
            backdrop: 'static',
            controller: 'ConfirmClose',
            resolve: {
              refItem: function() {
                return ref;
              },
              scopeItem: function() {
                return item;
              }
            }
          });
          return inst.result.then(function(closeItem) {
            return modalInstance.close(closeItem);
          });
        }
      };
    }
  ]);

  Admin.directive('bsDatetimePicker', function() {
    return {
      require: '?ngModel',
      link: function(scope, elm, attrs, ngModel) {
        elm.datetimepicker({
          defaultDate: new Date
        });
        attrs.$observe('disabled', function(val) {
          return elm.data('DateTimePicker')[val ? 'disable' : 'enable']();
        });
        elm.on('change', function(e) {
          if (scope.$$phase) {
            return;
          }
          return scope.$apply(function() {
            if (attrs.dateFormat) {
              return ngModel.$setViewValue(elm.data('DateTimePicker').getDate().format(attrs.dateFormat));
            } else {
              return ngModel.$setViewValue(elm.data('DateTimePicker').getDate().valueOf());
            }
          });
        });
        return ngModel.$render = function() {
          return elm.data('DateTimePicker').setDate(ngModel.$viewValue);
        };
      }
    };
  });

  Admin.directive('setClient', function() {
    return {
      link: function($scope, $element, $attributes) {
        return $element.on('click', function() {
          var form;
          form = $('<form action="../setclient" method="post" target="_blank"/>');
          form.append($('<input type="hidden"/>').attr({
            name: 'id',
            value: $attributes.setClient
          }));
          form.appendTo($element).submit().remove();
          $scope.isOpen = false;
          return $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        });
      }
    };
  });

  Admin.directive('bsRadio', function() {
    return {
      scope: {
        type: '=ngModel'
      },
      transclude: true,
      template: '<i class="fa fa-check-square-o truthy" />\n<i class="fa fa-square-o falsy" />\n<input type=\'checkbox\' />\n<span ng-transclude />',
      link: function($scope, $element, $attributes) {
        $element.addClass('checks');
        if (!('readOnly' in $attributes)) {
          $element.on('click', function(e) {
            var fs;
            fs = $($element).find(':checkbox').parents('fieldset');
            if (fs != null ? fs.is(':disabled') : void 0) {
              return;
            }
            e.preventDefault();
            return $scope.$apply(function() {
              return $scope.type = $attributes.value;
            });
          });
        }
        return $scope.$watch('type', function(nv, ov) {
          $scope.selected = nv === $attributes.value;
          $element.find('input').prop('checked', $scope.selected);
          $element.toggleClass('checked', $scope.selected);
          return $element.toggleClass($attributes.bsRadioSelected, $scope.selected);
        });
      }
    };
  });

  Admin.directive('bsConfirmClick', function($uibModal) {
    return {
      priority: -1,
      link: function(scope, element, attributes) {
        return element.bind('click', function(e) {
          var inst;
          e.preventDefault();
          e.stopImmediatePropagation();
          inst = $uibModal.open({
            templateUrl: 'dialogs/confirm.html',
            keyboard: false,
            backdrop: 'static',
            scope: _.assign(scope.$new(), {
              message: attributes.bsConfirmClick,
              permanent: !!attributes.bsConfirmClickPermanent
            })
          });
          return inst.result.then(function() {
            return scope.$eval(attributes.ngClick);
          });
        });
      }
    };
  });

  Admin.directive('svPagingRange', [
    '$location', function($location) {
      return {
        templateUrl: 'paging/range.html',
        replace: true,
        link: function(scope, elm, attrs) {
          var tmp;
          tmp = _.mapValues(_.pick($location.search(), ['from', 'to']), function(dt) {
            return moment(dt);
          });
          scope.range = {
            from: tmp.from || moment().startOf('month'),
            to: (tmp.to || moment()).endOf('day')
          };
          return elm.daterangepicker({
            format: 'YYYY-MM-DD',
            opens: 'left',
            startDate: moment(scope.range.from),
            endDate: moment(scope.range.to),
            ranges: {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
              'Last 7 Days': [moment().subtract('days', 6), moment()],
              'Last 30 Days': [moment().subtract('days', 29), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            }
          }, function(start, end) {
            var dt, k, ref1, results1;
            scope.$apply(function() {
              scope.range.from = start;
              return scope.range.to = end;
            });
            ref1 = scope.range;
            results1 = [];
            for (k in ref1) {
              if (!hasProp.call(ref1, k)) continue;
              dt = ref1[k];
              results1.push($location.search(k, moment(dt).format('YYYY-MM-DD')));
            }
            return results1;
          });
        }
      };
    }
  ]);

  Admin.directive('bsSaveButton', function() {
    return {
      scope: {
        onSave: '&'
      },
      restrict: 'E',
      require: '^form',
      template: '<button class="btn btn-primary" name="save" ng-click="save()" ng-disabled="form.$pristine">Save</button>\n<span class="spinner spinner-small no-animate" ng-if="form.$submitting"></span>\n<span class="fa fa-fw fa-check text-success" ng-if="form.$saved && form.$pristine"></span>\n',
      link: function(scope, element, attributes, form) {
        scope.form = form;
        element.addClass('bs-save-button');
        scope.save = function() {
          scope.start = (new Date).getTime();
          scope.form.$submitting = true;
          scope.form.$saved = false;
          return scope.onSave();
        };
        return (function(old) {
          return scope.form.$setPristine = function() {
            setTimeout(function() {
              scope.form.$submitting = false;
              return scope.form.$saved = true;
            }, Math.max(0, 600 - ((new Date).getTime() - scope.start)));
            return old();
          };
        })(scope.form.$setPristine);
      }
    };
  });

  Admin.directive('goBack', function($window) {
    return {
      transclude: true,
      template: '<a style="cursor: pointer" ng-transclude></a>',
      link: function(scope, element, attrs) {
        return element.on('click', function() {
          return $window.history.back();
        });
      }
    };
  });

  Admin.directive('ngListNewline', function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ngModel) {
        ngModel.$parsers.push(function(text) {
          return text.split('\n').filter(function(x) {
            return x.trim().length > 0;
          });
        });
        return ngModel.$formatters.push(function(array) {
          if (array != null ? array.length : void 0) {
            return array.join('\n');
          } else {
            return '';
          }
        });
      }
    };
  });

  Admin.config(function($routeProvider, $locationProvider) {
    $routeProvider.when('/dashboard', {
      templateUrl: 'dashboard.html',
      controller: 'Dashboard'
    }).when('/jobs', {
      templateUrl: 'jobs.html',
      controller: 'Jobs'
    }).when('/jobs/:client', {
      templateUrl: 'jobs.html',
      controller: 'Jobs'
    }).when('/clients/:id', {
      templateUrl: 'client.html',
      controller: 'Client'
    }).when('/clients', {
      templateUrl: 'clients.html',
      controller: 'Clients',
      reloadOnSearch: false
    }).when('/apikeys', {
      templateUrl: 'apikeys.html',
      controller: 'APIKeys',
      reloadOnSearch: false
    }).when('/users', {
      templateUrl: 'users.html',
      controller: 'Users',
      reloadOnSearch: false
    }).when('/billing', {
      templateUrl: 'billing.html',
      controller: 'Billing'
    }).when('/templates', {
      templateUrl: 'templates.html',
      controller: 'Templates'
    }).when('/dns', {
      templateUrl: 'dns.html',
      controller: 'DNS'
    }).when('/optouts', {
      templateUrl: 'optouts.html',
      controller: 'Optouts',
      reloadOnSearch: false
    }).when('/aws', {
      templateUrl: 'aws.html',
      controller: 'AWS'
    }).otherwise({
      redirectTo: '/dashboard'
    });
    return $locationProvider.html5Mode(true);
  });

  Admin.run(function($rootScope, $http, $location, $timeout, $q, $uibModal, socket, Client) {
    $rootScope.isActive = function(path) {
      return ~$location.path().indexOf(path);
    };
    $rootScope.clients = Client.query();
    $rootScope.clients.$promise.then(function(clients) {
      var client, id;
      return $rootScope.clients_array = (function() {
        var ref1, results1;
        ref1 = $rootScope.clients;
        results1 = [];
        for (id in ref1) {
          if (!hasProp.call(ref1, id)) continue;
          client = ref1[id];
          if (id[0] !== '$') {
            results1.push(client);
          }
        }
        return results1;
      })();
    });
    $http.get('api/events').success(function(events) {
      return $rootScope.events = events;
    });
    $rootScope.polls = {};
    socket.on('event', function(event) {
      if (!event.type) {
        return;
      }
      if (~event.type.indexOf('heartbeat')) {
        event.at = moment(event.at).subtract(1, 'second');
        return $rootScope.polls[event.type.slice(0, event.type.indexOf('.heartbeat'))] = event;
      } else {
        $rootScope.events.unshift(event);
        return $timeout(function() {
          return $rootScope.events = _.take($rootScope.events, 50);
        }, 1000);
      }
    });
    $rootScope.show = function(obj) {
      return $uibModal.open({
        templateUrl: 'json/raw.html',
        size: 'lg',
        scope: _.assign($rootScope.$new(), {
          obj: obj
        })
      });
    };
    return (function(ace) {
      var commands, dom;
      dom = ace.require('ace/lib/dom');
      commands = ace.require('ace/commands/default_commands').commands;
      commands.push({
        name: 'Toggle Fullscreen',
        bindKey: 'F11',
        exec: function(editor) {
          dom.toggleCssClass(document.body, 'fullScreen');
          dom.toggleCssClass(editor.container, 'fullScreen-editor');
          return editor.resize();
        }
      });
      return $rootScope.aceLoaded = function(editor) {
        ace.require('ace/config').set('workerPath', '../bower_components/ace-builds/src-min-noconflict');
        ace.require('ace/ext/language_tools');
        editor.setTheme('ace/theme/monokai');
        editor.getSession().setMode('ace/mode/html');
        editor.setShowPrintMargin(false);
        return editor.getSession().setUseWrapMode(true);
      };
    })(ace);
  });

  Admin.controller('Services', function($scope, $http, $interval) {
    $interval($scope.apply, 2000);
    $scope.services = _.map(['actionqueue', 'eventhose', 'pipeline', 'webhooks'], function(name) {
      return {
        name: name,
        running: true,
        restarting: false
      };
    });
    $scope.outdated = function(service) {
      var at, ref1;
      at = (ref1 = $scope.polls[service.name]) != null ? ref1.at : void 0;
      return !at || 1 <= moment().diff(at, 'minutes', true);
    };
    $scope.stop = function(service) {
      service.restarting = true;
      return $http.post('api/services/stop', {
        name: service.name
      }).then(function() {
        service.running = false;
        return service.restarting = false;
      });
    };
    $scope.start = function(service) {
      service.restarting = true;
      return $http.post('api/services/start', {
        name: service.name
      }).then(function() {
        service.restarting = false;
        return service.running = true;
      });
    };
    return $scope.restart = function(service) {
      service.restarting = true;
      return $http.post('api/services/restart', {
        name: service.name
      }).then(function() {
        service.restarting = false;
        return service.running = true;
      });
    };
  });

  Admin.controller('Dashboard', function() {});

  Admin.controller('Jobs', function($scope, $http, $routeParams, $interval, $timeout) {
    var allClientsJobs, clientJobs, currentJobs, id, jobsGet, jobsPromise, jobsReload;
    id = $routeParams.client;
    clientJobs = "api/jobs/" + id;
    allClientsJobs = 'api/jobs';
    currentJobs = null;
    jobsPromise = $interval(function() {
      $scope.$reloading = true;
      return jobsGet(function() {
        return $timeout(function() {
          return $scope.$reloading = false;
        }, 2000);
      });
    }, 60000);
    jobsGet = function(cb) {
      return $http.get(currentJobs).success(function(data) {
        $scope.jobs = data;
        return typeof cb === "function" ? cb() : void 0;
      });
    };
    jobsReload = function() {
      return jobsPromise;
    };
    if (id) {
      $scope.client = id;
      currentJobs = clientJobs;
      jobsGet(jobsReload);
    } else {
      currentJobs = allClientsJobs;
      jobsGet(jobsReload);
    }
    $scope.$on('$destroy', function() {
      return $interval.cancel(jobsPromise);
    });
    return $scope.trigger = function(job) {
      job.data.original_at = job.at;
      job.at = new Date();
      job.data.triggered = true;
      return $http.put('api/jobs', job).success(function(data) {});
    };
  });

  Admin.factory('ContextMenuService', function() {
    return {
      menu: null
    };
  });

  Admin.directive('contextMenu', [
    '$compile', '$parse', '$templateCache', 'ContextMenuService', function($compile, $parse, $templateCache, contextMenuService) {
      return {
        scope: false,
        link: function(scope, elem, attrs) {
          var el, html;
          html = $templateCache.get(attrs.contextMenu);
          el = $compile(html)(scope);
          elem.on('click', '.context', function(e) {
            e.stopPropagation();
            return scope.menu.contextmenu('show', e);
          });
          $('body').append(el);
          return scope.menu = $(elem[0]).contextmenu({
            target: el,
            before: function() {
              var ref1;
              if ((ref1 = contextMenuService.menu) != null) {
                ref1.contextmenu('closemenu');
              }
              contextMenuService.menu = scope.menu;
              scope.$apply(function() {
                return scope.item = $parse(attrs.contextMenuItem)(scope);
              });
              return true;
            }
          });
        }
      };
    }
  ]);

  Admin.directive('bs`FileInput`', function() {
    return {
      scope: {
        'onReset': '&'
      },
      link: function(scope, elm, attrs) {
        elm.fileinput();
        elm.on('clear.bs.fileinput', function() {
          return scope.$apply(function() {
            return scope.onReset();
          });
        });
        elm.on('reset.bs.fileinput', function() {
          return scope.$apply(function() {
            return scope.onReset();
          });
        });
        return scope.$on('clear.fileinput', function() {
          return elm.fileinput('clear');
        });
      }
    };
  });

  Admin.directive('fileModel', function() {
    return {
      require: '?ngModel',
      link: function(scope, elm, attrs, ngModel) {
        return elm.on('change.bs.fileinput', function() {
          return scope.$apply(function() {
            return ngModel.$setViewValue(elm[0].files[0]);
          });
        });
      }
    };
  });

  Admin.directive('readfile', function() {
    return {
      scope: {
        readfile: '='
      },
      link: function(scope, elm, attrs) {
        return elm.on('change.bs.fileinput', function() {
          return scope.$apply(function() {
            return scope.readfile = elm[0].files[0];
          });
        });
      }
    };
  });

  Admin.controller('Clients', function($scope, $http, $location, $routeParams, $uibModal, $window) {
    $scope.show = {
      archived: $routeParams.archived === 'yes' ? true : false
    };
    $scope.search = {};
    $scope.$watch('clients', function(clients) {
      var client, id;
      if (!clients) {
        return;
      }
      return $scope.ordered = (function() {
        var ref1, results1;
        results1 = [];
        for (id in clients) {
          if (!hasProp.call(clients, id)) continue;
          client = clients[id];
          if (id[0] !== '$') {
            results1.push({
              id: id,
              name: client.name,
              short: client.short,
              archive: client.archive || false,
              integration: ((ref1 = client.integrations) != null ? ref1._current : void 0) || ''
            });
          }
        }
        return results1;
      })();
    }, true);
    $scope.archiveClient = function(id) {
      $scope.clients[id].archive = true;
      return $http.post('api/clients', {
        client: $scope.clients[id]
      });
    };
    $scope.deleteClient = function(id) {
      if (!confirm('This operation is irreversible. Continue?')) {
        return;
      }
      return $http["delete"]("api/clients/" + id).success(function() {
        return delete $scope.clients[id];
      });
    };
    $scope.deleteEvents = function(id) {
      if (!confirm('This operation is irreversible. Continue?')) {
        return;
      }
      return $http["delete"]("api/clients/" + id + "/events").success(function() {});
    };
    $scope.deleteMessages = function(id) {
      if (!confirm('This operation is irreversible. Continue?')) {
        return;
      }
      return $http["delete"]("api/clients/" + id + "/messages").success(function() {});
    };
    $scope.toggleArchived = function() {
      return $location.search({
        archived: $scope.show.archived ? 'yes' : void 0
      });
    };
    $scope.showFilter = function(item) {
      if ($scope.show.archived || !item.archive) {
        return true;
      }
    };
    $scope.create = function() {
      var inst;
      inst = $uibModal.open({
        templateUrl: 'client-create.html',
        controller: function($scope, $http, $uibModalInstance, $timeout) {
          var requiredVariables, setErrors;
          $scope.client = {};
          $scope.forms = {};
          setErrors = function(start) {
            return $timeout(function() {
              $scope.forms.client.$invalid = true;
              $scope.$creating = false;
              return $scope.forms.client.$setPristine();
            }, Math.max(0, 800 - (Date.now() - start)));
          };
          requiredVariables = function(client, cb) {
            var formErrors;
            formErrors = {};
            if (!client.name) {
              formErrors.name = true;
            }
            return cb(formErrors);
          };
          $scope.create = function() {
            var start;
            $scope.formErrors = {};
            $scope.$creating = true;
            $scope.errorMessage = void 0;
            $scope.$created = false;
            start = Date.now();
            return requiredVariables($scope.client, function(formErrors) {
              var ref1;
              if (!_.isEmpty(formErrors)) {
                $scope.formErrors = formErrors;
                return setErrors(start);
              } else {
                if (!!((ref1 = $scope.client.manager) != null ? ref1.length : void 0)) {
                  if (!isValidEmailAddress($scope.client.manager)) {
                    $scope.formErrors.manager = true;
                    setErrors(start);
                    return;
                  }
                }
                return $http.post('api/clients', {
                  client: $scope.client
                }).success(function(data) {
                  $scope.client.id = data.id;
                  $scope.client.version = data.version;
                  return $timeout(function() {
                    $scope.$creating = false;
                    $scope.$created = true;
                    return $scope.forms.client.$setPristine();
                  }, Math.max(0, 800 - (Date.now() - start)));
                }).error(function(err) {
                  $scope.errorMessage = err;
                  return setErrors(start);
                });
              }
            });
          };
          return $scope.close = function() {
            if ($scope.forms.client.$dirty || !_.isEmpty($scope.formErrors)) {
              return $uibModalInstance.dismiss();
            } else {
              return $uibModalInstance.close($scope.client);
            }
          };
        }
      });
      return inst.result.then(function(client) {
        if (!!client.id) {
          $scope.clients[client.id] = client;
          return $location.path("/clients/" + client.id);
        }
      });
    };
    $scope["export"] = function(client, short) {
      return $http.post("api/clients/" + client + "/export", {
        short: short
      }).success(function(data) {
        return $window.open("api/clients/" + client + "/export");
      }).error(function(err) {
        $scope.errorMessage = err;
        return setErrors(start);
      });
    };
    return $scope.showAttributes = function(id) {
      var inst;
      return inst = $uibModal.open({
        templateUrl: 'client-attributes.html',
        controller: function($scope, $http, $uibModalInstance) {
          $scope.path = {};
          $http.get("api/attributes/" + id).success(function(attributes) {
            $scope.attributes = _(attributes).map(function(attr, id) {
              return _.assign(attr, {
                id: id,
                values: _(attr.values).map(function(value, id) {
                  return _.assign(value, {
                    id: id
                  });
                }).sortBy('name').value()
              });
            }).sortBy('name').value();
            return $scope.attributes.forEach(function(attr) {
              return $scope.path[attr.id] = attr.values[0].id;
            });
          });
          return $scope.make = function() {
            return JSON.stringify(_.map($scope.path, function(val, key) {
              return key + "/" + val;
            }), null, 2);
          };
        }
      });
    };
  });

  Admin.controller('Client', function($scope, $http, $route, $timeout, $uibModal) {
    var get_buckets;
    $scope.clients.$promise.then(function() {
      var base, base1;
      $scope.client = $scope.clients[$route.current.params.id];
      if ((base = $scope.client).dkim == null) {
        base.dkim = {};
      }
      if ($scope.client.debug) {
        $scope.client.email_mode = 'Debug';
      }
      if ((base1 = $scope.client).email_mode == null) {
        base1.email_mode = 'Live';
      }
      $scope.creating_aws = false;
      if ($scope.client.timezone) {
        return;
      }
      $scope.client.timezone = jstz.determine().name();
      return $scope.form.$setDirty();
    });
    $http.get("api/apikeys/" + $route.current.params.id).success(function(data) {
      return $scope.apikeys = data;
    });
    get_buckets = function() {
      return $http.get('api/clients/getBuckets').success(function(results) {
        var ref1, ref2;
        $scope.aws_buckets = _.pluck(results.buckets.Buckets, 'Name');
        $scope.aws_config = results.config;
        if (((ref1 = $scope.client.credentials) != null ? (ref2 = ref1.aws) != null ? ref2["private"] : void 0 : void 0) != null) {
          return $scope.private_folder = $scope.client.credentials.aws["private"].bucket + '/' + $scope.client.credentials.aws["private"].folder;
        }
      });
    };
    get_buckets();
    $scope.clear_selection = function() {
      var ref1, ref2;
      if ((ref1 = $scope.client) != null) {
        if ((ref2 = ref1.credentials) != null) {
          ref2["aws"] = {};
        }
      }
      return $scope.form.$setDirty();
    };
    $scope.save = function() {
      var ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8;
      if ((ref1 = $scope.client.webhooks) != null) {
        ref1.enabled_webhooks = [];
      }
      if (((ref2 = $scope.client.webhooks) != null ? ref2.use_integration : void 0) === true && ((ref3 = $scope.client.integrations) != null ? ref3._current : void 0) !== '') {
        if ($scope.client.integrations._current === 'velocify') {
          $scope.client.webhooks.url = 'http://localhost:9998/' + $scope.client.integrations._current + '?client=' + $scope.client.id;
        } else if ($scope.client.integrations._current === 'salesforce') {
          $scope.client.webhooks.url = 'http://localhost:9997/' + $scope.client.integrations._current + '?client=' + $scope.client.id;
        }
      }
      if ($scope.client.webhooks) {
        _.forEach($scope.client.webhooks.options, function(v, k) {
          if (v.enabled === true) {
            return $scope.client.webhooks.enabled_webhooks.push(k);
          }
        });
      }
      if ((ref4 = $scope.client.email_mode) === 'Live' || ref4 === 'Internal' || ref4 === 'Off') {
        $scope.client.debug = false;
      } else if ($scope.client.email_mode === 'Debug') {
        $scope.client.debug = true;
      }
      if ((((ref5 = $scope.client.credentials) != null ? (ref6 = ref5["aws"]) != null ? ref6.bucket : void 0 : void 0) != null) || (((ref7 = $scope.client.credentials) != null ? (ref8 = ref7["aws"]) != null ? ref8["private"] : void 0 : void 0) != null)) {
        $scope.client.credentials["aws"].key = $scope.aws_config.accessKeyId;
        $scope.client.credentials["aws"].secret = $scope.aws_config.secretAccessKey;
      }
      return $http.post('api/clients', {
        client: $scope.client
      }).success(function(data) {
        $scope.clients[data.id] = _.assign($scope.client, _.pick(data, 'version'));
        $scope.form.$setPristine();
        return get_buckets();
      });
    };
    $scope.create_private_folder = function() {
      var start;
      $scope.creating_aws = true;
      start = Date.now();
      return $http.post("api/clients/createFolder/" + $scope.client.id).success(function(results) {
        if ($scope.client.credentials == null) {
          $scope.client.credentials = {
            aws: {}
          };
        }
        $scope.client.credentials["aws"]["private"] = {};
        $scope.client.credentials["aws"]["private"].bucket = results.bucket_name;
        $scope.client.credentials["aws"]["private"].folder = results.folder_name;
        return $timeout(function() {
          $scope.creating_aws = false;
          return $scope.save();
        }, Math.max(0, 800 - (Date.now() - start)));
      });
    };
    $scope.createS3Bucket = function() {
      $scope.creatingBucket = true;
      $scope.client.credentials = _.assign({}, $scope.client.credentials, {
        aws: {
          bucket: ' ',
          key: '',
          secret: ''
        }
      });
      return $http.post("api/clients/createBucket/", {
        bucketName: $scope.client.name
      }).success(function(data) {
        $scope.client.credentials["aws"].bucket = data.bucketName;
        $scope.client.credentials["aws"].key = data.accessKeyId;
        $scope.client.credentials["aws"].secret = data.secretAccessKey;
        if ($scope.client.template === "6e2a0c50257c11e59604e772aaac09c0") {
          $http.post("api/clients/copyTemplateBucket/", {
            templateBucketName: "sv-solartemplate",
            newBucketName: data.bucketName
          });
          $scope.creatingBucket = false;
          return $scope.save();
        } else {
          $scope.creatingBucket = false;
          return $scope.save();
        }
      });
    };
    $scope.revoke = function(key) {
      return $http["delete"]("api/apikeys/" + key.id).success(function(data) {
        return key.revoked = data.key.revoked;
      });
    };
    $scope.addDKIM = function() {
      var inst;
      inst = $uibModal.open({
        templateUrl: 'client/addDKIM.html',
        controller: function($scope, $uibModalInstance) {
          $scope.key = {};
          return $scope.add = function() {
            return $uibModalInstance.close($scope.key);
          };
        }
      });
      return inst.result.then(function(key) {
        var base;
        if ((base = $scope.client.dkim).keys == null) {
          base.keys = [];
        }
        $scope.client.dkim.keys.push(key);
        return $scope.form.$setDirty();
      });
    };
    $scope.generateDKIM = function() {
      var inst;
      inst = $uibModal.open({
        templateUrl: 'client/generateDKIM.html',
        controller: function($scope, $uibModalInstance) {
          $scope.key = {};
          return $scope.generate = function() {
            return $uibModalInstance.close($scope.key);
          };
        }
      });
      return inst.result.then(function(key) {
        return $http.post('api/keypair').success(function(keypair) {
          var base;
          if ((base = $scope.client.dkim).keys == null) {
            base.keys = [];
          }
          $scope.client.dkim.keys.push(_.assign(key, keypair));
          return $scope.form.$setDirty();
        });
      });
    };
    $scope.viewDKIM = function(key) {
      var inst;
      return inst = $uibModal.open({
        templateUrl: 'client/addDKIM.html',
        scope: _.assign($scope.$new(), {
          ro: true,
          key: key
        })
      });
    };
    $scope.removeDKIM = function(key) {
      _.remove($scope.client.dkim.keys, key);
      return $scope.form.$setDirty();
    };
    $scope.setupDNS = function() {
      $scope.checking_dns = true;
      return $http.post('api/dns/check', {
        id: $scope.client.id,
        prefix: $scope.client.svemails.prefix
      }).success(function(result) {
        $scope.checking_dns = false;
        if (result.success) {
          return $scope.client.svemails.dnscheck = {
            prefix: $scope.client.svemails.prefix,
            valid: true,
            checked: new Date()
          };
        }
      });
    };
    return $scope["import"] = function(id, short) {
      var inst;
      return inst = $uibModal.open({
        templateUrl: 'client-import.html',
        controller: 'ClientImportModal',
        resolve: {
          id: function() {
            return id;
          },
          short: function() {
            return short;
          }
        }
      });
    };
  });

  Admin.controller('Users', function($scope, $http, $q, $location, $uibModal) {
    $scope.search = $location.search();
    $scope.search.disabled = $scope.search.disabled === 'yes' ? true : false;
    $http.get('api/users?limit=100').success(function(users) {
      return $scope.users = users;
    });
    $scope.$watch('users', function() {
      return _.forEach(_.values($scope.users), function(user) {
        return user.fullname = user.firstname + ' ' + user.lastname;
      });
    }, true);
    $scope.$watch('search', function() {
      return $location.search({
        disabled: $scope.search.disabled ? 'yes' : void 0
      });
    }, true);
    $scope.searching = function(item) {
      if ($scope.search.disabled || !item.disabled) {
        return true;
      }
    };
    $scope.create = function() {
      var inst;
      inst = $uibModal.open({
        templateUrl: 'user-edit.html',
        controller: function($scope, $http, $uibModalInstance, $timeout, dismissCheck) {
          var requiredVariables, setErrors, user_ref;
          $scope.$isNewUser = true;
          $scope.user = {
            client: ''
          };
          user_ref = {};
          angular.copy($scope.user, user_ref);
          setErrors = function(start) {
            return $timeout(function() {
              $scope.forms.user.$invalid = true;
              $scope.$editing = false;
              return $scope.forms.user.$setPristine();
            }, Math.max(0, 800 - (Date.now() - start)));
          };
          requiredVariables = function(user, cb) {
            var formErrors, ref1;
            formErrors = {};
            if (!((ref1 = user.username) != null ? ref1.length : void 0)) {
              formErrors.username = true;
            }
            if ($scope.$isNewUser && !user.password) {
              formErrors.password = true;
            }
            return cb(formErrors);
          };
          $scope.save = function() {
            var start;
            $scope.formErrors = {};
            $scope.$editing = true;
            $scope.errorMessage = void 0;
            $scope.$saved = false;
            start = Date.now();
            return requiredVariables($scope.user, function(formErrors) {
              if (!_.isEmpty(formErrors)) {
                $scope.formErrors = formErrors;
                return setErrors(start);
              } else {
                delete $scope.user.fullname;
                return $http.post('api/users', {
                  user: $scope.user
                }).success(function(data) {
                  $scope.user.id = data.id;
                  $scope.user.version = data.version;
                  return $timeout(function() {
                    $scope.$editing = false;
                    $scope.$saved = true;
                    angular.copy($scope.user, user_ref);
                    return $scope.forms.user.$setPristine();
                  }, Math.max(0, 800 - (Date.now() - start)));
                }).error(function(err) {
                  $scope.errorMessage = err;
                  return setErrors(start);
                });
              }
            });
          };
          return $scope.close = function() {
            console.log('forms.user:', $scope.forms.user);
            if ($scope.$saved && $scope.forms.user.$pristine) {
              return $uibModalInstance.close($scope.user);
            } else if ($scope.forms.user.$dirty || !_.isEmpty($scope.formErrors)) {
              return dismissCheck.dismiss($uibModalInstance, $scope.user, user_ref);
            } else {
              return $uibModalInstance.dismiss();
            }
          };
        }
      });
      return inst.result.then(function(user) {
        console.log('users: ', $scope.users);
        if (!!user) {
          $scope.users.push(user);
        }
        return console.log('post users: ', $scope.users);
      });
    };
    $scope.edit = function(user) {
      var inst;
      inst = $uibModal.open({
        templateUrl: 'user-edit.html',
        controller: function($scope, $http, $uibModalInstance, $timeout, dismissCheck) {
          var requiredVariables, setErrors, user_ref;
          $scope.$isNewUser = false;
          $scope.user = angular.copy(user);
          user_ref = {};
          angular.copy($scope.user, user_ref);
          setErrors = function(start) {
            return $timeout(function() {
              $scope.forms.user.$invalid = true;
              $scope.$editing = false;
              return $scope.forms.user.$setPristine();
            }, Math.max(0, 800 - (Date.now() - start)));
          };
          requiredVariables = function(user, cb) {
            var formErrors, ref1;
            formErrors = {};
            if (!((ref1 = user.username) != null ? ref1.length : void 0)) {
              formErrors.username = true;
            }
            return cb(formErrors);
          };
          $scope.save = function() {
            var start;
            $scope.formErrors = {};
            $scope.$editing = true;
            $scope.errorMessage = void 0;
            $scope.$saved = false;
            start = Date.now();
            return requiredVariables($scope.user, function(formErrors) {
              if (!_.isEmpty(formErrors)) {
                $scope.formErrors = formErrors;
                return setErrors(start);
              } else {
                delete $scope.user.fullname;
                return $http.put('api/users/' + $scope.user.username, {
                  user: $scope.user
                }).success(function(data) {
                  console.log('data: ', data);
                  $scope.user.version = data.version;
                  return $timeout(function() {
                    $scope.$editing = false;
                    $scope.$saved = true;
                    angular.copy($scope.user, user_ref);
                    angular.copy($scope.user, user);
                    return $scope.forms.user.$setPristine();
                  }, Math.max(0, 800 - (Date.now() - start)));
                }).error(function(err) {
                  $scope.errorMessage = err.message.message;
                  return setErrors(start);
                });
              }
            });
          };
          return $scope.close = function() {
            if ($scope.$saved && $scope.forms.user.$pristine) {
              return $uibModalInstance.close($scope.user);
            } else if ($scope.forms.user.$dirty || $scope.formErrors) {
              return dismissCheck.dismiss($uibModalInstance, $scope.user, user_ref);
            } else {
              return $uibModalInstance.dismiss();
            }
          };
        }
      });
      return inst.result.then(function(user) {
        return $http.get('api/users?limit=100').success(function(users) {
          return $scope.users = users;
        }).error(function(err) {
          return console.error('Failed to load users. ', err);
        });
      });
    };
    return $scope.disable = function(user) {
      user.disabled = true;
      return $http.put('api/users/' + user.username, {
        user: user
      });
    };
  });

  Admin.controller('APIKeys', function($scope, $location, $http) {
    $scope.search = $location.search();
    return $http.get("api/apikeys").success(function(data) {
      return $scope.keys = data;
    });
  });

  Admin.controller('Billing', function($scope, $http) {
    return $scope.$watch('clients', function(clients) {
      var reload;
      if (!clients) {
        return;
      }
      $scope.activeClients = _(clients).filter(function(v, k) {
        return k[0] !== '$';
      }).filter(function(client) {
        return !client.archive;
      }).map(function(client, id) {
        return {
          id: client.id,
          name: client.name,
          short: client.short
        };
      }).value();
      reload = function() {
        return _.forEach($scope.activeClients, function(client) {
          var params;
          params = {
            from: $scope.range.from.toISOString(),
            to: $scope.range.to.toISOString(),
            client: client.id
          };
          return $http.get('api/billing', {
            params: params
          }).then(function(res) {
            return client.billing = res.data;
          });
        });
      };
      return $scope.$watch('range', _.debounce(reload, 100), true);
    }, true);
  });

  Admin.controller('ClientImportModal', [
    '$scope', '$http', '$uibModalInstance', '$uibModal', '$window', 'id', 'short', function($scope, $http, $uibModalInstance, $uibModal, $window, id, short) {
      $scope.forms = {
        upload: null
      };
      $scope.$watch('forms.upload.json', function(json) {
        if (!json) {
          return;
        }
        return $scope.typeerror = !~json.name.indexOf('.json');
      });
      return $scope.upload = function() {
        var fd;
        if (!~$scope.forms.upload.json.name.indexOf('.json')) {
          return $scope.typeerror = true;
        }
        fd = new FormData;
        fd.append('file', $scope.forms.upload.json);
        return $http.post("api/clients/" + id + "/import/" + short, fd, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': void 0
          }
        }).success(function() {
          $window.open("api/clients/" + id + "/import");
          return $uibModalInstance.close();
        });
      };
    }
  ]);

  Admin.controller('Templates', function($scope, $http, $timeout) {
    $http.get('api/templates').success(function(data) {
      return $scope.templates = data;
    });
    return $scope.save = function() {
      return $http.post('api/templates', $scope.templates).success(function(data) {
        return $timeout((function() {
          return $scope.forms.templates.$setPristine();
        }), 1000);
      });
    };
  });

  Admin.service('timer', [
    '$timeout', function($timeout) {
      return function(min, cb) {
        var ref1, start;
        if (!cb) {
          ref1 = [min, 600], cb = ref1[0], min = ref1[1];
        }
        start = Date.now();
        return function() {
          var args;
          args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
          return $timeout(function() {
            return cb.apply(null, args);
          }, Math.max(0, min - (Date.now() - start)));
        };
      };
    }
  ]);

  Admin.controller('Optouts', [
    '$scope', '$http', '$location', '$timeout', 'timer', function($scope, $http, $location, $timeout, timer) {
      var email;
      $scope.search = $location.search();
      $scope.forms = {};
      $scope.load = function(email) {
        $scope.details = void 0;
        $scope.forms.search.$submitting = true;
        email = $.trim(email);
        if (!isValidEmailAddress(email)) {
          return setTimeout(function() {
            $scope.forms.search.$submitting = false;
            return $scope.details = {
              notfound: true
            };
          }, 600);
        }
        $location.search({
          email: email
        });
        return $http.get('api/optouts/' + email).success(timer(function(data) {
          $scope.forms.search.$submitting = false;
          return $scope.details = data;
        })).error(timer(function() {
          $scope.forms.search.$submitting = false;
          return $scope.details = {
            id: email,
            clients: [],
            history: [],
            global: false
          };
        }));
      };
      if (email = $scope.search.email) {
        $timeout(function() {
          return $scope.load($scope.search.email = email);
        });
      }
      $scope.search = function() {
        return $scope.load($scope.search.email);
      };
      $scope.set_global = function() {
        var method;
        method = $scope.details.global ? 'optout' : 'optin';
        return $http.post("api/" + method + "/" + $scope.details.id).success(function() {
          return $scope.details.history.unshift({
            at: new Date(),
            type: "global " + method,
            from: 'webadmin'
          });
        }).error(console.error.bind(console));
      };
      $scope.optout = function(client) {
        return $http.post("api/optout/" + $scope.details.id, {
          client: client
        }).success(function() {
          $scope.details.clients.push(client);
          return $scope.details.history.unshift({
            at: new Date(),
            type: 'optout',
            from: 'webadmin',
            client: client
          });
        }).error(console.error.bind(console));
      };
      return $scope.optin = function(client) {
        return $http.post("api/optin/" + $scope.details.id, {
          client: client
        }).success(function() {
          _.pull($scope.details.clients, client);
          return $scope.details.history.unshift({
            at: new Date(),
            type: 'optin',
            from: 'webadmin',
            client: client
          });
        }).error(console.error.bind(console));
      };
    }
  ]);

  Admin.controller('AWS', [
    '$scope', '$rootScope', '$http', '$location', '$timeout', '$uibModal', 'FileUploader', function($scope, $rootScope, $http, $location, $timeout, $uibModal, FileUploader) {
      $http.get('api/clients/getBuckets').success(function(results) {
        $scope.aws_buckets = _.pluck(results.buckets.Buckets, 'Name');
        $scope.aws_config = results.config;
        if ($scope.aws_config !== 'production') {
          return $scope.aws_bucket = 'softvu-staging';
        }
      });
      $scope.uploader = new FileUploader;
      return $scope.uploader.onBeforeUploadItem = function(item) {
        return item.formData.push({
          key: item.key
        });
      };
    }
  ]).directive('awsFileSelect', [
    '$http', function($http) {
      return {
        scope: {
          uploader: '=',
          bucket: '='
        },
        link: function(scope, elm, attrs) {
          if (!scope.uploader.isHTML5) {
            elm.removeAttr('multiple');
          }
          elm.bind('change', function() {
            var data, options;
            if (scope.bucket != null) {
              data = scope.uploader.isHTML5 ? this.files : [this];
              options = scope.$eval(attrs.awsFileSelect) || {};
              _.forEach(data, function(file) {
                return $http.get("api/public/aws/" + scope.bucket + "?type=" + (encodeURIComponent(file.type))).success(function(policy) {
                  var opts;
                  opts = _.extend({}, options);
                  if (opts.formData == null) {
                    opts.formData = [];
                  }
                  opts.formData.push(_.omit(policy, 'bucket'));
                  opts.formData.push({
                    acl: 'public-read',
                    'Content-Type': file.type,
                    success_action_status: '201'
                  });
                  opts.key = file.name;
                  opts.url = "https://" + scope.bucket + ".s3.amazonaws.com";
                  return scope.uploader.addToQueue(file, opts);
                });
              });
            } else {
              return;
            }
            if (scope.uploader.isHTML5 && elm.attr('multiple')) {
              return elm.prop('value', null);
            }
          });
          return elm.prop('value', null);
        }
      };
    }
  ]);

  $(function() {
    return angular.bootstrap(document, ['Admin']);
  });

  (function($) {
    return $(document).on('mouseenter mouseleave', 'tbody.events tr', function(e) {
      var el, tbody;
      el = $(this);
      if (!/\w{32}/.test(el.data('ref'))) {
        return;
      }
      tbody = el.parent();
      if (e.type === 'mouseenter') {
        return tbody.find(".evt-ref-" + (el.data('ref'))).addClass('hover');
      } else if (e.type === 'mouseleave') {
        return tbody.find('.hover').removeClass('hover');
      }
    });
  })(jQuery);

  Admin.controller('DNS', function($scope, $http, $timeout) {
    var g, m, y;
    window.record_html = $('#dns-record-template').html();
    window.message_html = '<div class={color_class}>&gt;&nbsp;{message}</div>';
    m = 'message';
    g = 'green-text';
    y = 'yellow-text';
    window.messages = {
      page_done: [m, g, 'ready'],
      get_actionqueue_fail: [m, y, 'get_actionqueue() function failed'],
      post_actionqueue_success: [m, g, 'updated actionqueue successfully'],
      post_actionqueue_fail: [m, y, 'update_actionqueue() function failed'],
      refresh_actionqueue_success: [m, g, 'refreshed actionqueue successfully'],
      get_settings_fail: [m, y, 'get_settings() function failed'],
      post_settings_success: [m, g, 'updated settings successfully'],
      post_settings_fail: [m, y, 'post_settings() function failed'],
      refresh_settings_success: [m, g, 'refreshed settings successfully'],
      get_records_fail: [m, y, 'get_records() function failed'],
      upsert_record_fail: [m, y, 'upsert_record() function failed'],
      upsert_record_success: [m, g, 'upserted record successfully'],
      delete_record_fail: [m, y, 'delete_record() function failed'],
      delete_record_success: [m, g, 'deleted record successfully'],
      verify_record_fail: [m, y, 'verify_record() function failed'],
      verify_record_success: [m, g, 'verify_record function ran successfully']
    };
    $(window).scroll(window_scroll);
    return start_dns_stuff();
  });

  start_dns_stuff = function() {
    $('#dns-update-aq').click(post_actionqueue);
    $('#dns-refresh-aq').click(refresh_actionqueue);
    $('#dns-update-settings').click(post_settings);
    $('#dns-refresh-settings').click(refresh_settings);
    return get_clients(function() {
      set_clients(function() {});
      return get_records(function() {
        return write_records(function() {
          return new_record(function() {
            return get_actionqueue(function() {
              return get_settings(function() {
                return dns_message(window.messages.page_done);
              });
            });
          });
        });
      });
    });
  };

  get_clients = function(cb) {
    return $.get('dns/clients', {}, (function(data) {
      window.clients = data;
      return cb();
    }), 'json');
  };

  set_clients = function(cb) {
    var c, i, o;
    i = 0;
    while (i < window.clients.length) {
      o = '<option value={value}>{name}</option>';
      c = [
        {
          k: 'value',
          v: window.clients[i].id
        }, {
          k: 'name',
          v: window.clients[i].name
        }
      ];
      $('#dns-client-select').append(o.map(c));
      i++;
    }
    $('#dns-client-select').change(function() {
      return client_select_change();
    });
    return cb();
  };

  get_records = function(cb) {
    return $.get('dns/records', {}, (function(data) {
      if (data.err != null) {
        dns_message(window.messages.get_records_fail);
        dns_message(['message', 'red-text', data.err.message]);
      } else {
        window.dns_records = data.ret;
      }
      return cb();
    }), 'json');
  };

  write_records = function(cb) {
    var a, color_class, i, r;
    $('#dns-records-div').html('');
    r = window.dns_records;
    i = 0;
    color_class = '';
    window.global_count = 0;
    window.client_count = 0;
    while (i < r.length) {
      if (r[i].isglobal === true) {
        color_class = 'dns-global-color';
        window.global_count++;
      } else {
        color_class = 'dns-client-color';
        window.client_count++;
      }
      a = [
        {
          k: 'id',
          v: r[i].id
        }, {
          k: 'name',
          v: r[i].name
        }, {
          k: 'domain',
          v: r[i].domain
        }, {
          k: 'value',
          v: r[i].value
        }
      ];
      $('#dns-records-div').append(window.record_html.map(a));
      $('#dns-container-' + r[i].id).addClass(color_class);
      $('#dns-type-' + r[i].id).val(r[i].type);
      $('#isglobal-' + r[i].id).prop('checked', r[i].isglobal);
      i++;
    }
    return cb();
  };

  new_record = function(cb) {
    var a, html;
    html = '<br/><h4 class="dns-left-column-title">New DNS Record</h4>';
    $('#dns-records-div').append(html);
    a = [
      {
        k: 'id',
        v: '0'
      }, {
        k: 'name',
        v: ''
      }, {
        k: 'domain',
        v: ''
      }, {
        k: 'value',
        v: ''
      }
    ];
    $('#dns-records-div').append(window.record_html.map(a));
    $('#dns-update-0').html('Add');
    $('#dns-delete-0').remove();
    $('#dns-verify-0').remove();
    $('.dns-delete').click(function(e) {
      return delete_record(e);
    });
    $('.dns-verify').click(function(e) {
      return verify_record(e);
    });
    $('.dns-update').click(function(e) {
      return upsert_record(e);
    });
    $('.dns-record-checkbox').click(function(e) {
      return checkbox_click(e);
    });
    $('#dns-container-0').addClass('dns-red');
    $('#dns-edit-globals').html(window.global_count);
    $('#dns-edit-clients').html(window.client_count);
    return cb();
  };

  get_actionqueue = function(cb) {
    return $.get('dns/actionqueue', {}, (function(data) {
      if (data.err != null) {
        dns_message(window.messages.get_actionqueue_fail);
        dns_message(['message', 'red-text', data.err.message]);
      } else {
        window.aq = data.ret[0];
        $('#dns-aq-status-input').val(window.aq.status);
        $('#dns-aq-cron-input').val(window.aq.repeat);
      }
      return cb();
    }), 'json');
  };

  post_actionqueue = function() {
    var cron, status;
    status = $('#dns-aq-status-input').val();
    cron = $('#dns-aq-cron-input').val();
    return $.post('dns/actionqueue', {
      status: status,
      cron: cron
    }, (function(data) {
      if (data.err != null) {
        dns_message(window.messages.post_actionqueue_fail);
        return dns_message(['message', 'red-text', data.err.message]);
      } else {
        return dns_message(window.messages.post_actionqueue_success);
      }
    }), 'json');
  };

  refresh_actionqueue = function() {
    return get_actionqueue(function() {
      return dns_message(window.messages.refresh_actionqueue_success);
    });
  };

  get_settings = function(cb) {
    return $.get('dns/settings', {}, (function(data) {
      if (data.err != null) {
        dns_message(window.messages.get_settings_fail);
        dns_message(['message', 'red-text', data.err.message]);
      } else {
        window.dns_settings = data.ret[0];
        $('#dns-settings-email-input').val(window.dns_settings.email_to);
        $('#dns-settings-zone-input').val(window.dns_settings.zone_id);
      }
      return cb();
    }), 'json');
  };

  post_settings = function() {
    var email, zone;
    email = $('#dns-settings-email-input').val();
    zone = $('#dns-settings-zone-input').val();
    return $.post('dns/settings', {
      email: email,
      zone: zone
    }, (function(data) {
      if (data.err != null) {
        dns_message(window.messages.post_settings_fail);
        return dns_message(['message', 'red-text', data.err.message]);
      } else {
        return dns_message(window.messages.post_settings_success);
      }
    }), 'json');
  };

  refresh_settings = function() {
    return get_settings(function() {
      return dns_message(window.messages.refresh_settings_success);
    });
  };

  delete_record = function(e) {
    var id;
    id = e.currentTarget.id.split('-')[2];
    return $.post('dns/delete_record', {
      id: id
    }, (function(data) {
      if (data.err != null) {
        dns_message(window.messages.delete_record_fail);
        return dns_message(['message', 'red-text', data.err.message]);
      } else {
        dns_message(window.messages.delete_record_success);
        return get_records(function() {
          return write_records(function() {
            return new_record();
          });
        });
      }
    }), 'json');
  };

  verify_record = function(e) {
    var id, records;
    id = e.currentTarget.id.split('-')[2];
    records = [id, 10];
    return verify_records(records);
  };

  verify_all = function() {
    var i, records;
    i = 0;
    records = [];
    while (i < window.dns_records.length) {
      records.push(window.dns_records[i].id);
      i++;
    }
    return verify_records(records);
  };

  verify_records = function(records) {
    return $.post('dns/verify_records', {
      records: records
    }, (function(data) {
      if (data.err != null) {
        dns_message(window.messages.verify_record_fail);
        return dns_message(['message', 'red-text', data.err.message]);
      } else {
        window.verify = data.ret;
        return dns_message(window.messages.verify_record_success);
      }
    }), 'json');
  };

  verify_records_old_old = function(records) {
    var i, results1;
    i = 0;
    results1 = [];
    while (i < records.length) {
      $.post('dns/verify_record', {
        records: records
      }, (function(data) {
        if (data.err != null) {
          dns_message(window.messages.verify_record_fail);
          return dns_message(['message', 'red-text', data.err.message]);
        } else {
          window.verify = data.ret;
          return dns_message(window.messages.verify_record_success);
        }
      }), 'json');
      results1.push(i++);
    }
    return results1;
  };

  upsert_record = function(e) {
    var id;
    id = e.currentTarget.id.split('-')[2];
    return $.post('dns/record', {
      id: id,
      client_id: $('#dns-client-select').val(),
      isglobal: $('#isglobal-' + id).prop('checked'),
      name: $('#dns-name-' + id).val(),
      type: $('#dns-type-' + id).val(),
      domain: $('#dns-domain-' + id).val(),
      value: $('#dns-value-' + id).val()
    }, (function(data) {
      if (data.err != null) {
        dns_message(window.messages.upsert_record_fail);
        return dns_message(['message', 'red-text', data.err.message]);
      } else {
        dns_message(window.messages.upsert_record_success);
        return get_records(function() {
          return write_records(function() {
            return new_record();
          });
        });
      }
    }), 'json');
  };

  window_scroll = function() {
    return sticky_right_col();
  };

  sticky_right_col = function() {
    var e, w;
    w = $(window).scrollTop();
    e = $('.dns-right-column');
    if ($(window).width() >= 1858) {
      return e.css('margin-top', w + 'px');
    } else {
      return e.css('margin-top', 0);
    }
  };

  checkbox_click = function(e) {
    var id, new_class;
    id = e.currentTarget.id.split('-')[1];
    $('#dns-container-' + id).removeClass('dns-global-color').removeClass('dns-client-color');
    new_class = $('#isglobal-' + id).prop('checked') ? 'dns-global-color' : 'dns-client-color';
    return $('#dns-container-' + id).addClass(new_class);
  };

  dns_message = function(args) {
    var e, html, obj;
    obj = [
      {
        k: 'color_class',
        v: args[1]
      }, {
        k: 'message',
        v: args[2]
      }
    ];
    html = window.message_html.map(obj);
    e = document.getElementsByClassName('dns-' + args[0] + '-scrollable')[0];
    $(e).append(html);
    return e.scrollTop = e.scrollHeight;
  };

  String.prototype.map = function() {
    var a, f, i, r, t;
    a = arguments;
    t = this;
    i = 0;
    while (i < a[0].length) {
      f = '{' + a[0][i].k + '}';
      r = new RegExp(f, 'g');
      t = t.replace(r, a[0][i].v);
      i++;
    }
    return t;
  };

  client_select_change = function() {
    var client_id;
    client_id = $('#dns-client-select').val();
    if (client_id === 'default') {
      return console.log(client_id);
    } else {
      return console.log('it ain\'t the default...');
    }
  };

  start_dns_stuff_old = function() {
    var client_id;
    client_id = window.location.pathname.split('/')[3];
    window.client_id = client_id;
    window.test = function() {
      return window_test();
    };
    window.test_html = $('#dns-test-template').html();
    load_dns_tests(function() {
      return print_tests(function() {
        return new_test();
      });
    });
    $('#dns-client-start-button').click(function() {
      return client_start_click();
    });
    return $('#dns-global-start-button').click(function() {
      return global_start_click();
    });
  };

  update_test = function(e) {
    var id;
    id = e.currentTarget.id.split('-')[2];
    return $.post('lib/update_test', {
      id: id,
      client_id: $('#dns-client-select').val(),
      isglobal: $('#isglobal-' + id).prop('checked'),
      name: $('#dns-name-' + id).val(),
      type: $('#dns-type-' + id).val(),
      domain: $('#dns-domain-' + id).val(),
      value: $('#dns-value-' + id).val()
    }, (function(data) {
      if (data.success === true) {
        dns_message('dns-main', 'Successfully Wrote DNS Record');
        return load_dns_tests(function() {
          return print_tests(function() {
            return new_test();
          });
        });
      } else {
        return dns_message('dns-main', data.err.message);
      }
    }), 'json');
  };

  client_start_click = function() {
    var client_id;
    client_id = window.location.pathname.split('/')[3];
    return $.post('lib/start_tests', {
      client_id: client_id,
      mode: 'client'
    }, (function(data) {
      return console.log(data);
    }), 'json');
  };

  global_start_click = function() {
    return console.log('start global tests');
  };

  get_doc = function(cb) {
    return $.post('lib/get_client_doc', {
      client_id: window.client_id
    }, (function(data) {
      window.client_doc = data;
      return cb();
    }), 'json');
  };

  start_tests_old = function() {
    var g, i, results1, t;
    i = 0;
    t = window.dns_tests;
    g = window.dns_test_mode === 'client' ? false : true;
    results1 = [];
    while (i < t.length) {
      if (t[i].isglobal === g) {
        run_test(i);
      }
      results1.push(i++);
    }
    return results1;
  };

  run_test = function(test_i) {
    return $.post('lib/run_test', {
      test_id: window.dns_tests[test_i].id
    }, (function(data) {
      console.log('done running test ' + window.dns_tests[test_i].id);
      return console.log(data.test_id);
    }), 'json');
  };

  String.prototype.f = function() {
    var args;
    args = arguments;
    return this.replace(/\{(\S+)\}/g, function(m, n) {
      return args[n];
    });
  };

  (function($) {
    var clips;
    clips = new Clipboard('.copyable');
    return clips.on('success', function(e) {
      var tip;
      tip = $(e.trigger);
      tip.tooltip({
        title: 'Copied!'
      });
      tip.tooltip('show');
      return setTimeout((function() {
        return tip.tooltip('destroy');
      }), 2000);
    });
  })(jQuery);

}).call(this);
