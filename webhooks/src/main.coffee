if ~process.cwd().indexOf 'webhooks'
  process.env.NODE_CONFIG_DIR ?= '../config'

config = require 'config'

events = require 'shared/eventlib'
listen = require 'shared/queues/listen'
logger = require('shared/logging').default()

processor = require './processor'

listener = listen config.queues.url,
  retries:
    enabled: yes
    waits: config.webhooks['retry wait in minutes'].map (x) -> x * 60 * 1000

listener.consume 'webhooks', processor, ->
  logger.info "listening to queue \"webhooks\" env: #{process.env.NODE_ENV or 'dev'}"
  events.heartbeat 'webhooks.heartbeat'

listener.on 'error', (err) ->
  logger.error 'listener errored', error: err
  events.emit 'webhooks.error', null, at: new Date, error: err

graceful = ->
  listener.close -> process.exit()

process.on 'SIGTERM', graceful
process.on 'SIGINT', graceful
process.on 'uncaughtException', (err) ->
  console.error 'Webhooks Exception', err

require('shared/utils/process').lockfile __dirname
require('shared/integration/velocify/webhooks')
require('shared/integration/salesforce/webhooks')
