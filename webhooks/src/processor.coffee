config = require 'config'

_ = require 'lodash'
request = require 'request'
uuid = require 'uuid'
xml2js = require 'xml2js'

{RepositoryFactory} = require 'shared/database'
factory = new RepositoryFactory config.db.url
clients = factory.use 'core/clients'
logs = factory.use 'core/logs'

module.exports = process_webhook_data = (data, done) ->
  # this is defensive, all the webhooks should be coming
  # from the eventhose, which also checks for a client before sending
  unless data.client?
    console.log 'webhook skipped, no client'
    return done()

  id = uuid.v1().replace /-/g, ''
  get_client data.client, (err, client) ->
    unless client?.webhooks?.enabled
      console.log 'webhook skipped, webhooks disabled'
      return done()

    json = _ data
      .omit 'client'
      .omit (val, key) -> key[0] in ['_', '$']
      .value()

    # TODO should we time these for diagnostics?
    # TODO we should also support batching... (possibly with rabbit tmp queues?)
    request.post client.webhooks.url,
      json: json
      headers:
        'X-Softvu-Delivery': id
        'X-Softvu-Type': data.type if data.type
    , (err, response, body) ->
      log =
        id: id
        target: client.webhooks.url
        at: new Date()
        type: 'webhook'
        client: data.client
        event: data.id
        hookid: id
        ok: !err

      if err
        log.req = json
        log.error = err
      else if response.statusCode is 404
        err = null
        log.req = json
        log.res = _.pick response, ['headers', 'statusCode', 'body']
      else if response.statusCode isnt 200
        err = body
        log.req = json
        log.res = _.pick response, ['headers', 'statusCode', 'body']

      logs.insert log
      done err

module.exports._get_client = get_client = do (cache = {}) ->
  (id, cb) ->
    # return cb null, cache[id] if cache[id]
    clients.get id, (err, client) -> cb err, cache[id] = client
