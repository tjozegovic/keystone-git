http = require 'http'

server = http.createServer()
require('shared/sockets') server
server.listen process.env.PORT or 9999
